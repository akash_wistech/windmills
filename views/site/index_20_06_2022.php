<?php

use yii\helpers\Url;
use app\models\Valuation;
use app\models\Buildings;
use app\models\Lead;
/* @var $this yii\web\View */
$this->title = Yii::t('app','Dashboard');
?>

<div class="row px-4">
    <?php
    $BarChartData='';
    //number count for client, buildings, valuation steps.
    $totalValuation =(int)Valuation::find()->count('id');
    $clients =(int)\app\models\Company::find()->count('id');
    $buildings =Buildings::find()->count('id');


    //Valuation steps such as received, reviewed. approved etc.
    foreach (Yii::$app->helperFunctions->valuationStatusListArr as $key => $value) {
        $valuationReport[$key] =(int)Valuation::find()->where(['valuation_status'=>$key])->count('id');
    }
    $valuationReport[9] =(int)Valuation::find()->where(['valuation_status'=>9])->count('id');

    $valuationReport_today =(int)Valuation::find()->where(['valuation_status'=>'1'])->andFilterWhere(['instruction_date'=> date('Y-m-d')])->count('id');
    $valuationReport_active =(int)Valuation::find()->andFilterWhere(['IN', 'valuation_status', [1,2,3,4,7,8]])->count('id');

    $valuationReport_today_receieved =(int)Valuation::find()->where(['instruction_date'=> date('Y-m-d')])->count('id');
    $valuationReport_cancel_today =(int)Valuation::find()->where(['valuation_status'=>'9'])->andFilterWhere(['instruction_date'=> date('Y-m-d')])->count('id');
    $valuationReport_challneged_today =(int)Valuation::find()->where(['revised_reason'=>[1,2]])->andFilterWhere(['instruction_date'=> date('Y-m-d')])->count('id');
    $valuationReport_challneged =(int)Valuation::find()->where(['revised_reason'=>[1,2]])->count('id');
    $valuationReport_active_challneged =(int)Valuation::find()->where(['revised_reason'=>[1,2]])->andFilterWhere(['IN', 'valuation_status', [1,2,3,4,7,8]])->count('id');
    $valuationReport_today_inspected=  \Yii::$app->db->createCommand('SELECT COUNT(valuation.id) as inspected
FROM valuation
INNER JOIN schedule_inspection ON valuation.id=schedule_inspection.valuation_id
WHERE schedule_inspection.inspection_date ="2021-11-23" AND
valuation.valuation_status NOT IN(6,9)')->execute();
    $totalResults = (new \yii\db\Query())
        ->select(' COUNT(valuation.id) as count')
        ->from('valuation')
        ->innerJoin('schedule_inspection',"schedule_inspection.valuation_id=valuation.id")

        //->innerJoin('schedule_inspection ON valuation.id=schedule_inspection.valuation_id')
        ->where(['schedule_inspection.inspection_date' => date('Y-m-d')])
        ->andWhere(['valuation.valuation_status' => [1,2,3,4,5,7,8]])
        ->all();
    $valuationReport_today_inspected = $totalResults[0]['count'];


$today_start_date = date('Y-m-d').' 00:00:00';
$today_end_date = date('Y-m-d').' 23:59:59';

 $valuationReport_today_inspected_done =(int)\app\models\InspectProperty::find()->andFilterWhere(['between','inspection_done_date',$today_start_date, $today_end_date])->count('id');
    $valuationReport_today_approved =(int)\app\models\ValuationApproversData::find()->where(['approver_type'=>'approver'])->andFilterWhere(['between','created_at',$today_start_date, $today_end_date])->count('id');


    if(Yii::$app->user->identity->permission_group_id == 15){
        foreach (Yii::$app->helperFunctions->valuationStatusListArr as $key => $value) {
            $valuationReport[$key] =(int)Valuation::find()->where(['valuation_status'=>$key])->andWhere(['client_id'=>Yii::$app->user->identity->company_id ])->count('id');
            $totalValuation =(int)Valuation::find()->where(['client_id'=>Yii::$app->user->identity->company_id])->count('id');
        }
    }


    // echo "<pre>";
    // print_r($valuationByyearjson);
    // echo "<pre>";
    // die();
    ?>
</div>
<?php  if(Yii::$app->menuHelperFunction->checkActionAllowed('dashboard')){ ?>
<section class="valuation-form card card-outline card-primary">
        <header class="card-header">
            <h2 class="card-title">Today block A</h2>
        </header>
        <div class="card-body" style="background: #E5E4E2">
            <div class="row px-4">

                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="py-2 shadow rounded ml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-7 position-absolute" style="right:20px;" >
                                <h3  class="card-category text-right" style="color:#757575">Today Received</h3 >
                                <h4 class=" float-right"><?= $valuationReport_today_receieved ?></h4></div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[instruction_date]'=> date('Y-m-d')])?>"> <span style="font-size:20px;">Go to Received</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="py-2 shadow rounded ml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-7 position-absolute" style="right:20px;" >
                                <h3  class="card-category text-right" style="color:#757575">Today Challenged</h3 >
                                <h4 class=" float-right"><?= $valuationReport_challneged_today ?></h4></div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[revised_reason]'=> [1,2],'ValuationSearch[instruction_date]'=> date('Y-m-d')])?>"> <span style="font-size:20px;">Go to Challenged</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="py-2 shadow rounded ml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-7 position-absolute" style="right:20px;" >
                                <h3  class="card-category text-right" style="color:#757575">Inspections Planned Today</h3 >
                                <h4 class=" float-right"><?= $valuationReport_today_inspected ?></h4></div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[inspection_date]'=> date('Y-m-d')])?>"> <span style="font-size:20px;">Go to Inspected</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="py-2 shadow rounded ml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-7 position-absolute" style="right:20px;" >
                                <h3  class="card-category text-right" style="color:#757575">Today Inspected</h3 >
                                <h4 class=" float-right"><?= $valuationReport_today_inspected_done ?></h4></div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[inspection_done_date]'=> date('Y-m-d')])?>"> <span style="font-size:20px;">Go to Inspected</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="py-2 shadow rounded ml-2 bg-info" style="width:90px; height:80px;">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                            </div>
                            <div class=" col-7 position-absolute" style="right:20px;" >
                                <h3  class="card-category text-right" style="color:#757575">Today Approved</h3 >
                                <h4 class=" float-right"><?= $valuationReport_today_approved ?></h4></div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                                <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['valuation/approved-today-list'])?>"> <span style="font-size:20px;">Go to Approved</span> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                    <div id="w0" class="card-stats">
                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                            <div class="py-2 shadow rounded ml-2" style="width:90px; height:80px; background-color:#C62828">
                                <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">money</i></div>
                            </div>
                            <div class=" col-7 position-absolute" style="right:20px;" >
                                <h3  class="card-category text-right" style="color:#757575">Today Cancelled </h3 >
                                <h4 class=" float-right"><?= $valuationReport_cancel_today  ?></h4></div>
                        </div>
                        <div class="card-footer bg-white">
                            <div class="stats position-relative" style="box-sizing: border-box;">
                                <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#C62828;" >money</i>
                                <a class="position-absolute" style="left:53px;top:5px; " href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[valuation_status]'=> 9,'ValuationSearch[instruction_date]'=> date('Y-m-d')])?>"> <span style="font-size:20px; color:#C62828;">Go to Cancelled</span> </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section>

<section class="valuation-form card card-outline card-primary">
    <header class="card-header">
        <h2 class="card-title">Active block B</h2>
    </header>
    <div class="card-body" style="background: #E5E4E2">
        <div class="row px-4">
            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                        <div class="py-2 shadow rounded ml-2 bg-info" style="width:90px; height:80px;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;" >
                            <h3  class="card-category text-right" style="color:#757575">Active Valuations</h3 >
                            <h4 class=" float-right"><?= $valuationReport_active ?></h4></div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                            <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['valuation/index'])?>"> <span style="font-size:20px;">Go to Active</span> </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                        <div class="py-2 shadow rounded ml-2 bg-info" style="width:90px; height:80px;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">description</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;" >
                            <h3  class="card-category text-right" style="color:#757575">Active Valuation Received</h3 >
                            <h4 class=" float-right"><?= $valuationReport[1] ?></h4></div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons text-info pt-1 pl-3" style="font-size:30px" >description</i>
                            <a class="position-absolute text-info" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[valuation_status]'=> 1])?>"> <span style="font-size:20px;">Go to Received</span> </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 " style="width:90px; height:80px; background-color:#C0CA33;">
                            <div class="card-icon pt-2 pl-2">
                                <i class="material-icons text-white pl-1" style="font-size:60px">business</i>
                            </div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;" >
                            <h3  class="card-category text-right" style="color:#757575">Active Inspection Requested</h3 >
                            <h4 class=" float-right"><?= $valuationReport[2] ?></h4></div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#C0CA33;" >business</i>
                            <a class="position-absolute" style="left:53px;top:5px;color:#C0CA33;" href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[valuation_status]'=> 2])?>"> <span style="font-size:20px;">Go to Inspection Requested</span> </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 " style="width:90px; height:80px; background-color:#C0CA33;">
                            <div class="card-icon pt-2 pl-2">
                                <i class="material-icons text-white pl-1" style="font-size:60px">business</i>
                            </div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;" >
                            <h3  class="card-category text-right" style="color:#757575">Active Valuation Challenged</h3 >
                            <h4 class=" float-right"><?= $valuationReport_active_challneged ?></h4></div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#C0CA33;" >business</i>
                            <a class="position-absolute" style="left:53px;top:5px;color:#C0CA33;" href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[valuation_status]'=> [1,2,3,4,7,8],'ValuationSearch[revised_reason]'=> [1,2],'ValuationSearch[instruction_date]'=> date('Y-m-d')])?>"> <span style="font-size:20px;">Go to Challenged</span> </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 bg-success" style="width:90px; height:80px;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">assignment</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;" >
                            <h3  class="card-category text-right" style="color:#757575">Active Documents Requested</h3 >
                            <h4 class=" float-right"><?=  $valuationReport[7] ?></h4></div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons text-success pt-1 pl-3" style="font-size:30px" >assignment</i>
                            <a class="position-absolute text-success" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[valuation_status]'=> 7]); ?>"> <span style="font-size:20px;">Go to Documents Requested</span> </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 " style="width:90px; height:80px; background-color:#FFA000;">
                            <div class="card-icon pt-2 pl-2">
                                <i class="material-icons text-white pl-1" style="font-size:60px">business</i>
                            </div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;" >
                            <h3  class="card-category text-right" style="color:#757575">Active Rejected</h3 >
                            <h4 class=" float-right"><?= $valuationReport[6] ?></h4></div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#FFA000;" >business</i>
                            <a class="position-absolute" style="left:53px;top:5px;color:#FFA000;" href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[valuation_status]'=> 6])?>"> <span style="font-size:20px;">Go to Rejected</span> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="valuation-form card card-outline card-primary">
    <header class="card-header">
        <h2 class="card-title">Total block C</h2>
    </header>
    <div class="card-body" style="background: #E5E4E2">
        <div class="row px-4">
            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2"  style="width:90px; height:80px; background-color:#6A1B9A;">
                            <div class="card-icon pt-2 pl-2">
                                <i class="material-icons text-white pl-1" style="font-size:60px">pending_actions</i>
                            </div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;" >
                            <h3  class="card-category text-right" style="color:#757575">Total Valuations</h3 >
                            <h4 class=" float-right"><?= $totalValuation ?></h4></div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#6A1B9A;" >pending_actions</i>
                            <a class="position-absolute" style="left:53px;top:5px; color:#6A1B9A;" href="<?= yii\helpers\Url::toRoute(['valuation/report-all']); ?>"> <span style="font-size:20px;">Go to Valuations</span> </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2"  style="width:90px; height:80px; background-color:#6A1B9A;">
                            <div class="card-icon pt-2 pl-2">
                                <i class="material-icons text-white pl-1" style="font-size:60px">pending_actions</i>
                            </div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;" >
                            <h3  class="card-category text-right" style="color:#757575">Total Valuations Challenged</h3 >
                            <h4 class=" float-right"><?= $valuationReport_challneged ?></h4></div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#6A1B9A;" >pending_actions</i>
                            <a class="position-absolute" style="left:53px;top:5px; color:#6A1B9A;" href="<?= yii\helpers\Url::toRoute(['valuation/report-all','ValuationSearch[revised_reason]'=> [1,2]]); ?>"> <span style="font-size:20px;">Go to Challenged</span> </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 bg-success" style="width:90px; height:80px;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">check_circle</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;" >
                            <h3  class="card-category text-right" style="color:#757575">Total Approved </h3 >
                            <h4 class=" float-right"><?= $valuationReport[5] ?></h4></div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons text-success pt-1 pl-3" style="font-size:30px" >check_circle</i>
                            <a class="position-absolute text-success" style="left:53px;top:5px;" href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[valuation_status]'=> 5])?>"> <span style="font-size:20px;">Go to Approved </span> </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 " style="width:90px; height:80px; background-color:#FFA000;">
                            <div class="card-icon pt-2 pl-2">
                                <i class="material-icons text-white pl-1" style="font-size:60px">business</i>
                            </div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;" >
                            <h3  class="card-category text-right" style="color:#757575">Total Rejected</h3 >
                            <h4 class=" float-right"><?= $valuationReport[6] ?></h4></div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#FFA000;" >business</i>
                            <a class="position-absolute" style="left:53px;top:5px;color:#FFA000;" href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[valuation_status]'=> 6])?>"> <span style="font-size:20px;">Go to Rejected</span> </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                        <div class="py-2 shadow rounded ml-2" style="width:90px; height:80px; background-color:#795548">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">money</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;" >
                            <h3  class="card-category text-right" style="color:#757575">Total Clients </h3 >
                            <h4 class=" float-right"><?= $clients ?></h4></div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#795548;" >money</i>
                            <a class="position-absolute" style="left:53px;top:5px; " href="<?= yii\helpers\Url::to(['client/index'])?>"> <span style="font-size:20px; color:#795548;">Go to Clients </span> </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white" >
                        <div class="py-2 shadow rounded ml-2" style="width:90px; height:80px; background-color:#C62828">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1" style="font-size:60px">money</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:20px;" >
                            <h3  class="card-category text-right" style="color:#757575">Total Cancelled </h3 >
                            <h4 class=" float-right"><?= $valuationReport[9]  ?></h4></div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#C62828;" >money</i>
                            <a class="position-absolute" style="left:53px;top:5px; " href="<?= yii\helpers\Url::to(['valuation/report-all','ValuationSearch[valuation_status]'=> 9])?>"> <span style="font-size:20px; color:#C62828;">Go to Cancelled</span> </a>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>
<?php } ?>