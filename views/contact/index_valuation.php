<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', $grid_title);
$cardTitle = Yii::t('app', 'List');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns='';
$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
  $createBtn=true;
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
  $actionBtns.='{view}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
  $actionBtns.='{update}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){
  $actionBtns.='{status}';
}
?>

<div class="user-index">
    <?php if($create_url <> null){?>
    <div class="card-tools" style="padding-bottom: 10px;">
        <a href="<?= Url::to([$create_url])?>" target="_blank" class="btn btn-success">
            Create
        </a>
    </div>
    <?php } ?>
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <?= CustomGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'cardTitle' => $cardTitle,
   /* 'createBtn' => $createBtn,*/
    'columns' => [
      ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']],




      ['attribute'=>'contact_person_name','label'=>Yii::t('app', 'Name')],

        [
            'attribute' => 'contact_phone_no',
            'label' => Yii::t('app', 'Mobile Number'),
            'value' => function ($model) {
                return (isset($model->contact_phone_no) && ($model->contact_phone_no <> null))? ($model->phone_code.' '.$model->contact_phone_no): '';
            },
        ],
        [
            'attribute' => 'land_line_no',
            'label' => Yii::t('app', 'Land Line Number'),
            'value' => function ($model) {
                return (isset($model->land_line_no) && ($model->land_line_no <> null))? ($model->	land_line_code.' '.$model->land_line_no): '';
            },
        ],
      ['attribute'=>'contact_email','label'=>Yii::t('app', 'Email')],

    ],
  ]);?>
  <?php CustomPjax::end(); ?>
</div>
