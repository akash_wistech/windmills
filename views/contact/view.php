<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\ActivityLogWidget;
use app\assets\ContactDetailAsset;
ContactDetailAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Contact */

$this->title = Yii::t('app', 'Contacts');
$cardTitle = Yii::t('app','View Contact:  {nameAttribute}', [
    'nameAttribute' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');

$this->registerJs('
$("#user-d-lead_score_star").rate({
  readonly: true,
    '.($model->profileInfo!=null && $model->profileInfo->lead_score>0 ? 'initial_value: '.$model->profileInfo->lead_score : '').'
});
$("#user-d-confidence_star").rate({
  readonly: true,
    '.($model->profileInfo!=null && $model->profileInfo->confidence>0 ? 'initial_value: '.$model->profileInfo->confidence : '').'
});
$(".numeral-span").each(function(index){
  //$(this).html(numeral($(this).html()).format("0,0.00"));
});
');
?>
<div class="contact-view">
  <section class="card card-outline card-primary">
    <header class="card-header">
      <h2 class="card-title"><?= $model->name?></h2>
      <?php if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){?>
      <div class="card-tools">
        <a href="<?= Url::to(['update','id'=>$model['id']])?>" class="btn btn-tool">
          <i class="fas fa-edit"></i>
        </a>
      </div>
      <?php }?>
    </header>
    <div class="card-body multi-cards">
      <div class="row">
        <div class="col-sm-12">
          <section class="card card-outline card-info mb-3">
            <header class="card-header">
              <h2 class="card-title"><?= Yii::t('app','Contact Info')?></h2>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </header>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Created:').'</strong> '.Yii::$app->formatter->asDate($model->created_at);?>
                </div>
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Full Name:').'</strong> '.$model->name;?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Company:').'</strong> '.($model->company!=null ? $model->company->title : Yii::t('app','N/A'));?>
                </div>
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Job Title:').'</strong> '.($model->profileInfo!=null && $model->profileInfo->jobTitle!=null ? $model->profileInfo->jobTitle->title : Yii::t('app','N/A'));?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Mobile:').'</strong> '.$model->profileInfo->mobile;?>
                </div>
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Phone 1:').'</strong> '.$model->profileInfo->phone1;?>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Phone 2:').'</strong>'.$model->profileInfo->phone2;?>
                </div>
                <div class="col-sm-6">
                  <?= '<strong>'.Yii::t('app','Email:').'</strong>'.$model->email;?>
                </div>
              </div>
            </div>
          </section>

        </div>

      </div>
    </div>
  </section>
</div>
