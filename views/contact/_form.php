<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\assets\ContactFormAsset;
ContactFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

if($model->profileInfo!=null){
  $model->job_title=$model->profileInfo->jobTitle->title;
  $model->job_title_id=$model->profileInfo->job_title_id;
  $model->mobile=$model->profileInfo->mobile;
  $model->lead_score=$model->profileInfo->lead_score;
  $model->confidence=$model->profileInfo->confidence;
}
if($model->company!=null){
  $model->company_name=$model->company->title;
}

$this->registerJs('
$("#user-job_title").autocomplete({
  serviceUrl: "'.Url::to(['suggestion/job-titles']).'",
  noCache: true,
  onSelect: function(suggestion) {
    if($("#user-job_title_id").val()!=suggestion.data){
      $("#user-job_title_id").val(suggestion.data);
    }
  },
  onInvalidateSelection: function() {
    $("#user-job_title_id").val("0");
  }
});
$("#selected_company_title").autocomplete({
  serviceUrl: "'.Url::to(['suggestion/company']).'",
  noCache: true,
  onSelect: function(suggestion) {
    if($("#selected_company_id").val()!=suggestion.data){
      $("#selected_company_id").val(suggestion.data);
    }
  },
  onInvalidateSelection: function() {
    $("#selected_company_id").val("0");
  }
});
$("#user-d-lead_date,#user-d-lead_expected_close_date,#user-d-lead_close_date").datetimepicker({
    allowInputToggle: true,
    viewMode: "months",
    format: "YYYY-MM-DD"
});
$("body").on("change", "#user-country_id", function () {
  $.get("'.Url::to(['suggestion/zone-options','country_id'=>'']).'"+$(this).val(),function(data){
    $("select#user-zone_id").html(data);
  });
});
$("#user-d-lead_score_star").rate({
    max_value: 5,
    step_size: 1,
    '.($model->profileInfo!=null && $model->profileInfo->lead_score>0 ? 'initial_value: '.$model->profileInfo->lead_score : '').'
});
$("#user-d-lead_score_star").on("afterChange", function(ev, data){
  $("#user-lead_score").val(data.to);
});
$("#user-d-confidence_star").rate({
    max_value: 5,
    step_size: 1,
    '.($model->profileInfo!=null && $model->profileInfo->confidence>0 ? 'initial_value: '.$model->profileInfo->confidence : '').'
});
$("#user-d-confidence_star").on("afterChange", function(ev, data){
  $("#user-confidence").val(data.to);
});
$("#user-manager_id").select2({
	allowClear: true,
	width: "100%",
});


$("body").on("click",".btn-random", function(){


$.ajax({

  url: "'.Url::to(['contact/generate-random-string','id'=>$model->id]).'",
  dataType: "html",
  type: "POST",

  });
});

$("body").on("click" ,".btn-random", function(){
         AppBlockSave();
      });


');
$companyAddLink='';
if(Yii::$app->menuHelperFunction->checkActionAllowed('create','client')){
  $companyAddLink =' <a href="javascript:;" class="btn btn-xs btn-success load-modal" data-url="'.Url::to(['client/create']).'" data-heading="'.Yii::t('app','New Client').'">';
  $companyAddLink.='  <i class="fa fa-plus"></i>';
  $companyAddLink.='</a>';
}
?>
<section class="contact-form card card-outline card-primary Main-class-block">
  <?php $form = ActiveForm::begin(); ?>
  <div class="hidden">
    <?= $form->field($model, 'job_title_id')->textInput()?>
    <?= $form->field($model, 'company_id')->textInput(['id'=>'selected_company_id'])?>
    <?= $form->field($model, 'lead_score')->textInput()?>
    <?= $form->field($model, 'confidence')->textInput()?>
  </div>
  <header class="card-header">
    <h2 class="card-title"><?= $cardTitle?></h2>
  </header>
  <div class="card-body">
    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'firstname')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'lastname')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'job_title')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'company_name')->textInput(['id'=>'selected_company_title', 'maxlength' => true])->label($model->getAttributeLabel('company_name').$companyAddLink)?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'mobile')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'phone1')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'phone2')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'email')->textInput(['maxlength' => true])?>
        <?= $form->field($model, 'password')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <?= $form->field($model, 'background_info')->textArea(['rows' => 4])?>
    <?= $form->field($model, 'manager_id')->dropDownList(Yii::$app->appHelperFunctions->staffMemberListArr,['multiple'=>'multiple'])?>
    <section class="card card-outline card-success">
      <header class="card-header">
        <h2 class="card-title"><?= Yii::t('app','Sales & Marketing')?></h2>
      </header>
      <div class="card-body">
        <div class="row">
          <div class="col-sm-4">
            <?= $form->field($model, 'lead_type')->dropDownList(Yii::$app->appHelperFunctions->leadTypeListArr,['prompt'=>Yii::t('app','Select')])?>
          </div>
          <div class="col-sm-4">
            <?= $form->field($model, 'lead_source')->dropDownList(Yii::$app->appHelperFunctions->leadSourceListArr,['prompt'=>Yii::t('app','Select')])?>
          </div>
          <div class="col-sm-4">
            <?= $form->field($model, 'lead_date',['template'=>'
            {label}
            <div class="input-group date" id="user-d-lead_date" data-target-input="nearest">
              {input}
              <div class="input-group-append" data-target="#user-d-lead_date" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
              </div>
            </div>
            {hint}{error}
            '])->textInput(['maxlength' => true])?>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label"><?= $model->getAttributeLabel('lead_score')?></label>
              <div>
                <div id="user-d-lead_score_star" class="rating"></div>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <?= $form->field($model, 'expected_close_date',['template'=>'
            {label}
            <div class="input-group date" id="user-d-lead_expected_close_date" data-target-input="nearest">
              {input}
              <div class="input-group-append" data-target="#user-d-lead_expected_close_date" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
              </div>
            </div>
            {hint}{error}
            '])->textInput(['maxlength' => true])?>
          </div>
          <div class="col-sm-4">
            <?= $form->field($model, 'close_date',['template'=>'
            {label}
            <div class="input-group date" id="user-d-lead_close_date" data-target-input="nearest">
              {input}
              <div class="input-group-append" data-target="#user-d-lead_close_date" data-toggle="datetimepicker">
                  <div class="input-group-text"><i class="fa fa-calendar"></i></div>
              </div>
            </div>
            {hint}{error}
            '])->textInput(['maxlength' => true])?>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <div class="form-group">
              <label class="control-label"><?= $model->getAttributeLabel('confidence')?></label>
              <div>
                <div id="user-d-confidence_star" class="rating"></div>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <?= $form->field($model, 'deal_status')->dropDownList(Yii::$app->appHelperFunctions->dealStatusListArr,['prompt'=>Yii::t('app','Select')])?>
          </div>
          <div class="col-sm-4">

            <?php if ($model->id <> null) { ?>
              <?= $form->field($model, 'password')->textInput(['maxlength' => true])?>
            <!-- <button type="button" name="button" class="btn btn-success float-right mt-2 mb-4 btn-random">Send Password</button> -->
          <?php } ?>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>
<?= $this->render('/client/js/create_company_script')?>

<script>
// random-string

function AppBlockSave(){
    App.blockUI({
        iconOnly: true,
        target: ".Main-class-block",
        overlayColor: "none",
        cenrerY: true,
        boxed: true
    });

};


</script>
