<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Derive MV Income Approach');
$cardTitle = Yii::t('app', 'Derive MV Income Approach:  {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_21/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

if ($model->email_status !=1) {
    $AlertText='Data and Valuation Status will be saved and Email will be sent once?';
}
else{
    $AlertText='Only Data will be saved?';
}

$this->registerJs('

    $("body").on("click", ".sav-btn1", function (e) {
_this=$(this);
e.preventDefault();
swal({
title: "'.Yii::t('app',$AlertText).'",
html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
      console.log("Hello 123")
      $("#w0").unbind("submit").submit();
    }
});
});



$("body").on("click", ".sav-btn", function (e) {

swal({
title: "'.Yii::t('app','Valuation status will be saved and Email will be sent to Clients ?').'",
html: "'.Yii::t('app','').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
  $.ajax({

  url: "'.Url::to(['valuation/send-email','id'=>$model->id,'step'=>21]).'",
  dataType: "html",
  type: "POST",
  });
    }
});
});


$("body").on("click",".load-model",function(){
modelurl=$(this).data("url")
$.ajax({
  url:modelurl,
  dataType: "html",
  type: "POST",
  success: function(html) {
  $(".modal-body").html(html);
  },
  error: function(xhr,ajaxoptions,thownError){

    alert(xhr.responseText);
  }

});

});

var preventClick = false;
$("#ThisLink").click(function(e) {
    $(this)
       .css("cursor", "default")
       .css("text-decoration", "none")
    if (!preventClick) {
        $(this).html($(this).html() );
    }
    preventClick = true;
    return false;
});



');
?>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $model->id  ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-4 col-sm-2">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                     aria-orientation="vertical">
                    <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => 21]); ?>
                </div>
            </div>
            <div class="col-8 col-sm-10">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">
                        <section class="valuation-form card card-outline card-primary">

                            <header class="card-header">
                                <h2 class="card-title">Valuation Summary</h2>
                            </header>
                            <div class="card-body">
                                <div class="row" style="padding: 10px">

                                    <table id="owner_container"
                                           class="table table-striped table-bordered table-hover text-center images-table ">
                                        <thead>

                                        </thead>
                                        <tbody>


                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Valuation Approach
                                            </td>
                                            <td class="text-center" colspan="3" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $summary['valuation_approach'] ?>
                                            </td>

                                        </tr>

                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Basis of Value
                                            </td>
                                            <td class="text-center" colspan="3" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $summary['basis_of_value']; ?>
                                            </td>
                                        </tr>

                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Approach Reasoning
                                            </td>
                                            <td class="text-center" colspan="3" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= $summary['approach_reasoning'] ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <strong>
                                                    Market Value
                                                </strong>
                                            </td>
                                            <td>
                                                <strong>
                                                    MV/BUA
                                                </strong>
                                            </td>
                                            <td>
                                                <strong>
                                                    MV/land
                                                </strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">&nbsp;</td>
                                        </tr>

                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Original Purchase Price
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['original_purchase_price']['mv'] > 0)? number_format($summary['original_purchase_price']['mv']) : '-'; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['original_purchase_price']['mv_per_bua'] > 0) ? number_format($summary['original_purchase_price']['mv_per_bua']): '-'; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['original_purchase_price']['mv_per_land_size'] > 0)? number_format($summary['original_purchase_price']['mv_per_land_size']): '-'; ?>
                                            </td>
                                        </tr>

                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Transaction Price
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['transaction_price']['mv'] > 0)? number_format($summary['transaction_price']['mv']) : '-'; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['transaction_price']['mv_per_bua'] > 0)? number_format($summary['transaction_price']['mv_per_bua']): '-'; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['transaction_price']['mv_per_land_size'] > 0 )? number_format($summary['transaction_price']['mv_per_land_size']): '-'; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">&nbsp;</td>
                                        </tr>

                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Elevation Analysis Price
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['elevation_analysis_price']['mv'] > 0) ? number_format($summary['elevation_analysis_price']['mv']) : '-'; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['elevation_analysis_price']['mv_per_bua'] > 0)? number_format($summary['elevation_analysis_price']['mv_per_bua']): '-'; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['elevation_analysis_price']['mv_per_land_size'] > 0)? number_format($summary['elevation_analysis_price']['mv_per_land_size']) : '-'; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">&nbsp;</td>
                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Estimated Market value <br>as per Sold Transactions
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['mv_sold']['mv'] > 0)? number_format($summary['mv_sold']['mv']): '-'; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['mv_sold']['mv_per_bua'] > 0)? number_format($summary['mv_sold']['mv_per_bua']): '-' ; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['mv_sold']['mv_per_land_size'] > 0)? number_format($summary['mv_sold']['mv_per_land_size']):'-'; ?>
                                            </td>
                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Estimated Market value as <br>per Previous Valuations
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['mv_prvious']['mv'] > 0)? number_format($summary['mv_prvious']['mv']) : '-' ; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['mv_prvious']['mv_per_bua'] > 0)? number_format($summary['mv_prvious']['mv_per_bua']): '-' ; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['mv_prvious']['mv_per_land_size'] > 0)? number_format($summary['mv_prvious']['mv_per_land_size']): '-' ; ?>
                                            </td>
                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Estimated Market value as<br> per Listing Valuations
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['mv_list']['mv'] > 0)? number_format($summary['mv_list']['mv']): '-' ; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['mv_list']['mv_per_bua'] > 0)? number_format($summary['mv_list']['mv_per_bua']) : '-' ; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['mv_list']['mv_per_land_size'] > 0)? number_format($summary['mv_list']['mv_per_land_size']): '-' ; ?>
                                            </td>
                                        </tr>
                                        <?php if($valuation->property->title != 'Apartment') { ?>
                                            <tr id="owner_data_row">
                                                <td style="padding-top: 30px!important;">
                                                    Estimated Market value<br> as per Cost Valuations
                                                </td>
                                                <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                    <?= ($summary['mv_cost']['mv'] > 0)? number_format($summary['mv_cost']['mv']): '-'; ?>
                                                </td>
                                                <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                    <?= ($summary['mv_cost']['mv_per_bua'] > 0)? number_format($summary['mv_cost']['mv_per_bua']): '-'; ?>
                                                </td>

                                                <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                    <?= ($summary['mv_cost']['mv_per_land_size'] > 0)? number_format($summary['mv_cost']['mv_per_land_size']): '-'; ?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Estimated Market Value<br> as per Income Approach
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['mv_income']['mv'] > 0)? number_format($summary['mv_income']['mv']): '-'; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['mv_income']['mv_per_bua'] > 0)? number_format($summary['mv_income']['mv_per_bua']): '-'; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['mv_income']['mv_per_land_size'] > 0)? number_format($summary['mv_income']['mv_per_land_size']): '-'; ?>
                                            </td>
                                        </tr>
                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                <strong>Average</strong>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['avaerage']['mv'] > 0)? number_format($summary['avaerage']['mv']): '-'; ?>
                                            </td>
                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['avaerage']['mv_per_bua'] > 0)? number_format($summary['avaerage']['mv_per_bua']): '-' ; ?>
                                            </td>

                                            <td class="text-center" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['avaerage']['mv_per_land_size'] > 0)? number_format($summary['avaerage']['mv_per_land_size']): '-' ; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">&nbsp;</td>
                                        </tr>

                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Market Value of Land
                                            </td>
                                            <td class="text-center" colspan="3" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['market_value_of_land'] > 0)? number_format($summary['market_value_of_land']): '-'; ?>
                                            </td>
                                        </tr>

                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Estimated Market Rent
                                            </td>
                                            <td class="text-center" colspan="3" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['estimated_market_rent'] > 0)? number_format($summary['estimated_market_rent']): '-'; ?>
                                            </td>
                                        </tr>

                                        <tr id="owner_data_row">
                                            <td style="padding-top: 30px!important;">
                                                Gross Yield
                                            </td>
                                            <td class="text-center" colspan="3" style="padding-top: 25px!important;padding-left: 10px!important;padding-right: 10px!important;">
                                                <?= ($summary['gross_yield'] > 0)? number_format($summary['gross_yield']): '-'; ?>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="4">&nbsp;</td>
                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </section>

                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title">Final Review</h2>
                            </header>
                            <div class="card-body">
                                <div class="row">


                                    <div class="col-sm-6">
                                        <?= $form->field($model, 'estimated_market_value')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $form->field($model, 'estimated_market_value_sqf')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $form->field($model, 'estimated_market_rent')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $form->field($model, 'estimated_market_rent_sqf')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $form->field($model, 'parking_market_value')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $form->field($model, 'parking_market_value_sqf')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                                    </div>

                                    <div class="col-sm-12 text-right pb10">
                                        <button type="button" id="bua_calculation" class="btn btn-info">BUA Calculate</button>
                                        <input type="hidden" id="bua" value="<?= $bua ?>">
                                    </div>


                                    <div class="col-sm-6">
                                        <?php
                                        echo $form->field($model, 'status')->widget(Select2::classname(), [
                                            'data' => array('Approve' => 'Recommended For Approval', 'Reject' => 'Reject'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-6"></div>

                                    <div class="col-sm-12" id="reason_text" style="<?= ($model->status == 'Reject')? "": 'display:none;' ?>" >

                                        <?= $form->field($model, 'reason')->textarea(['rows' => '6']) ?>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group ">
                                            <label class="control-label">Valuer</label>
                                            <input type="text" id="valuationapproversdata-parking_market_value_sqf"
                                                   class="form-control"
                                                   value="<?= (isset($model->created_by) && ($model->created_by <> null))? ($model->user->firstname): Yii::$app->user->identity->firstname  ?>"
                                                   readonly="">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group ">
                                            <label class="control-label">Date</label>
                                            <input type="text" id="valuationapproversdata-parking_market_value_sqf"
                                                   class="form-control"
                                                   value="<?= (isset($model->created_at) && ($model->created_at <> null))? (date('Y-m-d',strtotime($model->created_at))) : date('Y-m-d')  ?>"
                                                   readonly="">
                                        </div>
                                    </div>

                                    <?php if($valuation->parent_id <> null && $valuation->parent_id > 0){ ?>
                                        <div class="col-sm-12"  >
                                            <?= $form->field($model, 'revise_reason')->textarea(['rows' => '6']) ?>
                                        </div>
                                    <?php } ?>

                                </div>


                            </div>

                        </section>






                                <?php $model->step=21;  ?>
                                <?php echo $form->field($model, 'step')->textInput(['maxlength' => true,
                                    'type'=>'hidden'])->label('') ?>


                               <!-- <?php /*if($valuation->parent_id <> null && $valuation->parent_id > 0){ */?>

                                    <div class="col-sm-12"  >

                                        <?/*= $form->field($model, 'revise_reason')->textarea(['rows' => '6']) */?>
                                    </div>
                                --><?php /*} */?>
                                <input type="hidden" name="ValuationApproversData[created_by]" value="<?= (isset($model->created_by) && ($model->created_by <> null))? $model->created_by : Yii::$app->user->identity->id  ?>">
                                <!-- <div class="col-sm-6">
                                        <div class="form-group ">

                                            <label class="control-label">Valuer</label>
                                            <input type="text" id="valuationapproversdata-parking_market_value_sqf"
                                                   class="form-control"
                                                   value="<?/*= (isset($model->created_by) && ($model->created_by <> null))? ($model->user->firstname.''.$model->user->lastname): (Yii::$app->user->identity->firstname.' '.Yii::$app->user->identity->lastname)  */?>"
                                                   readonly="">
                                        </div>
                                    </div>
-->



                    </div>


                    <!-- Modal -->
                    <div class="modal" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header ">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">


                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                                    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1 mx-2']) ?>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <!-- Button trigger modal -->


                        <button type="button" class="btn btn-success load-model mr-2" data-url="<?= Url::toRoute(['valuation/pdf-html', 'id' =>$valuation->id]);  ?>" data-toggle="modal" data-target="#exampleModalLong">
                            Save
                        </button>
                        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.card -->
</div>

<script type="text/javascript">






</script>
