<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\assets\ListingsFormAsset;

ListingsFormAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Valuation */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Final Approval');
$cardTitle = Yii::t('app', 'Final Approval:  {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['valuation/step_23/' . $valuation->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
if ($model->email_status !=1) {
    $AlertText='Data and Valuation Status will be saved and Email will be sent once?';
}
else{
    $AlertText='Only Data will be saved?';
}

$this->registerJs('

$("body").on("click", ".sav-btn1", function (e) {
_this=$(this);
e.preventDefault();
swal({
title: "'.Yii::t('app',$AlertText).'",
html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
      console.log("Hello 123")
      $("#w0").unbind("submit").submit();
    }
});
});


$("body").on("click", ".sav-btn", function (e) {

swal({
title: "'.Yii::t('app','Valuation status will be saved and Email will be sent to Clients ?').'",
html: "'.Yii::t('app','').'",
type: "warning",
showCancelButton: true,
    confirmButtonColor: "#47a447",
confirmButtonText: "'.Yii::t('app','Yes, Do you want to save it!').'",
cancelButtonText: "'.Yii::t('app','Cancel').'",
},function(result) {
    if (result) {
  $.ajax({

  url: "'.Url::to(['send-email','id'=>$model->id,'step'=>23]).'",
  dataType: "html",
  type: "POST",
  });
    }
});
});



var preventClick = false;
$("#ThisLink").click(function(e) {
    $(this)
       .css("cursor", "default")
       .css("text-decoration", "none")
    if (!preventClick) {
        $(this).html($(this).html() );
    }
    preventClick = true;
    return false;
});

$("body").on("click",".load-model",function(){
modelurl=$(this).data("url")
$.ajax({
  url:modelurl,
  dataType: "html",
  type: "POST",
  success: function(html) {
  $(".modal-body").html(html);
  },
  error: function(xhr,ajaxoptions,thownError){
    alert(xhr.responseText);
  }
});
});

');
?>

<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">
            <i class="fas fa-edit"></i>
            <?= $model->id ?>
        </h3>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist"
                     aria-orientation="vertical">
                    <?php echo $this->render('../left-nav', ['model' => $valuation, 'step' => 23]); ?>
                </div>
            </div>
            <div class="col-7 col-sm-9">
                <div class="tab-content" id="vert-tabs-tabContent">
                    <div class="tab-pane active show text-left fade" id="vert-tabs-home" role="tabpanel"
                         aria-labelledby="vert-tabs-home-tab">


                        <section class="valuation-form card card-outline card-primary">

                            <?php $form = ActiveForm::begin(); ?>
                            <header class="card-header">
                                <h2 class="card-title">Final Approval</h2>
                            </header>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?= $form->field($model, 'estimated_market_value')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $form->field($model, 'estimated_market_value_sqf')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $form->field($model, 'estimated_market_rent')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $form->field($model, 'estimated_market_rent_sqf')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $form->field($model, 'parking_market_value')->textInput(['maxlength' => true]) ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <?= $form->field($model, 'parking_market_value_sqf')->textInput(['maxlength' => true, 'readonly' => true]) ?>
                                    </div>
                                    <div class="col-sm-12 text-right pb10">
                                        <button type="button" id="bua_calculation" class="btn btn-info">BUA Calculate</button>
                                        <input type="hidden" id="bua" value="<?= $bua ?>">
                                    </div>


                                    <div class="col-sm-6">
                                        <?php
                                        echo $form->field($model, 'status')->widget(Select2::classname(), [
                                            'data' => array('Approve' => 'Approve', 'Reject' => 'Reject'),
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],
                                        ]);
                                        ?>
                                    </div>
                                    <div class="col-sm-6"></div>

                                        <div class="col-sm-12" id="reason_text"  style="<?= ($model->status == 'Reject')? "": 'display:none;' ?>">

                                            <?= $form->field($model, 'reason')->textarea(['rows' => '6']) ?>
                                        </div>


                                    <div class="col-sm-6">
                                        <div class="form-group ">
                                            <label class="control-label">Approver</label>
                                            <input type="text" id="valuationapproversdata-parking_market_value_sqf"
                                                   class="form-control"
                                                    value="<?= (isset($model->created_by) && ($model->created_by <> null))? ($model->user->firstname.''.$model->user->lastname): (Yii::$app->user->identity->firstname.' '.Yii::$app->user->identity->lastname)  ?>"
                                                   readonly="">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group ">
                                            <label class="control-label">Date</label>
                                            <input type="text" id="valuationapproversdata-parking_market_value_sqf"
                                                   class="form-control"
                                                   value="<?= (isset($model->created_at) && ($model->created_at <> null))? (date('Y-m-d',strtotime($model->created_at))) : date('Y-m-d')  ?>"
                                                   readonly="">
                                        </div>
                                    </div>

                                    <?php $model->step=23;  ?>
                                    <?php echo $form->field($model, 'step')->textInput(['maxlength' => true,
                                        'type'=>'hidden'])->label('') ?>

                                </div>


                            </div>
                            <!-- Modal -->
                            <div class="modal" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header ">
                                            <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">


                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success sav-btn1']) ?>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer">

                                <button type="button" class="btn btn-success load-model mr-2" data-url="<?= Url::toRoute(['valuation/pdf-html', 'id' =>$valuation->id]);  ?>" data-toggle="modal" data-target="#exampleModalLong">
                                    Save
                                </button>
                                <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.card -->
</div>



<?=

$this->registerJs('

$(\'#valuationapproversdata-estimated_market_value\').keyup(function(event) {

    // skip for arrow keys
    if(event.which >= 37 && event.which <= 40) return;

    // format number
    $(this).val(function(index, value) {
        return value
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            ;
    });
});
$(\'#valuationapproversdata-estimated_market_rent\').keyup(function(event) {

    // skip for arrow keys
    if(event.which >= 37 && event.which <= 40) return;

    // format number
    $(this).val(function(index, value) {
        return value
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            ;
    });
});
$(\'#valuationapproversdata-parking_market_value\').keyup(function(event) {

    // skip for arrow keys
    if(event.which >= 37 && event.which <= 40) return;

    // format number
    $(this).val(function(index, value) {
        return value
            .replace(/\D/g, "")
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            ;
    });
});
');
?>