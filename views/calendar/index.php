<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\assets\FullCalendarAsset;
FullCalendarAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\CalendarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Calendar');
$cardTitle = Yii::t('app', 'Calendar');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns='';
$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
  $createBtn=true;
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
  $actionBtns.='{update}';
}
?>
<style>
  #script-warning {
    display: none;
    background: #eee;
    border-bottom: 1px solid #ddd;
    padding: 0 10px;
    line-height: 40px;
    text-align: center;
    font-weight: bold;
    font-size: 12px;
    color: red;
  }

  #loading {
    display: none;
    position: absolute;
    top: 10px;
    right: 10px;
  }

</style>
<div class="calender-index row">
  <div class="col-sm-9">
    <div id='script-warning'>
      <code><?= Yii::T('app','Sorry')?></code> <?= Yii::t('app','Seems like something broke')?>
    </div>
    <div id='loading'>loading...</div>
    <div id='calendar'></div>
  </div>
  <div class="col-sm-3">
    <?php CustomPjax::begin(['id'=>'grid-container']); ?>
    <?= CustomGridView::widget([
      'dataProvider' => $dataProvider,
      'filterModel' => $searchModel,
      'cardTitle' => $cardTitle,
      'createBtn' => $createBtn,
      'showSummary' => false,
      'columns' => [
        ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']],
        ['class' => 'yii\grid\CheckboxColumn','checkboxOptions'=>function ($model, $key, $index, $column){
					return ['value' => $model->id, 'class'=>'cb'];
				},'contentOptions'=>['style'=>'width: 20px;text-align:center;','class'=>'nosd']],
				['format'=>'raw','attribute'=>'title','label'=>Yii::t('app', 'Title'),'value'=>function($model){
          if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
            return Html::a($model['title'],['view','id'=>$model['id']],['data-pjax'=>'0']);
          }else{
            return $model['title'];
          }
        }],
        [
          'class' => 'yii\grid\ActionColumn',
          'header'=>'',
          'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
          'contentOptions'=>['class'=>'noprint actions'],
          'template' => '
            <div class="btn-group flex-wrap">
    					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                <span class="caret"></span>
              </button>
    					<div class="dropdown-menu" role="menu">
                '.$actionBtns.'
    					</div>
    				</div>',
          'buttons' => [
              'view' => function ($url, $model) {
                return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
                  'title' => Yii::t('app', 'View'),
                  'class'=>'dropdown-item text-1',
                  'data-pjax'=>"0",
                ]);
              },
              'update' => function ($url, $model) {
                return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                  'title' => Yii::t('app', 'Edit'),
                  'class'=>'dropdown-item text-1',
                  'data-pjax'=>"0",
                ]);
              },
              'status' => function ($url, $model) {
                if($model['status']==1){
                  return Html::a('<i class="fas fa-eye-slash"></i> '.Yii::t('app', 'Disable'), $url, [
                    'title' => Yii::t('app', 'Disable'),
                    'class'=>'dropdown-item text-1',
                    'data-confirm'=>Yii::t('app','Are you sure you want to disable this item?'),
                    'data-method'=>"post",
                  ]);
                }else{
                  return Html::a('<i class="fas fa-eye"></i> '.Yii::t('app', 'Enable'), $url, [
                    'title' => Yii::t('app', 'Enable'),
                    'class'=>'dropdown-item text-1',
                    'data-confirm'=>Yii::t('app','Are you sure you want to enable this item?'),
                    'data-method'=>"post",
                  ]);
                }
              },
              'delete' => function ($url, $model) {
                return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                  'title' => Yii::t('app', 'Delete'),
                  'class'=>'dropdown-item text-1',
                  'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                  'data-method'=>"post",
                  'data-pjax'=>"0",
                ]);
              },
          ],
        ],
      ],
    ]);?>
    <?php CustomPjax::end(); ?>
  </div>
</div>
<script>
document.addEventListener('DOMContentLoaded', function() {
  var calendarEl = document.getElementById('calendar');

  var calendar = new FullCalendar.Calendar(calendarEl, {
    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
    },
    initialDate: '<?= date("Y-m-d")?>',
    editable: true,
    navLinks: true, // can click day/week names to navigate views
    dayMaxEvents: true, // allow "more" link when too many events
    events: {
      url: '<?= Url::to(['suggestion/calendar-data'])?>',
      extraParams: function() { // a function that returns an object
        chkdVals='';
        $(".cb").each(function () {
           if(this.checked){
             chkdVals+=(chkdVals!="" ? ',' : '')+$(this).val();
           }
        });
        console.log("Here1?"+chkdVals);
        return {
          calender_id: chkdVals
        };
      },
      failure: function() {
        document.getElementById('script-warning').style.display = 'block'
      }
    },
    loading: function(bool) {
      document.getElementById('loading').style.display =
        bool ? 'block' : 'none';
    }
  });

  calendar.render();

  var elements = document.getElementsByClassName("cb");
  var elementsAll = document.getElementsByClassName("select-on-check-all");

  var reloadCalendar = function() {
      calendar.refetchEvents();
  };

  for (var i = 0; i < elements.length; i++) {
      elements[i].addEventListener('click', reloadCalendar, false);
  }
  for (var i = 0; i < elementsAll.length; i++) {
      elementsAll[i].addEventListener('click', reloadCalendar, false);
  }
});
</script>
