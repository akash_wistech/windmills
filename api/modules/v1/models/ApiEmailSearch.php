<?php
namespace api\modules\v1\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ApiTenantSearch represents the model behind the search form about `api\models\ApiTenant`.
 */
class ApiTenantSearch extends ApiTenant
{
    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['type', 'query', 'first_name', 'last_name', 'dob', 'passport_no', 'passport_expiryTo', 'passport_expiryFrom', 'visa_no', 'visa_expiry', 'office_address', 'po_box', 'City', 'landline_number', 'mobile_number', 'emergency_number', 'phone_number', 'email', 'marital_status', 'nationality', 'visa_expiryTo', 'visa_expiryFrom'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     * @return object of model scenarios
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params is for search model
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        $query = ApiTenant::find()
            ->leftJoin("user","user.id=tenants.user_id");

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            'tenants.id' => $this->id,
            'tenants.user_id' => $this->user_id,
            'tenants.type' => $this->type,
            'tenants.mobile_number' => $this->mobile_number,
            'user.email' => $this->email
            //'dob' => $this->dob,
            //'nationality' => $this->nationality,
        ]);

        return $dataProvider;
    }
}
