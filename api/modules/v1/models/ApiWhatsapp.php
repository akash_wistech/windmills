<?php

namespace api\modules\v1\models;

use Yii;
use common\models\Tenants;
use common\helpers\DateHelper;

class ApiTenant extends Tenants
{
    /**
     * @inheritdoc
     * @return array of attributes labels
     */

    public function fields()
    {
        return [
            // field name is the same as the attribute name
            'id',
            'full_name'=>function($model){
                if ($model->type == "residential") {
                    if ($model->residentialTenant <> null) {
                        return $model->residentialTenant->first_name . " " . $model->residentialTenant->last_name;
                    }
                }

                if ($model->type == "commercial") {
                    if ($model->commercialTenant <> null) {
                        return $model->commercialTenant->company_name;
                    }
                }
            },
            'first_name'=>function($model){
                if ($model->type == "residential") {
                    if ($model->residentialTenant <> null) {
                        return $model->residentialTenant->first_name;
                    }
                }

                if ($model->type == "commercial") {
                    if ($model->commercialTenant <> null) {
                        return $model->commercialTenant->company_name;
                    }
                }
            },
            'last_name'=>function($model){
                if ($model->type == "residential") {
                    if ($model->residentialTenant <> null) {
                        return $model->residentialTenant->last_name;
                    }
                }
            },
            'email'=>function($model){
                return $model->user->email;
            },
            'contract_status'=>function($model){
                if ($model->tenantContract <> null) {
                    return 'Generated ';
                }
            },
            'type'=>function($model){
                return ucwords($model->type);
            },
            'date'=>function($model){
                return DateHelper::getSystemDateAndTimeFormat($model->date);
            },
            'phone'=>function($model){
                if ($model->type == "residential") {
                    if ($model->residentialTenant <> null) {
                        return $model->residentialTenant->phone_number;
                    }
                }

                if ($model->type == "commercial") {
                    if ($model->commercialTenant <> null) {
                        return $model->commercialTenant->phone_number;
                    }
                }
            },
            'mobile'=>function($model){
                if ($model->type == "residential") {
                    if ($model->residentialTenant <> null) {
                        return $model->residentialTenant->mobile_number;
                    }
                }

                if ($model->type == "commercial") {
                    if ($model->commercialTenant <> null) {
                        return $model->commercialTenant->mobile_number;
                    }
                }
            },
            'landline'=>function($model){
                if ($model->type == "residential") {
                    if ($model->residentialTenant <> null) {
                        return $model->residentialTenant->landline_number;
                    }
                }

                if ($model->type == "commercial") {
                    if ($model->commercialTenant <> null) {
                        return $model->commercialTenant->landline_number;
                    }
                }
            },
            'emergency_number'=>function($model){
                if ($model->type == "residential") {
                    return $model->residentialTenant->emergency_number;
                }

                if ($model->type == "commercial") {
                    return $model->commercialTenant->emergency_number;
                }
            },
            'dob'=>function($model){
                if ($model->type == "residential") {
                    return DateHelper::systemDateFormat($model->residentialTenant->dob);
                }
            },
            'nationality'=>function($model){
                if ($model->type == "residential") {
                    return $model->residentialTenant->country!=null ? $model->residentialTenant->country->country_name : '';
                }
                if ($model->type == "commercial") {
                    return $model->commercialTenant->country!=null ? $model->commercialTenant->country->country_name : '';
                }
            },
            'passport_expiry'=>function($model){
                if ($model->type == "residential") {
                    return DateHelper::systemDateFormat($model->residentialTenant->passport_expiry);
                }
            },
            'visa_expiry'=>function($model){
                if ($model->type == "residential") {
                    return DateHelper::systemDateFormat($model->residentialTenant->visa_expiry);
                }
                if ($model->type == "commercial") {
                    return DateHelper::systemDateFormat($model->commercialTenant->visa_expiry);
                }
            },
            'visa_expiry'=>function($model){
                if ($model->type == "residential") {
                    return DateHelper::systemDateFormat($model->residentialTenant->emirates_id_expiry);
                }
                if ($model->type == "commercial") {
                    return DateHelper::systemDateFormat($model->commercialTenant->emirates_id_expiry);
                }
            },
            'marital_status'=>function($model){
                if ($model->type == "residential") {
                    return $model->residentialTenant->marital_status;
                }
            },
            'office_address'=>function($model){
                if ($model->type == "residential") {
                    return $model->residentialTenant->office_address;
                }
                if ($model->type == "commercial") {
                    return $model->commercialTenant->office_address;
                }
            },
            'po_box'=>function($model){
                if ($model->type == "residential") {
                    return $model->residentialTenant->po_box;
                }
            },
            'post_office_number'=>function($model){
                if ($model->type == "commercial") {
                    return $model->commercialTenant->post_office_number;
                }
            },
            'city'=>function($model){
                if ($model->type == "residential") {
                    return $model->residentialTenant->City;
                }
                if ($model->type == "commercial") {
                    return $model->commercialTenant->city_id;
                }
            },
            'company_name'=>function($model){
                if ($model->type == "commercial") {
                    return $model->commercialTenant->company_name;
                }
            },
            'authorize_person_name'=>function($model){
                if ($model->type == "commercial") {
                    return $model->commercialTenant->authorize_person_name;
                }
            },
            'trade_license_number'=>function($model){
                if ($model->type == "commercial") {
                    return $model->commercialTenant->trade_license_number;
                }
            },
            'trade_license_expiry'=>function($model){
                if ($model->type == "commercial") {
                    return DateHelper::systemDateFormat($model->commercialTenant->trade_license_expiry);
                }
            },
            'vat_number'=>function($model){
                if ($model->type == "commercial") {
                    return DateHelper::systemDateFormat($model->commercialTenant->vat_number);
                }
            },
            'secondary_office_address'=>function($model){
                if ($model->type == "commercial") {
                    return DateHelper::systemDateFormat($model->commercialTenant->secondary_office_address);
                }
            },
            'secondary_contact_name'=>function($model){
                if ($model->type == "commercial") {
                    return DateHelper::systemDateFormat($model->commercialTenant->secondary_contact_name);
                }
            },
            'secondary_landline_number'=>function($model){
                if ($model->type == "commercial") {
                    return DateHelper::systemDateFormat($model->commercialTenant->secondary_landline_number);
                }
            },
            'secondary_mobile_number'=>function($model){
                if ($model->type == "commercial") {
                    return DateHelper::systemDateFormat($model->commercialTenant->secondary_mobile_number);
                }
            },
            'active_contracts'=>function($model){
                return $model->paymentContract;
            },
            /*'active_units'=>function($model){
                $units = array();
                if(!empty($model->paymentContract)){
                    foreach ($model->paymentContract as $activeContract){
                        $units[]= $activeContract->unit_id;
                    }
                }
                return $units;
            },
            'active_properties'=>function($model){
                $properties = array();
                if(!empty($model->paymentContract)){
                    foreach ($model->paymentContract as $activeContract){
                        $properties[]= $activeContract->unit->property_id;
                    }
                }
                return $properties;
            },*/
            'active_units'=>function($model){
                $units = array();
                if(!empty($model->paymentContract)){
                    foreach ($model->paymentContract as $activeContract){
                        $units[]= $activeContract->unit->name;
                    }
                }
                return $units;
            },
            'active_properties'=>function($model){
                $properties = array();
                if(!empty($model->paymentContract)){
                    foreach ($model->paymentContract as $activeContract){
                        if(!in_array($activeContract->unit->property->name,$properties)){
                            $properties[]= $activeContract->unit->property->name;
                        }
                    }
                }
                return $properties;
            },
            'status'=>function($model){

                if(!empty($model->paymentContract)){
                    return 1;
                }
                return 2;
            },
        ];
    }
}
