<?php
namespace api\modules\v1\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ApiEmployeeSearch represents the model behind the search form about `api\models\ApiEmployee`.
 */
class ApiEmployeeSearch extends ApiEmployee
{
    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['type', 'query', 'first_name', 'last_name', 'dob', 'passport_no', 'passport_expiryTo', 'passport_expiryFrom', 'visa_no', 'visa_expiry', 'office_address', 'po_box', 'City', 'landline_number', 'mobile_number', 'emergency_number', 'phone_number', 'email', 'marital_status', 'nationality', 'visa_expiryTo', 'visa_expiryFrom'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     * @return object of model scenarios
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params is for search model
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        $query = ApiEmployee::find();

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        return $dataProvider;
    }
}
