<?php

namespace api\modules\v1\models;

use Yii;
use common\models\Employee;
use common\helpers\DateHelper;

class ApiEmployee extends Employee
{
  /**
  * @inheritdoc
  * @return array of attributes labels
  */

  public function fields()
  {
    return [
      // field name is the same as the attribute name
      'id',
      'name'=>function($model){
        return $model->first_name.' '.$model->last_name;
      },
      'email'=>function($model){
        return $model->user->email;
      },
    ];
  }
}
