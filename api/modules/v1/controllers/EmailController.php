<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use api\modules\v1\models\ApiEmployeeSearch;


class EmployeeController extends DefaultController
{
  public $serializer = [
    'class' => 'yii\rest\Serializer',
    'collectionEnvelope' => 'items',
  ];

  public function actionIndex()
  {
    $searchModel = new ApiEmployeeSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    return $dataProvider;
  }
}
