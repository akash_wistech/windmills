<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use api\modules\v1\models\ApiTenantSearch;


class TenantController extends DefaultController
{

    public $enableCsrfValidation = false;
  public $serializer = [
    'class' => 'yii\rest\Serializer',
    'collectionEnvelope' => 'items',
  ];
  
  public function actionIndex()
  {
    $searchModel = new ApiTenantSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    return $dataProvider;
  }
}
