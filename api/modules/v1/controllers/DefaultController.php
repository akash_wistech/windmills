<?php
namespace api\modules\v1\controllers;

use Yii;
use yii\web\Response;
use yii\rest\Controller;


class DefaultController extends Controller
{
    public $enableCsrfValidation = false;
  public function behaviors()
  {
    $behaviors = parent::behaviors();
    $behaviors['contentNegotiator'] = [
      'class' => 'yii\filters\ContentNegotiator',
      'formats' => [
        'application/json' => Response::FORMAT_JSON,
      ]
    ];
    return $behaviors;
  }
}
