<?php
date_default_timezone_set('Asia/Dubai');

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
      'v1' => [
        'class' => 'api\modules\v1\Module',
      ],
    ],
    'components' => [
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'request' => [
          'cookieValidationKey' => 'PlgF8crd8jEFssTeKOcclS2y-iv8rMHc',
          // Enable JSON Input:
          'parsers' => [
            'application/json' => 'yii\web\JsonParser',
          ]
        ],
        'urlManager' => [
          'enablePrettyUrl' => true,
          'enableStrictParsing' => false,
          'showScriptName' => false,
          'rules' => [
            ['class' => 'yii\rest\UrlRule','controller' => 'tenant/'],
            ['class' => 'yii\rest\UrlRule','controller' => 'employee/'],
          ],
        ],
        'user' => [
            'identityClass' => 'common\models\UserIdentity',
          'enableAutoLogin' => false,
        ],

    ],
    'params' => $params,

];
