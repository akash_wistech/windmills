<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Company;

use yii\helpers\Url;
use yii;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class StatusController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex()
    {

        if (date('l') != 'Saturday' && date('l') != 'Sunday') {

            //$clients = Company::find()->asArray()->all();
            $valuation_data = \app\models\Valuation::find()->select(['client_id'])->where(['!=', 'valuation_status', 5])->andWhere(['!=', 'valuation_status', 6])->andWhere(['!=', 'valuation_status', 9])->asArray()->all();
            $clients_array = array();

            foreach ($valuation_data as $client) {
                $clients_array[] = $client['client_id'];
            }
            $clients = array_unique($clients_array);
            foreach ($clients as $key => $value) {
                //Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document]
                $client = Company::find()->where(['id' => $value])->one();

                if($client->p_status_1 == 1) {

                    if ($client->primaryContact->email != null && $client->primaryContact->email != 'ziad@greenhouseuae.com') {
                        $url = Url::toRoute(['client/pdf', 'id' => $client['id']]);
                        $val = Yii::$app->helperFunctions->getDonwloadpdfnew($client['id']);
                        if ($val != null) {
                            $attachments = $val;
                            $notifyData = [
                                'client' => $client,
                                'uid' => $client['id'],
                                'attachments' => $attachments,
                                'subject' => 'Periodical Valuation Status Report',
                                'replacements' => [
                                    '{clientName}' => $client['title'],
                                ],
                            ];
                            \app\modules\wisnotify\listners\NotifyEvent::fire('Client.pdf.send', $notifyData);
                        } else {
                            $attachments = '';
                        }

                    }
                }
            }

            return 'Emails has successfully sent.';
        }
    }
}
