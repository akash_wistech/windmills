<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\CrmQuotations;
use app\models\ClientSegmentFile;
use app\models\CrmQuotationFollowupEmail;

use yii\helpers\Url;
use yii;

use DateTime;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CrmQuotationFollowupEmailController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */



    public function actionQuotationVerifiedFollowupEmail()
    {
        // $quotations = \app\models\CrmQuotations::find()->where(['quotation_status'=>2])->all();
        // $quotations = CrmQuotations::find()->where(['quotation_status' => 2])->andWhere(['>', 'id', 3056])->all();
        $quotations = CrmQuotations::find()
        ->where(['quotation_status' => 2])
        ->andWhere(['>', 'id', 3085])
         ->andWhere(['client_name' => 9166])
        ->all();
        
        // dd($quotations);

        if ($quotations <> null) {
            foreach ($quotations as $key => $quotation) {

                $follow_ups = CrmQuotationFollowupEmail::find()
                    ->where(['quotation_id' => $quotation->id, 'action' => 'QuotationVerifiedFollowUpEmail'])
                    ->asArray()->all();

                $follow_up_checker = CrmQuotationFollowupEmail::find()
                    ->where(['quotation_id' => $quotation->id, 'action' => 'QuotationVerifiedFollowUpEmail'])
                    ->asArray()->one();


                $no_of_reminder_sent = count($follow_ups);

                if ($follow_up_checker <> null) {
                    $created_date = date("Y-m-d", strtotime($follow_up_checker['date']));
                } else {
                    $created_date = date("Y-m-d", strtotime($quotation->approved_date));
                }
                $clientSegment = ClientSegmentFile::find()
                ->where(['client_type' => $quotation->client->client_type])
                ->andWhere(['status_verified' => 1])
                ->asArray()->one();

                if($clientSegment <> null){
                    
                    $no_of_reminder = $clientSegment['quotation_followup_email_no'];
                    $days_to_add = $clientSegment['quotation_followup_period__days'];
                    // $days_to_add = 3;

                    if($quotation->client->client_type == "bank"){ 
                        if($quotation->final_fee_approved >5000){
                            if($no_of_reminder_sent == 0 ){
                                $days_to_add = 2;
                            }else if($no_of_reminder_sent < $no_of_reminder){
                                $days_to_add = $clientSegment['quotation_followup_period__days']; 
                            }
                        }
                    }else { 
                        if($no_of_reminder_sent == 0 ){
                            $days_to_add = 2;
                        }else if($no_of_reminder_sent < $no_of_reminder){
                            $days_to_add = $clientSegment['quotation_followup_period__days']; 
                        }
                    }

                    // dd($days_to_add);

                    $currentDate = new DateTime($created_date);
                    $addedDays = 0;

                    while ($addedDays < $days_to_add) {
                        $currentDate->modify('+1 day');
                        if ($currentDate->format('N') < 6) { // 1 (for Monday) through 5 (for Friday)
                            $addedDays++;
                        }
                    }

                    $follow_up_date = $currentDate->format('Y-m-d');

                    //add days
                    $today_date = date("Y-m-d");
                    // $today_date = '2024-07-31'; // comment after testing is done

                    // $follow_up_date = date("Y-m-d", strtotime("$created_date + $days_to_add days"));

                    // $follow_up_date = '2024-07-26'; //comment after testing is done;
                  /*  echo $quotation->reference_number.'<br>kjkht';
                    echo $follow_up_date.'<br>kjkht';
                    echo $today_date;
                    die;*/

                    if (strtotime($follow_up_date) == strtotime($today_date)) {
                        if ($no_of_reminder_sent < $no_of_reminder) {
                            $notifyData = [
                                'client' => $quotation->client,
                                'reference_number' => $quotation->reference_number,
                                'attachments' => [],
                                'replacements' => [
                                    '{clientName}' => $quotation->client->title,
                                ],
                            ];
                            //sending email
                            \app\modules\wisnotify\listners\NotifyEvent::fireQuotationFollowupEmail('quotation.verified', $notifyData);
                            
                            //email sending entry
                            $email_entry = new \app\models\CrmQuotationFollowupEmail;
                            $email_entry->quotation_id  = $quotation->id;
                            $email_entry->client_id     = $quotation->client_name;
                            $email_entry->date          = date('Y-m-d H:i:s');
                            $email_entry->action        = 'QuotationVerifiedFollowUpEmail';
                            $email_entry->created_at    = date('Y-m-d H:i:s');
                            if ($email_entry->save()) {
                                echo "QuotationVerifiedFollowUpEmail sent";
                            }
                        } else {
                            echo "Total number of reminders reached \n";
                        }
                    } else {
                        echo "Date not match \n";
                    }

                }
            }
        }
    }
}
