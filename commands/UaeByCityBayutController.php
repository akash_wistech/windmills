<?php
namespace app\commands;
use Yii;
use yii\console\Controller;
use app\components\traits\BayutRentProperties;
use app\models\UaeByCityDltUrl;
use app\models\UaeByCityUrl;
use Exception;
use yii\db\Expression;

class UaeByCityBayutController extends Controller
{
    public function actionIndex($city_id, $purpose)
    {      
        $start = 21;
        $end = 22;
        $now = date('H');
        if ($now >= $start && $now <= $end) {
            
            $log_status = \app\models\CityCronLogs::find()
            ->where(['city_id'=>$city_id, 'purpose'=>$purpose, 'action' => 'index'])
            ->andWhere(new Expression(' DATE(date) = CURDATE() '))
            ->asArray()
            ->one();
            
            if ($log_status==null) {
                $connection=Yii::$app->db;
                $connection->createCommand(' TRUNCATE `uae_by_city_url`; TRUNCATE `uae_by_city_dlt_url`; ')->execute();
                $connection->createCommand('INSERT INTO `uae_by_city_url`(`url`, `city_id`, `purpose`) SELECT `url`, `city_id`, `purpose` FROM `uae_by_city_url_backup`')->execute();
                
                $log_status = new \app\models\CityCronLogs;
                $log_status->city_id = $city_id; 
                $log_status->purpose = $purpose;
                $log_status->date    = date("Y-m-d");
                $log_status->action  = 'index';
                $log_status->save();
            } 
            
            if ($log_status['date']== date("Y-m-d")) {
                $query = UaeByCityUrl::find()->where(['city_id'=>$city_id, 'purpose'=>$purpose])->one();
                if($query<>null)
                {
                    $httpClient = new \Goutte\Client();
                    $response = $httpClient->request('GET', $query->url);
                    $i=1;
                    $response->filter('._357a9937 li.ef447dde')->each(function ($node) use(&$query, &$purpose, &$i) {
                        $link = $node->filter('article.ca2f5674 div._4041eb80 a')->attr('href');
                        $link = 'https://www.bayut.com'.$link;
                        if ($link<>null) {
                            $newModel = UaeByCityDltUrl::find()->where(['url'=>$link, 'purpose'=>$purpose])->one();
                            if ($newModel==null) {
                                $newModel           = new UaeByCityDltUrl;
                                $newModel->url      = $link;
                                $newModel->city_id  = $query->city_id;
                                $newModel->status   = 0;
                                $newModel->purpose  = $purpose;
                                if ($newModel->save()) {
                                    echo " save => ".$i."<br>";
                                }
                            }
                        }
                        $i++;
                    });
                    
                    $buttonLinks = [];
                    $buttonTitles = [];
                    $response->filter('div._035521cc div._41cc3033 div.bbfbe3d2 div._6cab5d36 ul._92c36ba1 li a')->each(function($buttonNode) use(&$buttonLinks,&$buttonTitles){
                        $buttonLinks[] = $buttonNode->attr('href');
                        $buttonTitles[] = $buttonNode->attr('title');
                    });
                    $nextPageUrl=null;
                    foreach ($buttonLinks as $key => $value) {
                        if ($buttonTitles[$key]=='Next') {
                            $nextPageUrl = 'https://www.bayut.com'.$value;
                        }
                    }           
                    if ($nextPageUrl<>null) {
                        Yii::$app->db->createCommand()->update('uae_by_city_url', [ 'url' => $nextPageUrl ], [ 'city_id'=> $query->city_id, 'purpose' => $purpose ])->execute();    
                    }
                    else{ 
                    }
                }
            }
            else{
                
            }
        }else{ 
            echo "time out"; 
        }
        
    }
    
    public function actionDetail($city_id, $purpose)
    {
        
        
        $start = 21;
        $end = 22;
        $now = date('H'); 
        if ($now >= $start && $now <= $end) {
            error_log('bs -1');
            $query = UaeByCityDltUrl::find()->where(['status'=>0, 'city_id'=>$city_id, 'purpose'=>$purpose])->asArray()->limit(15)->all();
            if ($query<>null) {
                foreach($query as $query){
                    error_log('bs -2');
                    Yii::$app->db->createCommand()->update('uae_by_city_dlt_url', ['status' => 2], [ 'url'=>$query['url'], 'city_id' => $query['city_id'], 'purpose'=>$purpose ])->execute();
                    $data=[];
                    try{
                        error_log('bs -3');
                        $httpClient = new \Goutte\Client();
                        $crawler = $httpClient->request('GET', $query['url']);
                        error_log('bs -4');
                    }
                    catch(Exception $err){
                        error_log('bs -5');
                        $error = $err->getMessage(); print_r($error);
                    }
                    try {
                        $bed_bath_area = [];
                        $crawler->filter('div._6f6bb3bc div.ba1ca68e._0ee3305d')->each(function($etcNode)use(&$bed_bath_area){
                            $bed_bath_area[$etcNode->filter('span.cfe8d274')->attr('aria-label')] =  $etcNode->filter('span.fc2d1086')->text();
                        });
                    }
                    catch ( Exception $err ) {
                        $error = $err->getMessage();
                    }
                    try {
                        $propertyInfoArr =  [];
                        $i=1;
                        $crawler->filter('ul._033281ab li')->each(function($propertyNode) use(&$propertyInfoArr, &$i) {
                            $propertyInfoArr[$propertyNode->filter('span._3af7fa95')->text()] = $propertyNode->filter('span._812aa185')->text();
                            $i++;
                        });
                    }
                    catch (Exception $err) {
                        $error = $err->getMessage();
                        try{
                            $j =1;
                            $crawler->filter('ul._033281ab li')->each(function($propertyNode) use(&$propertyInfoArr, &$j, &$i) {
                                if ($j==$i) {
                                    $propertyInfoArr[$propertyNode->filter('div._3af7fa95')->text()] = $propertyNode->filter('span._812aa185')->text();
                                }
                                if ($j>$i) {
                                    $propertyInfoArr[$propertyNode->filter('span._3af7fa95')->text()] = $propertyNode->filter('span._812aa185')->text();
                                }
                                $j++;
                            });
                        }
                        catch(Exception $err){
                            $error = $err->getMessage();
                        }
                    }
                    try{
                        $title = $crawler->filter('div._1f0f1758')->text();
                        $titleExplode = explode(',', $title);
                        $buildingTitle = $community = $sub_community = $city = '';
                        if (isset($titleExplode[0]) AND $titleExplode[0]<>null) {
                            $buildingTitle = $titleExplode[0];
                        }
                        if (isset($titleExplode[1]) AND $titleExplode[1]<>null) {
                            $community = $titleExplode[1];
                        }
                        if (isset($titleExplode[3]) AND $titleExplode[3]<>null) {
                            if (isset($titleExplode[2]) && $titleExplode[2]<>null) {
                                $sub_community = $titleExplode[2];
                            }
                        }
                        $city = $query['city_id'];
                        $listingAndFinalPrice = $crawler->filter('div.c4fc20ba span._105b8a67')->text();
                        
                    }
                    catch(Exception $err){
                        $error = $err->getMessage();
                    }
                    $data['url'] = $query['url'];
                    if ($bed_bath_area<>null) {
                        $bed_bath_area_studio_arr = bayutRentProperties::get_Bed_Bath_Area($bed_bath_area);
                        if ($bed_bath_area_studio_arr<>null) {
                            $data['no_of_bedrooms'] = trim($bed_bath_area_studio_arr['no_of_bedrooms']);
                            $data['no_of_bathrooms'] = trim($bed_bath_area_studio_arr['no_of_bathrooms']);
                            $data['property_size'] = trim($bed_bath_area_studio_arr['area']);
                            $data['studio'] = trim($bed_bath_area_studio_arr['studio']);
                        }
                    }
                    if ($propertyInfoArr<>null) {
                        $propertyInfoArr = bayutRentProperties::getPropertyData($propertyInfoArr);
                        if ($propertyInfoArr<>null) {
                            $data ['property_type'] = trim(strtolower($propertyInfoArr['type']));
                            $data ['purpose'] = trim($propertyInfoArr['purpose']);
                            $data ['ref_number'] = trim($propertyInfoArr['ref_number']);
                            // $data ['average_rent'] = trim($propertyInfoArr['Average Rent']);
                            $data ['added_on'] = trim($propertyInfoArr['added_on']);
                        }
                    }
                    // $data['property_category'] = trim($property_type);
                    $data['building_title'] = trim($buildingTitle);
                    $data['community'] = trim($community);
                    $data['sub_community'] = trim($sub_community);
                    $data['city'] = trim($city);
                    $data['listing_price'] = trim(str_replace("," , "", $listingAndFinalPrice));
                    // $data['desc'] = trim(strtolower($propertyDescription));
                    
                    $this->saveListData($data, $query, $city_id, $purpose);
                }
            } else {
                
            } 
        } else { 
            echo " time out"; 
        }
    }
    
    public function saveListData($data=null , $query=null, $city_id=null, $purpose=null)
    {
        error_log('bs -6');
        $bedrooms = '';
        if ($data['no_of_bedrooms']<>null && is_numeric($data['no_of_bedrooms'])) {
            $bedrooms = $data['no_of_bedrooms'];
        }
        $listingTransaction = \app\models\ListData::find()->where(['listings_reference'=>$data['ref_number'], 'listing_website_link'=>$data['url'], 'purpose'=> $purpose ])->one();
        if ($listingTransaction==null) {
            $listingTransaction = new \app\models\ListData;
        }
        error_log('bs -7');
        $listingTransaction->listings_reference   = $data['ref_number'];
        $listingTransaction->source               = 15; 
        $listingTransaction->listing_website_link = $data['url'];
        $listingTransaction->listing_date         = $data['added_on'];
        $listingTransaction->building_info        = $data['building_title'];
        $listingTransaction->property_category    = '';
        $listingTransaction->property_type        = $data['property_type'];;
        $listingTransaction->community            = $data['community'];;
        $listingTransaction->sub_community        = $data['sub_community'];;
        $listingTransaction->city_id              = $data['city'];
        $listingTransaction->no_of_bedrooms       = ($bedrooms<>null AND $bedrooms!='') ? $bedrooms : 0;
        $listingTransaction->built_up_area        = $data['property_size'];
        $listingTransaction->land_size            = $data['property_size'];
        $listingTransaction->listings_price       = trim($data['listing_price']);
        $listingTransaction->listings_rent        = trim($data['listing_price']); //Yii::$app->propertyFinderHelperFunctions->getListingRent($data['property_size'],$bedrooms);
        $listingTransaction->final_price          = trim($data['listing_price']);
        $listingTransaction->status               = 2;
        $listingTransaction->move_to_listing      = 0;
        $listingTransaction->purpose              = $purpose;
        $listingTransaction->created_at           = date("Y-m-d h:i:s");
        error_log('bs -8');
        if (!$listingTransaction->save()) {
            error_log('bs -9');
            Yii::$app->db->createCommand()
            ->update('uae_by_city_dlt_url', [ 'status' => 1 ] , [ 'url' => $data['url'], 'city_id'=>$query['city_id'], 'purpose'=>$purpose ])
            ->execute();
            echo " Saved Successfully ";
        }
        if($listingTransaction->hasErrors()){
            error_log('bs -10');
            foreach($listingTransaction->getErrors() as $error){
                if(count($error)>0){
                    foreach($error as $key=>$val){
                        echo $val. "<br>";
                    }   
                }
            }
        }
    }
    
}