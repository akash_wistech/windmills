<?php
namespace app\commands;
use Yii;
use yii\console\Controller;

use app\modules\wisnotify\listners\NotifyEvent;
use app\models\SalesAndMarketing;

class MeetingFollowupController extends Controller
{
    public function actionSendemail()
    {
        $meetings = SalesAndMarketing::find()->where(['meeting_status'=>1, 'email_reminder_status'=>0])->all();
        if($meetings<>null){
            foreach($meetings as $key => $meeting){
                //meeting start date and time
                $meetingDateTime = date('Y-m-d', strtotime($meeting->date)).' '.date('H:i', strtotime($meeting->time));

                //get 3 hour previous date time from meeting start datetime
                $dateTimeObj = new \DateTime($meetingDateTime);
                $dateTimeObj->modify('-3 hours');
                $newDatetime = $dateTimeObj->format('Y-m-d H:i:s');
                
                //check if current date and meeting start date is same then run
                if(date('Y-m-d', strtotime($meeting->date)) == date('Y-m-d', strtotime($newDatetime))){
                    //get time hour digit from previous date time
                    $back_hour =  date('H', strtotime($newDatetime));
                    //get time hour digit from current date time
                    $current_hour = date('H', strtotime(date('Y-m-d H:i:s')));
                    //check if the current time hour is equal to the  previous tie hour then send an reminder email to only windmills atendees
                    // if($current_hour == $back_hour){
                    if($current_hour == 8){

                        $attendeeIds = [];
                        if($meeting->wmAttendees<>null){
                            $attendeeIds = \yii\helpers\ArrayHelper::getColumn($meeting->wmAttendees, 'sales_and_marketing_attendees_id');
                        }
                        
                        $countWmAttendee = count($meeting->wmAttendees);
                        $wm_attendeesHtml = '';
                        if($meeting->wmAttendees<>null){
                            foreach($meeting->wmAttendees as $index => $attendee){
                                $wm_attendeesHtml .= $attendee->client_attendee_title. ' '.$attendee->attendee->firstname.' '.$attendee->attendee->lastname;
                                if($index < $countWmAttendee - 1) {
                                    $wm_attendeesHtml .= ', ';
                                }
                            }
                        }

                        $countClientOtherAttendee = count($meeting->clientAllAttendees);
                        $client_attendeesHtml = '';
                        if($meeting->clientAllAttendees<>null){
                            foreach($meeting->clientAllAttendees as $index => $attendee){
                                $client_attendeesHtml .= $attendee->client_attendee_title. ' '.$attendee->attendee->firstname.' '.$attendee->attendee->lastname;
                                if($index < $countClientOtherAttendee - 1) {
                                    $client_attendeesHtml .= ', ';
                                }
                            }
                        }
                        
                        $notifyData = [
                            'client' => null,
                            'scheduler' => $meeting->scheduler,
                            'attendeeIds' => $attendeeIds,
                            'attachments' => [],
                            'replacements'=>[
                                '{meeting_start_date}'   => date('d F Y', strtotime($meeting->date)),
                                '{meeting_start_time}'   => date('H:m:s', strtotime($meeting->time)),
                                '{meeting_place}'        => $meeting->meeting_place,
                                '{client_attendees}'     => $client_attendeesHtml,
                                '{windmills_attendees}'  => $wm_attendeesHtml,
                                '{meeting_interface}'    => Yii::$app->smHelper->getMeetingInterfaceArr()[$meeting->meeting_interface],
                                '{purpose}'              => Yii::$app->smHelper->getPurposeArr()[$meeting->purpose],
                                '{google_location_pin}'  => str_replace(' ', '+', $meeting->location_url),
                                '{calendar_invitation_link}' => $meeting->calendar_invitation_link,
                                '{name_of_scheduler}'    => $meeting->scheduler->firstname.' '.$meeting->scheduler->lastname,
                
                            ],
                        ];
                        // dd($notifyData);

                        //sending email
                        \app\modules\wisnotify\listners\NotifyEvent::fireReminderEmail('sm.reminderemail', $notifyData);

                        Yii::$app->db->createCommand()
                        ->update(SalesAndMarketing::tableName(), [ 'email_reminder_status' => 1 ], [ 'id' => $meeting->id ])
                        ->execute();

                    }
                    
                }
            }
        }


    }
}
