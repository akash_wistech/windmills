<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Valuation;
use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Company;

use yii\helpers\Url;
use yii;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MonthlyBanksController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex()
    {



            $clients = Company::find()->where(['client_type'=> 'bank','status'=>1])->all();
           // $client = Company::find()->where(['client_type'=> 'bank','id'=> 9166])->one();

            $month =  date('m') - 1;
            $year = date('Y');
            if($month == 0){
                $month= 1;
            }
foreach ($clients as $client) {
    $valuations_month_number = Valuation::find()->where(['client_id' => $client->id])
        ->andWhere('valuation_status=5')
        ->andFilterWhere([
            'between', 'submission_approver_date', '2023-05-01 00:00:00','2023-05-31 23:59:59'
        ])
        ->all();

    $valuations_year_number = Valuation::find()->where(['client_id' => $client->id])

        ->andWhere('valuation_status=5')
        ->andFilterWhere([
            'between', 'submission_approver_date', '2023-01-01 00:00:00','2023-05-31 23:59:59'
        ])
        ->all();

    $last_month = date('F', strtotime('last month'));
    $date_value = $last_month . ' ' . date('Y');
    $subject = 'Valuation Instructions to Windmills in ' . $date_value;


    $notifyData = [
        'client' => $client,
        'subject' => $subject,
        'attachments' => [],
        'uid' => $client->id,
        'replacements' => [
            '{clientName}' => $client->title,
            '{total_valuation}' => count($valuations_year_number),
            '{date_value}' => $date_value,
            '{this_month_valuation}' => count($valuations_month_number),

        ],
    ];

    if (count($valuations_month_number) > 10) {
        \app\modules\wisnotify\listners\NotifyEvent::fireMonthlyAllBanks('valuation.monthlymorethanten', $notifyData);

    } else if (count($valuations_month_number) < 10) {
        \app\modules\wisnotify\listners\NotifyEvent::fireMonthlyAllBanks('valuation.monthlylessthanten', $notifyData);
    }

}
            return 'Emails has successfully sent.';
        }

}
