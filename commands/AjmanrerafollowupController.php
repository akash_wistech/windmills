<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Company;
use app\models\Valuation;
use app\models\Buildings;

use yii\helpers\Url;
use yii;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AjmanrerafollowupController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex()
    {

        if (date('l') != 'Saturday' && date('l') != 'Sunday') {

            $frequncy =  Yii::$app->appHelperFunctions->getValSetting('ajman_rera_followup_email_frequency');


            $valuations = Valuation::find()
                ->select([
                    Valuation::tableName() . '.id',
                    Valuation::tableName() . '.reference_number',
                    Valuation::tableName() . '.target_date',
                    Valuation::tableName() . '.client_name_passport',
                    Valuation::tableName() . '.title_deed',
                    Valuation::tableName() . '.submission_approver_date',
                    Valuation::tableName() . '.parent_id',
                    Valuation::tableName() . '.ajman_email_status',
                    Valuation::tableName() . '.ajman_follow_up_date',
                    Valuation::tableName() . '.ajman_approved',
                    Buildings::tableName() . '.title as Building',
                ])
                ->leftJoin(Buildings::tableName(), Buildings::tableName() . '.id=' . Valuation::tableName() . '.building_info')
                ->where([Buildings::tableName() . '.city' => 3507])
                ->andWhere([Valuation::tableName() . '.valuation_status' => 5])
                ->andWhere([Valuation::tableName() . '.ajman_approved' => [0,2]])
                ->andWhere(['>', Valuation::tableName() . '.id', 13963])
                ->asArray()->all();



            if($valuations<> null && !empty($valuations)){

                $main_array_vals = array();
                $main_array_vals_clients = array();
                $parent_array_vals = array();

                foreach ($valuations as $key => $data_set_a) {
                    if ($data_set_a['parent_id'] <> null) {
                        $parent_array_vals[] = $data_set_a['parent_id'];
                    }
                }


                foreach ($valuations as $key => $data_set_b) {
                    if (!in_array($data_set_b['id'], $parent_array_vals)) {
                        if($data_set_b['ajman_follow_up_date'] <> null){

                        }else{
                            $data_set_b['ajman_follow_up_date'] =  date('Y-m-d',strtotime($data_set_b['submission_approver_date']));
                        }
                        $main_array_vals_clients[] = $data_set_b;
                        if($data_set_b['ajman_approved'] == 0) {
                            $main_array_vals[] = $data_set_b;
                        }
                    }
                }

                /* echo "<pre>";
                 print_r($main_array_vals_clients);
                 print_r($main_array_vals);
                 die;*/

                foreach ($main_array_vals_clients as $key => $valuation){

                    if($valuation['ajman_follow_up_date'] <> null){

                        $current_date = date('Y-m-d');
                        $follow_up_date = $valuation['ajman_follow_up_date'];
                        $date1 = new \DateTime($current_date);
                        $date2 =  new \DateTime($follow_up_date);

                        $interval = $date1->diff($date2);

                        if($interval->days >= $frequncy){


                            $valuation = Valuation::find()->where(['id' => $valuation['id']])->one();
                            $uid= $valuation->id;
                            if(isset($valuation->quotation_id) && $valuation->quotation_id <> null){
                                $uid = 'crm'.$valuation->quotation_id;
                            }
                            $inspectionEmail ='';
                            //client email
                            $notifyData = [
                                'client' => $valuation->client,
                                'attachments' => [],
                                'uid' => $uid,
                                'subject' => $valuation->email_subject,
                                'valuer' => $valuation->approver->email,
                                'inspector' => $inspectionEmail,
                                'replacements' => [
                                    '{clientName}' => $valuation->client->title,
                                ],
                            ];
                            \app\modules\wisnotify\listners\NotifyEvent::fire1('ajmanrera.followupclient.send', $notifyData);

                        }
                    }

                }
                foreach ($main_array_vals as $key => $valuation_ajman){

                    if($valuation_ajman['ajman_follow_up_date'] <> null){

                        $current_date = date('Y-m-d');
                        $follow_up_date = $valuation_ajman['ajman_follow_up_date'];
                        $date1 = new \DateTime($current_date);
                        $date2 =  new \DateTime($follow_up_date);

                        $interval = $date1->diff($date2);

                        if($interval->days >= $frequncy){


                            $valuation = Valuation::find()->where(['id' => $valuation_ajman['id']])->one();
                            $uid= $valuation->id;
                            if(isset($valuation->quotation_id) && $valuation->quotation_id <> null){
                                $uid = 'crm'.$valuation->quotation_id;
                            }
                            $inspectionEmail ='';

                            //govt email
                            $subject = "Windmills Valuation Report -".$valuation->reference_number;
                            $client = $valuation->client;
                            $attachments=[];
                            $notifyData = [
                                'client' => $client,
                                'attachments' => $attachments,
                                'g_subject'=>$subject
                            ];
                            \app\modules\wisnotify\listners\NotifyEvent::fireToGovt('ajmanrera.followupgovt.send', $notifyData);

                            Yii::$app->db->createCommand()->update('valuation', ['ajman_follow_up_date' => date('Y-m-d')], 'id='.$valuation->id .'')->execute();
                        }
                    }

                }
            }

            echo 'Emails has successfully sent.';
            exit;
        }
    }
}
