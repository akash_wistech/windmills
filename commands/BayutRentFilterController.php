<?php
namespace app\commands;
use Yii;

use yii\console\Controller;
use app\models\BayutToRentLinks;
use app\models\FetchBayutToRentLink;
use app\models\BayutToRentDetailLinks;
use Exception;
use app\components\traits\bayutRentProperties;
use app\models\Buildings;
use app\models\ListingsRent;
use app\models\Valuation;

class BayutRentFilterController extends Controller
{
  public function actionForRentIndex()
  {


    $fetch_url = FetchBayutToRentLink::find()->one();

    if ($fetch_url==null) {
      $fetch_parent_url = Valuation::find()
            ->select(['id','b_to_rent_link_to_scrape'])
            ->where(['b_to_rent_link_to_scrape_status'=>0])
            ->andWhere(['not', ['b_to_rent_link_to_scrape' => null]])
            ->one();

      if ($fetch_parent_url<>null) {
        $model = new FetchBayutToRentLink;
        $model->parent_link_id     = $fetch_parent_url->id;
        $model->url_to_fetch       = $fetch_parent_url->b_to_rent_link_to_scrape;
        // $model->property_category  = $fetch_parent_url->property_category;
        if($model->save()){
          $parent_link_id     = $model->parent_link_id;
          $url                = $model->url_to_fetch;
        //   $property_category  = $model->property_category;
        }
      }
    }
    else{
      $parent_link_id         = $fetch_url->parent_link_id;
      $url                    = $fetch_url->url_to_fetch;
    //   $property_category      = $fetch_url->property_category;
    }


    // echo $url; die;

    $httpClient = new \Goutte\Client();
    $response = $httpClient->request('GET', $url);
    $i=1;
    $response->filter('._357a9937 li.ef447dde')->each(function ($node) use($property_category, &$i) {
      $link = $node->filter('article.ca2f5674 div._4041eb80 a')->attr('href');
      $link = 'https://www.bayut.com'.$link;
      if ($link<>null) {
        $newModel = BayutToRentDetailLinks::find()->where(['url'=>$link])->one();
        if ($newModel==null) {
          $newModel = new BayutToRentDetailLinks;
          $newModel->url = $link;
        //   $newModel->property_category = $property_category;
          $newModel->status = 0;
          if ($newModel->save()) {
            echo " save=> ".$i." => ".$link."<br>";
          }
        }
      }
      $i++;
    });
    $buttonLinks = [];
    $buttonTitles = [];

    $response->filter('div._035521cc div._41cc3033 div.bbfbe3d2 div._6cab5d36 ul._92c36ba1 li a')->each(function($buttonNode) use(&$buttonLinks,&$buttonTitles){
      $buttonLinks[] = $buttonNode->attr('href');
      $buttonTitles[] = $buttonNode->attr('title');
    });

    $nextPageUrl=null;
    foreach ($buttonLinks as $key => $value) {
      if ($buttonTitles[$key]=='Next') {
        $nextPageUrl = 'https://www.bayut.com'.$value;
      }
    }

    //  echo $nextPageUrl; die;


    if ($nextPageUrl<>null) {
      Yii::$app->db->createCommand()->update('fetch_bayut_to_rent_link', ['parent_link_id' => $parent_link_id,'url_to_fetch'=>$nextPageUrl])->execute();
    }
    else{

        Yii::$app->db->createCommand()
      ->update('valuation', ['b_to_rent_link_to_scrape_status' => 1], 'id ='.$parent_link_id)
      ->execute();

      $queryTwo = Valuation::find()
      ->select(['id', 'b_to_rent_link_to_scrape'])
      ->where(['>', 'id', $parent_link_id])
      ->andWhere(['b_to_rent_link_to_scrape_status'=>0])
      ->andWhere(['not', ['b_to_rent_link_to_scrape' => null]])
      ->asArray()
      ->one();
      if($queryTwo<>null){
        Yii::$app->db->createCommand()->update('fetch_bayut_to_rent_link', ['parent_link_id' => $queryTwo['id'],'url_to_fetch'=> $queryTwo['b_to_rent_link_to_scrape']])->execute();
      }
    }
  }







  public function actionForRentDetail()
  {

    $fetch_urls = BayutToRentDetailLinks::find()->where(['status'=>0])->asArray()->limit(20)->all();

    // echo "<pre>"; print_r($fetch_urls); echo "</pre>"; die();

    if ($fetch_urls<>null) {
      foreach($fetch_urls as $data){
        Yii::$app->db->createCommand()->update('bayut_to_rent_detail_links', ['status' => 2], ['url'=>$data['url']])->execute();
        $dataArr=[];
        try{
          $httpClient = new \Goutte\Client();
          $crawler = $httpClient->request('GET', $data['url']);
        }
        catch(Exception $err){
          $error = $err->getMessage(); print_r($error);
        }

        try {
          // Beds, Studio, Baths And Area Array
          $bed_bath_area = [];
          $crawler->filter('div._6f6bb3bc div.ba1ca68e._0ee3305d')->each(function($etcNode)use(&$bed_bath_area){
            $bed_bath_area[$etcNode->filter('span.cfe8d274')->attr('aria-label')] =  $etcNode->filter('span.fc2d1086')->text();
          });
          // echo "<pre>"; print_r($bed_bath_area); echo "</pre>"; //die();
        }
        catch ( Exception $err ) {
          $error = $err->getMessage();
          // echo "1st Catch<br>"; print_r($error); echo "<br>";
        }

        try {
          // Property Array
          $propertyInfoArr =  [];
          $i=1;
          $crawler->filter('ul._033281ab li')->each(function($propertyNode) use(&$propertyInfoArr, &$i) {
            $propertyInfoArr[$propertyNode->filter('span._3af7fa95')->text()] = $propertyNode->filter('span._812aa185')->text();
            $i++;
          });
        }
        catch (Exception $err) {
          $error = $err->getMessage();
          // echo "2nd Catch<br>"; print_r($error); echo "<br>";

          try{
            $j =1;
            $crawler->filter('ul._033281ab li')->each(function($propertyNode) use(&$propertyInfoArr, &$j, &$i) {
              if ($j==$i) {
                $propertyInfoArr["TruCheck ".$propertyNode->filter('div.f8dc1def span._405c89ee')->text()] = $propertyNode->filter('span._812aa185')->text();
              }
              if ($j>$i) {
                $propertyInfoArr[$propertyNode->filter('span._3af7fa95')->text()] = $propertyNode->filter('span._812aa185')->text();
              }
              $j++;
            });
          }
          catch(Exception $err){
            $error = $err->getMessage();
            // echo "3rd Catch<br>"; print_r($error); echo "<br>";
          }
        }

        try{
          $title = $crawler->filter('div._1f0f1758')->text();
          $titleExplode = explode(',', $title);
          $buildingTitle = $community = $sub_community = $city = '';
          if (isset($titleExplode[0]) AND $titleExplode[0]<>null) {
            $buildingTitle = $titleExplode[0];
          }
          if (isset($titleExplode[1]) AND $titleExplode[1]<>null) {
            $community = $titleExplode[1];
          }
          if (isset($titleExplode[3]) AND $titleExplode[3]<>null) {
            if (isset($titleExplode[2]) && $titleExplode[2]<>null) {
              $sub_community = $titleExplode[2];
            }
          }
          $city = array_pop($titleExplode);

          if ($data['property_category'] == 'residential') {
            $property_type = 1;
          }
          if ($data['property_category'] == 'commercial') {
            $property_type = 4;
          }

          $propertyDescription = $crawler->filter('div._2015cd68')->text();
          $listingAndFinalPrice = $crawler->filter('div.c4fc20ba span._105b8a67')->text();

        }
        catch(Exception $err){
          $error = $err->getMessage();
          // echo "4th Catch<br>"; print_r($error); echo "<br>";
        }

        $dataArr['url'] = $data['url'];

        if ($bed_bath_area<>null) {
          $bed_bath_area_studio_arr = bayutRentProperties::get_Bed_Bath_Area($bed_bath_area);
          if ($bed_bath_area_studio_arr<>null) {
            $dataArr['no_of_bedrooms'] = trim($bed_bath_area_studio_arr['no_of_bedrooms']);
            $dataArr['no_of_bathrooms'] = trim($bed_bath_area_studio_arr['no_of_bathrooms']);
            $dataArr['property_size'] = trim($bed_bath_area_studio_arr['area']);
            $dataArr['studio'] = trim($bed_bath_area_studio_arr['studio']);
          }
        }
        if ($propertyInfoArr<>null) {
          $propertyInfoArr = bayutRentProperties::getPropertyData($propertyInfoArr);
          if ($propertyInfoArr<>null) {
            $dataArr ['property_type'] = trim(strtolower($propertyInfoArr['type']));
            $dataArr ['purpose'] = trim($propertyInfoArr['purpose']);
            $dataArr ['ref_number'] = trim($propertyInfoArr['ref_number']);
            $dataArr ['truCheck_on'] = trim($propertyInfoArr['truCheck_on']);
            $dataArr ['added_on'] = trim($propertyInfoArr['added_on']);
          }
        }

        $dataArr['property_category'] = trim($property_type);
        $dataArr['building_title'] = trim($buildingTitle);
        $dataArr['community'] = trim($community);
        $dataArr['sub_community'] = trim($sub_community);
        $dataArr['city'] = trim($city);
        $dataArr['listing_price'] = trim(str_replace("," , "", $listingAndFinalPrice));
        $dataArr['desc'] = trim(strtolower($propertyDescription));

        $this->getForRentDecision($dataArr);
        // echo "DataStart:<pre>"; print_r($dataArr); echo "</pre>:DataEnd<br>"; die();
      }
    }
  }





  public function getForRentDecision($data=null)
  {
    // echo "<pre>"; print_r($data); echo "</pre>"; die();

    if ($data['building_title']<>null) {
      $city_id = Yii::$app->appHelperFunctions->getCityId($data['city']);
      // echo $city_id; //die();
      $commAndSubCommId = Yii::$app->propertyFinderHelperFunctions->getCommAndSubCommIds($data['community'],$data['sub_community']);
      // echo "<pre>"; print_r($commAndSubCommId); echo "</pre>"; die();

      $building = Buildings::find()->where([
        'title'         => trim($data['building_title']),
        // 'title'         => trim('Mudon Villas - Arabella 1 (Al Hebiah Sixth)'),
        'city'          => $city_id,
      ]);
      if ($commAndSubCommId<>null&&$commAndSubCommId['community_id']<>null&&$commAndSubCommId['sub_community_id']<>null) {
        $building->andWhere([
          'community'     => $commAndSubCommId['community_id'],
          'sub_community' => $commAndSubCommId['sub_community_id'],
        ]);
      }
      $building->andWhere(['city'=>$city_id]);
      $building->orWhere(['like', 'title', trim($data['building_title']) . '%', false]);
      // $building->orWhere(['like', 'title', trim('Mudon Villas - Arabella 1 (Al Hebiah Sixth)') . '%', false]);
      $model = $building->one();
      if ($model<>null) {
        // echo "<pre>"; print_r($model); echo "</pre>"; die();
        $this->saveForRentListingTransaction($data,$model);
      }else{
        // unlink($data['file_map']);
        echo " BuildingNotFound "; //die;
      }
    }
  }




  public function saveForRentListingTransaction($data=null,$model)
  {
    // echo "saveListingTransaction<pre>"; print_r($data); echo "</pre><br><br>";
    // // echo "saveListingTransaction<pre>"; print_r($model); echo "</pre><br><br>";
    // die();

    $desc = $data['desc'];

    $bedrooms = '';
    if ($data['no_of_bedrooms']<>null && is_numeric($data['no_of_bedrooms'])) {
      $bedrooms = $data['no_of_bedrooms'];
    }



    $findListing = ListingsRent::find()->where(['listings_reference'=>$data['ref_number'], 'listing_website_link'=>$data['url']])->one();
    if ($findListing==null) {
      $listingTransaction = new ListingsRent;
      $listingTransaction->listings_reference   = $data['ref_number']; //done
      $listingTransaction->source               = 15; //done
      $listingTransaction->listing_website_link = $data['url']; //done
      $listingTransaction->listing_date         = $data['added_on']; //done
      $listingTransaction->unit_number          = 'Not Known'; //done

      $listingTransaction->building_info = $model->id;

      // property category working
      $listingTransaction->property_category = $model->property_category; //$data['property_category'];

      //tenure working
      if ($model->tenure<>null) {
        $listingTransaction->tenure = $model->tenure;
      }

      // no of bedroom
      $listingTransaction->no_of_bedrooms = ($bedrooms<>null AND $bedrooms!='') ? $bedrooms : 0;
      $listingTransaction->land_size = $data['property_size'];;
      $listingTransaction->built_up_area = $data['property_size'];


      //Start Location Attributes
      $listingTransaction->location_highway_drive = ($model->location_highway_drive<>null) ? $model->location_highway_drive : 'minutes_5';
      $listingTransaction->location_school_drive  = ($model->location_school_drive<>null) ? $model->location_school_drive : 'minutes_5';
      $listingTransaction->location_mall_drive    = ($model->location_mall_drive<>null) ? $model->location_mall_drive : 'minutes_5';
      $listingTransaction->location_sea_drive     = ($model->location_sea_drive<>null) ? $model->location_sea_drive : 'minutes_5';
      $listingTransaction->location_park_drive    = ($model->location_park_drive<>null) ? $model->location_park_drive : 'minutes_5';
      //End Location Attributes

      // $listingTransaction->listings_price = trim($data['listing_price']);
      $listingTransaction->listings_rent = trim($data['listing_price']); //Yii::$app->propertyFinderHelperFunctions->getListingRent($data['property_size'],$bedrooms);
      $listingTransaction->final_price = trim($data['listing_price']);

      $listingTransaction->status = 2;
      if ($listingTransaction->save()) {
        echo " Saved Successfully ";
        Yii::$app->db->createCommand()
        ->update('bayut_to_rent_detail_links', [ 'status' => 1 ] , [ 'url' => $data['url'] ])
        ->execute();
      }
      if($listingTransaction->hasErrors()){
        foreach($listingTransaction->getErrors() as $error){
          if(count($error)>0){
            foreach($error as $key=>$val){
              echo $val. "<br>";
            }
          }
        }
        die();
      }

    }else{
      //  echo " listing already exists ";
    }
  }

}
