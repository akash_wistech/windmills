<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=coc;max_allowed_packet=64M',
            'username' => 'root',
            'password' => "YSqD6zeAaZMWEfTx",
            'charset' => 'utf8',
            'attributes' => [PDO::ATTR_CASE => PDO::CASE_LOWER],
        ],
	'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'IP_zstcYT6LrNZ7AsUzf_8Zz5Xt_EtX1',
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LcKvXIUAAAAANgCLHAPSqaC_sBzuc7c48gRRUNa',
            'secret' => '6LcKvXIUAAAAAOPs0yDafxmOhz6FRNXwQwHAnGEJ',
        ],
        /*'mailer' => [
            'class' => \YarCode\Yii2\Mailgun\Mailer::class,
            'domain => 'example.org',
            'apiKey => 'CHANGE-ME',
        ],*/
        'mailer' => [
            //'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.eu.mailgun.org',
                'username' => 'postmaster@cabinetofcuriosity.me',
                'password' => '4a40bb34516e585cdd9660ede750b22e-1f1bd6a9-ac2b4566',
                'port' => '587',
                'encryption' => 'tls',
            ],

            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.


            //'class' => '\YarCode\Yii2\Mailgun\Mailer',
            //'class' => \YarCode\Yii2\Mailgun\Mailer::class,
            //'domain' => 'cabinetofcuriosity.me',
            //'apiKey' => 'ad2b3f0646938e3faf519efd8bbcce73-bd350f28-08ff0c7c',
	    //'apiKey' => '5a1056f6f9bb0698839f3b364b439551-1f1bd6a9-db2c4702',

       'useFileTransport' => false,
        ],
        'sphinx' => [
            'class' => 'yii\sphinx\Connection',
            'dsn' => 'mysql:host=127.0.0.1;port=9306;',
            'username' => '',
            'password' => '',
        ],
    ],
];
