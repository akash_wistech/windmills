<?php
namespace app\commands;
use Yii;
use yii\console\Controller;

class BuildingController extends Controller
{
    public function actionIndex()
    {   
        $todayDate = date('Y-m-d');
        $threeMonthsLessDate = date("Y-m-d", strtotime("-3 months"));
        $models = \app\models\Buildings::find()
        ->asArray()
        ->all();

        foreach ($models as $key => $model) {
            
            $valuationCount = \app\models\Valuation::find()
            ->where(['building_info'=>$model['id']])
                ->andFilterWhere([
                    'between', 'created_at', $threeMonthsLessDate.' 00:00:00', $todayDate.' 23:59:59'
                ])
            ->count();
            
            if($valuationCount<3){
                echo "status changed". $model['id']."<br>";
                Yii::$app->db->createCommand()
                ->update('buildings', ['status' => 2], ['id' => $model['id']])
                ->execute();
            }    
        }
    }
}