<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Company;
use app\models\Valuation;

use yii\helpers\Url;
use yii;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AnnualValuationReminderController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */



    public function actionSendReminderEmail()
    {

        $clients = Company::find()
        ->where(['status' => 1, 'allow_for_valuation' => 1])
        ->andWhere(['or', ['client_type' => 'corporate'], ['client_type' => 'individual']])
        ->andWhere(['id' => 9166])
        ->all();

        if($clients !== null){ 

            $reminderSent = 0;
            $dateNotMatching = 0;

            $date_12months_ago = date('Y-m-d', strtotime('-12 months')). ' 23:59:59';
            
            foreach($clients as $key => $client){
                
                $valuations = Valuation::find()
                ->where(['client_id' => $client->id, 'valuation_status' => 5])
                ->orderBy(['id' => SORT_DESC])
                ->all();

                if (count($valuations) > 1 && $client->email_reminder_status != 1 && $client->primaryContact->email <> null ) {

                        
                        if ($valuations[0]->submission_approver_date <> null && strtotime($valuations[0]->submission_approver_date) <= strtotime($date_12months_ago)) {
                            
                            // echo $valuations[0]->client->id.'<br>'.$valuations[0]->reference_number.'<br>';
                            // echo $valuations[0]->submission_approver_date.'<br><br>';
                            // dd($valuations[0]->submission_approver_date, $date_12months_ago, $valuations);

                            $notifyData = [
                                'client' => $valuations[0]->client,
                                'attachments' => [],
                                // 'subject' => $email_subject,    
                                'replacements' => [
                                    '{wmReferenceNumber}' => $valuations[0]->reference_number,
                                    '{property}' => $valuations[0]->building->title,
                                    '{clientName}' => $valuations[0]->client->title,
                                    '{valuationDate}' => date('d-M-Y', strtotime($valuations[0]->submission_approver_date)),
                                ],
                            ];


                            if($client->client_type != "bank" ) {

                                 \app\modules\wisnotify\listners\NotifyEvent::fireAnnualReminderEmail('client.annual.valuation.reminder', $notifyData);

                            }
                            
                            Yii::$app->db->createCommand()->update(Company::tableName(), ['email_reminder_status' => 1, 'email_reminder_date' => date('Y-m-d H:i:s')], ['id' => $client->id])->execute();

                            $reminderSent += 1;
                        }else{
                            $dateNotMatching += 1;
                        }
                }
            }

            // echo "Reminder email sent = ".$reminderSent . "\n";
            // echo "Date not matching = ".$dateNotMatching . "\n";

        }

    }

    public function actionResetReminderEmail()
    {
        $clients = Company::find()
            ->where(['status' => 1, 'allow_for_valuation' => 1, 'email_reminder_status' => 1])
            ->andWhere(['or', ['client_type' => 'corporate'], ['client_type' => 'individual']])
            ->andWhere(['id' => 9166])
            ->all();

        if ($clients !== null) {
            foreach ($clients as $client) {
                $latestValuation = Valuation::find()
                    ->where(['client_id' => $client->id, 'valuation_status' => 5])
                    ->andWhere(['>', 'submission_approver_date', $client->email_reminder_date])
                    ->orderBy(['submission_approver_date' => SORT_DESC])
                    ->one();

                if ($latestValuation !== null) {
                    $date_12months_ago = date('Y-m-d', strtotime('-12 months'));

                    if (strtotime($latestValuation->submission_approver_date) <= strtotime($date_12months_ago . ' 23:59:59')) {
                        Yii::$app->db->createCommand()
                            ->update(Company::tableName(), ['email_reminder_status' => 0, 'email_reminder_date' => null], ['id' => $client->id])
                            ->execute();
                    }
                }
            }
        }
    }
}
