<?php ob_start();
defined('BASEPATH') or exit('No direct script access allowed');

class Receipts extends Admin_controller
{
    public function __construct()
    {
        ob_start();
        parent::__construct();

        $this->load->model('receipts_model');
        $this->load->model('invoices_model');
        $this->load->model('payments_model');
        $this->load->model('clients_model');
        $this->load->model('staff_model');
        $this->load->model('currencies_model');
        $this->load->model('projects_model');
        $this->load->helper('url');
        $this->load->model('cashadvance_model');
        $this->load->model('leads_model');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->model('customnotes_model');
        $this->load->library('zohoBooks');


    }

    /**
     * @param string $status
     */
    /* Get all invoices in case user go on index page */
    public function index($status = '')
    {

        if (!has_permission('receipts', '', 'view') && !has_permission('invoices', '', 'view_own')) {
            access_denied('receipts');
        }

        $data['canVerify'] = false;
        $data['canDesposit'] = false;
        $data['canHandover'] = false;

        $data['staff'] = $this->receipts_model->getAllStaff();

        $view = 'list_receipts';

        $where = array();
        $data['change_status'] = false;
        $data['reciept_owner'] = '';
        $data['receipt_created_by'] = '';
        $data['receipt_date'] = '';
        $data['receipt_cheque_date'] = '';
        $data['receipt_status'] = '';

        if ($status == "") {
            $status = 'created';
        } elseif ($status == 'handover') {
            $data['change_status'] = 'handover';
            $data['lang_heading'] = 'receipt_handover_title';
            $where['receipt_status'] = 'created';
        } elseif ($status == 'deposited') {
            $data['change_status'] = 'deposited';
            $data['lang_heading'] = 'receipt_deposit_title';
            $where['receipt_status'] = 'handover';
        } elseif ($status == 'verified') {
            $data['change_status'] = 'verified';
            $where['receipt_status'] = 'deposited';
            $data['lang_heading'] = 'receipt_verify_title';
        }

        if ($this->input->get('agent_id', TRUE)) {
            $agent_id = $this->input->get('agent_id', TRUE);
            $where['reciept_owner'] = $agent_id;
        }

        if ($this->input->get('status', TRUE)) {
            $status = $this->input->get('status', TRUE);
            $where['receipt_status'] = $status;
        }

        if ($this->input->post()) {

            if (!is_admin()) {
                $where['reciept_owner'] = $this->session->userdata['staff_user_id'];
                $where['receipt_created_by'] = $this->session->userdata['staff_user_id'];
            } else {

                if ($this->input->post('owner') != "") {
                    $where['reciept_owner'] = $this->input->post('owner');
                    $data['reciept_owner'] = $this->input->post('owner');
                }

                if ($this->input->post('created_by') != "") {
                    $where['receipt_created_by'] = $this->input->post('created_by');
                    $data['receipt_created_by'] = $this->input->post('created_by');
                }
            }

            if ($this->input->post('date') != "") {
                // $where['receipt_date'] = $this->input->post('date');
                $where['receipt_date'] = date('Y-m-d', strtotime($this->input->post('date')));
                $data['receipt_date'] = date('Y-m-d', strtotime($this->input->post('date')));
            }

            if ($this->input->post('cheque_date') != "") {
                //$where['receipt_cheque_date'] = $this->input->post('cheque_date');
                $where['receipt_cheque_date'] = date('Y-m-d', strtotime($this->input->post('cheque_date')));
                $data['receipt_cheque_date'] = date('Y-m-d', strtotime($this->input->post('cheque_date')));
            }

            if ($this->input->post('status') != "") {
                if(isset($_POST['status']) && $_POST['status'] == 'posted'){
                    $where['tblreciepts.zoho_id !='] = '';
                    $where['tblreciepts.adjustment !='] = 1;
                }else if(isset($_POST['status']) && $_POST['status'] == 'not_posted'){
                    $where['tblreciepts.zoho_id'] = null;
                    $where['tblreciepts.adjustment !='] = 1;
                }else {
                    $where['receipt_status'] = $this->input->post('status');
                }
                $data['receipt_status'] = $this->input->post('status');
            }

        } else {

            if (!is_admin()) {
                $where['reciept_owner'] = $this->session->userdata['staff_user_id'];
                $where['receipt_created_by'] = $this->session->userdata['staff_user_id'];
            }
        }

        //Set Permissions
        if (is_admin() || has_permission('receipt_verify', '', 'edit')) {
            $data['canVerify'] = true;
        }

        if (is_admin() || has_permission('receipt_deposit', '', 'edit')) {
            $data['canDesposit'] = true;
        }

        if (is_admin() || has_permission('receipt_handover', '', 'edit')) {
            $data['canHandover'] = true;
        }
        $data['invoiceid'] = '';
        /* if (is_numeric($id)) {
             $data['invoiceid'] = $id;
         }*/
        $datatable = array();

        $receipts = $this->receipts_model->get('', $where);

        foreach ($receipts as $value) {

            $created_by = '';
            $receipt = $value;

            if ($receipt <> null) {

                $rec_id = $receipt->receipt_id;

                if ($receipt->reciept_owner <> null) {
                    $reciept_owner = $this->receipts_model->staffNameById($receipt->reciept_owner);
                }

                if ($receipt->receipt_created_by <> null) {
                    $created_by = $this->receipts_model->staffNameById($receipt->receipt_created_by);
                }
                $row = array();

                $row[] = '<a  class="cpointor" onclick="init_reciept(' . $receipt->receipt_id . '); return false;">' . $receipt->receipt_num . '</a>';
                $row[] = date('d-m-Y', strtotime($value->receipt_date));
                $row[] = '<a target="_blank" href="/admin/clients/client/' . $value->receipt_client_id . '">' . $value->client_name . '</a><br>' . $value->client_phone;
                $row[] = $value->receipt_slip_no;
                $row[] = $value->receipt_amount;
                $row[] = $value->receipt_type;
                $row[] = date('d-m-Y', strtotime($value->receipt_cheque_date));
                $row[] = $value->receipt_note;

                $status = '';
                $status .= '<div class="invoice_status_li-' . $rec_id . '"><span class="label ';
                if ($receipt->receipt_status == 'handover') {
                    $status .= 'label-success';
                } elseif ($receipt->receipt_status == 'deposited') {
                    $status .= 'label-info';
                } elseif ($receipt->receipt_status == 'verified') {
                    $status .= 'label-warning';
                } else {
                    $status .= 'label-default';
                }
                $status .= ' s-status">';
                $status .= $receipt->receipt_status;
                $status .= ' </span>';
                if ($receipt->receipt_status == 'verified') {
                    $verify_date = '';
                    if ($receipt->verify_date <> null) {
                        $verify_date = date('d-m-Y', strtotime($receipt->verify_date));
                    }
                    $status .= '<h5 style="padding-left: 7px;">' . $verify_date . '</h5> </div>';
                }
                $row[] = $status;
                $zoho_status='';
                if ($receipt->adjustment != 1 && $receipt->adjustment != 2 && $receipt->adjustment != 3) {
                    if ($receipt->zoho_id != '') {
                        $zoho_status = '<a id="zoho_disabled" class="pull-right btn btn-default btn-with-tooltip" data-toggle="tooltip"
                       title="Posted" data-placement="bottom"
                       style="margin-right: 5px;"><i class="fa fa-clipboard"> Posted</i>
                    </a>';
                    } else {
                        $zoho_status = '<a id="post_to_zoho" class="pull-right btn btn-success post_to_zoho btn-with-tooltip"
                         data_id="' . $rec_id . '" data-toggle="tooltip"
                         title="Post To Zoho" data-placement="bottom"
                         style="margin-right: 5px;"><i class="fa fa-clipboard"> Post</i>
                         </a>';

                    }
                }
                $row[] = $zoho_status;
                $adjustment = '';
                if($receipt->adjustment == 1){
                    $adjustment = '<span class="label label-default">Adjustment</span>';
                }else if($receipt->adjustment == 2){
                    $adjustment = '<span class="label label-info">Out of Book</span>';
                }else if($receipt->adjustment == 3){
                    $adjustment = '<span class="label label-warning">Bad Debts</span>';
                }
                $row[] = $adjustment;

                $actions = '';
                $actions .= '<div class="btn-group">
                            <button class="label label-default-light dropdown-toggle"
                             data-toggle="dropdown">
                            Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="' . base_url() . 'admin/receipts/#/' . $rec_id . '"
                                                           id="' . $rec_id . '"
                                                           class="text text-primary" target="_blank">Preview</a>
                                    </li>
                                    <li>
                                        <a href="' . base_url() . 'admin/receipts/update/' . $rec_id . '"
                                            id="' . $rec_id . '"
                                            class="text text-primary" target="_blank">Edit
                                         </a>
                                    </li>';
                if (is_admin()) {
                    $actions .= '<li><a href="#" id="' . $rec_id . '" class="delete text text-danger">Delete</a> </li>';
                }
                $actions .= '<li>
                                <a href="javascript:void(0)" id="' . $rec_id . '"
        
                                 class="status_change_status text text-danger">Status <span class="glyphicon glyphicon-chevron-right pull-right"></span></a>
        
                                  <ul class="dropdown_custom">
                                   <li id="handover_' . $rec_id . '"><a href="javascript:void(0)">Handover</a></li>
                                   <li id="deposited_' . $rec_id . '"><a href="javascript:void(0)">Deposited</a></li>
                                   <li id="verified_' . $rec_id . '"><a href="javascript:void(0)">Verified</a></li>
                                   <li id="created_' . $rec_id . '"><a href="javascript:void(0)">Created</a></li>
                                   </ul>
                                </li>

                     </ul>
                </div>';


                $row[] = $actions;


                if (is_admin()) {

                    $row[] = ($reciept_owner <> null) ? $reciept_owner->firstname . ' ' . $reciept_owner->lastname : '';
                    $row[] = ($created_by <> null) ? $created_by->firstname . ' ' . $created_by->lastname : '';
                }
                $datatable[] = $row;
            }
        }
        /* $output = array(
             "data" => $datatable,
         );*/


        $data['receipts_data1'] = json_encode($datatable);

        $data['receipts'] = $this->receipts_model->get('', $where);
        $this->load->view('admin/receipts/' . $view, $data);

    }

    /* Get all invoices in case user go on index page */
    public function create()
    {

        if (!has_permission('receipts', '', 'create')) {
            access_denied('receipts');
        }

        $data['clients'] = $this->clients_model->get();
        $data['currencies'] = $this->currencies_model->get();
        $default_currency = $this->currencies_model->get_base_currency();
        $data['default_currency'] = $default_currency->id;
        $data['projects'] = $this->projects_model->get();
        $data['staff'] = $this->staff_model->get('', ['active' => 1]);
        $data['receipt_num'] = $this->receipts_model->makeReceiptNumber();
        $owner = $this->session->userdata['staff_user_id'];


        if ($this->input->post()) {

            $post = $this->input->post();
            // pre_array($post['data']['date']);
            // INSERT DATA

            $receipt_id = $this->receipts_model->insert($post['data']);


            $receipt_amount = $post['data']['amount'];
            $client = $post['data']['client_id'];

            $paid = 0;
            $advance = 0;
            $withdraw = 0;

            // IF insert successful
            if ($receipt_id) {
                // pre_array($post);
                if (isset($post['invoice'])) {

                    foreach ($post['invoice'] as $key => $inovice) {
                        $inv = $post['invoice'][$key];
                        $inv['do_not_send_email_template'] = 1;
                        // IF invoice in paid and amount greater than 0
                        if ($inv['amount'] > 0) {

                            unset($inv['total']);
                            unset($inv['amount_due']);
                            // unset($inv['discount']);
                            $inv['receipt_id'] = $receipt_id;
                            $paid = $paid + $inv['amount'];
                            $changeToTax = false;

                            if (isset($inv['change_to_tax_invoice'])) {
                                $changeToTax = true;
                                unset($inv['change_to_tax_invoice']);
                            }
                            $inv['date'] = $post['data']['date'];
                            $inv['client_id'] = $post['data']['client_id'];
                            $inv['adjustment'] = $post['data']['adjustment'];

                            $payment = $this->payments_model->process_payment($inv, '');

                            if ($payment && $changeToTax) {
                                if($post['data']['adjustment'] == 1){
                                    $changeToTax = false;
                                }
                                if ($changeToTax) {

                                    $update_data['number'] = make_next_invoice_num();
                                    $update_data['prefix'] = get_option('invoice_prefix');
                                    $update_data['type'] = 'invoice';
                                  //  $update_data['date'] = date('Y-m-d');
                                    $update_data['date'] = date('Y-m-d', strtotime($post['data']['date']));
                                    $this->db->where('id', $inv['invoiceid']);
                                    $this->db->update('tblinvoices', $update_data);
                                    //$unv = $this->createInvoice($inv['invoiceid']);
                                    // $this->create_zoho($inv['invoiceid']);

                                }
                            }
                        }
                    }
                }

                //$this->createReceipt($receipt_id);
            }
            // pre_array($receipt_id);
            // Get total advance remaining
            $balance = $this->get_clients_advance_cash($client);

            // If client choose the advance to use
            if (isset($post['data']['use_advance']) && $post['data']['use_advance'] > 0) {
                $withdraw = $post['data']['use_advance'];
                $remaining = $balance - $withdraw;
                $this->AddCashAdnvance($receipt_id, $client, $advance, $withdraw, $remaining, $owner);
            }

            // If client choose to add advance to use later
            if (isset($post['data']['add_advance']) && $post['data']['add_advance'] > 0) {
                $advance = $post['data']['add_advance'];
                $remaining = $balance + $advance;
                $this->AddCashAdnvance($receipt_id, $client, $advance, $withdraw, $remaining, $owner);
            }

            // redirect('admin/receipts/details/' . $receipt_id, 'refresh');
            redirect('admin/receipts/#' . $receipt_id, 'refresh');
        }

        $this->load->view('admin/receipts/create', $data);
    }

    /**
     * @param string $id
     */
    public function update($id = '')
    {

        if (!has_permission('receipts', '', 'edit')) {
            access_denied('receipts');
        }

        if (!is_admin()) {
            redirect('admin/', 'refresh');
        }

        if ($this->input->post()) {

            $post = $this->input->post();
            $update = $this->receipts_model->update($post);

            // redirect('admin/receipts/details/' . $id, 'refresh');
            redirect('admin/receipts/#' . $id, 'refresh');
        }

        $data['clients'] = $this->clients_model->get();
        $data['currencies'] = $this->currencies_model->get();
        $default_currency = $this->currencies_model->get_base_currency();
        $data['default_currency'] = $default_currency->id;
        $data['projects'] = $this->projects_model->get();
        $data['staff'] = $this->staff_model->get('', 1);

        $where['tblreciepts'] = $id;
        $data['receipts'] = $this->receipts_model->getbyId($id);

        $data['receipt_id'] = $id;
        $data['invoices'] = $this->invoices_model->get_all_receipts_invoices($id);
        $data['cashAdvance'] = $this->receipts_model->getCashAdvanceByReceipt($id);

        $advance = $this->get_clients_advance_cash($data['receipts']->receipt_client_id);
        if ($advance > 0) {
            $data['receipts']->advance_amount = $advance;
        } else {
            $data['receipts']->advance_amount = 0;
        }

        $this->load->view('admin/receipts/update', $data);
    }

    /**
     * @param $id
     */
    public function details($id)
    {
        $template_name = "receipt-send-to-client";

        $dataReceipts = $this->receipts_model->getbyId($id);

        if ($dataReceipts == null) {
            redirect('admin/receipts/', 'refresh');
        }

        $dataReceipts->clientid = $dataReceipts->receipt_client_id;

        $client = $this->clients_model->get($dataReceipts->receipt_client_id);
        $data['cashAdvance'] = $this->receipts_model->getCashAdvanceByReceipt($id);

        $dataReceipts->client = $client;
        $dataReceipts->billing_street = $client->billing_street;
        $dataReceipts->billing_city = $client->billing_city;
        $dataReceipts->billing_state = $client->billing_state;
        $dataReceipts->billing_zip = $client->billing_zip;
        $dataReceipts->billing_country = $client->billing_country;

        $data['receipts'] = $dataReceipts;
        $data['invoices'] = $this->invoices_model->get_all_receipts_invoices($id);
        $data['staff'] = $this->staff_model->get($dataReceipts->reciept_owner);

        $contact = $this->clients_model->get_contact(get_primary_contact_user_id($dataReceipts->clientid));
        $email = '';
        if ($contact) {
            $email = $contact->email;
        }

        $data['template'] = get_email_template_for_sending($template_name, $email);

        $data['template_name'] = $template_name;

        $data['template_name'] = $template_name;
        $this->db->where('slug', $template_name);
        $this->db->where('language', 'english');
        $template_result = $this->db->get('tblemailtemplates')->row();

        $data['template_system_name'] = $template_result->name;
        $data['template_id'] = $template_result->emailtemplateid;

        $data['template_disabled'] = false;
        if (total_rows('tblemailtemplates', array('slug' => $data['template_name'], 'active' => 0)) > 0) {
            $data['template_disabled'] = true;
        }

        $this->load->view('admin/receipts/receipt_preview_template', $data);
    }

    /**
     * @param $receipt_id
     * @param $client
     * @param string $advance
     * @param string $withdraw
     * @param $remaining
     * @param $owner
     * @return mixed
     */
    public function AddCashAdnvance($receipt_id, $client, $advance = '', $withdraw = '', $remaining, $owner)
    {
        $owner = $this->session->userdata['staff_user_id'];

        $table = [
            'receipt_id' => $receipt_id,
            'client_id' => $client,
            'amount' => $advance,
            'withdraw' => $withdraw,
            'remaining_advance' => $remaining,
            'date' => date('Y-m-d H:i:s'),
            'created_by' => $owner,
        ];

        return $this->cashadvance_model->insert($table);
    }

    /**
     * @param $id
     */
    /* Generates invoice PDF and senting to email of $send_to_email = true is passed */
    public function pdf($id)
    {

        $dataReceipts = $this->receipts_model->getbyId($id);

        $dataReceipts->clientid = $dataReceipts->receipt_client_id;

        $client = $this->clients_model->get($dataReceipts->receipt_client_id);

        $dataReceipts->client = $client;
        $dataReceipts->billing_street = $client->billing_street;
        $dataReceipts->billing_city = $client->billing_city;
        $dataReceipts->billing_state = $client->billing_state;
        $dataReceipts->billing_zip = $client->billing_zip;
        $dataReceipts->billing_country = $client->billing_country;
        $_pdf_receipt['receipts'] = $dataReceipts;


        $invoices = $this->invoices_model->get_all_receipts_invoices($id);

        foreach ($invoices as $invoice) {

            $invoice->total_amount = $this->receipts_model->getInvoicesTotal($invoice->invoiceid)->total;
            $inv_data = $this->invoices_model->get($invoice->invoiceid);
            $invoice->subject = $inv_data->subject;
            $invoice->date = $inv_data->date;
            $_pdf_receipt['invoices'][] = $invoice;

        }
        $_pdf_receipt['staff'] = $this->staff_model->get($dataReceipts->reciept_owner);

        $_pdf_receipt['created_by'] = $this->staff_model->get($dataReceipts->receipt_created_by);
        ob_end_clean();

        try {
            $pdf = receipt_pdf($_pdf_receipt);
        } catch (Exception $e) {
            $message = $e->getMessage();
            echo $message;
            if (strpos($message, 'Unable to get the size of the image') !== FALSE) {
                show_pdf_unable_to_get_image_size_error();
            }
            die;
        }

        $type = 'D';
        if ($this->input->get('print')) {
            $type = 'I';
        }

        $pdf_name = format_receipt_number($dataReceipts->receipt_num);

        $pdf->Output($pdf_name . '.pdf', $type);
    }

    /**
     * @param $id
     */
    public function delete()
    {
        if ($this->input->post()) {
            if ($this->receipts_model->delete_receipt_payments($this->input->post('receipt_id'))) {
                $this->receipts_model->delete_receipt($this->input->post('receipt_id'));
                redirect('admin/receipts/', 'refresh');
            }
        }
    }

    /**
     * @param string $status
     */
    public function updateStatus($status = '')
    {
        if ($this->input->post()) {

            if (count($this->input->post('receipt')) > 0) {
                foreach ($this->input->post('receipt') as $receipt) {
                    if (isset($receipt['status']) && isset($receipt['id'])) {
                        if ($receipt['id'] != "" && $receipt['status'] != "") {
                            $this->receipts_model->updateStatus($receipt['id'], $receipt['status']);
                        }
                    }
                }
                redirect('admin/receipts/', 'refresh');
            }

            if ($this->input->post('changeStatus')) {
                echo $this->receipts_model->updateStatus($this->input->post('id'), $this->input->post('status'));
            }
        }
    }

    /**
     * @param string $status
     */
    public function updateStatusVerify($status = '')
    {
        if ($this->input->post()) {

            if (count($this->input->post('receipt')) > 0) {
                foreach ($this->input->post('receipt') as $receipt) {
                    if (isset($receipt['status']) && isset($receipt['id'])) {
                        if ($receipt['id'] != "" && $receipt['status'] != "") {
                            $this->receipts_model->updateStatus($receipt['id'], $receipt['status']);
                        }
                    }
                }
                redirect('admin/receipts/', 'refresh');
            }

            if ($this->input->post('changeStatus')) {
                echo $this->receipts_model->updateStatusVerify($this->input->post('id'), $this->input->post('status'), $this->input->post('verify_date'));
            }
        }
    }

    /**
     * @param $client
     */
    public function clients_invoices($client)
    {
        if ($client) {

            $data = $this->invoices_model->get_all_Customer_invoices($client);
            // $data_read = $this->invoices_model->get_all_Customer_proforma_invoices($client);
            $data_read = [];


            $total_due = 0;
            $total_payable = 0;
            $html = '';
            $html_total = '';

            if ($data <> null) {

                $i = 0;
                foreach ($data as $item) {

                    $statusAccepted = true;
                    /* $invoice_accepted = get_invoice_status($item['id']);

                     if ($invoice_accepted <> null) {
                         if ($invoice_accepted->invst_status <> 'accepted') {
                             $statusAccepted = false;
                         }
                     }*/

                    if ($item['status'] == 6) {
                        $disable = 'disabled="true"';
                    } else {
                        $disable = '';
                    }

                    // $amount_left = $item['total'] - $item['discount_total'];

                    $amount_due = get_invoice_total_left_to_pay($item['id'], $item['total']);

                    if ($amount_due > 0) {

                        // if ($item['type'] == "invoice") {

                        $total_payable = $item['total'] + $total_payable;
                        $total_due = $amount_due + $total_due;
                        $to_pay = get_invoice_total_left_to_pay($item['id'], $item['total']);

                        // }

                        $html .= '<tr><input name="invoice[' . $i . '][paymentmode]" type="hidden" value="1"/>';

                        $html .= '<td>';

                        if ($item['type'] == "performa") {

                            if(is_admin()){
                                $html .= '<input  name="invoice[' . $i . '][change_to_tax_invoice]" type="checkbox" checked="checked" value="1" />';
                            }else{
                                $html .= '<input style="display:none;" name="invoice[' . $i . '][change_to_tax_invoice]" type="checkbox" checked="checked" value="1" />';
                            }
                        }
                        $html .= '</td>';

                        $html .= '<td><input name="invoice[' . $i . '][paymentmethod]" type="hidden" value="' . date("
                                             Y-m-d H:i:s") . '" />' . $item['date'] . '</td>';

                        if ($item['type'] == "invoice") {

                            $html .= '<td><input name="invoice[' . $i . '][invoiceid]" type="hidden"
                                             value="' . $item['id'] . '"/><a href="' . admin_url('/invoices/list_invoices#' . $item['id']) . '" target="_blank">' . format_invoice_number($item['id']) . '</a></td>';
                        } else {
                            $html .= '<td><input name="invoice[' . $i . '][invoiceid]" type="hidden"
                                             value="' . $item['id'] . '"/><strike><a style="color:#cc6600;" href="' . admin_url('/invoices/list_invoices#' . $item['id']) . '" target="_blank">' . format_invoice_number($item['id']) . '</a></strike></td>';
                        }

                        $html .= '<td><input ' . $disable . ' name="invoice[' . $i . '][total]" class="form-control" type="text"
                                             value="' . $item['total'] . '" style="width: 100px;"/></td>';
                        $html .= '<td><input id="amount_due_' . $item['id'] . '" ' . $disable . ' name="invoice[' . $i . '][amount_due]" class="form-control" type="text"
                                             value="' . $amount_due . '" style="width: 100px;"/></td>';
                        $html .= '<td><input ' . $disable . ' name="invoice[' . $i . '][discount]" class="form-control" type="text"
                                             value="0" min="0" max="' . $amount_due . '" style="width: 100px;"/></td>';
                        $html .= '<td><input ' . $disable . ' name="invoice[' . $i . '][amount]" class="payment_amount form-control"
                                             id="_' . $item['id'] . '" type="text" value="0" style="width: 100px;"/></td>';
                        $html .= '</tr>';

                        $i++;
                    }

                }

                $html_total .= '<tr>';
                $html_total .= '<td>&nbsp;</td>';
                $html_total .= '<td>&nbsp;</td>';
                $html_total .= '<td><h5>Total Payable: ' . $total_payable . '</h5></td>';
                $html_total .= '<td><h5>Total Due: ' . $total_due . '</h5></td>';
                $html_total .= '<td>&nbsp;</td>';
                $html_total .= '<td><h5>Total Amount: <span id="amount_total">0</span></h5></td>';
                $html_total .= '</tr>';

            }

            if ($data <> null) {
                $html .= $html_total;
            }

            print_r(json_encode(['html' => $html, 'total_payable' => $total_payable, 'amount_due' => $total_due]));
        }
    }

    /**
     * @param $client
     */
    public function get_clients_advance_cash($client)
    {
        echo $this->cashadvance_model->get_total_advance_amount($client);
    }

    /**
     *
     */
    /* Send invoiece to email */
    public function send_to_email($id)
    {
        ob_start();

        if (!has_permission('receipts', '', 'view') && !has_permission('receipts', '', 'view_own')) {
            access_denied('receipts');
        }
        $success = $this->receipts_model->send_receipt_to_client($id, '', $this->input->post('attach_pdf'), $this->input->post('cc'), $this->input->post('subject'));
        // pre_array($success);
        // In case client use another language
        load_admin_language();
        if ($success) {
            set_alert('success', _l('invoice_sent_to_client_success'));
        } else {
            set_alert('danger', _l('invoice_sent_to_client_fail'));
        }

        redirect(admin_url('receipts/#' . $id));
    }

    public function convert_performa_invoices_to_tax_invoice()
    {

        if ($this->input->post()) {

            $id = $this->input->post('invoice_id');

            $inv_data = $this->invoices_model->get($id);;
            $update_data['pinv_reference'] = $inv_data->number;
            $update_data['number'] = make_next_invoice_num();
            $update_data['prefix'] = get_option('invoice_prefix');
            $update_data['type'] = 'invoice';
            $update_data['date'] = date('Y-m-d');

            $this->db->where('id', $id);
            $update = $this->db->update('tblinvoices', $update_data);

            if ($update) {


                //$this->create_zoho($id);
                //$inv = $this->createInvoice($id);

                echo 1;
                return;
            }

            echo 0;
            return;

        }
    }

    public function get_ajax_client_contacts()
    {

        if ($this->input->post()) {

            $selected = array();
            $contacts = $this->clients_model->get_contacts($this->input->post('client'));

            foreach ($contacts as $contact) {
                if (has_contact_permission('invoices', $contact['id'])) {
                    array_push($selected, $contact['id']);
                }
            }

            if (count($selected) == 0) {
                echo '<p class="text-danger">' . _l('sending_email_contact_permissions_warning', _l('customer_permission_invoice')) . '</p><hr />';
            }

            echo render_select('SaveSend[sent_to][]', $contacts, array('id', 'email', 'firstname,lastname'), 'invoice_estimate_sent_to_email', $selected, array('multiple' => true), array(), 'form', '', false);

        }

    }

    /* Get all invoices in case user go on index page */
    public function create_zoho($id)
    {
        $this->load->library('form_validation');
        $data['staff'] = $this->staff_model->get('', 1);

        if ($id) {
            $errors = [];
            $res_data = "";

            // find invoice for given date
            $where['id ='] = $id;
            $where['type'] = 'invoice';

            //$invoices = $this->invoices_model->get('', $where);
            $invoice = $this->invoices_model->get($id);

            // Initialize Zoho API Class
            $zb = new ZohoBooks();


            if (empty($invoice->zoho_id)) {

                // Step 1: get or create client/customer in CRM
                $client_id = "";
                $client = $this->clients_model->get($invoice->clientid);

                if ($client <> null) {
                    if (!empty($client->vat)) {
                        $invoice->vat_reg_no = $client->vat;
                        $invoice->vat_treatment = "vat_registered";
                    } else {
                        $invoice->vat_reg_no = $client->vat;
                        $invoice->vat_treatment = "vat_not_registered";
                    }

                    if (!empty($client->zoho_id)) {
                        $client_id = $invoice->clientid = $client->zoho_id;
                    } else {
                        $client_id = $client->userid;
                    }
                }

                $customer = json_decode($zb->getContact($client_id));

                if ($customer == null || empty($customer)) {

                    $contact = $this->createContactData($invoice->clientid);

                    if (!empty($contact) && count($contact) > 0 && $contact <> null) {

                        $contact = $zb->postContact(json_encode($contact));
                        $result = json_decode($contact);

                        if ($result <> null) {
                            if ($result->code == 0) {
                                $contact_id_zoho = $result->contact->contact_id;
                                // $this->clients_model->updateZohoId($client_id, $contact_id_zoho);
                                update_zoho_id('tblclients', 'userid', $client_id, 'zoho_id', $contact_id_zoho);
                                $invoice->clientid = $contact_id_zoho;
                            } else {
                                $client = $this->clients_model->get($invoice->clientid);

                                if ($client <> null && !empty($client)) {
                                    $invoice->clientid = $client->zoho_id;
                                }
                            }
                        }
                    }
                }


                $invoiceJson = json_encode($this->invoiceJson((array)$invoice));
                $invoice_data = json_decode($zb->postInvoice($invoiceJson));

                if (!empty($invoice_data) && $invoice_data <> null) {

                    if (isset($invoice_data->code)) {

                        if ($invoice_data->code == 0) {
                            //items table
                            $this->db->where('rel_id', $invoice['id']);
                            $this->db->update('tblitems_in', array('zoho_id' => $invoice_data->invoice->invoice_id));
                            //invoice table
                            $this->db->where('id', $invoice['id']);
                            $this->db->update('tblinvoices', array('zoho_id' => $invoice_data->invoice->invoice_id));
                        }

                        /*  $res_data .= "<div class='alert alert-danger'>";
                          $res_data .= " Code:" . $invoice_data->code;
                          $res_data .= " Message:" . $invoice_data->message;
                          $res_data .= " Invoice Number:" . strip_tags($invoice['prefix'] . $invoice['number']);
                          $res_data .= "</div>";

                          $errors[]['code'] = $invoice_data->code;
                          $errors[]['message'] = $invoice_data->message;
                          $errors[]['invoice_id'] = strip_tags($invoice['prefix'] . $invoice['number']);*/
                    }
                }
            }

            //  sleep(20);


            //   print_r($res_data);
            //return;
            // $this->load->view('admin/sync_invoices/create', $data);

        }

        //$this->load->view('admin/sync_invoices/create', $data);
    }

    /**
     * @param $invoice_data
     * @return array
     */
    protected function invoiceJson($invoice_data)
    {

        $items = $this->invoices_model->get_invoice_items($invoice_data['id']);



        $line_items = [];
        $i = 0;
        $per_item_discount =0;
        if ($items <> null && count($items) > 0) {
            /*if($invoice_data['vat_treatment'] != 'vat_registered') {
                $total_items = count($items);
                if ($invoice_data['discount_total'] > 0) {
                    $per_item_discount = $invoice_data['discount_total'] / $total_items;
                    $per_item_discount = number_format((float)$per_item_discount, 2, '.', '');
                }
            }*/
            $total_items = count($items);
            $discount_type_flag = 0;
            if ($invoice_data['discount_total'] > 0) {
                $per_item_discount = $invoice_data['discount_total'] / $total_items;
                $per_item_discount = number_format((float)$per_item_discount, 2, '.', '');
                $discount_type_flag = 1;
            }

            foreach ($items as $item) {

                if ($item['zoho_id'] == "") {
                    $item_id_zoho = $this->getZohoItemId($item);
                } else {
                    $item_id_zoho = $item['zoho_id'];
                }

                $line_items[$i] = [
                    "item_id" => $item_id_zoho,
                    "project_id" => "",
                    "name" => strip_tags($item['description']),
                    "description" => strip_tags($item['long_description']),
                    "item_order" => strip_tags($item['item_order']),
                    "bcy_rate" => strip_tags($item['rate']),
                    "rate" => strip_tags($item['rate']),
                    "quantity" => strip_tags($item['qty']),
                    "unit" => strip_tags($item['unit']),
                    "discount_amount" => $per_item_discount,
                    "discount" => $per_item_discount,
                /*"discount_amount" => strip_tags($item['discount']),
                    "discount" => strip_tags($item['discount'])*/
                ];

                // get Item Tax
                $item_taxes = get_invoice_item_taxes($item['id']);

                if (count($item_taxes) > 0) {
                    foreach ($item_taxes as $taxes) {

                        if (strpos($taxes['taxname'], 'VAT|5.00') !== false) {
                            $line_items[$i]['tax_id'] = get_option('zoho_vat_id');
                            $line_items[$i]['tax_name'] = "VAT";
                            $line_items[$i]['tax_type'] = "tax";
                            $line_items[$i]['tax_percentage'] = $taxes['taxrate'];

                        }
                    }
                }
                $i++;
            }
        }

        if (count($invoice_data) > 0) {

            $sales_agent_name = "";
            $staff = $this->staff_model->get($invoice_data['sale_agent']);

            if ($staff != "" && $staff <> null) {

                $sales_agent_name = "";

                if ($staff->firstname <> null) {
                    $sales_agent_name = $staff->firstname;
                }

                if ($staff->lastname <> null) {
                    $sales_agent_name .= " " . $staff->lastname;
                }
            }
            $discount_type = 'entity_level';
            if($invoice_data['vat_treatment'] == 'vat_registered'){
                $discount_type = 'item_level';
            }else if($discount_type_flag == 1){
                $discount_type = 'item_level';
            }
            if($invoice_data['discount_type'] != "before_tax"){
                $discount_type = 'entity_level';
            }
           // $discount_type = 'item_level';
            $place_of_supply = "";
            $tax_treatment = "";

            if(strtotime($invoice_data['date']) >= strtotime('2018-01-01')){
                $place_of_supply =  $invoice_data['place_of_supply'];
                $tax_treatment = $invoice_data['vat_treatment'];
            }



            $invoice = [

                "customer_id" => $invoice_data['clientid'],
                "invoice_number" => strip_tags($invoice_data['prefix'] . $invoice_data['number']),
                //"place_of_supply" => $invoice_data['place_of_supply'],
                "place_of_supply" => $place_of_supply,
                "reference_number" => $invoice_data['id'],
                "date" => $invoice_data['date'],
                "due_date" => $invoice_data['date'],
                /* "due_date" => $invoice_data['duedate'],*/
               // "discount" => $invoice_data['discount_total'],
                "discount" => $invoice_data['discount_total'],
                // "is_discount_before_tax" => ($invoice_data['discount_calculation'] == "before_tax") ? true : false,
                "is_discount_before_tax" => ($invoice_data['discount_type'] == "before_tax") ? true : false,
                "discount_type" => $discount_type,
                "is_inclusive_tax" => false,
                "salesperson_name" => $sales_agent_name,
                "project_id" => $invoice_data['project_id'],
                "custom_body" => " ",
                "custom_subject" => " ",
                "notes" => strip_tags($invoice_data['clientnote']),
                "terms" => strip_tags($invoice_data['terms']),
                "shipping_charge" => 0,
                "adjustment" => 0,
                "adjustment_description" => " ",
                "reason" => " ",
                "expense_id" => " ",
                //"tax_treatment" => $invoice_data['vat_treatment'],
                "tax_treatment" => $tax_treatment,
                "line_items" => $line_items

            ];
        }

        return $invoice;

    }

    /**
     * @param $item
     * @return mixed
     */
    public function getZohoItemId($item)
    {
        if (count($item) > 0) {

            $data = [
                "name" => strip_tags($item['id']) . "-" . $item['description'],
                "rate" => $item['rate'],
                "description" => strip_tags($item['long_description']),
                "sku" => strip_tags($item['id']),
            ];

            $zb = new ZohoBooks();
            $do_item = json_decode($zb->postItems(json_encode($data)));

            if (!empty($do_item) && count($do_item) > 0 && $do_item <> null) {
                if ($do_item->code == 0) {

                    $item_id = $do_item->item->item_id;
                    //### update zoho Id
                    $this->db->where('id', $item['id']);
                    $this->db->update('tblitems_in', array('zoho_id' => $item_id));

                    return $item_id;
                }

            }
        }

    }

    /**
     * @param $client_id
     * @return array
     */
    protected function createContactData($client_id)
    {

        $client = $this->clients_model->get($client_id);
        $contact = [];

        if ($client <> null && !empty($client)) {

            $client_contacts = $this->clients_model->get_contacts($client_id);

            $primary_first_name = "";
            $primary_last_name = "";
            $primary_email = "";
            $tax_treatment = "vat_not_registered";

            $contacts = [];

            if (count($client_contacts) > 0) {

                foreach ($client_contacts as $contact) {

                    if ($contact['is_primary']) {
                        $primary_first_name = $contact['firstname'];
                        $primary_last_name = $contact['lastname'];
                        $primary_email = $contact['email'];
                    }

                    $contacts[] = [

                        "salutation" => substr($contact["title"],20),
                        "first_name" => $contact['firstname'],
                        "last_name" => $contact['lastname'],
                        "email" => $contact['email'],
                        "phone" => $contact['phonenumber'],
                        "mobile" => $contact['email'],
                        "designation" => $contact["title"],
                        "department" => "",
                        "skype" => "",
                       /* "is_primary_contact" => ($contact['is_primary']) ? 'true' : 'false',*/
                        "enable_portal" => ($contact['is_primary']) ? true : false
                    ];
                }
            }

            if (!empty($client->vat)) {
                $tax_treatment = "vat_registered";
            }

            $contact = [

                "contact_name" => $client->company,
                "company_name" => $client->company,
                "first_name" => $primary_first_name,
                "last_name" => $primary_last_name,
                "email" => $primary_email,
                "phone" => $client->phonenumber,
                "facebook" => "",
                "twitter" => "",
                "tax_reg_no" => $client->vat,
                "tax_treatment" => $tax_treatment,
                "billing_address" => [
                    "attention" => $client->company,
                    "address" => strip_tags($client->address),
                    "street2" => "",
                    "state_code" => "",
                    "city" => $client->city,
                    "state" => $client->state,
                    "zip" => $client->zip,
                    "country" => (get_country($client->country) <> null) ? get_country($client->country)->long_name : "",
                    "fax" => $client->phonenumber,
                    "phone" => $client->phonenumber
                ],
                "shipping_address" => [
                    "attention" => $client->company,
                    "address" => strip_tags($client->address),
                    "street2" => "",
                    "state_code" => "",
                    "city" => $client->city,
                    "state" => $client->state,
                    "zip" => $client->zip,
                    "country" => (get_country($client->country) <> null) ? get_country($client->country)->long_name : "",
                    "fax" => $client->phonenumber,
                    "phone" => $client->phonenumber
                ],
                "contact_persons" => $contacts
            ];

        }

        return $contact;
    }

    /* Get all reciepts in case user go on index page */
    public function createReceipt($receipt_id)
    {
        $data['staff'] = $this->staff_model->get('', 1);

        $errors = [];
        $res_data = "";


        $where['tblreciepts.receipt_id'] = $receipt_id;

        $receipts = $this->receipts_model->get_zoho('', $where);
        /*  echo $receipt_id;
          echo "<pre>";
          print_r($receipts);
          die;*/

        // Initialize Zoho API Class
        $zb = new ZohoBooks();

        if ($receipts <> null && count($receipts) > 0) {


            foreach ($receipts as $receipt) {

                if (empty($receipt['zoho_id'])) {

                    // Step 1: get or create client/customer in CRM
                    $client_id = "";
                    $client = $this->clients_model->get($receipt['receipt_client_id']);
                    if ($client <> null) {
                        if (!empty($client->zoho_id)) {
                            $client_id = $receipt['receipt_client_id'] = trim($client->zoho_id);
                        } else {
                            $client_id = $client->userid;
                        }
                    }

                    $customer = json_decode($zb->getContact($client_id));

                    if ($customer == null || empty($customer)) {
                        $contact = $this->createContactData($receipt['receipt_client_id']);

                        if (!empty($contact) && count($contact) > 0 && $contact <> null) {

                            $contact = $zb->postContact(json_encode($contact));


                            $result = json_decode($contact);


                            if ($result <> null) {
                                if ($result->code == '0') {
                                    $contact_id_zoho = $result->contact->contact_id;
                                    // $this->clients_model->updateZohoId($client_id, $contact_id_zoho);
                                    update_zoho_id('tblclients', 'userid', $client_id, 'zoho_id', $contact_id_zoho);
                                    $receipt['receipt_client_id'] = $contact_id_zoho;
                                } else {
                                    $client = $this->clients_model->get($receipt['receipt_client_id']);

                                    if ($client <> null && !empty($client)) {
                                        $receipt['receipt_client_id'] = $client->zoho_id;
                                    }
                                }
                            }
                        }
                    }
                    $client_contacts = $this->clients_model->get_contacts($client->userid);
                    $receipt['client_email'] = $client_contacts[0]['email'];

                    $receiptJson = $this->receiptJson($receipt);

                    $receiptJson = json_encode($this->receiptJson($receipt));
                    $receipt_data = json_decode($zb->postPayment($receiptJson));

                    if (!empty($receipt_data) && $receipt_data <> null) {

                        if (isset($receipt_data->code)) {

                            if ($receipt_data->code == '0') {
                                //recipts table
                                $this->db->where('receipt_id', $receipt['receipt_id']);
                                $this->db->update('tblreciepts', array('zoho_id' => $receipt_data->payment->payment_id));
                            }


                            $res_data .= "<div class='alert alert-danger'>";
                            $res_data .= " Code:" . $receipt_data->code;
                            $res_data .= " Message:" . $receipt_data->message;
                            $res_data .= " Receipt Number:" . strip_tags($receipt['receipt_num']);
                            $res_data .= "</div>";

                            $errors[]['code'] = $receipt_data->code;
                            $errors[]['message'] = $receipt_data->message;
                            $errors[]['receipt_id'] = strip_tags($receipt['receipt_num']);
                        }
                    }
                }
                /*$res_data .= "<div class='alert alert-danger'>";
                $res_data .= "Already exist";
                $res_data .= "</div>";*/

                sleep(15);
            }
        } else {
            $res_data .= "<div class='alert alert-danger'>";
            $res_data .= "No Receipt Found!";
            $res_data .= "</div>";
        }

        return;
    }

    /* Get all reciepts in case user go on index page */
    public function createReceipt_zoho_ajax()
    {

        if ($this->input->post('receipt_id')) {
            $receipt_id = $this->input->post('receipt_id');
        }
        if ($receipt_id == '') {
            echo '0';
            exit();
        }
        $data['staff'] = $this->staff_model->get('', 1);

        $errors = [];
        $res_data = "";


        $where['tblreciepts.receipt_id'] = $receipt_id;

        $receipts = $this->receipts_model->get_zoho('', $where);

        // Initialize Zoho API Class
        $zb = new ZohoBooks();

        if ($receipts <> null && count($receipts) > 0) {


            foreach ($receipts as $receipt) {

                if (empty($receipt['zoho_id'])) {

                    // Step 1: get or create client/customer in CRM
                    $client_id = "";
                    $client = $this->clients_model->get($receipt['receipt_client_id']);
                    if ($client <> null) {
                        if (!empty($client->zoho_id)) {
                            $client_id = $receipt['receipt_client_id'] = trim($client->zoho_id);
                        } else {
                            $client_id = $client->userid;
                        }
                    }

                    $customer = json_decode($zb->getContact($client_id));

                    if ($customer == null || empty($customer)) {
                        $contact = $this->createContactData($receipt['receipt_client_id']);
                     /*   echo "<pre>";
                        print_r($contact);*/
                        if (!empty($contact) && count($contact) > 0 && $contact <> null) {

                            $contact = $zb->postContact(json_encode($contact));

                         /*   print_r($contact);
                            die;*/


                            $result = json_decode($contact);


                            if ($result <> null) {
                                if ($result->code == '0') {
                                    $contact_id_zoho = $result->contact->contact_id;
                                    // $this->clients_model->updateZohoId($client_id, $contact_id_zoho);
                                    update_zoho_id('tblclients', 'userid', $client_id, 'zoho_id', $contact_id_zoho);
                                    $receipt['receipt_client_id'] = $contact_id_zoho;
                                } else {
                                    $client = $this->clients_model->get($receipt['receipt_client_id']);

                                    if ($client <> null && !empty($client)) {
                                        $receipt['receipt_client_id'] = $client->zoho_id;
                                    }
                                }
                            }
                        }
                    }
                    $client_contacts = $this->clients_model->get_contacts($client->userid);
                    $receipt['client_email'] = $client_contacts[0]['email'];

                    $receiptJson1 = $this->receiptJson($receipt);
                   /* echo "<pre>";
                    print_r($receiptJson1);
                    //print_r($receipt_data);
                    die;*/

                    $receiptJson = json_encode($this->receiptJson($receipt));
                    $receipt_data = json_decode($zb->postPayment($receiptJson));
                    /* echo "<pre>";
                    print_r($receiptJson1);
                    print_r($receipt_data);
                    die;*/


                    if (!empty($receipt_data) && $receipt_data <> null) {

                        if (isset($receipt_data->code)) {

                            if ($receipt_data->code == '0') {
                                //recipts table
                                $this->db->where('receipt_id', $receipt['receipt_id']);
                                $this->db->update('tblreciepts', array('zoho_id' => $receipt_data->payment->payment_id));
                                echo "1";
                                exit();
                            } else {
                                echo $receipt_data->message;
                                exit();
                            }


                            $res_data .= "<div class='alert alert-danger'>";
                            $res_data .= " Code:" . $receipt_data->code;
                            $res_data .= " Message:" . $receipt_data->message;
                            $res_data .= " Receipt Number:" . strip_tags($receipt['receipt_num']);
                            $res_data .= "</div>";

                            $errors[]['code'] = $receipt_data->code;
                            $errors[]['message'] = $receipt_data->message;
                            $errors[]['receipt_id'] = strip_tags($receipt['receipt_num']);
                        }
                    }
                }
                /*$res_data .= "<div class='alert alert-danger'>";
                $res_data .= "Already exist";
                $res_data .= "</div>";*/

                sleep(15);
            }
        } else {
            echo "0";
            exit();
            $res_data .= "<div class='alert alert-danger'>";
            $res_data .= "No Receipt Found!";
            $res_data .= "</div>";
        }
        return;
    }

    /**
     * @param $reciept_data
     * @return array
     */
    protected function receiptJson($receipt_data)
    {
        $invoices = $this->receipts_model->get_zoho_recipt_invoices($receipt_data['receipt_id']);
        //khuram iqbal
        $account_id = '1312911000000073107';

        $invoices_array = array();
        $refund = 0;
        $payment_mode = 'others';
        if ($receipt_data['receipt_type'] == 'Cheque') {
            $payment_mode = 'check';
            if($receipt_data['deposit_bank'] == 'enbd' ) {
                //emirates NBD
                $account_id = '1312911000000077839';
            }else if($receipt_data['deposit_bank']== 'rak'){
                //RAK bank
                $account_id = '1312911000000081257';
            }
        } else if ($receipt_data['receipt_type'] == 'Cash') {
            $payment_mode = 'cash';
            if ($receipt_data['reciept_owner'] == 21) {
                //dalbir
                $account_id = '1312911000000086053';
            } else {
                //khuram iqbal
                $account_id = '1312911000000073107';
            }
        } else if ($receipt_data['receipt_type'] == 'Bank Transfer') {
            $payment_mode = 'banktransfer';
            if($receipt_data['deposit_bank']== 'enbd' ) {
                //emirates NBD
                $account_id = '1312911000000077839';
            }else if($receipt_data['deposit_bank'] == 'rak'){
                //RAK bank
                $account_id = '1312911000000081257';
            }
        }

        foreach ($invoices as $key => $invoice) {
            $payment_old = $this->receipts_model->get_invoice_previous_payment($invoice['invoiceid'], $receipt_data['receipt_id']);

            if (!empty($invoice['zoho_id'])) {
                $invoice_id = $invoice['zoho_id'];
                if ($payment_old <> null) {

                    $invoice['total'] = $invoice['total'] - $payment_old;
                }

            } else {


                $invoice_id = $this->createInvoice($invoice['invoiceid']);

            }


            $total_amount = $this->receipts_model->getInvoicesTotal($invoice_id);
            if ($total_amount <> null) {
                $total = $total_amount->total;
            }

            $invoices_array[$key]['invoice_id'] = $invoice_id;
            $invoices_array[$key]['invoice_number'] = $invoice['prefix'] . $invoice['number'];
            $invoices_array[$key]['date'] = $invoice['date'];


                $invoices_array[$key]['invoice_amount'] = $invoice['total'];
                $invoices_array[$key]['amount_applied'] = $invoice['applied_amount'];

            /*        $invoices_array[$key]['invoice_amount'] = round($invoice['total']);
                    $invoices_array[$key]['amount_applied'] = round($invoice['applied_amount']);*/
            if ($invoices_array[$key]['amount_applied'] > $invoices_array[$key]['invoice_amount']) {
                //$amount_due_over = round($invoice['applied_amount']) - round($invoice['total']);
                $amount_due_over = $invoice['applied_amount'] - $invoices_array[$key]['invoice_amount'];
                $invoices_array[$key]['amount_applied'] = $invoices_array[$key]['amount_applied'] - $amount_due_over;
                $refund += $amount_due_over;
            }
        }


        if (count($receipt_data) > 0) {
            $receipt = [
                "payment_mode" => $payment_mode,
                "amount" => ($receipt_data['receipt_amount']),
                "amount_refunded" => $refund,
                "date" => $receipt_data['receipt_date'],
                /*"date" => $receipt_data['paydate'],*/
                "status" => 'success',
                "reference_number" => $receipt_data['receipt_num'],
                "description" => "",
                "customer_id" => $receipt_data['receipt_client_id'],
                "customer_name" => $small = substr($receipt_data['client_name'], 0, 100),
                "invoices" => $invoices_array,
                "email" => $receipt_data['client_email'],
                "currency_code" => 'AED',
                "currency_symbol" => 'AED',
                "exchange_rate" => 1,
                "exchange_rate" => 1,
                "account_id" => $account_id,
            ];
        }
          /* echo $refund;
           echo "<pre>";
           print_r($invoices);
           print_r($receipt_data);
           print_r($invoices_array);
           print_r($receipt);
           die;*/
        return $receipt;
    }

    /* create invoice if it id not exist */
    public function createInvoice($id)
    {
        $data['staff'] = $this->staff_model->get('', 1);

        $where['type'] = 'invoice';
        $where['tblinvoices.id'] = $id;

        $invoices = $this->invoices_model->get('', $where);

        // Initialize Zoho API Class
        $zb = new ZohoBooks();

        if ($invoices <> null && count($invoices) > 0) {


            foreach ($invoices as $invoice) {

                if (empty($invoice['zoho_id'])) {

                    // Step 1: get or create client/customer in CRM
                    $client_id = "";
                    $client = $this->clients_model->get($invoice['clientid']);

                    if ($client <> null) {
                        if (!empty($client->vat)) {
                            $invoice['vat_reg_no'] = $client->vat;
                            $invoice['vat_treatment'] = "vat_registered";
                        } else {
                            $invoice['vat_reg_no'] = $client->vat;
                            $invoice['vat_treatment'] = "vat_not_registered";
                        }


                        if ($client->city == "Dubai" || $client->city == "dubai") {
                            $invoice['place_of_supply'] = "DU";
                        } else if ($client->city == "Abu Dhabi" || $client->city == "abu dhabi") {
                            $invoice['place_of_supply'] = "AB";
                        } else if ($client->city == "Sharjah" || $client->city == "sharjah") {
                            $invoice['place_of_supply'] = "SH";
                        } else if ($client->city == "Ajman" || $client->city == "ajman") {
                            $invoice['place_of_supply'] = "AJ";
                        } else if ($client->city == "Fujairah" || $client->city == "fujairah") {
                            $invoice['place_of_supply'] = "FU";
                        } else if ($client->city == "Ras Al Khaimah" || $client->city == "ras al khaimah") {
                            $invoice['place_of_supply'] = "RA";
                        } else if ($client->city == "Umm Al Quwain" || $client->city == "umm al quwain") {
                            $invoice['place_of_supply'] = "UM";
                        } else {
                            $invoice['place_of_supply'] = "DU";
                        }
                        if (!empty($client->zoho_id)) {
                            $client_id = $invoice['clientid'] = $client->zoho_id;
                        } else {
                            $client_id = $client->userid;
                        }
                    }

                    $customer = json_decode($zb->getContact($client_id));

                    if ($customer == null || empty($customer)) {

                        $contact = $this->createContactData($invoice['clientid']);
                       /* echo "<pre>";
                        print_r($contact);*/
                        if (!empty($contact) && count($contact) > 0 && $contact <> null) {

                            $contact = $zb->postContact(json_encode($contact));

                           /* print_r($contact);
                            die;*/


                            $result = json_decode($contact);

                            if ($result <> null) {
                                if ($result->code == 0) {
                                    $contact_id_zoho = $result->contact->contact_id;
                                    // $this->clients_model->updateZohoId($client_id, $contact_id_zoho);
                                    update_zoho_id('tblclients', 'userid', $client_id, 'zoho_id', $contact_id_zoho);
                                    $invoice['clientid'] = $contact_id_zoho;
                                } else {
                                    $client = $this->clients_model->get($invoice['clientid']);

                                    if ($client <> null && !empty($client)) {
                                        $invoice['clientid'] = $client->zoho_id;
                                    }
                                }
                            }
                        }
                    }

                    $invoiceJson = json_encode($this->invoiceJson($invoice));

                    $invoice_data = json_decode($zb->postInvoice($invoiceJson));
                   /* echo "<pre>";
                    print_r($this->invoiceJson($invoice));
                    print_r($invoice_data);
                    die;*/

                    if (!empty($invoice_data) && $invoice_data <> null) {

                        if (isset($invoice_data->code)) {

                            if ($invoice_data->code == 0 || $invoice_data->code == '0') {
                                //items table
                                $this->db->where('rel_id', $invoice['id']);
                                $this->db->update('tblitems_in', array('zoho_id' => $invoice_data->invoice->invoice_id));
                                //invoice table
                                $this->db->where('id', $invoice['id']);
                                $this->db->update('tblinvoices', array('zoho_id' => $invoice_data->invoice->invoice_id));
                                return $invoice_data->invoice->invoice_id;
                            } else {
                            }
                        }
                    }
                }
            }
        } else {

        }

        return;
    }

    /* create invoice if it id not exist */
    public function createInvoice_zoho_ajax()
    {

        if ($this->input->post('invoice_id')) {
            $id = $this->input->post('invoice_id');
        }
        if ($id == '') {
            echo '0';
            exit();
        }

        $data['staff'] = $this->staff_model->get('', 1);

        $where['type'] = 'invoice';
        $where['tblinvoices.id'] = $id;

        $invoices = $this->invoices_model->get('', $where);

        // Initialize Zoho API Class
        $zb = new ZohoBooks();

        if ($invoices <> null && count($invoices) > 0) {

            foreach ($invoices as $invoice) {

                if (empty($invoice['zoho_id'])) {

                    // Step 1: get or create client/customer in CRM
                    $client_id = "";
                    $client = $this->clients_model->get($invoice['clientid']);

                    if ($client <> null) {
                        if (!empty($client->vat)) {
                            $invoice['vat_reg_no'] = $client->vat;
                            $invoice['vat_treatment'] = "vat_registered";
                        } else {
                            $invoice['vat_reg_no'] = $client->vat;
                            $invoice['vat_treatment'] = "vat_not_registered";
                        }


                        if ($client->city == "Dubai" || $client->city == "dubai") {
                            $invoice['place_of_supply'] = "DU";
                        } else if ($client->city == "Abu Dhabi" || $client->city == "abu dhabi") {
                            $invoice['place_of_supply'] = "AB";
                        } else if ($client->city == "Sharjah" || $client->city == "sharjah") {
                            $invoice['place_of_supply'] = "SH";
                        } else if ($client->city == "Ajman" || $client->city == "ajman") {
                            $invoice['place_of_supply'] = "AJ";
                        } else if ($client->city == "Fujairah" || $client->city == "fujairah") {
                            $invoice['place_of_supply'] = "FU";
                        } else if ($client->city == "Ras Al Khaimah" || $client->city == "ras al khaimah") {
                            $invoice['place_of_supply'] = "RA";
                        } else if ($client->city == "Umm Al Quwain" || $client->city == "umm al quwain") {
                            $invoice['place_of_supply'] = "UM";
                        } else {
                            $invoice['place_of_supply'] = "DU";
                        }
                        if (!empty($client->zoho_id)) {
                            $client_id = $invoice['clientid'] = $client->zoho_id;
                        } else {
                            $client_id = $client->userid;
                        }
                    }

                    $customer = json_decode($zb->getContact($client_id));

                    if ($customer == null || empty($customer)) {

                        $contact = $this->createContactData($invoice['clientid']);

                        if (!empty($contact) && count($contact) > 0 && $contact <> null) {
                           /* if($invoice['id'] == 5174) {
                                echo "<pre>";
                                print_r($contact);
                            }*/
                            $contact = $zb->postContact(json_encode($contact));
                          /*  if($invoice['id'] == 5174) {
                                     print_r($contact);
                                     die;
                            }*/

                            $result = json_decode($contact);

                            if ($result <> null) {
                                if ($result->code == 0) {
                                    $contact_id_zoho = $result->contact->contact_id;
                                    // $this->clients_model->updateZohoId($client_id, $contact_id_zoho);
                                    update_zoho_id('tblclients', 'userid', $client_id, 'zoho_id', $contact_id_zoho);
                                    $invoice['clientid'] = $contact_id_zoho;
                                } else {
                                    $client = $this->clients_model->get($invoice['clientid']);

                                    if ($client <> null && !empty($client)) {
                                        $invoice['clientid'] = $client->zoho_id;
                                    }
                                }
                            }
                        }
                    }

                    $invoiceJson = json_encode($this->invoiceJson($invoice));

                    $invoice_data = json_decode($zb->postInvoice($invoiceJson));
                  /*  if($invoice['id'] == 5174) {
                          echo "<pre>";
                          print_r($this->invoiceJson($invoice));
                          print_r($invoice_data);
                          die;
                    }*/

                    if (!empty($invoice_data) && $invoice_data <> null) {

                        if (isset($invoice_data->code)) {

                            if ($invoice_data->code == '0') {
                                //items table
                                $this->db->where('rel_id', $invoice['id']);
                                $this->db->update('tblitems_in', array('zoho_id' => $invoice_data->invoice->invoice_id));
                                //invoice table
                                $this->db->where('id', $invoice['id']);
                                $this->db->update('tblinvoices', array('zoho_id' => $invoice_data->invoice->invoice_id));
                                $this->createInvoice_sent_zoho_ajax($invoice_data->invoice->invoice_id);
                                echo "1";
                                exit();
                            } else {
                                echo "0";
                                exit();
                            }
                        }
                    }
                }
            }
        } else {

        }

        return;
    }
    public function createInvoice_sent_zoho_ajax($invoice_id){
        $zb = new ZohoBooks();
        $zb->postInvoice_status_sent($invoice_id);
    }


}