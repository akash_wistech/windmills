<?php
namespace app\commands;
use Yii;
use app\models\ListData;
use yii\console\Controller;
use yii\db\Expression;
use app\models\BuildingForSave;

class MigrationSaleListController extends Controller
{
    public function actionMigrateData()
    {

        $random = $this->generateRandomString();
        
        $start = 00;
        $end = 24;
        $now = date('H');
      //  if ($now >= $start && $now <= $end) {

            $startDate = '2024-06-01';
            $endDate = '2024-09-30';

            $query = ListData::find()
                ->where(['move_to_listing' => 0])
                ->andWhere(['purpose' => 'for-sale'])
              //  ->andWhere(['is not', 'building_info', null])
                //->andWhere(['in', 'building_info', ['The Springs 12']])
                ->andWhere(['between', 'list_data_date', $startDate, $endDate])
                ->orderBy(['list_data_date' => SORT_DESC])
                ->limit(5)
                ->asArray()
                ->all();



            $i = 1;
            foreach ($query as $key => $query) {

                /*$building_row = \app\models\Buildings::find()
                    ->where(['=', 'title', trim($query['building_info'])])
                    ->orWhere(['=', 'bayut_title', trim($query['building_info'])])
                    ->orWhere(['=', 'bayut_title_2', trim($query['building_info'])])
                    ->one();*/
                $building_row = \app\models\Buildings::find()
                    ->andWhere(['status'=>[1,2]])
                    ->andWhere(['or',
                        ['title'=> trim($query['building_info'])],
                        ['bayut_title'=> trim($query['building_info'])],
                        ['bayut_title_2'=> trim($query['building_info'])],
                        ['bayut_title_3'=> trim($query['building_info'])],
                        ['bayut_title_4'=> trim($query['building_info'])],
                        ['bayut_title_5'=> trim($query['building_info'])],
                        ['bayut_title_6'=> trim($query['building_info'])],
                        ['bayut_title_7'=> trim($query['building_info'])],
                        ['bayut_title_8'=> trim($query['building_info'])],
                        ['bayut_title_9'=> trim($query['building_info'])],
                        ['bayut_title_ten'=> trim($query['building_info'])],
                    ])->one();
                if ($building_row != null) {
                    $model = \app\models\ListingsTransactions::find()->where(['listings_reference' => $query['listings_reference']])->one();


                    if ($model <> null) {

                        $model = \app\models\ListingsTransactions::find()
                            ->where([
                                 'listings_reference'   => $query['listings_reference'],
                                'building_info' => $building_row->id,
                                //   'listing_date'         => $query['listing_date'],
                                'no_of_bedrooms' => $query['no_of_bedrooms'],
                                'built_up_area' => $query['land_size'],
                                'listings_price' => $query['listings_price'],
                            ])->one();
                        if ($model == null) {
                            $query['listings_reference'] = $query['listings_reference'] . '_' . $random;
                            $this->saveListingTransaction($query, $model);
                        } else {
                            // Yii::$app->db->createCommand()->update('list_data', [ 'move_to_listing' => 1 ], [ 'id'=>$query['id'] ])->execute();
                            Yii::$app->db->createCommand()->update('list_data', ['move_to_listing' => 3], ['id' => $query['id']])->execute();
                        }
                    } else {
                        $model = \app\models\ListingsTransactions::find()
                            ->where([
                                 'listings_reference'   => $query['listings_reference'],
                                'building_info' => $building_row->id,
                                //   'listing_date'         => $query['listing_date'],
                                'no_of_bedrooms' => $query['no_of_bedrooms'],
                                'built_up_area' => $query['land_size'],
                                'listings_price' => $query['listings_price'],
                            ])->one();
                        if ($model == null) {
                            $this->saveListingTransaction($query, $model);
                        } else {
                            Yii::$app->db->createCommand()->update('list_data', ['move_to_listing' => 3], ['id' => $query['id']])->execute();
                        }
                    }
                    $i++;
                }else{
                    Yii::$app->db->createCommand()->update('list_data', [ 'move_to_listing' => 2 ], [ 'id'=>$query['id'] ])->execute();
                }
            }
       /* }
            else{
                echo "time out";
            }*/
            
            
            
        }
        
        public function saveListingTransaction($query=null, $model=null)
        {

         /*   $building = \app\models\Buildings::find()
                ->where(['=', 'title', trim($query['building_info'])])
                ->orWhere(['=', 'bayut_title', trim($query['building_info'])])
                ->orWhere(['=', 'bayut_title_2', trim($query['building_info'])])
                ->one();*/

            $building = \app\models\Buildings::find()
                ->andWhere(['status'=>[1,2]])
                ->andWhere(['or',
                    ['title'=> trim($query['building_info'])],
                    ['bayut_title'=> trim($query['building_info'])],
                    ['bayut_title_2'=> trim($query['building_info'])],
                    ['bayut_title_3'=> trim($query['building_info'])],
                    ['bayut_title_4'=> trim($query['building_info'])],
                    ['bayut_title_5'=> trim($query['building_info'])],
                    ['bayut_title_6'=> trim($query['building_info'])],
                    ['bayut_title_7'=> trim($query['building_info'])],
                    ['bayut_title_8'=> trim($query['building_info'])],
                    ['bayut_title_9'=> trim($query['building_info'])],
                    ['bayut_title_ten'=> trim($query['building_info'])],
                ])->one();
            
            if($building<>null && $building->id<>null) {
                $model= new \app\models\ListingsTransactions;
                $model->listings_reference      = $query['listings_reference'];
                $model->source                  = $query['source'];
                $model->listing_website_link    = $query['listing_website_link'];
                $model->listing_date            = $query['listing_date'];
                $model->unit_number             = 'Not Known'; //static
                // print_r($query); die;
                
                if ($query['property_type'] == 'apartment'||
                $query['property_type'] == 'shop'
                ) {
                    $model->built_up_area           = $query['built_up_area'];
                    if($model->built_up_area <> null){

                    }else{
                        $model->built_up_area           = $query['land_size'];
                    }
                    $model->land_size               = '';
                }
                
                else if ($query['property_type'] == 'villa' ||
                $query['property_type'] == 'townhouse' ||
                $query['property_type'] == 'villa compound' ||
                $query['property_type'] == 'residential building' ||
                $query['property_type'] == 'hotel apartment' ||
                $query['property_type'] == 'office' ||
                $query['property_type'] == 'warehouse' ||
                $query['property_type'] == 'commercial villa' ||
                $query['property_type'] == 'factory'
                ) {
                    $model->built_up_area           = $query['built_up_area'];
                    $model->land_size               = $query['land_size'];
                }
                
                
                else {
                    $model->built_up_area           = $query['built_up_area'];
                    $model->land_size               = $query['land_size'];
                }
                
                
                
                $model->building_info           = $building->id;
                $model->property_category       = $building->property_category;
                $model->tenure                  = $building->tenure;
                $model->community               = $building->community;
                $model->sub_community           = $building->sub_community;
                $model->property_placement      = $building->property_placement;
                $model->property_visibility     = $building->property_visibility;
                $model->property_exposure       = $building->property_exposure;
                $model->property_condition      = $building->property_condition;
                $model->development_type        = $building->development_type;
                $model->utilities_connected     = $building->utilities_connected;
                $model->finished_status         = $building->finished_status;
                $model->pool                    = $building->pool;
                $model->gym                     = $building->gym;
                $model->play_area               = $building->play_area;
                $model->other_facilities        = ($building->other_facilities<>null)? explode(',' , $building->other_facilities) : '';
                $model->completion_status       = $building->completion_status;
                $model->white_goods             = $building->white_goods;
                $model->furnished               = $building->furnished;
                $model->landscaping             = $building->landscaping;
                $model->estimated_age           = ($building->estimated_age<>null)? $building->estimated_age : '0.00';
                $model->location_highway_drive	= $building->location_highway_drive;
                $model->location_school_drive	= $building->location_school_drive;
                $model->location_mall_drive		= $building->location_mall_drive;
                $model->location_sea_drive		= $building->location_sea_drive;
                $model->location_park_drive		= $building->location_park_drive;
                
                $model->no_of_bedrooms          = $query['no_of_bedrooms'];
                $model->listings_price          = $query['listings_price'];
                $model->listings_rent           = $query['listings_rent'];
                $model->final_price             = $query['final_price'];
                $model->status                  = 2;
                $model->listing_property_type   = $query['property_type'];
                
                
                if($model->save()) {
                    Yii::$app->db->createCommand()->update('list_data', [ 'move_to_listing' => 1 ], [ 'id'=>$query['id'] ])->execute();
                }
                if($model->hasErrors()){
                   /* foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                echo $val. "<br>";
                            }   
                        }
                    }*/
                }
            }else{
                Yii::$app->db->createCommand()->update('list_data', [ 'move_to_listing' => 2 ], [ 'id'=>$query['id'] ])->execute();
                $queryCheck = BuildingForSave::find()->where(['building_name'=>$query['building_info'], 'city'=>$query['city_id']])->one();
                if($queryCheck==null){
                    $saveModel = new BuildingForSave;
                    $saveModel->building_name = $query['building_info'];
                    $saveModel->city 		  = $query['city_id'];
                    $saveModel->save();
                    
                    if($saveModel->hasErrors()){
                       /* foreach($saveModel->getErrors() as $error){
                            if(count($error)>0){
                                foreach($error as $key=>$val){
                                    echo $val. "<br>";
                                }
                            }
                        }*/
                    }
                }
            }
            
        }
        
        
        public function findbuilding($query)
        {
            
            $length = strlen($query['building_info']); //echo $length."<br>";
            for ($i = 1; $i <= $length; $i++) {
                
                $remove_char = mb_substr($query['building_info'], '-'.$i);
                $trim_building_info  =  rtrim($query['building_info'], $remove_char);
                
                $building = \app\models\Buildings::find()
                ->where(new Expression(' title LIKE "%'.$trim_building_info.'%" '))
                ->orwhere(new Expression(' bayut_title LIKE "%'.$trim_building_info.'%" '))
              //  ->andWhere(['city' => $query['city_id']])
                ->one();
                
                if($building<>null){
                    return $building;
                    break;
                }
            }
            
        }
        
        
        
        
        public function generateRandomString($length = 5)
        {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return 'ST-'.$randomString;
        }   
        
        
        
        
        
    }
    
    