<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\SoldTransaction;
use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Company;

use yii\helpers\Url;
use yii;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SoldController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex()
    {

        $today_date = date('Y-m-d');

        $day = date('l', strtotime($today_date));

        if($day != 'Saturday' && $day !='Sunday'){
            $today_start_date = $today_date.' 00:00:00';
            $today_end_date = $today_date.' 23:59:59';
            $sold_valuations =(int)SoldTransaction::find()->where(['between','created_at',$today_start_date, $today_end_date])->count('id');
            if($sold_valuations <> null && $sold_valuations > 0){
            }else{
                $notifyData = [
                    'subject' => 'Sold Transactions Not Uploaded Today',
                    'replacements'=>[
                    ],
                ];
                 \app\modules\wisnotify\listners\NotifyEvent::fire('Sold.uploaded', $notifyData);

            }

        }

        echo 'Emails sent.';
        die;
               }
}
