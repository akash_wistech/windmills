<?php
namespace app\commands;
use Yii;
use yii\console\Controller;
use Illuminate\Http\Request;
use Symfony\Component\DomCrawler\Crawler;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Exception;
use app\models\PropertyFinderUrls;
use app\models\PropertyFinderFetchUrl;
use app\models\PropertyFinderDetailUrls;
use app\models\Buildings;
use app\models\ListingsTransactions;
use DOMXPath;
use DOMDocument;

class PfCurlController extends Controller
{

	public function actionSaveUrls($url=null, $link_id=null)
	{
		require __DIR__ . '/../components/helpers/simple_html_dom.php';

		$findUrl = PropertyFinderFetchUrl::find()->one();
		if ($findUrl==null) {
			$query = PropertyFinderUrls::find()->one();
			if ($query<>null) {
				$model = new PropertyFinderFetchUrl;
				$model->property_finder_url_id = $query->id;
				$model->url_to_fetch = $query->url;
				$model->property_category = $query->property_category;
				if($model->save()){
					$link_id            = $model->property_finder_url_id;
					$url                = $model->url_to_fetch;
					$property_category  = $model->property_category;
				}
			}
		}
		else{
			$link_id                = $findUrl->property_finder_url_id;
			$url                    = $findUrl->url_to_fetch;
			$property_category      = $findUrl->property_category;
		}

		try {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			curl_close($ch);	

			$html = new \simple_html_dom();
			$html->load($response); 

			$i=1;
			foreach ($html->find('a.card__link') as $key => $value) {
				$detail_url = "https://www.propertyfinder.ae".$value->href;
				if ($detail_url<>null) {
					$model = PropertyFinderDetailUrls::find()->where(['detail_url'=>$detail_url])->one();
					if ($model==null) {
						$model = new PropertyFinderDetailUrls;
						$model->detail_url = $detail_url;
						$model->property_category = $property_category;
						$model->status = 0;
						if ($model->save()) {
							echo "Saved-".$i."  "; 
						}
					}
				}
				$i++;
			}

			$nextPageUrl=[];
			try{
				foreach ($html->find('a.pagination__link--next') as $key => $value) {
					$nextPageUrl[] = $value->href;
				}
				if ($nextPageUrl<>null AND $nextPageUrl[0]<>null) {
					$url_to_fetch = 'https://www.propertyfinder.ae'.trim($nextPageUrl[0]).'';

					if ($url_to_fetch<>null) {
						Yii::$app->db->createCommand()
						->update('property_finder_fetch_url', ['property_finder_url_id' => $link_id, 'url_to_fetch'=> str_replace("amp;", "", trim($url_to_fetch))    , 'property_category'=>$property_category])
						->execute();
					}
				}

				else{
					$query = PropertyFinderUrls::find()->where(['>', 'id', $link_id])->one();
					if ($query<>null) {
						Yii::$app->db->createCommand()
						->update('property_finder_fetch_url', ['property_finder_url_id' => $query->id, 'url_to_fetch'=> $query->url, 'property_category'=>$query->property_category])
						->execute();
					}
				}

			}catch(Exception $err){
				$error = $err->getMessage();
				print_r($error);
			}


		} catch ( Exception $err ) {
			$error = $err->getMessage();
			echo "1st Catch vv<pre>"; print_r($error); echo "</pre>";
		}
	}


	public function actionGetDetail()
	{
		$randomString = Yii::$app->propertyFinderHelperFunctions->generateRandomString();
		$data = [];
		$urls = PropertyFinderDetailUrls::find()->where(['status'=>0])->limit(1)->all();
		if ($urls<>null) {
			foreach ($urls as $urls) {
				Yii::$app->db->createCommand()
				->update('property_finder_detail_urls', [ 'status' => 2 ] , [ 'detail_url' => $urls->detail_url ])
				->execute();
				$url = $urls->detail_url;	

				try {
					//1st try
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					$response = curl_exec($ch);

				// $file_map = 'uploads/pf-files/file-'.$randomString.'.txt';
				// file_put_contents('uploads/pf-files/file-'.$randomString.'.txt', $response);
				// $contents  = file_get_contents('uploads/pf-files/file-'.$randomString.'.txt');
				// $data['file_map'] = $file_map;

					$data['url'] = $url;

					$tel_Position = strpos($response, 'tel:');
					if ($tel_Position<>null) {
						$nextChar = substr($response, $tel_Position, 17);
					}
					$val = preg_replace('/[^0-9--.]/', '', $nextChar);

					$data['agent_telephone'] = $val;
					$data['agent_telephone'] = trim("+".$val);

					$data['agent_title'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.agent_title');
					$data['agent_name'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.agent_name');
					$data['agent_location'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.broker_location');
					$data['agent_company'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.broker_name');

					$data['property_name'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_name');
					$data['property_type'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_type');
					$data['bathrooms'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_bathrooms');
					$data['bedrooms'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_bedrooms');
					$data['property_category'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_sub_category');
					$data['property_completion'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_completion');
					$data['listed'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_listed_days');
					$data['property_listing_depth'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_listing_depth');
					$data['city'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_location_city');
					$data['community'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_location_community');
					$data['sub_community'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_location_sub_community');
					$data['building_info'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_location_tower');
					$data['listings_price'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_price');
					$data['reference'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_reference');
					$data['property_size'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_size_sqft');

					//date formatting
					$data['listed'] = str_replace(" ", "-", $data['listed']);
					$data['listed'] = date("Y-m-d", strtotime($data['listed']));

					//reference number formatting
					$data['reference'] = str_replace(" ", "-", $data['reference']);

					//building info changes
					if ($data['building_info']=='' AND $data['building_info']==null) {
						$data['building_info'] = $data['sub_community'];
					};
					$data['description'] = strtolower(trim($response));

					$this->getDecision($data);

					curl_close($ch);
				}
				catch ( Exception $err ) {
					//1st catch
					$error = $err->getMessage();
					echo "1st Catch cc<br>";
					print_r($error);
				}


			}
		}







	}






	public function getDecision($data=null)
	{
		if ($data['building_info']<>null) {
			$city_id = Yii::$app->appHelperFunctions->getCityId($data['city']);
			$commAndSubCommId = Yii::$app->propertyFinderHelperFunctions->getCommAndSubCommIds($data['community'],$data['sub_community']);

			$building = Buildings::find()->where([
				'title'         => trim($data['building_info']),
			// 'title'         => trim('Mudon Villas - Arabella 1 (Al Hebiah Sixth)'),
				'city'          => $city_id,
			]);
			if ($commAndSubCommId<>null&&$commAndSubCommId['community_id']<>null&&$commAndSubCommId['sub_community_id']<>null) {
				$building->andWhere([
					'community'     => $commAndSubCommId['community_id'],
					'sub_community' => $commAndSubCommId['sub_community_id'],
				]);
			}
			$building->andWhere(['city'=>$city_id]);
			$building->orWhere(['like', 'title', trim($data['building_info']) . '%', false]);
			// $building->orWhere(['like', 'title', trim('Mudon Villas - Arabella 1 (Al Hebiah Sixth)') . '%', false]);
			$model = $building->one();
			if ($model<>null) {
				// echo "<pre>"; print_r($model); echo "</pre>"; die();
				$this->saveListingTransaction($data,$model);
			}else{
			// unlink($data['file_map']);
				echo "Building not found";
			}
		}
	}





	public function saveListingTransaction($data=null,$model)
	{
		$desc = $data['description'];
		$white_goods = $furnished = $pool = $gym = $view_sea = '';
		if ($desc<>null) {
			if (strpos($desc, 'built in kitchen appliances') !== false) {
				$white_goods  = 'Yes';
			}if (strpos($desc, 'furnished') !== false) {
				$furnished  = 'Yes';
			}if (strpos($desc, 'private pool') !== false) {
				$pool  = 'Yes';
			}if (strpos($desc, 'private Gym') !== false) {
				$gym  = 'Yes';
			}if (strpos($desc, 'view of water') !== false) {
				$view_sea  = 'Yes';
			}
		}

		$bedrooms = $studio = '';
		if ($data['bedrooms']<>null && is_numeric($data['bedrooms'])) {
			$bedrooms = $data['bedrooms'];
		}
		if ($data['bedrooms']<>null && $data['bedrooms'] == 0) {
			$studio = 'Yes';
		}

		$findListing = ListingsTransactions::find()->where(['listings_reference'=>$data['reference'], 'listing_website_link'=>$data['url']])->one();
		if ($findListing==null) {
			$listingTransaction = new ListingsTransactions;
            $listingTransaction->listings_reference   = $data['reference']; //done
            $listingTransaction->source               = 16; //done
            $listingTransaction->listing_website_link = $data['url']; //done
            $listingTransaction->listing_date         = $data['listed']; //done
            $listingTransaction->unit_number          = 'Not Known'; //done

            $listingTransaction->building_info = $model->id;

            // property category working
            if ($data['property_category'] == 'residential') {
            	$listingTransaction->property_category = 1;
            }
            else if ($data['property_category'] == 'commercial') {
            	$listingTransaction->property_category = 4;
            }

            //tenure working
            if ($model->tenure<>null) {
            	$listingTransaction->tenure = $model->tenure;
            }
            else if(strpos($desc, 'freehold') !== false || strpos($desc, 'free hold') !== false || strpos($desc, 'free-hold') !== false)
            {
            	$listingTransaction->tenure   = 2;
            }
            else{
            	if ($data['city']=='Dubai') {
            		$listingTransaction->tenure   = 2;
            	}else{
            		$listingTransaction->tenure   = 1;
            	}
            }

            //property placement working
            if (strpos($data['property_type'], 'apartment') !== false || strpos($data['property_type'], 'office') !== false) {
            	$listingTransaction->property_placement  = 3.00;
            }else{
            	if (strpos($desc, 'middle') !== false || strpos($desc, 'mid unit') !== false || strpos($desc, 'mid-unit') !== false) {
            		$listingTransaction->property_placement  = 1.00;
            	}
            	else if(strpos($desc, 'corner') !== false){
            		$listingTransaction->property_placement  = 2.00;
            	}
            	else if (strpos($desc, 'semi corner') !== false || strpos($desc, 'semi-corner') !== false || strpos($desc, 'semicorner') !== false || strpos($desc, 'end unit') !== false) {
            		$listingTransaction->property_placement  = 1.50;
            	}
            	else{
            		$listingTransaction->property_placement  = 1.00;
            	}
            }

            //property visibility
            if ($data['property_type'] == 'shop') {
            	$listingTransaction->property_visibility = 2;

            	if(strpos($desc, 'back') !== false || strpos($desc, 'behind') !== false){
            		$listingTransaction->property_visibility = 1;
            	}
            }else{
            	$listingTransaction->property_visibility = 3;
            }

            //Property Exposure working
            if (strpos($data['property_type'], 'apartment') !== false || strpos($data['property_type'], 'office') !== false) {
            	$listingTransaction->property_exposure   = 3;
            }
            else{
            	if (strpos($desc, 'single row') !== false || strpos($desc, 'single-row') !== false) {
            		$listingTransaction->property_exposure   = 2;
            	}
            	else if(strpos($desc, 'back to back') !== false || strpos($desc, 'backtoback') !== false || strpos($desc, 'back 2 back') !== false || strpos($desc, 'back2back') !== false || strpos($desc, 'b2b') !== false || strpos($desc, 'back-to-back') !== false){
            		$listingTransaction->property_exposure   = 1;
            	}
            	else{
            		$listingTransaction->property_exposure   = 1;
            	}
            }


            $listingTransaction->property_condition  = 3;
            $listingTransaction->development_type    = 'Standard';

            //Utilities connected working
            if (strpos($data['property_type'], 'land') !== false) {
            	$listingTransaction->utilities_connected = 'No';
            }else{
            	$listingTransaction->utilities_connected = 'Yes';
            }

            //Finished status working
            if (strpos($data['property_type'], 'office') !== false) {
            	$listingTransaction->finished_status     = 'Fitted';

            	if(strpos($desc, 'fitted') !== false){
            		$listingTransaction->finished_status     = 'Shell & Core';
            	}
            }else{
            	$listingTransaction->finished_status     = 'Fitted';
            }


            // Pool working
            if ($pool=='Yes') {
            	$listingTransaction->pool = 'Yes';
            }
            else if (strpos($desc, 'private pool') !== false ||strpos($desc, 'private swimming pool') !== false ||strpos($desc, 'with pool') !== false ||strpos($desc, 'own swimming pool') !== false || strpos($desc, 'own pool') !== false) {
            	$listingTransaction->pool = 'Yes';
            }
            else{
            	$listingTransaction->pool = 'No';
            }

            // Gym Working
            if ($gym=='Yes') {
            	$listingTransaction->gym = 'Yes';
            }
            else if (strpos($desc, 'private gym') !== false ||strpos($desc, 'own gym') !== false || strpos($desc, 'private fitness') !== false) {
            	$listingTransaction->gym = 'Yes';
            }
            else{
            	$listingTransaction->gym = 'No';
            }


            // Play area Working
            if (strpos($desc, 'private play') !== false || strpos($desc, 'own play') !== false) {
            	$listingTransaction->play_area = 'Yes';
            }
            else{
            	$listingTransaction->play_area = 'No';
            }

            $listingTransaction->other_facilities    = ($model->other_facilities<>null)? explode(',' , $model->other_facilities) : '';
            $listingTransaction->completion_status   = 100.00;



            //White goods working
            if ($white_goods=='Yes') {
            	$listingTransaction->white_goods  = 'Yes';
            }
            else if (strpos($desc, 'equipped kitchen') !== false || strpos($desc, 'fully equipped') !== false || strpos($desc, 'fitted kitchen') !== false) {
            	$listingTransaction->white_goods  = 'Yes';
            }
            else{
            	$listingTransaction->white_goods  = 'No';
            }

            //Furnished Working
            if ($furnished == 'Yes') {
            	$listingTransaction->furnished = 'Yes';
            }
            else if (strpos($desc, 'fully furnished') !== false) {
            	$listingTransaction->furnished = 'Yes';
            }
            else{
            	$listingTransaction->furnished = 'No';
            }

            //Landscaping working
            if (strpos($data['property_type'], 'villa') !== false || strpos($data['property_type'], 'townhouse') !== false) {
            	$listingTransaction->landscaping = 'Yes';
            }
            else{
            	$listingTransaction->landscaping  = 'No';
            }

            // no of bedroom
            $listingTransaction->no_of_bedrooms = ($bedrooms<>null AND $bedrooms!='') ? $bedrooms : 0;

            //Upgrades
            if (strpos($desc, 'upgrades') !== false || strpos($desc, 'upgraded') !== false) {
            	$listingTransaction->upgrades  = 4;
            }
            else{
            	$listingTransaction->upgrades  = 3;
            }

            // Full Building Floors working
            if (strpos($data['property_type'], 'land') !== false) {
            	$listingTransaction->full_building_floors = 0;
            }
            else if (strpos($data['property_type'], 'villa') !== false  || strpos($data['property_type'], 'townhouse') !== false  ) {
            	if ($model->typical_floors<>null) {
            		$listingTransaction->full_building_floors = $model->typical_floors;
            	}
            	else{
            		$listingTransaction->full_building_floors = 1;
            	}
            }
            else{
            	if ($model->typical_floors<>null) {
            		$listingTransaction->full_building_floors = $model->typical_floors;
            	}
            	else{
            		$listingTransaction->full_building_floors = 0;
            	}
            }


            //Parking Space working
            if (strpos($data['property_type'], 'villa') !== false || strpos($data['property_type'], 'townhouse') !== false) {
            	$listingTransaction->parking_space  = 2;
            }
            if (strpos($data['property_type'], 'apartment') !== false) {
                // echo "studio: ". $studio; die();
            	if ($studio<>null && $studio=='yes' && $bedrooms==1) {
            		$listingTransaction->parking_space  = 1;
            	}
            	else if ($studio<>null && $studio=='yes' && $bedrooms==2) {
            		$listingTransaction->parking_space  = 1;
            	}
            	else if ($bedrooms>=3) {
            		$listingTransaction->parking_space  = 2;
            	}
            	else{
            		$listingTransaction->parking_space  = 1;
            	}
            }
            else{
            	$listingTransaction->parking_space  =0;
            }

            //Estiated age working
            if (strpos($data['property_type'], 'land') !== false) {
            	$listingTransaction->estimated_age = 0;
            }else{
            	if ($model->estimated_age<>null) {
            		$listingTransaction->estimated_age = $model->estimated_age;
            	}else{
            		$listingTransaction->estimated_age = 0;
            	}
            }

            //Floor Number Working
            if (strpos($data['property_type'], 'apartment') !== false || strpos($data['property_type'], 'office') !== false) {
            	if (strpos($desc, 'high floor') !== false || strpos($desc, 'higher floor') !== false || strpos($desc, 'high-floor') !== false) {
            		$listingTransaction->floor_number = round(3/4*$model->typical_floors);
            	}
            	else if (strpos($desc, 'low floor') !== false || strpos($desc, 'lower floor') !== false || strpos($desc, 'low-floor') !== false) {
            		$listingTransaction->floor_number = round(1/4*$model->typical_floors);
            	}
            	else{
            		$listingTransaction->floor_number = round(1/2*$model->typical_floors);
            	}
            }else{
            	$listingTransaction->floor_number = 0;
            }


            //No of levels Working
            if (strpos($data['property_type'], 'land') !== false || strpos($data['property_type'], 'apartment') !== false || strpos($data['property_type'], 'warehouse') !== false || strpos($data['property_type'], 'office') !== false) {
            	$listingTransaction->number_of_levels = 1;
            }
            else if (strpos($data['property_type'], 'villa') !== false || strpos($data['property_type'], 'townhouse') !== false || strpos($data['property_type'], 'duplex') !== false) {
            	if ($model->typical_floors<>null) {
            		$listingTransaction->number_of_levels = $model->typical_floors;
            	}
            	else{
            		$listingTransaction->number_of_levels = 2;
            	}
            }
            else{
            	if ($model->typical_floors<>null) {
            		$listingTransaction->number_of_levels = $model->typical_floors;
            	}else{
            		$listingTransaction->number_of_levels = 2;
            	}
            }


            //Start bua and land size
            if (strpos($data['property_type'], 'apartment') !== false || strpos($data['property_type'], 'office') !== false) {
            	$listingTransaction->land_size = 0;
            	$listingTransaction->built_up_area = $data['property_size'];
            }
            else{
                // echo "ello"; die();
            	$landAndBuiltUpArea = Yii::$app->propertyFinderHelperFunctions->getBuaAndLandArea($desc,$data['property_size'],$bedrooms);
                // echo "<br>In main function: <pre>"; print_r($landAndBuiltUpArea); echo "</pre>"; die();
            	$listingTransaction->built_up_area = $landAndBuiltUpArea['bua'];
            	$listingTransaction->land_size = $landAndBuiltUpArea['land_size'];
            }
            //End bua and land size

            $listingTransaction->balcony_size = '0';

            //Start Location Attributes
            $listingTransaction->location_highway_drive = ($model->location_highway_drive<>null) ? $model->location_highway_drive : 'minutes_5';
            $listingTransaction->location_school_drive  = ($model->location_school_drive<>null) ? $model->location_school_drive : 'minutes_5';
            $listingTransaction->location_mall_drive    = ($model->location_mall_drive<>null) ? $model->location_mall_drive : 'minutes_5';
            $listingTransaction->location_sea_drive     = ($model->location_sea_drive<>null) ? $model->location_sea_drive : 'minutes_5';
            $listingTransaction->location_park_drive    = ($model->location_park_drive<>null) ? $model->location_park_drive : 'minutes_5';
            //End Location Attributes


            //Start View Attributes
            $listingTransaction->view_community = 'normal';

            if (strpos($desc, 'shared pool') !== false) {
            	$listingTransaction->view_pool = 'none';
            }
            else if (strpos($desc, 'view of pool') !== false || strpos($desc, 'view of fountain') !== false || strpos($desc, 'fountain view') !== false) {
            	$listingTransaction->view_pool = 'full';
            }
            else{
            	$listingTransaction->view_pool = 'none';
            }
            if (strpos($desc, 'view of burj') !== false || strpos($desc, 'burj khalifa view') !== false || strpos($desc, 'burj view') !== false || strpos($desc, 'burj al arab view') !== false) {
            	$listingTransaction->view_burj = 'full';
            }
            else{
            	$listingTransaction->view_burj = 'none';
            }
            if ($view_sea=='Yes') {
            	$listingTransaction->view_sea = 'full';
            }
            else if (strpos($desc, 'sea view') !== false || strpos($desc, 'view of sea') !== false || strpos($desc, 'view of water') !== false || strpos($desc, 'water view') !== false) {
            	$listingTransaction->view_sea = 'full';
            }
            else{
            	$listingTransaction->view_sea = 'none';
            }
            if (strpos($desc, 'marina view') !== false || strpos($desc, 'view of marina') !== false || strpos($desc, 'view of dubai marina') !== false || strpos($desc, 'views of dubai marina') !== false || strpos($desc, 'views of marina') !== false) {
            	$listingTransaction->view_marina = 'full';
            }
            else{
            	$listingTransaction->view_marina = 'none';
            }
            if (strpos($desc, 'view of mountain') !== false || strpos($desc, 'mountain view') !== false || strpos($desc, 'mountains view') !== false || strpos($desc, 'views of mountain') !== false) {
            	$listingTransaction->view_mountains = 'full';
            }
            else{
            	$listingTransaction->view_mountains = 'none';
            }
            if (strpos($desc, 'view of lake') !== false || strpos($desc, 'lake view') !== false || strpos($desc, 'views of lake') !== false || strpos($desc, 'views of the lake') !== false) {
            	$listingTransaction->view_lake = 'full';
            }
            else{
            	$listingTransaction->view_lake = 'none';
            }
            if (strpos($desc, 'golf course view') !== false || strpos($desc, 'view of golf course') !== false || strpos($desc, 'views of golfcourse') !== false || strpos($desc, 'views of golf course') !== false || strpos($desc, 'golfcourse view') !== false || strpos($desc, 'view of golfcourse') !== false || strpos($desc, 'view of the golf course') !== false || strpos($desc, 'views of the golf course') !== false || strpos($desc, 'golf view') !== false || strpos($desc, 'view of golf') !== false) {
            	$listingTransaction->view_golf_course = 'full';
            }
            else{
            	$listingTransaction->view_golf_course = 'none';
            }
            if (strpos($desc, 'park view') !== false || strpos($desc, 'view of park') !== false || strpos($desc, 'view of the park') !== false || strpos($desc, 'views of park') !== false || strpos($desc, 'views of the park') !== false) {
            	$listingTransaction->view_park = 'full';
            }
            else{
            	$listingTransaction->view_park = 'none';
            }
            $listingTransaction->view_special = 'none';
            //End View Attributes

            //Start Price Information
            $listingPropertyType='';
            $webPageImgUrl = Yii::$app->propertyFinderHelperFunctions->getWebPageImageUrl($data['url']); //echo $webPageImgUrl; die();
            $imgHtml = Yii::$app->propertyFinderHelperFunctions->getFullPageContent($webPageImgUrl);
            if ($imgHtml<>null) {
            	$listingPropertyType = Yii::$app->propertyFinderHelperFunctions->getListingPropertyType($imgHtml);
            }
            if ($listingPropertyType<>null) {
                //Listing Property Type Working
            	$listingTransaction->listing_property_type = $listingPropertyType;
            }

            // print_r($data['listings_price']); die();
            //price working
            $listingTransaction->listings_price = trim($data['listings_price']);
            $listingTransaction->listings_rent = Yii::$app->propertyFinderHelperFunctions->getListingRent($data['property_size'],$bedrooms);
            $listingTransaction->final_price = trim($data['listings_price']);
            $listingTransaction->listing_website_link_image = ($webPageImgUrl<>null) ? $webPageImgUrl : '';
            $listingTransaction->agent_name = $data['agent_name'];
            $listingTransaction->agent_company = $data['agent_title']." at ".$data['agent_company'];
            $listingTransaction->agent_phone_no = $data['agent_telephone'];


            //Developer Margin Working
            if (strpos($desc, 'developer price') !== false || strpos($desc, 'direct from developer') !== false || strpos($desc, 'direct from the developer') !== false || strpos($desc, 'directly from the developer') !== false || strpos($desc, 'directly from developer') !== false || strpos($desc, 'payment plan') !== false) {
            	$listingTransaction->developer_margin  = 'Yes';
            }else{
            	$listingTransaction->developer_margin  = 'No';
            }

            $listingTransaction->status = 2;
            if ($listingTransaction->save()) {
            	echo " Saved Successfully ";
            //   unlink($data['file_map']);
            	Yii::$app->db->createCommand()
            	->update('property_finder_detail_urls', [ 'status' => 1 ] , [ 'detail_url' => $data['url'] ])
            	->execute();
            }
            if($listingTransaction->hasErrors()){
            // 	unlink($data['file_map']);
            	foreach($listingTransaction->getErrors() as $error){
            		if(count($error)>0){
            			foreach($error as $key=>$val){
            				echo $val. "<br>";
            			}
            			die();
            		}
            	}
            }

        }else{
          //  echo " listing already exists ";
        }
    }




















































}