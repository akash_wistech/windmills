<?php
namespace app\commands;
use Yii;
use yii\console\Controller;
use app\models\BayutToRentLinks;
use app\models\FetchBayutToRentLink;
use app\models\BayutToRentDetailLinks;
use Exception;
use app\models\Buildings;
use app\models\ListingsRent;

use app\components\traits\BayutRentProperties;

class BayutRentController extends Controller
{
  public function actionIndex()
  {
    $fetch_url = FetchBayutToRentLink::find()->one();
    if ($fetch_url==null) {
      $fetch_parent_url = BayutToRentLinks::find()->one();
      if ($fetch_parent_url<>null) {
        $model = new FetchBayutToRentLink;
        $model->parent_link_id     = $fetch_parent_url->id;
        $model->url_to_fetch       = $fetch_parent_url->url;
        $model->property_category  = $fetch_parent_url->property_category;
        if($model->save()){
          $parent_link_id     = $model->parent_link_id;
          $url                = $model->url_to_fetch;
          $property_category  = $model->property_category;
        }
      }
    }
    else{
      $parent_link_id         = $fetch_url->parent_link_id;
      $url                    = $fetch_url->url_to_fetch;
      $property_category      = $fetch_url->property_category;
    }
    $httpClient = new \Goutte\Client();
    $response = $httpClient->request('GET', $url);
    $i=1;
    $response->filter('._357a9937 li.ef447dde')->each(function ($node) use($property_category, &$i) {
      $link = $node->filter('article.ca2f5674 div._4041eb80 a')->attr('href');
      $link = 'https://www.bayut.com'.$link;
      if ($link<>null) {
        $newModel = BayutToRentDetailLinks::find()->where(['url'=>$link])->one();
        if ($newModel==null) {
          $newModel = new BayutToRentDetailLinks;
          $newModel->url = $link;
          $newModel->property_category = $property_category;
          $newModel->status = 0;
          if ($newModel->save()) {
            echo " save=> ".$i." => ".$link."<br>";
          }
        }
      }
      $i++;
    });
    $buttonLinks = [];
    $buttonTitles = [];
    $response->filter('div._035521cc div._41cc3033 div.bbfbe3d2 div._6cab5d36 ul._92c36ba1 li a')->each(function($buttonNode) use(&$buttonLinks,&$buttonTitles){
      $buttonLinks[] = $buttonNode->attr('href');
      $buttonTitles[] = $buttonNode->attr('title');
    });
    $nextPageUrl=null;
    foreach ($buttonLinks as $key => $value) {
      if ($buttonTitles[$key]=='Next') {
        $nextPageUrl = 'https://www.bayut.com'.$value;
      }
    }
    if ($nextPageUrl<>null) {
      Yii::$app->db->createCommand()->update('fetch_bayut_to_rent_link', ['parent_link_id' => $parent_link_id,'url_to_fetch'=>$nextPageUrl,'property_category'=>$property_category])->execute();
    }
    else{
      $queryTwo = \app\models\BayutToRentLinks::find()
      ->where(['>', 'id', $parent_link_id])
      ->asArray()->one();
      Yii::$app->db->createCommand()->update('fetch_bayut_to_rent_link', ['parent_link_id' => $queryTwo['id'],'url_to_fetch'=> $queryTwo['url'],'property_category'=>$queryTwo['property_category']])->execute();
    }
  }







  public function actionDetail()
  {

    $fetch_urls = BayutToRentDetailLinks::find()->where(['status'=>0])->asArray()->limit(1)->all();
    
    if ($fetch_urls<>null) {
      foreach($fetch_urls as $data){
        //   echo "<pre>"; print_r($data); echo "</pre>"; die();
        Yii::$app->db->createCommand()->update('bayut_to_rent_detail_links', ['status' => 2], ['url'=>$data['url']])->execute();
        // echo "hello"; die;
        $dataArr=[];
        try{
          $httpClient = new \Goutte\Client();
          $crawler = $httpClient->request('GET', $data['url']);
        }
        catch(Exception $err){
          $error = $err->getMessage(); print_r($error);
        }

        try {
          // Beds, Studio, Baths And Area Array
          $bed_bath_area = [];
          $crawler->filter('div._6f6bb3bc div.ba1ca68e._0ee3305d')->each(function($etcNode)use(&$bed_bath_area){
            $bed_bath_area[$etcNode->filter('span.cfe8d274')->attr('aria-label')] =  $etcNode->filter('span.fc2d1086')->text();
          });
          // echo "<pre>"; print_r($bed_bath_area); echo "</pre>"; //die();
        }
        catch ( Exception $err ) {
          $error = $err->getMessage();
          // echo "1st Catch<br>"; print_r($error); echo "<br>";
        }

        try {
          // Property Array
          $propertyInfoArr =  [];
          $i=1;
          $crawler->filter('ul._033281ab li')->each(function($propertyNode) use(&$propertyInfoArr, &$i) {
            $propertyInfoArr[$propertyNode->filter('span._3af7fa95')->text()] = $propertyNode->filter('span._812aa185')->text();
            $i++;
          });
        }
        catch (Exception $err) {
          $error = $err->getMessage();
          // echo "2nd Catch<br>"; print_r($error); echo "<br>";

          try{
            $j =1;
            $crawler->filter('ul._033281ab li')->each(function($propertyNode) use(&$propertyInfoArr, &$j, &$i) {
              if ($j==$i) {
                $propertyInfoArr["TruCheck ".$propertyNode->filter('div.f8dc1def span._405c89ee')->text()] = $propertyNode->filter('span._812aa185')->text();
              }
              if ($j>$i) {
                $propertyInfoArr[$propertyNode->filter('span._3af7fa95')->text()] = $propertyNode->filter('span._812aa185')->text();
              }
              $j++;
            });
          }
          catch(Exception $err){
            $error = $err->getMessage();
            // echo "3rd Catch<br>"; print_r($error); echo "<br>";
          }
        }

        try{
          $title = $crawler->filter('div._1f0f1758')->text();
          $titleExplode = explode(',', $title);
          $buildingTitle = $community = $sub_community = $city = '';
          if (isset($titleExplode[0]) AND $titleExplode[0]<>null) {
            $buildingTitle = $titleExplode[0];
          }
          if (isset($titleExplode[1]) AND $titleExplode[1]<>null) {
            $community = $titleExplode[1];
          }
          if (isset($titleExplode[3]) AND $titleExplode[3]<>null) {
            if (isset($titleExplode[2]) && $titleExplode[2]<>null) {
              $sub_community = $titleExplode[2];
            }
          }
          $city = array_pop($titleExplode);

          if ($data['property_category'] == 'residential') {
            $property_type = 1;
          }
          if ($data['property_category'] == 'commercial') {
            $property_type = 4;
          }

          $propertyDescription = $crawler->filter('div._2015cd68')->text();
          $listingAndFinalPrice = $crawler->filter('div.c4fc20ba span._105b8a67')->text();

        }
        catch(Exception $err){
          $error = $err->getMessage();
          // echo "4th Catch<br>"; print_r($error); echo "<br>";
        }

        $dataArr['url'] = $data['url'];

        if ($bed_bath_area<>null) {
          $bed_bath_area_studio_arr = BayutRentProperties::get_Bed_Bath_Area($bed_bath_area);
          if ($bed_bath_area_studio_arr<>null) {
            $dataArr['no_of_bedrooms'] = trim($bed_bath_area_studio_arr['no_of_bedrooms']);
            $dataArr['no_of_bathrooms'] = trim($bed_bath_area_studio_arr['no_of_bathrooms']);
            $dataArr['property_size'] = trim($bed_bath_area_studio_arr['area']);
            $dataArr['studio'] = trim($bed_bath_area_studio_arr['studio']);
          }
        }
        
        
        if ($propertyInfoArr<>null) {
          $propertyInfoArr = BayutRentProperties::getPropertyData($propertyInfoArr);
          if ($propertyInfoArr<>null) {
            $dataArr ['property_type'] = trim(strtolower($propertyInfoArr['type']));
            $dataArr ['purpose'] = trim($propertyInfoArr['purpose']);
            $dataArr ['ref_number'] = trim($propertyInfoArr['ref_number']);
            $dataArr ['truCheck_on'] = trim($propertyInfoArr['truCheck_on']);
            $dataArr ['added_on'] = trim($propertyInfoArr['added_on']);
          }
        }

        $dataArr['property_category'] = trim($property_type);
        $dataArr['building_title'] = trim($buildingTitle);
        $dataArr['community'] = trim($community);
        $dataArr['sub_community'] = trim($sub_community);
        $dataArr['city'] = trim($city);
        $dataArr['listing_price'] = trim(str_replace("," , "", $listingAndFinalPrice));
        $dataArr['desc'] = trim(strtolower($propertyDescription));

        $this->getDecision($dataArr);
        // echo "DataStart:<pre>"; print_r($dataArr); echo "</pre>:DataEnd<br>"; //die();
      }
    }
  }





  public function getDecision($data=null)
  {
    // echo "<pre>"; print_r($data); echo "</pre>"; die();

    if ($data['building_title']<>null) {
      $city_id = Yii::$app->appHelperFunctions->getCityId($data['city']);
      // echo $city_id; //die();
      $commAndSubCommId = Yii::$app->propertyFinderHelperFunctions->getCommAndSubCommIds($data['community'],$data['sub_community']);
      // echo "<pre>"; print_r($commAndSubCommId); echo "</pre>"; die();

      $building = Buildings::find()->where([
        'title'         => trim($data['building_title']),
        // 'title'         => trim('Mudon Villas - Arabella 1 (Al Hebiah Sixth)'),
        'city'          => $city_id,
      ]);
      if ($commAndSubCommId<>null&&$commAndSubCommId['community_id']<>null&&$commAndSubCommId['sub_community_id']<>null) {
        $building->andWhere([
          'community'     => $commAndSubCommId['community_id'],
          'sub_community' => $commAndSubCommId['sub_community_id'],
        ]);
      }
      $building->andWhere(['city'=>$city_id]);
      $building->orWhere(['like', 'title', trim($data['building_title']) . '%', false]);
      // $building->orWhere(['like', 'title', trim('Mudon Villas - Arabella 1 (Al Hebiah Sixth)') . '%', false]);
      $model = $building->one();
      if ($model<>null) {
        // echo "<pre>"; print_r($model); echo "</pre>"; die();
        $this->saveListingTransaction($data,$model);
      }else{
        echo "Building not found";
      }
    }
  }
















































  public function saveListingTransaction($data=null,$model)
  {
    // echo "saveListingTransaction<pre>"; print_r($data); echo "</pre><br><br>";
    // // echo "saveListingTransaction<pre>"; print_r($model); echo "</pre><br><br>";
    // die();

    $desc = $data['desc'];



    $white_goods = $furnished = $pool = $gym = $view_sea = '';
    if ($desc<>null) {
      if (strpos($desc, 'built in kitchen appliances') !== false) {
        $white_goods  = 'Yes';
      }if (strpos($desc, 'furnished') !== false) {
        $furnished  = 'Yes';
      }if (strpos($desc, 'private pool') !== false) {
        $pool  = 'Yes';
      }if (strpos($desc, 'private Gym') !== false) {
        $gym  = 'Yes';
      }if (strpos($desc, 'view of water') !== false) {
        $view_sea  = 'Yes';
      }
    }

    $bedrooms = '';
    if ($data['no_of_bedrooms']<>null && is_numeric($data['no_of_bedrooms'])) {
      $bedrooms = $data['no_of_bedrooms'];
    }



    $findListing = ListingsRent::find()->where(['listings_reference'=>$data['ref_number'], 'listing_website_link'=>$data['url']])->one();
    if ($findListing==null) {
      $listingTransaction = new ListingsRent;
      $listingTransaction->listings_reference   = $data['ref_number']; //done
      $listingTransaction->source               = 15; //done
      $listingTransaction->listing_website_link = $data['url']; //done
      $listingTransaction->listing_date         = $data['added_on']; //done
      $listingTransaction->unit_number          = 'Not Known'; //done

      $listingTransaction->building_info = $model->id;

      // property category working
      $listingTransaction->property_category = $data['property_category'];

      //tenure working
      if ($model->tenure<>null) {
        $listingTransaction->tenure = $model->tenure;
      }
      else if(strpos($desc, 'freehold') !== false || strpos($desc, 'free hold') !== false || strpos($desc, 'free-hold') !== false)
      {
        $listingTransaction->tenure   = 2;
      }
      else{
        if ($data['city']=='Dubai') {
          $listingTransaction->tenure   = 2;
        }else{
          $listingTransaction->tenure   = 1;
        }
      }

      //property placement working
      if (strpos($data['property_type'], 'apartment') !== false || strpos($data['property_type'], 'office') !== false) {
        $listingTransaction->property_placement  = 3.00;
      }else{
        if (strpos($desc, 'middle') !== false || strpos($desc, 'mid unit') !== false || strpos($desc, 'mid-unit') !== false) {
          $listingTransaction->property_placement  = 1.00;
        }
        else if(strpos($desc, 'corner') !== false){
          $listingTransaction->property_placement  = 2.00;
        }
        else if (strpos($desc, 'semi corner') !== false || strpos($desc, 'semi-corner') !== false || strpos($desc, 'semicorner') !== false || strpos($desc, 'end unit') !== false) {
          $listingTransaction->property_placement  = 1.50;
        }
        else{
          $listingTransaction->property_placement  = 1.00;
        }
      }

      //property visibility
      if ($data['property_type'] == 'shop') {
        $listingTransaction->property_visibility = 2;

        if(strpos($desc, 'back') !== false || strpos($desc, 'behind') !== false){
          $listingTransaction->property_visibility = 1;
        }
      }else{
        $listingTransaction->property_visibility = 3;
      }

      //Property Exposure working
      if (strpos($data['property_type'], 'apartment') !== false || strpos($data['property_type'], 'office') !== false) {
        $listingTransaction->property_exposure   = 3;
      }
      else{
        if (strpos($desc, 'single row') !== false || strpos($desc, 'single-row') !== false) {
          $listingTransaction->property_exposure   = 2;
        }
        else if(strpos($desc, 'back to back') !== false || strpos($desc, 'backtoback') !== false ||
        strpos($desc, 'back 2 back') !== false || strpos($desc, 'back2back') !== false ||
        strpos($desc, 'b2b') !== false || strpos($desc, 'back-to-back') !== false){
          $listingTransaction->property_exposure   = 1;
        }
        else{
          $listingTransaction->property_exposure   = 1;
        }
      }


      $listingTransaction->property_condition  = 3;
      $listingTransaction->development_type    = 'Standard';

      //Utilities connected working
      if (strpos($data['property_type'], 'land') !== false) {
        $listingTransaction->utilities_connected = 'No';
      }else{
        $listingTransaction->utilities_connected = 'Yes';
      }

      //Finished status working
      if (strpos($data['property_type'], 'office') !== false) {
        $listingTransaction->finished_status     = 'Fitted';

        if(strpos($desc, 'fitted') !== false){
          $listingTransaction->finished_status     = 'Shell & Core';
        }
      }else{
        $listingTransaction->finished_status     = 'Fitted';
      }


      // Pool working
      if ($pool=='Yes') {
        $listingTransaction->pool = 'Yes';
      }
      else if (strpos($desc, 'private pool') !== false ||strpos($desc, 'private swimming pool') !== false ||strpos($desc, 'with pool') !== false ||strpos($desc, 'own swimming pool') !== false || strpos($desc, 'own pool') !== false) {
        $listingTransaction->pool = 'Yes';
      }
      else{
        $listingTransaction->pool = 'No';
      }

      // Gym Working
      if ($gym=='Yes') {
        $listingTransaction->gym = 'Yes';
      }
      else if (strpos($desc, 'private gym') !== false ||strpos($desc, 'own gym') !== false || strpos($desc, 'private fitness') !== false) {
        $listingTransaction->gym = 'Yes';
      }
      else{
        $listingTransaction->gym = 'No';
      }


      // Play area Working
      if (strpos($desc, 'private play') !== false || strpos($desc, 'own play') !== false) {
        $listingTransaction->play_area = 'Yes';
      }
      else{
        $listingTransaction->play_area = 'No';
      }

      $listingTransaction->other_facilities    = ($model->other_facilities<>null)? explode(',' , $model->other_facilities) : '';
      $listingTransaction->completion_status   = 100.00;



      //White goods working
      if ($white_goods=='Yes') {
        $listingTransaction->white_goods  = 'Yes';
      }
      else if (strpos($desc, 'equipped kitchen') !== false || strpos($desc, 'fully equipped') !== false || strpos($desc, 'fitted kitchen') !== false) {
        $listingTransaction->white_goods  = 'Yes';
      }
      else{
        $listingTransaction->white_goods  = 'No';
      }

      //Furnished Working
      if ($furnished == 'Yes') {
        $listingTransaction->furnished = 'Yes';
      }
      else if (strpos($desc, 'fully furnished') !== false) {
        $listingTransaction->furnished = 'Yes';
      }
      else{
        $listingTransaction->furnished = 'No';
      }

      //Landscaping working
      if (strpos($data['property_type'], 'villa') !== false || strpos($data['property_type'], 'townhouse') !== false) {
        $listingTransaction->landscaping = 'Yes';
      }
      else{
        $listingTransaction->landscaping  = 'No';
      }

      // no of bedroom
      $listingTransaction->no_of_bedrooms = ($bedrooms<>null AND $bedrooms!='') ? $bedrooms : 0;

      //Upgrades
      if (strpos($desc, 'upgrades') !== false || strpos($desc, 'upgraded') !== false) {
        $listingTransaction->upgrades  = 4;
      }
      else{
        $listingTransaction->upgrades  = 3;
      }

      // Full Building Floors working
      if (strpos($data['property_type'], 'land') !== false) {
        $listingTransaction->full_building_floors = 0;
      }
      else if (strpos($data['property_type'], 'villa') !== false  || strpos($data['property_type'], 'townhouse') !== false  ) {
        if ($model->typical_floors<>null) {
          $listingTransaction->full_building_floors = $model->typical_floors;
        }
        else{
          $listingTransaction->full_building_floors = 1;
        }
      }
      else{
        if ($model->typical_floors<>null) {
          $listingTransaction->full_building_floors = $model->typical_floors;
        }
        else{
          $listingTransaction->full_building_floors = 0;
        }
      }


      //Parking Space working
      if (strpos($data['property_type'], 'villa') !== false || strpos($data['property_type'], 'townhouse') !== false) {
        $listingTransaction->parking_space  = 2;
      }
      if (strpos($data['property_type'], 'apartment') !== false) {
        if ($data['studio']<>null && $data['studio']=='yes' && $bedrooms==1) {
          $listingTransaction->parking_space  = 1;
        }
        else if ($data['studio']<>null && $data['studio']=='yes' && $bedrooms==2) {
          $listingTransaction->parking_space  = 1;
        }
        else if ($bedrooms>=3) {
          $listingTransaction->parking_space  = 2;
        }
        else{
          $listingTransaction->parking_space  = 1;
        }
      }
      else{
        $listingTransaction->parking_space  =0;
      }

      //Estiated age working
      if (strpos($data['property_type'], 'land') !== false) {
        $listingTransaction->estimated_age = 0;
      }else{
        if ($model->estimated_age<>null) {
          $listingTransaction->estimated_age = $model->estimated_age;
        }else{
          $listingTransaction->estimated_age = 0;
        }
      }

      //Floor Number Working
      if (strpos($data['property_type'], 'apartment') !== false || strpos($data['property_type'], 'office') !== false) {
        if (strpos($desc, 'high floor') !== false || strpos($desc, 'higher floor') !== false || strpos($desc, 'high-floor') !== false) {
          $listingTransaction->floor_number = round(3/4*$model->typical_floors);
        }
        else if (strpos($desc, 'low floor') !== false || strpos($desc, 'lower floor') !== false || strpos($desc, 'low-floor') !== false) {
          $listingTransaction->floor_number = round(1/4*$model->typical_floors);
        }
        else{
          $listingTransaction->floor_number = round(1/2*$model->typical_floors);
        }
      }else{
        $listingTransaction->floor_number = 0;
      }


      //No of levels Working
      if (strpos($data['property_type'], 'land') !== false || strpos($data['property_type'], 'apartment') !== false || strpos($data['property_type'], 'warehouse') !== false || strpos($data['property_type'], 'office') !== false) {
        $listingTransaction->number_of_levels = 1;
      }
      else if (strpos($data['property_type'], 'villa') !== false || strpos($data['property_type'], 'townhouse') !== false || strpos($data['property_type'], 'duplex') !== false) {
        if ($model->typical_floors<>null) {
          $listingTransaction->number_of_levels = $model->typical_floors;
        }
        else{
          $listingTransaction->number_of_levels = 2;
        }
      }
      else{
        if ($model->typical_floors<>null) {
          $listingTransaction->number_of_levels = $model->typical_floors;
        }else{
          $listingTransaction->number_of_levels = 2;
        }
      }


      //Start bua and land size
      if (strpos($data['property_type'], 'apartment') !== false || strpos($data['property_type'], 'office') !== false) {
        // die("hello");
        $listingTransaction->land_size = 0;
        $listingTransaction->built_up_area = $data['property_size'];
      }
      else{
        // echo "ello"; die();
        $landAndBuiltUpArea = BayutRentProperties::getBuaAndLandArea($desc,$data['property_size'],$bedrooms);
        // echo "<br>In main function: <pre>"; print_r($landAndBuiltUpArea); echo "</pre>"; die();
        $listingTransaction->built_up_area = $landAndBuiltUpArea['built_up_area'];
        $listingTransaction->land_size = $landAndBuiltUpArea['land_size'];
      }
      //End bua and land size

      $listingTransaction->balcony_size = '0';

      //Start Location Attributes
      $listingTransaction->location_highway_drive = ($model->location_highway_drive<>null) ? $model->location_highway_drive : 'minutes_5';
      $listingTransaction->location_school_drive  = ($model->location_school_drive<>null) ? $model->location_school_drive : 'minutes_5';
      $listingTransaction->location_mall_drive    = ($model->location_mall_drive<>null) ? $model->location_mall_drive : 'minutes_5';
      $listingTransaction->location_sea_drive     = ($model->location_sea_drive<>null) ? $model->location_sea_drive : 'minutes_5';
      $listingTransaction->location_park_drive    = ($model->location_park_drive<>null) ? $model->location_park_drive : 'minutes_5';
      //End Location Attributes


      //Start View Attributes
      $listingTransaction->view_community = 'normal';

      if (strpos($desc, 'shared pool') !== false) {
        $listingTransaction->view_pool = 'none';
      }
      else if (strpos($desc, 'view of pool') !== false || strpos($desc, 'view of fountain') !== false || strpos($desc, 'fountain view') !== false) {
        $listingTransaction->view_pool = 'full';
      }
      else{
        $listingTransaction->view_pool = 'none';
      }
      if (strpos($desc, 'view of burj') !== false || strpos($desc, 'burj khalifa view') !== false || strpos($desc, 'burj view') !== false || strpos($desc, 'burj al arab view') !== false) {
        $listingTransaction->view_burj = 'full';
      }
      else{
        $listingTransaction->view_burj = 'none';
      }
      if ($view_sea=='Yes') {
        $listingTransaction->view_sea = 'full';
      }
      else if (strpos($desc, 'sea view') !== false || strpos($desc, 'view of sea') !== false || strpos($desc, 'view of water') !== false || strpos($desc, 'water view') !== false) {
        $listingTransaction->view_sea = 'full';
      }
      else{
        $listingTransaction->view_sea = 'none';
      }
      if (strpos($desc, 'marina view') !== false || strpos($desc, 'view of marina') !== false || strpos($desc, 'view of dubai marina') !== false || strpos($desc, 'views of dubai marina') !== false || strpos($desc, 'views of marina') !== false) {
        $listingTransaction->view_marina = 'full';
      }
      else{
        $listingTransaction->view_marina = 'none';
      }
      if (strpos($desc, 'view of mountain') !== false || strpos($desc, 'mountain view') !== false || strpos($desc, 'mountains view') !== false || strpos($desc, 'views of mountain') !== false) {
        $listingTransaction->view_mountains = 'full';
      }
      else{
        $listingTransaction->view_mountains = 'none';
      }
      if (strpos($desc, 'view of lake') !== false || strpos($desc, 'lake view') !== false || strpos($desc, 'views of lake') !== false || strpos($desc, 'views of the lake') !== false) {
        $listingTransaction->view_lake = 'full';
      }
      else{
        $listingTransaction->view_lake = 'none';
      }
      if (strpos($desc, 'golf course view') !== false || strpos($desc, 'view of golf course') !== false || strpos($desc, 'views of golfcourse') !== false || strpos($desc, 'views of golf course') !== false || strpos($desc, 'golfcourse view') !== false || strpos($desc, 'view of golfcourse') !== false || strpos($desc, 'view of the golf course') !== false || strpos($desc, 'views of the golf course') !== false || strpos($desc, 'golf view') !== false || strpos($desc, 'view of golf') !== false) {
        $listingTransaction->view_golf_course = 'full';
      }
      else{
        $listingTransaction->view_golf_course = 'none';
      }
      if (strpos($desc, 'park view') !== false || strpos($desc, 'view of park') !== false || strpos($desc, 'view of the park') !== false || strpos($desc, 'views of park') !== false || strpos($desc, 'views of the park') !== false) {
        $listingTransaction->view_park = 'full';
      }
      else{
        $listingTransaction->view_park = 'none';
      }
      $listingTransaction->view_special = 'none';
      //End View Attributes



      //Start Price Information
      $listingPropertyType='';
      $webPageImgUrl = Yii::$app->propertyFinderHelperFunctions->getWebPageImageUrl($data['url']); //echo $webPageImgUrl; die();
      $imgHtml = Yii::$app->propertyFinderHelperFunctions->getFullPageContent($webPageImgUrl);
      if ($imgHtml<>null) {
        $listingPropertyType = Yii::$app->propertyFinderHelperFunctions->getListingPropertyType($imgHtml);
        $agent_name = BayutRentProperties::getAgentName($imgHtml);
      }
      if ($listingPropertyType<>null) {
        //Listing Property Type Working
        $listingTransaction->listing_property_type = $listingPropertyType;
      }

      // echo "agent_name:- ". $agent_name; die();
      //price working
    //   $listingTransaction->listings_price = trim($data['listing_price']);
      $listingTransaction->listings_rent = trim($data['listing_price']); //Yii::$app->propertyFinderHelperFunctions->getListingRent($data['property_size'],$bedrooms);
      $listingTransaction->final_price = trim($data['listing_price']);
      $listingTransaction->listing_website_link_image = ($webPageImgUrl<>null) ? $webPageImgUrl : '';
      $listingTransaction->agent_name = $agent_name; //BayutRentProperties::getAgentName($imgHtml);


      //Developer Margin Working
      if (strpos($desc, 'developer price') !== false || strpos($desc, 'direct from developer') !== false || strpos($desc, 'direct from the developer') !== false || strpos($desc, 'directly from the developer') !== false || strpos($desc, 'directly from developer') !== false || strpos($desc, 'payment plan') !== false) {
        $listingTransaction->developer_margin  = 'Yes';
      }else{
        $listingTransaction->developer_margin  = 'No';
      }

      $listingTransaction->status = 2;
      if ($listingTransaction->save()) {
        echo " Saved Successfully ";
        Yii::$app->db->createCommand()
        ->update('bayut_to_rent_detail_links', [ 'status' => 1 ] , [ 'url' => $data['url'] ])
        ->execute();
      }
      if($listingTransaction->hasErrors()){
        foreach($listingTransaction->getErrors() as $error){
          if(count($error)>0){
            foreach($error as $key=>$val){
              echo $val. "<br>";
            }
            die();
          }
        }
      }

    }else{
      //  echo " listing already exists ";
    }
  }

}
