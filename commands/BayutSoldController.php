<?php
namespace app\commands;
use app\models\SoldTransaction;
use Yii;
use yii\console\Controller;
use app\components\traits\BayutRentProperties;
use app\models\UaeByCityDltUrl;
use app\models\UaeByCityUrl;
use Exception;
use yii\db\Expression;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Symfony\Component\DomCrawler\Crawler;
use app\models\SoldData;
use app\models\AutoLinksSold;
use app\models\Buildings;
use app\models\Company;

class BayutSoldController extends Controller
{
    public function actionIndex()
    {      
        $start = 00;
        $end = 24;
        $now = date('H');
        if ($now >= $start && $now <= $end) {

            $all_links = AutoLinksSold::find()->where(['status'=>0])->all();

            $saved=0;
            $unsaved=0;
            $errNames='';
            $change=array();
            $curl = curl_init();

            foreach ($all_links as $key => $link_data){
                $query = Yii::$app->db->createCommand()
                    ->update('auto_links_sold', ['status' => 1], 'id = ' . $link_data->id . '')
                    ->execute();
                $url = $link_data->link;


                curl_setopt_array($curl, array(
                 //   CURLOPT_URL => 'https://api.scrapingant.com/v2/general?url='.$url.'%2F&x-api-key=f541ef233e7d4dcdb9d5bd2658832f8d', //cs
                 // ansin   CURLOPT_URL => 'https://api.scrapingant.com/v2/general?url='.$url.'%2F&x-api-key=b5aebb01ca274f59a178bef7ea090250',
                  //ash=fand
                    CURLOPT_URL => 'https://api.scrapingant.com/v2/general?url='.$url.'%2F&x-api-key=72cf1ff78e104cfab0bfcc3db88b5764',


                  //maxima  CURLOPT_URL => 'https://api.scrapingant.com/v2/general?url='.$url.'%2F&x-api-key=e59dc908e95c4e97a437eb2557c90290',
                   //wistech CURLOPT_URL => 'https://api.scrapingant.com/v2/general?url='.$url.'%2F&x-api-key=fc0416bae97a47e4a51af30e212232d1',
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                ));

                $response = curl_exec($curl);

                curl_close($curl);


                // Load the HTML content of the page
                $html = $response;

                $crawler = new Crawler($html);
                $title = $crawler->filter('table')->text();
                $table = $crawler->filter('table')->first();
                $rows = $table->filter('tr')->each(function (Crawler $row, $i) {
                    return $row->filter('td')->each(function (Crawler $cell, $j) {
                        return $cell->text();
                    });
                });
                $result = array_values(array_filter($rows));

                if (!empty($result)) {
                    foreach ($result as $key => $line) {
                        $buildingName = $line[0];
                        $type = $line[1];
                        $rooms = $line[2];
                        $bua_raw = str_replace(" sqft", "", $line[3]);
                        $bua = str_replace(",", "", $bua_raw);
                        $plotArea = str_replace(",", "", $line[4]);
                        $unit_number = str_replace(",", "", $line[5]);

                        $date = str_replace(' ', '-', trim($line[6]));
                        // echo $date; die();
                        $date = date("Y-m-d", strtotime($date));
                        $price_raw = str_replace("AED", "", $line[7]);
                        $price = str_replace(",", "", $price_raw);
                        $price = str_replace(",", "", $price);


                        if ($bua <> null) {
                            $pricePerSqt = number_format(floatval($price) / floatval($bua));
                        } else {
                            $pricePerSqt = 0;
                        }
                        /*  echo "<pre>";
                                        // print_r($price_raw);
                                         print_r($price);
                                      //   print_r($line);
                                         die;*/
                        $query['building_info'] = $buildingName;
                        $query['city_id'] = 3510;
                        $building_find = \app\models\Buildings::find()
                            ->where(new Expression(' title LIKE "%' . $query['building_info'] . '%" '))
                            ->andWhere(['city' => $query['city_id']])
                            ->asArray()
                            ->one();

                        if ($building_find <> null) {
                            $buildingRow = $building_find;
                        } else {
                            $buildingRow = $this->findbuilding($query);
                        }
                        if ($buildingRow == null) {
                            $buildingRow = $this->findbuilding($query);
                        }


                        $buildingRow = Buildings::find()->where(['=', 'auto_title', trim($buildingName)])->orWhere(['=', 'title', trim($buildingName)])->asArray()->one();

                        if ($buildingRow <> null && $buildingRow['id'] <> null) {

                            $comparison = SoldTransaction::find()
                                ->where(['building_info' => $buildingRow['id'],
                                    'no_of_bedrooms' => $rooms,
                                    'built_up_area' => $bua,
                                    'land_size' => $plotArea,
                                    'transaction_date' => $date,
                                    'listings_price' => $price,
                                    'price_per_sqt' => $pricePerSqt])
                                // ->asArray()
                                ->one();

                            if ($comparison == null) {
                                // echo "Record Found"; die();
                                $buildingId = $buildingRow['id'];


                                $model = new SoldTransaction;
                                $model->scenario = 'import';
                                $model->building_info = $buildingId;
                                $model->no_of_bedrooms = ($rooms != 'Unknown') ? $rooms : 0;
                                $model->built_up_area = ($bua <> null && $bua !== '-') ? $bua : 0;
                                $model->land_size = ($plotArea <> null && $plotArea !== '-') ? floatval($plotArea) : 0;;
                                //$model->type=$type;
                                $model->transaction_date = $date;
                                $model->listings_price = ($price <> null && $price !== '-') ? floatval($price) : 0;

                                $model->property_category = $buildingRow['property_category'];
                                $model->location = $buildingRow['location'];
                                $model->tenure = $buildingRow['tenure'];
                                $model->utilities_connected = $buildingRow['utilities_connected'];
                                $model->development_type = $buildingRow['development_type'];
                                $model->property_placement = $buildingRow['property_placement'];
                                $model->property_visibility = $buildingRow['property_visibility'];
                                $model->property_exposure = $buildingRow['property_exposure'];
                                $model->property_condition = $buildingRow['property_condition'];
                                $model->pool = $buildingRow['pool'];
                                $model->gym = $buildingRow['gym'];
                                $model->play_area = $buildingRow['play_area'];
                                // $model->other_facilities = $buildingRow['other_facilities'];
                                $model->landscaping = $buildingRow['landscaping'];
                                $model->white_goods = $buildingRow['white_goods'];
                                $model->furnished = $buildingRow['furnished'];
                                $model->finished_status = $buildingRow['finished_status'];


                                $model->price_per_sqt = floatval($pricePerSqt);
                                $model->list_type = 1;
                                if ($model->save()) {
                                    $saved++;
                                } else {
                                    if ($model->hasErrors()) {
                                        foreach ($model->getErrors() as $error) {
                                            if (count($error) > 0) {
                                                foreach ($error as $key => $val) {
                                                    echo $val;
                                                }
                                            }
                                        }
                                    }

                                    $errNames .= '<br />' . $buildingName;
                                    $unsaved++;
                                }


                            }


                        } else {

                            $soldmax = new SoldData();
                            $soldmax->building_title = $buildingName;
                            $soldmax->property_type = $type;
                            $soldmax->bedrooms = $rooms;
                            $soldmax->bua = $bua;
                            $soldmax->plot_area = $plotArea;
                            $soldmax->unit_number = $unit_number;
                            $soldmax->listing_date = $date;
                            $soldmax->price = $price;
                            $soldmax->created_at = date('Y-m-d h:i:s');
                            $soldmax->save();


                            $change[] = $buildingName;
                        }


                    }
                    $query = Yii::$app->db->createCommand()
                        ->update('auto_links_sold', ['status' => 1], 'id = ' . $link_data->id . '')
                        ->execute();

                }
            }
        }else{ 
            echo "time out"; 
        }
        
    }
    public function findbuilding($query)
    {

        $length = strlen($query['building_info']); //echo $length."<br>";
        for ($i = 1; $i <= $length; $i++) {

            $remove_char = mb_substr($query['building_info'], '-'.$i);
            $trim_building_info  =  rtrim($query['building_info'], $remove_char);

            $building = \app\models\Buildings::find()
                ->where(new Expression(' title LIKE "%'.$trim_building_info.'%" '))
                ->andWhere(['city' => $query['city_id']])
                ->asArray()
                ->one();

            if($building<>null){
                return $building;
                break;
            }
        }

    }
    
}