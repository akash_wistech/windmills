<?php
namespace app\commands;
use Yii;

use yii\console\Controller;
use app\models\Buildings;
use app\models\ListingsTransactions;

use app\components\traits\bayutRentProperties;

use app\models\BayutDetailPageLinks;
use app\models\BayutLinks;
use app\models\BayutLinksClild;
use app\models\Valuation;

use Exception;

class BayutFilterController extends Controller
{
  public function actionForSaleIndex()
  {







      $fetch_url = BayutLinksClild::find()->one();
    if ($fetch_url==null) {
      // die("here2");
      $fetch_parent_url = Valuation::find()
      ->select(['link_to_scrape','id'])
      ->where(['link_to_scrape_status'=>0])
      ->andWhere(['not', ['link_to_scrape' => null]])
      ->andWhere(['not', ['link_to_scrape' => '']])
      // ->asArray()
      ->one();
      
      
      if ($fetch_parent_url<>null) {
        $model = new BayutLinksClild;
        $model->link_id       = $fetch_parent_url->id;
        $model->url_to_fetch  = $fetch_parent_url->link_to_scrape;
        // $model->property_category  = $fetch_parent_url->property_category;
        if($model->save()){
          $parent_link_id     = $model->link_id;
          $url                = $model->url_to_fetch;
          // $property_category  = $model->property_category;
        }
      }
    }
    else{


        // die("here");
      $parent_link_id         = $fetch_url->link_id;
      $url                    = $fetch_url->url_to_fetch;
        $fetch_url_check = BayutLinksClild::find()->one();
        $newModel_check = BayutDetailPageLinks::find()->where(['url_child'=>$fetch_url_check->url_to_fetch])->andWhere(['status'=>0])->one();
        if($newModel_check ==  null){
            Yii::$app->db->createCommand()->delete('bayut_links_clild', [ 'url_to_fetch'=> $fetch_url_check->url_to_fetch ])->execute();
        }
      // $property_category      = $fetch_url->property_category;
    }
    
    // echo"<pre>"; print_r($parent_link_id); echo "</pre>";
    // echo"<pre>"; print_r($url); echo "</pre>"; die;
    
    
    
    $httpClient = new \Goutte\Client();
    $response = $httpClient->request('GET', $url);
    $i=1;
    $response->filter('._357a9937 li.ef447dde')->each(function ($node) use($url, &$i) {
      $link = $node->filter('article.ca2f5674 div._4041eb80 a')->attr('href');
      $link = 'https://www.bayut.com'.$link;
      if ($link<>null) {
        $newModel = BayutDetailPageLinks::find()->where(['url'=>$link])->one();
        if ($newModel==null) {
          $newModel = new BayutDetailPageLinks;
          $newModel->url = $link;
          $newModel->url_child = $url;
          // $newModel->property_category = $property_category;
          $newModel->status = 0;
          if ($newModel->save()) {
            echo " save=> ".$i." => ".$link."<br>";
          }
        }
      }
      $i++;
    });
    
    // die();
    
    
    $buttonLinks = [];
    $buttonTitles = [];
    $response->filter('div._035521cc div._41cc3033 div.bbfbe3d2 div._6cab5d36 ul._92c36ba1 li a')->each(function($buttonNode) use(&$buttonLinks,&$buttonTitles){
      $buttonLinks[] = $buttonNode->attr('href');
      $buttonTitles[] = $buttonNode->attr('title');
    });
    $nextPageUrl=null;
    foreach ($buttonLinks as $key => $value) {
      if ($buttonTitles[$key]=='Next') {
        $nextPageUrl = 'https://www.bayut.com'.$value;
      }
    }
    
    // echo"<pre>"; print_r($nextPageUrl); echo "</pre>"; die;
    
    
    
    
    if ($nextPageUrl<>null) {
      Yii::$app->db->createCommand()->update('bayut_links_clild', [ 'url_to_fetch'=>$nextPageUrl ])->execute();
      
    }
    else{
      // echo $parent_link_id; die;
      // Yii::$app->db->createCommand()->update('valuation', [ 'url_to_fetch'=>$nextPageUrl ])->execute();
      Yii::$app->db->createCommand()
      ->update('valuation', ['link_to_scrape_status' => 1], 'id ='.$parent_link_id)
      ->execute();
      
      $queryTwo = Valuation::find()
      ->select(['id', 'link_to_scrape'])
      ->where(['>', 'id', $parent_link_id])
      ->andWhere(['link_to_scrape_status'=>0])
      ->andWhere(['not', ['link_to_scrape' => null]])
      ->andWhere(['not', ['link_to_scrape' => '']])
      ->asArray()
      ->one();
      if($queryTwo<>null){
        Yii::$app->db->createCommand()->update('bayut_links_clild', [ 'link_id' => $queryTwo['id'],'url_to_fetch'=> $queryTwo['link_to_scrape'] ])->execute();
      }
    }
  }
  
  
  
  
  
  public function actionForSaleDetail()
  {



      $fetch_urls = BayutDetailPageLinks::find()->where(['status'=>0])->asArray()->limit(20)->all();
    // echo "<pre>"; print_r($fetch_urls); echo "</pre>"; die();
    if ($fetch_urls<>null) {
      foreach($fetch_urls as $data){
        Yii::$app->db->createCommand()->update('bayut_detail_page_links', ['status' => 2], ['url'=>$data['url']])->execute();
        // die("here");
        $dataArr=[];
        try{
          $httpClient = new \Goutte\Client();
          $crawler = $httpClient->request('GET', $data['url']);
        }
        catch(Exception $err){
         // $error = $err->getMessage(); print_r($error);
        }
        
        try {

          // Beds, Studio, Baths And Area Array
          $bed_bath_area = [];
          $crawler->filter('div._6f6bb3bc div.ba1ca68e._0ee3305d')->each(function($etcNode)use(&$bed_bath_area){
            $bed_bath_area[$etcNode->filter('span.cfe8d274')->attr('aria-label')] =  $etcNode->filter('span.fc2d1086')->text();
          });
          //  error_log('test3');
          // echo "<pre>"; print_r($bed_bath_area); echo "</pre>"; //die();
        }
        catch ( Exception $err ) {
          $error = $err->getMessage();
          //  error_log('tes4');
          // echo "1st Catch<br>"; print_r($error); echo "<br>";
        }
        
        try {
          // Property Array
         //   error_log('test3.1');
          $propertyInfoArr =  [];
          $i=1;
          $crawler->filter('ul._033281ab li')->each(function($propertyNode) use(&$propertyInfoArr, &$i) {
            $propertyInfoArr[$propertyNode->filter('span._3af7fa95')->text()] = $propertyNode->filter('span._812aa185')->text();
            $i++;
          });

        }
        catch (Exception $err) {
         //   error_log($err->getMessage());
         // $error = $err->getMessage();
          // echo "2nd Catch<br>"; print_r($error); echo "<br>";
          
          try{
            $j =1;
            $crawler->filter('ul._033281ab li')->each(function($propertyNode) use(&$propertyInfoArr, &$j, &$i) {
              if ($j==$i) {
                $propertyInfoArr[$propertyNode->filter('div._3af7fa95')->text()] = $propertyNode->filter('span._812aa185')->text();
              }
              if ($j>$i) {
                $propertyInfoArr[$propertyNode->filter('span._3af7fa95')->text()] = $propertyNode->filter('span._812aa185')->text();
              }
              $j++;
            });
          }
          catch(Exception $err){
            //$error = $err->getMessage();
            // echo "3rd Catch<br>"; print_r($error); echo "<br>";
          }
        }
        
        // echo "<pre>"; print_r($propertyInfoArr); echo "</pre>";
        
        
        try{
          $title = $crawler->filter('div._1f0f1758')->text();
          // echo "<pre>"; print_r($title); echo "</pre>"; die;
          
          
          $titleExplode = explode(',', $title);
          $buildingTitle = $community = $sub_community = $city = '';
          if (isset($titleExplode[0]) AND $titleExplode[0]<>null) {
            $buildingTitle = $titleExplode[0];
          }
          if (isset($titleExplode[1]) AND $titleExplode[1]<>null) {
            $community = $titleExplode[1];
          }
          if (isset($titleExplode[3]) AND $titleExplode[3]<>null) {
            if (isset($titleExplode[2]) && $titleExplode[2]<>null) {
              $sub_community = $titleExplode[2];
            }
          }
          $city = array_pop($titleExplode);
          
          if ($data['property_category'] == 'residential') {
            $property_type = 1;
          }
          if ($data['property_category'] == 'commercial') {
            $property_type = 4;
          }

          $propertyDescription = $crawler->filter('div._2015cd68')->text();
          $listingAndFinalPrice = $crawler->filter('div.c4fc20ba span._105b8a67')->text();
          
        }
        catch(Exception $err){
         // $error = $err->getMessage();
          // echo "4th Catch<br>"; print_r($error); echo "<br>";
        }
       //   error_log('test4.3');
        $dataArr['url'] = $data['url'];
        
        if ($bed_bath_area<>null) {
            error_log('test4.4');
          $bed_bath_area_studio_arr = $this->get_Bed_Bath_Area($bed_bath_area);
        //    error_log('test4.6');
          if ($bed_bath_area_studio_arr<>null) {
            $dataArr['no_of_bedrooms'] = trim($bed_bath_area_studio_arr['no_of_bedrooms']);
            $dataArr['no_of_bathrooms'] = trim($bed_bath_area_studio_arr['no_of_bathrooms']);
            $dataArr['property_size'] = trim($bed_bath_area_studio_arr['area']);
            $dataArr['studio'] = trim($bed_bath_area_studio_arr['studio']);
          }
        }
        if ($propertyInfoArr<>null) {
         //   error_log('test4.2');
          $propertyInfoArr = $this->getPropertyData($propertyInfoArr);
          if ($propertyInfoArr<>null) {
            $dataArr ['property_type'] = trim(strtolower($propertyInfoArr['type']));
            $dataArr ['purpose'] = trim($propertyInfoArr['purpose']);
            $dataArr ['ref_number'] = trim($propertyInfoArr['ref_number']);
            $dataArr ['average_rent'] = trim($propertyInfoArr['Average Rent']);
            $dataArr ['added_on'] = trim($propertyInfoArr['added_on']);
          }
        }
        //  error_log('test5.1');
        $dataArr['property_category'] = trim($property_type);
        $dataArr['building_title'] = trim($buildingTitle);
        $dataArr['community'] = trim($community);
        $dataArr['sub_community'] = trim($sub_community);
        $dataArr['city'] = trim($city);
        $dataArr['listing_price'] = trim(str_replace("," , "", $listingAndFinalPrice));
        $dataArr['desc'] = trim(strtolower($propertyDescription));
       //   error_log('test5');
        // echo "DataStart:<pre>"; print_r($dataArr); echo "</pre>:DataEnd<br>"; die();
        
        $this->getForSaleDecision($dataArr);
      }
    }
  }
  
  
  
  
  
  public function getForSaleDecision($data=null)
  {
    // echo "<pre>"; print_r($data); echo "</pre>"; die();

    if ($data['building_title']<>null) {
        error_log($data['building_title']);

      $city_id = Yii::$app->appHelperFunctions->getCityId($data['city']);
      error_log($data['community']);
      error_log($data['sub_community']);
     // error_log($commAndSubCommId['community_id']);
      // echo $city_id; //die();
      $commAndSubCommId = Yii::$app->propertyFinderHelperFunctions->getCommAndSubCommIds($data['community'],$data['sub_community']);
      // echo "<pre>"; print_r($commAndSubCommId); echo "</pre>"; die();
        error_log($commAndSubCommId['sub_community_id']);
        error_log(trim($data['building_title']));
        error_log('herewew');
      $building = Buildings::find()->where([
       // 'title'         => trim($data['building_title']),
        // 'title'         => trim('Mudon Villas - Arabella 1 (Al Hebiah Sixth)'),
        'city'          => $city_id,
      ])->andWhere(['like', 'title', '%'.trim($data['building_title']) . '%', false]);
      if ($commAndSubCommId['sub_community_id']<>null) {
        $building->andWhere([
         // 'community'     => $commAndSubCommId['community_id'],
         // 'sub_community' => $commAndSubCommId['sub_community_id'],
        ]);
      }
      //$building->andWhere(['city'=>$city_id]);
      //$building->orWhere(['like', 'title', '%'.trim($data['building_title']) . '%', false]);
      // $building->orWhere(['like', 'title', trim('Mudon Villas - Arabella 1 (Al Hebiah Sixth)') . '%', false]);
      $model = $building->one();
        error_log(trim($data['building_title']));
        error_log('here');
      if ($model<>null) {
        //  error_log('test5.6');
        // echo "<pre>"; print_r($model); echo "</pre>"; die();
        $this->saveForSaleListingTransaction($data,$model);
      }else{
          // the message
        //  error_log('test5.7');
       // echo "Building not found";
        $query = \app\models\BuildingForSave::find()->where(['building_name'=>trim($data['building_title']), 'city'=>trim($data['city'])])->one();
        if($query==null){
            $model = new \app\models\BuildingForSave;
            $model->building_name = trim($data['building_title']);
            $model->city 		  = trim($data['city']);
            $model->save();
            if($model->hasErrors()){
                foreach($model->getErrors() as $error){
                    if(count($error)>0){
                        foreach($error as $key=>$val){
                         //   echo $val. "<br>";
                        }
                    }
                }
                die();
            }
        }
      }
    }
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  public function saveForSaleListingTransaction($data=null,$model)
  {
    // echo "saveListingTransaction<pre>"; print_r($data); echo "</pre><br><br>";
    // echo "saveListingTransaction<pre>"; print_r($model); echo "</pre><br><br>";
    // die();
    
    // $desc = $data['desc'];

    
    $bedrooms = '';
    if ($data['no_of_bedrooms']<>null && is_numeric($data['no_of_bedrooms'])) {
      $bedrooms = $data['no_of_bedrooms'];
    }
    
    
    
    $findListing = ListingsTransactions::find()->where(['listings_reference'=>$data['ref_number'], 'listing_website_link'=>$data['url']])->one();
    if ($findListing==null) {
      $listingTransaction = new ListingsTransactions();
      $listingTransaction->listings_reference   = $data['ref_number']; //done
      $listingTransaction->source               = 15; //done
      $listingTransaction->listing_website_link = $data['url']; //done
      $listingTransaction->listing_date         = $data['added_on']; //done
      $listingTransaction->unit_number          = 'Not Known'; //done
      
      $listingTransaction->building_info = $model->id;
      
      // property category working
      $listingTransaction->property_category = $model->property_category; //$data['property_category'];
      
      //tenure working
      if ($model->tenure<>null) {
        $listingTransaction->tenure = $model->tenure;
      }
      
      $listingTransaction->no_of_bedrooms = ($bedrooms<>null AND $bedrooms!='') ? $bedrooms : 0;
      
      $listingTransaction->built_up_area = $data['property_size'];
      $listingTransaction->land_size     = $data['property_size'];
      
      //Start Location Attributes
      $listingTransaction->location_highway_drive = ($model->location_highway_drive<>null) ? $model->location_highway_drive : 'minutes_5';
      $listingTransaction->location_school_drive  = ($model->location_school_drive<>null) ? $model->location_school_drive : 'minutes_5';
      $listingTransaction->location_mall_drive    = ($model->location_mall_drive<>null) ? $model->location_mall_drive : 'minutes_5';
      $listingTransaction->location_sea_drive     = ($model->location_sea_drive<>null) ? $model->location_sea_drive : 'minutes_5';
      $listingTransaction->location_park_drive    = ($model->location_park_drive<>null) ? $model->location_park_drive : 'minutes_5';
      //End Location Attributes
      
      
      $listingTransaction->listings_price = trim($data['listing_price']);
      $listingTransaction->listings_rent = trim($data['listing_price']); //Yii::$app->propertyFinderHelperFunctions->getListingRent($data['property_size'],$bedrooms);
      $listingTransaction->final_price = trim($data['listing_price']);
      $listingTransaction->status = 2;
      //  error_log('final');
      if (!$listingTransaction->save()) {
          error_log('errorsave');
        echo " Saved Successfully ";
        Yii::$app->db->createCommand()
        ->update('bayut_detail_page_links', [ 'status' => 3 ] , [ 'url' => $data['url'] ])
        ->execute();
      }else{
          Yii::$app->db->createCommand()
              ->update('bayut_detail_page_links', [ 'status' => 1 ] , [ 'url' => $data['url'] ])
              ->execute();
      }
      if($listingTransaction->hasErrors()){
          error_log('here4');
        foreach($listingTransaction->getErrors() as $error){
          if(count($error)>0){
            foreach($error as $key=>$val){
            //  echo $val. "<br>";
            }
            
          }
        }
        die();
      }
      
    }else{
        error_log('listing already exists');
        Yii::$app->db->createCommand()
            ->update('bayut_detail_page_links', [ 'status' => 4 ] , [ 'url' => $data['url'] ])
            ->execute();
     // echo " listing already exists ";
      return false;
    }
  }

    public function get_Bed_Bath_Area($bed_bath_area=null)
    {
        if ($bed_bath_area<>null) {
          //  error_log('test4.7');
            $no_of_bedrooms = 0; $no_of_bathrooms = 0; $area = 0; $studio ='no';
            foreach ($bed_bath_area as $key => $value) {
                if ($key=='Beds' && $value!='Studio') {
                    $explodeNumOfBeds = (explode(" ", $value));
                    $no_of_bedrooms  = $explodeNumOfBeds[0];
                }
                if ($key=='Beds' && $value=='Studio') {
                    $studio = 'yes';
                }
                if ($key=='Baths') {
                    $explodeNumOfBaths = (explode(" ", $value));
                    $no_of_bathrooms  = $explodeNumOfBaths[0];
                }
                if ($key=='Area') {
                    $explodeArea = (explode(" ",$value));
                    $area = str_replace(',', '', $explodeArea[0]);;
                }
            }
            return [
                'no_of_bedrooms' => $no_of_bedrooms,
                'no_of_bathrooms' => $no_of_bathrooms,
                'area' => $area,
                'studio' => $studio,
            ];
        }
    }

    public function getPropertyData($propertyInfoArr=null)
    {
        if ($propertyInfoArr<>null) {
            $type = $purpose = $ref_number = $furnishing = $truCheck_on = $added_on = '';
            foreach ($propertyInfoArr as $key => $value) {
                if($key == 'Type'){
                    $type = $value;
                }
                if($key == 'Purpose'){
                    $purpose = $value;
                }
                if($key == 'Reference no.'){
                    $ref_number = $value;
                }
                if($key == 'Furnishing'){
                    $furnishing = $value;
                }
                if($key == 'TruCheck on'){
                    $truCheck_on = $value;
                }
                // if($key == 'Added on'){
                //   $added_on = date('Y-m-d', strtotime($value));
                // }
            }
            $added_on = date('Y-m-d', strtotime(end($propertyInfoArr)));
            return [
                'type' => $type,
                'purpose' => $purpose,
                'ref_number' => $ref_number,
                'truCheck_on' => $truCheck_on,
                'added_on' => $added_on,
            ];
        }
    }



    public function getBuaAndLandArea($propertyDescription=null, $MainLandOrBua=null, $no_of_bedrooms=null)
    {
        // echo "<pre>"; print_r($propertyDescription); echo "</pre><br>";  //die();
        // echo "num of bedrooms: ".$no_of_bedrooms."<br>"; die();
        $totalArea = str_replace(',', '', $MainLandOrBua);
        $percentageValue = $totalArea*10/100;
        $addInTotalArea = $totalArea+$percentageValue;
        $removeInTotalArea = $totalArea-$percentageValue;

        $buaWords = [
            'bua','built up','builtup','built-up','b u a','building area','building size','buildup','build up','b.u.a'
        ];
        $PlotWords = [
            'plot','land','total area','plot'
        ];

        $built_up_area ='';
        $land_size ='';

        if (strpos($propertyDescription, 'bua') !== false || strpos($propertyDescription, 'built up') !== false || strpos($propertyDescription, 'builtup') !== false || strpos($propertyDescription, 'built-up') !== false || strpos($propertyDescription, 'building area') !== false ||
            strpos($propertyDescription, 'building size') !== false || strpos($propertyDescription, 'buildup') !== false || strpos($propertyDescription, 'build up') !== false || strpos($propertyDescription, 'b.u.a')) {
            // echo "Bua found in description<br>"; die();
            foreach ($buaWords as $word) {
                $findWord = strpos($propertyDescription, $word);
                if ($findWord<>null) {
                    $nextChar = substr($propertyDescription, $findWord, 20);
                    $val = preg_replace('/[^0-9-,-.]/', '', $nextChar);
                    $finalBUA = str_replace(',', '', $val);
                    if ($val<>null && $val>0) {
                        $built_up_area = $finalBUA;
                        $land_size     = $totalArea;
                    }else{
                        $built_up_area = $totalArea;
                    }
                }
            }
//            print_r($built_up_area); die();
        }
        else{
//             echo "bua not found in description<br>"; die();
            $findInPreviousListing = \app\models\ListingsTransactions::find()
                ->where(['no_of_bedrooms'=>$no_of_bedrooms])
                ->andWhere(['and',['>=', 'built_up_area', $removeInTotalArea],['<=', 'built_up_area', $addInTotalArea]])
                ->asArray()->all();
            // echo "<pre>"; print_r($findInPreviousListing); echo "</pre>"; die();
            if ($findInPreviousListing<>null) {
                $totalBuildUpArea = 0;
                foreach ($findInPreviousListing as $key => $value) {
                    $totalBuildUpArea += $value['built_up_area'];
                }
                $built_up_areaa = $totalBuildUpArea/count($findInPreviousListing);
                $built_up_area = number_format((float)$built_up_areaa, 2, '.', '');
                // echo "average built up area: ".$built_up_area."<br>";
            }else{
                $built_up_area = $totalArea;
                // echo "<br>BUA not found in range now Bua Is: ".$built_up_area."<br>";
            }
        }
        // echo "<br><br>"; // die();

        // plot area array loop

        if (strpos($propertyDescription, 'plot') !== false || strpos($propertyDescription, 'land') !== false || strpos($propertyDescription, 'total area') !== false || strpos($propertyDescription, 'plot') !== false) {
//                echo "plot area fount in description"; die();
            $i=1;
            foreach ($PlotWords as $word) {
                // echo "i is equal to:- ".$i."<br>";
                $findWord = strpos($propertyDescription, $word);
                if ($findWord<>null) {
//                     echo "word find:- ".$word."<br>"; //die();
                    $nextChar = substr($propertyDescription, $findWord, 20);
                    $extraWords = strpos($nextChar, 'parks');
//                    echo $nextChar; echo "<br>";
//                    echo "Hello ".$extraWords; die();
                    if ($extraWords==null){
//                        echo "usaamamama"; //die();
                        $val = preg_replace('/[^0-9-,-.]/', '', $nextChar);
//                     echo "next char: ".$nextChar; echo "<br>";
//                     echo "val: ".$val; echo "<br>"; die();
                        $finalLandSize = str_replace(',', '', $val);
                        if ($val<>null && $val>0) {
                            // echo "val is >0:- ".$val."<br>";
                            $land_size = $finalLandSize;
                        }else{
                            $land_size = $totalArea;
                            // echo "else land size is total area: ".$land_size."<br>";
                        }
                    }

//


                }
                $i++;
            }
//            echo $land_size; die();
        }

        if($land_size==null){
//            echo "plot area not fount in description"; die();
            //if bua not found in description and plot area also not found in description then the area mentioned
            //in bayut is built up area.
            // $built_up_area = $totalArea;
            // echo "Word found but integer not found:- ".$word."<br>";
            $findInPreviousListing = \app\models\ListingsTransactions::find()
                ->where(['no_of_bedrooms'=>$no_of_bedrooms])
                ->andWhere(['and',['>=', 'land_size', $removeInTotalArea],['<=', 'land_size', $addInTotalArea]])
                ->asArray()->all();

            if ($findInPreviousListing<>null) {
                // echo "land size lie in bayut range<br>";
                $totalLandArea = 0;
                foreach ($findInPreviousListing as $key => $value) {
                    $totalLandArea += $value['built_up_area'];
                }
                $landArea = $totalLandArea/count($findInPreviousListing);
                $land_size = number_format((float)$landArea, 2, '.', '');
//                 echo "land size after range is:- ".$land_size."<br>";
            }
            else{
                $land_size = $totalArea;
                // echo "so land size is also equal to bayut area given in website: ".$land_size."<br>";
            }
        }



        //end plot area  loop
        return [
            'built_up_area'=>$built_up_area,
            'land_size'=>$land_size,
        ];

        // echo "<br><br>built up area: ".$built_up_area; echo "<br>";
        // echo "land size: ".$land_size; echo "<br>";
        // die();
    }




    public function getAgentName($html='')
    {
        $agent_name='';
        if ($html<>null) {
            if (
                strpos($html, 'agent:')!==false
                || strpos($html, 'agent;')!==false
                || strpos($html, 'agen e')!==false
                || strpos($html, 'agene')!==false
                || strpos($html, 'agen t:')!==false
                || strpos($html, "agent'")!==false
                || strpos($html, "t:")!==false
            ) {
                $agentPosition = strpos($html, 'agent:');
                $agentPosition2 = strpos($html, 'agent;');
                $agentPosition3 = strpos($html, 'agen e');
                $agentPosition4 = strpos($html, 'agene');
                $agentPosition5 = strpos($html, 'agen t:');
                $agentPosition6 = strpos($html, "agent'");
                $agentPosition7 = strpos($html, "t:");
                if ($agentPosition<>null && $agentPosition>0) {
                    //  echo "222<br>";
                    $nextChar = substr($html, $agentPosition, 35);
                    $chunks = explode("view",$nextChar);
                    $againChunks = explode(":",$chunks[0]);
                    $agent_name = $againChunks[1];
                }else if ($agentPosition2<>null && $agentPosition2>0) {
                    //  echo "333<br>";
                    $nextChar2 = substr($html, $agentPosition2, 35);
                    $chunks2 = explode("view",$nextChar2);
                    $againChunks2 = explode(";",$chunks2[0]);
                    $agent_name = $againChunks2[1];
                }else if ($agentPosition3<>null && $agentPosition3>0) {
                    //   echo "444<br>";
                    $nextChar3 = substr($html, $agentPosition3, 35);
                    $chunks3 = explode("view",$nextChar3);
                    $againChunks3 = explode(" e",$chunks3[0]);
                    $agent_name = $againChunks3[1];
                }else if ($agentPosition4<>null && $agentPosition4>0) {
                    //    echo "555<br>";
                    $nextChar4 = substr($html, $agentPosition4, 35);
                    $chunks4 = explode("view",$nextChar4);
                    $againChunks4 = explode("agene ",$chunks4[0]);
                    $agent_name = $againChunks4[1];
                }else if ($agentPosition5<>null && $agentPosition5>0) {
                    //   echo "666<br>";
                    $nextChar5 = substr($html, $agentPosition5, 35);
                    $chunks5 = explode("view",$nextChar5);
                    $againChunks5 = explode(":",$chunks5[0]);
                    $agent_name = $againChunks5[1];
                }else if ($agentPosition6<>null && $agentPosition6>0) {
                    //   echo "777<br>";
                    $nextChar6 = substr($html, $agentPosition6, 35);
                    $chunks6 = explode("view",$nextChar6);
                    $againChunks6 = explode("agent'",$chunks6[0]);
                    $agent_name = $againChunks6[1];
                }else if ($agentPosition7<>null && $agentPosition7>0) {
                    //    echo "888<br>";
                    $nextChar7 = substr($html, $agentPosition7, 35);
                    $chunks7 = explode("view",$nextChar7);
                    $againChunks7 = explode(":",$chunks7[0]);
                    $agent_name = $againChunks7[1];
                }
                return $agent_name;
            }
            else{}
        }
    }


}