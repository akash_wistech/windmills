<?php
namespace app\commands;
use Yii;
use yii\console\Controller;

use yii2tech\csvgrid\CsvGrid;
use yii\data\ActiveDataProvider;
use app\models\Company;
use app\models\Valuation;
use app\models\Buildings;
use app\models\Properties;
use app\models\ValuationPurposes;
use app\models\Zone;
use app\models\Communities;
use app\models\SubCommunities;
use app\models\ValuationApproversData;
use app\modules\wisnotify\listners\NotifyEvent;

class ClientValuationsController extends Controller
{
    public function actionIndex()
    {

        $client = Company::findOne(9166);
        $file_pathCsv = Yii::$app->params['client_valuations'] . 'ajman_rera.xlsx';

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        /* Load a CSV file and save as a XLS */
        $spreadsheet = $reader->load($file_pathCsv);
        $sheet = $spreadsheet->getActiveSheet();
        $month =  date("F", strtotime("first day of previous month"));
        $sheet->setCellValue('A1', $month);
        //$sheet->setCellValue('A1', 'July');



        $first_day_last_month = date("Y-m-d", strtotime("first day of last month")) . ' 00:00:00';
        $last_day_last_month = date("Y-m-d", strtotime("last day of last month")) . ' 23:59:59';

        $valuations = Valuation::find()
            ->select([
                Valuation::tableName() . '.id',
                Valuation::tableName() . '.reference_number',
                Valuation::tableName() . '.target_date',
                Valuation::tableName() . '.client_name_passport',
                Valuation::tableName() . '.title_deed',
                Valuation::tableName() . '.submission_approver_date',
                Valuation::tableName() . '.parent_id',
                Buildings::tableName() . '.title as Building',
            ])
            ->leftJoin(Buildings::tableName(), Buildings::tableName() . '.id=' . Valuation::tableName() . '.building_info')
            ->where([Buildings::tableName() . '.city' => 3507])
            ->andWhere([Valuation::tableName() . '.valuation_status' => 5])
            ->andFilterWhere([
                'between', 'valuation.submission_approver_date', $first_day_last_month, $last_day_last_month
            ])
            ->asArray()->all();


        if($valuations <> null && !empty($valuations)) {
            $main_array_vals = array();
            $parent_array_vals = array();

            foreach ($valuations as $key => $data_set_a) {
                if ($data_set_a['parent_id'] <> null) {
                    $parent_array_vals[] = $data_set_a['parent_id'];
                }
            }


            foreach ($valuations as $key => $data_set_b) {
                if (!in_array($data_set_b['id'], $parent_array_vals)) {
                    $main_array_vals[] = $data_set_b;
                }
            }


            foreach ($main_array_vals as $key => $data_set) {


                $model = Valuation::findOne($data_set['id']);

                $land = array(4, 5, 20, 23, 26, 29, 39, 46, 47, 48, 49, 50, 53);
                if (in_array($model->property_id, $land)) {
                    $completion_year = 'Not Applicable';
                } else {
                    $completion_year = ((int)date('Y')) - round($model->inspectProperty->estimated_age) . ' ';
                }

                $plot_size = ($model->land_size > 0) ? Yii::$app->appHelperFunctions->wmFormate($model->land_size) : '';
                $approver_data = ValuationApproversData::find()->where(['valuation_id' => $data_set['id'], 'approver_type' => 'approver'])->one();
                $estimate_price_byapprover = number_format($approver_data->estimated_market_value);

                $owners = \app\models\ValuationOwners::find()->where(['valuation_id' => $data_set['id']])->all();
                $owners_name = "";
                foreach ($owners as $record) {
                    $owners_name .= $record->name . " " . $record->lastname . ", ";
                }


                $sheet->setCellValue('A' . ($key + 3), ($key + 1));
                $sheet->setCellValue('B' . ($key + 3), date('d-M-Y', strtotime($data_set['submission_approver_date'])));
                $sheet->setCellValue('C' . ($key + 3), $data_set['reference_number']);
                $sheet->setCellValue('D' . ($key + 3), $model->client->title);
                $sheet->setCellValue('E' . ($key + 3), $owners_name);
                $sheet->setCellValue('F' . ($key + 3), $data_set['title_deed']);
                $sheet->setCellValue('G' . ($key + 3), $data_set['sector_community']);
                $sheet->setCellValue('H' . ($key + 3), $data_set['area_development']);
                $sheet->setCellValue('I' . ($key + 3), $plot_size);
                $sheet->setCellValue('J' . ($key + 3), number_format($model->inspectProperty->built_up_area, 2));
                $sheet->setCellValue('K' . ($key + 3), Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category]);
                $sheet->setCellValue('L' . ($key + 3), $model->property->title);
                $sheet->setCellValue('M' . ($key + 3), $completion_year);
                $sheet->setCellValue('N' . ($key + 3), $estimate_price_byapprover);



            }
        }


        $writer2 = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $file_nameXlxs = 'ajman-rera-valuation-'.$month.'.xlsx';
        if (file_exists(Yii::$app->params['client_valuations']. $file_nameXlxs)) {
            unlink(Yii::$app->params['client_valuations']. $file_nameXlxs);
        }


       //


        $file_pathXlsx = Yii::$app->params['client_valuations'] . $file_nameXlxs;
       // $fullPath = realpath(dirname(__FILE__).'/../../uploads/client_valuation_csv').'/'.$file_nameXlxs;
        $writer2->save($file_pathXlsx);
       // echo $file_pathXlsx;
       // die;


            $attachments = [$file_pathXlsx];
            $notifyData = [
                'subject'=> 'Monthly Valuations Report -'. $month,
                'g_subject'=> 'Monthly Valuations Report -'. $month,
                'client' => $client,
                'attachments' => $attachments,
                'replacements'=>[
                    '{month}'=>$month,
                ],
            ];


            NotifyEvent::fireToGovt('ajman.valuations.send', $notifyData);
            exit;

            echo "Emails Send Successfully;";

    }

}
