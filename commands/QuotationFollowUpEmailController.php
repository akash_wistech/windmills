<?php
namespace app\commands;
use Yii;
use yii\console\Controller;
use app\models\CrmQuotations;
use app\models\QuotationFollowupEmailChecker;

class QuotationFollowUpEmailController extends Controller
{
    //send follow up email if quotation status is 0 => Inquiry Received
    public function actionInquiryReceivedFollowUpEmail()
    {
        $models = \app\models\CrmQuotations::find()->where(['quotation_status'=>0])->all();
        if($models<>null){
            foreach($models as $key => $quotation){

                $follow_up_checker = QuotationFollowupEmailChecker::find()
                ->where(['quotation_id'=>$quotation->id, 'action'=>'InquiryReceivedFollowUpEmail'])
                ->asArray()->one();
                if($follow_up_checker<>null){
                    $created_date = date("Y-m-d", strtotime($follow_up_checker['date']));
                }else{
                    $created_date = date("Y-m-d", strtotime($quotation->created_at));
                }

                $time_period = \app\models\ClientSegmentFile::find()->where(['client_type'=>$quotation->client->client_type])->asArray()->one();
                $days_to_add = $time_period['quotation_followup_period__days'];
                //add days
                $follow_up_date = date("Y-m-d", strtotime("$created_date +$days_to_add days"));
                $today_date = date("Y-m-d");
                // $follow_up_date = '2023-04-25'; //comment when testing is done;
                
                if (strtotime($follow_up_date) == strtotime($today_date)) {
                    $notifyData = [
                        'client' => $quotation->client,
                        'attachments' => [],
                        'replacements'=>[
                            '{clientName}'=>$quotation->client->title,
                        ],
                    ];
                    //sending email
                    \app\modules\wisnotify\listners\NotifyEvent::fire2('quotation.docsfe', $notifyData);
                    //email sending record entry
                    $email_entry = new \app\models\QuotationFollowupEmailChecker;
                    $email_entry->quotation_id = $quotation->id;
                    $email_entry->date         = date('Y-m-d H:i:s');
                    $email_entry->action       = 'InquiryReceivedFollowUpEmail';
                    $email_entry->created_at   = date('Y-m-d H:i:s');
                    if($email_entry->save()){
                        echo "|InquiryReceivedFollowUpEmail send| ";
                    }
                }
                else{
                    echo "|date not match| ";
                }
            }
        }
    }

    //send follow up email if quotation status is 3 => Toe Sent
    public function actionToeSentFollowUpEmail()
    {
        $models = \app\models\CrmQuotations::find()->where(['quotation_status'=>3])->all();
        if($models<>null){
            foreach($models as $key => $quotation){

                $follow_up_checker = QuotationFollowupEmailChecker::find()
                ->where(['quotation_id'=>$quotation->id, 'action'=>'ToeSentFollowUpEmail'])
                ->asArray()->one();
                if($follow_up_checker<>null){
                    $created_date = date("Y-m-d", strtotime($follow_up_checker['date']));
                }else{
                    $created_date = date("Y-m-d", strtotime($quotation->created_at));
                }
            
                $time_period = \app\models\ClientSegmentFile::find()->where(['client_type'=>$quotation->client->client_type])->asArray()->one();
                $days_to_add = $time_period['quotation_followup_period__days'];
                //add days
                $follow_up_date = date("Y-m-d", strtotime("$created_date +$days_to_add days"));
                $today_date = date("Y-m-d");
                // $follow_up_date = '2023-04-25'; //comment when testing is done;
                
                if (strtotime($follow_up_date) == strtotime($today_date)) {
                    $notifyData = [
                        'client' => $quotation->client,
                        'attachments' => [],
                        'replacements'=>[
                            '{clientName}'=>$quotation->client->title,
                        ],
                    ];
                    //sending email
                    \app\modules\wisnotify\listners\NotifyEvent::fire2('quotation.docsfe', $notifyData);
                    //email sending record entry
                    $email_entry = new \app\models\QuotationFollowupEmailChecker;
                    $email_entry->quotation_id = $quotation->id;
                    $email_entry->date         = date('Y-m-d H:i:s');
                    $email_entry->action       = 'ToeSentFollowUpEmail';
                    $email_entry->created_at   = date('Y-m-d H:i:s');
                    if($email_entry->save()){
                        echo "|ToeSentFollowUpEmail send| ";
                    }
                }
                else{
                    echo "|date not match| ";
                }
            }
        }
    }

    //send follow up email if quotation status is 5 => Toe Signed And Receivd
    public function actionToeSignedAndReceivedFollowUpEmail()
    {
        $models = \app\models\CrmQuotations::find()->where(['quotation_status'=>5])->all();
        if($models<>null){
            foreach($models as $key => $quotation){
                $follow_up_checker = QuotationFollowupEmailChecker::find()
                ->where(['quotation_id'=>$quotation->id, 'action'=>'ToeSignedAndReceivedFollowUpEmail'])
                ->asArray()->one();
                if($follow_up_checker<>null){
                    $created_date = date("Y-m-d", strtotime($follow_up_checker['date']));
                }else{
                    $created_date = date("Y-m-d", strtotime($quotation->created_at));
                }
                
                $time_period = \app\models\ClientSegmentFile::find()->where(['client_type'=>$quotation->client->client_type])->asArray()->one();
                $days_to_add = $time_period['quotation_followup_period__days'];
                //add days
                $follow_up_date = date("Y-m-d", strtotime("$created_date +$days_to_add days"));
                $today_date = date("Y-m-d");
                // $follow_up_date = '2023-04-25'; //comment when testing is done;
                
                if (strtotime($follow_up_date) == strtotime($today_date)) {
                    $notifyData = [
                        'client' => $quotation->client,
                        'attachments' => [],
                        'replacements'=>[
                            '{clientName}'=>$quotation->client->title,
                        ],
                    ];
                    //sending email
                    \app\modules\wisnotify\listners\NotifyEvent::fire2('quotation.docsfe', $notifyData);
                    //email sending entry
                    $email_entry = new \app\models\QuotationFollowupEmailChecker;
                    $email_entry->quotation_id = $quotation->id;
                    $email_entry->date         = date('Y-m-d H:i:s');
                    $email_entry->action       = 'ToeSignedAndReceivedFollowUpEmail';
                    $email_entry->created_at   = date('Y-m-d H:i:s');
                    if($email_entry->save()){
                        echo "|ToeSignedAndReceivedFollowUpEmail send| ";
                    }
                }
                else{
                    echo "|date not match| ";
                }
            }
        }
    }

    //send follow up email if quotation status is 10 => On Hold
    public function actionOnHoldFollowUpEmail()
    {
        $models = \app\models\CrmQuotations::find()->where(['quotation_status'=>10])->all();
        if($models<>null){
            foreach($models as $key => $quotation){

                $follow_up_checker = QuotationFollowupEmailChecker::find()
                ->where(['quotation_id'=>$quotation->id, 'action'=>'OnHoldFollowUpEmail'])
                ->asArray()->one();
                if($follow_up_checker<>null){
                    $created_date = date("Y-m-d", strtotime($follow_up_checker['date']));
                }else{
                    $created_date = date("Y-m-d", strtotime($quotation->created_at));
                }
                
                $time_period = \app\models\ClientSegmentFile::find()->where(['client_type'=>$quotation->client->client_type])->asArray()->one();
                $days_to_add = $time_period['quotation_followup_period__days'];
                //add days
                $follow_up_date = date("Y-m-d", strtotime("$created_date +$days_to_add days"));
                $today_date = date("Y-m-d");
                // $follow_up_date = '2023-04-25'; //comment when testing is done;
                
                if (strtotime($follow_up_date) == strtotime($today_date)) {
                    $notifyData = [
                        'client' => $quotation->client,
                        'attachments' => [],
                        'replacements'=>[
                            '{clientName}'=>$quotation->client->title,
                        ],
                    ];
                    //sending email
                    \app\modules\wisnotify\listners\NotifyEvent::fire2('quotation.docsfe', $notifyData);
                    //email sending entry
                    $email_entry = new \app\models\QuotationFollowupEmailChecker;
                    $email_entry->quotation_id = $quotation->id;
                    $email_entry->date         = date('Y-m-d H:i:s');
                    $email_entry->action       = 'OnHoldFollowUpEmail';
                    $email_entry->created_at   = date('Y-m-d H:i:s');
                    if($email_entry->save()){
                        echo "|OnHoldFollowUpEmail send| ";
                    }
                }
                else{
                    echo "|date not match| ";
                }
            }
        }
    }


    
    public function actionIndex()
    {
        $quotations = \app\models\CrmQuotations::find()
        ->where(['quotation_status' => 3])
        ->andWhere(['<', 'updated_at', new \yii\db\Expression('DATE_SUB(NOW(), INTERVAL 3 DAY)')])
        ->andWhere(['not exists',
            (new \yii\db\Query())
                ->select('*')
                ->from(QuotationFollowupEmailChecker::tableName().' qfec')
                ->where(['qfec.quotation_id' => new \yii\db\Expression('crm_quotations.id')])
                ->andWhere([
                    '>', 'qfec.date', new \yii\db\Expression('DATE_SUB(NOW(), INTERVAL 3 DAY)')
                ])
        ])
        ->all();
        // dd(count($quotations));

        foreach($quotations as $key => $quotation){
            $notifyData = [
                'client' => $quotation->client,
                'attachments' => [],
                'replacements'=>[
                    '{clientName}'=>$quotation->client->title,
                ],
            ];
            //sending email
            \app\modules\wisnotify\listners\NotifyEvent::fire2('quotation.docsfe', $notifyData);
            //email sending entry
            $email_entry = new \app\models\QuotationFollowupEmailChecker;
            $email_entry->quotation_id = $quotation->id;
            $email_entry->date = date('Y-m-d H:i:s');
            if($email_entry->save()){
                echo"sended ";
            }
        }
    }
}