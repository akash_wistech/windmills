<?php
namespace app\commands;
use Yii;
use yii\console\Controller;
use Illuminate\Http\Request;
use Symfony\Component\DomCrawler\Crawler;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Exception;
use DOMXPath;
use DOMDocument;
use yii\db\Expression;

use app\models\PropertyfinderCityCronLogs;
use app\models\PropertyfinderByCityUrl;
use app\models\PropertyfinderByCityDltUrl;

class PropertyfinderByCitySaleController extends Controller
{
    
    public function actionIndex($city_id, $purpose)
    {
        require __DIR__ . '/../components/helpers/simple_html_dom.php';
        
        $start = 21;
        $end = 22;
        $now = date('H');
        if ($now >= $start && $now <= $end) {
            
            $log_status = PropertyfinderCityCronLogs::find()
            ->where(['city_id'=>$city_id, 'purpose'=>$purpose, 'action' => 'index'])
            ->andWhere(new Expression(' DATE(date) = CURDATE() '))
            ->asArray()
            ->one();
            
            if ($log_status==null) {
                $connection=Yii::$app->db;
                $connection->createCommand(' TRUNCATE `propertyfinder_by_city_url`; TRUNCATE `propertyfinder_by_city_dlt_url`; ')->execute();
                $connection->createCommand('INSERT INTO `propertyfinder_by_city_url`(`url`, `city_id`, `purpose`) SELECT `url`, `city_id`, `purpose` FROM `propertyfinder_by_city_url_backup`')->execute();
                
                $log_status = new PropertyfinderCityCronLogs;
                $log_status->city_id = $city_id; 
                $log_status->purpose = $purpose;
                $log_status->date    = date("Y-m-d");
                $log_status->action  = 'index';
                $log_status->save();
            } 
            
            // die("here");
            
            if ($log_status['date']== date("Y-m-d")) {
                $query = PropertyfinderByCityUrl::find()->where(['city_id'=>$city_id, 'purpose'=>$purpose])->one();
                if ($query<>null) {
                     $url = 'https://usama-com.stackstaging.com/maxima_latest/crawl-helper/index?url='.$query->url.'';
                    // echo $query->url; die;
                    
                    try {
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        $response = curl_exec($ch);
                        curl_close($ch);
                        
                        $html = new \simple_html_dom();
                        $html->load($response);
                        
                        $i=1;
                        foreach ($html->find('a.card__link') as $key => $value) {
                            $detail_url = "https://www.propertyfinder.ae".$value->href;
                            // echo $detail_url."-".$i. " ";
                            if ($detail_url<>null) {
                                $model = PropertyfinderByCityDltUrl::find()->where([ 'url'=>$detail_url, 'purpose'=>$purpose ])->one();
                                if ($model==null) {
                                    $model          = new PropertyfinderByCityDltUrl;
                                    $model->url     = $detail_url;
                                    $model->city_id = $city_id;
                                    $model->status  = 0;
                                    $model->purpose = $purpose;
                                    if ($model->save()) {
                                        echo "save-".$i." ";
                                    }
                                }
                            }
                            $i++;
                        }
                        
                        $nextPageUrl=[];
                        try{
                            foreach ($html->find('a.pagination__link--next') as $key => $value) {
                                $nextPageUrl[] = $value->href;
                            }
                            
                            if ($nextPageUrl<>null AND $nextPageUrl[0]<>null) {
                                $strPlaceUrl = str_replace("amp;", "", trim($nextPageUrl[0]));
                                $urlForSave = 'https://www.propertyfinder.ae'.trim($strPlaceUrl);
                                Yii::$app->db->createCommand()->update('propertyfinder_by_city_url', [ 'url' => $urlForSave ], [ 'city_id'=> $query->city_id, 'purpose' => $purpose ])->execute();
                            } else {
                                
                            }
                        }catch(Exception $err){
                            $error = $err->getMessage();
                            print_r($error);
                        }
                    } catch ( Exception $err ) {
                        $error = $err->getMessage();
                        echo "1st Catch<pre>"; print_r($error); echo "</pre>";
                    }
                }
                
                
                
            }else{
                
            }
            }else{
                    die("Time Out");
                }
                
                
                
                
            }
            
            
            
            
            
            public function actionDetail($city_id, $purpose)
            {
                $start = 21;
                $end = 22;
                $now = date('H'); 
                if ($now >= $start && $now <= $end) {  
                    // require __DIR__ . '/../components/helpers/simple_html_dom.php';
                    $randomString = Yii::$app->propertyFinderHelperFunctions->generateRandomString();
                    $data = [];
                    $query = PropertyfinderByCityDltUrl::find()->where(['status'=>0, 'city_id'=>$city_id, 'purpose'=>$purpose])->asArray()->limit(25)->all();
                    if ($query<>null) {
                        foreach ($query as $query) {
                            Yii::$app->db->createCommand()->update('propertyfinder_by_city_dlt_url', ['status' => 2], [ 'url'=>$query['url'], 'city_id' => $query['city_id'], 'purpose'=>$purpose ])->execute();
                            
                             $url = 'https://usama-com.stackstaging.com/maxima_latest/crawl-helper/index?url='.$query['url'].'';
                            // echo $url; die;
                            try {
                                //1st try
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                $response = curl_exec($ch);
                                // echo"<pre>"; print_r($response); echo"</pre>"; die;
                                
                                $data['url'] = $url;
                                
                                $tel_Position = strpos($response, 'tel:');
                                if ($tel_Position<>null) {
                                    $nextChar = substr($response, $tel_Position, 17);
                                }
                                $val = preg_replace('/[^0-9--.]/', '', $nextChar);
                                
                                $data['agent_telephone'] = $val;
                                $data['agent_telephone'] = trim("+".$val);
                                
                                $data['agent_title'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.agent_title');
                                $data['agent_name'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.agent_name');
                                $data['agent_location'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.broker_location');
                                $data['agent_company'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.broker_name');
                                
                                $data['property_name'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_name');
                                $data['property_type'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_type');
                                $data['bathrooms'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_bathrooms');
                                $data['bedrooms'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_bedrooms');
                                $data['property_category'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_sub_category');
                                $data['property_completion'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_completion');
                                $data['listed'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_listed_days');
                                $data['property_listing_depth'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_listing_depth');
                                // $data['city'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_location_city');
                                $data['city'] = $city = $query['city_id'];
                                $data['community'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_location_community');
                                $data['sub_community'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_location_sub_community');
                                $data['building_info'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_location_tower');
                                $data['listings_price'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_price');
                                $data['reference'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_reference');
                                $data['property_size'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_size_sqft');
                                
                                //date formatting
                                $data['listed'] = str_replace(" ", "-", $data['listed']);
                                $data['listed'] = date("Y-m-d", strtotime($data['listed']));
                                
                                //reference number formatting
                                $data['reference'] = str_replace(" ", "-", $data['reference']);
                                
                                //building info changes
                                if ($data['building_info']=='' AND $data['building_info']==null) {
                                    $data['building_info'] = $data['sub_community'];
                                };
                                $data['description'] = ''; //strtolower(trim($response));
                                
                                // echo "<pre>"; print_r($data); echo "</pre>"; die();
                                $this->saveListData($data, $city_id, $purpose);
                                
                                curl_close($ch);
                            }
                            catch ( Exception $err ) {
                                //1st catch
                                $error = $err->getMessage();
                                echo "1st Catch cc<br>";
                                print_r($error);
                            }       
                        }
                    }
                    }else{
                            echo "time out"; 
                        }
                    }
                    
                    
                    
                    public function saveListData($data=null , $city_id=null, $purpose=null)
                    {
                        // echo "<pre>"; print_r($data); echo "</pre>"; die();
                        
                        $bedrooms = 0;
                        if ($data['bedrooms']<>null && is_numeric($data['bedrooms']) && $data['bedrooms']>0) {
                            $bedrooms = $data['bedrooms'];
                        }
                        
                        $listingTransaction = \app\models\PropertyfinderListData::find()
                        ->where([
                            'source'=>16, 'listings_reference'=>$data['reference'], 
                            'listing_website_link'=>$data['url'], 'purpose'=> $purpose,
                            'city_id' => $data['city'], 'no_of_bedrooms' => $bedrooms,
                            'listings_price' => trim($data['listings_price']), 'land_size' => $data['property_size']
                            ])
                            ->one();
                            if ($listingTransaction==null) {
                                // echo $data['ref_number'].' , '.$data['url']; die; 
                                $listingTransaction = new \app\models\PropertyfinderListData;
                            }
                            $listingTransaction->listings_reference   = $data['reference'];
                            $listingTransaction->source               = 16; 
                            $listingTransaction->listing_website_link = $data['url']; 
                            $listingTransaction->listing_date         = $data['listed'];
                            $listingTransaction->building_info        = $data['building_info'];
                            $listingTransaction->property_category    = '';
                            $listingTransaction->property_type        = $data['property_type'];;
                            $listingTransaction->community            = $data['community'];;
                            $listingTransaction->sub_community        = $data['sub_community'];;
                            $listingTransaction->city_id              = $data['city'];
                            $listingTransaction->no_of_bedrooms       = ($bedrooms<>null AND $bedrooms!='') ? $bedrooms : 0;
                            $listingTransaction->built_up_area        = $data['property_size'];
                            $listingTransaction->land_size            = $data['property_size'];
                            $listingTransaction->listings_price       = trim($data['listings_price']);
                            $listingTransaction->listings_rent        = trim($data['listings_price']); //Yii::$app->propertyFinderHelperFunctions->getListingRent($data['property_size'],$bedrooms);
                            $listingTransaction->final_price          = trim($data['listings_price']);
                            $listingTransaction->status               = 2;
                            $listingTransaction->move_to_listing      = 0;
                            $listingTransaction->purpose              = $purpose;
                            $listingTransaction->created_at           = date("Y-m-d h:i:s");
                            if ($listingTransaction->save()) {
                                Yii::$app->db->createCommand()->update('propertyfinder_by_city_dlt_url', [ 'status' => 1 ] , [ 'url' => $data['url'], 'city_id'=>$city_id, 'purpose'=>$purpose ])->execute();
                                echo " Saved Successfully ";
                            }
                            if($listingTransaction->hasErrors()){
                                foreach($listingTransaction->getErrors() as $error){
                                    if(count($error)>0){
                                        foreach($error as $key=>$val){
                                            echo $val. "<br>";
                                        }   
                                    }
                                }
                              //  die();
                            }
                        }
                        
                    }
                    