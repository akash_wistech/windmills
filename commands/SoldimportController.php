<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Company;
use app\models\SoldData;

use yii\helpers\Url;
use yii;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SoldimportController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex()
    {

        $all_data = \app\models\SoldDataUpgrade::find()
            ->where(['inserted'=>0])
            //->andWhere(['in', 'building_name', ['zahra apartments 1','zahra apartments 2']])
            ->orderBy([
                'id' => SORT_DESC,
            ])->limit(25)->all();


        foreach ($all_data as $record){
            $buildingRow = \app\models\Buildings::find()
                ->where(['=', 'title', trim($record->building_name)])
                ->orWhere(['=', 'reidin_title', trim($record->building_name)])
                ->orWhere(['=', 'reidin_title_2', trim($record->building_name)])
                ->orWhere(['=', 'reidin_title_2', trim($record->building_name)])
                ->asArray()->one();


            if ($buildingRow != null) {


                $comparison = \app\models\SoldTransaction::find()
                    ->where(['building_info' => $buildingRow['id'],
                        'no_of_bedrooms' => $record->no_of_rooms,
                        'built_up_area' => $record->bua,
                        'unit_number' => $record->unit_number,
                        'land_size' => $record->plot_area,
                        'transaction_date' => $record->sold_date,
                        'listings_price' => $record->price,
                        'price_per_sqt' => $record->price_sqf,
                        // 'reidin_ref_number' => $reidin_ref_number,
                    ])->one();

                if ($comparison == null) {
                    // echo "Record Found"; die();
                    $buildingId = $buildingRow['id'];




                    $model = new \app\models\SoldTransaction;
                    $model->scenario = 'import';
                    $model->building_info = $buildingId;
                    if($record->no_of_rooms != 'Unknown' && $record->no_of_rooms != 'PENTHOUSE' && $record->no_of_rooms <> null){
                        $model->no_of_bedrooms = $record->no_of_rooms;
                    }
                    //  $model->no_of_bedrooms = ($rooms <> null && ($rooms != 'Unknown')) ? $rooms : 0;
                    $model->built_up_area = ($record->bua <> null && $record->bua !== '-') ? $record->bua : 0;
                    $model->land_size = ($record->plot_area <> null && $record->plot_area !== '-') ? $record->plot_area : 0;;
                    $model->unit_number = ($record->unit_number <> null && $record->unit_number !== '-') ? $record->unit_number : '';
                    $model->floor_number = ($record->floor_number <> null && $record->floor_number !== '-') ? $record->floor_number : '';
                    $model->balcony_size = ($record->balcony_size <> null && $record->balcony_size !== '-') ? $record->balcony_size : '';
                    $model->parking_space_number = ($record->parking_space_number <> null && $record->parking_space_number !== '-') ? $record->parking_space_number : '';
                    $model->transaction_type = ($record->transaction_type <> null) ? $record->transaction_type : '';
                    $model->sub_type = ($record->sub_type <> null && $record->sub_type !== '-') ? $record->sub_type : '';
                    $model->sales_sequence = ($record->sales_sequence <> null && $record->sales_sequence !== '-') ? $record->sales_sequence : '';
                    $model->reidin_ref_number = ($record->reidin_ref_number <> null && $record->reidin_ref_number !== '-') ? $record->reidin_ref_number : '';
                    $model->reidin_community = ($record->reidin_community <> null && $record->reidin_community !== '-') ? $record->reidin_community : '';
                    $model->reidin_property_type = ($record->reidin_property_type <> null && $record->reidin_property_type !== '-') ? $record->reidin_property_type : '';
                    $model->reidin_developer = ($record->reidin_developer <> null && $record->reidin_developer !== '-') ? $record->reidin_developer : '';

                    $model->transaction_date = $record->sold_date;
                    $model->listings_price = ($record->price <> null && $record->price !== '-') ? $record->price : 0;

                    $model->property_category = $buildingRow['property_category'];
                    $model->location = $buildingRow['location'];
                    $model->tenure = $buildingRow['tenure'];
                    $model->utilities_connected = $buildingRow['utilities_connected'];
                    $model->development_type = $buildingRow['development_type'];
                    $model->property_placement = $buildingRow['property_placement'];
                    $model->property_visibility = $buildingRow['property_visibility'];
                    $model->property_exposure = $buildingRow['property_exposure'];
                    $model->property_condition = $buildingRow['property_condition'];
                    $model->pool = $buildingRow['pool'];
                    $model->gym = $buildingRow['gym'];
                    $model->play_area = $buildingRow['play_area'];
                    // $model->other_facilities = $buildingRow['other_facilities'];
                    $model->landscaping = $buildingRow['landscaping'];
                    $model->white_goods = $buildingRow['white_goods'];
                    $model->furnished = $buildingRow['furnished'];
                    $model->finished_status = $buildingRow['finished_status'];


                    $model->price_per_sqt = ($record->price_sqf <> null && $record->price_sqf !== '#VALUE!') ? $record->price_sqf : 0;;
                    if($model->transaction_type == 'Sales - Off-Plan'){
                        $model->type= 2;
                    }else{
                        $model->type= 1;
                    }
                    /*  echo "<pre>";
                      print_r($model);
                      die;*/
                    if ($model->save()) {
                        Yii::$app->db->createCommand()->update('sold_data_upgrade', ['inserted' => 1], ['id' => $record->id])->execute();

                    }


                }else{
                    Yii::$app->db->createCommand()->update('sold_data_upgrade', ['inserted' => 2], ['id' => $record->id])->execute();
                }


            }else{
                Yii::$app->db->createCommand()->update('sold_data_upgrade', ['inserted' => 3], ['id' => $record->id])->execute();
            }

        }

        die('here');
    }
}
