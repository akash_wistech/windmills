<?php
namespace app\commands;
use Yii;
use yii\console\Controller;

class QuotationClosedController extends Controller
{
    public function actionIndex()
    {
        $todatDate = date("Y-m-d h:i:s");
        $twoMonthsLessDate = date("Y-m-d", strtotime("-2 months"));

        $models = \app\models\CrmQuotations::find()
        ->select(['id', 'quotation_status'])
        ->where(['quotation_status' => [11,12]])
        ->andWhere(['<=', 'status_change_date', $twoMonthsLessDate])
        ->asArray()
        ->all();
       // echo "<pre>"; print_r($models); echo "</pre>"; die;

        if($models<>null)
        {
            foreach ($models as $key => $model) {
                Yii::$app->db->createCommand()
                ->update('crm_quotations', ['quotation_status' => 13, 'status_change_date' => $todatDate], ['id' => $model['id']])
                ->execute();
            }
        }
    }
}
