<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use yii\helpers\Url;
use  \app\modules\wisnotify\listners\NotifyEvent;


class QuotationEmailController extends Controller
{

    public function actionOnholdQuotations()
    {
        $date = date('Y-m-d', strtotime('-10 days'));
        $quotations = \app\models\CrmQuotations::find()
        ->where(['quotation_status'=>10])
        ->Andwhere(['<=', 'created_at', $date])
        ->asArray()
        ->all();

        $dateTwo = date('Y-m-d', strtotime('-12 days'));
        $quotationsTwo = \app\models\CrmQuotations::find()
        ->where(['quotation_status'=>10])
        ->Andwhere(['<=', 'created_at', $dateTwo])
        ->asArray()
        ->all();

        //send email to business team
        $html  = '';
        $i =1;
        if (is_array($quotations) AND $quotations<>null) {
            foreach ($quotations as $key => $quotation) {
                $html .= $i.') '.Url::base()."/crm-quotations/step_8?id=".$quotation['id']."<br>"; 
                $i++;               
            }
        }

        $notifyData1 = [
            'replacements'=>[
                '{days}'=>10,
                '{status}'=>'On Hold',
                '{html}'=>$html,
            ],
        ];

        $event1 = 'send.QuotationsToBusinessTeam';
        $this->SendFire($notifyData1, $event1);


        //send email to CEO
        $html2  = '';
        $j=1;
        if (is_array($quotationsTwo) AND $quotationsTwo<>null) {
            foreach ($quotationsTwo as $key => $quotation) {
                $html2 .= $j.') '.Url::base()."/crm-quotations/step_8?id=".$quotation['id']."<br>";  
                $j++;              
            }
        }
        $notifyData2 = [
            'replacements'=>[
                '{days}'=>12,
                '{status}'=>'On Hold',
                '{html}'=>$html2,
            ],
        ];
        $event2 = 'send.QuotationsToCEO';
        $this->SendFire($notifyData2, $event2);
    }





    public function actionGetActiveQuotations()
    {
        $date_5_days = date('Y-m-d', strtotime('-5 days'));
        $date_7_days =  date('Y-m-d', strtotime('-7 days'));
        $quotations = \app\models\CrmQuotations::find()
        ->where(['quotation_status'=>0])
        ->where(['<=', 'created_at', $date_5_days.' 00:00:00'])
        ->asArray()
        ->all();
        $html1 = '';
        $i=1;
        $html= '';
        if (is_array($quotations) AND $quotations<>null) {
            foreach($quotations as $quotation){
                $html .= $i.') '.Url::base()."/crm-quotations/step_8?id=".$quotation['id']."<br>";
                $i++;
            }
        }

        // echo "<pre>"; print_r($html); echo "</pre>"; die();

        $notifyData1 = [
            'replacements'=>[
                '{days}'=>5,
                '{status}'=>'Active',
                '{html}'=>$html
            ],
        ];
        $event1 = 'send.QuotationsToBusinessTeam';
        $this->SendFire($notifyData1, $event1);

        $quotations2 = \app\models\CrmQuotations::find()
        ->where(['quotation_status'=>0])
        ->where(['<=', 'created_at',$date_7_days.' 00:00:00'])
        ->asArray()
        ->all();

        $html1 = '';
        $j=1;
        if (is_array($quotations2) AND $quotations2<>null) {
            foreach($quotations2 as $quotation){
                $html .= $j.') '.Url::base()."/crm-quotations/step_8?id=".$quotation['id']."<br>";
                $j++;
            }
        }

        $notifyData2 = [
            'replacements'=>[
                '{days}'=>7,
                '{status}'=>'Active',
                '{html}'=>$html
            ],
        ];
        $event2 = 'send.QuotationsToCEO';
        $this->SendFire($notifyData2, $event2);
    }



    public function SendFire($notifyData='', $keyword='')
    {
        echo "<pre>";
        print_r($notifyData);
        die;
        NotifyEvent::fire2($keyword, $notifyData);
    }
}