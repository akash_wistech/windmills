<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Company;
use app\models\SoldData;

use yii\helpers\Url;
use yii;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SoldadimportController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex()
    {

        $all_data = \app\models\SoldAd::find()
            ->where(['status'=>0])
            ->andWhere(['is not', 'projectsub_comm', null])
            //->andWhere(['<>','projectsub_comm', 'Other'])
           // ->andWhere(['in', 'community', ['z33','z5','z24','Z19','z5','z20','Z12','z2','MSH27','MSH28','MSH7']])
            ->orderBy([
                'id' => SORT_DESC,
            ])->limit(5)->all();



        foreach ($all_data as $record){
            //$record->projectsub_comm = $record->community;
            if($record->projectsub_comm <> null) {
                $buildingRow = \app\models\Buildings::find()
                    ->where(['=', 'title', trim($record->projectsub_comm)])
                    ->orWhere(['=', 'reidin_title', trim($record->projectsub_comm)])
                    ->orWhere(['=', 'reidin_title_2', trim($record->projectsub_comm)])
                    ->orWhere(['=', 'reidin_title_3', trim($record->projectsub_comm)])
                    ->orWhere(['=', 'reidin_title_4', trim($record->projectsub_comm)])
                    ->orWhere(['=', 'reidin_title_5', trim($record->projectsub_comm)])
                    ->orWhere(['=', 'reidin_title_6', trim($record->projectsub_comm)])
                    ->orWhere(['=', 'reidin_title_7', trim($record->projectsub_comm)])
                    ->orWhere(['=', 'reidin_title_8', trim($record->projectsub_comm)])
                    ->asArray()->one();



                if ($buildingRow != null) {

                    if ($buildingRow['city'] == 3506 || $buildingRow['city'] == 4260) {


                        $needle_bedroom = 'Bedroom';
                        if (strpos($record->bedroom, $needle_bedroom) !== false) {
                            $berooms = explode(' Bedroom', trim($record->bedroom));
                            $rooms = trim($berooms[0]);
                        } else {
                            $rooms = null;
                        }


                        if ($record->floor == 'G') {

                            $floor = 0;

                        } else {
                            $str = $record->floor;
                            $str = preg_replace('/\D/', '', $str);
                            $floor = $str;
                        }

                        $bua = str_replace(",", "", $record->unit_area_sqm);
                        $bua = (float)$bua * 10.764;
                        $plotArea = str_replace(",", "", $record->plot_area_sqm);
                        $plotArea = (float)$plotArea * 10.764;
                        $price = str_replace(",", "", $record->price);
                        $pricePerSqt = str_replace(",", "", $record->average_price_sqm);
                        $valuation_amount = str_replace(",", "", $record->valuation_amount);
                            $date = str_replace('/', '-', trim($record->t_date));
                            $date = date("Y-m-d", strtotime($date));
                       // $date = trim($record->t_date);
                        if ($record->projectsub_comm == 'Agricultural Land' || $record->projectsub_comm == 'Development (Commercial) Land' || $record->projectsub_comm == 'Other Land' || $record->projectsub_comm == 'Villa/Townhouse Plot') {
                            if ($plotArea > 0) {
                                $pricePerSqt = round($price / $plotArea);
                            } else {
                                $pricePerSqt = 0;
                            }
                        } else {
                            if ($bua > 0) {
                                $pricePerSqt = round($price / $bua);
                            } else {
                                $pricePerSqt = 0;
                            }
                        }


                        $comparison = \app\models\SoldTransaction::find()
                            ->where(['building_info' => $buildingRow['id'],
                                'no_of_bedrooms' => $rooms,
                                'built_up_area' => $bua,
                                // 'unit_number' => $record->unit_number,
                                'land_size' => $plotArea,
                                'transaction_date' => $date,
                                'listings_price' => $price,
                                'price_per_sqt' => $pricePerSqt,
                                // 'reidin_ref_number' => $reidin_ref_number,
                            ])->one();

                        if ($comparison == null) {

                            // echo "Record Found"; die();
                            $buildingId = $buildingRow['id'];


                            $model = new \app\models\SoldTransaction();
                            $model->scenario = 'import';
                            $model->building_info = $buildingId;
                            if ($record->bedroom != 'Unknown' && $record->bedroom != 'PENTHOUSE' && $record->bedroom <> null) {
                                $model->no_of_bedrooms = $rooms;
                            }
                            //  $model->no_of_bedrooms = ($rooms <> null && ($rooms != 'Unknown')) ? $rooms : 0;
                            $model->built_up_area = ($bua <> null && $bua !== '-') ? $bua : 0;
                            $model->land_size = ($plotArea <> null && $plotArea !== '-') ? $plotArea : 0;;
                            //  $model->unit_number = ($record->unit_number <> null && $record->unit_number !== '-') ? $record->unit_number : '';
                            $model->floor_number = $floor;
                            //  $model->balcony_size = ($record->balcony_size <> null && $record->balcony_size !== '-') ? $record->balcony_size : '';
                            //  $model->parking_space_number = ($record->parking_space_number <> null && $record->parking_space_number !== '-') ? $record->parking_space_number : '';
                            $model->transaction_type = ($record->sales_type <> null) ? $record->sales_type : '';
                            //   $model->sub_type = ($record->sub_type <> null && $record->sub_type !== '-') ? $record->sub_type : '';
                            $model->sales_sequence = ($record->sales_sequence <> null && $record->sales_sequence !== '-') ? $record->sales_sequence : '';
                            $model->reidin_ref_number = ($record->reidin_ref <> null && $record->reidin_ref !== '-') ? $record->reidin_ref : '';
                            $model->reidin_community = ($record->community <> null && $record->community !== '-') ? $record->community : '';
                            $model->reidin_property_type = ($record->property_subtype <> null && $record->property_subtype !== '-') ? $record->property_subtype : '';
                            $model->reidin_developer = ($record->developer <> null && $record->developer !== '-') ? $record->developer : '';
                            $model->projectsub_comm = ($record->projectsub_comm <> null && $record->projectsub_comm !== '-') ? $record->projectsub_comm : '';

                            $model->transaction_date = $date;
                            $model->listings_price = ($price <> null && $price !== '-') ? $price : 0;

                            $model->property_category = $buildingRow['property_category'];
                            $model->location = $buildingRow['location'];
                            $model->tenure = $buildingRow['tenure'];
                            $model->utilities_connected = $buildingRow['utilities_connected'];
                            $model->development_type = $buildingRow['development_type'];
                            $model->property_placement = $buildingRow['property_placement'];
                            $model->property_visibility = $buildingRow['property_visibility'];
                            $model->property_exposure = $buildingRow['property_exposure'];
                            $model->property_condition = $buildingRow['property_condition'];
                            $model->pool = $buildingRow['pool'];
                            $model->gym = $buildingRow['gym'];
                            $model->play_area = $buildingRow['play_area'];
                            // $model->other_facilities = $buildingRow['other_facilities'];
                            $model->landscaping = $buildingRow['landscaping'];
                            $model->white_goods = $buildingRow['white_goods'];
                            $model->furnished = $buildingRow['furnished'];
                            $model->finished_status = $buildingRow['finished_status'];


                            $model->price_per_sqt = ($pricePerSqt <> null && $pricePerSqt !== '#VALUE!') ? $pricePerSqt : 0;;
                            $model->valuation_amount = ($valuation_amount <> null && $valuation_amount !== '#VALUE!') ? $valuation_amount : 0;;
                            $model->valuation_amount = ($valuation_amount <> null && $valuation_amount !== '#VALUE!') ? $valuation_amount : 0;;
                            $model->district = ($record->district <> null && $record->district !== '#VALUE!') ? $record->district : 0;;
                            $model->municipality = ($record->municipality <> null && $record->municipality !== '#VALUE!') ? $record->municipality : 0;;
                            $model->sold_db_id = $record->id;
/*
                              echo "<pre>";
                              print_r($model);
                              die('ddd');*/
                            if ($model->save()) {
                                Yii::$app->db->createCommand()->update('sold_24', ['status' => 1], ['id' => $record->id])->execute();

                            }else{
                                Yii::$app->db->createCommand()->update('sold_24', ['status' => 3], ['id' => $record->id])->execute();
                            }


                        } else {
                            Yii::$app->db->createCommand()->update('sold_24', ['status' => 2], ['id' => $record->id])->execute();
                        }

                    } else {
                        Yii::$app->db->createCommand()->update('sold_24', ['status' => 3], ['id' => $record->id])->execute();
                    }
                } else {
                    echo $record->projectsub_comm;
                   // die('hererr');
                    Yii::$app->db->createCommand()->update('sold_24', ['status' => 3], ['id' => $record->id])->execute();
                }
            }

        }

        die('here');
    }
}
