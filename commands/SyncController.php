<?php
namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use Yii;
use yii\db\Expression;
use app\models\CronJobSyncApi;
use app\models\ApiSource;
use app\models\Prospect;

/**
* Console actions for sync
*/
class SyncController extends Controller
{
  public $limit = 5;
  public function actionProcess($frequency)
  {
    //Get todays done jobs
    // Yii::info('Inside sync process','sync_log');
    $doneProcesses = CronJobSyncApi::find()
    ->select(['api_source_id'])
    ->where(['status'=>[1,2],'DATE(started_at)'=>date("Y-m-d")]);
    //Fetch pending today's jobs
    $results = ApiSource::find()
    ->where([
      'and',
      ['sync_frequency'=>$frequency,'status'=>1],
      [
        'or',
        ['is','last_sync',new Expression('null')],
        ['<=','DATE(last_sync)',date("Y-m-d",strtotime("-1 ".Yii::$app->helperFunctions->syncDateDifArr[$frequency]."",strtotime(date("Y-m-d")))),]
      ],
      ['not in','id',$doneProcesses],
      ['is','deleted_at',new Expression('null')]
    ])
    ->limit($this->limit)
    ->all();
    if($results!=null){
      foreach($results as $result){
        //Create cron job entry
        $cronJob = CronJobSyncApi::find()->where(['api_source_id'=>$result['id'],'DATE(started_at)'=>date("Y-m-d")]);
        if(!$cronJob->exists()){
          $cronJob=new CronJobSyncApi;
          $cronJob->api_source_id = $result['id'];
          $cronJob->started_at = date("Y-m-d H:i:s");
          $cronJob->next_page = $result['url'];
          $cronJob->status = 2;
          $cronJob->save();
        }
      }
    }else{
      $this->syncProcess($frequency);
    }
  }

  public function syncProcess($frequency)
  {
    $connection = Yii::$app->db;
    $results = CronJobSyncApi::find()
    ->where(['DATE(started_at)'=>date("Y-m-d"),'status'=>2])
    ->limit(1)->all();
    if($results!=null){
      foreach($results as $result){
        if($result->next_page!='' && $result->next_page!=null){
          $urlToVisit = $result->next_page;
        }else{
          $urlToVisit = $result->apiSource->url;
        }
        $doneThisTime=0;
        $curlResponse = Yii::$app->helperFunctions->curlReq($urlToVisit);
        if($curlResponse!=null){
          $totalPageCount=0;
          $donePages=0;
          $nextPageLink = '';
          
          $metaInfo = $curlResponse->_meta;
          $linksInfo = $curlResponse->_links;
          $items = $curlResponse->items;
          
          $totalPageCount = $metaInfo->pageCount;
          $donePages = $metaInfo->currentPage;

          $result->total_pages = $totalPageCount;
          $result->done_pages = $donePages;
          
          if(isset($linksInfo) && $linksInfo!=null && isset($linksInfo->next) && $linksInfo->next!=null){
            $result->next_page = $linksInfo->next->href;
          }else{
            $finished = date("Y-m-d H:i:s");
            $result->next_page = NULL;
            $result->status=1;
            $result->finished_at=$finished;
            $connection->createCommand()
            ->update(ApiSource::tableName(), ['last_sync'=>$finished], [
              'id'=>$result->api_source_id,
            ])
            ->execute();
          }
          $result->save();

          if($items!=null){
            foreach($items as $item){
              $status=2;
              $first_name=$item->{Yii::$app->inputHelperFunctions->getApiMapColumn($result->api_source_id,'first_name')};
              $last_name=$item->{Yii::$app->inputHelperFunctions->getApiMapColumn($result->api_source_id,'last_name')};
              $email=$item->{Yii::$app->inputHelperFunctions->getApiMapColumn($result->api_source_id,'email')};

              $statusCol = Yii::$app->inputHelperFunctions->getApiMapColumn($result->api_source_id,'status');
              if($statusCol!='')$status=$item->$statusCol;
              
              $model=Prospect::find()
              ->where([
                'company_id'=>$result->apiSource->company_id,
                'email'=>trim($email),
                'deleted_at'=>null,
              ])
              ->one();
              Yii::info('Checking ('.trim($email).')','sync_log');
              if($model==null){
                Yii::info('New ('.trim($email).')','sync_log');
                $model=new Prospect;
                $model->company_id=$result->apiSource->company_id;
                $model->email=trim($email);
              }
              $model->firstname=$first_name;
              $model->lastname=$last_name;
              $inputFields=Yii::$app->inputHelperFunctions->getCustomerInputTypesByModule($result->apiSource->company_id,'prospect');
              if($inputFields!=null){
                foreach($inputFields as $inputField){
                    // Yii::info('Field: '.$inputField['title'],'sync_log');
                  $targetCol=Yii::$app->inputHelperFunctions->getApiMapColumn($result->api_source_id,$inputField['id']);
                  if($targetCol!=""){
                    //   Yii::info('Target Found: '.$targetCol,'sync_log');
                    $targetCol = $item->$targetCol;
                    if(is_array($targetCol)){
                      $targetColVal = implode(", ",$targetCol);
                    }else{
                      $targetColVal = $targetCol;
                    }
                    // Yii::info('Target Value: '.$targetColVal,'sync_log');
                    $model->input_field[$inputField['id']]=$targetColVal;
                    // Yii::info('Model Input ('.$inputField['id'].'): '.$model->input_field[$inputField['id']],'sync_log');
                  }
                }
              }
              Yii::info('Working on ('.$model->email.')','sync_log');
              $model->status=$status;
              if(!$model->save()){
                  Yii::info('Error while saving('.$result->next_page.'):','sync_log');
                  Yii::info($model->getErrors(),'sync_log');
              }

              $doneThisTime++;
              sleep(1);
            }
          }
          $result->done_records = $result->done_records+$doneThisTime;
          if($result->total_records<>$metaInfo->totalCount)$result->total_records = $metaInfo->totalCount;
          $result->save();
        }
      }
    }
  }
}
