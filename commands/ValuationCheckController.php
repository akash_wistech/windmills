<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\SoldTransaction;
use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Valuation;


use yii\helpers\Url;
use yii;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ValuationCheckController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
     public function actionTime(){


          
            $ImageNullValuation=SoldTransaction::find()->where(['like', 'created_at',date('Y-m-d')])->asArray()->all();
            if (!empty($ImageNullValuation) ) {
             foreach ($ImageNullValuation as $key => $value) {
              $singleVal= Valuation::find()->where(['id'=>$value['id']])->one();
              $singleVal->ban_date=date("Y-m-d");
              // $singleVal->status=2;

              $singleVal->userbycreated->status=2;
              $singleVal->save(); 
               }
               $ImageNullValuationCount=Valuation::find()->where(['<', 'created_at',date('Y-m-d h:i:s', strtotime(' -1 day'))])->andWhere(['signature_img'=>null])->andWhere(['status'=>0])->count();
               print_r('Total '.$ImageNullValuationCount);
                    die();
            }else{
                    print_r('Empty');
                    die();
               }
               
         }

           public function actionBanScanofficer(){
          
            $ImageNullValuation=Valuation::find()->where(['<', 'created_at',date('Y-m-d h:i:s', strtotime(' -2 day'))])->andWhere(['signature_img'=>null])->count();
            if ($ImageNullValuation != null) {
            Yii::$app->db->createCommand()->update('user', ['status' => 0], 'permission_group_id=11')->execute();
            }
            print_r($ImageNullValuation);
                    die();
            
         }


     
}
