<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\ScheduleInspection;
use app\models\Valuation;
use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Company;

use yii\helpers\Url;
use yii;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AutoFollowUpInspectionController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex()
    {

        $scheduled_inspections = ScheduleInspection::find()
            ->where(['valuation_status' => 2])
            ->andWhere(['inspection_reason' => 0])
            ->andWhere(['not', ['inspection_type' => 3]])
            ->orderBy(['id' => SORT_DESC])->asArray()->all();

        $scheduled_inspections_with_reasons = ScheduleInspection::find()
            ->where(['valuation_status' => 2])
            ->andWhere(['not', ['inspection_reason' => 0]])
            ->andWhere(['not', ['inspection_type' => 3]])
            ->orderBy(['id' => SORT_DESC])->asArray()->all();

       /* foreach ($scheduled_inspections as $){

        }*/


    }

}
