<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use app\models\Buildings;
use app\models\Communities;
use app\models\Properties;
use app\models\SoldTransaction;
use app\models\SubCommunities;
use yii\console\ExitCode;
use app\models\Company;
use app\models\SoldData;
use yii\helpers\Url;
use yii;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SoldimportdailyController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex()
    {

        $all_data = \app\models\DailySoldsImport::find()
           // ->where(['in', 'property', ['J8 - Al Sufouh']])
            ->where(['status'=>0])
            ->orderBy([
                'id' => SORT_DESC,
            ])->limit(30)->all();


        foreach ($all_data as $record){




            $transaction_type = $record->transaction_type;
            $sub_type = $record->subtype;
            $sales_sequence = $record->sales_sequence;
            $reidin_ref_number = $record->reidin_ref;
            $date = str_replace('/', '-', trim($record->transaction_date));
            $date = date("Y-m-d", strtotime($date));

            $reidin_community = $record->community;
            $buildingName = $record->property;
            $reidin_property_type = $record->property_type;
            $unit_number = $record->unit;
            $needle = '-';
            if (strpos($unit_number, $needle) !== false) {
                $explode_unit_value = explode('-', $unit_number);
                if($explode_unit_value <> null && count($explode_unit_value) > 0){
                    $unit_number = str_replace($explode_unit_value[0].'-', "", $unit_number);
                    $unit_number= trim($unit_number);
                }
            }

            $rooms = trim($record->bedrooms);

            $floor_number = $record->floor;
            if ($floor_number == 'G') {
                $floor_number = 0;
            }
            $parking_spaces = $record->parking;
            $balcony_size = $record->balcony_area;

            $bua = str_replace(",", "", $record->size_sqf);
            $plotArea = str_replace(",", "", $record->land_size);
            $price = str_replace(",", "", $record->amount);
            $pricePerSqt = str_replace(",", "", $record->sqf);
            $reidin_developer = $record->developer;

            if($buildingName == ''){

                $subCommRow = SubCommunities::find()
                    ->where(['=', 'title', trim($reidin_community)])->one();



                if ($subCommRow != null) {
                    //echo $reidin_property_type;
                    $getProperty_id =  Yii::$app->appHelperFunctions->propertyTypeImport[$reidin_property_type];

                    if($getProperty_id != ''){
                        $property = Properties::findOne($getProperty_id);
                        $property_cat = $property->category;
                    }else{
                        $getProperty_id = 92;
                        $property_cat = 6;
                    }

                    $buildingRow = Buildings::find()
                        ->where(['=', 'title', trim($reidin_community)])
                        ->orWhere(['=', 'reidin_title', trim($reidin_community)])
                        ->asArray()->one();

                    if($buildingRow !=''){

                    }else {


                        $newBuildings = new Buildings;
                        $newBuildings->title = trim($reidin_community);
                        $newBuildings->property_category = $property_cat;
                        $newBuildings->property_id = $getProperty_id;
                        $newBuildings->city = 3510;
                        $newBuildings->tenure = 2;
                        $newBuildings->community = $subCommRow->community;
                        $newBuildings->sub_community = $subCommRow->id;
                        $newBuildings->developer_id = 1;
                        $newBuildings->vacancy = 1;
                        $newBuildings->property_visibility = 3;
                        $newBuildings->utilities_connected = 'Yes';
                        $newBuildings->development_type = 'Standard';
                        $newBuildings->property_condition = 3;
                        $newBuildings->other_facilities = '';
                        $newBuildings->completion_status = 100.00;
                        $newBuildings->location_highway_drive = 'minutes_5';
                        $newBuildings->location_school_drive = 'minutes_5';
                        $newBuildings->location_mall_drive = 'minutes_5';
                        $newBuildings->location_sea_drive = 'minutes_5';
                        $newBuildings->location_park_drive = 'minutes_5';
                        /* echo "<pre>";
                         print_r($subCommRow);
                         print_r($newBuildings);
                         die;*/
                        if (!$newBuildings->save()) {
                            echo "<pre>";
                            print_r($newBuildings->errors);
                            die('sub');
                        }else{
                            Yii::$app->db->createCommand()->update('daily_solds_import', ['new_building_id' => $newBuildings->id], ['id' => $record->id])->execute();
                        }
                    }


                }else{
                    $commRow = Communities::find()
                        ->where(['=', 'title', trim($reidin_community)])
                        ->one();

                    if ($commRow != null) {
                        $getProperty_id =  Yii::$app->appHelperFunctions->propertyTypeImport[$reidin_property_type];

                        if($getProperty_id != ''){
                            $property = Properties::findOne($getProperty_id);
                            $property_cat = $property->category;
                        }else{
                            $getProperty_id = 92;
                            $property_cat = 6;
                        }
                        $subCommRow = SubCommunities::find()
                            ->where(['=', 'community', $commRow->id])
                            ->one();
                        $buildingRow = Buildings::find()
                            ->where(['=', 'title', trim($reidin_community)])
                            ->orWhere(['=', 'reidin_title', trim($reidin_community)])
                            ->asArray()->one();

                        if($buildingRow !=''){

                        }else {


                            $newBuildings = new Buildings;
                            $newBuildings->title = trim($reidin_community);;
                            $newBuildings->property_category = $property_cat;
                            $newBuildings->property_id =$getProperty_id;
                            $newBuildings->city = 3510;
                            $newBuildings->tenure = 2;
                            $newBuildings->property_visibility = 3;
                            $newBuildings->community = $commRow->id;
                            $newBuildings->sub_community = $subCommRow->id;
                            $newBuildings->developer_id = 1;
                            $newBuildings->vacancy = 1;
                            $newBuildings->utilities_connected = 'Yes';
                            $newBuildings->development_type = 'Standard';
                            $newBuildings->property_condition = 3;
                            $newBuildings->other_facilities = '';
                            $newBuildings->completion_status = 100.00;
                            $newBuildings->location_highway_drive = 'minutes_5';
                            $newBuildings->location_school_drive = 'minutes_5';
                            $newBuildings->location_mall_drive = 'minutes_5';
                            $newBuildings->location_sea_drive = 'minutes_5';
                            $newBuildings->location_park_drive = 'minutes_5';
                            if (!$newBuildings->save()) {
                                echo "<pre>";
                                print_r($newBuildings);
                                die('com');
                            }else{
                                Yii::$app->db->createCommand()->update('daily_solds_import', ['new_building_id' => $newBuildings->id], ['id' => $record->id])->execute();
                            }
                        }
                    }
                }

                /*echo "<pre>";
                print_r($line);
                die;
                die('here1');*/
            }







            if($buildingName == ''){
                $buildingId = 0;
                $buildingRow = Buildings::find()
                    ->where(['=', 'title', trim($reidin_community)])
                    ->orWhere(['=', 'reidin_title', trim($reidin_community)])
                    ->orWhere(['=', 'reidin_title_2', trim($reidin_community)])
                    ->asArray()->one();
            }else {

                $buildingId = 0;
                $buildingRow = Buildings::find()
                    ->where(['=', 'title', trim($buildingName)])
                    ->orWhere(['=', 'reidin_title', trim($buildingName)])
                    ->orWhere(['=', 'reidin_title_2', trim($buildingName)])
                    ->asArray()->one();
            }


            if ($buildingRow != null) {



                $building = array(
                    'building_info' => $buildingRow['id'],
                    'no_of_bedrooms' => $rooms,
                    'built_up_area' => $bua,
                    //'unit_number' => $unit_number,
                    'land_size' => $plotArea,
                    'transaction_date' => $date,
                    'listings_price' => $price,
                    'price_per_sqt' => $pricePerSqt,
                    'reidin_ref_number' => $reidin_ref_number,
                );




                $comparison = SoldTransaction::find()
                    ->where(['building_info' => $buildingRow['id'],
                        'no_of_bedrooms' =>  $rooms,
                        'built_up_area' => $bua,
                        //'unit_number' => $unit_number,
                        'land_size' => $plotArea,
                        'transaction_date' => $date,
                        'listings_price' => $price,
                        'price_per_sqt' => $pricePerSqt,
                        // 'reidin_ref_number' => $reidin_ref_number,
                    ])->one();



                if ($comparison == null) {
                    // echo "Record Found"; die();
                    $buildingId = $buildingRow['id'];


                    $model = new SoldTransaction;
                    $model->scenario = 'import';
                    $model->building_info = $buildingId;
                    if($rooms != 'Unknown' && $rooms != 'PENTHOUSE' && $rooms <> null){
                        $model->no_of_bedrooms = $rooms;
                    }else{
                     //   $model->no_of_bedrooms = 0;
                    }
                    $bua = trim($bua);
                    //  $model->no_of_bedrooms = ($rooms <> null && ($rooms != 'Unknown')) ? $rooms : 0;
                    $model->built_up_area = ($bua <> null && $bua !== '-') ?  (float)$bua : 0;
                    $model->land_size = ($plotArea <> null && $plotArea !== '-') ? $plotArea : 0;;
                    $model->unit_number = ($unit_number <> null && $unit_number !== '-') ? $unit_number : '';
                    $model->floor_number = ($floor_number <> null && $floor_number !== '-') ? $floor_number : '';
                    $model->balcony_size = ($balcony_size <> null && $balcony_size !== '-') ? $balcony_size : '';
                    $model->parking_space_number = ($parking_spaces <> null && $parking_spaces !== '-') ? $parking_spaces : '';
                    $model->transaction_type = ($transaction_type <> null) ? $transaction_type : '';
                    $model->sub_type = ($sub_type <> null && $sub_type !== '-') ? $sub_type : '';
                    $model->sales_sequence = ($sales_sequence <> null && $sales_sequence !== '-') ? $sales_sequence : '';
                    $model->reidin_ref_number = ($reidin_ref_number <> null && $reidin_ref_number !== '-') ? $reidin_ref_number : '';
                    $model->reidin_community = ($reidin_community <> null && $reidin_community !== '-') ? $reidin_community : '';
                    $model->reidin_property_type = ($reidin_property_type <> null && $reidin_property_type !== '-') ? $reidin_property_type : '';
                    $model->reidin_developer = ($reidin_developer <> null && $reidin_developer !== '-') ? $reidin_developer : '';

                    $model->transaction_date = $date;
                    $model->listings_price = ($price <> null && $price !== '-') ? $price : 0;

                    $model->property_category = $buildingRow['property_category'];
                    $model->location = $buildingRow['location'];
                    $model->tenure = $buildingRow['tenure'];
                    $model->utilities_connected = $buildingRow['utilities_connected'];
                    $model->development_type = $buildingRow['development_type'];
                    $model->property_placement = $buildingRow['property_placement'];
                    $model->property_visibility = $buildingRow['property_visibility'];
                    $model->property_exposure = $buildingRow['property_exposure'];
                    $model->property_condition = $buildingRow['property_condition'];
                    $model->pool = $buildingRow['pool'];
                    $model->gym = $buildingRow['gym'];
                    $model->play_area = $buildingRow['play_area'];
                    // $model->other_facilities = $buildingRow['other_facilities'];
                    $model->landscaping = $buildingRow['landscaping'];
                    $model->white_goods = $buildingRow['white_goods'];
                    $model->furnished = $buildingRow['furnished'];
                    $model->finished_status = $buildingRow['finished_status'];
                    $model->land_transaction_id = $record->id;


                    $model->price_per_sqt = ($pricePerSqt <> null && $pricePerSqt !== '#VALUE!') ? $pricePerSqt : 0;;
                    if($transaction_type == 'Sales - Off-Plan'){
                        $model->type= 2;
                    }else{
                        $model->type= 1;
                    }
                    /*  echo "<pre>";
                      print_r($model);
                      die;*/
                    if ($model->save()) {
                        Yii::$app->db->createCommand()->update('daily_solds_import', ['status' => 1], ['id' => $record->id])->execute();
                        //  Yii::$app->db->createCommand()->update('daily_solds_import', ['status' => 1], ['id' => $record->id])->execute();
                        // $saved++;
                    } else {
                        if ($model->hasErrors()) {
                            foreach ($model->getErrors() as $error) {
                                if (count($error) > 0) {
                                    foreach ($error as $key => $val) {
                                        echo $val.'<br>';
                                        echo $pricePerSqt;
                                    }
                                }
                            }
                        }
                        // die();
                        /* $errNames .= '<br />' . $buildingName;
                         $unsaved++;*/
                    }


                }else{

                    Yii::$app->db->createCommand()->update('daily_solds_import', ['status' => 2], ['id' => $record->id])->execute();
                }


            } else {
                Yii::$app->db->createCommand()->update('daily_solds_import', ['status' => 3], ['id' => $record->id])->execute();
            }
        }
        die('here');
    }
}
