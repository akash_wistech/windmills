<?php
namespace app\commands;
use Yii;

use yii\console\Controller;
use app\components\traits\bayutRentProperties;
use app\models\ListingsTransactions;
use app\models\UaeByCityUrl;
use Exception;

class UpdateBayutListingController extends Controller
{    
    public function actionUpdate()
    {
        $start = 18;
        $end = 21;
        $now = date('H'); //15
        
        // echo "start:- ".$start.', end:- '.$end.', now:- '.$now; //die;
        
        // if ($now >= $start && $now <= $end)
        // {
            $query = ListingsTransactions::find()->where([ 'source'=>15, 'is_update'=>0 ])->asArray()->limit(50)->all();
            if ($query<>null) {
                foreach($query as $query){                    
                    $data=[];
                    try{
                        $httpClient = new \Goutte\Client();
                        $crawler = $httpClient->request('GET', $query['listing_website_link']);
                    }
                    catch(Exception $err){
                        $error = $err->getMessage(); print_r($error);
                    }
                    
                    try {
                        // Beds, Studio, Baths And Area Array
                        $bed_bath_area = [];
                        $crawler->filter('div._6f6bb3bc div.ba1ca68e._0ee3305d')->each(function($etcNode)use(&$bed_bath_area){
                            $bed_bath_area[$etcNode->filter('span.cfe8d274')->attr('aria-label')] =  $etcNode->filter('span.fc2d1086')->text();
                        });
                        // echo "<pre>"; print_r($bed_bath_area); echo "</pre>"; //die();
                    }
                    catch ( Exception $err ) {
                        $error = $err->getMessage();
                        // echo "1st Catch<br>"; print_r($error); echo "<br>";
                    }
                    
                    try {
                        // Property Array
                        $propertyInfoArr =  [];
                        $i=1;
                        $crawler->filter('ul._033281ab li')->each(function($propertyNode) use(&$propertyInfoArr, &$i) {
                            $propertyInfoArr[$propertyNode->filter('span._3af7fa95')->text()] = $propertyNode->filter('span._812aa185')->text();
                            $i++;
                        });
                    }
                    catch (Exception $err) {
                        $error = $err->getMessage();
                        // echo "2nd Catch<br>"; print_r($error); echo "<br>";
                        
                        try{
                            $j =1;
                            $crawler->filter('ul._033281ab li')->each(function($propertyNode) use(&$propertyInfoArr, &$j, &$i) {
                                if ($j==$i) {
                                    $propertyInfoArr[$propertyNode->filter('div._3af7fa95')->text()] = $propertyNode->filter('span._812aa185')->text();
                                }
                                if ($j>$i) {
                                    $propertyInfoArr[$propertyNode->filter('span._3af7fa95')->text()] = $propertyNode->filter('span._812aa185')->text();
                                }
                                $j++;
                            });
                        }
                        catch(Exception $err){
                            $error = $err->getMessage();
                            // echo "3rd Catch<br>"; print_r($error); echo "<br>";
                        }
                    }
                    
                    // echo "<pre>"; print_r($propertyInfoArr); echo "</pre>";
                    
                    
                    try{
                        $title = $crawler->filter('div._1f0f1758')->text();
                        // echo "<pre>"; print_r($title); echo "</pre>"; die;
                        
                        
                        $titleExplode = explode(',', $title);
                        $buildingTitle = $community = $sub_community = $city = '';
                        if (isset($titleExplode[0]) AND $titleExplode[0]<>null) {
                            $buildingTitle = $titleExplode[0];
                        }
                        if (isset($titleExplode[1]) AND $titleExplode[1]<>null) {
                            $community = $titleExplode[1];
                        }
                        if (isset($titleExplode[3]) AND $titleExplode[3]<>null) {
                            if (isset($titleExplode[2]) && $titleExplode[2]<>null) {
                                $sub_community = $titleExplode[2];
                            }
                        }
                        $city = $query['city_id'];
                        
                        // $propertyDescription = $crawler->filter('div._2015cd68')->text();
                        $listingAndFinalPrice = $crawler->filter('div.c4fc20ba span._105b8a67')->text();
                        
                    }
                    catch(Exception $err){
                        $error = $err->getMessage();
                        // echo "4th Catch<br>"; print_r($error); echo "<br>";
                    }
                    
                    $data['listing_id'] = $query['id'];
                    $data['url'] = $query['listing_website_link'];
                    
                    if ($bed_bath_area<>null) {
                        $bed_bath_area_studio_arr = bayutRentProperties::get_Bed_Bath_Area($bed_bath_area);
                        if ($bed_bath_area_studio_arr<>null) {
                            $data['no_of_bedrooms'] = trim($bed_bath_area_studio_arr['no_of_bedrooms']);
                            $data['no_of_bathrooms'] = trim($bed_bath_area_studio_arr['no_of_bathrooms']);
                            $data['property_size'] = trim($bed_bath_area_studio_arr['area']);
                            $data['studio'] = trim($bed_bath_area_studio_arr['studio']);
                        }
                    }
                    if ($propertyInfoArr<>null) {
                        $propertyInfoArr = bayutRentProperties::getPropertyData($propertyInfoArr);
                        if ($propertyInfoArr<>null) {
                            $data ['property_type'] = trim(strtolower($propertyInfoArr['type']));
                            $data ['purpose'] = trim($propertyInfoArr['purpose']);
                            $data ['ref_number'] = trim($propertyInfoArr['ref_number']);
                            $data ['average_rent'] = trim($propertyInfoArr['Average Rent']);
                            $data ['added_on'] = trim($propertyInfoArr['added_on']);
                        }
                    }
                    
                    $data['property_category'] = trim($property_type);
                    $data['building_id'] = trim($query['building_info']);
                    $data['listing_price'] = trim(str_replace("," , "", $listingAndFinalPrice));
                    // $data['desc'] = trim(strtolower($propertyDescription));
                    
                    // echo "DataStart:<pre>"; print_r($data); echo "</pre>:DataEnd<br>"; die();
                    
                    $this->updateListData($data, $query, $city_id, $purpose);
                }
                // die();
            }
            // }else {
                //     echo "time out";
                // }
                
                
                
            }
            
            
            public function updateListData($data=null , $query=null, $city_id=null, $purpose=null)
            {
                // echo "DataStart:<pre>"; print_r($data); echo "</pre>:DataEnd<br>"; die();
                
                $building = \app\models\Buildings::find()
                ->where(['id' => $data['building_id'] ])
                ->one();
                
                $bedrooms = '';
                if ($data['no_of_bedrooms']<>null && is_numeric($data['no_of_bedrooms'])) {
                    $bedrooms = $data['no_of_bedrooms'];
                }
                
                $building = \app\models\Buildings::find()->where(['id' => $data['building_id']])->one();
                $model = \app\models\ListingsTransactions::find()->where(['id'=>$data['listing_id'] ])->one();
                
                $model->listings_reference   = $data['ref_number'];
                $model->source               = 15; 
                $model->listing_website_link = $data['url']; 
                $model->listing_date         = $data['added_on'];
                $model->unit_number          = 'Not Known'; //static
                
                
                $model->building_info           = $building->id;
                $model->property_category       = $building->property_category;
                $model->tenure                  = $building->tenure;
                $model->community               = $building->community;
                $model->sub_community           = $building->sub_community;
                $model->property_placement      = $building->property_placement;
                $model->property_visibility     = $building->property_visibility;
                $model->property_exposure       = $building->property_exposure;
                $model->property_condition      = $building->property_condition;
                $model->development_type        = $building->development_type;
                $model->utilities_connected     = $building->utilities_connected;
                $model->finished_status         = $building->finished_status;
                $model->pool                    = $building->pool;
                $model->gym                     = $building->gym;
                $model->play_area               = $building->play_area;
                $model->other_facilities        = ($building->other_facilities<>null)? explode(',' , $building->other_facilities) : '';
                $model->completion_status       = $building->completion_status;
                $model->white_goods             = $building->white_goods;
                $model->furnished               = $building->furnished;
                $model->landscaping             = $building->landscaping;
                $model->estimated_age           = ($building->estimated_age<>null)? $building->estimated_age : 0.00;  
                $model->location_highway_drive	= $building->location_highway_drive;
                $model->location_school_drive	= $building->location_school_drive;
                $model->location_mall_drive		= $building->location_mall_drive;
                $model->location_sea_drive		= $building->location_sea_drive;
                $model->location_park_drive		= $building->location_park_drive;
                
                
                
                $model->no_of_bedrooms  = ($bedrooms<>null AND $bedrooms!='') ? $bedrooms : 0;
                $model->built_up_area   = $data['property_size'];
                $model->land_size       = $data['property_size'];
                $model->listings_price  = trim($data['listing_price']);
                $model->listings_rent   = trim($data['listing_price']); //Yii::$app->propertyFinderHelperFunctions->getListingRent($data['property_size'],$bedrooms);
                $model->final_price     = trim($data['listing_price']);
                $model->status          = 2;
                if ($model->save()) {
                    Yii::$app->db->createCommand()
                    ->update('listings_transactions', [ 'is_update' => 1 ] , [ 'id' => $data['listing_id'], 'source'=>15 ])->execute();
                    echo " Updated Successfully ";
                }
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                echo $val. "<br>";
                            }   
                        }
                    }
                    // die();
                }
            }
            
            
        }