<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

use Illuminate\Http\Request;
use Symfony\Component\DomCrawler\Crawler;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Exception;


use app\models\PropertyFinderUrls;
use app\models\PropertyFinderFetchUrl;
use app\models\PropertyFinderDetailUrls;
use app\models\Buildings;
use app\models\ListingsTransactions;

class PropertyFinderScrapeController extends Controller
{
    public function actionSaveUrls($url=null, $link_id=null)
    {
        $findUrl = PropertyFinderFetchUrl::find()->one();
        if ($findUrl==null) {
            $query = PropertyFinderUrls::find()->one();
            if ($query<>null) {
                $model = new PropertyFinderFetchUrl;
                $model->property_finder_url_id = $query->id;
                $model->url_to_fetch = $query->url;
                $model->property_category = $query->property_category;
                if($model->save()){
                    $link_id            = $model->property_finder_url_id;
                    $url                = $model->url_to_fetch;
                    $property_category  = $model->property_category;
                }
            }
        }
        else{
            $link_id                = $findUrl->property_finder_url_id;
            $url                    = $findUrl->url_to_fetch;
            $property_category      = $findUrl->property_category;
        }

        try {
            $response = new \GuzzleHttp\Client();
            $response = $response->get($url);
            $content = $response->getBody()->getContents();
            $crawler = new Crawler( $content );

            $i=1;
            $crawler->filter('div.card-list__item')->each(function (Crawler $node) use(&$i, $property_category){
                $detail_url = $node->filter('a.card.card--clickable')->attr('href');
                $detail_url = "https://www.propertyfinder.ae".$detail_url;
                if ($detail_url<>null) {
                    $model = PropertyFinderDetailUrls::find()->where(['detail_url'=>$detail_url])->one();
                    if ($model==null) {
                        $model = new PropertyFinderDetailUrls;
                        $model->detail_url = $detail_url;
                        $model->property_category = $property_category;
                        $model->status = 0;
                        if ($model->save()) {
                          //  echo "save-".$i." , ";
                        }
                    }

                }
                $i++;
            });

            try{
                $nextPageUrl = $crawler->filter('div.pagination__serp div.pagination a.pagination__link.pagination__link--next')->attr('href');
                if ($nextPageUrl<>null) {
                    $url_to_fetch = 'https://www.propertyfinder.ae'.$nextPageUrl.'';
                    Yii::$app->db->createCommand()
                        ->update('property_finder_fetch_url', ['property_finder_url_id' => $link_id, 'url_to_fetch'=>$url_to_fetch, 'property_category'=>$property_category])
                        ->execute();
                }
            }catch(Exception $err){
                $error = $err->getMessage();
                if ($error=='The current node list is empty.') {
                    $query = PropertyFinderUrls::find()->where(['>', 'id', $link_id])->one();
                    if ($query<>null) {
                        Yii::$app->db->createCommand()
                            ->update('property_finder_fetch_url', ['property_finder_url_id' => $query->id, 'url_to_fetch'=> $query->url, 'property_category'=>$query->property_category])
                            ->execute();
                    }
                }
            }

        } catch ( Exception $err ) {
            $error = $err->getMessage();
            print_r($error);
        }
    }



    public function actionSaveDetail()
    {
        $urls = PropertyFinderDetailUrls::find()->where(['status'=>0])->limit(1)->all();
        if ($urls<>null) {
            foreach ($urls as $urls) {
                Yii::$app->db->createCommand()
                    ->update('property_finder_detail_urls', [ 'status' => 2 ] , [ 'detail_url' => $urls->detail_url ])
                    ->execute();
                $url = $urls->detail_url; //echo $url; die();
                $property_category = $urls->property_category;

                $titleClass = 'div.property-page__column h1.text.text--size6.text--bold.property-page__title';
                $propertyInfoClass = 'div.property-page__column ul.property-facts li';
                $propertyInfoLableClass = 'div.text.property-facts__label';
                $propertyInfoValueClass = 'div.text.text--bold.property-facts__content';
                $agentNameClass = 'div.property-page__column div.property-page__agent-location-area div.property-agent div.property-agent__area div.property-agent__detail-area div.text.text--size3.property-agent__name.property-agent__name--unverified';
                $agentCompanyClass = 'div.property-page__column div.property-page__agent-location-area div.property-agent div.property-agent__area div.property-agent__detail-area div.property-agent__broker-image-area div.property-agent__position-broker-name';
                $amenitiesClass = 'div.property-page__column div.property-amenities div.property-amenities__list';
                $propertyDescriptionClass = 'div.property-page__column div.property-page__column--left';
                $propertyInfo2ndClass = 'div.property-page__column div.property-page__column--left div.panel.panel--style1.panel--style4.panel--style5 ul.property-page__legal-list-area.property-page__legal-list-area--spacing li';
                $propertyInfo2ndLableClass = 'div.property-page__legal-list-label';
                $propertyInfo2ndValueClass = 'div.property-page__legal-list-content';

                try {
                    $response = new \GuzzleHttp\Client();
                    $response = $response->get($url);
                    $content = $response->getBody()->getContents();
                    $crawler = new Crawler( $content );
                    // echo "<pre>"; print_r($crawler); echo "</pre>"; die();

                    $cityComSubComClass = 'div.container div.property-page__breadcrumb-area.property-page__breadcrumb-area--desktop div.property-page__back-button-area div.breadcrumb a.text.text--size1.link.link--underline';
                    $cityComSubComArr = [];
                    $city = $crawler->filter($cityComSubComClass)->each(function (Crawler $node) use (&$cityComSubComArr){
                        $cityComSubComArr[] = $node->text();
                    });


                    $price = '';
                    $priceClass = 'div.property-page__column div.property-page__column--right div.property-price';
                    try{
                        $price = $crawler->filter($priceClass)->text();
                        $explodePrice = explode('AED', $price);
                        $price = str_replace(',', '', $explodePrice[0]);
                        // print_r($price);
                    }catch( Exception $err ){
                        $error = $err->getMessage();
                    }



                    $title = $crawler->filter($titleClass)->text();

                    $propertyInfo = [];
                    $crawler->filter($propertyInfoClass)->each(function (Crawler $node) use (&$propertyInfo,$propertyInfoLableClass,$propertyInfoValueClass){
                        $propertyInfo[$node->filter($propertyInfoLableClass)->text()]=$node->filter($propertyInfoValueClass)->text();
                    });

                    $agent_name='';
                    $cls = 'div.property-page__column div.property-page__agent-location-area div.property-agent div.property-agent__area div.property-agent__detail-area a.text.text--size3.link.link--underline.property-agent__name';
                    try{
                        $agent_name = $crawler->filter($cls)->text();
                    }catch( Exception $err ){
                        $error = $err->getMessage();
                        if ($error=='The current node list is empty.') {
                            $agent_name = $crawler->filter($agentNameClass)->text();
                        }
                    }


                    $agent_company = $crawler->filter($agentCompanyClass)->text();
                    if (strpos($agent_company, ' at ') !== false) {
                        $agent_company = explode(' at ', $agent_company);
                        $agent_company = $agent_company[1];
                    }else{
                        $agent_company = $agent_company;
                    }


                    $amenities = [];
                    $crawler->filter($amenitiesClass)->each(function (Crawler $node) use (&$amenities){
                        $amenities[] = $node->text();
                    });

                    $propertyDescription = $crawler->filter($propertyDescriptionClass)->text();
                    $propertyDescription = strtolower($propertyDescription);

                    $crawler->filter($propertyInfo2ndClass)->each(function (Crawler $node) use (&$propertyInfo,$propertyInfo2ndLableClass,$propertyInfo2ndValueClass){
                        $propertyInfo[$node->filter($propertyInfo2ndLableClass)->text()]=$node->filter($propertyInfo2ndValueClass)->text();
                    });




                    // cityComSubComArr working
                    $city = $community = $sub_community = $building_info = '';
                    if (is_array($cityComSubComArr)) {
                        if (isset($cityComSubComArr[0]) && $cityComSubComArr[0]<>null) {
                            $city = $cityComSubComArr[0];
                        }
                        if (isset($cityComSubComArr[1]) && $cityComSubComArr[1]<>null) {
                            $community = $cityComSubComArr[1];
                        }
                        if (isset($cityComSubComArr[3]) && $cityComSubComArr[3]<>null) {
                            $building_info = $cityComSubComArr[3];
                            if (isset($cityComSubComArr[2]) && $cityComSubComArr[2]<>null) {
                                $sub_community = $cityComSubComArr[2];
                            }
                        }else{
                            if (isset($cityComSubComArr[2]) && $cityComSubComArr[2]<>null) {
                                $building_info = $cityComSubComArr[2];
                            }else{
                                $sub_community = '';
                            }
                        }
                    }


                    $property_type=$property_size=$bedrooms=$bathrooms=$reference=$listed=$trakheesi_permit=$broker_orn=$agent_brn='';
                    if ($propertyInfo<>null) {
                        foreach ($propertyInfo as $key => $value) {
                            // echo $key.'=>'.$value."<br>";
                            if ($key=='Property type:') {
                                $property_type = $value;
                            }
                            if ($key=='Property size:') {
                                $property_size = trim($value);
                                $property_size = explode('/', $property_size);
                                if (isset($property_size[0]) && $property_size[0]<>null) {
                                    $property_size = explode(' sqft', $property_size[0]);
                                    if (isset($property_size[0]) && $property_size[0]<>null) {
                                        $property_size = preg_replace('/[^0-9-,-.]/', '', $property_size[0]);
                                        $property_size = str_replace(',', '', $property_size);
                                    }
                                }
                            }
                            if ($key=='Bedrooms:') {
                                if (strpos($value, '+')!==false) {
                                    $explode = explode('+', $value);
                                    if (isset($explode[0])&&$explode[0]<>null) {
                                        $bedrooms = $explode[0];
                                    }
                                }else{
                                    $bedrooms = $value;
                                }
                            }
                            if ($key=='Bathrooms:') {
                                $bathrooms = $value;
                            }
                            if ($key=='Reference:') {
                                $reference = $value;
                            }
                            if ($key=='Listed:') {
                                $explodeListed = explode(' ', $value);
                                $listed = date('Y-m-d', strtotime('-'.$explodeListed[0].' '.$explodeListed[1]));
                            }
                            if ($key=='Trakheesi Permit: Issued to RERA registered companies and Dubai developers as confirmation of their right to advertise properties') {
                                $trakheesi_permit = $value;
                            }
                            if ($key=='Broker ORN: Office Registration Number given to each RERA-registered real estate company in Dubai') {
                                $broker_orn = $value;
                            }
                            if ($key=='Agent BRN: Broker Registration Number issued to agents working for RERA-registered brokerages in Dubai') {
                                $agent_brn = $value;
                            }
                        }
                    }

                    $data = [
                        'url' => $url,
                        'property_category' => $property_category,
                        'city' => $city,
                        'community' => $community,
                        'sub_community' => $sub_community,
                        'building_info' => $building_info,
                        'property_type' => $property_type,
                        'property_size' => $property_size,
                        'listings_price' => $price,
                        'bedrooms' => $bedrooms,
                        'bathrooms' => $bathrooms,
                        'reference' => $reference,
                        'listed' => $listed,
                        'trakheesi_permit' => $trakheesi_permit,
                        'broker_orn' => $broker_orn,
                        'agent_brn' => $agent_brn,
                        'agent_name' => $agent_name,
                        'agent_company' => $agent_company,
                        'amenities' => $amenities,
                        'description' => $propertyDescription,
                    ];
                    $this->SaveForListing($data);

                }
                catch ( Exception $err ) {
                    $error = $err->getMessage();
                    echo "detail Error<br>";
                    print_r($error);
                }
            }
        }
    }


    public function SaveForListing($data=null)
    {
        if ($data['building_info']<>null) {
            $city_id = Yii::$app->appHelperFunctions->getCityId($data['city']);
            $commAndSubCommId = Yii::$app->propertyFinderHelperFunctions->getCommAndSubCommIds($data['community'],$data['sub_community']);
          //  echo "<pre>"; print_r($commAndSubCommId); echo "</pre>"; //die();

            $building = Buildings::find()->where([
                'title'         => trim($data['building_info']),
                'city'          => $city_id,
            ]);
            if ($commAndSubCommId<>null&&$commAndSubCommId['community_id']<>null&&$commAndSubCommId['sub_community_id']<>null) {
                $building->andWhere([
                    'community'     => $commAndSubCommId['community_id'],
                    'sub_community' => $commAndSubCommId['sub_community_id'],
                ]);
            }
            $building->andWhere(['city'=>$city_id]);
            $building->orWhere(['like', 'title', trim($data['building_info']) . '%', false]);
            $model = $building->one();
            if ($model<>null) {
                $this->saveListingTransaction($data,$model);
            }else{
              //  echo "Building not found";
            }
        }
    }


    public function saveListingTransaction($data=null,$model)
    {
        // echo "<pre>"; print_r($data); echo "</pre><br><br>"; die();
        // echo "<pre>"; print_r($model); echo "</pre><br><br>";

        $desc = $data['description'];
        $white_goods = $furnished = $pool = $gym = $view_sea = '';
        if ($data['amenities']<>null) {
            foreach ($data['amenities'] as $key => $value) {
                if ($value == 'Built in Kitchen Appliances') {
                    $white_goods  = 'Yes';
                }
                if ($value == 'Furnished') {
                    $furnished = 'Yes';
                }
                if ($value == 'Private pool') {
                    $pool = 'Yes';
                }
                if ($value == 'Private Gym') {
                    $gym = 'Yes';
                }
                if ($value == 'View of Water') {
                    $view_sea = 'Yes';
                }
            }
        }
        // print_r($data['bedrooms']); die();
        $bedrooms = $studio = '';
        if ($data['bedrooms']<>null && is_numeric($data['bedrooms'])) {
            $bedrooms = $data['bedrooms'];
        }
        if ($data['bedrooms']<>null && $data['bedrooms'] == 'studio') {
            $studio = 'Yes';
        }

        // echo "studio"; print_r($studio); die();

        // echo $bedrooms; die();
        $findListing = ListingsTransactions::find()->where(['listings_reference'=>$data['reference'], 'listing_website_link'=>$data['url']])->one();
        if ($findListing==null) {
            $listingTransaction = new ListingsTransactions;
            $listingTransaction->listings_reference   = $data['reference']; //done
            $listingTransaction->source               = 16; //done
            $listingTransaction->listing_website_link = $data['url']; //done
            $listingTransaction->listing_date         = $data['listed']; //done
            $listingTransaction->unit_number          = 'Not Known'; //done

            $listingTransaction->building_info = $model->id;

            // property category working
            if ($data['property_category'] == 'Residential') {
                $listingTransaction->property_category = 1;
            }
            else if ($data['property_category'] == 'Commercial') {
                $listingTransaction->property_category = 4;
            }

            //tenure working
            if ($model->tenure<>null) {
                $listingTransaction->tenure = $model->tenure;
            }
            else if(strpos($desc, 'freehold') !== false || strpos($desc, 'free hold') !== false || strpos($desc, 'free-hold') !== false)
            {
                $listingTransaction->tenure   = 2;
            }
            else{
                if ($data['city']=='Dubai') {
                    $listingTransaction->tenure   = 2;
                }else{
                    $listingTransaction->tenure   = 1;
                }
            }

            //property placement working
            if (strpos($data['property_type'], 'Apartment') !== false || strpos($data['property_type'], 'Office') !== false) {
                $listingTransaction->property_placement  = 3.00;
            }else{
                if (strpos($desc, 'middle') !== false || strpos($desc, 'mid unit') !== false || strpos($desc, 'mid-unit') !== false) {
                    $listingTransaction->property_placement  = 1.00;
                }
                else if(strpos($desc, 'corner') !== false){
                    $listingTransaction->property_placement  = 2.00;
                }
                else if (strpos($desc, 'semi corner') !== false || strpos($desc, 'semi-corner') !== false || strpos($desc, 'semicorner') !== false || strpos($desc, 'end unit') !== false) {
                    $listingTransaction->property_placement  = 1.50;
                }
                else{
                    $listingTransaction->property_placement  = 1.00;
                }
            }

            //property visibility
            if ($data['property_type'] == 'Shop') {
                $listingTransaction->property_visibility = 2;

                if(strpos($desc, 'back') !== false || strpos($desc, 'behind') !== false){
                    $listingTransaction->property_visibility = 1;
                }
            }else{
                $listingTransaction->property_visibility = 3;
            }

            //Property Exposure working
            if (strpos($data['property_type'], 'Apartment') !== false || strpos($data['property_type'], 'Office') !== false) {
                $listingTransaction->property_exposure   = 3;
            }
            else{
                if (strpos($desc, 'single row') !== false || strpos($desc, 'single-row') !== false) {
                    $listingTransaction->property_exposure   = 2;
                }
                else if(strpos($desc, 'back to back') !== false || strpos($desc, 'backtoback') !== false || strpos($desc, 'back 2 back') !== false || strpos($desc, 'back2back') !== false || strpos($desc, 'b2b') !== false || strpos($desc, 'back-to-back') !== false){
                    $listingTransaction->property_exposure   = 1;
                }
                else{
                    $listingTransaction->property_exposure   = 1;
                }
            }


            $listingTransaction->property_condition  = 3;
            $listingTransaction->development_type    = 'Standard';

            //Utilities connected working
            if (strpos($data['property_type'], 'Land') !== false) {
                $listingTransaction->utilities_connected = 'No';
            }else{
                $listingTransaction->utilities_connected = 'Yes';
            }

            //Finished status working
            if (strpos($data['property_type'], 'Office') !== false) {
                $listingTransaction->finished_status     = 'Fitted';

                if(strpos($desc, 'fitted') !== false){
                    $listingTransaction->finished_status     = 'Shell & Core';
                }
            }else{
                $listingTransaction->finished_status     = 'Fitted';
            }


            // Pool working
            if ($pool=='Yes') {
                $listingTransaction->pool = 'Yes';
            }
            else if (strpos($desc, 'private pool') !== false ||strpos($desc, 'private swimming pool') !== false ||strpos($desc, 'with pool') !== false ||strpos($desc, 'own swimming pool') !== false || strpos($desc, 'own pool') !== false) {
                $listingTransaction->pool = 'Yes';
            }
            else{
                $listingTransaction->pool = 'No';
            }

            // Gym Working
            if ($gym=='Yes') {
                $listingTransaction->gym = 'Yes';
            }
            else if (strpos($desc, 'private gym') !== false ||strpos($desc, 'own gym') !== false || strpos($desc, 'private fitness') !== false) {
                $listingTransaction->gym = 'Yes';
            }
            else{
                $listingTransaction->gym = 'No';
            }


            // Play area Working
            if (strpos($desc, 'private play') !== false || strpos($desc, 'own play') !== false) {
                $listingTransaction->play_area = 'Yes';
            }
            else{
                $listingTransaction->play_area = 'No';
            }

            $listingTransaction->other_facilities    = ($model->other_facilities<>null)? explode(',' , $model->other_facilities) : '';
            $listingTransaction->completion_status   = 100.00;



            //White goods working
            if ($white_goods=='Yes') {
                $listingTransaction->white_goods  = 'Yes';
            }
            else if (strpos($desc, 'equipped kitchen') !== false || strpos($desc, 'fully equipped') !== false || strpos($desc, 'fitted kitchen') !== false) {
                $listingTransaction->white_goods  = 'Yes';
            }
            else{
                $listingTransaction->white_goods  = 'No';
            }

            //Furnished Working
            if ($furnished == 'Yes') {
                $listingTransaction->furnished = 'Yes';
            }
            else if (strpos($desc, 'fully furnished') !== false) {
                $listingTransaction->furnished = 'Yes';
            }
            else{
                $listingTransaction->furnished = 'No';
            }

            //Landscaping working
            if (strpos($data['property_type'], 'Villa') !== false || strpos($data['property_type'], 'Townhouse') !== false) {
                $listingTransaction->landscaping = 'Yes';
            }
            else{
                $listingTransaction->landscaping  = 'No';
            }

            // no of bedroom
            $listingTransaction->no_of_bedrooms = $bedrooms;

            //Upgrades
            if (strpos($desc, 'upgrades') !== false || strpos($desc, 'upgraded') !== false) {
                $listingTransaction->upgrades  = 4;
            }
            else{
                $listingTransaction->upgrades  = 3;
            }

            // Full Building Floors working
            if (strpos($data['property_type'], 'Land') !== false) {
                $listingTransaction->full_building_floors = 0;
            }
            else if (strpos($data['property_type'], 'Villa') !== false  || strpos($data['property_type'], 'Townhouse') !== false  ) {
                if ($model->typical_floors<>null) {
                    $listingTransaction->full_building_floors = $model->typical_floors;
                }
                else{
                    $listingTransaction->full_building_floors = 1;
                }
            }
            else{
                if ($model->typical_floors<>null) {
                    $listingTransaction->full_building_floors = $model->typical_floors;
                }
                else{
                    $listingTransaction->full_building_floors = 0;
                }
            }


            //Parking Space working
            if (strpos($data['property_type'], 'Villa') !== false || strpos($data['property_type'], 'Townhouse') !== false) {
                $listingTransaction->parking_space  = 2;
            }
            if (strpos($data['property_type'], 'Apartment') !== false) {
                // echo "studio: ". $studio; die();
                if ($studio<>null && $studio=='yes' && $bedrooms==1) {
                    $listingTransaction->parking_space  = 1;
                }
                else if ($studio<>null && $studio=='yes' && $bedrooms==2) {
                    $listingTransaction->parking_space  = 1;
                }
                else if ($bedrooms>=3) {
                    $listingTransaction->parking_space  = 2;
                }
                else{
                    $listingTransaction->parking_space  = 1;
                }
            }
            else{
                $listingTransaction->parking_space  =0;
            }

            //Estiated age working
            if (strpos($data['property_type'], 'Land') !== false) {
                $listingTransaction->estimated_age = 0;
            }else{
                if ($model->estimated_age<>null) {
                    $listingTransaction->estimated_age = $model->estimated_age;
                }else{
                    $listingTransaction->estimated_age = 0;
                }
            }

            //Floor Number Working
            if (strpos($data['property_type'], 'Apartment') !== false || strpos($data['property_type'], 'Office') !== false) {
                if (strpos($desc, 'high floor') !== false || strpos($desc, 'higher floor') !== false || strpos($desc, 'high-floor') !== false) {
                    $listingTransaction->floor_number = 3/4*$model->typical_floors;
                }
                else if (strpos($desc, 'low floor') !== false || strpos($desc, 'lower floor') !== false || strpos($desc, 'low-floor') !== false) {
                    $listingTransaction->floor_number = 1/4*$model->typical_floors;
                }
                else{
                    $listingTransaction->floor_number = 1/2*$model->typical_floors;
                }
            }else{
                $listingTransaction->floor_number = 0;
            }


            //No of levels Working
            if (strpos($data['property_type'], 'Land') !== false || strpos($data['property_type'], 'Apartment') !== false || strpos($data['property_type'], 'Warehouse') !== false || strpos($data['property_type'], 'Office') !== false) {
                $listingTransaction->number_of_levels = 1;
            }
            else if (strpos($data['property_type'], 'Villa') !== false || strpos($data['property_type'], 'Townhouse') !== false || strpos($data['property_type'], 'Duplex') !== false) {
                if ($model->typical_floors<>null) {
                    $listingTransaction->number_of_levels = $model->typical_floors;
                }
                else{
                    $listingTransaction->number_of_levels = 2;
                }
            }
            else{
                if ($model->typical_floors<>null) {
                    $listingTransaction->number_of_levels = $model->typical_floors;
                }else{
                    $listingTransaction->number_of_levels = 2;
                }
            }


            //Start bua and land size
            if (strpos($data['property_type'], 'Apartment') !== false || strpos($data['property_type'], 'Office') !== false) {
                $listingTransaction->land_size = 0;
                $listingTransaction->built_up_area = $data['property_size'];
            }
            else{
                // echo "ello"; die();
                $landAndBuiltUpArea = Yii::$app->propertyFinderHelperFunctions->getBuaAndLandArea($desc,$data['property_size'],$bedrooms);
                // echo "<br>In main function: <pre>"; print_r($landAndBuiltUpArea); echo "</pre>"; die();
                $listingTransaction->built_up_area = $landAndBuiltUpArea['bua'];
                $listingTransaction->land_size = $landAndBuiltUpArea['land_size'];
            }
            //End bua and land size

            $listingTransaction->balcony_size = '0';

            //Start Location Attributes
            $listingTransaction->location_highway_drive = ($model->location_highway_drive<>null) ? $model->location_highway_drive : 'minutes_5';
            $listingTransaction->location_school_drive  = ($model->location_school_drive<>null) ? $model->location_school_drive : 'minutes_5';
            $listingTransaction->location_mall_drive    = ($model->location_mall_drive<>null) ? $model->location_mall_drive : 'minutes_5';
            $listingTransaction->location_sea_drive     = ($model->location_sea_drive<>null) ? $model->location_sea_drive : 'minutes_5';
            $listingTransaction->location_park_drive    = ($model->location_park_drive<>null) ? $model->location_park_drive : 'minutes_5';
            //End Location Attributes


            //Start View Attributes
            $listingTransaction->view_community = 'normal';

            if (strpos($desc, 'shared pool') !== false) {
                $listingTransaction->view_pool = 'none';
            }
            else if (strpos($desc, 'view of pool') !== false || strpos($desc, 'view of fountain') !== false || strpos($desc, 'fountain view') !== false) {
                $listingTransaction->view_pool = 'full';
            }
            else{
                $listingTransaction->view_pool = 'none';
            }
            if (strpos($desc, 'view of burj') !== false || strpos($desc, 'burj khalifa view') !== false || strpos($desc, 'burj view') !== false || strpos($desc, 'burj al arab view') !== false) {
                $listingTransaction->view_burj = 'full';
            }
            else{
                $listingTransaction->view_burj = 'none';
            }
            if ($view_sea=='Yes') {
                $listingTransaction->view_sea = 'full';
            }
            else if (strpos($desc, 'sea view') !== false || strpos($desc, 'view of sea') !== false || strpos($desc, 'view of water') !== false || strpos($desc, 'water view') !== false) {
                $listingTransaction->view_sea = 'full';
            }
            else{
                $listingTransaction->view_sea = 'none';
            }
            if (strpos($desc, 'marina view') !== false || strpos($desc, 'view of marina') !== false || strpos($desc, 'view of dubai marina') !== false || strpos($desc, 'views of dubai marina') !== false || strpos($desc, 'views of marina') !== false) {
                $listingTransaction->view_marina = 'full';
            }
            else{
                $listingTransaction->view_marina = 'none';
            }
            if (strpos($desc, 'view of mountain') !== false || strpos($desc, 'mountain view') !== false || strpos($desc, 'mountains view') !== false || strpos($desc, 'views of mountain') !== false) {
                $listingTransaction->view_mountains = 'full';
            }
            else{
                $listingTransaction->view_mountains = 'none';
            }
            if (strpos($desc, 'view of lake') !== false || strpos($desc, 'lake view') !== false || strpos($desc, 'views of lake') !== false || strpos($desc, 'views of the lake') !== false) {
                $listingTransaction->view_lake = 'full';
            }
            else{
                $listingTransaction->view_lake = 'none';
            }
            if (strpos($desc, 'golf course view') !== false || strpos($desc, 'view of golf course') !== false || strpos($desc, 'views of golfcourse') !== false || strpos($desc, 'views of golf course') !== false || strpos($desc, 'golfcourse view') !== false || strpos($desc, 'view of golfcourse') !== false || strpos($desc, 'view of the golf course') !== false || strpos($desc, 'views of the golf course') !== false || strpos($desc, 'golf view') !== false || strpos($desc, 'view of golf') !== false) {
                $listingTransaction->view_golf_course = 'full';
            }
            else{
                $listingTransaction->view_golf_course = 'none';
            }
            if (strpos($desc, 'park view') !== false || strpos($desc, 'view of park') !== false || strpos($desc, 'view of the park') !== false || strpos($desc, 'views of park') !== false || strpos($desc, 'views of the park') !== false) {
                $listingTransaction->view_park = 'full';
            }
            else{
                $listingTransaction->view_park = 'none';
            }
            $listingTransaction->view_special = 'none';
            //End View Attributes

            //Start Price Information
            $listingPropertyType='';
            $webPageImgUrl = Yii::$app->propertyFinderHelperFunctions->getWebPageImageUrl($data['url']); //echo $webPageImgUrl; die();
            $imgHtml = Yii::$app->propertyFinderHelperFunctions->getFullPageContent($webPageImgUrl);
            if ($imgHtml<>null) {
                $listingPropertyType = Yii::$app->propertyFinderHelperFunctions->getListingPropertyType($imgHtml);
            }
            if ($listingPropertyType<>null) {
                //Listing Property Type Working
                $listingTransaction->listing_property_type = $listingPropertyType;
            }

            // print_r($data['listings_price']); die();
            //price working
            $listingTransaction->listings_price = trim($data['listings_price']);
            $listingTransaction->listings_rent = Yii::$app->propertyFinderHelperFunctions->getListingRent($data['property_size'],$bedrooms);
            $listingTransaction->final_price = trim($data['listings_price']);
            $listingTransaction->listing_website_link_image = ($webPageImgUrl<>null) ? $webPageImgUrl : '';
            $listingTransaction->agent_name = $data['agent_name'];
            $listingTransaction->agent_company = $data['agent_company'];


            //Developer Margin Working
            if (strpos($desc, 'developer price') !== false || strpos($desc, 'direct from developer') !== false || strpos($desc, 'direct from the developer') !== false || strpos($desc, 'directly from the developer') !== false || strpos($desc, 'directly from developer') !== false || strpos($desc, 'payment plan') !== false) {
                $listingTransaction->developer_margin  = 'Yes';
            }else{
                $listingTransaction->developer_margin  = 'No';
            }

            $listingTransaction->status = 2;
            if ($listingTransaction->save()) {
              //  echo " Saved Successfully ";
                Yii::$app->db->createCommand()
                    ->update('property_finder_detail_urls', [ 'status' => 1 ] , [ 'detail_url' => $data['url'] ])
                    ->execute();
            }
            if($listingTransaction->hasErrors()){
                foreach($listingTransaction->getErrors() as $error){
                    if(count($error)>0){
                        foreach($error as $key=>$val){
                       //     echo $val;
                        }
                        die();
                    }
                }
            }

        }else{
          //  echo " listing already exists ";
        }
    }

}
