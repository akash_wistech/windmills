<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Company;
use app\models\SoldData;

use yii\helpers\Url;
use yii;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SoldimportoffplanController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex()
    {

        $all_data = \app\models\SoldOffPlan::find()
            ->where(['status'=>0])
            //->andWhere(['in', 'property', 'Sur La Mer'])
            ->orderBy([
                'id' => SORT_DESC,
            ])->limit(30)->all();


        foreach ($all_data as $record){
            $buildingRow = \app\models\Buildings::find()
                ->where(['=', 'title', trim($record->property)])
                ->orWhere(['=', 'reidin_title', trim($record->property)])
                ->orWhere(['=', 'reidin_title_2', trim($record->property)])
                ->asArray()->one();


            if ($buildingRow != null) {
                if($buildingRow['city'] == 3510) {

                $needle_bedroom = 'B/R';
                if (strpos($record->bedrooms, $needle_bedroom) !== false) {
                    $berooms = explode(' B/R', trim($record->bedrooms));
                    $rooms = trim($berooms[0]);
                } else {
                    $rooms = null;
                }


                if ($record->floor == 'G') {
                    $record->floor = 0;
                }


                $bua = str_replace(",", "", $record->size_sqf);
                $plotArea = str_replace(",", "", $record->land_size);
                $price = str_replace(",", "", $record->price);
                $pricePerSqt = str_replace(",", "", $record->price_per_sqf);
                $date = str_replace('/', '-', trim($record->transanction_date));
                $date = date("Y-m-d", strtotime($date));
                $reidin_developer = $record->developer;


                $comparison = \app\models\SoldTransaction::find()
                    ->where(['building_info' => $buildingRow['id'],
                        'no_of_bedrooms' => $rooms,
                        'built_up_area' => $bua,
                        'unit_number' => $record->unit_number,
                        'land_size' => $plotArea,
                        'transaction_date' => $date,
                        'listings_price' => $price,
                        'price_per_sqt' => $pricePerSqt,
                        // 'reidin_ref_number' => $reidin_ref_number,
                    ])->one();

                if ($comparison == null) {
                    // echo "Record Found"; die();
                    $buildingId = $buildingRow['id'];


                    $model = new \app\models\SoldTransaction;
                    $model->scenario = 'import';
                    $model->building_info = $buildingId;
                    if ($rooms != 'Unknown' && $rooms != 'PENTHOUSE' && $rooms <> null) {
                        $model->no_of_bedrooms = $rooms;
                    }
                    //  $model->no_of_bedrooms = ($rooms <> null && ($rooms != 'Unknown')) ? $rooms : 0;
                    $model->built_up_area = ($bua <> null && $bua !== '-') ? $bua : 0;
                    $model->land_size = ($plotArea <> null && $plotArea !== '-') ? $plotArea : 0;;
                    $model->unit_number = ($record->unit_number <> null && $record->unit_number !== '-') ? $record->unit_number : '';
                    $model->floor_number = ($record->floor <> null && $record->floor !== '-') ? $record->floor : '';
                    $model->balcony_size = ($record->balcony_area <> null && $record->balcony_area !== '-') ? $record->balcony_area : '';
                    $model->parking_space_number = ($record->parking <> null && $record->parking !== '-') ? $record->parking : '';
                    $model->transaction_type = ($record->transaction_type <> null) ? $record->transaction_type : '';
                    $model->sub_type = ($record->subtype <> null && $record->subtype !== '-') ? $record->subtype : '';
                    $model->sales_sequence = ($record->sales_sequence <> null && $record->sales_sequence !== '-') ? $record->sales_sequence : '';
                    $model->reidin_ref_number = ($record->red_number <> null && $record->red_number !== '-') ? $record->red_number : '';
                    $model->reidin_community = ($record->community <> null && $record->community !== '-') ? $record->community : '';
                    $model->reidin_property_type = ($record->property_Type <> null && $record->property_Type !== '-') ? $record->property_Type : '';
                    $model->reidin_developer = ($record->developer <> null && $record->developer !== '-') ? $record->developer : '';

                    $model->transaction_date = $date;
                    $model->listings_price = ($price <> null && $price !== '-') ? $price : 0;

                    $model->property_category = $buildingRow['property_category'];
                    $model->location = $buildingRow['location'];
                    $model->tenure = $buildingRow['tenure'];
                    $model->utilities_connected = $buildingRow['utilities_connected'];
                    $model->development_type = $buildingRow['development_type'];
                    $model->property_placement = $buildingRow['property_placement'];
                    $model->property_visibility = $buildingRow['property_visibility'];
                    $model->property_exposure = $buildingRow['property_exposure'];
                    $model->property_condition = $buildingRow['property_condition'];
                    $model->pool = $buildingRow['pool'];
                    $model->gym = $buildingRow['gym'];
                    $model->play_area = $buildingRow['play_area'];
                    // $model->other_facilities = $buildingRow['other_facilities'];
                    $model->landscaping = $buildingRow['landscaping'];
                    $model->white_goods = $buildingRow['white_goods'];
                    $model->furnished = $buildingRow['furnished'];
                    $model->finished_status = $buildingRow['finished_status'];
                    $model->off_plan_id = $record->id;
                    $model->type = 2;


                    $model->price_per_sqt = ($pricePerSqt <> null && $pricePerSqt !== '#VALUE!') ? $pricePerSqt : 0;;

                    /*  echo "<pre>";
                      print_r($model);
                      die;*/
                    if ($model->save()) {
                        Yii::$app->db->createCommand()->update('sold_off_plan', ['status' => 1], ['id' => $record->id])->execute();

                    }


                } else {
                    Yii::$app->db->createCommand()->update('sold_off_plan', ['status' => 2], ['id' => $record->id])->execute();
                }

            }else{
                    Yii::$app->db->createCommand()->update('sold_off_plan', ['status' => 3], ['id' => $record->id])->execute();
                }


            }else{
                Yii::$app->db->createCommand()->update('sold_off_plan', ['status' => 3], ['id' => $record->id])->execute();
            }

        }

        die('here');
    }
}
