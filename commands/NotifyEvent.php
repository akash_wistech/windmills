<?php
namespace app\modules\wisnotify\listners;

use Yii;
use yii\base\Event;
use yii\base\Component;
use app\modules\wisnotify\models\NotificationTrigger;
use app\modules\wisnotify\models\NotificationTemplate;
use app\models\User;

class NotifyEvent extends Component
{
    public static function fire($eventId,$data)
    {


        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');

        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;
        // Log::debug($eventId);


        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);

                if($userType!=null && $userType!=''){
                    // foreach($userTypes as $key=>$val){
                    $toInfo=[];

                    if($userType==0 && $data['client']!=null){
                        //Client
                        // print_r($data);
                        //              die();
                        $toUser = $data['client'];


                        if($toUser->id == 1){
                            $toInfo[] = [
                                'name' => $toUser->title,
                                'email' => 'DIBUAEESDExternalEvaluation@dib.ae',
                                // 'email'=>'akash@wistech.biz',
                                'mobile' => '',
                                'deviceIdz' => null,
                                'subject' => $data['subject'],
                            ];
                        }else {
                            $toInfo[] = [
                                'name' => $toUser->title,
                                'email' => $toUser->primaryContact->email !== '' ? $toUser->primaryContact->email : '',
                                // 'email'=>'akash@wistech.biz',
                                'mobile' => '',
                                'deviceIdz' => null,
                                'subject' => $data['subject'],
                            ];
                        }


                    }
                    elseif($userType==8 && $data['reviewer']!=null){
                        $toUser = $data['reviewer'];
                        $toInfo[] = [
                            'name'=>$toUser->firstname,
                            'email'=>$toUser->email,
                            'mobile'=>'',
                            'deviceIdz' => null,
                            'subject' => $data['subject'],
                        ];
                    }


                    else{

                        $toUsers = User::find()->where(['permission_group_id'=>$userType,'status'=>1])->all();
                        if($toUsers!=null){
                            foreach($toUsers as $toUser){
                                $toInfo[]=[
                                    'name'=>$toUser->name,
                                    'email'=>$toUser->email,
                                    'mobile'=>'',
                                    'deviceIdz' => null,
                                    'subject' => $data['subject'],
                                ];
                            }
                        }
                    }

                    $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();


                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){



                                (new NotifyEvent)->sendEmail($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments);


                            }
                            //Sms
                            if($ntVal=='sms'){
                                (new NotifyEvent)->sendSms($moduleId,$template,$replacements,$toInfo);
                            }
                            //Push
                            if($ntVal=='push'){
                                (new NotifyEvent)->sendPush($moduleId,$template,$replacements,$toInfo);
                            }
                        }
                    }
                    // }
                }
            }
        }
    }
    public static function fire1($eventId,$data)
    {


        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');

        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;
        // Log::debug($eventId);


        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);

                if($userType!=null && $userType!=''){
                    // foreach($userTypes as $key=>$val){
                    $toInfo=[];

                    if($userType==0 && $data['client']!=null){
                        //Client
                        // print_r($data);
                        //              die();
                        $toUser = $data['client'];

                        if($toUser->id == 1){
                            $toInfo[] = [
                                'name' => $toUser->title,
                                'email' => 'DIBUAEESDExternalEvaluation@dib.ae',
                                // 'email'=>'akash@wistech.biz',
                                'mobile' => '',
                                'deviceIdz' => null,
                                'subject' => $data['subject'],
                                'uid' => $data['uid'],
                                'valuer' => $data['valuer'],
                            ];
                        }else {
                            $toInfo[] = [
                                'name' => $toUser->title,
                                'email' => $toUser->primaryContact->email !== '' ? $toUser->primaryContact->email : '',
                                // 'email'=>'akash@wistech.biz',
                                'mobile' => '',
                                'deviceIdz' => null,
                                'subject' => $data['subject'],
                                'uid' => $data['uid'],
                                'valuer' => $data['valuer'],
                            ];
                        }


                    }
                    elseif($userType==8 && $data['reviewer']!=null){
                        $toUser = $data['reviewer'];
                        $toInfo[] = [
                            'name'=>$toUser->firstname,
                            'email'=>$toUser->email,
                            'mobile'=>'',
                            'deviceIdz' => null,
                            'subject' => $data['subject'],
                            'uid' => $data['uid'],
                            'valuer' => $data['valuer'],
                        ];
                    }


                    else{

                        $toUsers = User::find()->where(['permission_group_id'=>$userType,'status'=>1])->all();
                        if($toUsers!=null){
                            foreach($toUsers as $toUser){
                                $toInfo[]=[
                                    'name'=>$toUser->name,
                                    'email'=>$toUser->email,
                                    'mobile'=>'',
                                    'deviceIdz' => null,
                                    'subject' => $data['subject'],
                                    'uid' => $data['uid'],
                                    'valuer' => $data['valuer'],
                                ];
                            }
                        }
                    }

                    $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();


                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){


                               // if(trim($data['subject']) == "Evaluation request for : EV-123456-4 .") {
                                    (new NotifyEvent)->sendEmail1($moduleId, $template, $replacements, $toInfo, $ccUsers, $attachments);

                                //}
                            }
                            //Sms
                            if($ntVal=='sms'){
                                (new NotifyEvent)->sendSms($moduleId,$template,$replacements,$toInfo);
                            }
                            //Push
                            if($ntVal=='push'){
                                (new NotifyEvent)->sendPush($moduleId,$template,$replacements,$toInfo);
                            }
                        }
                    }
                    // }
                }
            }
        }
    }
    public static function fire2($eventId,$data)
    {



        $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
        $moduleId = \Yii::$app->getModule('wisnotify');

        $replacements = isset($data['replacements']) ? $data['replacements'] : null;
        $attachments = isset($data['attachments']) ? $data['attachments'] : null;
        // Log::debug($eventId);


        if($dbTriggers!=null){
            foreach($dbTriggers as $dbTrigger){
                $userType = $dbTrigger['user_type'];
                $ccUserTypes = json_decode($dbTrigger['cc_roles']);
                $template = NotificationTemplate::findOne($dbTrigger['template_id']);
                $notificationTypes = json_decode($dbTrigger['notification_triggers']);

                if($userType!=null && $userType!=''){
                    // foreach($userTypes as $key=>$val){
                    $toInfo=[];

                    if($userType==0 && $data['client']!=null){
                        //Client
                        // print_r($data);
                        //              die();
                        $toUser = $data['client'];
                        $toInfo[] = [
                            'name'=>$toUser->title,
                            'email'=>$toUser->primaryContact->email !== '' ? $toUser->primaryContact->email : '',
                            // 'email'=>'akash@wistech.biz',
                            'mobile'=>'',
                            'deviceIdz' => null,
                            'subject' => $data['subject'],
                        ];


                    }
                    elseif($userType==8 && $data['reviewer']!=null){
                        $toUser = $data['reviewer'];
                        $toInfo[] = [
                            'name'=>$toUser->firstname,
                            'email'=>$toUser->email,
                            'mobile'=>'',
                            'deviceIdz' => null,
                            'subject' => $data['subject'],
                        ];
                    }


                    else{

                        $toUsers = User::find()->where(['permission_group_id'=>$userType,'status'=>1])->all();
                        if($toUsers!=null){
                            foreach($toUsers as $toUser){
                                $toInfo[]=[
                                    'name'=>$toUser->name,
                                    'email'=>$toUser->email,
                                    'mobile'=>'',
                                    'deviceIdz' => null,
                                    'subject' => $data['subject'],
                                ];
                            }
                        }
                    }

                    $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();


                    if($notificationTypes!=null){
                        foreach($notificationTypes as $ntKey=>$ntVal){
                            //Email
                            if($ntVal=='email'){



                                (new NotifyEvent)->sendEmail2($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments);


                            }
                            //Sms
                            if($ntVal=='sms'){
                                (new NotifyEvent)->sendSms($moduleId,$template,$replacements,$toInfo);
                            }
                            //Push
                            if($ntVal=='push'){
                                (new NotifyEvent)->sendPush($moduleId,$template,$replacements,$toInfo);
                            }
                        }
                    }
                    // }
                }
            }
        }
    }
    public function sendEmail1($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments){


        $emailBody = $template->langDetail->email_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }

        if($toInfo!=null){

            foreach($toInfo as $key=>$info){

                if ($info['email'] !='') {
                    $emailBody = str_replace("{name}",$info['name'],$emailBody);
                    $emailBody = str_replace("{clientName}",$info['name'],$emailBody);

                    Yii::$app->mailer->viewPath = '@app/modules/wisnotify/mail';
                    $mail = Yii::$app->mailer->compose();//compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);
                    //$mail->setTextBody(strip_tags($emailBody));
                    //$mail->setHtmlBody($emailBody);
                    $mail->setFrom([$moduleId->params['notificationSenderEmail'] => $moduleId->params['nNotificationSenderName']]);
                    /* echo "<pre>";
                     print_r($info);
                     die;*/

                    /* $info['email'];
                     die;*/
                    // $to[] = 'akash@wistech.biz';
                    $to[] = $info['email'];
                   // $mail->setTo($info['email']);
                   // $mail->setReplyTo($info['email']);
                    $reply_to =$info['email'];
                    if($ccUsers!=null){
                        foreach($ccUsers as $ccUser){
                            if ($ccUser->email !='') {
                                $ccEmails[]=$ccUser->email;
                            }
                        }

                    }
                    if($info['valuer'] <> null) {
                        $ccEmails[] = $info['valuer'];
                    }

                    /* gmail connection,with port number 993 */
                    $host = '{imap.gmail.com:993/ssl}';
                    /* Your gmail credentials */
                    $user = 'support@windmillsgroup.com';
                    $password = 'wmsupport2021#';
                    /*  $user = 'support@windmillsgroup.com';
                      $password = 'wmsupport2021#';*/

                    /* Establish a IMAP connection */
                    $conn = imap_open($host, $user, $password)
                    or die('unable to connect Gmail: ' . imap_last_error());

                    $allmessages = array();
                    $cc_addaresses=array();

                    if ($info['subject']<>null) {
                        /* Search emails from gmail inbox*/
                        $CheckSubject = imap_search($conn, 'SUBJECT "'.trim($info['subject']).'"');

                        if (!empty($CheckSubject)) {


                            foreach ($CheckSubject as $key => $Singelemail) {
                                $headers = imap_header($conn, $Singelemail, 0);

                                if($headers->subject == trim($info['subject'])){
                                    $allmessages[] = $headers->Msgno;
                                }



                                // $UserEmail=$headers->from[0]->mailbox.'@'.$headers->from[0]->host;
                                /* if ($UserEmail==$info['email']) {
                                     $mail->setSubject('Re:'.$info['subject']);
                                 }else {
                                     $mail->setSubject($info['subject']);
                                 }*/
                            }

                            if (!empty($allmessages)) {
                                //$mail->setSubject('Re:'.$info['subject']);
                                $mail->setSubject($info['subject']);
                            }else {
                                $mail->setSubject($info['subject']);
                            }
                        }else {
                            $mail->setSubject($info['subject']);
                        }

                        /* echo "<pre>";
                         print_r($CheckSubject);
                       //  print_r($CheckSubject);
                         die;*/

                        /* print_r($headers->ccaddress);
                         print_r($cc_addaresses);
                         print_r(quoted_printable_decode($message));
                         die;*/

                        if($template->id == 10 && !empty($allmessages)) {
                           /* $message = imap_fetchbody($conn,end($allmessages),3.2);
                            if($message <> null){

                            }else {
                                $message = imap_fetchbody($conn, end($allmessages), 2.2);
                                if($message <> null){

                                }else {
                                    $message = imap_fetchbody($conn, end($allmessages), 1);
                                    if($message <> null){

                                    }else {
                                        $message = imap_fetchbody($conn, end($allmessages), 1.2);
                                    }

                                }
                            }*/
                            //$message = imap_fetchbody($conn, end($allmessages), 1.2);
                           /* if($message <> null){

                            }else {
                                $message = imap_fetchbody($conn, end($allmessages), 1.1);
                            }*/
                           // $headers = imap_headerinfo($conn, end($allmessages));
                            $headers = imap_headerinfo($conn, $allmessages[0]);

                            if($headers->cc <> null && !empty($headers->cc) ){


                                foreach ($headers->cc as $key => $Singelemail) {


                                    $cc_addaresses[]=$Singelemail->mailbox.'@'.$Singelemail->host;
                                }

                            }
                            if(isset($headers->reply_to) && $headers->reply_to <> null && !empty($headers->reply_to)){
                                $reply_to = $headers->reply_to[0]->mailbox.'@'.$headers->reply_to[0]->host;
                            }



                           // $client_email_body = quoted_printable_decode($message);
                           // $client_email_body = $headers->subject;
                            if($headers->subject <> null) {
                                $client_email_body = $info['subject'];
                            }
                           /* echo "<pre>";
                            print_r($headers->subject);
                            die;*/

                            if($client_email_body <> null){
                                $email_with_respect_to_client = 'With respect to following email:<br><br>';
                                $email_with_respect_to_client .=$client_email_body;
                                $emailBody = str_replace("{WITHRESPECTTO}", $email_with_respect_to_client, $emailBody);
                            }else{
                                $emailBody = str_replace("{WITHRESPECTTO}", '', $emailBody);
                            }
                        }else if(!empty($allmessages)){

                            $headers = imap_headerinfo($conn,  $allmessages[0]);

                            if($headers->cc <> null && !empty($headers->cc) ){


                                foreach ($headers->cc as $key => $Singelemail) {


                                    $cc_addaresses[]=$Singelemail->mailbox.'@'.$Singelemail->host;
                                }

                            }
                            if(isset($headers->reply_to) && $headers->reply_to <> null && !empty($headers->reply_to)){
                                $reply_to = $headers->reply_to[0]->mailbox.'@'.$headers->reply_to[0]->host;
                            }
                            $emailBody = str_replace("{WITHRESPECTTO}", '', $emailBody);

                        }else{
                            $emailBody = str_replace("{WITHRESPECTTO}", '', $emailBody);
                        }



                        /* echo "<pre>";
                         print_r($emailBody);
                         die;*/
                        $mail->setHtmlBody($emailBody);
                        $ccEmails = array_merge($ccEmails,$cc_addaresses);
                        /* echo "<pre>";
                         print_r($ccEmails);
                         die;*/
                        if($info['email'] == 'Santunu.Barua@rakbank.ae'){
                            $ccEmails[]= 'anand.neelakantan@rakbank.ae';
                        }

                        if($ccEmails!=null)$mail->setCc($ccEmails);


                    }
                    else{
                        $mail->setSubject($template->langDetail->subject);
                    }

                    if($attachments!=null){
                        foreach($attachments as $attachment){
                            $mail->attach($attachment);
                        }
                    }

                    if($info['email'] == 'DIBUAEESDExternalEvaluation@dib.ae'){
                        $mail->setTo('DIBUAEESDExternalEvaluation@dib.ae');
                        $mail->setReplyTo('DIBUAEESDExternalEvaluation@dib.ae');
                    }else{
                        $mail->setTo($reply_to);
                        $mail->setReplyTo($reply_to);
                    }


                    /*echo $reply_to;
                    die;*/

                    $mail->setHeader('Sender', 'support@windmillsgroup.com');
                    //  $mail->setHeader('Message-ID', '8b825002b011b834a1e2bc9abeb7aa81@local.windmills');
                    // $mail->setHeader('In-Reply-To', $info['uid'].'@local.windmills');
                    // $mail->setHeader('Thread-Index', $info['uid'].'@local.windmills');
                    $mail->setHeader('References', $info['uid'].'@local.windmills');

                    $mail->send();
                    return true;
                }
            }
        }
    }
    /*
    * Send Emsil
    */
    public function sendEmail($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments){

        $emailBody = $template->langDetail->email_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }

        if($toInfo!=null){
            foreach($toInfo as $key=>$info){

                if ($info['email'] !='') {
                    $emailBody = str_replace("{name}",$info['name'],$emailBody);
                    $emailBody = str_replace("{clientName}",$info['name'],$emailBody);

                    Yii::$app->mailer->viewPath = '@app/modules/wisnotify/mail';
                    $mail = Yii::$app->mailer->compose();//compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);
                    $mail->setTextBody(strip_tags($emailBody));
                    $mail->setHtmlBody($emailBody);
                    $mail->setFrom([$moduleId->params['notificationSenderEmail'] => $moduleId->params['nNotificationSenderName']]);
                    $mail->setTo($info['email']);
                    if($ccUsers!=null){
                        foreach($ccUsers as $ccUser){
                            if ($ccUser->email !='') {
                                $ccEmails[]=$ccUser->email;
                            }
                        }
                         if($ccEmails!=null)$mail->setCc($ccEmails);

                    }

                    /* gmail connection,with port number 993 */
                    $host = '{imap.gmail.com:993/ssl}';
                    /* Your gmail credentials */
                    $user = 'support@windmillsgroup.com';
                    $password = 'wmsupport2021#';

                    /* Establish a IMAP connection */
                    $conn = imap_open($host, $user, $password)
                    or die('unable to connect Gmail: ' . imap_last_error());



                    if ($info['subject']<>null) {
                        /* Search emails from gmail inbox*/
                        $CheckSubject = imap_search($conn, 'SUBJECT "'.$info['subject'].'"');
                        if (!empty($CheckSubject)) {

                            foreach ($CheckSubject as $key => $Singelemail) {
                                $headers = imap_header($conn, $Singelemail, 0);
                                $UserEmail=$headers->from[0]->mailbox.'@'.$headers->from[0]->host;
                                if ($UserEmail==$info['email']) {
                                    $mail->setSubject('Re:'.$info['subject']);
                                    // print_r($UserEmail);
                                    // die();
                                }else {
                                    $mail->setSubject($info['subject']);
                                }
                            }
                        }else {
                            $mail->setSubject($info['subject']);
                        }
                    }
                    else{
                        $mail->setSubject($template->langDetail->subject);
                    }

                    if($attachments!=null){
                        foreach($attachments as $attachment){
                            $mail->attach($attachment);
                        }
                    }
                   
                    $mail->send();
                }
            }
        }
    }

    public function sendEmail2($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments){

        $emailBody = $template->langDetail->email_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $emailBody = str_replace($rKey,$rVal,$emailBody);
            }
        }

        if($toInfo!=null){


            foreach($toInfo as $key=>$info){

                if ($info['email'] !='') {
                    $emailBody = str_replace("{name}",$info['name'],$emailBody);
                    $emailBody = str_replace("{clientName}",$info['name'],$emailBody);

                    Yii::$app->mailerb->viewPath = '@app/modules/wisnotify/mail';
                    $mail = Yii::$app->mailerb->compose();//compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);
                    $mail->setTextBody(strip_tags($emailBody));
                    $mail->setHtmlBody($emailBody);
                    $mail->setFrom([$moduleId->params['snotificationSenderEmail'] => $moduleId->params['sNotificationSenderName']]);
                    $mail->setTo($info['email']);
                    if($ccUsers!=null){
                        foreach($ccUsers as $ccUser){
                            if ($ccUser->email !='') {
                                $ccEmails[]=$ccUser->email;
                            }
                        }
                        $ccEmails[]= 'support@windmillsgroup.com';
                        if($ccEmails!=null)$mail->setCc($ccEmails);

                    }

                    $mail->setSubject($template->langDetail->subject);

                    if($attachments!=null){
                        foreach($attachments as $attachment){
                            $mail->attach($attachment);
                        }
                    }


                      $mail->send();


                }
            }
        }
    }

    /*
    * Send Sms
    */
    public function sendSms($moduleId,$template,$replacements,$toInfo){
        $smsBody = $template->langDetail->sms_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $smsBody = str_replace($rKey,$rVal,$smsBody);
            }
        }
        if($toInfo!=null){
            foreach($toInfo as $key=>$info){
                if($info['mobile']!=''){
                    $smsBody = str_replace("{name}",$toInfo['name'],$smsBody);
                }
            }
        }
    }

    /*
    * Send Push Notification
    */
    public function sendPush($moduleId,$template,$replacements,$toInfo){
        $pushBody = $template->langDetail->push_body;
        if($replacements!=null){
            foreach($replacements as $rKey=>$rVal){
                $pushBody = str_replace($rKey,$rVal,$pushBody);
            }
        }
        $pushBody = str_replace("{name}",$toInfo['name'],$pushBody);
    }
}
