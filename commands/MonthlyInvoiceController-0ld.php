<?php
namespace app\commands;
use Yii;
use yii\console\Controller;
use app\models\Valuation;
use app\models\Company;
use yii\helpers\Url;


class MonthlyInvoiceController extends Controller
{
	public function actionIndex()
	{
		$day = date('d');
		$day=1;
		if($day == 1){
			$clients = Company::find()->where(['client_type'=>'bank','send_monthly_report'=>1])->all();
			if($clients <> null)
			{
				foreach($clients as $key => $client)
				{
					$curl_handle=curl_init();
					curl_setopt($curl_handle, CURLOPT_URL,Url::toRoute(['valuation/invoice_monthly_new', 'id' => $client->id, 'goal' => 'email']));
					curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
					curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($curl_handle, CURLOPT_USERAGENT, 'Maxima');
					
					$val = curl_exec($curl_handle);
					$ip = curl_getinfo($curl_handle,CURLINFO_PRIMARY_IP);
					curl_close($curl_handle);
					$attachments[]=$val;
					
					$notifyData = [
						'client' => $client,
						'uid' => $client->id,
						'attachments' => $attachments,
						// 'subject' => $model->email_subject,
						//'valuer' => $model->approver->email,
						'valuer' => '',
						'replacements'=>[
							'{clientName}'=>   $client->title,
						],
					];
                    \app\modules\wisnotify\listners\NotifyEvent::fireMonthly('Client.pdf.send', $notifyData);
				}
			}
		}
		else{
			echo "day is not 1";
		}
	}
}