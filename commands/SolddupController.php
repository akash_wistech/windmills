<?php
namespace app\commands;
use app\models\Buildings;
use app\models\ListingsTransactions;
use app\models\MergeProjectChilds;
use app\models\SoldTransaction;
use Yii;
use app\models\ListData;
use yii\console\Controller;
use yii\db\Expression;
use app\models\BuildingForSave;

class SolddupController extends Controller
{
    public function actionIndex()
    {
        $mergprojects = MergeProjectChilds::find()->select('merge_id')->distinct()->asArray()->all();
        if($mergprojects <> null){
            $count=0;
            foreach ($mergprojects as $project_m) {

                $merge_data_set = SoldTransaction::find()
                    ->where(['duplicate_status' => 0])
                    ->andWhere(['building_info' => $project_m['merge_id']])
                    ->andWhere(['not', ['old_merge_id_building' => null]])
                    ->andWhere(['not', ['old_merge_id' => null]])
                    ->limit(5)
                    ->asArray()
                    ->all();


                $duplicate_list = array();
                $correct_list = array();
                if ($merge_data_set <> null) {
                    foreach ($merge_data_set as $record) {


                        if ($record['reidin_ref_number'] <> null) {

                            $comparison = SoldTransaction::find()
                                ->where(['building_info' => $record['building_info'],
                                    'reidin_ref_number' => $record['reidin_ref_number'],
                                    'old_merge_id_building' => null,
                                    'old_merge_id' => null
                                ])->one();


                        } else {
                            $comparison = SoldTransaction::find()
                                ->where(['building_info' => $record['building_info'],
                                    'no_of_bedrooms' => $record['no_of_bedrooms'],
                                    'built_up_area' => $record['built_up_area'],
                                    'land_size' => $record['land_size'],
                                    'transaction_date' => $record['transaction_date'],
                                    'listings_price' => $record['listings_price'],
                                    'price_per_sqt' => $record['price_per_sqt'],
                                    'old_merge_id_building' => null,
                                    'old_merge_id' => null
                                ])->one();
                        }
                        if ($comparison == null) {


                            if ($record['reidin_ref_number'] <> null) {

                                $comparison_2 = SoldTransaction::find()
                                    ->where(['building_info' => $record['building_info'],
                                        'reidin_ref_number' => $record['reidin_ref_number'],
                                        'duplicate_status' => 6,
                                    ])->one();


                            } else {
                                $comparison_2 = SoldTransaction::find()
                                    ->where(['building_info' => $record['building_info'],
                                        'no_of_bedrooms' => $record['no_of_bedrooms'],
                                        'built_up_area' => $record['built_up_area'],
                                        'land_size' => $record['land_size'],
                                        'transaction_date' => $record['transaction_date'],
                                        'listings_price' => $record['listings_price'],
                                        'price_per_sqt' => $record['price_per_sqt'],
                                        'duplicate_status' => 6,
                                    ])->one();
                            }

                            if ($comparison_2 == null) {
                                $correct_list[] = $record['id'];
                            } else {
                                $duplicate_list[] = $record['id'];
                            }
                        } else {
                            $duplicate_list[] = $record['id'];
                        }
                        $count++;
                    }
                    Yii::$app->db->createCommand()->update('sold_transaction', ['duplicate_status' => 8], ['id' => $duplicate_list])->execute();
                    Yii::$app->db->createCommand()->update('sold_transaction', ['duplicate_status' => 6], ['id' => $correct_list])->execute();
                    exit();
                }
            }
        }


            
    }
        
        public function actionListingDup()
        {
            $mergprojects = MergeProjectChilds::find()->select('merge_id')->distinct()->asArray()->all();
            if($mergprojects <> null) {
                foreach ($mergprojects as $project_m) {
                    $merge_data_set = ListingsTransactions::find()
                        ->where(['duplicate_status' => 0])
                        ->andWhere(['building_info' => $project_m['merge_id']])
                        ->andWhere(['not', ['old_merge_id_building' => null]])
                        ->andWhere(['not', ['old_merge_id' => null]])
                        ->limit(5)
                        ->asArray()
                        ->all();
                    if ($merge_data_set <> null) {
                        $duplicate_list = array();
                        $correct_list = array();
                        foreach ($merge_data_set as $record) {

                            $comparison = ListingsTransactions::find()
                                ->where(['building_info' => $record['building_info'],
                                    'no_of_bedrooms' => $record['no_of_bedrooms'],
                                    'built_up_area' => $record['built_up_area'],
                                    'listings_price' => $record['listings_price'],
                                    'listings_reference' => $record['listings_reference'],
                                    'old_merge_id_building' => null,
                                    'old_merge_id' => null
                                ])->one();

                            if ($comparison == null) {
                                $comparison_2 = ListingsTransactions::find()
                                    ->where(['building_info' => $record['building_info'],
                                        'no_of_bedrooms' => $record['no_of_bedrooms'],
                                        'built_up_area' => $record['built_up_area'],
                                        'listings_price' => $record['listings_price'],
                                        'listings_reference' => $record['listings_reference'],
                                        'duplicate_status' => 6,
                                    ])->one();

                                if ($comparison_2 == null) {
                                    $correct_list[] = $record['id'];
                                } else {
                                    $duplicate_list[] = $record['id'];
                                }


                            } else {
                                $duplicate_list[] = $record['id'];
                            }
                        }
                        Yii::$app->db->createCommand()->update('listings_transactions', ['duplicate_status' => 8], ['id' => $duplicate_list])->execute();
                        Yii::$app->db->createCommand()->update('listings_transactions', ['duplicate_status' => 6], ['id' => $correct_list])->execute();
                  exit();
                    }
                }
            }

        }

        
    }
    
    