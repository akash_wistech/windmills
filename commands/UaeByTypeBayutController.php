<?php
namespace app\commands;
use Yii;

use yii\console\Controller;
use app\components\traits\BayutRentProperties;
use Exception;
use app\models\UaeTypeUrl;
use app\models\UaeTypeDtlUrl;
use app\models\UaeListData;

class UaeByTypeBayutController extends Controller
{
    public $type = 'apartments';
    
    public function actionIndex($typename)
    {    
        // echo "typename:- ". $typename; die();
        $query = UaeTypeUrl::find()->where(['type'=>$typename])->asArray()->one();
        if($query<>null)
        {
            $httpClient = new \Goutte\Client();
            $response = $httpClient->request('GET', $query['url']);
            $i=1;
            $response->filter('._357a9937 li.ef447dde')->each(function ($node) use(&$query, &$typename, &$i) {
                $link = $node->filter('article.ca2f5674 div._4041eb80 a')->attr('href');
                $link = 'https://www.bayut.com'.$link;
                if ($link<>null) {
                    $newModel = UaeTypeDtlUrl::find()->where(['url'=>$link])->one();
                    if ($newModel==null) {
                        $newModel                    = new UaeTypeDtlUrl;
                        $newModel->url               = $link;
                        $newModel->status            = 0;
                        $newModel->property_category = $query['property_category'];
                        $newModel->type              = $typename;
                        if ($newModel->save()) {
                            echo " save => ".$i."<br>";
                        }
                    }
                }
                $i++;
            }); 
            // die();
            $buttonLinks = [];
            $buttonTitles = [];
            $response->filter('div._035521cc div._41cc3033 div.bbfbe3d2 div._6cab5d36 ul._92c36ba1 li a')->each(function($buttonNode) use(&$buttonLinks,&$buttonTitles){
                $buttonLinks[] = $buttonNode->attr('href');
                $buttonTitles[] = $buttonNode->attr('title');
            });
            $nextPageUrl=null;
            foreach ($buttonLinks as $key => $value) {
                if ($buttonTitles[$key]=='Next') {
                    $nextPageUrl = 'https://www.bayut.com'.$value;
                }
            }           
            if ($nextPageUrl<>null) {
                Yii::$app->db->createCommand()
                ->update('uae_type_url', ['url' => $nextPageUrl], ['type' => $typename])
                ->execute();
            }
            else{
            }
        }
    }
    
    public function actionDetail($typename)
    {
        // echo "typename:- ". $typename; die;
        $query = UaeTypeDtlUrl::find()->where(['status'=>0, 'type'=>$typename])->asArray()->limit(50)->all();
        // echo "<pre>"; print_r($query); echo "</pre>"; die();
        if ($query<>null) {
            foreach($query as $query){
                Yii::$app->db->createCommand()->update('uae_type_dtl_url', [ 'status' => 2 ], [ 'url'=>$query['url'], 'type' => $typename ])->execute();
                
                $data=[];
                try{
                    $httpClient = new \Goutte\Client();
                    $crawler = $httpClient->request('GET', $query['url']);
                }
                catch(Exception $err){
                    $error = $err->getMessage(); print_r($error);
                }
                
                try {
                    // Beds, Studio, Baths And Area Array
                    $bed_bath_area = [];
                    $crawler->filter('div._6f6bb3bc div.ba1ca68e._0ee3305d')->each(function($etcNode)use(&$bed_bath_area){
                        $bed_bath_area[$etcNode->filter('span.cfe8d274')->attr('aria-label')] =  $etcNode->filter('span.fc2d1086')->text();
                    });
                }
                catch ( Exception $err ) {
                    $error = $err->getMessage();
                    // echo "1st Catch<br>"; print_r($error); echo "<br>";
                }
                
                try {
                    // Property Array
                    $propertyInfoArr =  [];
                    $i=1;
                    $crawler->filter('ul._033281ab li')->each(function($propertyNode) use(&$propertyInfoArr, &$i) {
                        $propertyInfoArr[$propertyNode->filter('span._3af7fa95')->text()] = $propertyNode->filter('span._812aa185')->text();
                        $i++;
                    });
                }
                catch (Exception $err) {
                    $error = $err->getMessage();
                    // echo "2nd Catch<br>"; print_r($error); echo "<br>";
                    
                    try{
                        $j =1;
                        $crawler->filter('ul._033281ab li')->each(function($propertyNode) use(&$propertyInfoArr, &$j, &$i) {
                            if ($j==$i) {
                                $propertyInfoArr[$propertyNode->filter('div._3af7fa95')->text()] = $propertyNode->filter('span._812aa185')->text();
                            }
                            if ($j>$i) {
                                $propertyInfoArr[$propertyNode->filter('span._3af7fa95')->text()] = $propertyNode->filter('span._812aa185')->text();
                            }
                            $j++;
                        });
                    }
                    catch(Exception $err){
                        $error = $err->getMessage();
                        // echo "3rd Catch<br>"; print_r($error); echo "<br>";
                    }
                }
                
                try{
                    $title = $crawler->filter('div._1f0f1758')->text();
                    
                    $titleExplode = explode(',', $title);
                    $buildingTitle = $community = $sub_community = $city = '';
                    if (isset($titleExplode[0]) AND $titleExplode[0]<>null) {
                        $buildingTitle = $titleExplode[0];
                    }
                    if (isset($titleExplode[1]) AND $titleExplode[1]<>null) {
                        $community = $titleExplode[1];
                    }
                    if (isset($titleExplode[3]) AND $titleExplode[3]<>null) {
                        if (isset($titleExplode[2]) && $titleExplode[2]<>null) {
                            $sub_community = $titleExplode[2];
                        }
                    }
                    $city = array_pop($titleExplode);
                    
                    // $propertyDescription = $crawler->filter('div._2015cd68')->text();
                    $listingAndFinalPrice = $crawler->filter('div.c4fc20ba span._105b8a67')->text();
                    
                }
                catch(Exception $err){
                    $error = $err->getMessage();
                    // echo "4th Catch<br>"; print_r($error); echo "<br>";
                }
                
                $data['url'] = $query['url'];
                if ($bed_bath_area<>null) {
                    $bed_bath_area_studio_arr = bayutRentProperties::get_Bed_Bath_Area($bed_bath_area);
                    if ($bed_bath_area_studio_arr<>null) {
                        $data['no_of_bedrooms'] = trim($bed_bath_area_studio_arr['no_of_bedrooms']);
                        $data['no_of_bathrooms'] = trim($bed_bath_area_studio_arr['no_of_bathrooms']);
                        $data['property_size'] = trim($bed_bath_area_studio_arr['area']);
                        $data['studio'] = trim($bed_bath_area_studio_arr['studio']);
                    }
                }
                if ($propertyInfoArr<>null) {
                    $propertyInfoArr = bayutRentProperties::getPropertyData($propertyInfoArr);
                    if ($propertyInfoArr<>null) {
                        $data ['property_type'] = trim(strtolower($propertyInfoArr['type']));
                        $data ['purpose'] = trim($propertyInfoArr['purpose']);
                        $data ['ref_number'] = trim($propertyInfoArr['ref_number']);
                        // $data ['average_rent'] = trim($propertyInfoArr['Average Rent']);
                        $data ['added_on'] = trim($propertyInfoArr['added_on']);
                    }
                }
                
                $data['property_category'] = trim($query['property_category']);
                $data['building_title'] = trim($buildingTitle);
                $data['community'] = trim($community);
                $data['sub_community'] = trim($sub_community);
                $data['city'] = trim($city);
                $data['listing_price'] = trim(str_replace("," , "", $listingAndFinalPrice));
                // $data['desc'] = trim(strtolower($propertyDescription));
                
                echo "DataStart:<pre>"; print_r($data); echo "</pre>:DataEnd<br>"; die();
                
                $this->saveListData($data, $query, $typename);
            }
        }
    }
    
    
    public function saveListData($data=null , $query=null, $typename=null)
    {
        $bedrooms = '';
        if ($data['no_of_bedrooms']<>null && is_numeric($data['no_of_bedrooms'])) {
            $bedrooms = $data['no_of_bedrooms'];
        }
        $city_id='';
        if($data['city']=='Dubai') { $city_id = 3510; }
        if($data['city']=='Abu Dhabi') { $city_id = 3506; }
        if($data['city']=='Sharjah') { $city_id = 3509; }
        if($data['city']=='Ajman') { $city_id = 3507; }
        if($data['city']=='Ras Al Khaimah') { $city_id = 3511; }
        if($data['city']=='Umm Al Quwain') { $city_id = 3512; }
        if($data['city']=='Al Ain') { $city_id = 4260; }
        if($data['city']=='Fujairah') { $city_id = 3508; }
        
        $findListing = UaeListData::find()->where(['listings_reference'=>$data['ref_number'], 'listing_website_link'=>$data['url']])->one();
        if ($findListing==null) {
            $listingTransaction = new UaeListData;
            $listingTransaction->listings_reference   = $data['ref_number'];
            $listingTransaction->source               = 15; 
            $listingTransaction->listing_website_link = $data['url']; 
            $listingTransaction->listing_date         = $data['added_on'];
            $listingTransaction->building_info        = $data['building_title'];
            $listingTransaction->city_id              = $city_id;
            $listingTransaction->property_category    = $data['property_category'];
            $listingTransaction->community            = $data['community'];
            $listingTransaction->sub_community        = $data['sub_community'];
            $listingTransaction->property_type        = $data['property_type'];
            $listingTransaction->no_of_bedrooms       = ($bedrooms<>null AND $bedrooms!='') ? $bedrooms : 0;
            $listingTransaction->no_of_bathrooms      = ($data['no_of_bathrooms']<>null AND $data['no_of_bathrooms']!='') ? $data['no_of_bathrooms'] : 0;
            $listingTransaction->built_up_area        = $data['property_size'];
            $listingTransaction->land_size            = $data['property_size'];
            $listingTransaction->listings_price       = trim($data['listing_price']);
            $listingTransaction->listings_rent        = trim($data['listing_price']); //Yii::$app->propertyFinderHelperFunctions->getListingRent($data['property_size'],$bedrooms);
            $listingTransaction->final_price          = trim($data['listing_price']);
            $listingTransaction->status               = 2;
            if ($listingTransaction->save()) {
                Yii::$app->db->createCommand()->update('uae_type_dtl_url', [ 'status' => 1 ], [ 'url'=>$data['url'], 'type' => $typename ])->execute();
                echo " Saved Successfully "; //die();
            }
            if($listingTransaction->hasErrors()){
                foreach($listingTransaction->getErrors() as $error){
                    if(count($error)>0){
                        foreach($error as $key=>$val){
                            echo $val. "<br>";
                        }   
                    }
                }
                die();
            }
        }else{
            echo " listing already exists ";
        }
    }
    
    
}