<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Company;

use yii\helpers\Url;
use yii;
use app\controllers\CommunitiesController;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CheckimagesController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex()
    {

        $url = 'https://maxima.windmillsgroup.com/communities/imagesresize'; // Change this to your actual URL

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 60); // Timeout in seconds

        // Optional: More curl options depending on your requirements
        // curl_setopt($curl, CURLOPT_POST, true);
        // curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($postData));

        $response = curl_exec($curl);

        if ($response === false) {
            // Handle error; curl_error($curl) can give you more details
            echo "CURL Error: " . curl_error($curl) . PHP_EOL;
        } else {
            // Process your response here
            echo "Response from web application: " . $response . PHP_EOL;
        }

        curl_close($curl);

        die;
    }
}
