<?php

namespace app\controllers;

use app\models\AutoListings;
use app\models\AutoListingsMax;
use app\models\Branch;
use app\models\Buildings;
use app\models\Company;
use app\models\ConfigurationFiles;
use app\models\CostApproach;
use app\models\CostDetails;
use app\models\Country;
use app\models\CrmQuotations;
use app\models\CrmReceivedProperties;
use app\models\CustomAttachements;
use app\models\GreenEffects;
use app\models\IncomeApproach;
use app\models\InspectionCancelReasons;
use app\models\InspectionCancelReasonsLog;
use app\models\InspectProperty;
use app\models\ListingsTransactions;
use app\models\ListingsTransactionsSearch;
use app\models\PreviousTransactions;
use app\models\PreviousTransactionsSearch;
use app\models\Properties;
use app\models\PublicDocsFiles;
use app\models\ReceivedDocs;
use app\models\ReceivedDocsFiles;
use app\models\ReCostAssets;
use app\models\RecostValues;
use app\models\ReinstatementCostsValues;
use app\models\ReTowersData;
use app\models\ScheduleInspection;
use app\models\ScheduleInspectionHistory;
use app\models\SoldTransactionSearch;
use app\models\TransportData;
use app\models\User;
use app\models\UserProfileInfo;
use app\models\ValuationAgents;
use app\models\ValuationApproversData;
use app\models\ValuationConfiguration;
use app\models\ValuationConflict;
use app\models\ValuationDetail;
use app\models\ValuationDetailData;
use app\models\ValuationDeveloperDriveMargin;
use app\models\ValuationDmPayments;
use app\models\ValuationDriveMv;
use app\models\ValuationEnquiry;
use app\models\ValuationListCalculation;
use app\models\ValuationOwners;
use app\models\ValuationPublicData;
use app\models\ValuationReportsSearch;
use app\models\ValuationSelectedLists;
use app\models\ValuationSelectedListsAuto;
use app\models\Zone;
use Yii;
use app\models\Valuation;
use app\models\ValuationSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use \app\modules\wisnotify\listners\NotifyEvent;
use DateTime;
use QuickBooksOnline\API\Facades\TaxService;
use QuickBooksOnline\API\Facades\TaxRate;
use QuickBooksOnline\API\DataService\DataService;

/**
 * ValuationController implements the CRUD actions for Valuation model.
 */
class ValuationController extends DefController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Valuation models.
     * @return mixed
     */
    public function actionIndex()
    {

        $this->checkLogin();


        /*$valuations = Valuation::find()->where(['fee'=>null])->all();
        foreach ($valuations as $valuation ){
            $fee_parameters = array();
            $fee_parameters['clientType'] = $valuation->client->client_type;
            $fee_parameters['paymentTerms'] = "0%";
            $fee_parameters['property'] = $valuation->property_id;
            $fee_parameters['city'] = $valuation->building->city;
            $fee_parameters['tenure'] = $valuation->tenure;
            $fee_parameters['complexity'] = $valuation->property->complexity;
            $fee_parameters['repeat_valuation'] = 'new-valuation';
            $fee_parameters['built_up_area'] = $valuation->inspectProperty->built_up_area;
            $fee_parameters['type_of_valuation'] = $valuation->inspection_type;
            $fee_parameters['number_of_comparables'] = '11-15';
            $fee_parameters['no_of_units'] = 1;
            $fee_parameters['land_size'] = $valuation->land_size;
            $amount = yii::$app->propertyCalcHelperFunction->getAmount_new($fee_parameters);

            if (isset($amount['fee'])) {
                Yii::$app->db->createCommand()->update('valuation', ['fee' =>$amount['fee']], 'id='.$valuation->id .'')->execute();
            }
        }*/

        $allowedId = [1, 14];
        if (Yii::$app->user->identity->permission_group_id == 15) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['report-all']);
        }

        $searchModel = new ValuationSearch();
        $searchModel->search_status_check = 2;

        if (Yii::$app->request->queryParams['ValuationSearch']['page_title'] == "Pending Valuation Received") {
            $searchModel->search_status_check = 3;
        }

        if (Yii::$app->request->queryParams['ValuationSearch']['page_title'] == "Pending Inspection Scheduled") {
            $searchModel->search_status_check = 4;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $page_title = Yii::$app->request->queryParams['ValuationSearch']['page_title'];

        // Disable pagination
        if (isset(Yii::$app->request->queryParams['ValuationSearch']['target']) && Yii::$app->request->queryParams['ValuationSearch']['target'] == 'from_dashboard') {
            $dataProvider->pagination = false;
        }

        if (isset(Yii::$app->request->queryParams['ValuationSearch']['page_title'])) {
            return $this->render('pending_valuation_performance', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'allowedId' => $allowedId,
                'page_title' => $page_title,
            ]);
        } else {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'allowedId' => $allowedId,
                'page_title' => $page_title,
            ]);
        }


    }

    public function actionIncome()
    {

        $this->checkLogin();

        $allowedId = [1, 14];
        if (Yii::$app->user->identity->permission_group_id == 15) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['report-all']);
        }
        $searchModel = new ValuationSearch();
        $searchModel->search_status_check = 2;

        if (Yii::$app->request->queryParams['ValuationSearch']['page_title'] == "Pending Valuation Received") {
            $searchModel->search_status_check = 3;
        }

        if (Yii::$app->request->queryParams['ValuationSearch']['page_title'] == "Pending Inspection Scheduled") {
            $searchModel->search_status_check = 4;
        }

        // for incom apporach
        $searchModel->valuation_approach = 1;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $page_title = Yii::$app->request->queryParams['ValuationSearch']['page_title'];

        // Disable pagination
        if (isset(Yii::$app->request->queryParams['ValuationSearch']['target']) && Yii::$app->request->queryParams['ValuationSearch']['target'] == 'from_dashboard') {
            $dataProvider->pagination = false;
        }

        if (isset(Yii::$app->request->queryParams['ValuationSearch']['page_title'])) {
            return $this->render('pending_valuation_performance', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'allowedId' => $allowedId,
                'page_title' => $page_title,
            ]);
        } else {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'allowedId' => $allowedId,
                'page_title' => $page_title,
            ]);
        }


    }

    public function actionIncomeApprovedList()
    {
        $this->checkLogin();
        $searchModel = new ValuationSearch();
        $searchModel->search_status_check = 1;
        $searchModel->valuation_approach = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('approved_list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIncomeCancelledList()
    {
        $this->checkLogin();
        $searchModel = new ValuationSearch();
        $searchModel->search_status_check = 3;
        $searchModel->valuation_approach = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('cancelled_list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionApprovedList()
    {
        $this->checkLogin();
        $searchModel = new ValuationSearch();
        $searchModel->search_status_check = 1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('approved_list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionApprovedTodayList()
    {
        $this->checkLogin();
        $today_start_date = date('Y-m-d') . ' 00:00:00';
        $today_end_date = date('Y-m-d') . ' 23:59:59';
        //$selected_data = ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $id, 'type' => 'sold'])->all(), 'id', 'selected_id');
        $valuationReport_today_approved = ArrayHelper::map(\app\models\ValuationApproversData::find()->where(['approver_type' => 'approver'])->andFilterWhere(['between', 'created_at', $today_start_date, $today_end_date])->all(), 'id', 'valuation_id');
        /* echo "<pre>";
         print_r(count($valuationReport_today_approved));
         die;*/
        $searchModel = new ValuationSearch();
        $searchModel->search_status_check = 1;
        // $searchModel->id = $valuationReport_today_approved;
        $dataProvider = $searchModel->searchTodayApproved(Yii::$app->request->queryParams, $valuationReport_today_approved);

        return $this->render('today_list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'page_title' => 'Today Approved',
            'callback_url' => 'recommended-today-list',
        ]);
    }

    public function actionRecommendedTodayList_old()
    {
        $this->checkLogin();
        $today_start_date = date('Y-m-d') . ' 00:00:00';
        $today_end_date = date('Y-m-d') . ' 23:59:59';


        $totalResults_valuer = (new \yii\db\Query())
            ->select('*')
            ->from('valuation')
            ->innerJoin('valuation_approvers_data', "valuation_approvers_data.valuation_id=valuation.id")
            //->innerJoin('schedule_inspection ON valuation.id=schedule_inspection.valuation_id')
            ->where(['valuation.parent_id' => null])
            // ->andWhere(['valuation.valuation_status' => 3])
            ->andFilterWhere(['between', 'valuation_approvers_data.created_at', $today_start_date, $today_end_date])
            ->andWhere(['valuation_approvers_data.approver_type' => 'valuer'])
            ->all();

        //$selected_data = ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $id, 'type' => 'sold'])->all(), 'id', 'selected_id');
        $valuationReport_today_approved = ArrayHelper::map($totalResults_valuer, 'id', 'valuation_id');
        $searchModel = new ValuationSearch();
        $searchModel->search_status_check = 1;
        // $searchModel->id = $valuationReport_today_approved;
        $dataProvider = $searchModel->searchTodayRecomended(Yii::$app->request->queryParams, $valuationReport_today_approved);

        return $this->render('today_list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'page_title' => 'Today Recommended',
        ]);
    }

    public function actionRecommendedTodayList()
    {
        $this->checkLogin();
        $today_start_date = date('Y-m-d') . ' 00:00:00';
        $today_end_date = date('Y-m-d') . ' 23:59:59';


        $totalResults_valuer = (new \yii\db\Query())
            ->select('*')
            ->from('valuation')
            ->innerJoin('valuation_approvers_data', "valuation_approvers_data.valuation_id=valuation.id")
//->innerJoin('schedule_inspection ON valuation.id=schedule_inspection.valuation_id')
            ->where(['valuation.parent_id' => null])
            // ->andWhere(['valuation.valuation_status' => 3])
            ->andFilterWhere(['between', 'valuation_approvers_data.created_at', $today_start_date, $today_end_date])
            ->andWhere(['valuation_approvers_data.approver_type' => 'valuer'])
            ->all();

        //$selected_data = ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $id, 'type' => 'sold'])->all(), 'id', 'selected_id');
        $valuationReport_today_approved = ArrayHelper::map($totalResults_valuer, 'id', 'valuation_id');
        $searchModel = new ValuationSearch();
        $searchModel->search_status_check = 1;
        // $searchModel->id = $valuationReport_today_approved;
        $dataProvider = $searchModel->searchTodayRecomended(Yii::$app->request->queryParams, $valuationReport_today_approved);

        return $this->render('today_list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'page_title' => 'Today Recommended',
        ]);
    }

    public function actionRecommendedPendingList()
    {
        $this->checkLogin();
        $today_start_date = date('Y-m-d') . ' 00:00:00';
        $today_end_date = date('Y-m-d') . ' 23:59:59';


        $totalResults_valuer = (new \yii\db\Query())
            ->select('*')
            ->from('valuation')
            ->innerJoin('valuation_approvers_data', "valuation_approvers_data.valuation_id=valuation.id")
            //->innerJoin('schedule_inspection ON valuation.id=schedule_inspection.valuation_id')
            ->where(['valuation.parent_id' => null])
            ->andWhere(['valuation.valuation_status' => 3])
            // ->andFilterWhere(['between', 'valuation_approvers_data.created_at', $today_start_date, $today_end_date])

            ->andWhere(['valuation_approvers_data.approver_type' => 'valuer'])
            ->all();

        //$selected_data = ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $id, 'type' => 'sold'])->all(), 'id', 'selected_id');
        $valuationReport_today_approved = ArrayHelper::map($totalResults_valuer, 'id', 'valuation_id');
        $searchModel = new ValuationSearch();
        $searchModel->search_status_check = 1;
        // $searchModel->id = $valuationReport_today_approved;
        $dataProvider = $searchModel->searchTodayRecomended(Yii::$app->request->queryParams, $valuationReport_today_approved);

        return $this->render('pending_valuation_performance', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'page_title' => 'Pending Recommended',
        ]);
    }

    public function actionReviewedPendingList()
    {
        $this->checkLogin();
        $today_start_date = date('Y-m-d') . ' 00:00:00';
        $today_end_date = date('Y-m-d') . ' 23:59:59';


        $totalResults_valuer = (new \yii\db\Query())
            ->select('*')
            ->from('valuation')
            ->innerJoin('valuation_approvers_data', "valuation_approvers_data.valuation_id=valuation.id")
            //->innerJoin('schedule_inspection ON valuation.id=schedule_inspection.valuation_id')
            ->where(['valuation.parent_id' => null])
            ->andWhere(['valuation.valuation_status' => 3])
            // ->andFilterWhere(['between', 'valuation_approvers_data.created_at', $today_start_date, $today_end_date])

            ->andWhere(['valuation_approvers_data.approver_type' => 'reviewer'])
            ->all();

        //$selected_data = ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $id, 'type' => 'sold'])->all(), 'id', 'selected_id');
        $valuationReport_today_approved = ArrayHelper::map($totalResults_valuer, 'id', 'valuation_id');
        $searchModel = new ValuationSearch();
        $searchModel->search_status_check = 1;
        // $searchModel->id = $valuationReport_today_approved;
        $dataProvider = $searchModel->searchTodayRecomended(Yii::$app->request->queryParams, $valuationReport_today_approved);

        return $this->render('pending_valuation_performance', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'page_title' => 'Pending Review',
        ]);
    }

    public function actionRecommendedActiveList()
    {
        $this->checkLogin();
        $valuationReport_today_approved = array();
        $totalResults_valuer_active_count = (new \yii\db\Query())
            ->select('valuation.id')
            ->from('valuation')
            ->innerJoin('valuation_approvers_data', "valuation_approvers_data.valuation_id=valuation.id")
            ->andWhere(['valuation.valuation_status' => [3]])
            ->andWhere(['valuation_approvers_data.approver_type' => 'valuer']);

        //start by usama
        if (isset(Yii::$app->request->queryParams['ValuationSearch']['time_period'])) {
            $time_period = Yii::$app->request->queryParams['ValuationSearch']['time_period'];
        }
        if (isset(Yii::$app->request->queryParams['ValuationSearch']['custom_date_btw'])) {
            $custom_date_btw = Yii::$app->request->queryParams['ValuationSearch']['custom_date_btw'];
        }
        if ($time_period <> null && $time_period != 0) {
            if ($time_period == 9) {
                if ($custom_date_btw <> null) {
                    $Date = (explode(" - ", $custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            } else {
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }
            $totalResults_valuer_active_count->andFilterWhere([
                'between',
                Valuation::tableName() . '.instruction_date',
                $from_date . " 00:00:00",
                $to_date . " 23:59:59"
            ]);
        }
        //end start by usama


        $totalResults_valuer_active_count = $totalResults_valuer_active_count->all();
        if ($totalResults_valuer_active_count <> null && !empty($totalResults_valuer_active_count)) {
            foreach ($totalResults_valuer_active_count as $value) {
                $valuationReport_today_approved[$value['id']] = $value['id'];

            }
        }
        $searchModel = new ValuationSearch();
        $searchModel->search_status_check = 1;
        // $searchModel->id = $valuationReport_today_approved;
        $dataProvider = $searchModel->searchTodayRecomended(Yii::$app->request->queryParams, $valuationReport_today_approved);

        return $this->render('today_list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'page_title' => 'Active Recommended',
            'callback_url' => 'recommended-active-list',
        ]);
    }

    public function actionInspectedTodayList()
    {
        $this->checkLogin();
        $today_start_date = date('Y-m-d') . ' 00:00:00';
        $today_end_date = date('Y-m-d') . ' 23:59:59';
        //$selected_data = ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $id, 'type' => 'sold'])->all(), 'id', 'selected_id');
        $valuationReport_today_inspected = ArrayHelper::map(\app\models\InspectProperty::find()->where(['status' => 1])->andFilterWhere(['between', 'created_at', $today_start_date, $today_end_date])->all(), 'id', 'valuation_id');

        $searchModel = new ValuationSearch();
        $searchModel->search_status_check = 1;
        // $searchModel->id = $valuationReport_today_approved;
        $dataProvider = $searchModel->searchTodayInspected(Yii::$app->request->queryParams, $valuationReport_today_inspected);

        return $this->render('today_list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'page_title' => 'Today Inspected',
        ]);
    }

    public function actionCancelledList()
    {
        $this->checkLogin();
        $searchModel = new ValuationSearch();
        $searchModel->search_status_check = 3;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('cancelled_list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Valuation model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Valuation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->checkLogin();

        $model = new Valuation(['scenario' => Valuation::SCENARIO_INQUIRY]);

        if ($model->load(Yii::$app->request->post())) {
            $model->status = 2;
            $this->StatusVerify($model);
            $model->valuation_status = 1;
            $model->step_number = 1;
            if ($model->contact_email == '' && $model->contact_phone_no == '' && $model->land_line_no == '') {

                Yii::$app->getSession()->addFlash('error', "Please enter Contact detail first!");
                return $this->redirect(['valuation/create']);
            }

            $model->valuation_approach = 0;

            if ($model->save()) {
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                    'action' => 'data_created',
                    'verify_field' => 'status',
                ]);

                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                return $this->redirect(['valuation/step_1/' . $model->id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    /**
     * Creates a new Valuation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionIncomeCreate()
    {
        $this->checkLogin();

        $model = new Valuation(['scenario' => Valuation::SCENARIO_INQUIRY]);

        if ($model->load(Yii::$app->request->post())) {
            $model->status = 2;
            $this->StatusVerify($model);
            $model->valuation_status = 1;
            $model->step_number = 1;
            if ($model->contact_email == '' && $model->contact_phone_no == '' && $model->land_line_no == '') {

                Yii::$app->getSession()->addFlash('error', "Please enter Contact detail first!");
                return $this->redirect(['valuation/income-create']);
            }

            // for date format change
            $model->instruction_date = date('Y-m-d', strtotime($model->instruction_date));
            $model->client_requirement_date = date('Y-m-d', strtotime($model->client_requirement_date));
            $model->target_date = date('Y-m-d', strtotime($model->target_date));

            $model->valuation_approach = 1;

            if ($model->save()) {
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                    'action' => 'data_created',
                    'verify_field' => 'status',
                ]);


                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                return $this->redirect(['valuation/step_1/' . $model->id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }


        return $this->render('income-create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Valuation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        $model = $this->redirect('valuation/step_1/' . $id);
        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_1/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionStep_1($id)
    {
        if (!Yii::$app->menuHelperFunction->checkActionAllowed('step_1')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $model = $this->findModel($id);
        $model->scenario = Valuation::SCENARIO_INQUIRY;
        if ($model->valuation_status == 5) {
            if (isset($_POST['Valuation']['taqyeem_number']) && isset($_POST['Valuation']['taqyeem_number'])) {
                Yii::$app->db->createCommand()->update('valuation', ['taqyeem_number' => $_POST['Valuation']['taqyeem_number']], 'id=' . $model->id . '')->execute();

                if (isset($model->building->city) && ($model->building->city == 3507) && $model->ajman_email_status == 1 && $model->ajman_approved != 1) {
                    if (isset($_POST['Valuation']['ajman_approved']) && ($_POST['Valuation']['ajman_approved'] == 1)) {
                        $model_approver = ValuationApproversData::find()->where(['valuation_id' => $id, 'approver_type' => 'approver'])->one();
                        Yii::$app->db->createCommand()->update('valuation', ['ajman_approved' => 1], 'id=' . $model->id . '')->execute();
                        Yii::$app->helperFunctions->getModelStepSubmit($model_approver, $model);
                    }
                }

                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_1/' . $id]);

            }
        }

        // check value for valuation approach view change
        $valuation_approach = $model->valuation_approach;

        $old_verify_status = $model->status;
        $model->status_verified = $model->status;
        if ($model->load(Yii::$app->request->post())) {

            // for date format change
            $model->instruction_date = date('Y-m-d', strtotime($model->instruction_date));
            $model->client_requirement_date = date('Y-m-d', strtotime($model->client_requirement_date));
            $model->target_date = date('Y-m-d', strtotime($model->target_date));

            //$model->valuation_status = 1;
            $model->status = $model->status_verified;
            $this->StatusVerify($model);
            $model->step_number = 1;
            if ($model->save()) {
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                    'action' => 'data_updated',
                    'verify_field' => 'status',
                    'old_verify_status' => $old_verify_status,
                ]);

                $sequence = array_search($model->valuation_status, Yii::$app->appHelperFunctions->getStepsSequence(), true);

                if ($sequence == 1 && $model->email_status !== 1) {
                    $uid = $model->id;
                    if (isset($model->quotation_id) && $model->quotation_id <> null) {
                        $uid = 'crm' . $model->quotation_id;
                    }
                    //   UPDATE (table name, column values, condition)
                    Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 1], 'id=' . $model->id . '')->execute();
                    Yii::$app->db->createCommand()->update('valuation', ['email_status' => 1], 'id=' . $id . '')->execute();
                }

                if ($model->sms == 2 && $model->status_verified == 1 && $model->contact_phone_no != '') {
                    $str = ltrim($model->phone_code, '0');
                    $contact_number = $str . $model->contact_phone_no;
                    $contact_number = '971' . $contact_number;
                    if ($model->client->client_type == 'bank') {
                        $numbers = array();
                        $numbers[] = '971521547162';
                        // $numbers[] = '971528078852';
                        //  $numbers[] = '971522916145';
                        $numbers[] = $contact_number;
                        /* $numbers[] = '971556218786';
                         $numbers[] = '971528984794';
                         $numbers[] = '971521547162';
                         $numbers[] = '971524987212';
                         $numbers[] = '971524987164';*/
                        foreach ($numbers as $number) {
                            $text = "Valuation received from your financing bank.
Thank you.
Please advise your suitable inspection day/time on the phone number/email below.
Jeniza Lorenzo
Valuation Support Team
Windmills Valuation Services 
+971 52 154 7162
support@windmillsgroup.com
";

                            $numbers = "971521547162";

                            $ch = curl_init();

                            curl_setopt($ch, CURLOPT_URL, "https://restapi.smscountry.com/v0.1/Accounts/GSzAf5xi3QzofjOMtlkl/SMSes/");
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, FALSE);
                            curl_setopt($ch, CURLOPT_HEADER, FALSE);

                            curl_setopt($ch, CURLOPT_POST, TRUE);

                            curl_setopt($ch, CURLOPT_POSTFIELDS, "{
  \"Text\":\"$text\",
  \"Number\":\"$number\",
  \"SenderId\": \"WINDMILLS\",

  \"DRNotifyHttpMethod\": \"POST\",
  \"Tool\": \"API\"
}");

                            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                    "Content-Type: application/json",
                                    'Authorization: Basic ' . base64_encode("GSzAf5xi3QzofjOMtlkl:m82nK16BVJ67vFfulSxosIfhlgwEJgx2XYaBUPHm")

                                )
                            );

                            $response = curl_exec($ch);
                            Yii::$app->db->createCommand()->update('valuation', ['sms' => 1], 'id=' . $model->id . '')->execute();
                            /*echo "<pre>";
                            print_r($response);
                            die;*/
                            curl_close($ch);

                            //var_dump($response);
                        }
                    } else {
                        /*  echo $contact_number;
                          die;*/
                        $numbers = array();
                        $numbers[] = '971521547162';
                        //  $numbers[] = '971528078852';
                        //   $numbers[] = '971522916145';
                        $numbers[] = $contact_number;
                        /*   $numbers[] = '971556218786';
                           $numbers[] = '971528984794';
                           $numbers[] = '971521547162';
                           $numbers[] = '971524987212';
                           $numbers[] = '971524987164';*/
                        foreach ($numbers as $number) {
                            $text = "Valuation Received.
Thank you.
Please advise your suitable inspection day/time on the phone number/email below.
Jeniza Lorenzo
Valuation Support Team
Windmills Valuation Services
+971 52 154 7162
support@windmillsgroup.com
";

                            $numbers = "971521547162";

                            $ch = curl_init();

                            curl_setopt($ch, CURLOPT_URL, "https://restapi.smscountry.com/v0.1/Accounts/GSzAf5xi3QzofjOMtlkl/SMSes/");
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, FALSE);
                            curl_setopt($ch, CURLOPT_HEADER, FALSE);

                            curl_setopt($ch, CURLOPT_POST, TRUE);

                            curl_setopt($ch, CURLOPT_POSTFIELDS, "{
  \"Text\":\"$text\",
  \"Number\":\"$number\",
  \"SenderId\": \"WINDMILLS\",

  \"DRNotifyHttpMethod\": \"POST\",
  \"Tool\": \"API\"
}");

                            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                    "Content-Type: application/json",
                                    'Authorization: Basic ' . base64_encode("GSzAf5xi3QzofjOMtlkl:m82nK16BVJ67vFfulSxosIfhlgwEJgx2XYaBUPHm")

                                )
                            );

                            $response = curl_exec($ch);
                            Yii::$app->db->createCommand()->update('valuation', ['sms' => 1], 'id=' . $model->id . '')->execute();
                            /*echo "<pre>";
                            print_r($response);
                            die;*/
                            curl_close($ch);

                            //var_dump($response);
                        }
                    }
                }

                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_1/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        $viewpath = 'steps/_step1';
        if ($valuation_approach == 1) {
            $viewpath = 'steps_income/_step1';
        }
        if ($model->valuation_status == 5) {
            $viewpath = 'steps_locked/_step1';
        }

        return $this->render($viewpath, [
            'model' => $model,
        ]);
    }

    public function actionStep_101($id)
    {
        if (!Yii::$app->menuHelperFunction->checkActionAllowed('step_1')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }

        $valuation = $this->findModel($id);
        $model = ValuationDetail::find()->where(['valuation_id' => $id])->one();


        if ($model !== null) {
            $model->city = Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city];
            $model->community = $model->building->communities->title;
            $model->sub_community = $model->building->subCommunities->title;
            $city = Zone::find()
                ->select([
                    'country_id',
                ])
                ->where(['id' => $model->building->city])->asArray()->one();
            $country = Country::find()
                ->select([
                    'title',
                ])
                ->where(['id' => $city['country_id']])->asArray()->one();
            $model->country = $country['title'];
        } else {
            $model = new ValuationDetail();
        }


        // check value for valuation approach view change
        $valuation_approach = $valuation->valuation_approach;

        $old_verify_status = $model->status;
        $model->status_verified = $model->status;
        if ($model->load(Yii::$app->request->post())) {
            $model->valuation_id = $id;
            $model->status = $model->status_verified;
            $this->StatusVerify($model);
            // $model->step_number = 101;
            if ($model->save()) {
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => 'app\models\ValuationDetail',
                    'action' => 'data_updated',
                    'verify_field' => 'status',
                    'old_verify_status' => $old_verify_status,
                ]);


                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_101/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        $viewpath = 'steps/_step101';
        if ($valuation_approach == 1) {
            $viewpath = 'steps_income/_step101';
        }
        if ($valuation->valuation_status == 5) {
            $viewpath = 'steps_locked/_step1';
        }
        $model->title_deed = $valuation->title_deed;

        return $this->render($viewpath, [
            'model' => $model,
            'valuation' => $valuation,
        ]);
    }


    public function StatusVerify($model = null)
    {
        if ($model->status_verified == 1) {
            $model->status_verified_at = date('Y-m-d H:i:s');
            $model->status_verified_by = Yii::$app->user->identity->id;
        }
    }


    public function saveStatusHistory($data, $status_action)
    {
        if ($data) {
            $h_model = new \app\models\History;
            $h_model->model_id = $data['model']->id;
            $h_model->action = $status_action;
            $h_model->model_name = $data['model_name'];
            $h_model->created_at = date('Y-m-d H:i:s');
            $h_model->updated_at = date('Y-m-d H:i:s');
            $h_model->created_by = Yii::$app->user->id;
            $h_model->updated_by = Yii::$app->user->id;

            if (isset($data['file_type']) && $data['file_type'] <> null) {
                $h_model->file_type = $data['file_type'];
                $h_model->rec_type = 'masterfile';
            }
            $h_model->save();
        }
    }


    public function makeHistory($data)
    {
        $check_field = $data['verify_field'];

        if ($data) {

            $history = new \app\models\History;
            $history->model_id = $data['model']->id;
            $history->action = $data['action'];
            $history->model_name = $data['model_name'];
            $history->created_at = date('Y-m-d H:i:s');
            $history->updated_at = date('Y-m-d H:i:s');
            $history->created_by = Yii::$app->user->id;
            $history->updated_by = Yii::$app->user->id;

            if (isset($data['file_type']) && $data['file_type'] <> null) {
                $history->file_type = $data['file_type'];
                $history->rec_type = 'masterfile';
            }

            if ($history->save()) {
                // update status to 0 (zero) when data changed
                if ($data['action'] == 'data_updated') {
                    // $modelObject = new $data['model_name'];
                    // $tableName = $modelObject->tableName();
                    // Yii::$app->db->createCommand()
                    //   ->update($tableName,
                    //     [$data['verify_field'] => 0, 'status_verified_at'=>null, 'status_verified_by'=>null],
                    //     ['id' => $data['model']->id])
                    //   ->execute();
                }
            }


            if ($data['action'] == 'data_created') {

                if ($data['model']->$check_field == 1) {
                    $status_action = 'status_verified';
                    $this->saveStatusHistory($data, $status_action);
                }

            } else if ($data['action'] == 'data_updated') {

                // if(isset($data['old_verify_status']) && $data['old_verify_status'] <> null){
                //   if($data['old_verify_status'] != $data['model']->$check_field){

                if ($data['model']->$check_field == 1) {
                    $status_action = 'status_verified';
                    $this->saveStatusHistory($data, $status_action);
                } else {
                    $status_action = 'status_unverified';
                    $this->saveStatusHistory($data, $status_action);
                }

                //   }
                // }
            }


        }
    }


    public function actionStep_2($id)
    {
        if (!Yii::$app->menuHelperFunction->checkActionAllowed('step_2')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $valuation = $this->findModel($id);
        $model = ReceivedDocs::find()->where(['valuation_id' => $id])->one();


        if ($model !== null) {

        } else {
            $model = new ReceivedDocs();
        }
        $old_verify_status = $model->status;
        $model->status_verified = $model->status;
        if ($model->load(Yii::$app->request->post())) {
            $model->valuation_id = $id;
            if ($model->status_verified <> null) {
                $model->status = $model->status_verified;
            }
            $this->StatusVerify($model);
            if ($model->save()) {
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => 'app\models\ReceivedDocs',
                    'action' => 'data_updated',
                    'verify_field' => 'status',
                    'old_verify_status' => $old_verify_status,
                ]);

                $sequence = array_search($valuation->valuation_status, Yii::$app->appHelperFunctions->getStepsSequence(), true);

                if ($sequence == 1 && $model->email_status !== 1) {

                    // echo 'why is it in?';
                    // die();
                    $uid = $valuation->id;
                    if (isset($valuation->quotation_id) && $valuation->quotation_id <> null) {
                        $uid = 'crm' . $valuation->quotation_id;
                    }
                    $notifyData = [
                        'client' => $valuation->client,
                        'attachments' => [],
                        'subject' => $valuation->email_subject,
                        'uid' => $uid,
                        'valuer' => $valuation->approver->email,
                        'replacements' => [
                            '{clientName}' => $valuation->client->title,
                        ],
                    ];

                    // \app\modules\wisnotify\listners\NotifyEvent::fire1('Received.Doc', $notifyData);
                    // UPDATE (table name, column values, condition)
                    Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 7, 'updated_at' => date('Y-m-d H:i:s')], 'id=' . $model->valuation_id . '')->execute();
                    Yii::$app->db->createCommand()->update('received_docs', ['email_status' => 1], 'id=' . $model->id . '')->execute();
                }


                // $valuation->valuation_status = 7;
                // $valuation->save();
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_2/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }
        $viewpath = 'steps/_step2';
        if ($valuation->valuation_status == 5) {
            $viewpath = 'steps_locked/_step2';
        }
        return $this->render($viewpath, [
            'model' => $model,
            'valuation' => $valuation,
        ]);
    }

    public function actionStep_3($id)
    {
        if (!Yii::$app->menuHelperFunction->checkActionAllowed('step_3')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $valuation = $this->findModel($id);
        $model = ValuationConflict::find()->where(['valuation_id' => $id])->one();

        $client = Company::find()->where(['id' => $valuation->client_id])->one();

        //$related_to_buyer_check =  PreviousTransactions::find()->where(['client_customer_name' => $valuation->client_name_passport])->all();
        /*        $owners_in_valuation = ArrayHelper::map(\app\models\ValuationOwners::find()->where(['valuation_id' => $id])->all(), 'id', 'name');
                $owners_valutions_id = ArrayHelper::map(\app\models\ValuationOwners::find()->where(['name' => $owners_in_valuation])->all(), 'id', 'valuation_id');
                $owners_previous_data = PreviousTransactions::find()->where(['valuation_id' => $owners_valutions_id])->all();*/

        $related_to_buyer_check = PreviousTransactions::find()->where(['client_name' => $valuation->client_name_passport])->all();

        $owners_in_valuation = ArrayHelper::map(\app\models\ValuationOwners::find()->where(['valuation_id' => $id])->all(), 'id', 'name');
        $owners_valutions_names = ArrayHelper::map(\app\models\ValuationOwners::find()->where(['name' => $owners_in_valuation])->all(), 'id', 'name');


        $owners_previous_data = PreviousTransactions::find()->where(['client_name' => $owners_valutions_names])->all();

        $related_to_client_check = PreviousTransactions::find()->where(['client_name' => $client->title])->all();
        $related_to_property_check = PreviousTransactions::find()->where(['building_info' => $valuation->building_info, 'unit_number' => $valuation->unit_number])->all();


        if ($model !== null) {

        } else {
            $model = new ValuationConflict();
            if ($related_to_buyer_check <> null && count($related_to_buyer_check) > 0) {
                $model->related_to_buyer = 'Yes';
            }
            if ($owners_previous_data <> null && count($owners_previous_data) > 0) {
                $model->related_to_seller = 'Yes';
            }
            if ($related_to_client_check <> null && count($related_to_client_check) > 0) {
                $model->related_to_client = 'Yes';
            }
            if ($related_to_property_check <> null && count($related_to_property_check) > 0) {
                $model->related_to_property = 'Yes';
            }

        }

        if ($model->load(Yii::$app->request->post())) {
            $model->valuation_id = $id;
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_3/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error)
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                }
            }
        }
        $viewpath = 'steps/_step3';
        if ($valuation->valuation_status == 5) {
            $viewpath = 'steps_locked/_step3';
        }

        return $this->render($viewpath, [
            'model' => $model,
            'valuation' => $valuation,
            'buyer_data' => $related_to_buyer_check,
            'seller_data' => $owners_previous_data,
            'client_data' => $related_to_client_check,
            'property_data' => $related_to_property_check,
        ]);
    }

    public function actionStep_4($id)
    {


        /* $all_contacts = (new \yii\db\Query())
             ->select('contact_phone_no,valuation_id')
             ->from('schedule_inspection')
             ->where(['not', ['contact_phone_no' => null]])
             ->all();*/

        /*
                $string = "The price is $10.99 and the quantity is 20";
                preg_match_all('/\d+/', $string, $matches);
                print_r($matches[0]);
                die;*/


        /*

                $phone_number_array = array('50','52','54','55','56','58');
                $land_number_array = array('2','3','4','6','7','9');

                $all_contacts = (new \yii\db\Query())
                    ->select('contact_phone_no,valuation_id')
                    ->from('schedule_inspection')
                    ->where(['not', ['contact_phone_no' => null]])
                    ->all();

        foreach($all_contacts as $all_contact){
            $model = ScheduleInspection::find()->where(['valuation_id' => $all_contact['valuation_id']])->one();
            $str = $all_contact['contact_phone_no'];
            $withoutsevendigits = substr($str, 0, -7);
            $exact_seven_digits = substr($str, -7);
            $withoutsevendigits_digits_1 = substr($withoutsevendigits, -1);
            $withoutsevendigits_digits_2 = substr($withoutsevendigits, -2);
          //  $withoutsevendigits_digits_1 = substr($withoutsevendigits, 0, -1);
          //  $withoutsevendigits_digits_2 = substr($withoutsevendigits, 0, -2);



            if (in_array($withoutsevendigits_digits_2, $phone_number_array))
            {
                Yii::$app->db->createCommand()->update('schedule_inspection', ['phone_code' => '0'.$withoutsevendigits_digits_2], 'id='.$model->id.'')->execute();
                Yii::$app->db->createCommand()->update('schedule_inspection', ['contact_phone_no' => $exact_seven_digits], 'id='.$model->id.'')->execute();
                Yii::$app->db->createCommand()->update('schedule_inspection', ['contact_fine' => 1], 'id='.$model->id.'')->execute();
                Yii::$app->db->createCommand()->update('schedule_inspection', ['land_line_no' => ''], 'id=' . $model->id . '')->execute();
            }
            else if(in_array($withoutsevendigits_digits_1, $land_number_array)) {
                Yii::$app->db->createCommand()->update('schedule_inspection', ['land_line_code' => '0' . $withoutsevendigits_digits_1], 'id=' . $model->id . '')->execute();
                Yii::$app->db->createCommand()->update('schedule_inspection', ['land_line_no' => $exact_seven_digits], 'id=' . $model->id . '')->execute();
                Yii::$app->db->createCommand()->update('schedule_inspection', ['contact_fine' => 1], 'id=' . $model->id . '')->execute();
                Yii::$app->db->createCommand()->update('schedule_inspection', ['contact_phone_no' => ''], 'id=' . $model->id . '')->execute();
            }

        }
        die('hehe');*/

        if (!Yii::$app->menuHelperFunction->checkActionAllowed('step_4')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $valuation = $this->findModel($id);
        $model = ScheduleInspection::find()->where(['valuation_id' => $id])->one();
        $model_previous = ScheduleInspection::find()->where(['valuation_id' => $id])->one();

        if ($model !== null) {

        } else {
            $model = new ScheduleInspection();
        }

        // check value for valuation approach view change
        $valuation_approach = $valuation->valuation_approach;

        $getInspectionOfficer = ScheduleInspection::find()->where(['valuation_id' => $id])->one();
        if (isset($getInspectionOfficer->inspection_officer) && ($getInspectionOfficer->inspection_officer > 0)) {
            $inspectionOfficer = User::find()->where(['id' => $getInspectionOfficer->inspection_officer])->one();
            $inspectionEmail = $inspectionOfficer->email;
        } else {
            $inspectionEmail = '';
        }


        /* echo "<pre>";
         print_r($model->valuation->cancelReasons);
         die;*/
        $model_reason = new InspectionCancelReasonsLog();

        if ($model_reason->load(Yii::$app->request->post())) {
            if ($model_reason->reason_id == null) {

                Yii::$app->getSession()->addFlash('error', "Please select reason first!");
                return $this->redirect(['valuation/step_4/' . $id]);
            }
            $uid = $valuation->id;
            if (isset($valuation->quotation_id) && $valuation->quotation_id <> null) {
                $uid = 'crm' . $valuation->quotation_id;
            }


            $notifyData = [
                'client' => $valuation->client,
                'subject' => $valuation->email_subject,
                'attachments' => [],
                'uid' => $uid,
                'valuer' => $valuation->approver->email,
                'inspector' => $inspectionEmail,
                'replacements' => [
                    '{clientName}' => $valuation->client->title,
                    '{reason}' => $model_reason->reason->title,
                    '{inspectionType}' => Yii::$app->appHelperFunctions->inspectionTypeArr[$valuation->inspection_type],

                ],
            ];
            if ($valuation->inspection_type != 3) {

                \app\modules\wisnotify\listners\NotifyEvent::fire1('reason.send', $notifyData);

            }

            $model_reason->save();
            Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Email sent successfully!'));
            return $this->redirect(['valuation/step_4/' . $id]);
        }
        $old_verify_status = $model->status;
        $model->status_verified = $model->status;
        if ($model->load(Yii::$app->request->post())) {

            // for date format change
            $model->inspection_date = date('Y-m-d', strtotime($model->inspection_date));

            $model->valuation_id = $id;
            if ($model->status_verified <> null) {
                $model->status = $model->status_verified;
            }
            $this->StatusVerify($model);

            //if(($model->inspection_officer != $valuation->service_officer_name) && $model->transfer_reason =='' && $valuation->inspection_type != 3){
            if (($model->inspection_officer != $valuation->service_officer_name) && $model->transfer_reason == '' && $valuation->inspection_type != 3) {

                Yii::$app->getSession()->addFlash('error', "Please select Inspection Transfer reason first!");
                return $this->redirect(['valuation/step_4/' . $id]);
            }


            if ($model->save()) {
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => 'app\models\ScheduleInspection',
                    'action' => 'data_updated',
                    'verify_field' => 'status',
                    'old_verify_status' => $old_verify_status,
                ]);
                $again_email = 0;
                $sequence = array_search($valuation->valuation_status, Yii::$app->appHelperFunctions->getStepsSequence(), true);
                if ($model_previous->inspection_date != $model->inspection_date) {
                    $again_email = 1;
                }
                if ($model_previous->inspection_time != $model->inspection_time) {
                    $again_email = 1;
                }

                if (($again_email == 1) || (($sequence == 2 || $sequence == 1) && $model->email_status !== 1)) {


                    $InspectionTypeLabel = '';
                    $InspectionTypeLabelDate = '';
                    $InspectionTimeLabel = '';
                    $InspectionTime = '';
                    $InspectionOfficerLabel = '';
                    $InspectionOfficer = '';

                    $contactPersonNameLabel = '';
                    $contactPersonName = '';
                    $contactEmailLabel = '';
                    $contactEmail = '';
                    $contactPhoneNoLabel = '';
                    $contactPhoneNo = '';

                    if ($valuation->inspection_type == 1 || $valuation->inspection_type == 2) {
                        $InspectionTypeLabel = Yii::$app->appHelperFunctions->inspectionTypeArr[$valuation->inspection_type];
                        $InspectionTypeLabelDate = date('d-m-Y', strtotime($model->inspection_date));
                        $InspectionTimeLabel = $InspectionTypeLabel . ' Time:  ';
                        $InspectionTypeLabel .= ':';
                        $InspectionTime = $model->inspection_time;
                        $InspectionOfficerLabel = 'Inspection Officer:';
                        $InspectionOfficer = Yii::$app->appHelperFunctions->staffMemberListArr[$model->inspection_officer];
                    }
                    if ($valuation->inspection_type == 2) {
                        $contactPersonNameLabel = 'Contact Person Name:';
                        $contactPersonName = $model->contact_person_name;
                        $contactEmailLabel = 'Contact Email:';
                        $contactEmail = $model->contact_email;
                        $contactPhoneNoLabel = 'Contact PhoneNo:';
                        $contactPhoneNo = $model->contact_phone_no;
                    }


                    $uid = $valuation->id;
                    if (isset($valuation->quotation_id) && $valuation->quotation_id <> null) {
                        $uid = 'crm' . $valuation->quotation_id;
                    }
                    $notifyData = [
                        'client' => $valuation->client,
                        'subject' => $valuation->email_subject,
                        'attachments' => [],
                        'uid' => $uid,
                        'valuer' => $valuation->approver->email,
                        'inspector' => $inspectionEmail,
                        'replacements' => [
                            // '{clientName}'=>          $valuation->client->title,
                            // '{inspectionType}'=>      Yii::$app->appHelperFunctions->inspectionTypeArr[$valuation->inspection_type],
                            //  '{valuationData}'=>       $model->valuation_date,
                            // '{valuationReportDate}'=> $model->valuation_report_date,
                            //  '{label}'=>               $InspectionTypeLabel,
                            '{labelDate}' => $InspectionTypeLabelDate,
                            // '{inspectionTimeLabel}'=> $InspectionTimeLabel,
                            '{inspectionTime}' => $InspectionTime,
                            //  '{inspectionOfficerLabel}'=> $InspectionOfficerLabel,
                            //  '{inspectionOfficer}'=>   $InspectionOfficer,
                            //  '{contactPersonNameLable}'=>   $contactPersonNameLabel,
                            //  '{contactPersonName}'=>   $contactPersonName,
                            //  '{contactEmailLabel}'=>        $contactEmailLabel,
                            // '{contactEmail}'=>        $contactEmail,
                            //  '{contactPhoneNoLabel}'=>      $contactPhoneNoLabel,
                            //  '{contactPhoneNo}'=>      $contactPhoneNo,
                        ],
                    ];
                    if ($valuation->inspection_type != 3) {
                        //  $allow_properties = [1,2,5,6,12,4,20,24,28,37,39,17];
                        //  if (in_array($valuation->property_id, $allow_properties)) {
                        if ($valuation->parent_id > 0) {
                            if ($valuation->revised_reason == 4) {
                                \app\modules\wisnotify\listners\NotifyEvent::fire1('schedule.send', $notifyData);
                            }

                        } else {

                            \app\modules\wisnotify\listners\NotifyEvent::fire1('schedule.send', $notifyData);
                            $history = new ScheduleInspectionHistory();
                            $history->inspection_type = $valuation->inspection_type;
                            $history->inspection_date = $model->inspection_date;
                            $history->inspection_time = $model->inspection_time;
                            $history->valuation_id = $model->valuation_id;
                            $history->inspection_officer = $model->inspection_officer;
                            $history->save();


                        }
                        // }
                    }
                    Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 2], 'id=' . $model->valuation_id . '')->execute();
                    Yii::$app->db->createCommand()->update('schedule_inspection', ['email_status' => 1], 'id=' . $model->id . '')->execute();

                }

                if ($model->sms == 2 && $model->status_verified == 1 && $valuation->contact_phone_no != '') {
                    $str = ltrim($valuation->phone_code, '0');
                    $contact_number = $str . $valuation->contact_phone_no;
                    $contact_number = '971' . $contact_number;
                    if ($valuation->client->client_type == 'bank') {
                        $numbers = array();
                        $numbers[] = '971521547162';
                        // $numbers[] = '971528078852';
                        // $numbers[] = '971522916145';
                        $numbers[] = $contact_number;
                        /*   $numbers[] = '971556218786';
                           $numbers[] = '971528984794';
                           $numbers[] = '971521547162';
                           $numbers[] = '971524987212';
                           $numbers[] = '971524987164';*/
                        foreach ($numbers as $number) {
                            $text = "Inspection  Scheduled at " . $model->inspection_time . " on " . date('d-m-Y', strtotime($model->inspection_date)) . ".
Thank you.
Jeniza Lorenzo
Valuation Support Team
Windmills Valuation Services 
+971 52 154 7162
support@windmillsgroup.com
";

                            $numbers = "971521547162";

                            $ch = curl_init();

                            curl_setopt($ch, CURLOPT_URL, "https://restapi.smscountry.com/v0.1/Accounts/GSzAf5xi3QzofjOMtlkl/SMSes/");
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, FALSE);
                            curl_setopt($ch, CURLOPT_HEADER, FALSE);

                            curl_setopt($ch, CURLOPT_POST, TRUE);

                            curl_setopt($ch, CURLOPT_POSTFIELDS, "{
  \"Text\":\"$text\",
  \"Number\":\"$number\",
  \"SenderId\": \"WINDMILLS\",

  \"DRNotifyHttpMethod\": \"POST\",
  \"Tool\": \"API\"
}");

                            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                    "Content-Type: application/json",
                                    'Authorization: Basic ' . base64_encode("GSzAf5xi3QzofjOMtlkl:m82nK16BVJ67vFfulSxosIfhlgwEJgx2XYaBUPHm")

                                )
                            );

                            $response = curl_exec($ch);
                            Yii::$app->db->createCommand()->update('schedule_inspection', ['sms' => 1], 'id=' . $model->id . '')->execute();
                            /*echo "<pre>";
                            print_r($response);
                            die;*/
                            curl_close($ch);

                            //var_dump($response);
                        }
                    } else {
                        $numbers = array();
                        $numbers[] = '971521547162';
                        //  $numbers[] = '971528078852';
                        // $numbers[] = '971522916145';
                        $numbers[] = $contact_number;
                        /*  $numbers[] = '971556218786';
                          $numbers[] = '971528984794';
                          $numbers[] = '971521547162';
                          $numbers[] = '971524987212';
                          $numbers[] = '971524987164';*/
                        foreach ($numbers as $number) {
                            $text = "Inspection  Scheduled at " . $model->inspection_time . " on " . date('d-m-Y', strtotime($model->inspection_date)) . ".
Thank you.
Jeniza Lorenzo
Valuation Support Team
Windmills Valuation Services
+971 52 154 7162
support@windmillsgroup.com
";

                            $numbers = "971521547162";

                            $ch = curl_init();

                            curl_setopt($ch, CURLOPT_URL, "https://restapi.smscountry.com/v0.1/Accounts/GSzAf5xi3QzofjOMtlkl/SMSes/");
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, FALSE);
                            curl_setopt($ch, CURLOPT_HEADER, FALSE);

                            curl_setopt($ch, CURLOPT_POST, TRUE);

                            curl_setopt($ch, CURLOPT_POSTFIELDS, "{
  \"Text\":\"$text\",
  \"Number\":\"$number\",
  \"SenderId\": \"WINDMILLS\",

  \"DRNotifyHttpMethod\": \"POST\",
  \"Tool\": \"API\"
}");

                            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                    "Content-Type: application/json",
                                    'Authorization: Basic ' . base64_encode("GSzAf5xi3QzofjOMtlkl:m82nK16BVJ67vFfulSxosIfhlgwEJgx2XYaBUPHm")

                                )
                            );

                            $response = curl_exec($ch);
                            Yii::$app->db->createCommand()->update('schedule_inspection', ['sms' => 1], 'id=' . $model->id . '')->execute();
                            /*echo "<pre>";
                            print_r($response);
                            die;*/
                            curl_close($ch);

                            //var_dump($response);
                        }
                    }
                }
                // $valuation->valuation_status = 2;
                //$valuation->save();
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_4/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }
        $viewpath = 'steps/_step4';
        if ($valuation_approach == 1) {
            $viewpath = 'steps_income/_step4';
        }
        if ($valuation->valuation_status == 5) {
            $viewpath = 'steps_locked/_step4';
        }
        return $this->render($viewpath, [
            'model' => $model,
            'model_reason' => $model_reason,
            'valuation' => $valuation,
        ]);
    }

    public function actionStep_401($id)
    {

        /* if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_4')) {
             Yii::$app->getSession()->addFlash('error', "Permission denied!");
             return $this->redirect(['index']);
         }*/
        $valuation = $this->findModel($id);
        $model = ValuationDetailData::find()->where(['valuation_id' => $id])->one();
        $model_previous = ScheduleInspection::find()->where(['valuation_id' => $id])->one();

        if ($model !== null) {

        } else {
            $model = new ValuationDetailData();
            $model->valuation_date = $model_previous->inspection_date;
        }

        // check value for valuation approach view change
        $valuation_approach = $valuation->valuation_approach;

        $old_verify_status = $model->status;
        $model->status_verified = $model->status;
        if ($model->load(Yii::$app->request->post())) {

            // for date format change
            $model->valuation_date = date('Y-m-d', strtotime($model->valuation_date));

            $model->valuation_id = $id;
            if ($model->status_verified <> null) {
                $model->status = $model->status_verified;
            }
            $this->StatusVerify($model);


            if ($model->save()) {
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => 'app\models\ValuationDetailData',
                    'action' => 'data_updated',
                    'verify_field' => 'status',
                    'old_verify_status' => $old_verify_status,
                ]);
                $again_email = 0;

                // Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 2], 'id=' . $model->valuation_id . '')->execute();
                // Yii::$app->db->createCommand()->update('schedule_inspection', ['email_status' => 1], 'id=' . $model->id . '')->execute();

            }


            // $valuation->valuation_status = 2;
            //$valuation->save();
            Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
            return $this->redirect(['valuation/step_401/' . $id]);
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $viewpath = 'steps/_step401';
        if ($valuation_approach == 1) {
            $viewpath = 'steps_income/_step401';
        }
        if ($valuation->valuation_status == 5) {
            $viewpath = 'steps_locked/_step4';
        }
        return $this->render($viewpath, [
            'model' => $model,

            'valuation' => $valuation,
        ]);
    }

    public function actionStep_5($id)
    {

        if (!Yii::$app->menuHelperFunction->checkActionAllowed('step_5')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $valuation = $this->findModel($id);
        $total_towers = $valuation->no_of_towers;

        $model = InspectProperty::find()->where(['valuation_id' => $id])->one();
        $tower_data = ReTowersData::find()->where(['valuation_id' => $id])->all();


        // check value for valuation approach view change
        $valuation_approach = $valuation->valuation_approach;


        if ($tower_data !== null && !empty($tower_data)) {

        } else {
            $total_towers = $valuation->no_of_towers;

            for ($i = 0; $i <= $total_towers; $i++) {
                $tower = new ReTowersData();
                if ($i == $total_towers) {
                    $tower->title = '';
                } else {
                    $tower->title = 'T' . $i;
                }

                $tower->gfa = 0;
                $tower->bua = 0;
                $tower->floor_details = '';
                $tower->makani_number = '';
                $tower->unit_number = '';
                $tower->valuation_id = $id;
                $tower->save();
            }

        }

        if ($model !== null) {

        } else {

            $model = new InspectProperty();
            $model->makani_number = $valuation->building->makani_number;
            //$model->location = $valuation->building->location;

            $model->latitude = $valuation->building->latitude;
            $model->longitude = $valuation->building->longitude;
            $model->property_placement = $valuation->building->property_placement;
            $model->property_visibility = $valuation->building->property_visibility;
            $model->property_exposure = $valuation->building->property_exposure;
            $model->property_category = $valuation->building->property_category;
            $model->property_condition = $valuation->building->property_condition;
            $model->development_type = $valuation->building->development_type;
            $model->finished_status = $valuation->building->finished_status;
            $model->developer_id = $valuation->building->developer_id;
            $model->estimated_age = $valuation->building->estimated_age;
            $model->estimated_remaining_life = $valuation->property->age - $valuation->building->estimated_age;
            $model->number_of_basement = $valuation->building->number_of_basement;
            $model->pool = $valuation->building->pool;
            // $model->gym = $valuation->building->gym;
            // $model->play_area = $valuation->building->play_area;
            $model->other_facilities = $valuation->building->other_facilities;
            $model->completion_status = $valuation->building->completion_status;
            $model->landscaping = $valuation->building->landscaping;
            $model->white_goods = $valuation->building->white_goods;
            $model->furnished = $valuation->building->furnished;
            $model->utilities_connected = $valuation->building->utilities_connected;
            $model->location_highway_drive = $valuation->building->location_highway_drive;
            $model->location_school_drive = $valuation->building->location_school_drive;
            $model->location_mall_drive = $valuation->building->location_mall_drive;
            $model->location_sea_drive = $valuation->building->location_sea_drive;
            $model->location_park_drive = $valuation->building->location_park_drive;
        }
        $old_verify_status = $model->status;
        $model->status_verified = $model->status;

        if ($model->load(Yii::$app->request->post())) {

            $model->valuation_id = $id;
            if ($model->location_pin <> null) {
                $lat_array = explode(',', $model->location_pin);
                if (isset($lat_array[0])) {
                    $model->latitude = $lat_array[0];
                }
                if (isset($lat_array[1])) {
                    $model->longitude = $lat_array[1];
                }
            }


            if ($model->status_verified <> null) {
                $model->status = $model->status_verified;
            }
            $this->StatusVerify($model);
            if ($model->save()) {
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => 'app\models\InspectProperty',
                    'action' => 'data_updated',
                    'verify_field' => 'status',
                    'old_verify_status' => $old_verify_status,
                ]);
                $sequence = array_search($valuation->valuation_status, Yii::$app->appHelperFunctions->getStepsSequence(), true);

                if ($sequence == 3) {


                    // UPDATE (table name, column values, condition)
                    Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 3], 'id=' . $model->valuation_id . '')->execute();
                }
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_5/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }


            /*if ($model->save()) {

                if ($valuation->valuation_status!==3 && $model->email_status !== 1) {

                    $notifyData = [
                        'client' => $valuation->client,
                        'subject' => $valuation->email_subject,
                        'attachments' => [],
                        'replacements'=>[
                            '{clientName}'=>   $valuation->client->title,
                        ],
                    ];
                   // \app\modules\wisnotify\listners\NotifyEvent::fire1('Inspect.Property', $notifyData);
                    // UPDATE (table name, column values, condition)
                    Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 3], 'id='.$model->valuation_id.'')->execute();
                    Yii::$app->db->createCommand()->update('inspect_property', ['email_status' => 1], 'id='.$model->id .'')->execute();
                }
               // $valuation->valuation_status = 3;
               // $valuation->save();

                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_5/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }*/
        }
        $viewpath = 'steps/_step5';
        if ($valuation_approach == 1) {
            $viewpath = 'steps_income/_step5';
        }
        if ($valuation->valuation_status == 5) {
            $viewpath = 'steps_locked/_step5';
        }


        return $this->render($viewpath, [
            'model' => $model,
            'valuation' => $valuation,
            'tower_data' => $tower_data,
        ]);
    }

    public function actionStep_501($id)
    {

        /*  if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_4')) {
              Yii::$app->getSession()->addFlash('error', "Permission denied!");
              return $this->redirect(['index']);
          }*/
        $valuation = $this->findModel($id);
        $model = ValuationPublicData::find()->where(['valuation_id' => $id])->one();

        if ($model !== null) {

        } else {
            $model = new ValuationPublicData();
        }


        // check value for valuation approach view change
        $valuation_approach = $valuation->valuation_approach;

        $old_verify_status = $model->status;
        $model->status_verified = $model->status;
        if ($model->load(Yii::$app->request->post())) {
            $model->valuation_id = $id;
            if ($model->status_verified <> null) {
                $model->status = $model->status_verified;
            }
            $this->StatusVerify($model);


            if ($model->save()) {
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => 'app\models\ValuationPublicData',
                    'action' => 'data_updated',
                    'verify_field' => 'status',
                    'old_verify_status' => $old_verify_status,
                ]);

            }

            Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
            return $this->redirect(['valuation/step_501/' . $id]);
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $viewpath = 'steps/_step501';
        if ($valuation_approach == 1) {
            $viewpath = 'steps_income/_step501';
        }
        if ($valuation->valuation_status == 5) {
            $viewpath = 'steps_locked/_step4';
        }
        return $this->render($viewpath, [
            'model' => $model,

            'valuation' => $valuation,
        ]);
    }

    public function actionStep_502($id)
    {

        /*  if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_4')) {
              Yii::$app->getSession()->addFlash('error', "Permission denied!");
              return $this->redirect(['index']);
          }*/
        $valuation = $this->findModel($id);
        $model = GreenEffects::find()->where(['valuation_id' => $id])->one();

        if ($model !== null) {

        } else {
            $model = new GreenEffects();
        }


        $old_verify_status = $model->status;
        $model->status_verified = $model->status;
        if ($model->load(Yii::$app->request->post())) {
            $model->valuation_id = $id;
            if ($model->status_verified <> null) {
                $model->status = $model->status_verified;
            }
            $this->StatusVerify($model);


            if ($model->save()) {
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => 'app\models\GreenEffects',
                    'action' => 'data_updated',
                    'verify_field' => 'status',
                    'old_verify_status' => $old_verify_status,
                ]);

            }

            Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
            return $this->redirect(['valuation/step_502/' . $id]);
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $viewpath = 'steps/_step502';
        if ($valuation->valuation_status == 5) {
            $viewpath = 'steps_locked/_step5';
        }
        return $this->render($viewpath, [
            'model' => $model,

            'valuation' => $valuation,
        ]);
    }

    public function actionStep_503($id)
    {

        /*  if(!Yii::$app->menuHelperFunction->checkActionAllowed('step_4')) {
              Yii::$app->getSession()->addFlash('error', "Permission denied!");
              return $this->redirect(['index']);
          }*/
        $valuation = $this->findModel($id);
        $model = TransportData::find()->where(['valuation_id' => $id])->one();

        if ($model !== null) {

        } else {
            $model = new TransportData();
        }

        // check value for valuation approach view change
        $valuation_approach = $valuation->valuation_approach;


        $old_verify_status = $model->status;
        $model->status_verified = $model->status;
        if ($model->load(Yii::$app->request->post())) {
            $model->valuation_id = $id;
            if ($model->status_verified <> null) {
                $model->status = $model->status_verified;
            }
            $this->StatusVerify($model);


            if ($model->save()) {
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => 'app\models\TransportData',
                    'action' => 'data_updated',
                    'verify_field' => 'status',
                    'old_verify_status' => $old_verify_status,
                ]);

            }

            Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
            return $this->redirect(['valuation/step_503/' . $id]);
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $viewpath = 'steps/_step503';
        if ($valuation_approach == 1) {
            $viewpath = 'steps_income/_step503';
        }
        if ($valuation->valuation_status == 5) {
            $viewpath = 'steps_locked/_step5';
        }
        return $this->render($viewpath, [
            'model' => $model,

            'valuation' => $valuation,
        ]);
    }

    public function actionStep_5_0($id)
    {

        if (!Yii::$app->menuHelperFunction->checkActionAllowed('step_5')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $valuation = $this->findModel($id);
        $total_towers = $valuation->no_of_towers;

        $model = ReCostAssets::find()->where(['valuation_id' => $id])->one();
        $electrical_data_all = ReCostAssets::find()->where(['type_id' => 1, 'valuation_id' => $id])->all();
        $fire_system_data_all = ReCostAssets::find()->where(['type_id' => 2, 'valuation_id' => $id])->all();
        $heat_ventilation_data_all = ReCostAssets::find()->where(['type_id' => 3, 'valuation_id' => $id])->all();
        $mechanical_data_all = ReCostAssets::find()->where(['type_id' => 4, 'valuation_id' => $id])->all();
        $security_system_data_all = ReCostAssets::find()->where(['type_id' => 5, 'valuation_id' => $id])->all();
        $water_system_data_all = ReCostAssets::find()->where(['type_id' => 6, 'valuation_id' => $id])->all();


        if (isset($_POST) && $_POST <> null) {
            // $model->load(Yii::$app->request->post());
            $electrical_data = $_POST['ReCostAssets']['electrical'];
            $fire_system_data = $_POST['ReCostAssets']['fire_system'];
            $heat_ventilation_data = $_POST['ReCostAssets']['heat_ventilation'];
            $mechanical_data = $_POST['ReCostAssets']['mechanical'];
            $security_system_data = $_POST['ReCostAssets']['security_system'];
            $water_system_data = $_POST['ReCostAssets']['water_system'];

            if (isset($electrical_data) && ($electrical_data <> null)) {
                // ReCostAssets::deleteAll(['type_id' => 1]);
                // Save ALL banners Images
                foreach ($electrical_data as $attachment) {
                    if ($attachment['name'] != '' && $attachment['quantity'] != '') {
                        $model_saved = ReCostAssets::findOne($attachment['id']);
                        if ($model_saved !== null) {
                            $model_saved->name = $attachment['name'];
                            $model_saved->quantity = $attachment['quantity'];
                            $model_saved->type_id = 1;
                            $model_saved->valuation_id = $id;
                            $model_saved->save();
                        } else {
                            $customAttachments = new ReCostAssets();
                            $customAttachments->name = $attachment['name'];
                            $customAttachments->quantity = $attachment['quantity'];
                            $customAttachments->type_id = 1;
                            $customAttachments->valuation_id = $id;
                            $customAttachments->save();
                        }
                    }
                }
            }

            if (isset($fire_system_data) && ($fire_system_data <> null)) {
                // ReCostAssets::deleteAll(['type_id' => 1]);
                // Save ALL banners Images
                foreach ($fire_system_data as $attachment_1) {
                    if ($attachment_1['name'] != '' && $attachment_1['quantity'] != '') {
                        $model_saved = ReCostAssets::findOne($attachment_1['id']);
                        if ($model_saved !== null) {
                            $model_saved->name = $attachment_1['name'];
                            $model_saved->quantity = $attachment_1['quantity'];
                            $model_saved->type_id = 2;
                            $model_saved->valuation_id = $id;
                            $model_saved->save();
                        } else {
                            $customAttachments = new ReCostAssets();
                            $customAttachments->name = $attachment_1['name'];
                            $customAttachments->quantity = $attachment_1['quantity'];
                            $customAttachments->type_id = 2;
                            $customAttachments->valuation_id = $id;
                            $customAttachments->save();
                        }
                    }
                }
            }

            if (isset($heat_ventilation_data) && ($heat_ventilation_data <> null)) {
                // ReCostAssets::deleteAll(['type_id' => 1]);
                // Save ALL banners Images
                foreach ($heat_ventilation_data as $attachment_3) {
                    if ($attachment_3['name'] != '' && $attachment_3['quantity'] != '') {
                        $model_saved = ReCostAssets::findOne($attachment_3['id']);
                        if ($model_saved !== null) {
                            $model_saved->name = $attachment_3['name'];
                            $model_saved->quantity = $attachment_3['quantity'];
                            $model_saved->type_id = 3;
                            $model_saved->valuation_id = $id;
                            $model_saved->save();
                        } else {
                            $customAttachments = new ReCostAssets();
                            $customAttachments->name = $attachment_3['name'];
                            $customAttachments->quantity = $attachment_3['quantity'];
                            $customAttachments->type_id = 3;
                            $customAttachments->valuation_id = $id;
                            $customAttachments->save();
                        }
                    }
                }
            }

            if (isset($mechanical_data) && ($mechanical_data <> null)) {
                // ReCostAssets::deleteAll(['type_id' => 1]);
                // Save ALL banners Images
                foreach ($mechanical_data as $attachment_4) {
                    if ($attachment_4['name'] != '' && $attachment_4['quantity'] != '') {
                        $model_saved = ReCostAssets::findOne($attachment_4['id']);
                        if ($model_saved !== null) {
                            $model_saved->name = $attachment_4['name'];
                            $model_saved->quantity = $attachment_4['quantity'];
                            $model_saved->type_id = 4;
                            $model_saved->valuation_id = $id;
                            $model_saved->save();
                        } else {
                            $customAttachments = new ReCostAssets();
                            $customAttachments->name = $attachment_4['name'];
                            $customAttachments->quantity = $attachment_4['quantity'];
                            $customAttachments->type_id = 4;
                            $customAttachments->valuation_id = $id;
                            $customAttachments->save();
                        }
                    }
                }
            }

            if (isset($security_system_data) && ($security_system_data <> null)) {
                // ReCostAssets::deleteAll(['type_id' => 1]);
                // Save ALL banners Images
                foreach ($security_system_data as $attachment_5) {
                    if ($attachment_5['name'] != '' && $attachment_5['quantity'] != '') {
                        $model_saved = ReCostAssets::findOne($attachment_5['id']);
                        if ($model_saved !== null) {
                            $model_saved->name = $attachment_5['name'];
                            $model_saved->quantity = $attachment_5['quantity'];
                            $model_saved->type_id = 5;
                            $model_saved->valuation_id = $id;
                            $model_saved->save();
                        } else {
                            $customAttachments = new ReCostAssets();
                            $customAttachments->name = $attachment_5['name'];
                            $customAttachments->quantity = $attachment_5['quantity'];
                            $customAttachments->type_id = 5;
                            $customAttachments->valuation_id = $id;
                            $customAttachments->save();
                        }
                    }
                }
            }

            if (isset($water_system_data) && ($water_system_data <> null)) {
                // ReCostAssets::deleteAll(['type_id' => 1]);
                // Save ALL banners Images
                foreach ($water_system_data as $attachment_6) {
                    if ($attachment_6['name'] != '' && $attachment_6['quantity'] != '') {
                        $model_saved = ReCostAssets::findOne($attachment_6['id']);
                        if ($model_saved !== null) {
                            $model_saved->name = $attachment_6['name'];
                            $model_saved->quantity = $attachment_6['quantity'];
                            $model_saved->type_id = 6;
                            $model_saved->valuation_id = $id;
                            $model_saved->save();
                        } else {
                            $customAttachments = new ReCostAssets();
                            $customAttachments->name = $attachment_6['name'];
                            $customAttachments->quantity = $attachment_6['quantity'];
                            $customAttachments->type_id = 6;
                            $customAttachments->valuation_id = $id;
                            $customAttachments->save();
                        }
                    }
                }
            }

            Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
            return $this->redirect(['valuation/step_5_0/' . $id]);


            /* if ($model->save()) {
                 Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                 return $this->redirect(['valuation/step_5/' . $id]);
             } else {
                 if ($model->hasErrors()) {
                     foreach ($model->getErrors() as $error) {
                         if (count($error) > 0) {
                             foreach ($error as $key => $val) {
                                 Yii::$app->getSession()->addFlash('error', $val);
                             }
                         }
                     }
                 }
             }*/
        }
        $viewpath = 'steps/_step5_0';
        if ($valuation->valuation_status == 5) {
            $viewpath = 'steps_locked/_step5';
        }


        return $this->render($viewpath, [
            'model' => $model,
            'valuation' => $valuation,
            'electrical_data_all' => $electrical_data_all,
            'fire_system_data_all' => $fire_system_data_all,
            'heat_ventilation_data_all' => $heat_ventilation_data_all,
            'mechanical_data_all' => $mechanical_data_all,
            'security_system_data_all' => $security_system_data_all,
            'water_system_data_all' => $water_system_data_all,
        ]);
    }

    public function actionStep_6($id)
    {
        if (!Yii::$app->menuHelperFunction->checkActionAllowed('step_6')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $valuation = $this->findModel($id);
        $configuration = InspectProperty::find()->where(['valuation_id' => $id])->one();
        $model = ValuationConfiguration::find()->where(['valuation_id' => $id])->one();

        if ($model !== null) {

        } else {
            $model = new ValuationConfiguration();
        }

        // check value for valuation approach view change
        $valuation_approach = $valuation->valuation_approach;

        if ($model->load(Yii::$app->request->post())) {
            $model->valuation_id = $id;


            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_6/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        $viewpath = 'steps/_step6';
        if ($valuation_approach == 1) {
            $viewpath = 'steps_income/_step6';
        }
        if ($valuation->valuation_status == 5) {
            $viewpath = 'steps_locked/_step6';
        }

        return $this->render($viewpath, [
            'model' => $model,
            'valuation' => $valuation,
            'configuration' => $configuration
        ]);
    }

    public function actionStep_7($id)
    {
        if (!Yii::$app->menuHelperFunction->checkActionAllowed('step_7')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }

        $valuation = $this->findModel($id);
        $model = CostDetails::find()->where(['valuation_id' => $id])->one();
        if ($model !== null) {

        } else {
            $model = new CostDetails();
        }

        // check value for valuation approach view change
        $valuation_approach = $valuation->valuation_approach;

        if ($model->load(Yii::$app->request->post())) {
            $model->valuation_id = $id;
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_7/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }
        $viewpath = 'steps/_step7';
        if ($valuation_approach == 1) {
            $viewpath = 'steps_income/_step7';
        }
        if ($valuation->valuation_status == 5) {
            $viewpath = 'steps_locked/_step7';
        }
        return $this->render($viewpath, [
            'model' => $model,
            'valuation' => $valuation,
        ]);
    }

    public function actionClientProvidedRents($id)
    {
        if (!Yii::$app->menuHelperFunction->checkActionAllowed('client-provided-rents')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }

        $valuation = $this->findModel($id);
        $previous_records = \app\models\ClientProvidedRents::find()->where(['valuation_id' => $id])->all();
        $model = new \app\models\ClientProvidedRents();
        // echo "<pre>"; print_r($previous_records); echo "</pre>"; die;
        if (Yii::$app->request->post()) {
            $data = Yii::$app->request->post();
            // echo "<pre>"; print_r($data['ClientProvidedRents']['cpr']); echo "</pre>"; die;
            if ($data['ClientProvidedRents']['cpr'] <> null and is_array($data['ClientProvidedRents']['cpr'])) {
                \app\models\ClientProvidedRents::deleteAll(['valuation_id' => $id]);
                foreach ($data['ClientProvidedRents']['cpr'] as $key => $record) {
                    $model = new \app\models\ClientProvidedRents();
                    $model->valuation_id = $id;
                    $model->income_type = $record['income_type'];
                    $model->unit_number = $record['unit_number'];
                    $model->nla = $record['nla'];
                    $model->contract_date = $record['contract_date'];
                    $model->contract_end_date = $record['contract_end_date'];
                    $model->rent = $record['rent'];
                    $model->rent_sqf = $record['rent_sqf'];
                    $model->service_charges_unit = $record['service_charges_unit'];
                    $model->status = $record['status'];
                    if (!$model->save()) {
                        if ($model->hasErrors()) {
                            foreach ($model->getErrors() as $error) {
                                if (count($error) > 0) {
                                    foreach ($error as $key => $val) {
                                        Yii::$app->getSession()->addFlash('error', $val);
                                    }
                                }
                            }
                        }
                    }
                } //die;
            }
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->redirect(['valuation/client-provided-rents?id=' . $id]);

        }

        return $this->render('steps/client-provided-rents', [
            'valuation' => $valuation,
            'previous_records' => $previous_records,
            'model' => $model,
        ]);
    }

    public function actionStep_8($id, $action = null)
    {
        if (!Yii::$app->menuHelperFunction->checkActionAllowed('step_8')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $total_days = 0;
        $total_percentage = 0;
        $total_amount = 0;
        $total_present_value = 0;
        $d_m_percentage = 0;
        $valuation = $this->findModel($id);
        $model = ValuationDeveloperDriveMargin::find()->where(['valuation_id' => $id])->one();


        if ($model !== null) {

            if (isset($_POST['ValuationDeveloperDriveMargin']['frequency_of_payments']) && ($_POST['ValuationDeveloperDriveMargin']['frequency_of_payments'] != $model->frequency_of_payments)) {


                ValuationDmPayments::deleteAll(['valuation_id' => $id]);

            }
        } else {
            $model = new ValuationDeveloperDriveMargin();
        }
        $payments_table = ValuationDmPayments::find()->where(['valuation_id' => $id])->all();
        if (!empty($payments_table) && $payments_table <> null) {
            $show_payment_table = 1;
        } else {
            $show_payment_table = 0;
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->valuation_id = $id;
            // $model->frequency_of_payments = 4;


            if (!empty($payments_table) && $payments_table <> null) {


                $CostDetails = CostDetails::find()->where(['valuation_id' => $id])->one();
                $start_date = $model->payment_info[0]['installment_date'];

                if ($start_date <> null) {
                    foreach ($model->payment_info as $key => $payment) {

                        $date1 = date_create($start_date);
                        $date2 = date_create($payment['installment_date']);
                        $diff = date_diff($date1, $date2);
                        $model->payment_info[$key]['no_of_days'] = $diff->format("%a");


                        $percentage = $payment['percentage'];
                        $original_purchase_price = $CostDetails['original_purchase_price'];
                        $model->payment_info[$key]['amount'] = round((($percentage / 100) * $original_purchase_price), 2);
                        $total_rate = $model->funding_cost + $model->admin_charges;
                        if ($key > 0) {
                            $sqrt = $model->payment_info[$key]['no_of_days'] / 365;
                            $sqrt_interest = pow((1 + ($total_rate / 100)), $sqrt);
                            $model->payment_info[$key]['present_value'] = round(($model->payment_info[$key]['amount'] / $sqrt_interest), 2);
                        } else {
                            $model->payment_info[$key]['present_value'] = $model->payment_info[$key]['amount'];
                        }
                        $model->payment_info[$key]['index_id'] = $key;
                        $model->payment_info[$key]['valuation_id'] = $id;

                        $total_amount = $total_amount + $model->payment_info[$key]['amount'];
                        $total_present_value = $total_present_value + $model->payment_info[$key]['present_value'];


                    }

                }
                if ($total_amount > 0) {
                    $model->developer_margin = $total_amount - $total_present_value;
                    $model->developer_margin = round($model->developer_margin);
                    $model->developer_margin_percentage = round((($model->developer_margin / $total_amount) * 100), 2);
                    $model->cash_equivalent_price = $total_amount - $model->developer_margin;
                    $model->estimated_price_per_valuation = round($model->cash_equivalent_price * (1 - ($model->decline_in_prices / 100)));
                }

            } else {
                $payments_table = array();

                for ($i = 0; $i < $model->frequency_of_payments; $i++) {
                    $sub_payment = array();
                    if ($i == 0) {
                        $sub_payment['installment_date'] = $model->initial_deposit_contract_date;
                    } else if (($i + 1) == $model->frequency_of_payments) {
                        $sub_payment['installment_date'] = $model->last_payment_date;
                    } else {
                        $sub_payment['installment_date'] = '';
                    }
                    $sub_payment['no_of_days'] = 0;
                    $sub_payment['percentage'] = '';
                    $sub_payment['amount'] = 0;
                    $sub_payment['present_value'] = 0;
                    $sub_payment['index_id'] = $i;
                    $sub_payment['valuation_id'] = $id;
                    $payments_table[] = $sub_payment;
                }
                $model->payment_info = $payments_table;
            }

            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_8/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }

        }
        $viewpath = 'steps/_step8';
        if ($valuation->valuation_status == 5) {
            $viewpath = 'steps_locked/_step8';
        }
        return $this->render($viewpath, [
            'model' => $model,
            'valuation' => $valuation,
            'show_payment_table' => $show_payment_table,
            'payments_table' => $payments_table,
        ]);
    }


    public function actionStep_9($id)
    {
        if (!Yii::$app->menuHelperFunction->checkActionAllowed('step_9')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $post_data = Yii::$app->request->post();
            Yii::$app->db->createCommand()->update('valuation', ['special_assumption' => $post_data['Valuation']['special_assumption']], ['id' => $id])->execute();
            Yii::$app->db->createCommand()->update('valuation', ['general_assumption' => $post_data['Valuation']['general_assumption']], ['id' => $id])->execute();


            Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
            return $this->redirect(['valuation/step_9/' . $id]);

        }
        $viewpath = 'steps/_step9';
        if ($model->valuation_status == 5) {
            $viewpath = 'steps_locked/_step9';
        }
        return $this->render($viewpath, [
            'model' => $model,
            'valuation' => $model,
        ]);
    }

    /* Start sold calculations*/
    public function actionStep_10_07($id)
    {
        if (!Yii::$app->menuHelperFunction->checkActionAllowed('step_10')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }

        $valuation = $this->findModel($id);
        $model = ValuationEnquiry::find()->where(['valuation_id' => $id, 'type' => 'sold'])->one();
        if ($model !== null) {

        } else {
            $model = new ValuationEnquiry();
            $model->date_from = '2020-01-01';
            $model->date_to = date('Y-m-d');
            $model->building_info = $valuation->building_info;
            $model->bedroom_from = $valuation->inspectProperty->no_of_bedrooms;
            $model->bedroom_to = $valuation->inspectProperty->no_of_bedrooms;
        }

        if ($model->load(Yii::$app->request->post())) {
            // ValuationEnquiry::deleteAll(['valuation_id' => $id,'type'=> 'sold']);
            $model->valuation_id = $id;
            $model->type = 'sold';
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_10/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('steps/_step10', [
            'model' => $model,
            'valuation' => $valuation,
        ]);
    }


    public function actionStep_10_sold($id)
    {
        if (!Yii::$app->menuHelperFunction->checkActionAllowed('step_10')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $InspectProperty = \app\models\InspectProperty::find()->where(['valuation_id' => $id])->one();
        $current_date = date('Y-m-d');
        $current_date_1_year = date('Y-m-d', strtotime($current_date . ' -1 year'));

        $autolisting_count = 0;
        $mode = 0;

        $valuation = $this->findModel($id);
        $model = ValuationEnquiry::find()->where(['valuation_id' => $id, 'type' => 'sold'])->one();
        if ($model !== null) {
            $model->date_from = $current_date_1_year;

        } else {
            //  $model = new ValuationEnquiry();
            $model = new ValuationEnquiry();
            $model->date_from = $current_date_1_year;
            $model->new_from_date = $current_date_1_year;
            $model->date_to = $current_date;
            $model->building_info = $valuation->building_info;
            $model->valuation_id = $id;
            $model->bedroom_from = $InspectProperty->no_of_bedrooms;
            $model->bedroom_to = $InspectProperty->no_of_bedrooms;
            $model->type = 'sold';
            $model->save();
        }


        //step 1 check no of records
        $autolists = \app\models\SoldTransaction::find()
            ->where(['building_info' => $valuation->building_info])
            ->andFilterWhere(['between', 'transaction_date', $model->date_from, $model->date_to])
            ->all();

        if (!empty($autolists) && $autolists <> null) {
            $autolisting_count = count($autolists);
        }
        echo $autolisting_count;


        $autoListings = AutoListings::find()->where(['data_type' => 'sold'])->one();


        //Decide Mode
        //    if(((int)$autolisting_count >= (int)$autoListings->relax_records_limit_from ) && ((int)$autolisting_count <= (int)$autolisting_count->relax_records_limit_to)){
        if (((int)$autolisting_count >= (int)$autoListings->relax_records_limit_from) && ((int)$autolisting_count <= (int)$autoListings->relax_records_limit_to)) {
            /* echo 'Relax Mode';
             die;*/
            $bua_avg = (new \yii\db\Query())
                ->select('AVG(built_up_area) as built_up_avg')
                ->from('sold_transaction')
                ->where(['building_info' => $valuation->building_info])
                ->andFilterWhere(['between', 'transaction_date', $model->date_from, $model->date_to])
                ->one();

            //calculate bua percentage criterias
            $criteria_percenatge = ($autoListings->relax_bua_percentage / 100) * $InspectProperty->built_up_area;
            $bua_from = $InspectProperty->built_up_area - $criteria_percenatge;
            $bua_to = $InspectProperty->built_up_area + $criteria_percenatge;


            //Applying First Filter
            $autolists_1 = \app\models\SoldTransaction::find()
                ->where(['building_info' => $valuation->building_info])
                ->andWhere(['>=', 'built_up_area', $bua_from])
                ->andWhere(['<=', 'built_up_area', $bua_to])
                ->andFilterWhere(['between', 'transaction_date', $model->date_from, $model->date_to])
                ->all();

            $autolisting_1_count = count($autolists_1);


            //calculate price/sft percentage criterias
            $price_per_sqt_avg = (new \yii\db\Query())
                ->select('AVG(price_per_sqt) as price_per_sqt_avg')
                ->from('sold_transaction')
                ->where(['building_info' => $valuation->building_info])
                ->andWhere(['>=', 'built_up_area', $bua_from])
                ->andWhere(['<=', 'built_up_area', $bua_to])
                ->andFilterWhere(['between', 'transaction_date', $model->date_from, $model->date_to])
                ->one();

            //calculate price/sft percentage criterias
            $criteria_percenatge = ($autoListings->relax_price_percentage / 100) * $price_per_sqt_avg['price_per_sqt_avg'];
            $price_from = $price_per_sqt_avg['price_per_sqt_avg'] - $criteria_percenatge;
            $price_to = $price_per_sqt_avg['price_per_sqt_avg'] + $criteria_percenatge;


            //Applying Second Filter
            $autolists_2 = \app\models\SoldTransaction::find()
                ->where(['building_info' => $valuation->building_info])
                ->andWhere(['>=', 'built_up_area', $bua_from])
                ->andWhere(['<=', 'built_up_area', $bua_to])
                ->andWhere(['>=', 'price_per_sqt', $price_from])
                ->andWhere(['<=', 'price_per_sqt', $price_to])
                ->andFilterWhere(['between', 'transaction_date', $model->date_from, $model->date_to])
                ->all();

            $autolisting_2_count = count($autolists_2);


            $autolists_2_bedroom = \app\models\SoldTransaction::find()
                ->where(['building_info' => $valuation->building_info])
                ->andWhere(['>=', 'built_up_area', $bua_from])
                ->andWhere(['<=', 'built_up_area', $bua_to])
                ->andWhere(['>=', 'price_per_sqt', $price_from])
                ->andWhere(['<=', 'price_per_sqt', $price_to])
                // ->andWhere(['no_of_bedrooms', $model->no_of_bedrooms])
                ->andFilterWhere(['between', 'no_of_bedrooms', $model->bedroom_from, $model->bedroom_to])
                ->andFilterWhere(['between', 'transaction_date', $model->date_from, $model->date_to])
                ->all();

            $autolists_2_bedroom = count($autolists_2_bedroom);

            //Applying Third Filter


            $from_date = date('Y-m-d', strtotime($current_date . ' -' . $autoListings->relax_date . ' months'));


            $autolists_3 = \app\models\SoldTransaction::find()
                ->where(['building_info' => $valuation->building_info])
                ->andWhere(['>=', 'built_up_area', $bua_from])
                ->andWhere(['<=', 'built_up_area', $bua_to])
                ->andWhere(['>=', 'price_per_sqt', $price_from])
                ->andWhere(['<=', 'price_per_sqt', $price_to])
                ->andFilterWhere(['between', 'no_of_bedrooms', $model->bedroom_from, $model->bedroom_to])
                ->andFilterWhere(['between', 'transaction_date', $from_date, $model->date_to])
                ->all();


            $autolisting_3_count = count($autolists_3);


            if ($autolisting_3_count >= $autoListings->search_limit) {
                //updating from date
                Yii::$app->db->createCommand()->update('valuation_enquiry', ['date_from' => $from_date], ['valuation_id' => $id, 'type' => 'list'])->execute();

                Yii::$app->db->createCommand()->update('valuation_enquiry', ['new_from_date' => $from_date], ['valuation_id' => $id, 'type' => 'list'])->execute();
                Yii::$app->db->createCommand()->update('valuation_enquiry', ['mode' => 1], ['valuation_id' => $id, 'type' => 'list'])->execute();
                $model = ValuationEnquiry::find()->where(['valuation_id' => $id, 'type' => 'sold'])->one();


                //updating selected list
                if (!empty($autolists_3) && $autolists_3 <> null) {
                    ValuationSelectedLists::deleteAll(['valuation_id' => $id, 'type' => 'sold']);
                    foreach ($autolists_3 as $selected_row) {
                        $selected_data_detail = new ValuationSelectedLists();
                        $selected_data_detail->selected_id = $selected_row->id;
                        $selected_data_detail->type = 'sold';
                        $selected_data_detail->valuation_id = $id;
                        $selected_data_detail->save();
                    }
                }

                $selected_data = ArrayHelper::map(\app\models\ValuationSelectedLists::find()
                    ->where(['valuation_id' => $id, 'type' => 'sold'])
                    ->limit(10)
                    ->orderBy(['latest' => SORT_DESC])
                    ->all(), 'id', 'selected_id');

                $totalResults = (new \yii\db\Query())
                    ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) avg_listings_price_size, MIN(listings_price) as min_price, MIN(price_per_sqt) as min_price_sqt, MAX(listings_price) as max_price, MAX(price_per_sqt) as max_price_sqt , FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
                    ->from('sold_transaction')
                    ->where(['id' => $selected_data])
                    ->all();


                //Average and calculations

                if (!empty($totalResults) && $totalResults <> null) {
                    if (isset($totalResults[0]['avg_bedrooms'])) {
                        ValuationListCalculation::deleteAll(['valuation_id' => $id, 'type' => 'sold']);
                        $calculations = new ValuationListCalculation();
                        $calculations->avg_bedrooms = round($totalResults[0]['avg_bedrooms']);
                        $calculations->avg_land_size = round($totalResults[0]['avg_land_size']);
                        $calculations->built_up_area_size = round($totalResults[0]['built_up_area_size']);
                        $calculations->avg_listings_price_size = round($totalResults[0]['avg_listings_price_size']);
                        $calculations->min_price = round($totalResults[0]['min_price']);
                        $calculations->max_price = round($totalResults[0]['max_price']);
                        $calculations->min_price_sqt = round($totalResults[0]['min_price_sqt']);
                        $calculations->max_price_sqt = round($totalResults[0]['max_price_sqt']);
                        $calculations->avg_listing_date = date('Y-m-d', strtotime($totalResults[0]['avg_listing_date']));
                        $calculations->avg_psf = round($calculations->avg_listings_price_size / $calculations->built_up_area_size);
                        $calculations->avg_gross_yield = 0;
                        $calculations->type = 'sold';
                        $calculations->valuation_id = $id;
                        $calculations->save();
                    }

                }
                $mode = 1;
            }


        } else if ($autolisting_count >= $autoListings->moderate_records_limit_from && $autolisting_count <= $autoListings->moderate_records_limit_to) {
            /*  echo 'Moderate Mode';
               die;*/
            $bua_avg = (new \yii\db\Query())
                ->select('AVG(built_up_area) as built_up_avg')
                ->from('sold_transaction')
                ->where(['building_info' => $valuation->building_info])
                ->andFilterWhere(['between', 'transaction_date', $model->date_from, $model->date_to])
                ->one();

            //calculate bua percentage criterias
            $criteria_percenatge = ($autoListings->moderate_bua_percentage / 100) * $InspectProperty->built_up_area;
            $bua_from = $InspectProperty->built_up_area - $criteria_percenatge;
            $bua_to = $InspectProperty->built_up_area + $criteria_percenatge;


            //Applying First Filter
            $autolists_1 = \app\models\ListingsTransactions::find()
                ->where(['building_info' => $valuation->building_info])
                ->andWhere(['>=', 'built_up_area', $bua_from])
                ->andWhere(['<=', 'built_up_area', $bua_to])
                ->andFilterWhere(['between', 'transaction_date', $model->date_from, $model->date_to])
                ->all();

            $autolisting_1_count = count($autolists_1);


            //calculate price/sft percentage criterias
            $price_per_sqt_avg = (new \yii\db\Query())
                ->select('AVG(price_per_sqt) as price_per_sqt_avg')
                ->from('sold_transaction')
                ->where(['building_info' => $valuation->building_info])
                ->andWhere(['>=', 'built_up_area', $bua_from])
                ->andWhere(['<=', 'built_up_area', $bua_to])
                ->andFilterWhere(['between', 'transaction_date', $model->date_from, $model->date_to])
                ->one();

            //calculate price/sft percentage criterias
            $criteria_percenatge = ($autoListings->moderate_price_percentage / 100) * $price_per_sqt_avg['price_per_sqt_avg'];
            $price_from = $price_per_sqt_avg['price_per_sqt_avg'] - $criteria_percenatge;
            $price_to = $price_per_sqt_avg['price_per_sqt_avg'] + $criteria_percenatge;


            //Applying Second Filter
            $autolists_2 = \app\models\SoldTransaction::find()
                ->where(['building_info' => $valuation->building_info])
                ->andWhere(['>=', 'built_up_area', $bua_from])
                ->andWhere(['<=', 'built_up_area', $bua_to])
                ->andWhere(['>=', 'price_per_sqt', $price_from])
                ->andWhere(['<=', 'price_per_sqt', $price_to])
                ->andFilterWhere(['between', 'transaction_date', $model->date_from, $model->date_to])
                ->all();

            $autolisting_2_count = count($autolists_2);

            $autolists_2_bedroom = \app\models\SoldTransaction::find()
                ->where(['building_info' => $valuation->building_info])
                ->andWhere(['>=', 'built_up_area', $bua_from])
                ->andWhere(['<=', 'built_up_area', $bua_to])
                ->andWhere(['>=', 'price_per_sqt', $price_from])
                ->andWhere(['<=', 'price_per_sqt', $price_to])
                // ->andWhere(['no_of_bedrooms', $model->no_of_bedrooms])
                ->andFilterWhere(['between', 'no_of_bedrooms', $model->bedroom_from, $model->bedroom_to])
                ->andFilterWhere(['between', 'transaction_date', $model->date_from, $model->date_to])
                ->all();

            $autolists_2_bedroom = count($autolists_2_bedroom);


            //Applying Third Filter

            for ($i = $autoListings->moderate_date; $i <= 6; $i++) {

                $from_date = date('Y-m-d', strtotime($current_date . ' -' . $i . ' month'));


                $autolists_3 = \app\models\SoldTransaction::find()
                    ->where(['building_info' => $valuation->building_info])
                    ->andWhere(['>=', 'built_up_area', $bua_from])
                    ->andWhere(['<=', 'built_up_area', $bua_to])
                    ->andWhere(['>=', 'price_per_sqt', $price_from])
                    ->andWhere(['<=', 'price_per_sqt', $price_to])
                    ->andFilterWhere(['between', 'no_of_bedrooms', $model->bedroom_from, $model->bedroom_to])
                    ->andFilterWhere(['between', 'transaction_date', $from_date, $model->date_to])
                    ->all();

                $autolisting_3_count = count($autolists_3);
                if ($autolisting_3_count >= $autoListings->search_limit) {
                    //updating from date
                    Yii::$app->db->createCommand()->update('valuation_enquiry', ['date_from' => $from_date], ['valuation_id' => $id, 'type' => 'sold'])->execute();
                    Yii::$app->db->createCommand()->update('valuation_enquiry', ['new_from_date' => $from_date], ['valuation_id' => $id, 'type' => 'sold'])->execute();
                    Yii::$app->db->createCommand()->update('valuation_enquiry', ['mode' => 2], ['valuation_id' => $id, 'type' => 'sold'])->execute();
                    $model = ValuationEnquiry::find()->where(['valuation_id' => $id, 'type' => 'sold'])->one();


                    //updating selected list
                    if (!empty($autolists_3) && $autolists_3 <> null) {
                        ValuationSelectedLists::deleteAll(['valuation_id' => $id, 'type' => 'sold']);
                        foreach ($autolists_3 as $selected_row) {
                            $selected_data_detail = new ValuationSelectedLists();
                            $selected_data_detail->selected_id = $selected_row->id;
                            $selected_data_detail->type = 'sold';
                            $selected_data_detail->valuation_id = $id;
                            $selected_data_detail->save();
                        }

                    }


                    $selected_data = ArrayHelper::map(\app\models\ValuationSelectedLists::find()
                        ->where(['valuation_id' => $id, 'type' => 'sold'])
                        ->limit(10)
                        ->orderBy(['latest' => SORT_DESC])
                        ->all(), 'id', 'selected_id');

                    $totalResults = (new \yii\db\Query())
                        ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) avg_listings_price_size, MIN(listings_price) as min_price, MIN(price_per_sqt) as min_price_sqt, MAX(listings_price) as max_price, MAX(price_per_sqt) as max_price_sqt , FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
                        ->from('sold_transaction')
                        ->where(['id' => $selected_data])
                        ->all();


                    //Average and calculations

                    if (!empty($totalResults) && $totalResults <> null) {
                        if (isset($totalResults[0]['avg_bedrooms'])) {
                            ValuationListCalculation::deleteAll(['valuation_id' => $id, 'type' => 'sold']);
                            $calculations = new ValuationListCalculation();
                            $calculations->avg_bedrooms = round($totalResults[0]['avg_bedrooms']);
                            $calculations->avg_land_size = round($totalResults[0]['avg_land_size']);
                            $calculations->built_up_area_size = round($totalResults[0]['built_up_area_size']);
                            $calculations->avg_listings_price_size = round($totalResults[0]['avg_listings_price_size']);
                            $calculations->min_price = round($totalResults[0]['min_price']);
                            $calculations->max_price = round($totalResults[0]['max_price']);
                            $calculations->min_price_sqt = round($totalResults[0]['min_price_sqt']);
                            $calculations->max_price_sqt = round($totalResults[0]['max_price_sqt']);
                            $calculations->avg_listing_date = date('Y-m-d', strtotime($totalResults[0]['avg_listing_date']));
                            $calculations->avg_psf = round($calculations->avg_listings_price_size / $calculations->built_up_area_size);
                            $calculations->avg_gross_yield = 0;
                            $calculations->type = 'sold';
                            $calculations->valuation_id = $id;
                            $calculations->save();
                        }

                    }

                    $mode = 2;
                    break;
                }

            }


        } else if ($autolisting_count > $autoListings->strict_records_limit_from) {

            //   echo "total".$autolisting_count.'<br><br>';


            $bua_avg = (new \yii\db\Query())
                ->select('AVG(built_up_area) as built_up_avg')
                ->from('sold_transaction')
                ->where(['building_info' => $valuation->building_info])
                ->andFilterWhere(['between', 'transaction_date', $model->date_from, $model->date_to])
                ->one();


            //calculate bua percentage criterias
            $criteria_percenatge = ($autoListings->strict_bua_percentage / 100) * $InspectProperty->built_up_area;
            $bua_from = $InspectProperty->built_up_area - $criteria_percenatge;
            $bua_to = $InspectProperty->built_up_area + $criteria_percenatge;

            //             echo 'BUA Filter'.'<br>';
//             echo $InspectProperty->built_up_area .'<br>';
//             echo $bua_from.'<br>';
//             echo $bua_to.'<br>';
            // die;


            //Applying First Filter
            $autolists_1 = \app\models\SoldTransaction::find()
                ->where(['building_info' => $valuation->building_info])
                ->andWhere(['>=', 'built_up_area', $bua_from])
                ->andWhere(['<=', 'built_up_area', $bua_to])
                ->andFilterWhere(['between', 'transaction_date', $model->date_from, $model->date_to])
                ->all();

            $autolisting_1_count = count($autolists_1);

            //   echo 'Total After Bua filter:'.$autolisting_1_count.'<br><br>';
            //die;


            //calculate price/sft percentage criterias
            $price_per_sqt_avg = (new \yii\db\Query())
                ->select('AVG(price_per_sqt) as price_per_sqt_avg')
                ->from('sold_transaction')
                ->where(['building_info' => $valuation->building_info])
                ->andWhere(['>=', 'built_up_area', $bua_from])
                ->andWhere(['<=', 'built_up_area', $bua_to])
                ->andFilterWhere(['between', 'transaction_date', $model->date_from, $model->date_to])
                ->one();

            //calculate price/sft percentage criterias
            $criteria_percenatge = ($autoListings->strict_price_percentage / 100) * $price_per_sqt_avg['price_per_sqt_avg'];
            $price_from = $price_per_sqt_avg['price_per_sqt_avg'] - $criteria_percenatge;
            $price_to = $price_per_sqt_avg['price_per_sqt_avg'] + $criteria_percenatge;

            /*   echo 'price/sqt Filter'.'<br>';
               echo $price_per_sqt_avg['price_per_sqt_avg'] .'<br>';
               echo $price_from.'<br>';
               echo $price_to.'<br>';*/
            // die;

            //Applying Second Filter
            $autolists_2 = \app\models\SoldTransaction::find()
                ->where(['building_info' => $valuation->building_info])
                ->andWhere(['>=', 'built_up_area', $bua_from])
                ->andWhere(['<=', 'built_up_area', $bua_to])
                ->andWhere(['>=', 'price_per_sqt', $price_from])
                ->andWhere(['<=', 'price_per_sqt', $price_to])
                ->andFilterWhere(['between', 'transaction_date', $model->date_from, $model->date_to])
                ->all();

            $autolisting_2_count = count($autolists_2);

            //  echo $autolisting_2_count;
            //   echo 'Total After Price filter :'.$autolisting_2_count.'<br><br>';
            // die;

            $autolists_2_bedroom = \app\models\SoldTransaction::find()
                ->where(['building_info' => $valuation->building_info])
                ->andWhere(['>=', 'built_up_area', $bua_from])
                ->andWhere(['<=', 'built_up_area', $bua_to])
                ->andWhere(['>=', 'price_per_sqt', $price_from])
                ->andWhere(['<=', 'price_per_sqt', $price_to])
                // ->andWhere(['no_of_bedrooms', $model->no_of_bedrooms])
                ->andFilterWhere(['between', 'no_of_bedrooms', $model->bedroom_from, $model->bedroom_to])
                ->andFilterWhere(['between', 'transaction_date', $model->date_from, $model->date_to])
                ->all();

            $autolists_2_bedroom = count($autolists_2_bedroom);

            //Applying Third Filter

            for ($i = $autoListings->strict_date; $i <= 6; $i++) {

                $from_date = date('Y-m-d', strtotime($current_date . ' -' . $i . ' month'));


                $autolists_3 = \app\models\SoldTransaction::find()
                    ->where(['building_info' => $valuation->building_info])
                    ->andWhere(['>=', 'built_up_area', $bua_from])
                    ->andWhere(['<=', 'built_up_area', $bua_to])
                    ->andWhere(['>=', 'price_per_sqt', $price_from])
                    ->andWhere(['<=', 'price_per_sqt', $price_to])
                    ->andFilterWhere(['between', 'no_of_bedrooms', $model->bedroom_from, $model->bedroom_to])
                    ->andFilterWhere(['between', 'transaction_date', $from_date, $model->date_to])
                    ->all();

                $autolisting_3_count = count($autolists_3);
                if ($autolisting_3_count >= 10) {
                    /* echo "Month ".$i." count = ".$autolisting_3_count.'<br>';
                  die;*/
                    //updating from date
                    Yii::$app->db->createCommand()->update('valuation_enquiry', ['date_from' => $from_date], ['valuation_id' => $id, 'type' => 'list'])->execute();
                    Yii::$app->db->createCommand()->update('valuation_enquiry', ['new_from_date' => $from_date], ['valuation_id' => $id, 'type' => 'list'])->execute();
                    Yii::$app->db->createCommand()->update('valuation_enquiry', ['mode' => 3], ['valuation_id' => $id, 'type' => 'list'])->execute();
                    $model = ValuationEnquiry::find()->where(['valuation_id' => $id, 'type' => 'list'])->one();


                    //updating selected list
                    if (!empty($autolists_3) && $autolists_3 <> null) {
                        ValuationSelectedLists::deleteAll(['valuation_id' => $id, 'type' => 'list']);
                        foreach ($autolists_3 as $selected_row) {
                            $selected_data_detail = new ValuationSelectedLists();
                            $selected_data_detail->selected_id = $selected_row->id;
                            $selected_data_detail->type = 'list';
                            $selected_data_detail->valuation_id = $id;
                            $selected_data_detail->save();
                        }

                    }


                    $selected_data = ArrayHelper::map(\app\models\ValuationSelectedLists::find()
                        ->where(['valuation_id' => $id, 'type' => 'sold'])
                        ->limit(10)
                        ->orderBy(['latest' => SORT_DESC])
                        ->all(), 'id', 'selected_id');

                    $totalResults = (new \yii\db\Query())
                        ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) avg_listings_price_size, MIN(listings_price) as min_price, MIN(price_per_sqt) as min_price_sqt, MAX(listings_price) as max_price, MAX(price_per_sqt) as max_price_sqt , FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
                        ->from('sold_transaction')
                        ->where(['id' => $selected_data])
                        ->all();


                    //Average and calculations

                    if (!empty($totalResults) && $totalResults <> null) {
                        if (isset($totalResults[0]['avg_bedrooms'])) {
                            ValuationListCalculation::deleteAll(['valuation_id' => $id, 'type' => 'sold']);
                            $calculations = new ValuationListCalculation();
                            $calculations->avg_bedrooms = round($totalResults[0]['avg_bedrooms']);
                            $calculations->avg_land_size = round($totalResults[0]['avg_land_size']);
                            $calculations->built_up_area_size = round($totalResults[0]['built_up_area_size']);
                            $calculations->avg_listings_price_size = round($totalResults[0]['avg_listings_price_size']);
                            $calculations->min_price = round($totalResults[0]['min_price']);
                            $calculations->max_price = round($totalResults[0]['max_price']);
                            $calculations->min_price_sqt = round($totalResults[0]['min_price_sqt']);
                            $calculations->max_price_sqt = round($totalResults[0]['max_price_sqt']);
                            $calculations->avg_listing_date = date('Y-m-d', strtotime($totalResults[0]['avg_listing_date']));
                            $calculations->avg_psf = round($calculations->avg_listings_price_size / $calculations->built_up_area_size);
                            $calculations->avg_gross_yield = 0;
                            $calculations->type = 'sold';
                            $calculations->valuation_id = $id;
                            $calculations->save();
                        }

                    }

                    $mode = 3;
                    break;
                } else {
                    //   echo "Month ".$i." count = ".$autolisting_3_count.'<br>';
                }

            }

        }


        if ($model->load(Yii::$app->request->post())) {
            // ValuationEnquiry::deleteAll(['valuation_id' => $id,'type'=> 'sold']);
            $model->valuation_id = $id;
            $model->type = 'sold';
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_10/' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('steps/_step10', [
            'model' => $model,
            'valuation' => $valuation,
        ]);
    }

    public function actionStep_10_v($id)
    {
        if (!Yii::$app->menuHelperFunction->checkActionAllowed('step_10')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }


        //Step 1st enquiry (step 10)


        $InspectProperty = \app\models\InspectProperty::find()->where(['valuation_id' => $id])->one();
        $current_date = date('Y-m-d');
        $current_date_1_year = date('Y-m-d', strtotime($current_date . ' -1 year'));

        $autolisting_count = 0;
        $mode = 0;

        $valuation = $this->findModel($id);

        $model = ValuationEnquiry::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1])->one();
        if ($valuation->valuation_status != 5 && $valuation->pre_summary == 0) {
            if ($model !== null) {
                // $model->date_from = $current_date_1_year;

            } else {
                //  $model = new ValuationEnquiry();
                $model = new ValuationEnquiry();
                $model->date_from = $current_date_1_year;
                $model->new_from_date = $current_date_1_year;
                $model->date_to = $current_date;
                $model->building_info = $valuation->building_info;
                $model->valuation_id = $id;
                $model->bedroom_from = $InspectProperty->no_of_bedrooms;
                $model->bedroom_to = $InspectProperty->no_of_bedrooms;
                $model->type = 'sold';
                $model->search_type = 0;
                $model->save();

                $model = new ValuationEnquiry();
                $model->date_from = $current_date_1_year;
                $model->new_from_date = $current_date_1_year;
                $model->date_to = $current_date;
                $model->building_info = $valuation->building_info;
                $model->valuation_id = $id;
                $model->bedroom_from = $InspectProperty->no_of_bedrooms;
                $model->bedroom_to = $InspectProperty->no_of_bedrooms;
                $model->type = 'sold';
                $model->search_type = 1;
                $model->save();
            }


            $this->actionAutolistingValuer($id, 'sold');
        }


        //Step 2nd Selection (step 11)
        $selected_data = ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');
        $selected_data_auto = ArrayHelper::map(\app\models\ValuationSelectedListsAuto::find()
            ->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');


        $totalResults = (new \yii\db\Query())
            ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) avg_listings_price_size, MIN(listings_price) as min_price, MIN(price_per_sqt) as min_price_sqt, MAX(listings_price) as max_price, MAX(price_per_sqt) as max_price_sqt , FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
            ->from('sold_transaction')
            ->where(['id' => $selected_data])
            ->all();


        //Average and calculations
        if ($valuation->valuation_status != 5 && $valuation->pre_summary == 0) {
            if (!empty($totalResults) && $totalResults <> null) {
                if (isset($totalResults[0]['avg_bedrooms'])) {
                    ValuationListCalculation::deleteAll(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 0]);
                    $calculations = new ValuationListCalculation();
                    $calculations->avg_bedrooms = round($totalResults[0]['avg_bedrooms']);
                    $calculations->avg_land_size = round($totalResults[0]['avg_land_size']);
                    $calculations->built_up_area_size = round($totalResults[0]['built_up_area_size']);
                    $calculations->avg_listings_price_size = round($totalResults[0]['avg_listings_price_size']);
                    $calculations->min_price = round($totalResults[0]['min_price']);
                    $calculations->max_price = round($totalResults[0]['max_price']);
                    $calculations->min_price_sqt = round($totalResults[0]['min_price_sqt']);
                    $calculations->max_price_sqt = round($totalResults[0]['max_price_sqt']);
                    $calculations->avg_listing_date = date('Y-m-d', strtotime($totalResults[0]['avg_listing_date']));
                    $calculations->avg_psf = round($calculations->avg_listings_price_size / $calculations->built_up_area_size);
                    $calculations->avg_gross_yield = 0;
                    $calculations->type = 'sold';
                    $calculations->valuation_id = $id;
                    $calculations->search_type = 0;
                    $calculations->save();

                    ValuationListCalculation::deleteAll(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1]);
                    $calculations_v = new ValuationListCalculation();
                    $calculations_v->avg_bedrooms = round($totalResults[0]['avg_bedrooms']);
                    $calculations_v->avg_land_size = round($totalResults[0]['avg_land_size']);
                    $calculations_v->built_up_area_size = round($totalResults[0]['built_up_area_size']);
                    $calculations_v->avg_listings_price_size = round($totalResults[0]['avg_listings_price_size']);
                    $calculations_v->min_price = round($totalResults[0]['min_price']);
                    $calculations_v->max_price = round($totalResults[0]['max_price']);
                    $calculations_v->min_price_sqt = round($totalResults[0]['min_price_sqt']);
                    $calculations_v->max_price_sqt = round($totalResults[0]['max_price_sqt']);
                    $calculations_v->avg_listing_date = date('Y-m-d', strtotime($totalResults[0]['avg_listing_date']));
                    $calculations_v->avg_psf = round($calculations_v->avg_listings_price_size / $calculations_v->built_up_area_size);
                    $calculations_v->avg_gross_yield = 0;
                    $calculations_v->type = 'sold';
                    $calculations_v->valuation_id = $id;
                    $calculations_v->search_type = 1;
                    $calculations_v->save();
                }

            }
        }

        $select_calculations = \app\models\ValuationListCalculation::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1])->one();


        // $sold_selected_records = \app\models\ValuationSelectedLists::find()->where(['selected_id' => $selected_data,'type'=> 'sold'])->all();

        // selected_data
        $sold_selected_records = \app\models\ValuationEnquiry::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1])->one();
        $sold_selected_records_0 = \app\models\ValuationEnquiry::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 0])->one();


        $listing_filter = array();
        if (!empty($sold_selected_records) && $sold_selected_records) {

            $listing_filter['SoldTransactionSearch']['date_from'] = $sold_selected_records->date_from;
            $listing_filter['SoldTransactionSearch']['date_to'] = $sold_selected_records->date_to;
            $listing_filter['SoldTransactionSearch']['building_info_data'] = $sold_selected_records->building_info;
            $listing_filter['SoldTransactionSearch']['property_id'] = $sold_selected_records->property_id;
            $listing_filter['SoldTransactionSearch']['property_category'] = $sold_selected_records->property_category;
            $listing_filter['SoldTransactionSearch']['bedroom_from'] = $sold_selected_records->bedroom_from;
            $listing_filter['SoldTransactionSearch']['bedroom_to'] = $sold_selected_records->bedroom_to;
            $listing_filter['SoldTransactionSearch']['landsize_from'] = $sold_selected_records->landsize_from;
            $listing_filter['SoldTransactionSearch']['landsize_to'] = $sold_selected_records->landsize_to;
            $listing_filter['SoldTransactionSearch']['bua_from'] = $sold_selected_records->bua_from;
            $listing_filter['SoldTransactionSearch']['bua_to'] = $sold_selected_records->bua_to;
            $listing_filter['SoldTransactionSearch']['price_from'] = $sold_selected_records->price_from;
            $listing_filter['SoldTransactionSearch']['price_to'] = $sold_selected_records->price_to;
            $listing_filter['SoldTransactionSearch']['price_psf_from'] = $sold_selected_records->price_psf_from;
            $listing_filter['SoldTransactionSearch']['price_psf_to'] = $sold_selected_records->price_psf_to;
        } else {
            $valuation = $this->findModel($id);
            $listing_filter['SoldTransactionSearch']['date_from'] = '2020-01-01';
            $listing_filter['SoldTransactionSearch']['date_to'] = date('Y-m-d');
            $listing_filter['SoldTransactionSearch']['building_info_data'] = $valuation->building_info;
        }


        $selected_records_display = $sold_selected_records_0;

        if ($selected_records_display <> null & !empty($selected_records_display)) {
            $building_info_data = '';

            if ($selected_records_display->building_info <> null) {
                $buildings = explode(",", $sold_selected_records->building_info);
                $all_buildings_titles = Buildings::find()->where(['id' => $buildings])->all();

                if ($all_buildings_titles <> null && !empty($all_buildings_titles)) {
                    foreach ($all_buildings_titles as $title) {
                        $building_info_data .= $title['title'] . ',&nbsp';
                    }

                }

            }
            $selected_records_display->building_info = $building_info_data;
            $property_data = Properties::find()->where(['id' => $sold_selected_records->property_id])->one();
            if ($property_data <> null && !empty($property_data)) {
                $selected_records_display->property_id = $property_data->title;
            }
            $property_data = Properties::find()->where(['id' => $sold_selected_records->property_id])->one();
            if ($selected_records_display->property_category <> null) {
                $selected_records_display->property_category = Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$sold_selected_records_display->property_category];
            }
        }

        $valuation = $this->findModel($id);
        // $searchModel = new ListingsTransactionsSearch();
        $searchModel = new SoldTransactionSearch();
        $dataProvider = $searchModel->searchvaluation($listing_filter);
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;


        //Step 3rd derive function (step 12)
        $model = $this->findModel($id);

        $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $id])->one();

        $location_attributes_value = Yii::$app->appHelperFunctions->getLocationAttributes($inspection_data);
        $view_attributes_value = Yii::$app->appHelperFunctions->getViewAttributes($inspection_data);

        $list_calculate_data = \app\models\ValuationListCalculation::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1])->one();
        $weightages = \app\models\Weightages::find()->where(['property_type' => $model->property_id])->one();


        $buildongs_info = \app\models\Buildings::find()->where(['id' => $model->building_info])->one();
        $cost_info = \app\models\CostDetails::find()->where(['valuation_id' => $id])->one();
        $developer_margin = \app\models\ValuationDeveloperDriveMargin::find()->where(['valuation_id' => $id])->one();

        $weightages_required_month = array();
        $weightages_required_year = array();

        $weightages_required_month[1] = $weightages->less_than_1_month;
        $weightages_required_month[2] = $weightages->less_than_2_month;
        $weightages_required_month[3] = $weightages->less_than_3_month;
        $weightages_required_month[4] = $weightages->less_than_4_month;
        $weightages_required_month[5] = $weightages->less_than_5_month;
        $weightages_required_month[6] = $weightages->less_than_6_month;
        $weightages_required_month[7] = $weightages->less_than_7_month;
        $weightages_required_month[8] = $weightages->less_than_8_month;
        $weightages_required_month[9] = $weightages->less_than_9_month;
        $weightages_required_month[10] = $weightages->less_than_10_month;
        $weightages_required_month[11] = $weightages->less_than_11_month;
        $weightages_required_month[12] = $weightages->less_than_12_month;

        $weightages_required_year[2] = $weightages->less_than_2_year;
        $weightages_required_year[3] = $weightages->less_than_3_year;
        $weightages_required_year[4] = $weightages->less_than_4_year;
        $weightages_required_year[5] = $weightages->less_than_5_year;
        $weightages_required_year[6] = $weightages->less_than_6_year;
        $weightages_required_year[7] = $weightages->less_than_7_year;
        $weightages_required_year[8] = $weightages->less_than_8_year;
        $weightages_required_year[9] = $weightages->less_than_9_year;
        $weightages_required_year[10] = $weightages->less_than_10_year;

        $avg_date = date_create($list_calculate_data->avg_listing_date);
        $current_date = date('Y-m-d');
        $current_date = date_create($current_date);
        $diff = date_diff($current_date, $avg_date);
        $difference_in_days = $diff->format("%a");
        $date_weightages = 0;

        $years_remaining = intval($difference_in_days / 365);
        $days_remaining = $difference_in_days % 365;

        if ($years_remaining > 1) {
            if ($days_remaining > 0) {

                $date_weightages = $weightages_required_year[$years_remaining + 1];
            } else {
                $date_weightages = $weightages_required_year[$years_remaining];
            }
        } else {
            $month_no = intval($days_remaining / 30);
            $days_remaining_month = $days_remaining % 30;
            if ($days_remaining_month > 0) {
                $date_weightages = $weightages_required_month[$month_no + 1];
            } else {
                $date_weightages = $weightages_required_month[$month_no];
            }
        }

        $drive_margin_saved = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1])->one();
        $drive_margin_saved_m = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 0])->one();
        /*if ($list_calculate_data->built_up_area_size > 0) {
            $landsize = round(($list_calculate_data->avg_land_size / $list_calculate_data->built_up_area_size), 4);
        } else {
            $landsize = 0;
        }
        $difference_land_size = ($landsize * $inspection_data->built_up_area) - $model->land_size;
        if ($list_calculate_data->avg_land_size > 0) {
            $difference_land_size_percentage = round(abs(($difference_land_size / $list_calculate_data->avg_land_size) * 100), 2);
        } else {
            $difference_land_size_percentage = 0;
        }*/

        $simple_land_check = 0;
        $allow_properties_simple = [4, 5, 29, 39, 44, 46, 48, 49, 50, 51, 52, 53, 5, 55, 56];
        if (in_array($valuation->property_id, $allow_properties_simple)) {
            $simple_land_check = 1;
        }
        if ($simple_land_check == 1) {
            $gfa = $inspection_data->gfa;
            if ($gfa <= 0) {
                $gfa = round($inspection_data->built_up_area / $model->land_size);
            }
            if ($list_calculate_data->avg_land_size > 0) {
                $gfa_avg = round($list_calculate_data->built_up_area_size / $list_calculate_data->avg_land_size);
            } else {
                $gfa_avg = 0;
            }
            $difference_land_size_percentage = $gfa - $gfa_avg;
        } else {


            $difference_land_size = $model->land_size - $list_calculate_data->avg_land_size;
            if ($list_calculate_data->avg_land_size > 0) {
                $difference_land_size_percentage = round(abs(($difference_land_size / $list_calculate_data->avg_land_size) * 100), 2);
            } else {
                $difference_land_size_percentage = 0;
            }
        }

        $difference_bua = $inspection_data->built_up_area - $list_calculate_data->built_up_area_size;
        if ($list_calculate_data->built_up_area_size > 0) {
            $difference_bua_percentage = round(abs(($difference_bua / $list_calculate_data->built_up_area_size) * 100), 2);
        } else {
            $difference_bua_percentage = 0;
        }
        if ($valuation->valuation_status != 5 && $valuation->pre_summary == 0) {
            if (empty($drive_margin_saved) && $drive_margin_saved == null) {

                //  $selected_data_last_step = ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $id, 'type' => 'sold'])->all(), 'id', 'selected_id');
                $selected_data_last_step = ArrayHelper::map(\app\models\ValuationSelectedLists::find()
                    ->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1])
                    ->limit(10)
                    ->orderBy(['latest' => SORT_DESC])
                    ->all(), 'id', 'selected_id');
                $totalResults_average = (new \yii\db\Query())
                    ->select('AVG(no_of_bedrooms) as avg_no_of_bedrooms')
                    ->from('sold_transaction')
                    ->where(['id' => $selected_data_last_step])
                    ->all();

                $drive_margin_saved_data = new ValuationDriveMv();

                $drive_margin_saved_data->average_location = $weightages->location_avg;
                $drive_margin_saved_data->average_age = $buildongs_info->estimated_age;
                $drive_margin_saved_data->average_tenure = $weightages->tenure_avg;
                $drive_margin_saved_data->average_view = $weightages->view_avg;
                $drive_margin_saved_data->average_finished_status = $weightages->finished_status_avg;
                $drive_margin_saved_data->average_property_condition = $weightages->property_condition_avg;
                $drive_margin_saved_data->average_upgrades = $weightages->upgrades_avg;
                // $drive_margin_saved_data->average_furnished = 3;
                $drive_margin_saved_data->average_property_exposure = $weightages->property_exposure_avg;
                $drive_margin_saved_data->average_property_placement = $weightages->property_exposure_avg;
                // $drive_margin_saved_data->average_full_building_floors = (isset($model->floor_number) ? round($model->floor_number, 2) : 0);;
                $drive_margin_saved_data->average_full_building_floors = (isset($inspection_data->full_building_floors) ? round($inspection_data->full_building_floors / 2, 2) : 0);
                $drive_margin_saved_data->average_number_of_levels = $weightages->number_of_levels_avg;
                $drive_margin_saved_data->average_no_of_bedrooms = (isset($totalResults_average[0]['avg_no_of_bedrooms']) ? round($totalResults_average[0]['avg_no_of_bedrooms'], 2) : 0);;
                $drive_margin_saved_data->average_parking_space = $weightages->parking_space_avg;
                $drive_margin_saved_data->average_pool = $weightages->pool_avg;
                $drive_margin_saved_data->average_landscaping = $weightages->landscaping_avg;
                $drive_margin_saved_data->average_white_goods = $weightages->white_goods_avg;
                $drive_margin_saved_data->average_utilities_connected = $weightages->utilities_connected_avg;
                $drive_margin_saved_data->average_developer_margin = $weightages->developer_margin_avg;
                $drive_margin_saved_data->average_land_size = $weightages->land_size_avg;
                $drive_margin_saved_data->average_balcony_size = $weightages->balcony_size_avg;;
                $drive_margin_saved_data->average_built_up_area = $list_calculate_data->built_up_area_size;
                $drive_margin_saved_data->average_date = $list_calculate_data->avg_listing_date;
                $drive_margin_saved_data->average_sale_price = $list_calculate_data->avg_listings_price_size;;
                $drive_margin_saved_data->average_psf = $list_calculate_data->avg_psf;;

                $drive_margin_saved_data->weightage_location = $weightages->location;
                $drive_margin_saved_data->weightage_age = $weightages->age;
                $drive_margin_saved_data->weightage_tenure = $weightages->tenure;
                $drive_margin_saved_data->weightage_view = $weightages->view;
                $drive_margin_saved_data->weightage_finished_status = $weightages->finishing_status;
                $drive_margin_saved_data->weightage_property_condition = $weightages->upgrades;
                $drive_margin_saved_data->weightage_upgrades = $weightages->quality;
                $drive_margin_saved_data->weightage_furnished = $weightages->furnished;
                $drive_margin_saved_data->weightage_property_exposure = $weightages->property_exposure;
                $drive_margin_saved_data->weightage_property_placement = $weightages->property_placement;
                $drive_margin_saved_data->weightage_full_building_floors = $weightages->floor;
                $drive_margin_saved_data->weightage_number_of_levels = $weightages->number_of_levels;
                $drive_margin_saved_data->weightage_no_of_bedrooms = $weightages->bedrooom;
                $drive_margin_saved_data->weightage_parking_space = $weightages->parking;
                $drive_margin_saved_data->weightage_pool = $weightages->pool;
                $drive_margin_saved_data->weightage_landscaping = $weightages->landscape;
                $drive_margin_saved_data->weightage_white_goods = $weightages->white_goods;
                $drive_margin_saved_data->weightage_utilities_connected = $weightages->utilities_connected;
                $drive_margin_saved_data->weightage_developer_margin = $developer_margin->developer_margin_percentage;
                // $drive_margin_saved_data->weightage_land_size = Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);


                if ($simple_land_check == 1) {
                    $drive_margin_saved_data->weightage_land_size = Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);
                } else {
                    $drive_margin_saved_data->weightage_land_size = Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);
                }

                $drive_margin_saved_data->weightage_balcony_size = $weightages->balcony_size;
                $drive_margin_saved_data->weightage_built_up_area = Yii::$app->appHelperFunctions->getBuaweightages($difference_bua_percentage);
                $drive_margin_saved_data->weightage_date = $date_weightages;
                $drive_margin_saved_data->valuation_id = $id;
                $drive_margin_saved_data->type = 'sold';
                $drive_margin_saved_data->search_type = 0;
                if (!$drive_margin_saved_data->save()) {
                }

                $drive_margin_saved_data = new ValuationDriveMv();

                $drive_margin_saved_data->average_location = $weightages->location_avg;
                $drive_margin_saved_data->average_age = $buildongs_info->estimated_age;
                $drive_margin_saved_data->average_tenure = $weightages->tenure_avg;
                $drive_margin_saved_data->average_view = $weightages->view_avg;
                $drive_margin_saved_data->average_finished_status = $weightages->finished_status_avg;
                $drive_margin_saved_data->average_property_condition = $weightages->property_condition_avg;
                $drive_margin_saved_data->average_upgrades = $weightages->upgrades_avg;
                // $drive_margin_saved_data->average_furnished = 3;
                $drive_margin_saved_data->average_property_exposure = $weightages->property_exposure_avg;
                $drive_margin_saved_data->average_property_placement = $weightages->property_exposure_avg;
                // $drive_margin_saved_data->average_full_building_floors = (isset($model->floor_number) ? round($model->floor_number, 2) : 0);;
                $drive_margin_saved_data->average_full_building_floors = (isset($inspection_data->full_building_floors) ? round($inspection_data->full_building_floors / 2, 2) : 0);
                $drive_margin_saved_data->average_number_of_levels = $weightages->number_of_levels_avg;
                $drive_margin_saved_data->average_no_of_bedrooms = (isset($totalResults_average[0]['avg_no_of_bedrooms']) ? round($totalResults_average[0]['avg_no_of_bedrooms'], 2) : 0);;
                $drive_margin_saved_data->average_parking_space = $weightages->parking_space_avg;
                $drive_margin_saved_data->average_pool = $weightages->pool_avg;
                $drive_margin_saved_data->average_landscaping = $weightages->landscaping_avg;
                $drive_margin_saved_data->average_white_goods = $weightages->white_goods_avg;
                $drive_margin_saved_data->average_utilities_connected = $weightages->utilities_connected_avg;
                $drive_margin_saved_data->average_developer_margin = $weightages->developer_margin_avg;
                $drive_margin_saved_data->average_land_size = $weightages->land_size_avg;
                $drive_margin_saved_data->average_balcony_size = $weightages->balcony_size_avg;;
                $drive_margin_saved_data->average_built_up_area = $list_calculate_data->built_up_area_size;
                $drive_margin_saved_data->average_date = $list_calculate_data->avg_listing_date;
                $drive_margin_saved_data->average_sale_price = $list_calculate_data->avg_listings_price_size;;
                $drive_margin_saved_data->average_psf = $list_calculate_data->avg_psf;;

                $drive_margin_saved_data->weightage_location = $weightages->location;
                $drive_margin_saved_data->weightage_age = $weightages->age;
                $drive_margin_saved_data->weightage_tenure = $weightages->tenure;
                $drive_margin_saved_data->weightage_view = $weightages->view;
                $drive_margin_saved_data->weightage_finished_status = $weightages->finishing_status;
                $drive_margin_saved_data->weightage_property_condition = $weightages->upgrades;
                $drive_margin_saved_data->weightage_upgrades = $weightages->quality;
                $drive_margin_saved_data->weightage_furnished = $weightages->furnished;
                $drive_margin_saved_data->weightage_property_exposure = $weightages->property_exposure;
                $drive_margin_saved_data->weightage_property_placement = $weightages->property_placement;
                $drive_margin_saved_data->weightage_full_building_floors = $weightages->floor;
                $drive_margin_saved_data->weightage_number_of_levels = $weightages->number_of_levels;
                $drive_margin_saved_data->weightage_no_of_bedrooms = $weightages->bedrooom;
                $drive_margin_saved_data->weightage_parking_space = $weightages->parking;
                $drive_margin_saved_data->weightage_pool = $weightages->pool;
                $drive_margin_saved_data->weightage_landscaping = $weightages->landscape;
                $drive_margin_saved_data->weightage_white_goods = $weightages->white_goods;
                $drive_margin_saved_data->weightage_utilities_connected = $weightages->utilities_connected;
                $drive_margin_saved_data->weightage_developer_margin = $developer_margin->developer_margin_percentage;
                if ($simple_land_check == 1) {
                    $drive_margin_saved_data->weightage_land_size = Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);
                } else {
                    $drive_margin_saved_data->weightage_land_size = Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);
                }
                $drive_margin_saved_data->weightage_balcony_size = $weightages->balcony_size;
                $drive_margin_saved_data->weightage_built_up_area = Yii::$app->appHelperFunctions->getBuaweightages($difference_bua_percentage);
                $drive_margin_saved_data->weightage_date = $date_weightages;
                $drive_margin_saved_data->valuation_id = $id;
                $drive_margin_saved_data->type = 'sold';
                $drive_margin_saved_data->search_type = 1;
                if (!$drive_margin_saved_data->save()) {
                }


                $drive_margin_saved = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1])->one();
                $drive_margin_saved_m = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 0])->one();
            }
        }

        $preCalculationArray = array();

        $preCalculationArray['location']['average'] = $drive_margin_saved->average_location;
        // $preCalculationArray['location']['subject_property'] = $inspection_data->location;
        $preCalculationArray['location']['subject_property'] = $location_attributes_value;
        $preCalculationArray['location']['difference'] = $preCalculationArray['location']['subject_property'] - $preCalculationArray['location']['average'];
        $preCalculationArray['location']['standard_weightage'] = $weightages->location;
        $preCalculationArray['location']['change_weightage'] = $drive_margin_saved->weightage_location;
        $preCalculationArray['location']['adjustments'] = round(($preCalculationArray['location']['difference'] * ($preCalculationArray['location']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['age']['average'] = $drive_margin_saved->average_age;
        $preCalculationArray['age']['subject_property'] = $inspection_data->estimated_age;
        $preCalculationArray['age']['difference'] = $preCalculationArray['age']['average'] - $preCalculationArray['age']['subject_property'];
        $preCalculationArray['age']['standard_weightage'] = $weightages->age;
        $preCalculationArray['age']['change_weightage'] = $drive_margin_saved->weightage_age;
        $preCalculationArray['age']['adjustments'] = round(($preCalculationArray['age']['difference'] * ($preCalculationArray['age']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['tenure']['average'] = $drive_margin_saved->average_tenure;
        $preCalculationArray['tenure']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure]];
        $preCalculationArray['tenure']['difference'] = $preCalculationArray['tenure']['subject_property'] - $preCalculationArray['tenure']['average'];
        $preCalculationArray['tenure']['standard_weightage'] = $weightages->tenure;
        $preCalculationArray['tenure']['change_weightage'] = $drive_margin_saved->weightage_tenure;
        $preCalculationArray['tenure']['adjustments'] = round(($preCalculationArray['tenure']['difference'] * ($preCalculationArray['tenure']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['view']['average'] = $drive_margin_saved->average_view;
        // $preCalculationArray['view']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->otherPropertyViewListArr[$inspection_data->view]];
        $preCalculationArray['view']['subject_property'] = $view_attributes_value;
        $preCalculationArray['view']['difference'] = $preCalculationArray['view']['subject_property'] - $preCalculationArray['view']['average'];
        $preCalculationArray['view']['standard_weightage'] = $weightages->view;
        $preCalculationArray['view']['change_weightage'] = $drive_margin_saved->weightage_view;
        $preCalculationArray['view']['adjustments'] = round(($preCalculationArray['view']['difference'] * ($preCalculationArray['view']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['finished_status']['average'] = $drive_margin_saved->average_finished_status;
        $preCalculationArray['finished_status']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->finished_status];
        $preCalculationArray['finished_status']['difference'] = $preCalculationArray['finished_status']['subject_property'] - $preCalculationArray['finished_status']['average'];
        $preCalculationArray['finished_status']['standard_weightage'] = $weightages->furnished;
        $preCalculationArray['finished_status']['change_weightage'] = $drive_margin_saved->weightage_finished_status;
        $preCalculationArray['finished_status']['adjustments'] = round(($preCalculationArray['finished_status']['difference'] * ($preCalculationArray['finished_status']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['property_condition']['average'] = $drive_margin_saved->average_property_condition;
        $preCalculationArray['property_condition']['subject_property'] = Yii::$app->appHelperFunctions->propertyConditionRatingtArr[$inspection_data->property_condition];
        $preCalculationArray['property_condition']['difference'] = $preCalculationArray['property_condition']['subject_property'] - $preCalculationArray['property_condition']['average'];
        $preCalculationArray['property_condition']['standard_weightage'] = $weightages->upgrades;
        $preCalculationArray['property_condition']['change_weightage'] = $drive_margin_saved->weightage_property_condition;
        $preCalculationArray['property_condition']['adjustments'] = round(($preCalculationArray['property_condition']['difference'] * ($preCalculationArray['property_condition']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));


        $preCalculationArray['upgrades']['average'] = $drive_margin_saved->average_upgrades;
        $preCalculationArray['upgrades']['subject_property'] = $model->valuationConfiguration->over_all_upgrade;
        $preCalculationArray['upgrades']['difference'] = $preCalculationArray['upgrades']['subject_property'] - $preCalculationArray['upgrades']['average'];
        $preCalculationArray['upgrades']['standard_weightage'] = $weightages->quality;
        $preCalculationArray['upgrades']['change_weightage'] = $drive_margin_saved->weightage_upgrades;
        $preCalculationArray['upgrades']['adjustments'] = round(($preCalculationArray['upgrades']['difference'] * ($preCalculationArray['upgrades']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));


        $preCalculationArray['property_exposure']['average'] = $drive_margin_saved->average_property_exposure;
        $preCalculationArray['property_exposure']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->propertyExposureListArr[$inspection_data->property_exposure]];
        $preCalculationArray['property_exposure']['difference'] = $preCalculationArray['property_exposure']['subject_property'] - $preCalculationArray['property_exposure']['average'];
        $preCalculationArray['property_exposure']['standard_weightage'] = $weightages->property_exposure;
        $preCalculationArray['property_exposure']['change_weightage'] = $drive_margin_saved->weightage_property_exposure;
        $preCalculationArray['property_exposure']['adjustments'] = round(($preCalculationArray['property_exposure']['difference'] * ($preCalculationArray['property_exposure']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['property_placement']['average'] = $drive_margin_saved->average_property_placement;
        $preCalculationArray['property_placement']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->propertyPlacementListArr[$inspection_data->property_placement]];
        $preCalculationArray['property_placement']['difference'] = $preCalculationArray['property_placement']['subject_property'] - $preCalculationArray['property_placement']['average'];
        $preCalculationArray['property_placement']['standard_weightage'] = $weightages->property_placement;
        $preCalculationArray['property_placement']['change_weightage'] = $drive_margin_saved->weightage_property_placement;
        $preCalculationArray['property_placement']['adjustments'] = round(($preCalculationArray['property_placement']['difference'] * ($preCalculationArray['property_placement']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['floors_adjustment']['average'] = $drive_margin_saved->average_full_building_floors;
        $preCalculationArray['floors_adjustment']['subject_property'] = (isset($model->floor_number) ? $model->floor_number : 0);
        $preCalculationArray['floors_adjustment']['difference'] = $preCalculationArray['floors_adjustment']['subject_property'] - $preCalculationArray['floors_adjustment']['average'];
        $preCalculationArray['floors_adjustment']['standard_weightage'] = $weightages->floor;
        $preCalculationArray['floors_adjustment']['change_weightage'] = $drive_margin_saved->weightage_full_building_floors;
        $preCalculationArray['floors_adjustment']['adjustments'] = round(($preCalculationArray['floors_adjustment']['difference'] * ($preCalculationArray['floors_adjustment']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['number_of_levels']['average'] = $drive_margin_saved->average_number_of_levels;
        $preCalculationArray['number_of_levels']['subject_property'] = $inspection_data->number_of_levels;
        $preCalculationArray['number_of_levels']['difference'] = $preCalculationArray['number_of_levels']['subject_property'] - $preCalculationArray['number_of_levels']['average'];
        $preCalculationArray['number_of_levels']['standard_weightage'] = $weightages->number_of_levels;
        $preCalculationArray['number_of_levels']['change_weightage'] = $drive_margin_saved->weightage_number_of_levels;
        $preCalculationArray['number_of_levels']['adjustments'] = round(($preCalculationArray['number_of_levels']['difference'] * ($preCalculationArray['number_of_levels']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['no_of_bedrooms']['average'] = $drive_margin_saved->average_no_of_bedrooms;
        $preCalculationArray['no_of_bedrooms']['subject_property'] = $inspection_data->no_of_bedrooms;
        $preCalculationArray['no_of_bedrooms']['difference'] = $preCalculationArray['no_of_bedrooms']['subject_property'] - $preCalculationArray['no_of_bedrooms']['average'];
        $preCalculationArray['no_of_bedrooms']['standard_weightage'] = $weightages->bedrooom;
        $preCalculationArray['no_of_bedrooms']['change_weightage'] = $drive_margin_saved->weightage_no_of_bedrooms;
        $preCalculationArray['no_of_bedrooms']['adjustments'] = round(($preCalculationArray['no_of_bedrooms']['difference'] * ($preCalculationArray['no_of_bedrooms']['standard_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['parking_space']['average'] = $drive_margin_saved->average_parking_space;
        $preCalculationArray['parking_space']['subject_property'] = $inspection_data->parking_floors;
        $preCalculationArray['parking_space']['difference'] = $preCalculationArray['parking_space']['subject_property'] - $preCalculationArray['parking_space']['average'];
        $preCalculationArray['parking_space']['standard_weightage'] = $weightages->parking;
        $preCalculationArray['parking_space']['change_weightage'] = $drive_margin_saved->weightage_parking_space;
        $preCalculationArray['parking_space']['adjustments'] = round(($cost_info->parking_price * $preCalculationArray['parking_space']['difference']) * ($preCalculationArray['parking_space']['change_weightage'] / 100), 2);


        $preCalculationArray['pool']['average'] = $drive_margin_saved->average_pool;
        $preCalculationArray['pool']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->pool];
        $preCalculationArray['pool']['difference'] = $preCalculationArray['pool']['subject_property'] - $preCalculationArray['pool']['average'];
        $preCalculationArray['pool']['standard_weightage'] = $weightages->pool;
        $preCalculationArray['pool']['change_weightage'] = $drive_margin_saved->weightage_pool;
        $preCalculationArray['pool']['adjustments'] = round(($cost_info->pool_price * $preCalculationArray['pool']['difference']) * ($preCalculationArray['pool']['change_weightage'] / 100), 2);

        $preCalculationArray['landscaping']['average'] = $drive_margin_saved->average_landscaping;
        $preCalculationArray['landscaping']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->landscaping];
        $preCalculationArray['landscaping']['difference'] = $preCalculationArray['landscaping']['subject_property'] - $preCalculationArray['landscaping']['average'];
        $preCalculationArray['landscaping']['standard_weightage'] = $weightages->landscape;
        $preCalculationArray['landscaping']['change_weightage'] = $drive_margin_saved->weightage_landscaping;;
        $preCalculationArray['landscaping']['adjustments'] = round(($cost_info->landscape_price * $preCalculationArray['landscaping']['difference']) * ($preCalculationArray['landscaping']['change_weightage'] / 100), 2);

        $preCalculationArray['white_goods']['average'] = $drive_margin_saved->average_white_goods;
        $preCalculationArray['white_goods']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->white_goods];
        $preCalculationArray['white_goods']['difference'] = $preCalculationArray['white_goods']['subject_property'] - $preCalculationArray['white_goods']['average'];
        $preCalculationArray['white_goods']['standard_weightage'] = $weightages->white_goods;
        $preCalculationArray['white_goods']['change_weightage'] = $drive_margin_saved->weightage_white_goods;
        $preCalculationArray['white_goods']['change_weightage'] = $drive_margin_saved->weightage_white_goods;
        $preCalculationArray['white_goods']['adjustments'] = round(($cost_info->white_goods_price * $preCalculationArray['white_goods']['difference']) * ($preCalculationArray['white_goods']['change_weightage'] / 100), 2);

        $preCalculationArray['utilities_connected']['average'] = $drive_margin_saved->average_utilities_connected;
        $preCalculationArray['utilities_connected']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->utilities_connected];
        $preCalculationArray['utilities_connected']['difference'] = $preCalculationArray['utilities_connected']['subject_property'] - $preCalculationArray['utilities_connected']['average'];
        $preCalculationArray['utilities_connected']['standard_weightage'] = $weightages->utilities_connected;
        $preCalculationArray['utilities_connected']['change_weightage'] = $drive_margin_saved->weightage_utilities_connected;
        $preCalculationArray['utilities_connected']['adjustments'] = round(($cost_info->utilities_connected_price * $preCalculationArray['utilities_connected']['difference']) * ($preCalculationArray['utilities_connected']['change_weightage'] / 100), 2);

        $preCalculationArray['developer_margin']['average'] = 0;
        $preCalculationArray['developer_margin']['subject_property'] = 0;
        $preCalculationArray['developer_margin']['difference'] = 0;
        $preCalculationArray['developer_margin']['standard_weightage'] = 0;
        $preCalculationArray['developer_margin']['change_weightage'] = ($drive_margin_saved->weightage_developer_margin > 0) ? -$drive_margin_saved->weightage_developer_margin : $drive_margin_saved->weightage_developer_margin;
        $preCalculationArray['developer_margin']['adjustments'] = round((($preCalculationArray['developer_margin']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size), 2);
        if ($list_calculate_data->built_up_area_size > 0) {
            $landsize = round(($list_calculate_data->avg_land_size / $list_calculate_data->built_up_area_size), 4);
        } else {
            $landsize = 0;
        }
        $preCalculationArray['land_size']['average'] = $landsize * $inspection_data->built_up_area;
        $preCalculationArray['land_size']['subject_property'] = $model->land_size;
        $preCalculationArray['land_size']['difference'] = $preCalculationArray['land_size']['subject_property'] - $preCalculationArray['land_size']['average'];
        $preCalculationArray['land_size']['standard_weightage'] = Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);
        $preCalculationArray['land_size']['change_weightage'] = $drive_margin_saved->weightage_land_size;
        $preCalculationArray['land_size']['adjustments'] = round(($preCalculationArray['land_size']['difference'] * ($preCalculationArray['land_size']['change_weightage'] / 100) * $cost_info->lands_price));

        $preCalculationArray['balcony_size']['average'] = $drive_margin_saved->average_balcony_size;
        $preCalculationArray['balcony_size']['subject_property'] = $inspection_data->balcony_size;
        $preCalculationArray['balcony_size']['difference'] = $preCalculationArray['balcony_size']['subject_property'] - $preCalculationArray['balcony_size']['average'];
        $preCalculationArray['balcony_size']['standard_weightage'] = $weightages->balcony_size;
        $preCalculationArray['balcony_size']['change_weightage'] = $drive_margin_saved->weightage_balcony_size;
        // $preCalculationArray['balcony_size']['adjustments'] = round(($preCalculationArray['balcony_size']['difference'] * ($preCalculationArray['balcony_size']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));
        $preCalculationArray['balcony_size']['adjustments'] = round(($preCalculationArray['balcony_size']['difference'] * ($preCalculationArray['balcony_size']['change_weightage'] / 100) * $list_calculate_data->avg_psf));

        $preCalculationArray['built_up_area']['average'] = $drive_margin_saved->average_built_up_area;
        $preCalculationArray['built_up_area']['subject_property'] = $inspection_data->built_up_area;
        $preCalculationArray['built_up_area']['difference'] = $preCalculationArray['built_up_area']['subject_property'] - $preCalculationArray['built_up_area']['average'];
        $preCalculationArray['built_up_area']['standard_weightage'] = Yii::$app->appHelperFunctions->getBuaweightages($difference_bua_percentage);
        $preCalculationArray['built_up_area']['change_weightage'] = $drive_margin_saved->weightage_built_up_area;
        $preCalculationArray['built_up_area']['adjustments'] = round(($preCalculationArray['built_up_area']['difference'] * ($preCalculationArray['built_up_area']['change_weightage'] / 100) * $list_calculate_data->avg_psf));

        $preCalculationArray['date']['average'] = $drive_margin_saved->average_date;
        $preCalculationArray['date']['subject_property'] = date('Y-m-d');
        $preCalculationArray['date']['difference'] = $difference_in_days;
        $preCalculationArray['date']['standard_weightage'] = $date_weightages;
        $preCalculationArray['date']['change_weightage'] = $drive_margin_saved->weightage_date;
        $preCalculationArray['date']['adjustments'] = round((($preCalculationArray['date']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        if ($inspection_data->built_up_area > 0) {
            $sold_avg_psf = round($list_calculate_data->avg_psf / $inspection_data->built_up_area, 2);
        } else {
            $sold_avg_psf = 0;
        }


        $sold_adjustments = 0;

        foreach ($preCalculationArray as $calucation) {
            $sold_adjustments = $sold_adjustments + $calucation['adjustments'];
        }
        $total_estimate_price = $list_calculate_data->avg_listings_price_size + $sold_adjustments;
        if ($inspection_data->built_up_area > 0) {
            $sold_avg_psf = round($total_estimate_price / $inspection_data->built_up_area, 2);
        } else {
            $sold_avg_psf = 0;
        }
        if ($valuation->valuation_status != 5 && $valuation->pre_summary == 0) {
            $drive_margin_saved->mv_total_price = $total_estimate_price;
            $drive_margin_saved->mv_avg_psf = $sold_avg_psf;
            $drive_margin_saved->search_type = 1;
            $drive_margin_saved->save();

            $drive_margin_saved_m->mv_total_price = $total_estimate_price;
            $drive_margin_saved_m->mv_avg_psf = $sold_avg_psf;
            $drive_margin_saved_m->search_type = 0;
            $drive_margin_saved_m->save();
        }

        $drive_margin_saved_updated = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1])->one();

        $viewpath = 'steps_locked/_step11_v';
        if ($valuation->valuation_status == 5) {
            $viewpath = 'steps_locked/_step11_v';
        }

        return $this->render($viewpath, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'valuation' => $valuation,
            'selected_data' => $selected_data,
            'selected_data_auto' => $selected_data_auto,
            'totalResults' => $totalResults,
            'select_calculations' => $select_calculations,
            'selected_records_display' => $selected_records_display,
            'selected_records_display' => $sold_selected_records_0,
            'model' => $drive_margin_saved_updated,
            'readonly' => $readonly

        ]);

        /* $viewpath = 'steps/_step12_v';
         if( $model->valuation_status == 5){
             $viewpath = 'steps_locked/_step12_v';
         }
         return $this->render($viewpath, [
             'model' => $drive_margin_saved_updated,
             'valuation' => $model,
             'preCalculationArray' => $preCalculationArray,
             'avg_psf' => $list_calculate_data->avg_psf,
             'avg_listings_price_size' => $list_calculate_data->avg_listings_price_size,
             'sold_avg_psf' => $sold_avg_psf,
             'inspection_data' => $inspection_data,
             'readonly' => $readonly,
         ]);*/


    }

    public function actionStep_10($id)
    {
        if (!Yii::$app->menuHelperFunction->checkActionAllowed('step_10')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $InspectProperty = \app\models\InspectProperty::find()->where(['valuation_id' => $id])->one();
        $current_date = date('Y-m-d');
        $current_date_1_year = date('Y-m-d', strtotime($current_date . ' -1 year'));

        $autolisting_count = 0;
        $mode = 0;

        $valuation = $this->findModel($id);

        $model = ValuationEnquiry::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 0])->one();
        if ($valuation->valuation_status != 5) {
            if ($model !== null) {
                //$model->date_from = $current_date_1_year;

            } else {
                //  $model = new ValuationEnquiry();
                $model = new ValuationEnquiry();
                $model->date_from = $current_date_1_year;
                $model->new_from_date = $current_date_1_year;
                $model->date_to = $current_date;
                $model->building_info = $valuation->building_info;
                $model->valuation_id = $id;
                $model->bedroom_from = $InspectProperty->no_of_bedrooms;
                $model->bedroom_to = $InspectProperty->no_of_bedrooms;
                $model->type = 'sold';
                $model->search_type = 0;
                $model->save();
            }


            $this->actionAutolisting($id, 'sold');
        }


        if ($model->load(Yii::$app->request->post())) {
            // ValuationEnquiry::deleteAll(['valuation_id' => $id,'type'=> 'sold']);
            $model->valuation_id = $id;
            $model->type = 'sold';
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['valuation/step_10/' . $id]);
            } else {
                if ($model->hasErrors()) {
                }
            }
        }
    }
}