<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Buildings;
use app\models\Communities;
use app\models\Properties;
use app\models\SoldTransaction;
use app\models\SubCommunities;
use yii\console\Controller;
use yii\console\ExitCode;
use app\models\Company;
use app\models\SoldData;

use yii\helpers\Url;
use yii;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class SoldcommimportController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex()
    {

        $all_data = \app\models\CommercialSolds::find()
            ->where(['status'=>0])
            ->orderBy([
                'id' => SORT_DESC,
            ])->limit(100)->all();


        foreach ($all_data as $record){




            $transaction_type = $record->transaction_type;
            $sub_type = $record->subtype;
            $sales_sequence = $record->sales_sequence;
            $reidin_ref_number = $record->reidin_ref;
            $date = str_replace('/', '-', trim($record->transaction_date));
            $date = date("Y-m-d", strtotime($date));
            $reidin_community = $record->community;
            $buildingName = $record->property;
            $reidin_property_type = $record->property_type;
            $unit_number = $record->unit;
            $needle = '-';

            /*  if (strpos($unit_number, $needle) !== false) {
                  $explode_unit_value = explode('-', $unit_number);
                  $unit_number = $explode_unit_value[1];
              }*/
            if (strpos($unit_number, $needle) !== false) {
                $explode_unit_value = explode('-', $unit_number);
                if($explode_unit_value <> null && count($explode_unit_value) > 0){
                    $unit_number = str_replace($explode_unit_value[0].'-', "", $unit_number);
                    $unit_number= trim($unit_number);
                }
            }

            $needle_bedroom = 'B/R';
            if (strpos($record->bedrooms, $needle_bedroom) !== false) {
                $berooms = explode(' B/R', trim($record->bedrooms));
                $rooms = trim($berooms[0]);
            }else{
                $rooms = null;
            }

            /* $berooms = explode(' B/R', trim($line[9]));
             $rooms = trim($berooms[0]);*/

            $floor_number = $record->floor;
            if ($floor_number == 'G') {
                $floor_number = 0;
            }
            $parking_spaces = $record->parking;
            $balcony_size = $record->balcony_area;

            $bua = str_replace(",", "", $record->size_sqf);
            $plotArea = str_replace(",", "", $record->land_size);
            $price = str_replace(",", "", $record->amount);
            $pricePerSqt = str_replace(",", "", $record->sqf);
            $reidin_developer = $record->developer;



            $buildingRow = Buildings::find()
                ->where(['=', 'title', trim($buildingName)])
                ->orWhere(['=', 'reidin_title', trim($buildingName)])
                ->orWhere(['=', 'reidin_title_2', trim($buildingName)])
                ->asArray()->one();

            if ($buildingRow != null) {


                $comparison = SoldTransaction::find()
                    ->where(['building_info' => $buildingRow['id'],
                        'no_of_bedrooms' =>  $rooms,
                        'built_up_area' => $bua,
                        //'unit_number' => $unit_number,
                        'land_size' => $plotArea,
                        'transaction_date' => $date,
                        'listings_price' => $price,
                        'price_per_sqt' => $pricePerSqt,
                        // 'reidin_ref_number' => $reidin_ref_number,
                    ])->one();



                if ($comparison == null) {
                    // echo "Record Found"; die();
                    $buildingId = $buildingRow['id'];


                    $model = new SoldTransaction;
                    $model->scenario = 'import';
                    $model->building_info = $buildingId;
                    if($rooms != 'Unknown' && $rooms != 'PENTHOUSE' && $rooms <> null){
                        $model->no_of_bedrooms = $rooms;
                    }
                    $bua = trim($bua);
                    //  $model->no_of_bedrooms = ($rooms <> null && ($rooms != 'Unknown')) ? $rooms : 0;
                    $model->built_up_area = ($bua <> null && $bua !== '-') ?  (float)$bua : 0;
                    $model->land_size = ($plotArea <> null && $plotArea !== '-') ? $plotArea : 0;;
                    $model->unit_number = ($unit_number <> null && $unit_number !== '-') ? $unit_number : '';
                    $model->floor_number = ($floor_number <> null && $floor_number !== '-') ? $floor_number : '';
                    $model->balcony_size = ($balcony_size <> null && $balcony_size !== '-') ? $balcony_size : '';
                    $model->parking_space_number = ($parking_spaces <> null && $parking_spaces !== '-') ? $parking_spaces : '';
                    $model->transaction_type = ($transaction_type <> null) ? $transaction_type : '';
                    $model->sub_type = ($sub_type <> null && $sub_type !== '-') ? $sub_type : '';
                    $model->sales_sequence = ($sales_sequence <> null && $sales_sequence !== '-') ? $sales_sequence : '';
                    $model->reidin_ref_number = ($reidin_ref_number <> null && $reidin_ref_number !== '-') ? $reidin_ref_number : '';
                    $model->reidin_community = ($reidin_community <> null && $reidin_community !== '-') ? $reidin_community : '';
                    $model->reidin_property_type = ($reidin_property_type <> null && $reidin_property_type !== '-') ? $reidin_property_type : '';
                    $model->reidin_developer = ($reidin_developer <> null && $reidin_developer !== '-') ? $reidin_developer : '';

                    $model->transaction_date = $date;
                    $model->listings_price = ($price <> null && $price !== '-') ? $price : 0;

                    $model->property_category = $buildingRow['property_category'];
                    $model->location = $buildingRow['location'];
                    $model->tenure = $buildingRow['tenure'];
                    $model->utilities_connected = $buildingRow['utilities_connected'];
                    $model->development_type = $buildingRow['development_type'];
                    $model->property_placement = $buildingRow['property_placement'];
                    $model->property_visibility = $buildingRow['property_visibility'];
                    $model->property_exposure = $buildingRow['property_exposure'];
                    $model->property_condition = $buildingRow['property_condition'];
                    $model->pool = $buildingRow['pool'];
                    $model->gym = $buildingRow['gym'];
                    $model->play_area = $buildingRow['play_area'];
                    // $model->other_facilities = $buildingRow['other_facilities'];
                    $model->landscaping = $buildingRow['landscaping'];
                    $model->white_goods = $buildingRow['white_goods'];
                    $model->furnished = $buildingRow['furnished'];
                    $model->finished_status = $buildingRow['finished_status'];
                    $model->land_transaction_id = $record->id;


                    $model->price_per_sqt = ($pricePerSqt <> null && $pricePerSqt !== '#VALUE!') ? $pricePerSqt : 0;;
                    if($transaction_type == 'Sales - Off-Plan'){
                        $model->type= 2;
                    }else{
                        $model->type= 1;
                    }
                    /*  echo "<pre>";
                      print_r($model);
                      die;*/
                    if ($model->save()) {
                        Yii::$app->db->createCommand()->update('commercial_solds', ['status' => 1], ['id' => $record->id])->execute();
                      //  Yii::$app->db->createCommand()->update('commercial_solds', ['status' => 1], ['id' => $record->id])->execute();
                       // $saved++;
                    } else {
                        if ($model->hasErrors()) {
                            foreach ($model->getErrors() as $error) {
                                if (count($error) > 0) {
                                    foreach ($error as $key => $val) {
                                        echo $val.'<br>';
                                        echo $pricePerSqt;
                                    }
                                }
                            }
                        }
                        // die();
                       /* $errNames .= '<br />' . $buildingName;
                        $unsaved++;*/
                    }


                }else{

                    Yii::$app->db->createCommand()->update('commercial_solds', ['status' => 2], ['id' => $record->id])->execute();
                }


            } else {
                Yii::$app->db->createCommand()->update('commercial_solds', ['status' => 3], ['id' => $record->id])->execute();
            }
            }
        die('here');
        }



}
