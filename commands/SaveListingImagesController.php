<?php
namespace app\commands;
use Yii;
use yii\console\Controller;

class SaveListingImagesController extends Controller
{
    public function actionIndex()
    {
        $results = \app\models\ValuationSelectedLists::find()
        ->leftJoin('valuation', 'valuation.id = valuation_selected_lists.valuation_id')
        ->where([ '>=', 'valuation.created_at', '2022-05-20 00:00:00'])
        ->andWhere([ 'img_save_status' => 0])
        ->limit(5)
            ->orderBy(['valuation.id' => SORT_DESC])
        ->all();
        if ($results<>null){
            foreach ($results as $key => $value){
                $checker = \app\models\ListingsTransactions::find()->where(['id'=>$value->selected_id])->one();
                if ($checker<>null AND $checker->listing_website_link_image==null){
                    $webPage_link = Yii::$app->appHelperFunctions->GetImgLink($value->selected_id);
                    if ($webPage_link<>null) {
                        $img_url = Yii::$app->appHelperFunctions->SaveBayutImage($webPage_link,'Yes');
                        if ($img_url<>null) {
                            Yii::$app->db->createCommand()->update('listings_transactions' , ['listing_website_link_image'=>$img_url] , ['id'=>$value->selected_id])->execute();
                            Yii::$app->db->createCommand()->update('valuation_selected_lists' , ['img_save_status' => 1 ] , ['id'=>$value->id, 'type'=>'list'])->execute();
                            echo "Saved ";
                        }
                    }
                }else{
                    Yii::$app->db->createCommand()->update('valuation_selected_lists' , ['img_save_status' => 1 ] , ['id'=>$value->id, 'type'=>'list'])->execute();
                }
            }
        }
    }

    public function actionRun()
    {
        $results = \app\models\ValuationSelectedLists::find()
        ->leftJoin('valuation', 'valuation.id = valuation_selected_lists.valuation_id')
        ->where([ '>=', 'valuation.created_at', '2022-05-20 00:00:00'])
        ->where([ 'img_save_status' => 0])
        ->limit(5)
            ->orderBy(['valuation.id' => SORT_DESC])
        ->all();
        if ($results<>null){
            foreach ($results as $key => $value){
                $checker = \app\models\ListingsTransactions::find()->where(['id'=>$value->selected_id])->one();
                if ($checker<>null AND $checker->listing_website_link_image==null){
                    $webPage_link = Yii::$app->appHelperFunctions->GetImgLink($value->selected_id);
                    if ($webPage_link<>null) {
                        $img_url = Yii::$app->appHelperFunctions->SaveBayutImage($webPage_link,'Yes');
                        if ($img_url<>null) {
                            Yii::$app->db->createCommand()->update('listings_transactions' , ['listing_website_link_image'=>$img_url] , ['id'=>$value->selected_id])->execute();
                            Yii::$app->db->createCommand()->update('valuation_selected_lists' , ['img_save_status' => 1 ] , ['id'=>$value->id, 'type'=>'list'])->execute();
                            echo "Saved ";
                        }
                    }
                }else{
                    Yii::$app->db->createCommand()->update('valuation_selected_lists' , ['img_save_status' => 1 ] , ['id'=>$value->id, 'type'=>'list'])->execute();
                }
            }
        }
    }
}