<?php
namespace app\commands;
use Yii;
use app\models\UaeListData;
use app\models\UaeListDataSearch;
use yii\console\Controller;
use yii\db\Expression;
use app\models\BuildingForSave;

class MigrationCronController extends Controller 
{
    public function actionMigration() 
    {
        $query = UaeListData::find()
        ->where(['move_to_listing'=>0])
        ->limit(500)
        ->asArray()
        ->all();
        // echo"<pre>"; print_r($query); echo"</pre>"; die();
        
        foreach ($query as $key => $query) {
            $model = \app\models\ListingsTransactions::find()->where([ 'listings_reference' => $query['listings_reference'] , 'listing_website_link' => $query['listing_website_link']])->one();
            if($model==null){



                $building = \app\models\Buildings::find()
                ->where(['city' => $query['city_id']])
                ->andWhere(new Expression(' title LIKE "%'.$query['building_info'].'%" '))
                ->one();

                if($building == null) {

                }

                explode(" ",$query['building_info']);



                $building = \app\models\Buildings::find()
                    ->where(['city' => $query['city_id']])
                    ->andWhere(new Expression(' title LIKE "%'.$query['building_info'].'%" '))
                    ->one();



                if($building<>null && $building->id<>null) {
                    $model= new \app\models\ListingsTransactions;
                    $model->listings_reference   = $query['listings_reference'];
                    $model->source               = $query['source'];
                    $model->listing_website_link = $query['listing_website_link'];
                    $model->listing_date         = $query['listing_date'];
                    $model->building_info        = $building->id;
                    $model->no_of_bedrooms       = $query['no_of_bedrooms'];
                    $model->built_up_area        = $query['built_up_area'];
                    $model->land_size            = $query['land_size'];
                    $model->listings_price       = $query['listings_price'];
                    $model->listings_rent        = $query['listings_rent'];
                    $model->final_price          = $query['final_price'];
                    $model->property_category    = $building->property_category;
                    $model->tenure               = $building->tenure;
                    $model->unit_number          = 'Not Known'; //static
                    $model->status               = 2;
                    if($model->save()) {
                        Yii::$app->db->createCommand()->update('uae_list_data', [ 'move_to_listing' => 1 ], [ 'id'=>$query['id'] ])->execute();
                    }
                    if($model->hasErrors()){
                        foreach($model->getErrors() as $error){
                            if(count($error)>0){
                                foreach($error as $key=>$val){
                                    echo $val. "e<br>";
                                }   
                            }
                        }
                      //  die();
                    }
                }else{
                    Yii::$app->db->createCommand()->update('uae_list_data', [ 'move_to_listing' => 2 ], [ 'id'=>$query['id'] ])->execute();
                    $queryCheck = BuildingForSave::find()->where(['building_name'=>$query['building_info'], 'city'=>$query['city_id']])->one();
                    if($queryCheck==null){
                        $saveModel = new BuildingForSave;
                        $saveModel->building_name = $query['building_info'];
                        $saveModel->city 		  = $query['city_id'];
                        $saveModel->save();
                        if($saveModel->hasErrors()){
                            foreach($saveModel->getErrors() as $error){
                                if(count($error)>0){
                                    foreach($error as $key=>$val){
                                        echo $val. "d<br>";
                                    }
                                }
                            }
                         //   die();
                        }
                    }
                } 
            }else{
                Yii::$app->db->createCommand()->update('uae_list_data', [ 'move_to_listing' => 3 ], [ 'id'=>$query['id'] ])->execute();
            }
        }               
    }
}

