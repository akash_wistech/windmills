<?php
namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Url;
use yii;

use app\models\ListingsTransactions;
use app\models\Buildings;
use app\models\Properties;
use app\models\Developers;
use app\models\BayutDetailPageLinks;
use app\models\BayutLinks;
use app\models\BayutLinksClild;
use app\models\BuildingForSave;
use DOMDocument;
use DOMXPath;
use Goutte\Client;


class BayutScrappingController extends Controller
{

    public function actionSaveLinks($url=null, $link_id=1)
    {
        $findUrl = BayutLinksClild::find()->one();
        if ($findUrl==null) {
            $query = BayutLinks::find()->one();
            if ($query<>null) {
                $model = new BayutLinksClild;
                $model->link_id       = $query->id;
                $model->url_to_fetch  = $query->url;
                $model->property_category  = $query->property_category;
                if($model->save()){
                    $link_id            = $model->link_id;
                    $url                = $model->url_to_fetch;
                    $property_category  = $model->property_category;
                }
            }
        }
        else{
            $link_id                = $findUrl->link_id;
            $url                    = $findUrl->url_to_fetch;
            $property_category      = $findUrl->property_category;
        }

        $httpClient = new \Goutte\Client();
        $response = $httpClient->request('GET', $url);
        $i=1;
        $response->filter('._357a9937 li.ef447dde')->each(function ($node) use($property_category, &$i) {
            $link = $node->filter('article.ca2f5674 div._4041eb80 a')->attr('href');
            $link = 'https://www.bayut.com'.$link;
            if ($link<>null) {
                $newModel = BayutDetailPageLinks::find()->where(['url'=>$link])->one();
                if ($newModel==null) {
                    $newModel = new BayutDetailPageLinks;
                    $newModel->url = $link;
                    $newModel->property_category = $property_category;
                    $newModel->status = 0;
                    if ($newModel->save()) {
                   //     echo "save-".$i." ";
                    }
                }

            }
            $i++;
        });

        $buttonLinks = [];
        $buttonTitles = [];
        $response->filter('div._035521cc div._41cc3033 div.bbfbe3d2 div._6cab5d36 ul._92c36ba1 li a')->each(function($buttonNode) use(&$buttonLinks,&$buttonTitles){
            $buttonLinks[] = $buttonNode->attr('href');
            $buttonTitles[] = $buttonNode->attr('title');
        });

        $nextPageUrl=null;
        foreach ($buttonLinks as $key => $value) {
            if ($buttonTitles[$key]=='Next') {
                $nextPageUrl = $value;
            }
        }

        if ($nextPageUrl<>null) {
            $url_to_fetch = 'https://www.bayut.com'.$nextPageUrl.'';
            $command =  Yii::$app->db->createCommand()
                ->update('bayut_links_clild', ['link_id' => $link_id,'url_to_fetch'=>$url_to_fetch, 'property_category'=>$property_category])
                ->execute();
        }
        else{
            $queryTwo = \app\models\BayutLinks::find()
                ->where(['>', 'id', $link_id])
                ->asArray()->one();
            $query =  Yii::$app->db->createCommand()
                ->update('bayut_links_clild', ['link_id' => $queryTwo['id'],'url_to_fetch'=> $queryTwo['url'], 'property_category'=>$queryTwo['property_category']])
                ->execute();
        }
    }



    public function actionSaveDetail()
    {
        $detailUrls = BayutDetailPageLinks::find()->where(['status'=>0])->asArray()->limit(1)->all();
        $i=1;
        if ($detailUrls<>null) {
            foreach ($detailUrls as $key => $detailUrl) {
                Yii::$app->db->createCommand()
                    ->update('bayut_detail_page_links', ['status' => 2], ['url'=>$detailUrl['url']])
                    ->execute();

                $httpClient = new \Goutte\Client();
                $crawler = $httpClient->request('GET', $detailUrl['url']);

                // Beds, Studio, Baths And Area Array
                $BedBathStudioEtcValues = [];
                $crawler->filter('div._6f6bb3bc div.ba1ca68e._0ee3305d')->each(function($etcNode)use(&$BedBathStudioEtcValues){
                    $BedBathStudioEtcValues[$etcNode->filter('span.cfe8d274')->attr('aria-label')] =  $etcNode->filter('span.fc2d1086')->text();
                });

                // Property Array
                $propertyInformationArr =  [];
                $crawler->filter('ul._033281ab li')->each(function($propertyNode) use(&$propertyInformationHeadings,&$propertyInformationArr) {
                    $propertyInformationArr[] = $propertyNode->filter('span._812aa185')->text();
                });

                $buildingNameFull = $crawler->filter('div._1f0f1758')->text();
                $completeTitle = $buildingNameFull;

                $pieces = explode(',', $buildingNameFull);
                $buildingName = $community = $sub_community = $city ='';
                if (isset($pieces[0])&&$pieces[0]<>null) {
                    $buildingName = $pieces[0];
                }
                if (isset($pieces[1]) && $pieces[1]<>null) {
                    $community = $pieces[1];
                }
                if (isset($pieces[3]) && $pieces[3]<>null) {
                    if (isset($pieces[2]) && $pieces[2]<>null) {
                        $sub_community = $pieces[2];
                    }
                }
                $city = array_pop($pieces);

                $propertyDescription = $crawler->filter('div._2015cd68')->text();
                $listingAndFinalPrice = $crawler->filter('div.c4fc20ba span._105b8a67')->text();

                if ($propertyInformationArr<>null) {
                    $type = $propertyInformationArr[0];
                    $referenceNumber = $propertyInformationArr[2];
                }



                if ($buildingName<>null) {
                    $city_id = Yii::$app->appHelperFunctions->getCityId($city);
                    $commSubCommArr = Yii::$app->appHelperFunctions->getCommunityAndSubCommunityId($community, $sub_community, $city_id);
                    // echo "<pre>"; print_r($commSubCommArr); die();
                    $building = Buildings::find()->where([
                        'title'         => trim($buildingName),
                        'city'          => $city_id,
                    ]);

                    if ($commSubCommArr<>null&&$commSubCommArr['community_id']<>null&&$commSubCommArr['subCommunity_id']<>null) {
                        $building->andWhere([
                            'community'     => $commSubCommArr['community_id'],
                            'sub_community' => $commSubCommArr['subCommunity_id'],
                        ]);
                    }
                    // $building->orWhere(['like', 'title', trim($buildingName) . '%', false]);
                    $building = $building->one();
                    // echo "<br><br>hello <pre>"; print_r($building); echo "</pre>"; die();

                    if ($building<>null) {
                        $data = [
                            'model' => $building,
                            'type' => $type,
                            'city' => $city,
                            'referenceNumber' => $referenceNumber,
                            'listing_website_link' => $detailUrl['url'],
                            'listingAndFinalPrice' => $listingAndFinalPrice,
                            'BedBathStudioEtcValues' => $BedBathStudioEtcValues,
                            'propertyDescription' => $propertyDescription,
                            'propertyInformationArr' => $propertyInformationArr,
                            'property_category' => $detailUrl['property_category'],
                            'completeTitle' => $completeTitle,
                        ];
                        // save listing transaction
                        $this->SaveListingTransaction($data);
                    }
                    else{
                        // echo "building null:---"; echo "<br>"; die();
                        $findBuildName = BuildingForSave::find()->where(['building_name'=>$buildingName])->one();
                        if ($findBuildName==null) {
                            $saveBuildName = new BuildingForSave;
                            $saveBuildName->building_name = $buildingName;
                            $saveBuildName->city = '';
                            if ($saveBuildName->save()) {
                                //code
                            };
                        }

                        $permission = Yii::$app->appHelperFunctions->getSetting('save_bayut_building');
                        if ($permission=="Yes") {
                            $data=[
                                'buildingName' => $buildingName,
                                'type' => $type,
                                'city' => $city,
                                'listingAndFinalPrice' => $listingAndFinalPrice,
                                'BedBathStudioEtcValues' => $BedBathStudioEtcValues,
                                'propertyDescription' => $propertyDescription,
                                'referenceNumber' => $referenceNumber,
                                'listing_website_link' => $detailUrl['url'],
                                'propertyInformationArr' => $propertyInformationArr,
                                'community' => $commSubCommArr['community_id'],
                                'sub_community' => $commSubCommArr['subCommunity_id'],
                                'city_id' => $city_id,
                                'completeTitle' => $completeTitle,
                            ];
                            $this->saveBuilding($data);
                        }
                    }
                }
                echo "<br>"; echo $i; echo "<br>";
                $i++;
                // die('break die');
            }
        }

    }




    public function SaveListingTransaction($data='')
    {

        // echo "<pre>"; print_r($data); echo "</pre>"; echo "<br><br>"; die();
        $model                       = $data['model'];
        $type                        = $data['type'];
        $listing_website_link        = $data['listing_website_link'];
        $referenceNumber             = $data['referenceNumber'];
        $BedBathStudioEtcValues      = $data['BedBathStudioEtcValues'];
        $propertyDescription         = $data['propertyDescription'];
        $propertyInformationArr      = $data['propertyInformationArr'];
        $listingAndFinalPrice        = $data['listingAndFinalPrice'];
        $city                        = $data['city'];
        $property_category           = $data['property_category'];
        $completeTitle               = $data['completeTitle'];
        //convert description into lower case
        $propertyDescription         =  strtolower($propertyDescription);
        // print_r($propertyDescription); die();
        // print_r(str_replace(',', '', $listingAndFinalPrice)); die();


        //Start Save Listing Transaction
        $checkListing = ListingsTransactions::find()->where(['listings_reference'=>$referenceNumber, 'listing_website_link'=>$listing_website_link])->one();
        if ($checkListing==null) {
            $listingModel = new ListingsTransactions;
            //Start New Listing Transaction
            $listingModel->listings_reference   = $referenceNumber;
            $listingModel->source               = 15;
            $listingModel->listing_website_link = $listing_website_link;
            $listingModel->listing_date         = date('Y-m-d', strtotime(end($propertyInformationArr)));
            $listingModel->unit_number          = 'Not Known';
            //End New Listing Transaction

            //Start Building Information
            $listingModel->building_info = $model->id;

            //not yet working properly
            if ($property_category=='Residential') {
                $listingModel->property_category = 1;
            }
            else if ($property_category=='Commercial') {
                $listingModel->property_category = 4;
            }

            //tenure working
            if ($model->tenure<>null) {
                $listingModel->tenure = $model->tenure;
            }
            else if(strpos($propertyDescription, 'freehold') !== false || strpos($propertyDescription, 'free hold') !== false || strpos($propertyDescription, 'free-hold') !== false || strpos($completeTitle, 'freehold') !== false || strpos($completeTitle, 'free hold') !== false || strpos($completeTitle, 'free-hold') !== false)
            {
                $listingModel->tenure   = 2;
            }
            else{
                if ($city=='Dubai') {
                    $listingModel->tenure   = 2;
                }else{
                    $listingModel->tenure   = 1;
                }
            }

            //property placement working
            if (strpos($type, 'Apartment') !== false || strpos($type, 'Office') !== false) {
                $listingModel->property_placement  = 3.00;
            }else{
                if (strpos($propertyDescription, 'middle') !== false || strpos($propertyDescription, 'mid unit') !== false || strpos($propertyDescription, 'mid-unit') !== false || strpos($completeTitle, 'middle') !== false || strpos($completeTitle, 'mid unit') !== false || strpos($completeTitle, 'mid-unit') !== false) {
                    $listingModel->property_placement  = 1.00;
                }
                else if(strpos($propertyDescription, 'corner') !== false || strpos($completeTitle, 'corner') !== false){
                    $listingModel->property_placement  = 2.00;
                }
                else if (strpos($propertyDescription, 'semi corner') !== false || strpos($propertyDescription, 'semi-corner') !== false || strpos($propertyDescription, 'semicorner') !== false || strpos($propertyDescription, 'end unit') !== false || strpos($completeTitle, 'semi corner') !== false || strpos($completeTitle, 'semi-corner') !== false || strpos($completeTitle, 'semicorner') !== false || strpos($completeTitle, 'end unit') !== false) {
                    $listingModel->property_placement  = 1.50;
                }
                else{
                    $listingModel->property_placement  = 1.00;
                }
            }


            //property visibility
            if ($type=='Shop') {
                $listingModel->property_visibility = 2;
                if(strpos($propertyDescription, 'back') !== false || strpos($propertyDescription, 'behind') !== false || strpos($completeTitle, 'back') !== false || strpos($completeTitle, 'behind') !== false){
                    $listingModel->property_visibility = 1;
                }
            }
            else{
                $listingModel->property_visibility = 3;
            }


            //Property Exposure working
            if (strpos($type, 'Apartment') !== false || strpos($type, 'Office') !== false) {
                $listingModel->property_exposure   = 3;
            }
            else{
                if (strpos($propertyDescription, 'single row') !== false || strpos($propertyDescription, 'single-row') !== false || strpos($completeTitle, 'single row') !== false || strpos($completeTitle, 'single-row') !== false) {
                    $listingModel->property_exposure   = 2;
                }
                else if(strpos($propertyDescription, 'back to back') !== false || strpos($propertyDescription, 'backtoback') !== false || strpos($propertyDescription, 'back 2 back') !== false || strpos($propertyDescription, 'back2back') !== false || strpos($propertyDescription, 'b2b') !== false || strpos($propertyDescription, 'back-to-back') !== false || strpos($completeTitle, 'back to back') !== false || strpos($completeTitle, 'backtoback') !== false || strpos($completeTitle, 'back 2 back') !== false || strpos($completeTitle, 'back2back') !== false || strpos($completeTitle, 'b2b') !== false || strpos($completeTitle, 'back-to-back') !== false){
                    $listingModel->property_exposure   = 1;
                }
                else{
                    $listingModel->property_exposure   = 1;
                }
            }


            $listingModel->property_condition  = 3;
            $listingModel->development_type    = 'Standard';

            //Utilities connected working
            if (strpos($type, 'Land') !== false) {
                $listingModel->utilities_connected = 'No';
            }else{
                $listingModel->utilities_connected = 'Yes';
            }

            //Finished status working
            if (strpos($type, 'Office') !== false) {
                $listingModel->finished_status     = 'Fitted';

                if(strpos($propertyDescription, 'fitted') !== false || strpos($completeTitle, 'fitted') !== false){
                    $listingModel->finished_status     = 'Shell & Core';
                }
            }
            else{
                $listingModel->finished_status     = 'Fitted';
            }

            // Pool working
            if (strpos($propertyDescription, 'private pool') !== false ||strpos($propertyDescription, 'private swimming pool') !== false ||strpos($propertyDescription, 'with pool') !== false ||strpos($propertyDescription, 'own swimming pool') !== false || strpos($propertyDescription, 'own pool') !== false || strpos($completeTitle, 'private pool') !== false ||strpos($completeTitle, 'private swimming pool') !== false ||strpos($completeTitle, 'with pool') !== false ||strpos($completeTitle, 'own swimming pool') !== false || strpos($completeTitle, 'own pool') !== false) {
                $listingModel->pool = 'Yes';
            }
            else{
                $listingModel->pool = 'No';
            }

            // Gym Working
            if (strpos($propertyDescription, 'private gym') !== false ||strpos($propertyDescription, 'own gym') !== false || strpos($propertyDescription, 'private fitness') !== false || strpos($completeTitle, 'private gym') !== false ||strpos($completeTitle, 'own gym') !== false || strpos($completeTitle, 'private fitness') !== false) {
                $listingModel->gym = 'Yes';
            }
            else{
                $listingModel->gym = 'No';
            }


            // Play area Working
            if (strpos($propertyDescription, 'private play') !== false || strpos($propertyDescription, 'own play') !== false || strpos($completeTitle, 'private play') !== false || strpos($completeTitle, 'own play') !== false) {
                $listingModel->play_area = 'Yes';
            }
            else{
                $listingModel->play_area = 'No';
            }


            $listingModel->other_facilities    = ($model->other_facilities<>null)? explode(',' , $model->other_facilities) : '';
            $listingModel->completion_status   = 100.00;

            //White goods working
            if (strpos($propertyDescription, 'equipped kitchen') !== false ||strpos($propertyDescription, 'fully equipped') !== false ||strpos($propertyDescription, 'fitted kitchen') !== false || strpos($completeTitle, 'equipped kitchen') !== false ||strpos($completeTitle, 'fully equipped') !== false ||strpos($completeTitle, 'fitted kitchen') !== false) {
                $listingModel->white_goods  = 'Yes';
            }else{
                $listingModel->white_goods  = 'No';
            }

            //Furnished Working
            $furnishedCheck = '';
            foreach ($propertyInformationArr as $key => $value) {
                if ($value == 'Furnished') {
                    $furnishedCheck = 'Furnished';
                }
            }

            if ($furnishedCheck == 'Furnished') {
                $listingModel->furnished = 'Yes';
            }
            else if (strpos($propertyDescription, 'fully furnished') !== false || strpos($completeTitle, 'fully furnished') !== false) {
                $listingModel->furnished = 'Yes';
            }
            else{
                $listingModel->furnished = 'No';
            }

            //Landscaping working
            if (strpos($type, 'Villa') !== false || strpos($type, 'Townhouse') !== false) {
                $listingModel->landscaping = 'Yes';
            }
            else{
                $listingModel->landscaping  = 'No';
            }
            //End Building Information





            //Start Property Other Information
            if ($BedBathStudioEtcValues<>null) {
                $no_of_bedrooms = 0;
                $studio ='no';
                foreach ($BedBathStudioEtcValues as $Lkey => $Lvalue) {
                    if ($Lkey=='Beds' && $Lvalue!='Studio') {
                        $numOfBeds = $Lvalue;
                        $explodeNumOfBeds = (explode(" ",$numOfBeds));
                        $no_of_bedrooms  = $explodeNumOfBeds[0];
                        // echo $no_of_bedrooms; echo "<br>"; //die();
                    }

                    if ($Lkey=='Beds' && $Lvalue=='Studio') {
                        $studio = 'yes';
                    }

                    if ($Lkey=='Area') {
                        $area = $Lvalue;
                        $explodeArea = (explode(" ",$area));
                        // print_r($explodeArea); die();
                        $MainLandOrBua = str_replace(',', '', $explodeArea[0]);;
                    }
                }
                $listingModel->no_of_bedrooms = $no_of_bedrooms;
            }



            if (strpos($propertyDescription, 'upgrades') !== false || strpos($propertyDescription, 'upgraded') !== false || strpos($completeTitle, 'upgrades') !== false || strpos($completeTitle, 'upgraded') !== false) {
                $listingModel->upgrades  = 4;
            }
            else{
                $listingModel->upgrades  = 3;
            }


            if (strpos($type, 'Land') !== false) {
                $listingModel->full_building_floors = 0;
            }
            else if (strpos($type, 'Villa') !== false  || strpos($type, 'Townhouse') !== false  ) {
                if ($model->typical_floors<>null) {
                    $listingModel->full_building_floors = $model->typical_floors;
                }
                else{
                    $listingModel->full_building_floors = 1;
                }
            }
            else{
                if ($model->typical_floors<>null) {
                    $listingModel->full_building_floors = $model->typical_floors;
                }
                else{
                    $listingModel->full_building_floors = 0;
                }
            }



            if (strpos($type, 'Villa') !== false || strpos($type, 'Townhouse') !== false) {
                $listingModel->parking_space  = 2;
            }
            else if(strpos($type, 'Apartment') !== false  && $studio=='yes'  && $no_of_bedrooms==1){
                $listingModel->parking_space  = 1;
            }
            else if(strpos($type, 'Apartment') !== false  && $studio=='yes'  && $no_of_bedrooms==2){
                $listingModel->parking_space  = 1;
            }
            else if(strpos($type, 'Apartment') !== false && $no_of_bedrooms>2){
                $listingModel->parking_space  = 2;
            }
            else{
                $listingModel->parking_space  =0;
            }

            //Estiated age working
            if (strpos($type, 'Land') !== false) {
                $listingModel->estimated_age = 0;
            }else{
                if ($model->estimated_age<>null) {
                    $listingModel->estimated_age = $model->estimated_age;
                }else{
                    $listingModel->estimated_age = 0;
                }
            }

            //Floor Number Working
            if (strpos($type, 'Apartment') !== false || strpos($type, 'Office') !== false) {
                if (strpos($propertyDescription, 'high floor') !== false || strpos($propertyDescription, 'higher floor') !== false || strpos($propertyDescription, 'high-floor') !== false || strpos($completeTitle, 'high floor') !== false || strpos($completeTitle, 'higher floor') !== false || strpos($completeTitle, 'high-floor') !== false) {
                    $listingModel->floor_number = 3/4*$model->typical_floors;
                }
                else if (strpos($propertyDescription, 'low floor') !== false || strpos($propertyDescription, 'lower floor') !== false || strpos($propertyDescription, 'low-floor') !== false || strpos($completeTitle, 'low floor') !== false || strpos($completeTitle, 'lower floor') !== false || strpos($completeTitle, 'low-floor') !== false) {
                    $listingModel->floor_number = 1/4*$model->typical_floors;
                }
                else{
                    $listingModel->floor_number = 1/2*$model->typical_floors;
                }
            }else{
                $listingModel->floor_number = 0;
            }


            //No of levels Working
            if (strpos($type, 'Land') !== false || strpos($type, 'Apartment') !== false || strpos($type, 'Warehouse') !== false || strpos($type, 'Office') !== false) {
                $listingModel->number_of_levels = 1;
            }
            else if (strpos($type, 'Villa') !== false || strpos($type, 'Townhouse') !== false || strpos($type, 'Duplex') !== false) {
                if ($model->typical_floors<>null) {
                    $listingModel->number_of_levels = $model->typical_floors;
                }
                else{
                    $listingModel->number_of_levels = 2;
                }
            }
            else{
                if ($model->typical_floors<>null) {
                    $listingModel->number_of_levels = $model->typical_floors;
                }else{
                    $listingModel->number_of_levels = 2;
                }
            }


            //Start bua and land size
            if (strpos($type, 'Apartment') !== false || strpos($type, 'Office') !== false) {
                $listingModel->land_size = '0';
                $listingModel->built_up_area = $MainLandOrBua;
            }
            else{
                // echo "in else hh"; die();
                $landAndBuiltUpArea = Yii::$app->appHelperFunctions->getBuaAndLandArea($propertyDescription,$MainLandOrBua,$no_of_bedrooms);
                // echo "<br>In main function: <pre>"; print_r($landAndBuiltUpArea); echo "</pre>"; die();
                $listingModel->land_size = $landAndBuiltUpArea['land_size'];
                $listingModel->built_up_area = $landAndBuiltUpArea['built_up_area'];
            }
            //End bua and land size

            $listingModel->balcony_size          = '0';
            //End Property Other Information

            //Start Location Attributes
            $listingModel->location_highway_drive = ($model->location_highway_drive<>null) ? $model->location_highway_drive : 'minutes_5';
            $listingModel->location_school_drive  = ($model->location_school_drive<>null) ? $model->location_school_drive : 'minutes_5';
            $listingModel->location_mall_drive    = ($model->location_mall_drive<>null) ? $model->location_mall_drive : 'minutes_5';
            $listingModel->location_sea_drive     = ($model->location_sea_drive<>null) ? $model->location_sea_drive : 'minutes_5';
            $listingModel->location_park_drive    = ($model->location_park_drive<>null) ? $model->location_park_drive : 'minutes_5';
            //End Location Attributes

            //Start View Attributes
            $listingModel->view_community = 'normal';
            if (strpos($propertyDescription, 'view of pool') !== false || strpos($propertyDescription, 'view of fountain') !== false || strpos($propertyDescription, 'fountain view') !== false || strpos($propertyDescription, 'pool view') !== false || strpos($completeTitle, 'view of pool') !== false || strpos($completeTitle, 'view of fountain') !== false || strpos($completeTitle, 'fountain view') !== false || strpos($completeTitle, 'pool view') !== false) {
                $listingModel->view_pool = 'full';
            }
            else{
                $listingModel->view_pool = 'none';
            }
            if (strpos($propertyDescription, 'view of burj') !== false || strpos($propertyDescription, 'burj khalifa view') !== false || strpos($propertyDescription, 'burj view') !== false || strpos($propertyDescription, 'burj al arab view') !== false || strpos($completeTitle, 'view of burj') !== false || strpos($completeTitle, 'burj khalifa view') !== false || strpos($completeTitle, 'burj view') !== false || strpos($completeTitle, 'burj al arab view') !== false) {
                $listingModel->view_burj = 'full';
            }
            else{
                $listingModel->view_burj = 'none';
            }
            if (strpos($propertyDescription, 'sea view') !== false || strpos($propertyDescription, 'view of sea') !== false || strpos($propertyDescription, 'view of water') !== false || strpos($propertyDescription, 'water view') !== false || strpos($completeTitle, 'sea view') !== false || strpos($completeTitle, 'view of sea') !== false || strpos($completeTitle, 'view of water') !== false || strpos($completeTitle, 'water view') !== false) {
                $listingModel->view_sea = 'full';
            }
            else{
                $listingModel->view_sea = 'none';
            }
            if (strpos($propertyDescription, 'marina view') !== false || strpos($propertyDescription, 'view of marina') !== false || strpos($propertyDescription, 'view of dubai marina') !== false || strpos($propertyDescription, 'views of dubai marina') !== false || strpos($propertyDescription, 'views of marina') !== false || strpos($completeTitle, 'marina view') !== false || strpos($completeTitle, 'view of marina') !== false || strpos($completeTitle, 'view of dubai marina') !== false || strpos($completeTitle, 'views of dubai marina') !== false || strpos($completeTitle, 'views of marina') !== false) {
                $listingModel->view_marina = 'full';
            }
            else{
                $listingModel->view_marina = 'none';
            }
            if (strpos($propertyDescription, 'view of mountain') !== false || strpos($propertyDescription, 'mountain view') !== false || strpos($propertyDescription, 'mountains view') !== false || strpos($propertyDescription, 'views of mountain') !== false || strpos($completeTitle, 'view of mountain') !== false || strpos($completeTitle, 'mountain view') !== false || strpos($completeTitle, 'mountains view') !== false || strpos($completeTitle, 'views of mountain') !== false) {
                $listingModel->view_mountains = 'full';
            }
            else{
                $listingModel->view_mountains = 'none';
            }
            if (strpos($propertyDescription, 'view of lake') !== false || strpos($propertyDescription, 'lake view') !== false || strpos($propertyDescription, 'views of lake') !== false || strpos($propertyDescription, 'views of the lake') !== false || strpos($completeTitle, 'view of lake') !== false || strpos($completeTitle, 'lake view') !== false || strpos($completeTitle, 'views of lake') !== false || strpos($completeTitle, 'views of the lake') !== false) {
                $listingModel->view_lake = 'full';
            }
            else{
                $listingModel->view_lake = 'none';
            }
            if (strpos($propertyDescription, 'golf course view') !== false || strpos($propertyDescription, 'view of golf course') !== false || strpos($propertyDescription, 'views of golfcourse') !== false || strpos($propertyDescription, 'views of golf course') !== false || strpos($propertyDescription, 'golfcourse view') !== false || strpos($propertyDescription, 'view of golfcourse') !== false || strpos($propertyDescription, 'view of the golf course') !== false || strpos($propertyDescription, 'views of the golf course') !== false || strpos($propertyDescription, 'golf view') !== false || strpos($propertyDescription, 'view of golf') !== false || strpos($completeTitle, 'golf course view') !== false || strpos($completeTitle, 'view of golf course') !== false || strpos($completeTitle, 'views of golfcourse') !== false || strpos($completeTitle, 'views of golf course') !== false || strpos($completeTitle, 'golfcourse view') !== false || strpos($completeTitle, 'view of golfcourse') !== false || strpos($completeTitle, 'view of the golf course') !== false || strpos($completeTitle, 'views of the golf course') !== false || strpos($completeTitle, 'golf view') !== false || strpos($completeTitle, 'view of golf') !== false) {
                $listingModel->view_golf_course = 'full';
            }
            else{
                $listingModel->view_golf_course = 'none';
            }
            if (strpos($propertyDescription, 'park view') !== false || strpos($propertyDescription, 'view of park') !== false || strpos($propertyDescription, 'view of the park') !== false || strpos($propertyDescription, 'views of park') !== false || strpos($propertyDescription, 'views of the park') !== false || strpos($completeTitle, 'park view') !== false || strpos($completeTitle, 'view of park') !== false || strpos($completeTitle, 'view of the park') !== false || strpos($completeTitle, 'views of park') !== false || strpos($completeTitle, 'views of the park') !== false) {
                $listingModel->view_park = 'full';
            }
            else{
                $listingModel->view_park = 'none';
            }
            $listingModel->view_special = 'none';
            //End View Attributes

            //Start Price Information
            $listing_website_link_image = Yii::$app->appHelperFunctions->SaveBayutImage($listing_website_link);
            $contentData = Yii::$app->appHelperFunctions->getFullPageContent($listing_website_link_image,$MainLandOrBua,$no_of_bedrooms);
            // echo "<pre>"; print_r($contentData); echo "</pre>"; die();

            $listingModel->listings_price             = str_replace(',', '', $listingAndFinalPrice);
            $listingModel->listings_rent              = $contentData['listings_rent'];
            $listingModel->final_price                = str_replace(',', '', $listingAndFinalPrice);
            $listingModel->listing_website_link_image = $listing_website_link_image;
            $listingModel->agent_name                 = $contentData['agent_name'];



            //Listing Property Type Working
            $listingModel->listing_property_type = $contentData['listing_property_type'];

            if (strpos($propertyDescription, 'developer') !== false || strpos($propertyDescription, 'margin') !== false || strpos($propertyDescription, 'developer margin') !== false) {
                $listingModel->developer_margin  = 'Yes';
            }else{
                $listingModel->developer_margin  = 'No';
            }
            //End Price Information

            $listingModel->status = 2;
            if ($listingModel->save()) {
                Yii::$app->db->createCommand()
                    ->update('bayut_detail_page_links', ['status' => 1], ['url'=>$listing_website_link])
                    ->execute();
            }
            if($listingModel->hasErrors()){

                foreach($listingModel->getErrors() as $error){
                    if(count($error)>0){
                        foreach($error as $key=>$val){
                            echo "listing error: ".$val; echo "<br>";
                        }
                        die();
                    }
                }
            }
            // die('in the end of listing');
        }
        else{
            // echo "listing already exists"; die();
        }
        //End Save Listing Transaction
    }

































    public function saveBuilding($data)
    {
        $type                       = $data['type'];
        $city                       = $data['city'];
        $city_id                    = $data['city_id'];
        $community                  = $data['community'];
        $sub_community              = $data['sub_community'];
        $buildingName               = $data['buildingName'];
        $BedBathStudioEtcValues     = $data['BedBathStudioEtcValues'];
        $propertyDescription        = $data['propertyDescription'];
        $referenceNumber            = $data['referenceNumber'];
        $listing_website_link       = $data['listing_website_link'];
        $listingAndFinalPrice       = $data['listingAndFinalPrice'];
        $propertyInformationArr     = $data['propertyInformationArr'];


        //Start save Building
        $model = new Buildings;
        $model->title = $buildingName;
        $property_cats =  Yii::$app->appHelperFunctions->propertiesCategoriesListArr;
        foreach ($property_cats as $key => $value) {
            if ($value==$type) {
                $model->property_category = $key;
            }else{
                $model->property_category = 6;
            }
        }

        $model->property_id = Yii::$app->appHelperFunctions->getPropertyType($type);
        $model->city = $city_id;
        $model->community = $community;
        $model->sub_community = $sub_community;

        if ($city<>null && $city=='Dubai') {
            $model->tenure = 2;
        }
        else{
            $model->tenure = 1;
        }

        $model->developer_id = 1;
        $model->vacancy = 1;
        $model->utilities_connected    = 'Yes';
        $model->development_type    = 'Standard';
        $model->property_condition  = 3;

        if ($type=='Apartment') {
            $model->property_placement = 3.00;
        }
        else if (strpos($propertyDescription, 'corner') !== false) {
            $model->property_placement = 2.00;
        }
        else{
            $model->property_placement = 1.00;
        }

        if ($type=='Shop') {
            $model->property_visibility = 2;
        }
        else{
            $model->property_visibility = 3;
        }

        if ($type=='Apartment') {
            $model->property_exposure = 3;
        }

        else if (strpos($propertyDescription, 'single row') !== false){
            $model->property_exposure = 2;
        }

        else{
            $model->property_exposure = 1;
        }


        if (strpos($propertyDescription, 'private pool') !== false) {
            $model->pool = 'Yes';
        }

        else{
            $model->pool = 'No';
        }

        if (strpos($propertyDescription, 'Gym') !== false) {
            $model->gym = 'Yes';
        }

        else{
            $model->gym = 'No';
        }

        if (strpos($propertyDescription, 'Play area') !== false) {
            $play_area = 'Yes';
        }

        else{
            $play_area = 'No';
        }

        $model->other_facilities = '';
        $model->completion_status = 100.00;

        if (strpos($propertyDescription, 'kitchen') !== false) {
            $model->white_goods = 'Yes';
        }
        else if (strpos($propertyDescription, 'fitted') !== false) {
            $model->white_goods = 'Yes';
        }
        else{
            $model->white_goods = 'No';
        }

        if (strpos($propertyDescription, 'Furnished') !== false) {
            $model->furnished = 'Yes';
        }
        else{
            $model->furnished = 'No';
        }

        if ($type=='Villa' || $type=='Land' || $type=='Townhouse') {
            if (strpos($propertyDescription, 'Furnished') !== false) {
                $model->landscaping = 'Yes';
            }
            else{
                $model->landscaping = 'No';
            }
        }
        else{
            $model->landscaping = 'No';
        }

        $model->location_highway_drive  = 'minutes_5';
        $model->location_school_drive   = 'minutes_5';
        $model->location_mall_drive     = 'minutes_5';
        $model->location_sea_drive      = 'minutes_5';
        $model->location_park_drive     = 'minutes_5';

        if ($model->save()) {
            $data = [
                'model' => $model,
                'type' => $type,
                'city' => $city,
                'community' => $community,
                'sub_community' => $sub_community,
                'referenceNumber' => $referenceNumber,
                'listing_website_link' => $listing_website_link,
                'listingAndFinalPrice' => $listingAndFinalPrice,
                'BedBathStudioEtcValues' => $BedBathStudioEtcValues,
                'propertyDescription' => $propertyDescription,
                'propertyInformationArr' => $propertyInformationArr,
            ];
            // save listing transaction
            $this->SaveListingTransaction($data);
        }
        if($model->hasErrors()){
            foreach($model->getErrors() as $error){
                if(count($error)>0){
                    foreach($error as $key=>$val){
                        //echo "Building error: ".$val; echo "<br>";
                    }
                    die();
                }
            }
        }
    }

}
