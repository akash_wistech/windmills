<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
Use GuzzleHttp\Client;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class InspectionController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */

    public function actionIndex($message = 'hello world')
    {
// Your cron job logic goes here
        // You can use Node.js to run JavaScript code
        $output = [];
        $returnValue = null;
       // exec('node D:\xampp\htdocs\windmills/iploc.php', $output, $returnValue);
        exec('node D:\xampp\htdocs\windmills/js/inspection.js', $output, $returnValue);

        if ($returnValue === 0) {
            echo "Cron job ran successfully\n";
        } else {
            echo "Error running cron job\n";
        }
        $httpClient = new \Goutte\Client();
        $httpClient->request('GET', 'http://local.windmills/site/valuer-lat-long');

    }

    public function actionRunView()
    {
        // Path to the view file you want to run
        $viewFile = '@app/views/site/iploc.php';

        // Render the view file
        $output = $this->renderFile($viewFile);

        // Output the result (you can also log it or perform other actions)
        echo $output;
    }

}
