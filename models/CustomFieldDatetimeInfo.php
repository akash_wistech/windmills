<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordTsBa;

/**
* This is the model class for table "{{%custom_field_datetime_info}}".
*
* @property int $id
* @property string $module_type
* @property int $module_id
* @property int $input_field_id
* @property string $start_time
* @property string $start_org_time
* @property string $end_time
* @property string $end_org_time
* @property string $created_at
* @property int $created_by
* @property string $updated_at
* @property int $updated_by
*/
class CustomFieldDatetimeInfo extends ActiveRecordTsBa
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%custom_field_datetime_info}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['module_type','module_id'],'required'],
      [['created_at','updated_at'],'safe'],
      [['module_id','input_field_id','created_by','updated_by'],'integer'],
      [['module_type','start_time','start_org_time','end_time','end_org_time'],'string'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'module_id' => Yii::t('app', 'Module'),
      'start_time' => Yii::t('app', 'Width'),
      'end_time' => Yii::t('app', 'Height'),
      'created_at' => Yii::t('app', 'Created'),
      'created_by' => Yii::t('app', 'Created By'),
      'updated_at' => Yii::t('app', 'Updated'),
      'updated_by' => Yii::t('app', 'Updated By'),
    ];
  }
}
