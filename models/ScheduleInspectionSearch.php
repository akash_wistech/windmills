<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ScheduleInspection;

/**
 * CommunitiesSearch represents the model behind the search form of `app\models\Communities`.
 */
class ScheduleInspectionSearch extends ScheduleInspection
{
    public $pageSize;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by','contact_phone_no'], 'integer'],
            [['inspection_type', 'valuation_date', 'contact_person_name', 'inspection_date,contact_phone_no', 'land_line_no', 'contact_email','contact_fine'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ScheduleInspection::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'contact_person_name' => $this->contact_person_name,
          //  'contact_phone_no' => $this->contact_phone_no,
           // 'land_line_no' => $this->land_line_no,
           // 'contact_email' => $this->contact_email,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => $this->trashed,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
        ]);
        $query->andFilterWhere(['contact_fine'=> [1]]);

        $query->andFilterWhere(['like', 'contact_person_name', $this->contact_person_name]);
        $query->andFilterWhere(['like', 'contact_phone_no', $this->contact_phone_no]);
        $query->andFilterWhere(['like', 'land_line_no', $this->land_line_no]);
        $query->andFilterWhere(['like', 'contact_email', $this->contact_email]);

        return $dataProvider;
    }
}
