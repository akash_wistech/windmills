<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "valuation_quotation_properties_details".
 *
 * @property int $id
 * @property int|null $valuation_quotation_id
 * @property string|null $building
 * @property string|null $property
 * @property string|null $property_category
 * @property string|null $tenure
 * @property string|null $city
 * @property string|null $community
 * @property string|null $sub_community
 * @property string|null $building_number
 * @property string|null $owner_name
 * @property string|null $plot_number
 * @property string|null $street
 * @property string|null $unit_number
 * @property string|null $floor_number
 * @property string|null $land_size
 * @property string|null $built_up_area
 * @property string|null $valuation_type
 * @property string|null $latitude
 * @property string|null $name
 * @property int|null $phone_number
 * @property string|null $email
 * @property int|null $no_of_floors
 * @property int|null $no_of_units
 * @property int|null $repeat_valuation
 * @property int|null $valuation_date
 * @property int $status
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 */
class ValuationQuotationPropertiesDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'valuation_quotation_properties_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['building', 'property', 'property_category', 'tenure', 'building_number', 'owner_name', 'plot_number', 'street', 'unit_number',
             'floor_number', 'land_size', 'built_up_area', 'latitude', 'longitude', 'no_of_floors', 'no_of_units',
             'repeat_valuation', 'valuation_date','required_documents','date_of_inspection'], 'safe'],
            [['valuation_quotation_id', 'status', 'created_by', 'updated_by', 'deleted_by'], 'safe'],
            [['created_at', 'updated_at', 'deleted_at', 'city', 'community', 'sub_community',], 'safe'],
        ];
    }


    public function getBuildingTitle()
    {
        return $this->hasOne(Buildings::className(), ['id' => 'building']);
    }

    public function getPropertyTitle()
    {
        return $this->hasOne(Properties::className(), ['id' => 'property']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'valuation_quotation_id' => 'Valuation Quotation ID',
            'building' => 'Building',
            'property' => 'Property',
            'property_category' => 'Property Category',
            'tenure' => 'Tenure',
            'city' => 'City',
            'community' => 'Community',
            'sub_community' => 'Sub Community',
            'building_number' => 'Building Number',
            'owner_name' => 'Owner Name',
            'plot_number' => 'Plot Number',
            'street' => 'Street',
            'unit_number' => 'Unit Number',
            'floor_number' => 'Floor Number',
            'land_size' => 'Land Size',
            'built_up_area' => 'Built Up Area',
            'valuation_type' => 'Valuation Type',
            'location' => 'Location',
            'name' => 'Name',
            'phone_number' => 'Phone Number',
            'email' => 'Email',
            'no_of_floors' => 'No Of Floors',
            'no_of_units' => 'No Of Units',
            'repeat_valuation' => 'New / Repeat Valuation',
            'valuation_date' => 'Valuation Date',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'deleted_by' => 'Deleted By',
        ];
    }
}
