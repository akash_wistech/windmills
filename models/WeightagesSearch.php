<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Weightages;

/**
 * WeightagesSearch represents the model behind the search form of `app\models\Weightages`.
 */
class WeightagesSearch extends Weightages
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'property_type', 'emirates', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['location', 'age', 'property_exposure', 'property_placement', 'finishing_status', 'bedrooom', 'view', 'quality', 'floor', 'land_size', 'bua', 'balcony_size', 'furnished', 'upgrades', 'parking', 'pool', 'landscape', 'white_goods', 'utilities_connected', 'tenure', 'number_of_levels', 'property_visibility', 'less_than_1_month', 'less_than_2_month', 'less_than_3_month', 'less_than_4_month', 'less_than_5_month', 'less_than_6_month', 'less_than_7_month', 'less_than_8_month', 'less_than_9_month', 'less_than_10_month', 'less_than_11_month', 'less_than_12_month', 'less_than_2_year', 'less_than_3_year', 'less_than_4_year', 'less_than_5_year', 'less_than_6_year', 'less_than_7_year', 'less_than_8_year', 'less_than_9_year', 'less_than_10_year'], 'number'],
            [['created_at', 'updated_at', 'trashed_at','status_verified'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Weightages::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'property_type' => $this->property_type,
            'emirates' => $this->emirates,
            'location' => $this->location,
            'age' => $this->age,
            'property_exposure' => $this->property_exposure,
            'property_placement' => $this->property_placement,
            'finishing_status' => $this->finishing_status,
            'bedrooom' => $this->bedrooom,
            'view' => $this->view,
            'quality' => $this->quality,
            'floor' => $this->floor,
            'land_size' => $this->land_size,
            'bua' => $this->bua,
            'balcony_size' => $this->balcony_size,
            'furnished' => $this->furnished,
            'upgrades' => $this->upgrades,
            'parking' => $this->parking,
            'pool' => $this->pool,
            'landscape' => $this->landscape,
            'white_goods' => $this->white_goods,
            'utilities_connected' => $this->utilities_connected,
            'tenure' => $this->tenure,
            'number_of_levels' => $this->number_of_levels,
            'property_visibility' => $this->property_visibility,
            'less_than_1_month' => $this->less_than_1_month,
            'less_than_2_month' => $this->less_than_2_month,
            'less_than_3_month' => $this->less_than_3_month,
            'less_than_4_month' => $this->less_than_4_month,
            'less_than_5_month' => $this->less_than_5_month,
            'less_than_6_month' => $this->less_than_6_month,
            'less_than_7_month' => $this->less_than_7_month,
            'less_than_8_month' => $this->less_than_8_month,
            'less_than_9_month' => $this->less_than_9_month,
            'less_than_10_month' => $this->less_than_10_month,
            'less_than_11_month' => $this->less_than_11_month,
            'less_than_12_month' => $this->less_than_12_month,
            'less_than_2_year' => $this->less_than_2_year,
            'less_than_3_year' => $this->less_than_3_year,
            'less_than_4_year' => $this->less_than_4_year,
            'less_than_5_year' => $this->less_than_5_year,
            'less_than_6_year' => $this->less_than_6_year,
            'less_than_7_year' => $this->less_than_7_year,
            'less_than_8_year' => $this->less_than_8_year,
            'less_than_9_year' => $this->less_than_9_year,
            'less_than_10_year' => $this->less_than_10_year,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'trashed_at' => $this->trashed_at,
            'trashed' => $this->trashed,
            'trashed_by' => $this->trashed_by,
            'status_verified' => $this->status_verified,
        ]);

        return $dataProvider;
    }
}
