<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proposal_standard_report".
 *
 * @property int $id
 * @property string|null $payment_method
 * @property string|null $timings
 * @property string|null $valuer_duties_supervision
 * @property string|null $internal_external_status_of_the_valuer
 * @property string|null $previous_involvement_and_conflict_of_interest
 * @property string|null $decleration_of_independence_and_objectivity
 * @property string|null $rics_monitoring
 * @property string|null $rera_monitoring
 * @property string|null $limitions_on_liabity
 * @property string|null $jurisdiction
 * @property string|null $acceptance_of_terms_of_engagement
 * @property string|null $rfs_sos_details
 * @property string|null $scs_sos_details
 * @property string|null $bca_sos_details
 * @property string|null $rica_sos_details
 * @property string|null $ts_sos_details
 * @property string|null $exclutions
 * @property string|null $payment_terms
 * @property string|null $required_documents
 * @property string|null $terms_versus_agreement
 * @property string|null $providing_the_service
 * @property string|null $liability
 * @property string|null $remuneration
 * @property string|null $communication
 * @property string|null $estimates
 * @property string|null $confidentiality
 * @property string|null $termination
 * @property string|null $miscellaneous
 * @property string|null $force_majeure
 * @property string|null $waiver_and_severance
 * @property string|null $no_responsibility
 * @property string|null $no_partnership
 * @property string|null $conflict
 * @property string|null $dispute_resolution
 * @property string|null $complaints_handling_procedure
 * 
 * 
 */
class ProposalBcsReport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proposal_bcs_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[
                'firstpage_content', 'firstpage_footer', 
                'exclusions', 'payment_method', 'payment_terms', 'timings', 'limitions_on_liabity', 'valuer_duties_supervision', 'internal_external_status_of_the_valuer',
                'previous_involvement_and_conflict_of_interest', 'decleration_of_independence_and_objectivity', 'required_documents', 'terms_versus_agreement', 'providing_the_service', 'liability', 'remuneration', 'communication', 'estimates', 'confidentiality', 'termination', 'miscellaneous', 'force_majeure', 'waiver_and_severance', 'no_responsibility', 'no_partnership', 'conflict', 'dispute_resolution', 'complaints_handling_procedure', 'jurisdiction', 'acceptance_of_terms_of_engagement', 'quotation_last_paragraph',
            ], 'safe'],
            [['rfs_sos_details', 'scs_sos_details', 'bca_sos_details', 'rica_sos_details', 'ts_sos_details','err_sos_details','arct_sos_details','rfv_sos_details'], 'safe'],
            [['assumptions_rfs','assumptions_scs','assumptions_bca','assumptions_rica','assumptions_ts','assumptions_err','assumptions_arct','assumptions_rfv'], 'safe'],
            [['status_verified', 'status_verified_at', 'status_verified_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_method' => 'Payment Method',
            'timings' => 'Timings',
            'internal_external_status_of_the_valuer' => 'Internal External Status Of The Surveyor',
            'previous_involvement_and_conflict_of_interest' => 'Previous Involvement And Conflict Of Interest',
            'decleration_of_independence_and_objectivity' => 'Decleration Of Independence And Objectivity',
            'client_acceptance_of_the_valuation_report' => 'Client Acceptance Of The Valuation Report',
            'rics_monitoring' => 'Rics Monitoring',
            'rera_monitoring' => 'Rera Monitoring',
            'limitions_on_liabity' => 'Limitions On Liabity',
            'jurisdiction' => 'Jurisdiction',
            'acceptance_of_terms_of_engagement' => 'Acceptance Of Terms Of Engagement',
            'rfs_sos_details' => 'Reserve Fund Study Details',
            'scs_sos_details' => 'Service Cost Study Details',
            'bca_sos_details' => 'Building Condition Audit Details',
            'rica_sos_details' => 'Rinstatement Cost Details',
            'ts_sos_details' => 'Technical Snagging Details',
            'err_sos_details' => 'Escrow Retention Release Details',
            'arct_sos_details' => 'Asset Register Creation and Tagging Details',
            'rfv_sos_details' => 'Reserve Fund Validation',
            'exclusions' => 'Exclusions',
            'payment_terms' => 'Payment Terms',
            'required_documents' => 'Documentary and information Requirements',
            'assumptions_rfs' => 'Assumptions, limitations and considerations RFS',
            'assumptions_scs' => 'Assumptions, limitations and considerations SCS',
            'assumptions_bca' => 'Assumptions, limitations and considerations BCA',
            'assumptions_rica' => 'Assumptions, limitations and considerations RICA',
            'assumptions_ts' => 'Assumptions, limitations and considerations TS',
            'assumptions_err' => 'Assumptions, limitations and considerations ERR',
            'assumptions_arct' => 'Assumptions, limitations and considerations ARCT',
            'assumptions_rfv' => 'Assumptions, limitations and considerations RFV',
            'terms_versus_agreement' => 'Terms versus Agreement',
            'providing_the_service' => 'Providing the Service',
            'liability' => 'Liability',
            'remuneration' => 'Remuneration',
            'communication' => 'Communication',
            'estimates' => 'Estimates',
            'confidentiality' => 'Confidentiality',
            'termination' => 'Termination',
            'miscellaneous' => 'Miscellaneous',
            'force_majeure' => 'Force Majeure',
            'waiver_and_severance' => 'Waiver and Severance',
            'no_responsibility' => 'No Responsibility',
            'no_partnership' => 'No Partnership',
            'conflict' => 'Conflict',
            'dispute_resolution' => 'Dispute Resolution',
            'complaints_handling_procedure' => 'Complaints Handling Procedure',
        ];
    }

    public function beforeSave($insert){

        
        // verify status reset after edit only something changed
        $isChanged = false;
        $ignoredAttributes = ['updated_at','status_verified_at']; 
        foreach ($this->attributes as $attribute => $value) {
            if (in_array($attribute, $ignoredAttributes)) {
                continue;
            }
            if ($this->getOldAttribute($attribute) != $value) {
                $isChanged = true;
                break;
            }
        }
        if ($isChanged) {
            if($this->status_verified == 1 && $this->getOldAttribute('status_verified') == 1){
                $this->status_verified = 2;
            }elseif ($this->status_verified == 2 && $this->getOldAttribute('status_verified') == 1) {
                $this->status_verified = 1;
            }
        }
        
        return parent::beforeSave($insert);
    }
}
