<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SoldData;

/**
 * SoldDataReasonsSearch represents the model behind the search form of `app\models\SoldData`.
 */
class SoldDataSearch extends SoldData
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['building_title', 'bedrooms', 'bua', 'plot_area', 'unit_number', 'property_type', 'listing_date', 'price', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search_old($params)
    {

        $query = $this->generateQuery($params);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);



        return $dataProvider;
    }
    public function search($params)
    {
        $query = SoldData::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            /*            SoldData::tableName().'.id' => $this->id,*/
            SoldData::tableName().'.building_title' => $this->building_title,
        ]);

        return $dataProvider;
    }
    public function generateQuery($params)
    {
        $this->load($params);
        $query = SoldData::find()
            ->select([
                'DISTINCT(building_title)',
            ])
            ->groupBy('building_title')
            ->asArray();
        $query = SoldData::find()->asArray();

        // grid filtering conditions
        $query->andFilterWhere([
/*            SoldData::tableName().'.id' => $this->id,*/
            SoldData::tableName().'.building_title' => $this->building_title,
        ]);

        return $query;
    }
}
