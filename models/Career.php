<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;
use yii\web\UploadedFile;
use yii\validators\RegularExpressionValidator;

/**
* User model
*
* @property integer $id
* @property integer $user_type
* @property integer $permission_group_id
* @property integer $active_contract_id
* @property integer $active_package_id
* @property string $username
* @property string $firstname
* @property string $lastname
* @property string $email
* @property string $auth_key
* @property string $password_hash
* @property string $image
* @property integer $status
* @property string $start_date
* @property string $end_date
*/
class Career extends ActiveRecordFull
{
  public $file,$oldfile;
  public $allowedImageSize = 1048576;
  public $allowedImageTypes=['jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'];

  public $new_password,$job_title_id,$job_title,$department_id,$department_name;
  public $primary_contact,$mobile,$phone1,$phone2,$country_id,$zone_id,$city,$postal_code,$address,$background_info;
  public $lead_type,$lead_source,$lead_date,$lead_score,$expected_close_date,$close_date,$confidence,$deal_status;
  public $company_name,$manager_id;

  public $gender,$middlename,$nationality,$passport_number,$birth_date,$marital_status,$marriage_date;
  public $no_of_children,$mother_name,$mother_profession,$father_name,$father_profession,$apartment;
  public $street,$community,$ijari,$origin_apartment,$origin_street,$origin_country,$origin_city;
  public $emergency_uae,$emergency_home,$whatsapp_groups,$personal_email,$group_emails;

  public $degree_name, $institute_name, $passing_year, $percentage_obtained, $attested, $degree_attached;
  public $company, $position, $department, $start_date, $end_date, $gross_salary, $leave_reason;
  public $total_experience, $relevant_experience;
  public $ncc, $ncc_breakage_cost, $notice_period, $early_breakage_cost, $visa_type, $visa_emirate, $joining_date, $visa_start_date, $visa_end_date;
  public $driving_license, $hobby, $language,$tech_skills;
  public $ref_fname, $ref_lname, $ref_nationality, $ref_company, $ref_dept, $ref_position, $ref_landline, $ref_mobile, $ref_letter;

  public $basic_salary, $house_allowance, $transport_allowance, $last_gross_salary, $tax_rate, $net_salary , $other_allowance;
  public $windmills_position, $reports_to;
  public $picture, $cv,  $emirates_id, $residence_visa, $work_permit, $last_contract, $offer_letter, $job_description,$kpi;


  public $valuer_qualifications,$valuer_status,$valuer_experience_expertise,$signature_img_name,$signature_file;

  const STATUS_ACTIVE = '1';
  const STATUS_HOLD = '2';
  const STATUS_CANCEL = '3';
  const STATUS_PENDIND = '20';
  const STATUS_DELETED = '0';

  public $before_after_save = 0, $is_save=0;
  public $qualification = [];
  public $experience = [];
  public $reference = [];
  public $kpis = [];
  public $achievement = [];
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%career}}';
  }


  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_type' => Yii::t('app', 'Member Type'),
      'permission_group_id' => Yii::t('app', 'Permission Group'),
      'firstname' => Yii::t('app', 'First Name'),
      'lastname' => Yii::t('app', 'Last Name'),
      'email' => Yii::t('app', 'Email'),
      'new_password' => Yii::t('app', 'Password'),
      'job_title_id' => Yii::t('app', 'Job Title'),
      'job_title' => Yii::t('app', 'Job Title'),
      'department_id' => Yii::t('app', 'Department'),
      'department_name' => Yii::t('app', 'Department'),

      'country_id' => Yii::t('app', 'Country'),
      'zone_id' => Yii::t('app', 'State / Province'),
      'city' => Yii::t('app', 'City'),
      'postal_code' => Yii::t('app', 'Postal Code'),

      'mobile' => Yii::t('app', 'Office Mobile Phone'),
      'phone1' => Yii::t('app', 'Office Landline'),
      'phone2' => Yii::t('app', 'Personal Mobile Phone'),
      'address' => Yii::t('app', 'Address'),
      'background_info' => Yii::t('app', 'Background Info'),

      'image' => Yii::t('app', 'Photo'),
			'manager_id' => Yii::t('app', 'Assign To'),


      'valuer_qualifications' => Yii::t('app', 'Valuer Qualifications'),
      'valuer_status' => Yii::t('app', 'Valuer Status'),
      'valuer_experience_expertise' => Yii::t('app', 'Valuer Experience Expertise'),
      'signature_img_name' => Yii::t('app', 'Signature Image'),
        'quotation_approver' => Yii::t('app', 'Quotation Approver'),

      'middlename' => Yii::t('app', 'Middle Name'),
      'birth_date' => Yii::t('app', 'Date of Birth'),
      
      'origin_apartment' => Yii::t('app', 'Apartment'),
      'origin_street' => Yii::t('app', 'Street'),
      'origin_country' => Yii::t('app', 'Country'),
      'origin_city' => Yii::t('app', 'City'),

      'emergency_uae' => Yii::t('app', 'UAE Emergency Contact'),
      'emergency_home' => Yii::t('app', 'Home Emergency Contact'),
      
    ];
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['email', 'firstname', 'lastname','birth_date','marital_status','no_of_children','gender',
          'origin_country','phone1','origin_city','language','tech_skills','cover_letter'], 'required'],
      [['email','personal_email'],'email'],
    //  ['email','unique'],
      ['password','safe'],
      [[
        'firstname','lastname','email','new_password','job_title','department_name','address','background_info',
        'lead_date','expected_close_date','close_date','valuer_qualifications','valuer_status','valuer_experience_expertise',
        'mobile','phone1','phone2',
      ],'string'],
      [['firstname','lastname','email','mobile','phone1','phone2'], 'trim'],
      [['firstname','lastname','email','mobile','phone1','phone2','client_approver','cover_letter','kpis','achievement','community'], 'safe'],
  /*    [[
        'company_id','department_id','permission_group_id','lead_type','lead_source',
        'lead_score','confidence','deal_status','job_title_id','bilal_contact'
      ], 'integer'],*/
        [[
            'company_id','department_id','permission_group_id','lead_type','lead_source',
            'lead_score','confidence','deal_status','job_title_id','bilal_contact'
        ], 'safe'],
      [['image'], 'file', 'extensions'=>implode(",",$this->allowedImageTypes), 'maxSize' => $this->allowedImageSize, 'tooBig' => 'Image is too big, Maximum allowed size is 1MB'],
     // [['manager_id'],'each','rule'=>['integer']],

/*      ['firstname', 'string', 'max' => 14],
      ['firstname', 'match', 'pattern' => '/^[a-zA-Z\s]+$/', 'message' => 'Only alphanumeric characters and spaces are allowed.'],

      ['lastname', 'string', 'max' => 14],
      ['lastname', 'match', 'pattern' => '/^[a-zA-Z\s]+$/', 'message' => 'Only alphanumeric characters and spaces are allowed.'],
      ['middlename', 'string', 'max' => 14],
      ['middlename', 'match', 'pattern' => '/^[a-zA-Z\s]+$/', 'message' => 'Only alphanumeric characters and spaces are allowed.'],

      ['mother_name', 'string', 'max' => 14],
      ['mother_name', 'match', 'pattern' => '/^[a-zA-Z\s]+$/', 'message' => 'Only alphanumeric characters and spaces are allowed.'],

      ['father_name', 'string', 'max' => 14],
      ['father_name', 'match', 'pattern' => '/^[a-zA-Z\s]+$/', 'message' => 'Only alphanumeric characters and spaces are allowed.'],

      ['phone2', 'string', 'length' => 12],
      ['phone2', 'match', 'pattern' => '/^\d{12}$/', 'message' => 'Please enter a valid 12-digit phone number.'],
      
      ['phone1', 'string', 'length' => 12],
      ['phone1', 'match', 'pattern' => '/^\d{12}$/', 'message' => 'Please enter a valid 12-digit phone number.'],
      
      ['mobile', 'string', 'length' => 12],
      ['mobile', 'match', 'pattern' => '/^\d{12}$/', 'message' => 'Please enter a valid 12-digit phone number.'],
      
      ['emergency_uae', 'string', 'length' => 12],
      ['emergency_uae', 'match', 'pattern' => '/^\d{12}$/', 'message' => 'Please enter a valid 12-digit phone number.'],

      ['emergency_home', 'string', 'length' => 12],
      ['emergency_home', 'match', 'pattern' => '/^\d{12}$/', 'message' => 'Please enter a valid 12-digit phone number.'],*/


      [['image', 'manager_id', 'middlename', 'mother_name', 'father_name', 'phone2','phone1','mobile','emergency_uae','emergency_home' , 'visa_emirate'], 'safe'],
      [['existing_valuation_contact', 'bilal_contact', 'valuation_contact', 'prospect_contact', 'property_owner_contact', 'inquiry_valuations_contact','icai_contact','broker_contact','developer_contact','is_save','linked_in_page','mobile_1','source_of_relationship'], 'safe'],

      [['degree_attached' , 'ref_letter' , 'emirates_id' , 'residence_visa' , 'work_permit' , 'last_contract',
      'offer_letter', 'job_description','kpi'], 'file'],
        
      [['degree_attached' , 'ref_letter' , 'emirates_id' , 'residence_visa' , 'work_permit' , 'last_contract',
      'offer_letter', 'job_description', 'kpi','driving_license'], 'safe'],
    ];
  }

    



  /**
  * return full name
  */
  public function getName()
  {
    return trim($this->firstname.($this->lastname!='' ? ' '.$this->lastname : ''));
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecTitle()
  {
    return $this->name;
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecType()
  {
    return 'career';
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getProfileInfo()
  {
    return $this->hasOne(CareerProfileInfo::className(), ['user_id' => 'id']);
  }
  /*  public function getProfileInfo()
    {
        return $this->hasOne(CareerProfileInfo::className(), ['user_id' => 'id']);
    }*/


  /**
  * @inheritdoc
  */
  public function beforeValidate()
  {
    //Uploading Logo
    if(UploadedFile::getInstance($this, 'image')){
      $this->file = UploadedFile::getInstance($this, 'image');
      // if no file was uploaded abort the upload
      if (!empty($this->file)) {
        $pInfo=pathinfo($this->file->name);
        $ext = $pInfo['extension'];
        if (in_array($ext,$this->allowedImageTypes)) {
          // Check to see if any PHP files are trying to be uploaded
          $content = file_get_contents($this->file->tempName);
          if (preg_match('/\<\?php/i', $content)) {
            $this->addError('image', Yii::t('app', 'Invalid file provided!'));
            return false;
          }else{
            if (filesize($this->file->tempName) <= $this->allowedImageSize) {
              // generate a unique file name
              $this->image = Yii::$app->fileHelperFunctions->generateName().".{$ext}";
            }else{
              $this->addError('image', Yii::t('app', 'File size is too big!'));
              return false;
            }
          }
        }else{
          $this->addError('image', Yii::t('app', 'Invalid file provided!'));
          return false;
        }
      }
    }
    if($this->image==null && $this->oldfile!=null){
      $this->image=$this->oldfile;
    }
    return parent::beforeValidate();
  }


  /**
  * @inheritdoc
  */
  public function afterSave($insert, $changedAttributes)
  {
    $url = $_SERVER['REQUEST_URI'];
    $path = parse_url($url, PHP_URL_PATH);
    $lastSegment = basename($path);

    if($lastSegment == "update-profile")
    {
          //Saving New Job title if its not in db
        if($this->job_title!=null && $this->job_title!=""){
          $jobTitleRow=JobTitle::find()->where(['title'=>trim($this->job_title)])->one();
          if($jobTitleRow==null){
            $jobTitleRow=new JobTitle;
            $jobTitleRow->title=$this->job_title;
            $jobTitleRow->status=1;
            if($jobTitleRow->save()){
              $this->job_title_id=$jobTitleRow->id;
            }
          }else{
            $this->job_title_id=$jobTitleRow->id;
          }
        }
        //Saving New Department if its not in db
        if($this->department_name!=null && $this->department_name!=""){
          $departmentRow=Department::find()->where(['title'=>trim($this->department_name)])->one();
          if($departmentRow==null){
            $departmentRow=new Department;
            $departmentRow->title=$this->department_name;
            $departmentRow->status=1;
            if($departmentRow->save()){
              $this->department_id=$departmentRow->id;
            }
          }else{
            $this->department_id=$departmentRow->id;
          }
        }

        $careerProfileInfo = CareerProfileInfo::find()->where(['user_id'=>$this->id])->one();
        if($careerProfileInfo==null){
          $careerProfileInfo=new CareerProfileInfo;
          $careerProfileInfo->user_id=$this->id;
        }
        //$careerProfileInfo->primary_contact=($this->primary_contact!=null ? $this->primary_contact : 0);
        $careerProfileInfo->primary_contact=($this->primary_contact!=null ? $this->primary_contact : 1);

        if($this->job_title_id!=null)$careerProfileInfo->job_title_id=$this->job_title_id;
        if($this->department_id!=null)$careerProfileInfo->department_id=$this->department_id;
        if($this->mobile!=null)$careerProfileInfo->mobile=$this->mobile;
        if($this->phone1!=null)$careerProfileInfo->phone1=$this->phone1;
        if($this->phone2!=null)$careerProfileInfo->phone2=$this->phone2;
        if($this->address!=null)$careerProfileInfo->address=$this->address;
    }else{
        //Saving New Job title if its not in db
        if($this->job_title!=null && $this->job_title!=""){
          $jobTitleRow=JobTitle::find()->where(['title'=>trim($this->job_title)])->one();
          if($jobTitleRow==null){
            $jobTitleRow=new JobTitle;
            $jobTitleRow->title=$this->job_title;
            $jobTitleRow->status=1;
            if($jobTitleRow->save()){
              $this->job_title_id=$jobTitleRow->id;
            }
          }else{
            $this->job_title_id=$jobTitleRow->id;
          }
        }
        //Saving New Department if its not in db
        if($this->department_name!=null && $this->department_name!=""){
          $departmentRow=Department::find()->where(['title'=>trim($this->department_name)])->one();
          if($departmentRow==null){
            $departmentRow=new Department;
            $departmentRow->title=$this->department_name;
            $departmentRow->status=1;
            if($departmentRow->save()){
              $this->department_id=$departmentRow->id;
            }
          }else{
            $this->department_id=$departmentRow->id;
          }
        }


        $careerProfileInfo = CareerProfileInfo::find()->where(['user_id'=>$this->id])->one();
        if($careerProfileInfo==null){
          $careerProfileInfo=new CareerProfileInfo;
          $careerProfileInfo->user_id=$this->id;
        }
        //$careerProfileInfo->primary_contact=($this->primary_contact!=null ? $this->primary_contact : 0);
        $careerProfileInfo->primary_contact=($this->primary_contact!=null ? $this->primary_contact : 1);

        if($this->job_title_id!=null)$careerProfileInfo->job_title_id=$this->job_title_id;
        if($this->department_id!=null)$careerProfileInfo->department_id=$this->department_id;
        if($this->mobile!=null)$careerProfileInfo->mobile=$this->mobile;
        if($this->phone1!=null)$careerProfileInfo->phone1=$this->phone1;
        if($this->phone2!=null)$careerProfileInfo->phone2=$this->phone2;
        if($this->address!=null)$careerProfileInfo->address=$this->address;

        if($this->country_id!=null)$careerProfileInfo->country_id=$this->country_id;
        if($this->zone_id!=null)$careerProfileInfo->zone_id=$this->zone_id;
        if($this->city!=null)$careerProfileInfo->city=$this->city;
        if($this->postal_code!=null)$careerProfileInfo->postal_code=$this->postal_code;

        if($this->background_info!=null)$careerProfileInfo->background_info=$this->background_info;
        if($this->lead_type!=null)$careerProfileInfo->lead_type=$this->lead_type;
        if($this->lead_source!=null)$careerProfileInfo->lead_source=$this->lead_source;
        if($this->lead_date!=null)$careerProfileInfo->lead_date=$this->lead_date;
        if($this->lead_score!=null)$careerProfileInfo->lead_score=$this->lead_score;
        if($this->expected_close_date!=null)$careerProfileInfo->expected_close_date=$this->expected_close_date;
        if($this->close_date!=null)$careerProfileInfo->close_date=$this->close_date;
        if($this->confidence!=null)$careerProfileInfo->confidence=$this->confidence;
        if($this->deal_status!=null)$careerProfileInfo->deal_status=$this->deal_status;

        if($this->valuer_qualifications!=null)$careerProfileInfo->valuer_qualifications=$this->valuer_qualifications;
        if($this->valuer_status!=null)$careerProfileInfo->valuer_status=$this->valuer_status;
        if($this->valuer_experience_expertise!=null)$careerProfileInfo->valuer_experience_expertise=$this->valuer_experience_expertise;


        if(Yii::$app->request->post()['Career']['gender']!=null)
        {
          $careerProfileInfo->gender=Yii::$app->request->post()['Career']['gender'];
        }else{
          $careerProfileInfo->gender = NULL;
        }

        if(Yii::$app->request->post()['Career']['middlename']!=null)
        {
          $careerProfileInfo->middlename=Yii::$app->request->post()['Career']['middlename'];
        }else{
          $careerProfileInfo->middlename = NULL;
        }

        if(Yii::$app->request->post()['Career']['country_id']!=null)
        {
          $careerProfileInfo->country_id=Yii::$app->request->post()['Career']['country_id'];
        }else{
          $careerProfileInfo->country_id = NULL;
        }

        if(Yii::$app->request->post()['Career']['nationality']!=null)
        {
          $careerProfileInfo->nationality=Yii::$app->request->post()['Career']['nationality'];
        }else{
          $careerProfileInfo->nationality = NULL;
        }

        if(Yii::$app->request->post()['Career']['passport_number']!=null)
        {
          $careerProfileInfo->passport_number=Yii::$app->request->post()['Career']['passport_number'];
        }else{
          $careerProfileInfo->passport_number = NULL;
        }

        if(Yii::$app->request->post()['Career']['birth_date']!=null)
        {
          $careerProfileInfo->birth_date=Yii::$app->request->post()['Career']['birth_date'];
        }else{
          $careerProfileInfo->birth_date = NULL;
        }

        if(Yii::$app->request->post()['Career']['marital_status']!=null)
        {
          $careerProfileInfo->marital_status=Yii::$app->request->post()['Career']['marital_status'];
        }else{
          $careerProfileInfo->marital_status = NULL;
        }

        if(Yii::$app->request->post()['Career']['marriage_date']!=null)
        {
          $careerProfileInfo->marriage_date=Yii::$app->request->post()['Career']['marriage_date'];
        }else{
          $careerProfileInfo->marriage_date = NULL;
        }

        if(Yii::$app->request->post()['Career']['no_of_children']!=null)
        {
          $careerProfileInfo->no_of_children=Yii::$app->request->post()['Career']['no_of_children'];
        }else{
          $careerProfileInfo->no_of_children = NULL;
        }

        if(Yii::$app->request->post()['Career']['mother_name']!=null)
        {
          $careerProfileInfo->mother_name=Yii::$app->request->post()['Career']['mother_name'];
        }else{
          $careerProfileInfo->mother_name = NULL;
        }

        if(Yii::$app->request->post()['Career']['mother_profession']!=null)
        {
          $careerProfileInfo->mother_profession=Yii::$app->request->post()['Career']['mother_profession'];
        }else{
          $careerProfileInfo->mother_profession = NULL;
        }

        if(Yii::$app->request->post()['Career']['father_name']!=null)
        {
          $careerProfileInfo->father_name=Yii::$app->request->post()['Career']['father_name'];
        }else{
          $careerProfileInfo->father_name = NULL;
        }

        if(Yii::$app->request->post()['Career']['father_profession']!=null)
        {
          $careerProfileInfo->father_profession=Yii::$app->request->post()['Career']['father_profession'];
        }else{
          $careerProfileInfo->father_profession = NULL;
        }

        if(Yii::$app->request->post()['Career']['apartment']!=null)
        {
          $careerProfileInfo->apartment=Yii::$app->request->post()['Career']['apartment'];
        }else{
          $careerProfileInfo->apartment = NULL;
        }

        if(Yii::$app->request->post()['Career']['street']!=null)
        {
          $careerProfileInfo->street=Yii::$app->request->post()['Career']['street'];
        }else{
          $careerProfileInfo->street = NULL;
        }

        if(Yii::$app->request->post()['Career']['community']!=null)
        {
          $careerProfileInfo->community=Yii::$app->request->post()['Career']['community'];
        }else{
          $careerProfileInfo->community = NULL;
        }

        if(Yii::$app->request->post()['Career']['city']!=null)
        {
          $careerProfileInfo->city=Yii::$app->request->post()['Career']['city'];
        }else{
          $careerProfileInfo->city = NULL;
        }

        if(Yii::$app->request->post()['Career']['ijari']!=null)
        {
          $careerProfileInfo->ijari=Yii::$app->request->post()['Career']['ijari'];
        }else{
          $careerProfileInfo->ijari = NULL;
        }


        if(Yii::$app->request->post()['Career']['origin_apartment']!=null)
        {
          $careerProfileInfo->origin_apartment=Yii::$app->request->post()['Career']['origin_apartment'];
        }else{
          $careerProfileInfo->origin_apartment = NULL;
        }

        if(Yii::$app->request->post()['Career']['origin_street']!=null)
        {
          $careerProfileInfo->origin_street=Yii::$app->request->post()['Career']['origin_street'];
        }else{
          $careerProfileInfo->origin_street = NULL;
        }

        if(Yii::$app->request->post()['Career']['origin_country']!=null)
        {
          $careerProfileInfo->origin_country=Yii::$app->request->post()['Career']['origin_country'];
        }else{
          $careerProfileInfo->origin_country = NULL;
        }

        if(Yii::$app->request->post()['Career']['origin_city']!=null)
        {
          $careerProfileInfo->origin_city=Yii::$app->request->post()['Career']['origin_city'];
        }else{
          $careerProfileInfo->origin_city = NULL;
        }

        if(Yii::$app->request->post()['Career']['emergency_uae']!=null)
        {
          $careerProfileInfo->emergency_uae=Yii::$app->request->post()['Career']['emergency_uae'];
        }else{
          $careerProfileInfo->emergency_uae = NULL;
        }

        if(Yii::$app->request->post()['Career']['emergency_home']!=null)
        {
          $careerProfileInfo->emergency_home=Yii::$app->request->post()['Career']['emergency_home'];
        }else{
          $careerProfileInfo->emergency_home = NULL;
        }

        if(Yii::$app->request->post()['Career']['whatsapp_groups']!=null)
        {
          $careerProfileInfo->whatsapp_groups= json_encode(Yii::$app->request->post()['Career']['whatsapp_groups']);
        }else{
          $careerProfileInfo->whatsapp_groups = NULL;
        }

        if(Yii::$app->request->post()['Career']['personal_email']!=null)
        {
          $careerProfileInfo->personal_email=Yii::$app->request->post()['Career']['personal_email'];
        }else{
          $careerProfileInfo->personal_email = NULL;
        }


        if(Yii::$app->request->post()['Career']['group_emails']!=null)
        {
          $careerProfileInfo->group_emails = json_encode(Yii::$app->request->post()['Career']['group_emails']);
        }else{
          $careerProfileInfo->group_emails = NULL;
        }

        if(isset(Yii::$app->request->post()['Career']['qualification']) && (Yii::$app->request->post()['Career']['qualification'] != 0 || Yii::$app->request->post()['Career']['qualification'] != "" ) ){
          $qualificationArray = Yii::$app->request->post()['Career']['qualification'];
          $indexes = array_keys($qualificationArray);
          $latestIndex = min($indexes);
            UserDegreeInfoCareer::deleteAll(['user_id' => Yii::$app->request->post()['Career']['qualification'][$latestIndex]['user_id']]);


          foreach (Yii::$app->request->post()['Career']['qualification'] as $key => $data) {

              $user_degree_info = new UserDegreeInfoCareer();
              $user_degree_info->degree_name = $data['degree_name'];
              $user_degree_info->institute_name = $data['institute_name'];
              $user_degree_info->passing_year = $data['passing_year'];
              $user_degree_info->percentage = $data['percentage'];
              $user_degree_info->degree_attested = $data['degree_attested'];
              $user_degree_info->user_id = $this->id;
              $user_degree_info->degree_attachment = $data['degree_attachment'];

              $user_degree_info->save();
             /* echo "<pre>";
              print_r($_FILES);
              print_r(Yii::$app->request->post());
              die;*/
          }
        }


        if(isset(Yii::$app->request->post()['Career']['experience']) && (Yii::$app->request->post()['Career']['experience'] != 0 || Yii::$app->request->post()['Career']['experience'] != "" ) ){
          $expArray = Yii::$app->request->post()['Career']['experience'];
          $indexes = array_keys($expArray);
          $latestIndex = min($indexes);
          UserExperienceCareer::deleteAll(['user_id' => Yii::$app->request->post()['Career']['experience'][$latestIndex]['user_id']]);

          foreach (Yii::$app->request->post()['Career']['experience'] as $key => $data) {
              $user_experience = new UserExperienceCareer();
              $user_experience->company_name = $data['company_name'];
              $user_experience->position = $data['position'];
              $user_experience->department = $data['department'];
              $user_experience->start_date = $data['start_date'];
              $user_experience->end_date = $data['end_date'];
              $user_experience->gross_salary = $data['gross_salary'];
              $user_experience->leave_reason = $data['leave_reason'];
              $user_experience->user_id = $this->id;
              $user_experience->save();
          }
        }

        if(Yii::$app->request->post()['Career']['total_experience']!=null)
        {
          $careerProfileInfo->total_experience=Yii::$app->request->post()['Career']['total_experience'];
        }else{
          $careerProfileInfo->total_experience = NULL;
        }

        if(Yii::$app->request->post()['Career']['relevant_experience']!=null)
        {
          $careerProfileInfo->relevant_experience=Yii::$app->request->post()['Career']['relevant_experience'];
        }else{
          $careerProfileInfo->relevant_experience = NULL;
        }
        if(isset(Yii::$app->request->post()['Career']['achievement']) && (Yii::$app->request->post()['Career']['achievement'] != 0 || Yii::$app->request->post()['Career']['achievement'] != "" ) ){


            VaCareer::deleteAll(['user_id' => $this->id]);

            foreach (Yii::$app->request->post()['Career']['achievement'] as $key => $data) {
                $user_VaCareer = new VaCareer();
                $user_VaCareer->subject = $data['subject'];
                $user_VaCareer->action_made = $data['action_made'];
                $user_VaCareer->trt = $data['trt'];
                $user_VaCareer->percentage = $data['percentage'];
                $user_VaCareer->user_id = $this->id;
                $user_VaCareer->save();
            }
        }

        if(isset(Yii::$app->request->post()['Career']['kpis']) && (Yii::$app->request->post()['Career']['kpis'] != 0 || Yii::$app->request->post()['Career']['kpis'] != "" ) ){


            TrtCareer::deleteAll(['user_id' => $this->id]);

            foreach (Yii::$app->request->post()['Career']['kpis'] as $key => $data) {
                $user_TrtCareer = new TrtCareer();
                $user_TrtCareer->subject = $data['subject'];
                $user_TrtCareer->action_made = $data['action_made'];
                $user_TrtCareer->trt = $data['trt'];
                $user_TrtCareer->percentage = $data['percentage'];
                $user_TrtCareer->user_id = $this->id;
                $user_TrtCareer->save();
            }
        }

        if(Yii::$app->request->post()['Career']['ncc']!=null)
        {
          $careerProfileInfo->ncc=Yii::$app->request->post()['Career']['ncc'];
        }else{
          $careerProfileInfo->ncc = NULL;
        }

        if(Yii::$app->request->post()['Career']['ncc_breakage_cost']!=null)
        {
          $careerProfileInfo->ncc_breakage_cost=Yii::$app->request->post()['Career']['ncc_breakage_cost'];
        }else{
          $careerProfileInfo->ncc_breakage_cost = NULL;
        }

        if(Yii::$app->request->post()['Career']['notice_period']!=null)
        {
          $careerProfileInfo->notice_period=Yii::$app->request->post()['Career']['notice_period'];
        }else{
          $careerProfileInfo->notice_period = NULL;
        }

        if(Yii::$app->request->post()['Career']['early_breakage_cost']!=null)
        {
          $careerProfileInfo->early_breakage_cost=Yii::$app->request->post()['Career']['early_breakage_cost'];
        }else{
          $careerProfileInfo->early_breakage_cost = NULL;
        }

        if(Yii::$app->request->post()['Career']['visa_type']!=null)
        {
          $careerProfileInfo->visa_type=Yii::$app->request->post()['Career']['visa_type'];
        }else{
          $careerProfileInfo->visa_type = NULL;
        }

        if( Yii::$app->request->post()['Career']['visa_emirate']!=null )
        {
          $careerProfileInfo->visa_emirate=Yii::$app->request->post()['Career']['visa_emirate'];
        }else{
          $careerProfileInfo->visa_emirate = NULL;
        }

        if(Yii::$app->request->post()['Career']['joining_date']!=null)
        {
          $careerProfileInfo->joining_date=Yii::$app->request->post()['Career']['joining_date'];
        }else{
          $careerProfileInfo->joining_date = NULL;
        }

        if(Yii::$app->request->post()['Career']['visa_start_date']!=null)
        {
          $careerProfileInfo->visa_start_date=Yii::$app->request->post()['Career']['visa_start_date'];
        }else{
          $careerProfileInfo->visa_start_date = NULL;
        }

        if(Yii::$app->request->post()['Career']['visa_end_date']!=null)
        {
          $careerProfileInfo->visa_end_date=Yii::$app->request->post()['Career']['visa_end_date'];
        }else{
          $careerProfileInfo->visa_end_date = NULL;
        }


        if(Yii::$app->request->post()['Career']['driving_license']!=null)
        {
          $careerProfileInfo->driving_license=json_encode(Yii::$app->request->post()['Career']['driving_license']);
        }else{
          $careerProfileInfo->driving_license = NULL;
        }

        if(Yii::$app->request->post()['Career']['hobby']!=null)
        {
          $careerProfileInfo->hobby = json_encode(Yii::$app->request->post()['Career']['hobby']);
        }else{
          $careerProfileInfo->hobby = NULL;
        }

        if(Yii::$app->request->post()['Career']['language']!=null)
        {
          $careerProfileInfo->language = json_encode(Yii::$app->request->post()['Career']['language']);
        }else{
          $careerProfileInfo->language = NULL;
        }

        if(Yii::$app->request->post()['Career']['tech_skills']!=null)
        {
            $careerProfileInfo->tech_skills = json_encode(Yii::$app->request->post()['Career']['tech_skills']);
        }else{
            $careerProfileInfo->tech_skills = NULL;
        }

        if(isset(Yii::$app->request->post()['Career']['reference']) && (Yii::$app->request->post()['Career']['reference'] != 0 || Yii::$app->request->post()['Career']['reference'] != "" ) ){
          $referenceArray = Yii::$app->request->post()['Career']['reference'];
          $indexes = array_keys($referenceArray);
          $latestIndex = min($indexes);
          UserReferenceCareer::deleteAll(['user_id' => Yii::$app->request->post()['Career']['reference'][$latestIndex]['user_id']]);

          foreach (Yii::$app->request->post()['Career']['reference'] as $key => $data) {

              $user_reference = new UserReferenceCareer();
              $user_reference->first_name = $data['first_name'];
              $user_reference->last_name = $data['last_name'];
              $user_reference->nationality = $data['nationality'];
              $user_reference->company = $data['company'];
              $user_reference->department = $data['department'];
              $user_reference->position = $data['position'];
              $user_reference->office_phone = $data['office_phone'];
              $user_reference->mobile_phone = $data['mobile_phone'];
              $user_reference->user_id = $this->id;
              if( isset($data['ref_attachment']) &&  $data['ref_attachment'] != "" )
              {
                $user_reference->ref_attachment = $data['ref_attachment'];
              }else{
                if( $_FILES['Career']['name']['reference'][$key]["ref_attachment"] != "" ){
                  $user_reference->ref_attachment = $data['ref_attachment'];
                  if( $_FILES['Career']['name']['reference'][$latestIndex]["ref_attachment"] == UPLOAD_ERR_OK) {
  
                    $fileTmpPath = $_FILES['Career']['tmp_name']['reference'][$key]["ref_attachment"];
                    $fileName = $_FILES['Career']['name']['reference'][$key]["ref_attachment"];
                    $fileSize = $_FILES['Career']['size']['reference'][$key]["ref_attachment"];
                    $fileType = $_FILES['Career']['type']['reference'][$key]["ref_attachment"];
                    $uploadDir = 'uploads/hr-uploads/';
  
                    if (!file_exists($uploadDir)) {
                        mkdir($uploadDir, 0777, true);
                    }
                
                    $destPath_1 = $uploadDir . $fileName;
                    if (move_uploaded_file($fileTmpPath, $destPath_1)) {
                        Yii::$app->getSession()->addFlash('success', "Reference Uploaded Successfully");
                    } else {
                        Yii::$app->getSession()->addFlash('error', "Unable to upload Reference");
                    }
                  } else {
                      Yii::$app->getSession()->addFlash('error', "Reference Not Selected");
                  }
  
                  if( isset($destPath_1) )
                  {
                    $user_reference->ref_attachment = $destPath_1;
                  }else{
                  $user_reference->ref_attachment = NULL;
  
                  }
                }
              }
              $user_reference->save();
          }
        }

        if(Yii::$app->request->post()['Career']['basic_salary']!=null)
        {
          $careerProfileInfo->basic_salary=Yii::$app->request->post()['Career']['basic_salary'];
        }else{
          $careerProfileInfo->basic_salary = NULL;
        }

        if(Yii::$app->request->post()['Career']['other_allowance']!=null)
        {
          $careerProfileInfo->other_allowance=Yii::$app->request->post()['Career']['other_allowance'];
        }else{
          $careerProfileInfo->other_allowance = NULL;
        }

        if(Yii::$app->request->post()['Career']['house_allowance']!=null)
        {
          $careerProfileInfo->house_allowance=Yii::$app->request->post()['Career']['house_allowance'];
        }else{
          $careerProfileInfo->house_allowance = NULL;
        }

        if(Yii::$app->request->post()['Career']['transport_allowance']!=null)
        {
          $careerProfileInfo->transport_allowance=Yii::$app->request->post()['Career']['transport_allowance'];
        }else{
          $careerProfileInfo->transport_allowance = NULL;
        }

        if(Yii::$app->request->post()['Career']['last_gross_salary']!=null)
        {
          $careerProfileInfo->last_gross_salary=Yii::$app->request->post()['Career']['last_gross_salary'];
        }else{
          $careerProfileInfo->last_gross_salary = NULL;
        }

        if(Yii::$app->request->post()['Career']['tax_rate']!=null)
        {
          $careerProfileInfo->tax_rate=Yii::$app->request->post()['Career']['tax_rate'];
        }else{
          $careerProfileInfo->tax_rate = NULL;
        }

        if(Yii::$app->request->post()['Career']['net_salary']!=null)
        {
          $careerProfileInfo->net_salary=Yii::$app->request->post()['Career']['net_salary'];
        }else{
          $careerProfileInfo->net_salary = NULL;
        }

        if(Yii::$app->request->post()['Career']['windmills_position']!=null)
        {
          $careerProfileInfo->windmills_position=Yii::$app->request->post()['Career']['windmills_position'];
        }else{
          $careerProfileInfo->windmills_position = NULL;
        }

        if(Yii::$app->request->post()['Career']['reports_to']!=null)
        {
          $careerProfileInfo->reports_to=Yii::$app->request->post()['Career']['reports_to'];
        }else{
          $careerProfileInfo->reports_to = NULL;
        }

        if(Yii::$app->user->identity->user_type == 10 || Yii::$app->user->identity->user_type == 20)
        {

          if (isset($_FILES["Career"]["error"]["picture"]) && $_FILES["Career"]["error"]["picture"] == UPLOAD_ERR_OK) {
            // Get file details
            $fileTmpPath = $_FILES["Career"]["tmp_name"]["picture"];
            $fileName = $_FILES["Career"]["name"]["picture"];
            $fileSize = $_FILES["Career"]["size"]["picture"];
            $fileType = $_FILES["Career"]["type"]["picture"];
        
            // Specify the directory where you want to save the uploaded file
            $uploadDir = 'uploads/hr-uploads/';
        
            // Create the directory if it doesn't exist
            if (!file_exists($uploadDir)) {
                mkdir($uploadDir, 0777, true);
            }
        
            // Move the uploaded file to the specified directory
            $destPath_pic = $uploadDir . $fileName;
            if (move_uploaded_file($fileTmpPath, $destPath_pic)) {
                Yii::$app->getSession()->addFlash('success', "Picture Uploaded Successfully");
            } else {
                Yii::$app->getSession()->addFlash('error', "Unable to upload Picture");
            }
          } else {
              Yii::$app->getSession()->addFlash('error', "Picture Not Selected");
          }
      
          if(isset($_FILES["Career"]["error"]["picture"]))
          {
            if( isset($destPath_pic) )
            {
                $careerProfileInfo->picture = $destPath_pic;
            }
          }else{
            $careerProfileInfo->picture = NULL;
          }
      
      
          if (isset($_FILES["Career"]["error"]["cv"]) && $_FILES["Career"]["error"]["cv"] == UPLOAD_ERR_OK) {
            // Get file details
            $fileTmpPath = $_FILES["Career"]["tmp_name"]["cv"];
            $fileName = $_FILES["Career"]["name"]["cv"];
            $fileSize = $_FILES["Career"]["size"]["cv"];
            $fileType = $_FILES["Career"]["type"]["cv"];
        
            // Specify the directory where you want to save the uploaded file
            $uploadDir = 'uploads/hr-uploads/';
        
            // Create the directory if it doesn't exist
            if (!file_exists($uploadDir)) {
                mkdir($uploadDir, 0777, true);
            }
        
            // Move the uploaded file to the specified directory
            $destPath_2 = $uploadDir . $fileName;
            if (move_uploaded_file($fileTmpPath, $destPath_2)) {
                Yii::$app->getSession()->addFlash('success', "CV Uploaded Successfully");
            } else {
                Yii::$app->getSession()->addFlash('error', "Unable to upload CV");
            }
          } else {
              Yii::$app->getSession()->addFlash('error', "CV Not Selected");
          }
      
          if(isset($_FILES["Career"]["error"]["cv"]))
          {
            if( isset($destPath_2) )
            {
                $careerProfileInfo->cv = $destPath_2;
            }
          }else{
            $careerProfileInfo->cv = NULL;
          }

      
         /* if(isset($_FILES["Career"]["error"]["cover_letter"]))
          {
            if( isset($destPath_3) )
            {
                $careerProfileInfo->cover_letter = $destPath_3;
            }
          }else{
            $careerProfileInfo->cover_letter = NULL;
          }*/
      

      
          if(isset($_FILES["Career"]["error"]["emirates_id"]))
          {
            if( isset($destPath_4) )
            {
                $careerProfileInfo->emirates_id = $destPath_4;
            }
          }else{
            $careerProfileInfo->emirates_id = NULL;
          }
      
          if (isset($_FILES["Career"]["error"]["residence_visa"]) && $_FILES["Career"]["error"]["residence_visa"] == UPLOAD_ERR_OK) {
            // Get file details
            $fileTmpPath = $_FILES["Career"]["tmp_name"]["residence_visa"];
            $fileName = $_FILES["Career"]["name"]["residence_visa"];
            $fileSize = $_FILES["Career"]["size"]["residence_visa"];
            $fileType = $_FILES["Career"]["type"]["residence_visa"];
        
            // Specify the directory where you want to save the uploaded file
            $uploadDir = 'uploads/hr-uploads/';
        
            // Create the directory if it doesn't exist
            if (!file_exists($uploadDir)) {
                mkdir($uploadDir, 0777, true);
            }
        
            // Move the uploaded file to the specified directory
            $destPath_5 = $uploadDir . $fileName;
            if (move_uploaded_file($fileTmpPath, $destPath_5)) {
                Yii::$app->getSession()->addFlash('success', "residence_visa Uploaded Successfully");
            } else {
                Yii::$app->getSession()->addFlash('error', "Unable to upload residence_visa");
            }
          } else {
              Yii::$app->getSession()->addFlash('error', "residence_visa Not Selected");
          }
      
          if(isset($_FILES["Career"]["error"]["residence_visa"]))
          {
            if( isset($destPath_5) )
            {
                $careerProfileInfo->residence_visa = $destPath_5;
            }
          }else{
            $careerProfileInfo->residence_visa = NULL;
          }
      
          if (isset($_FILES["Career"]["error"]["work_permit"]) && $_FILES["Career"]["error"]["work_permit"] == UPLOAD_ERR_OK) {
            // Get file details
            $fileTmpPath = $_FILES["Career"]["tmp_name"]["work_permit"];
            $fileName = $_FILES["Career"]["name"]["work_permit"];
            $fileSize = $_FILES["Career"]["size"]["work_permit"];
            $fileType = $_FILES["Career"]["type"]["work_permit"];
        
            // Specify the directory where you want to save the uploaded file
            $uploadDir = 'uploads/hr-uploads/';
        
            // Create the directory if it doesn't exist
            if (!file_exists($uploadDir)) {
                mkdir($uploadDir, 0777, true);
            }
        
            // Move the uploaded file to the specified directory
            $destPath_6 = $uploadDir . $fileName;
            if (move_uploaded_file($fileTmpPath, $destPath_6)) {
                Yii::$app->getSession()->addFlash('success', "work_permit Uploaded Successfully");
            } else {
                Yii::$app->getSession()->addFlash('error', "Unable to upload work_permit");
            }
          } else {
              Yii::$app->getSession()->addFlash('error', "work_permit Not Selected");
          }
      
          if(isset($_FILES["Career"]["error"]["work_permit"]))
          {
            if( isset($destPath_6) )
            {
                $careerProfileInfo->work_permit = $destPath_6;
            }
          }else{
            $careerProfileInfo->work_permit = NULL;
          }
      
          if (isset($_FILES["Career"]["error"]["last_contract"]) && $_FILES["Career"]["error"]["last_contract"] == UPLOAD_ERR_OK) {
            // Get file details
            $fileTmpPath = $_FILES["Career"]["tmp_name"]["last_contract"];
            $fileName = $_FILES["Career"]["name"]["last_contract"];
            $fileSize = $_FILES["Career"]["size"]["last_contract"];
            $fileType = $_FILES["Career"]["type"]["last_contract"];
        
            // Specify the directory where you want to save the uploaded file
            $uploadDir = 'uploads/hr-uploads/';
        
            // Create the directory if it doesn't exist
            if (!file_exists($uploadDir)) {
                mkdir($uploadDir, 0777, true);
            }
        
            // Move the uploaded file to the specified directory
            $destPath_7 = $uploadDir . $fileName;
            if (move_uploaded_file($fileTmpPath, $destPath_7)) {
                Yii::$app->getSession()->addFlash('success', "last_contract Uploaded Successfully");
            } else {
                Yii::$app->getSession()->addFlash('error', "Unable to upload last_contract");
            }
          } else {
              Yii::$app->getSession()->addFlash('error', "last_contract Not Selected");
          }
      
          if(isset($_FILES["Career"]["error"]["last_contract"]))
          {
            if( isset($destPath_7) )
            {
                $careerProfileInfo->last_contract = $destPath_7;
            }
          }else{
            $careerProfileInfo->last_contract = NULL;
          }
      
          if (isset($_FILES["Career"]["error"]["offer_letter"]) && $_FILES["Career"]["error"]["offer_letter"] == UPLOAD_ERR_OK) {
            // Get file details
            $fileTmpPath = $_FILES["Career"]["tmp_name"]["offer_letter"];
            $fileName = $_FILES["Career"]["name"]["offer_letter"];
            $fileSize = $_FILES["Career"]["size"]["offer_letter"];
            $fileType = $_FILES["Career"]["type"]["offer_letter"];
        
            // Specify the directory where you want to save the uploaded file
            $uploadDir = 'uploads/hr-uploads/';
        
            // Create the directory if it doesn't exist
            if (!file_exists($uploadDir)) {
                mkdir($uploadDir, 0777, true);
            }
        
            // Move the uploaded file to the specified directory
            $destPath_8 = $uploadDir . $fileName;
            if (move_uploaded_file($fileTmpPath, $destPath_8)) {
                Yii::$app->getSession()->addFlash('success', "offer_letter Uploaded Successfully");
            } else {
                Yii::$app->getSession()->addFlash('error', "Unable to upload offer_letter");
            }
          } else {
              Yii::$app->getSession()->addFlash('error', "offer_letter Not Selected");
          }
      
          if(isset($_FILES["Career"]["error"]["offer_letter"]))
          {
            if( isset($destPath_8) )
            {
                $careerProfileInfo->offer_letter = $destPath_8;
            }
          }else{
            $careerProfileInfo->offer_letter = NULL;
          }
      
        }



    }

    
    



      
      if($this->is_save == 0){
        // dd($this->is_save);
        if(!$careerProfileInfo->save()){
            echo "<pre>";
            print_r($careerProfileInfo->errors);
            die;
          // echo "careerProfileInfo user model";
          // dd($careerProfileInfo);
        }else{
          
          if ($careerProfileInfo->hasErrors()) {
            foreach ($careerProfileInfo->getErrors() as $error) {
                if (count($error) > 0) {
                    foreach ($error as $key => $val) {
                        // echo $val."<br>";
                        Yii::$app->getSession()->addFlash('error', $val);
                    }
                }
            }
            // die();
          }
    
        }
      }


		//Saving Managers
		if($this->manager_id!=null){
			ContactManager::deleteAll(['and',['contact_id'=>$this->id],['not in','staff_id',$this->manager_id]]);
			foreach($this->manager_id as $key=>$val){
				$managerRow=ContactManager::find()->where(['contact_id'=>$this->id,'staff_id'=>$val])->one();
				if($managerRow==null){
					$managerRow=new ContactManager;
					$managerRow->contact_id=$this->id;
					$managerRow->staff_id=$val;
					$managerRow->save();
				}
			}
		}
    $managerRow=ContactManager::find()->where(['contact_id'=>$this->id,'staff_id'=>$this->created_by]);
    if(!$managerRow->exists()){
      $managerRow=new ContactManager;
      $managerRow->contact_id=$this->id;
      $managerRow->staff_id=$this->created_by;
      $managerRow->save();
    }

    if ($this->file!== null && $this->image!=null) {
      if($this->oldfile!=null && $this->image!=$this->oldfile && file_exists(Yii::$app->fileHelperFunctions->memberImagePath['abs'].$this->oldfile)){
        unlink(Yii::$app->fileHelperFunctions->memberImagePath['abs'].$this->oldfile);
      }
      $this->file->saveAs(Yii::$app->fileHelperFunctions->memberImagePath['abs'].$this->image);

    }
   // die('here aftersave');
     parent::afterSave($insert, $changedAttributes);
  }



	/**
	* Get Assigned Staff members
	* @return \yii\db\ActiveQuery
	*/
	public function getManagerIdz()
	{
		return ContactManager::find()->select(['staff_id'])->where(['contact_id'=>$this->id])->asArray()->all();
	}
}