<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use app\models\SoldDataUpgrade;

/**
 * SoldTransactionImportForm is the model behind the sold transaction import form.
 */
class SoldTransactionImportFormBucket extends Model
{
    public $importfile;
    public $allowedFileTypes = ['csv'];//'xls', 'xlsx',

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['importfile'], 'safe'],
            [['importfile'], 'file', 'extensions' => implode(",", $this->allowedFileTypes)]
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'importfile' => Yii::t('app', 'File'),
        ];
    }

    /**
     * import the entries into table
     */
    public function save()
    {

        $saved = 0;
        $unsaved = 0;
        $errNames = '';
        $change = array();
        if ($this->validate()) {
           // ini_set('max_execution_time', '0');
            if (UploadedFile::getInstance($this, 'importfile')) {
                $importedFile = UploadedFile::getInstance($this, 'importfile');

                // if no image was uploaded abort the upload
                if (!empty($importedFile)) {
                    $pInfo = pathinfo($importedFile->name);

                    $ext = $pInfo['extension'];

                    if (in_array($ext, $this->allowedFileTypes)) {
                        // Check to see if any PHP files are trying to be uploaded
                        $content = file_get_contents($importedFile->tempName);

                        if (preg_match('/\<\?php/i', $content)) {

                        } else {
                            $this->importfile = Yii::$app->params['temp_abs_path'] . $importedFile->name;
                            $importedFile->saveAs($this->importfile);

                            $csvFile = fopen($this->importfile, 'r');

                            $data = [];
                            $n = 1;
                            while (($line = fgetcsv($csvFile)) !== FALSE) {


                                $transaction_type = $line[0];
                                $sub_type = $line[1];
                                $sales_sequence = $line[2];
                                $reidin_ref_number = $line[3];
                                $date = str_replace('/', '-', trim($line[4]));
                                $date = date("Y-m-d", strtotime($date));
                                //$date = trim($line[4]);
                                $reidin_community = $line[5];
                                $buildingName = $line[6];
                                $reidin_property_type = $line[7];
                                $unit_number = $line[8];
                                $needle = '-';


                                if (strpos($unit_number, $needle) !== false) {
                                    $explode_unit_value = explode('-', $unit_number);
                                    if($explode_unit_value <> null && count($explode_unit_value) > 0){
                                        $unit_number = str_replace($explode_unit_value[0].'-', "", $unit_number);
                                        $unit_number= trim($unit_number);
                                    }
                                }

                                $needle_bedroom = 'B/R';
                                if (strpos($line[9], $needle_bedroom) !== false) {
                                    $berooms = explode(' B/R', trim($line[9]));
                                    $rooms = trim($berooms[0]);
                                }else{
                                    $rooms = null;
                                }

                               /* $berooms = explode(' B/R', trim($line[9]));
                                $rooms = trim($berooms[0]);*/

                                $floor_number = $line[10];
                                if ($floor_number == 'G') {
                                    $floor_number = 0;
                                }
                                $parking_spaces = $line[16];
                                $balcony_size = $line[17];

                               // $bua = str_replace(",", "", $line[13]);
                                $bua = str_replace(",", "", $line[11]);
                               // $plotArea = str_replace(",", "", $line[14]);
                                $plotArea = str_replace(",", "", $line[12]);
                                $price = str_replace(",", "", $line[13]);
                                $pricePerSqt = str_replace(",", "", $line[14]);
                                $reidin_developer = $line[18];


                                if ($n > 1)
                                {
                                    $sold_record= new DailySoldsImport();

                                    $sold_record->transaction_type = $transaction_type;
                                    $sold_record->subtype = $sub_type;
                                    $sold_record->sales_sequence = $sales_sequence;
                                    $sold_record->reidin_ref = $reidin_ref_number;
                                    $sold_record->transaction_date = $date;
                                    $sold_record->transaction_type = $transaction_type;
                                    $sold_record->community = $reidin_community;
                                    $sold_record->property = $buildingName;
                                    $sold_record->property_type = $reidin_property_type;
                                    $sold_record->unit = $unit_number;
                                    $sold_record->bedrooms = $rooms;
                                    $sold_record->floor = $floor_number;
                                    $sold_record->parking = $parking_spaces;
                                    $sold_record->balcony_area = $balcony_size;
                                    $sold_record->size_sqf = $bua;
                                    $sold_record->land_size = $plotArea;
                                    $sold_record->amount = $price;
                                    $sold_record->sqf = $pricePerSqt;
                                    $sold_record->developer = $reidin_developer;
                                    $sold_record->status = 0;
                                    $sold_record->created_at = date('Y-m-d h:i:s');
                                    $sold_record->created_by =  Yii::$app->user->identity->id;
                                    if(!$sold_record->save()){
                                        echo "<pre>";
                                        print_r($sold_record->errors);
                                        die;
                                    }else{
                                        $saved=1;
                                    }

                                }
                                $n++;
                            }

                            fclose($csvFile);
                            //Unlink File
                            unlink($this->importfile);
                        }
                    }
                }
            }


            if ($saved > 0) {

                Yii::$app->getSession()->setFlash('success', $saved . ' - ' . Yii::t('app', 'rows saved successfully'));
            }
            if ($unsaved > 0) {
                Yii::$app->getSession()->setFlash('error', $unsaved . ' - ' . Yii::t('app', 'rows were not saved!') . $errNames);
            }
            return true;
        } else {
            return false;
        }
    }
}
