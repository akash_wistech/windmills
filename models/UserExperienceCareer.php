<?php

namespace app\models;

use Yii;

class UserExperienceCareer extends \yii\db\ActiveRecord
{
    public $status_verified;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_experience_career';
    }
}
