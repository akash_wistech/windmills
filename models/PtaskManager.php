<?php

namespace app\models;

use Yii;

// dd($_REQUEST);

class PtaskManager extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ptask_manager';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['modification', 'description', 'module', 'frequency', 'priority', 'start_date', 'end_date','completed_date','meeting_date','entered_by','status','agreed_by','expiry_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'modification' => 'modification',
            'description' => 'Description',
            'module' => 'module',
            'frequency' => 'frequency',
            'priority' => 'priority',
            'start_date' => 'start_date',
            'end_date' => 'end_date',
            'completed_date' => 'completed_date',
            'meeting_date' => 'meeting_date',
            'entered_by' => 'entered_by',
            'status' => 'status',
            'expiry_date' => 'expiry_date',
        ];
    }
}
