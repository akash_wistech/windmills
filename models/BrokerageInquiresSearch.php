<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BrokerageInquires;

/**
 * BrokerageInquiresSearch represents the model behind the search form of `app\models\BrokerageInquires`.
 */
class BrokerageInquiresSearch extends BrokerageInquires
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'inquiry_type', 'inquiry_importance', 'purpose_of_brokerage_service', 'building_info', 'property_id', 'property_category', 'community', 'sub_community', 'tenure', 'number_bed_rooms', 'city', 'vacant', 'eviction_notice_served', 'parking_spaces', 'listing_permitted', 'leasing_service_required', 'facility_management_service_required', 'property_management_service_required', 'property_status', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['reference_number', 'client_segment', 'referred_by', 'inquiry_date', 'vacancy_date', 'expected_time_of_buying', 'expected_exit_period', 'created_at', 'updated_at', 'trashed_at','referred_by','desired_rice_rent_amount','deal_possibility'], 'safe'],
            [['current_rent_amount', 'service_charges', 'land_size', 'target_investment', 'preferred_return_of_investment', 'cash_mortgage_buyer'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BrokerageInquires::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'inquiry_date' => $this->inquiry_date,
            'inquiry_type' => $this->inquiry_type,
            'deal_possibility' => $this->deal_possibility,
            'inquiry_importance' => $this->inquiry_importance,
            'purpose_of_brokerage_service' => $this->purpose_of_brokerage_service,
            'building_info' => $this->building_info,
            'property_id' => $this->property_id,
            'property_category' => $this->property_category,
            'community' => $this->community,
            'sub_community' => $this->sub_community,
            'tenure' => $this->tenure,
            'number_bed_rooms' => $this->number_bed_rooms,
            'city' => $this->city,
            'vacant' => $this->vacant,
            'vacancy_date' => $this->vacancy_date,
            'eviction_notice_served' => $this->eviction_notice_served,
            'current_rent_amount' => $this->current_rent_amount,
            'service_charges' => $this->service_charges,
            'parking_spaces' => $this->parking_spaces,
            'listing_permitted' => $this->listing_permitted,
            'land_size' => $this->land_size,
            'target_investment' => $this->target_investment,
            'preferred_return_of_investment' => $this->preferred_return_of_investment,
            'cash_mortgage_buyer' => $this->cash_mortgage_buyer,
            'expected_time_of_buying' => $this->expected_time_of_buying,
            'expected_exit_period' => $this->expected_exit_period,
            'leasing_service_required' => $this->leasing_service_required,
            'facility_management_service_required' => $this->facility_management_service_required,
            'property_management_service_required' => $this->property_management_service_required,
            'property_status' => $this->property_status,
            'desired_rice_rent_amount' => $this->desired_rice_rent_amount,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => $this->trashed,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
        ]);

        $query->andFilterWhere(['like', 'reference_number', $this->reference_number])
            ->andFilterWhere(['like', 'client_segment', $this->client_segment])
            ->andFilterWhere(['like', 'referred_by', $this->referred_by]);

        return $dataProvider;
    }
}
