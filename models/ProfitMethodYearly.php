<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profit_method_yearly".
 *
 * @property int $id
 * @property float|null $number_of_rooms
 * @property float|null $rooms_revenue_percentage
 * @property float|null $days_per_year
 * @property float|null $nights_available
 * @property float|null $occupancy
 * @property float|null $occupied_nights_per_year
 * @property float|null $adr
 * @property float|null $rev_par
 * @property float|null $rooms_revenue
 * @property float|null $f_and_b_revenue
 * @property float|null $f_and_b_revenue_percentage
 * @property float|null $net_rental_income
 * @property float|null $net_rental_income_percentage
 * @property float|null $total_revenue
 * @property float|null $total_revenue_percentage
 * @property float|null $rooms
 * @property float|null $rooms_percentage
 * @property float|null $f_and_b
 * @property float|null $f_and_b_percentage
 * @property float|null $other_operating_departments
 * @property float|null $other_operating_departments_percentage
 * @property float|null $net_rental_income_exp
 * @property float|null $net_rental_income_exp_percentage
 * @property float|null $total_costs_and_expenses_percentage
 * @property float|null $total_operating_dept_income
 * @property float|null $total_operating_dept_income_percentage
 * @property float|null $total_costs_and_expenses
 * @property float|null $admin_and_general
 * @property float|null $admin_and_general_percentage
 * @property float|null $information_and_communication
 * @property float|null $information_and_communication_percentage
 * @property float|null $marketing
 * @property float|null $marketing_percentage
 * @property float|null $pom
 * @property float|null $pom_percentage
 * @property float|null $energy
 * @property float|null $energy_percentage
 * @property float|null $total_undistributed_expenses
 * @property float|null $total_undistributed_expenses_percentage
 * @property float|null $gross_operating_profit
 * @property float|null $gross_operating_profit_percentage
 * @property float|null $management_fee_base
 * @property float|null $management_fee_base_percentage
 * @property float|null $adjusted_gross_profit
 * @property float|null $adjusted_gross_profit_percentage
 * @property float|null $rental_expenses
 * @property float|null $rental_expenses_percentage
 * @property float|null $trade_license_fee
 * @property float|null $trade_license_fee_percentage
 * @property float|null $insurance
 * @property float|null $insurance_percentage
 * @property float|null $capacity_charges
 * @property float|null $capacity_charges_percentage
 * @property float|null $master_community_charges
 * @property float|null $master_community_charges_percentage
 * @property float|null $incentive_fee_as_based_on_performance
 * @property float|null $incentive_fee_as_based_on_performance_percentage
 * @property float|null $other_expenses
 * @property float|null $other_expenses_percentage
 * @property float|null $total_fixed_expenses
 * @property float|null $total_fixed_expenses_percentage
 * @property float|null $leasehold_rent
 * @property float|null $leasehold_rent_percentage
 * @property float|null $reserve_for_replacement
 * @property float|null $reserve_for_replacement_percentage
 * @property float|null $total_a_b
 * @property float|null $total_a_b_percentage
 * @property float|null $rent_expeneses_fhp
 * @property float|null $rent_expeneses_fhp_percentage
 * @property float|null $asset_management_fee
 * @property float|null $asset_management_fee_percentage
 * @property float|null $ebida_noi
 * @property float|null $ebida_noi_percentage
 * @property float|null $pv_of_noi
 * @property float|null $pv_of_noi_percentage
 * @property int $valuation_id
 * @property int|null $year_number
 */
class ProfitMethodYearly extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profit_method_yearly';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number_of_rooms', 'rooms_revenue_percentage', 'days_per_year', 'nights_available', 'occupancy', 'occupied_nights_per_year', 'adr', 'rev_par', 'rooms_revenue', 'f_and_b_revenue', 'f_and_b_revenue_percentage', 'net_rental_income', 'net_rental_income_percentage', 'total_revenue', 'total_revenue_percentage', 'rooms', 'rooms_percentage', 'f_and_b', 'f_and_b_percentage', 'other_operating_departments', 'other_operating_departments_percentage', 'net_rental_income_exp', 'net_rental_income_exp_percentage', 'total_costs_and_expenses_percentage', 'total_operating_dept_income', 'total_operating_dept_income_percentage', 'total_costs_and_expenses', 'admin_and_general', 'admin_and_general_percentage', 'information_and_communication', 'information_and_communication_percentage', 'marketing', 'marketing_percentage', 'pom', 'pom_percentage', 'energy', 'energy_percentage', 'total_undistributed_expenses', 'total_undistributed_expenses_percentage', 'gross_operating_profit', 'gross_operating_profit_percentage', 'management_fee_base', 'management_fee_base_percentage', 'adjusted_gross_profit', 'adjusted_gross_profit_percentage', 'rental_expenses', 'rental_expenses_percentage', 'trade_license_fee', 'trade_license_fee_percentage', 'insurance', 'insurance_percentage', 'capacity_charges', 'capacity_charges_percentage', 'master_community_charges', 'master_community_charges_percentage', 'incentive_fee_as_based_on_performance', 'incentive_fee_as_based_on_performance_percentage', 'other_expenses', 'other_expenses_percentage', 'total_fixed_expenses', 'total_fixed_expenses_percentage', 'leasehold_rent', 'leasehold_rent_percentage', 'reserve_for_replacement', 'reserve_for_replacement_percentage', 'total_a_b', 'total_a_b_percentage', 'rent_expeneses_fhp', 'rent_expeneses_fhp_percentage', 'asset_management_fee', 'asset_management_fee_percentage', 'ebida_noi', 'ebida_noi_percentage', 'pv_of_noi', 'pv_of_noi_percentage'], 'number'],
            [['valuation_id'], 'required'],
            [['valuation_id', 'year_number'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number_of_rooms' => 'Number Of Rooms',
            'rooms_revenue_percentage' => 'Rooms Revenue Percentage',
            'days_per_year' => 'Days Per Year',
            'nights_available' => 'Nights Available',
            'occupancy' => 'Occupancy',
            'occupied_nights_per_year' => 'Occupied Nights Per Year',
            'adr' => 'Adr',
            'rev_par' => 'Rev Par',
            'rooms_revenue' => 'Rooms Revenue',
            'f_and_b_revenue' => 'F And B Revenue',
            'f_and_b_revenue_percentage' => 'F And B Revenue Percentage',
            'net_rental_income' => 'Net Rental Income',
            'net_rental_income_percentage' => 'Net Rental Income Percentage',
            'total_revenue' => 'Total Revenue',
            'total_revenue_percentage' => 'Total Revenue Percentage',
            'rooms' => 'Rooms',
            'rooms_percentage' => 'Rooms Percentage',
            'f_and_b' => 'F And B',
            'f_and_b_percentage' => 'F And B Percentage',
            'other_operating_departments' => 'Other Operating Departments',
            'other_operating_departments_percentage' => 'Other Operating Departments Percentage',
            'net_rental_income_exp' => 'Net Rental Income Exp',
            'net_rental_income_exp_percentage' => 'Net Rental Income Exp Percentage',
            'total_costs_and_expenses_percentage' => 'Total Costs And Expenses Percentage',
            'total_operating_dept_income' => 'Total Operating Dept Income',
            'total_operating_dept_income_percentage' => 'Total Operating Dept Income Percentage',
            'total_costs_and_expenses' => 'Total Costs And Expenses',
            'admin_and_general' => 'Admin And General',
            'admin_and_general_percentage' => 'Admin And General Percentage',
            'information_and_communication' => 'Information And Communication',
            'information_and_communication_percentage' => 'Information And Communication Percentage',
            'marketing' => 'Marketing',
            'marketing_percentage' => 'Marketing Percentage',
            'pom' => 'Pom',
            'pom_percentage' => 'Pom Percentage',
            'energy' => 'Energy',
            'energy_percentage' => 'Energy Percentage',
            'total_undistributed_expenses' => 'Total Undistributed Expenses',
            'total_undistributed_expenses_percentage' => 'Total Undistributed Expenses Percentage',
            'gross_operating_profit' => 'Gross Operating Profit',
            'gross_operating_profit_percentage' => 'Gross Operating Profit Percentage',
            'management_fee_base' => 'Management Fee Base',
            'management_fee_base_percentage' => 'Management Fee Base Percentage',
            'adjusted_gross_profit' => 'Adjusted Gross Profit',
            'adjusted_gross_profit_percentage' => 'Adjusted Gross Profit Percentage',
            'rental_expenses' => 'Rental Expenses',
            'rental_expenses_percentage' => 'Rental Expenses Percentage',
            'trade_license_fee' => 'Trade License Fee',
            'trade_license_fee_percentage' => 'Trade License Fee Percentage',
            'insurance' => 'Insurance',
            'insurance_percentage' => 'Insurance Percentage',
            'capacity_charges' => 'Capacity Charges',
            'capacity_charges_percentage' => 'Capacity Charges Percentage',
            'master_community_charges' => 'Master Community Charges',
            'master_community_charges_percentage' => 'Master Community Charges Percentage',
            'incentive_fee_as_based_on_performance' => 'Incentive Fee As Based On Performance',
            'incentive_fee_as_based_on_performance_percentage' => 'Incentive Fee As Based On Performance Percentage',
            'other_expenses' => 'Other Expenses',
            'other_expenses_percentage' => 'Other Expenses Percentage',
            'total_fixed_expenses' => 'Total Fixed Expenses',
            'total_fixed_expenses_percentage' => 'Total Fixed Expenses Percentage',
            'leasehold_rent' => 'Leasehold Rent',
            'leasehold_rent_percentage' => 'Leasehold Rent Percentage',
            'reserve_for_replacement' => 'Reserve For Replacement',
            'reserve_for_replacement_percentage' => 'Reserve For Replacement Percentage',
            'total_a_b' => 'Total A B',
            'total_a_b_percentage' => 'Total A B Percentage',
            'rent_expeneses_fhp' => 'Rent Expeneses Fhp',
            'rent_expeneses_fhp_percentage' => 'Rent Expeneses Fhp Percentage',
            'asset_management_fee' => 'Asset Management Fee',
            'asset_management_fee_percentage' => 'Asset Management Fee Percentage',
            'ebida_noi' => 'Ebida Noi',
            'ebida_noi_percentage' => 'Ebida Noi Percentage',
            'pv_of_noi' => 'Pv Of Noi',
            'pv_of_noi_percentage' => 'Pv Of Noi Percentage',
            'valuation_id' => 'Valuation ID',
            'year_number' => 'Year Number',
        ];
    }
}
