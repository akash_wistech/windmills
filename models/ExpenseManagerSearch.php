<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ExpenseManager;
use app\models\AssetManager;
use Yii;

// dd($_REQUEST);
/**
 * ExpenseManagerSearch represents the model behind the search form of `app\models\ExpenseManager`.
 */
class ExpenseManagerSearch extends ExpenseManager
{
    /**
     * {@inheritdoc}
     */

    public $date_range, $time_period,$time_period_compare, $custom_date_btw;
    public function rules()
    {
        return [
            [['transaction_type' , 'expense_id' , 'action', 'asset_id', 'purpose', 'category_id', 'category_name', 'depriciation', 'priority', 'frequency',
            'first_supplier','second_supplier','third_supplier','image_1','image_2','image_3',
            'applicant','department','approved_by','approvers_dept','asset_owner','entered_date',
            'approved_date','purchasing_date','target_date','collection_date','meeting_date',
            'start_date','expiry_date','frequency_start','frequency_end','status','amount','asset_life'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchTravel($params)
    {
        $user_id = Yii::$app->user->identity->id;

        if( $user_id == 1 || $user_id == 14 || $user_id == 86 || $user_id == 21 || $user_id == 110465 || $user_id == 33  || $user_id == 53  )
        {
            $query = ExpenseManager::find()
            ->where(['action' => null])
            ->andWhere(['total_time' => null]);
        }else{
            $query = ExpenseManager::find()
            ->where(['applicant' => $user_id])
            ->andWhere(['total_time' => null]);      
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        // $query->andFilterWhere([
        //     'modification' => $this->modification,
        //     'description' => $this->description,
        //     'module' => $this->module,
        //     'frequency' => $this->frequency,
        //     'priority' => $this->priority,
        //     'start_date' => $this->start_date,
        //     'end_date' => $this->end_date,
        //     'completed_date' => $this->completed_date,
        //     'meeting_date' => $this->meeting_date,
        //     'status' => $this->status,
        // ]);

        $query->andFilterWhere(['like', 'action', $this->action])
            ->andFilterWhere(['like', 'asset_id', $this->asset_id])
            ->andFilterWhere(['like', 'department', $this->department])
            ->andFilterWhere(['like', 'purpose', $this->purpose])
            ->andFilterWhere(['like', 'frequency', $this->frequency])
            ->andFilterWhere(['like', 'priority', $this->priority])
            ->andFilterWhere(['like', 'start_date', $this->start_date])
            ->andFilterWhere(['like', 'category_id', $this->category_id])
            ->andFilterWhere(['like', 'category_name', $this->category_name])
            ->andFilterWhere(['like', 'meeting_date', $this->meeting_date])
            // ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'depriciation', $this->depriciation])
            ->andFilterWhere(['like', 'first_supplier', $this->first_supplier])
            ->andFilterWhere(['like', 'second_supplier', $this->second_supplier])
            ->andFilterWhere(['like', 'third_supplier', $this->third_supplier])
            ->andFilterWhere(['like', 'image_1', $this->image_1])
            ->andFilterWhere(['like', 'image_2', $this->image_2])
            ->andFilterWhere(['like', 'image_3', $this->image_3])
            ->andFilterWhere(['like', 'applicant', $this->applicant])
            ->andFilterWhere(['like', 'approved_by', $this->approved_by])
            ->andFilterWhere(['like', 'approvers_dept', $this->approvers_dept])
            ->andFilterWhere(['like', 'asset_owner', $this->asset_owner])
            ->andFilterWhere(['like', 'expiry_date', $this->expiry_date])
            ->andFilterWhere(['like', 'entered_date', $this->entered_date])
            ->andFilterWhere(['like', 'approved_date', $this->approved_date])
            ->andFilterWhere(['like', 'purchasing_date', $this->purchasing_date])
            ->andFilterWhere(['like', 'target_date', $this->target_date])
            ->andFilterWhere(['like', 'collection_date', $this->collection_date])
            ->andFilterWhere(['like', 'frequency_start', $this->frequency_start])
            ->andFilterWhere(['like', 'frequency_end', $this->frequency_end])
            ->andFilterWhere(['like', 'status', $this->status]);


        return $dataProvider;
    }

    public function search($params)
    {
        $user_id = Yii::$app->user->identity->id;

        if( $user_id == 1 || $user_id == 14 || $user_id == 15  || $user_id == 111364)
        {
            $query = ExpenseManager::find();  
        }else{
            $query = ExpenseManager::find()
            ->where(['approver_by' => $user_id]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        // $query->andFilterWhere([
        //     'modification' => $this->modification,
        //     'description' => $this->description,
        //     'module' => $this->module,
        //     'frequency' => $this->frequency,
        //     'priority' => $this->priority,
        //     'start_date' => $this->start_date,
        //     'end_date' => $this->end_date,
        //     'completed_date' => $this->completed_date,
        //     'meeting_date' => $this->meeting_date,
        //     'status' => $this->status,
        // ]);

        $query
            ->andFilterWhere(['like', 'purpose', $this->purpose])
            ->andFilterWhere(['like', 'start_date', $this->start_date])
            ->andFilterWhere(['like', 'start_time', $this->start_time])
            ->andFilterWhere(['like', 'category_name', $this->category_name])
            ->andFilterWhere(['like', 'meeting_date', $this->meeting_date])
            // ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'depriciation', $this->depriciation])
            ->andFilterWhere(['like', 'first_supplier', $this->first_supplier])
            ->andFilterWhere(['like', 'second_supplier', $this->second_supplier])
            ->andFilterWhere(['like', 'third_supplier', $this->third_supplier])
            ->andFilterWhere(['like', 'image_1', $this->image_1])
            ->andFilterWhere(['like', 'image_2', $this->image_2])
            ->andFilterWhere(['like', 'image_3', $this->image_3])
            ->andFilterWhere(['like', 'applicant', $this->applicant])
            ->andFilterWhere(['like', 'approved_by', $this->approved_by])
            ->andFilterWhere(['like', 'approvers_dept', $this->approvers_dept])
            ->andFilterWhere(['like', 'asset_owner', $this->asset_owner])
            ->andFilterWhere(['like', 'expiry_date', $this->expiry_date])
            ->andFilterWhere(['like', 'entered_date', $this->entered_date])
            ->andFilterWhere(['like', 'approved_date', $this->approved_date])
            ->andFilterWhere(['like', 'purchasing_date', $this->purchasing_date])
            ->andFilterWhere(['like', 'target_date', $this->target_date])
            ->andFilterWhere(['like', 'collection_date', $this->collection_date])
            ->andFilterWhere(['like', 'frequency_start', $this->frequency_start])
            ->andFilterWhere(['like', 'frequency_end', $this->frequency_end])
            ->andFilterWhere(['like', 'status', $this->status]);


        return $dataProvider;
    }

    public function searchOt($params)
    {

        $user_id = Yii::$app->user->identity->id;

        if( $user_id == 1 || $user_id == 14 || $user_id == 33 || $user_id == 110465 || $user_id == 86 || $user_id == 53 )
        {
            $query = ExpenseManager::find()
            ->where(['action' => null])
            ->andWhere(['is not', 'start_time', null])
            ->andWhere(['reference_number' => null]);
        }else{
            $query = ExpenseManager::find()
            ->where(['applicant' => $user_id])
            ->andWhere(['action' => null])
            ->andWhere(['is not', 'start_time', null])
            ->andWhere(['reference_number' => null]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params); 

        // dd($params['ExpenseManagerSearch']['time_period']);

        if($params['ExpenseManagerSearch']['time_period'] <> null && $params['ExpenseManagerSearch']['time_period'] != 0 ) {

            $today = date('Y-m-d');
            $thirtyDaysAgo = date('Y-m-d', strtotime('-30 days'));
            $dayOfMonth = date('j');

            if($params['ExpenseManagerSearch']['time_period'] == 1){

                $from_date = $thirtyDaysAgo;
                $to_date = $today;

            }elseif ($params['ExpenseManagerSearch']['time_period'] == 2) {
                if ($dayOfMonth >= 20) {
                    $from_date = date('Y-m-20');
                    $to_date = date('Y-m-20', strtotime('+1 month'));
                } else {
                    $from_date = date('Y-m-20', strtotime('-1 month'));
                    $to_date = date('Y-m-20');
                }
            }elseif ($params['ExpenseManagerSearch']['time_period'] == 4) {
                $from_date = date('Y-01-01');
                $to_date = $today;
            }elseif ($params['ExpenseManagerSearch']['time_period'] == 5) {
                $from_date = date('Y-m-20', strtotime('-1 month'));
                $to_date = date('Y-m-20');
            }elseif ($params['ExpenseManagerSearch']['time_period'] == 7) {
                $from_date = date('Y-01-01', strtotime('-1 year'));
                $to_date = date('Y-12-31', strtotime('-1 year'));
            }

            if($params['ExpenseManagerSearch']['time_period'] == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }
        }else{
            $from_date = '2020-04-28 00:00:00';
            $to_date = date('Y-m-d 23:59:00');
        }

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['between', 'start_date', $from_date, $to_date]);
        $query->andFilterWhere(['like', 'applicant', $this->applicant]);

        // dd($query);

        return $dataProvider;
    }
}
