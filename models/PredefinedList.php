<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "{{%predefined_list}}".
 *
 * @property int $id
 * @property int $parent
 * @property string $title
 * @property string $other_name
 * @property string $descp
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property int $trashed
 * @property string $trashed_at
 * @property int $trashed_by
 */
class PredefinedList extends ActiveRecordFull
{
  //country attributes
  public $iso_code_2,$iso_code_3,$phone_code,$phone_format,$mobile_format,$address_format,
  $postcode_required;

  //state/zone attributes
  public $country_id,$code;
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return '{{%predefined_list}}';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['parent','title'], 'required'],
      //[['title'], 'unique'],
      [['title','other_name'], 'filter', 'filter' => 'trim'],
      [['created_at', 'updated_at', 'trashed_at'], 'safe'],
      [[
        'parent', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by','postcode_required','country_id'
      ], 'integer'],
      [[
        'title','other_name','descp','phone_code','phone_format','mobile_format','address_format',
        'code'
      ],'string'],
      [['iso_code_2'],'string','max'=>2],
      [['iso_code_3'],'string','max'=>3],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'parent' => Yii::t('app', 'Parent'),
      'title' => Yii::t('app', 'Name'),
      'other_name' => Yii::t('app', 'Singular Name'),
      'descp' => Yii::t('app', 'Description'),
      'status' => Yii::t('app', 'Status'),
      'created_at' => Yii::t('app', 'Created'),
      'created_by' => Yii::t('app', 'Created By'),
      'updated_at' => Yii::t('app', 'Updated'),
      'updated_by' => Yii::t('app', 'Updated By'),
      'trashed' => Yii::t('app', 'Trashed'),
      'trashed_at' => Yii::t('app', 'Trashed At'),
      'trashed_by' => Yii::t('app', 'Trashed By'),

      'iso_code_2' => Yii::t('app', 'ISO Code 2'),
      'iso_code_3' => Yii::t('app', 'ISO Code 3'),
      'phone_code' => Yii::t('app', 'Phone Code'),
      'phone_format' => Yii::t('app', 'Phone Format'),
      'mobile_format' => Yii::t('app', 'Mobile Format'),

      'country_id' => Yii::t('app', 'Country'),
      'code' => Yii::t('app', 'Code'),
    ];
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecTitle()
  {
    return $this->title;
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecType()
  {
		return 'Predefined List';
  }

	/**
	* @inheritdoc
	*/
	// public function afterSave($insert, $changedAttributes)
	// {
  //   $set_country_id=Yii::$app->appHelperFunctions->getSetting('country_list_id');
  //   $set_zone_id=Yii::$app->appHelperFunctions->getSetting('zone_list_id');
  //   //Country
	// 	if($this->parent==$set_country_id){
  //     $childRow = PredefinedListCountryDetail::find()->where(['id'=>$this->id])->one();
  //     if($childRow==null){
  //       $childRow = new PredefinedListCountryDetail;
  //     }
  //     $childRow->id = $this->id;
  //     $childRow->title = $this->title;
  //     if($this->iso_code_2!='')$childRow->iso_code_2 = $this->iso_code_2;
  //     if($this->iso_code_3!='')$childRow->iso_code_3 = $this->iso_code_3;
  //     if($this->phone_code!='')$childRow->phone_code = $this->phone_code;
  //     if($this->phone_format!='')$childRow->phone_format = $this->phone_format;
  //     if($this->mobile_format!='')$childRow->mobile_format = $this->mobile_format;
  //     if($this->address_format!='')$childRow->address_format = $this->address_format;
  //     if($this->postcode_required!='')$childRow->postcode_required = $this->postcode_required;
  //     $childRow->save();
	// 	}
  //   //Zone
	// 	if($this->parent==$set_zone_id){
	// 		$childRow = PredefinedListZoneDetail::find()->where(['id'=>$this->id])->one();
  //     if($childRow==null){
  //       $childRow = new PredefinedListZoneDetail;
  //       $childRow->id = $this->id;
  //     }
  //     $childRow->title = $this->title;
  //     if($this->country_id!='')$childRow->country_id = $this->country_id;
  //     if($this->code!='')$childRow->code = $this->code;
  //     $childRow->save();
	// 	}
	// 	parent::afterSave($insert, $changedAttributes);
	// }
}
