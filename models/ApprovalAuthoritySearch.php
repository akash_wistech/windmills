<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApprovalAuthority;


// dd($_REQUEST);
/**
 * ApprovalAuthoritySearch represents the model behind the search form of `app\models\ApprovalAuthority`.
 */
class ApprovalAuthoritySearch extends ApprovalAuthority
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['asset_name' , 'category_id' , 'depreciation'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApprovalAuthority::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        // $query->andFilterWhere([
        //     'modification' => $this->modification,
        //     'description' => $this->description,
        //     'module' => $this->module,
        //     'frequency' => $this->frequency,
        //     'priority' => $this->priority,
        //     'start_date' => $this->start_date,
        //     'end_date' => $this->end_date,
        //     'completed_date' => $this->completed_date,
        //     'meeting_date' => $this->meeting_date,
        //     'entered_by' => $this->entered_by,
        //     'status' => $this->status,
        // ]);

        $query->andFilterWhere(['like', 'asset_name', $this->asset_name])
            ->andFilterWhere(['like', 'category_id', $this->category_id])
            ->andFilterWhere(['like', 'depreciation', $this->depreciation]);


        return $dataProvider;
    }

}
