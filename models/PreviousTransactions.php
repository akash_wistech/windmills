<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "previous_transactions".
 *
 * @property int $id
 * @property string|null $instruction_date
 * @property string|null $inspection_date
 * @property string|null $approver
 * @property float|null $fee
 * @property string|null $target_date
 * @property string|null $due_date
 * @property string|null $date_submitted
 * @property string|null $reference_number
 * @property string|null $client_reference
 * @property string|null $client_name
 * @property string|null $client_type
 * @property string|null $client_customer_name
 * @property string|null $contact_person
 * @property string|null $designation
 * @property string|null $contact_number
 * @property int|null $property_category
 * @property int|null $property_id
 * @property int|null $tenure
 * @property string|null $plot_number
 * @property int|null $unit_number
 * @property int $building_info
 * @property int|null $valuation_type
 * @property int|null $valuation_method
 * @property string|null $valuer
 * @property int|null $taqyimee_number
 * @property int|null $location
 * @property int|null $view
 * @property int|null $property_condition
 * @property int|null $property_placement
 * @property string|null $finished_status
 * @property int|null $property_exposure
 * @property int|null $floor_number
 * @property int|null $no_of_bedrooms
 * @property string|null $land_size
 * @property float|null $market_value_of_land
 * @property float|null $built_up_area
 * @property float|null $estimated_age
 * @property float|null $transaction_price
 * @property float|null $market_value
 * @property float|null $psf
 * @property int|null $purpose_of_valuation
 * @property int $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class PreviousTransactions extends ActiveRecordFull
{
    public $status_verified;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'previous_transactions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['instruction_date', 'inspection_date', 'target_date', 'due_date', 'date_submitted', 'created_at', 'updated_at', 'trashed_at','inspector','community','sub_community','unit_number','property_id','number_of_levels','parking_space','pool','white_goods','utilities_connected','developer_margin','balcony_size','landscaping','location_highway_drive','location_school_drive','location_mall_drive','location_sea_drive','location_park_drive','view_community','view_pool','view_burj','view_sea','view_marina','view_mountains','view_lake','view_golf_course','view_park','view_special', 'plot_number', 'property_exposure'], 'safe'],
            [['fee', 'market_value_of_land', 'built_up_area', 'estimated_age', 'transaction_price', 'market_value', 'psf','land_size','balcony_size', 'property_placement'], 'number'],
            [['property_category', 'property_id', 'tenure', 'building_info', 'valuation_type', 'valuation_method', 'taqyimee_number', 'property_condition', 'floor_number', 'no_of_bedrooms', 'purpose_of_valuation', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by','number_of_levels','parking_space'], 'integer'],
            [['building_info'], 'required'],
            [['finished_status'], 'string'],
            [['reference_number'], 'unique'],
            [['approver', 'reference_number', 'client_reference', 'client_name', 'client_type', 'client_customer_name', 'contact_person', 'designation', 'contact_number', 'valuer'], 'string', 'max' => 255],
            [['status_verified','status_verified_at','status_verified_by','valuation_approach'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'instruction_date' => 'Instruction Date',
            'inspection_date' => 'Inspection Date',
            'approver' => 'Approver',
            'fee' => 'Fee',
            'target_date' => 'Target Date',
            'due_date' => 'Due Date',
            'date_submitted' => 'Date Submitted',
            'reference_number' => 'Reference Number',
            'client_reference' => 'Client Reference',
            'client_name' => 'Client Name',
            'client_type' => 'Client Type',
            'client_customer_name' => 'Client Customer Name',
            'contact_person' => 'Contact Person',
            'designation' => 'Designation',
            'contact_number' => 'Contact Number',
            'property_category' => 'Property Category',
            'property_id' => 'Property ID',
            'tenure' => 'Tenure',
            'plot_number' => 'Plot Number',
            'unit_number' => 'Unit Number',
            'building_info' => 'Building Info',
            'community' => 'Community',
            'sub_community' => 'Sub Community',
            'valuation_type' => 'Valuation Type',
            'valuation_method' => 'Valuation Method',
            'valuer' => 'Valuer',
            'taqyimee_number' => 'Taqyimee Number',
            'property_condition' => 'Property Condition',
            'property_placement' => 'Property Placement',
            'finished_status' => 'Finished Status',
            'property_exposure' => 'Property Exposure',
            'floor_number' => 'Floor Number',
            'no_of_bedrooms' => 'No Of Bedrooms',
            'land_size' => 'Land Size',
            'market_value_of_land' => 'Market Value Of Land',
            'built_up_area' => 'Built Up Area',
            'estimated_age' => 'Estimated Age',
            'transaction_price' => 'Transaction Price',
            'market_value' => 'Market Value',
            'psf' => 'Psf',
            'purpose_of_valuation' => 'Purpose Of Valuation',
            'location_highway_drive' => 'Drive to Highway/Main Road and metro',
            'location_school_drive' => 'Drive to school',
            'location_mall_drive' => 'Drive to commercial area/Mall',
            'location_sea_drive' => 'Drive to special landmark, sea, marina',
            'location_park_drive' => 'Drive to pool and park',
            'view_community' => 'Community',
            'view_pool' => 'Pool / Fountain',
            'view_park' => 'Small Park',
            'view_special' => 'Special view',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }

    public function beforeSave($insert)
    {

        if(!empty($this->building)){
            $this->community = $this->building->community;
            $this->sub_community = $this->building->sub_community;
        }

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }
    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Previous Transactions';
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->title;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuilding()
    {
        return $this->hasOne(Buildings::className(), ['id' => 'building_info']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Properties::className(), ['id' => 'property_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommunities()
    {
        return $this->hasOne(Communities::className(), ['id' => 'community']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubCommunities()
    {
        return $this->hasOne(SubCommunities::className(), ['id' => 'sub_community']);
    }
    public function getValuation()
    {
        return $this->hasOne(Valuation::className(), ['reference_number' => 'reference_number']);
    }
}
