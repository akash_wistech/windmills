<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sold_ad".
 *
 * @property int $id
 * @property string $reidin_ref
 * @property string|null $sales_type
 * @property string|null $sales_sequence
 * @property string|null $transaction_date
 * @property string|null $t_date
 * @property string|null $municipality
 * @property string|null $district
 * @property string|null $community
 * @property string|null $projectsub_comm
 * @property string|null $building
 * @property string|null $property_subtype
 * @property string|null $floor
 * @property string|null $bedroom
 * @property string|null $sale_percent
 * @property string|null $unit_area_sqm
 * @property float|null $plot_area_sqm
 * @property string|null $price
 * @property string|null $mortgage_value
 * @property string|null $average_price_sqm
 * @property string|null $valuation_amount
 * @property string|null $developer
 * @property int|null $status
 */
class SoldAd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sold_24';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reidin_ref'], 'required'],
            [['transaction_date', 'sales_price_sqft_unit','unit_size_sqft','plot_size_sqft'], 'safe'],
            [['plot_area_sqm'], 'number'],
            [['status'], 'integer'],
            [['reidin_ref', 'developer'], 'string', 'max' => 45],
            [['sales_type'], 'string', 'max' => 8],
            [['sales_sequence'], 'string', 'max' => 9],
            [['t_date', 'municipality'], 'string', 'max' => 16],
            [['district', 'community'], 'string', 'max' => 25],
            [['projectsub_comm'], 'string', 'max' => 47],
            [['building'], 'string', 'max' => 38],
            [['property_subtype'], 'string', 'max' => 29],
            [['floor'], 'string', 'max' => 2],
            [['bedroom'], 'string', 'max' => 18],
            [['sale_percent'], 'string', 'max' => 4],
            [['unit_area_sqm'], 'string', 'max' => 7],
            [['price', 'valuation_amount'], 'string', 'max' => 13],
            [['mortgage_value', 'average_price_sqm'], 'string', 'max' => 11],
            [['maid', 'study','balcony','custom_view','floor_level','evdnc_name','dev_name','unit_no'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reidin_ref' => 'Reidin Ref',
            'sales_type' => 'Sales Type',
            'sales_sequence' => 'Sales Sequence',
            'transaction_date' => 'Transaction Date',
            't_date' => 'T Date',
            'municipality' => 'Municipality',
            'district' => 'District',
            'community' => 'Community',
            'projectsub_comm' => 'Projectsub Comm',
            'building' => 'Building',
            'property_subtype' => 'Property Subtype',
            'floor' => 'Floor',
            'bedroom' => 'Bedroom',
            'sale_percent' => 'Sale Percent',
            'unit_area_sqm' => 'Unit Area Sqm',
            'plot_area_sqm' => 'Plot Area Sqm',
            'price' => 'Price',
            'mortgage_value' => 'Mortgage Value',
            'average_price_sqm' => 'Average Price Sqm',
            'valuation_amount' => 'Valuation Amount',
            'developer' => 'Developer',
            'status' => 'Status',
        ];
    }
}
