<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;
use yii\web\IdentityInterface;
use yii\web\UploadedFile;
use yii\validators\RegularExpressionValidator;

/**
* User model
*
* @property integer $id
* @property integer $user_type
* @property integer $permission_group_id
* @property integer $active_contract_id
* @property integer $active_package_id
* @property string $username
* @property string $firstname
* @property string $lastname
* @property string $email
* @property string $auth_key
* @property string $password_hash
* @property string $image
* @property integer $status
* @property string $start_date
* @property string $end_date
*/
class User extends ActiveRecordFull implements IdentityInterface
{
  public $file,$oldfile;
  public $allowedImageSize = 1048576;
  public $allowedImageTypes=['jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'];

  public $new_password,$job_title_id,$job_title,$department_id,$department_name ,$unit_id,$unit_name;
  public $primary_contact,$mobile,$phone1,$phone2,$country_id,$zone_id,$city,$postal_code,$address,$background_info;
  public $lead_type,$lead_source,$lead_date,$lead_score,$expected_close_date,$close_date,$confidence,$deal_status;
  public $company_name,$manager_id;

  public $gender,$middlename,$nationality,$passport_number,$birth_date,$marital_status,$marriage_date;
  public $no_of_children,$mother_name,$mother_profession,$father_name,$father_profession,$apartment;
  public $street,$community,$ijari,$origin_apartment,$origin_street,$origin_country,$origin_city;
  public $emergency_uae,$emergency_home,$whatsapp_groups,$personal_email,$group_emails;

  public $degree_name, $institute_name, $passing_year, $percentage_obtained, $attested, $degree_attached;
  public $company, $position, $department, $start_date, $end_date, $gross_salary, $leave_reason;
  public $total_experience, $relevant_experience;
  public $ncc, $ncc_breakage_cost, $notice_period, $early_breakage_cost, $visa_type, $visa_emirate, $joining_date, $visa_start_date, $visa_end_date;
  public $driving_license, $hobby, $language;
  public $ref_fname, $ref_lname, $ref_nationality, $ref_company, $ref_dept, $ref_position, $ref_landline, $ref_mobile, $ref_letter;

  public $basic_salary, $house_allowance, $transport_allowance, $last_gross_salary, $tax_rate, $net_salary , $other_allowance;
  public $windmills_position, $reports_to;
  public $picture, $cv, $cover_letter, $emirates_id, $residence_visa, $work_permit, $last_contract, $offer_letter, $job_description,$kpi;


  public $valuer_qualifications,$valuer_status,$valuer_experience_expertise,$signature_img_name,$signature_file;

  const STATUS_ACTIVE = '1';
  const STATUS_HOLD = '2';
  const STATUS_CANCEL = '3';
  const STATUS_PENDIND = '20';
  const STATUS_DELETED = '0';

  public $before_after_save = 0, $is_save=0;
  public $qualification = [];
  public $experience = [];
  public $reference = [];
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%user}}';
  }


  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_type' => Yii::t('app', 'Member Type'),
      'permission_group_id' => Yii::t('app', 'Permission Group'),
      'firstname' => Yii::t('app', 'First Name'),
      'lastname' => Yii::t('app', 'Last Name'),
      'email' => Yii::t('app', 'Email'),
      'new_password' => Yii::t('app', 'Password'),
      'job_title_id' => Yii::t('app', 'Job Title'),
      'job_title' => Yii::t('app', 'Job Title'),
      'department_id' => Yii::t('app', 'Department'),
      'department_name' => Yii::t('app', 'Unit'),
      'unit_name' => Yii::t('app', 'Department'),

      'country_id' => Yii::t('app', 'Country'),
      'zone_id' => Yii::t('app', 'State / Province'),
      'city' => Yii::t('app', 'City'),
      'postal_code' => Yii::t('app', 'Postal Code'),

      'mobile' => Yii::t('app', 'Office Mobile Phone'),
      'phone1' => Yii::t('app', 'Office Landline'),
      'phone2' => Yii::t('app', 'Personal Mobile Phone'),
      'address' => Yii::t('app', 'Address'),
      'background_info' => Yii::t('app', 'Background Info'),

      'image' => Yii::t('app', 'Photo'),
			'manager_id' => Yii::t('app', 'Assign To'),


      'valuer_qualifications' => Yii::t('app', 'Valuer Qualifications'),
      'valuer_status' => Yii::t('app', 'Valuer Status'),
      'valuer_experience_expertise' => Yii::t('app', 'Valuer Experience Expertise'),
      'signature_img_name' => Yii::t('app', 'Signature Image'),
        'quotation_approver' => Yii::t('app', 'Quotation Approver'),

      'middlename' => Yii::t('app', 'Middle Name'),
      'birth_date' => Yii::t('app', 'Date of Birth'),
      
      'origin_apartment' => Yii::t('app', 'Apartment'),
      'origin_street' => Yii::t('app', 'Street'),
      'origin_country' => Yii::t('app', 'Country'),
      'origin_city' => Yii::t('app', 'City'),

      'emergency_uae' => Yii::t('app', 'UAE Emergency Contact'),
      'emergency_home' => Yii::t('app', 'Home Emergency Contact'),
      
    ];
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['email', 'firstname', 'lastname'], 'required'],
      [['email','personal_email'],'email'],
      ['email','unique'],
      ['password','safe'],
      [[
        'firstname','lastname','email','new_password','job_title','department_name','unit_name','address','background_info',
        'lead_date','expected_close_date','close_date','valuer_qualifications','valuer_status','valuer_experience_expertise',
        'mobile','phone1','phone2',
      ],'string'],
      [['firstname','lastname','email','mobile','phone1','phone2'], 'trim'],
      [['firstname','lastname','email','mobile','phone1','phone2','client_approver'], 'safe'],
  /*    [[
        'company_id','department_id','permission_group_id','lead_type','lead_source',
        'lead_score','confidence','deal_status','job_title_id','bilal_contact'
      ], 'integer'],*/
        [[
            'company_id','department_id','permission_group_id','lead_type','lead_source',
            'lead_score','confidence','deal_status','job_title_id','bilal_contact'
        ], 'safe'],
      [['image'], 'file', 'extensions'=>implode(",",$this->allowedImageTypes), 'maxSize' => $this->allowedImageSize, 'tooBig' => 'Image is too big, Maximum allowed size is 1MB'],
     // [['manager_id'],'each','rule'=>['integer']],

/*      ['firstname', 'string', 'max' => 14],
      ['firstname', 'match', 'pattern' => '/^[a-zA-Z\s]+$/', 'message' => 'Only alphanumeric characters and spaces are allowed.'],

      ['lastname', 'string', 'max' => 14],
      ['lastname', 'match', 'pattern' => '/^[a-zA-Z\s]+$/', 'message' => 'Only alphanumeric characters and spaces are allowed.'],
      ['middlename', 'string', 'max' => 14],
      ['middlename', 'match', 'pattern' => '/^[a-zA-Z\s]+$/', 'message' => 'Only alphanumeric characters and spaces are allowed.'],

      ['mother_name', 'string', 'max' => 14],
      ['mother_name', 'match', 'pattern' => '/^[a-zA-Z\s]+$/', 'message' => 'Only alphanumeric characters and spaces are allowed.'],

      ['father_name', 'string', 'max' => 14],
      ['father_name', 'match', 'pattern' => '/^[a-zA-Z\s]+$/', 'message' => 'Only alphanumeric characters and spaces are allowed.'],

      ['phone2', 'string', 'length' => 12],
      ['phone2', 'match', 'pattern' => '/^\d{12}$/', 'message' => 'Please enter a valid 12-digit phone number.'],
      
      ['phone1', 'string', 'length' => 12],
      ['phone1', 'match', 'pattern' => '/^\d{12}$/', 'message' => 'Please enter a valid 12-digit phone number.'],
      
      ['mobile', 'string', 'length' => 12],
      ['mobile', 'match', 'pattern' => '/^\d{12}$/', 'message' => 'Please enter a valid 12-digit phone number.'],
      
      ['emergency_uae', 'string', 'length' => 12],
      ['emergency_uae', 'match', 'pattern' => '/^\d{12}$/', 'message' => 'Please enter a valid 12-digit phone number.'],

      ['emergency_home', 'string', 'length' => 12],
      ['emergency_home', 'match', 'pattern' => '/^\d{12}$/', 'message' => 'Please enter a valid 12-digit phone number.'],*/


      [['image', 'manager_id', 'middlename', 'mother_name', 'father_name', 'phone2','phone1','mobile','emergency_uae','emergency_home' , 'visa_emirate'], 'safe'],
      [['existing_valuation_contact', 'bilal_contact', 'valuation_contact', 'prospect_contact', 'property_owner_contact', 'inquiry_valuations_contact','icai_contact','broker_contact','developer_contact','is_save','linked_in_page','mobile_1','source_of_relationship'], 'safe'],

      [['degree_attached' , 'ref_letter' , 'emirates_id' , 'residence_visa' , 'work_permit' , 'last_contract',
      'offer_letter', 'job_description','kpi'], 'file'],
        
      [['degree_attached' , 'ref_letter' , 'emirates_id' , 'residence_visa' , 'work_permit' , 'last_contract',
      'offer_letter', 'job_description', 'kpi'], 'safe'],
    ];
  }


  public  function generateRandomString($length = 50) {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
  }


  /**
  * @inheritdoc
  */
  public static function findIdentity($id)
  {
    return static::findOne(['id' => $id, 'status' => 1, 'verified' => 1, 'trashed'=>0]);
  }

  /**
  * {@inheritdoc}
  */
  public static function findIdentityByAccessToken($token, $type = null)
  {
    return static::findOne(['auth_key' => $token, 'status' => 1, 'verified' => 1, 'trashed' => 0]);
  }

  /**
  * Finds user by username
  *
  * @param string $username
  * @return static|null
  */
  public static function findByUsername($username)
  {
    return static::find()->where(['email' => $username, 'trashed' => 0])->one();
  }

  /**
  * {@inheritdoc}
  */
  public function getId()
  {
    return $this->getPrimaryKey();
  }

  /**
  * {@inheritdoc}
  */
  public function getAuthKey()
  {
    return $this->auth_key;
  }

  /**
  * {@inheritdoc}
  */
  public function validateAuthKey($authKey)
  {
    return $this->auth_key === $authKey;
  }

  /**
  * Validates password
  *
  * @param string $password password to validate
  * @return bool if password provided is valid for current user
  */
  public function validatePassword($password)
  {
    return Yii::$app->security->validatePassword($password, $this->password_hash);
  }

  /**
  * Generates password hash from password and sets it to the model
  *
  * @param string $password
  */
  public function setPassword($password)
  {
    $this->password_hash = Yii::$app->security->generatePasswordHash($password);
  }

  /**
  * Generates "remember me" authentication key
  */
  public function generateAuthKey()
  {
    $this->auth_key = Yii::$app->security->generateRandomString();
  }

  /**
  * Generates "login from backend" authentication key
  */
  public function generateVerifyToken()
  {
    $this->verify_token = Yii::$app->security->generateRandomString();
  }

  /**
  * Generates new password reset token
  */
  public function generatePasswordResetToken()
  {
    $connection = \Yii::$app->db;
    $connection->createCommand("update ".UserReset::tableName()." set status=:status where user_id=:user_id",[
      ':status' => 10,':user_id' => $this->id,
    ])
    ->execute();

    $newResetInfo = new UserReset;
    $newResetInfo->user_id = $this->id;
    $newResetInfo->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    $newResetInfo->status = 0;
    $newResetInfo->save();
  }

  public function getPasswordResetToken()
  {
    $token = '';
    $resetInfo=UserReset::find()->where(['user_id'=>$this->id])->orderBy(['created_at'=>SORT_DESC])->one();
    if($resetInfo!=null){
      $token = $resetInfo->password_reset_token;
    }
    return $token;
  }

  /**
  * Finds user by password reset token
  *
  * @param string $token password reset token
  * @return static|null
  */
  public static function findByPasswordResetToken($token)
  {
    if (!static::isPasswordResetTokenValid($token)) {
      return null;
    }
    return UserReset::find()
    ->innerJoin("user",User::tableName().".id=".UserReset::tableName().".user_id")
    ->where([
      UserReset::tableName().'.password_reset_token' => $token,
      UserReset::tableName().'.status' => 0,
      User::tableName().'.status' => 1,
    ])
    ->one();
  }

  /**
  * Finds out if password reset token is valid
  *
  * @param string $token password reset token
  * @return boolean
  */
  public static function isPasswordResetTokenValid($token)
  {
    if (empty($token) || $token==null) {
      return false;
    }
    $expire = Yii::$app->params['user.passwordResetTokenExpire'];
    $parts = explode('_', $token);
    $timestamp = (int) end($parts);
    return $timestamp + $expire >= time();
  }

  /**
  * return full name
  */
  public function getName()
  {
    return trim($this->firstname.($this->lastname!='' ? ' '.$this->lastname : ''));
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecTitle()
  {
    return $this->name;
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecType()
  {
    return ($this->user_type==10 ? 'Staff Member' : 'User/Contact');
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getProfileInfo()
  {
    return $this->hasOne(UserProfileInfo::className(), ['user_id' => 'id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getResetInfo()
  {
    return $this->hasOne(UserReset::className(), ['user_id' => 'id'])->where(['status'=>0]);
  }

  public function getLastLoginInfo()
  {
    $history=UserLoginHistory::find()
    ->select(['created_at'])
    ->where(['user_id'=>$this->id,'login'=>1])
    ->orderBy(['created_at'=>SORT_DESC])
    ->offset(1)
    ->asArray()->one();
    if($history!=null){
      return 'on '.Yii::$app->formatter->asDate($history['created_at']);
    }else{
      return ' first time';
    }
  }

  /**
  * @return boolean
  */
  public function getPrivilege($keyword)
  {
    $permission=false;
    $services=ArrayHelper::map(PackageOption::find()->all(),'keyword','id');
    $result=PackageAllowedOptions::find()->where(['package_id'=>$this->activeContract->package_id,'service_id'=>$services[$keyword]])->one();
    if($result!=null){
      $permission=true;
    }

    return $permission;
  }

  /**
  * @inheritdoc
  */
  public function beforeValidate()
  {
    //Uploading Logo
    if(UploadedFile::getInstance($this, 'image')){
      $this->file = UploadedFile::getInstance($this, 'image');
      // if no file was uploaded abort the upload
      if (!empty($this->file)) {
        $pInfo=pathinfo($this->file->name);
        $ext = $pInfo['extension'];
        if (in_array($ext,$this->allowedImageTypes)) {
          // Check to see if any PHP files are trying to be uploaded
          $content = file_get_contents($this->file->tempName);
          if (preg_match('/\<\?php/i', $content)) {
            $this->addError('image', Yii::t('app', 'Invalid file provided!'));
            return false;
          }else{
            if (filesize($this->file->tempName) <= $this->allowedImageSize) {
              // generate a unique file name
              $this->image = Yii::$app->fileHelperFunctions->generateName().".{$ext}";
            }else{
              $this->addError('image', Yii::t('app', 'File size is too big!'));
              return false;
            }
          }
        }else{
          $this->addError('image', Yii::t('app', 'Invalid file provided!'));
          return false;
        }
      }
    }
    if($this->image==null && $this->oldfile!=null){
      $this->image=$this->oldfile;
    }
    return parent::beforeValidate();
  }

  /**
  * @inheritdoc
  * Generates auth_key if it is a new user
  */
  public function beforeSave($insert)
  {
    if (parent::beforeSave($insert)) {
      if ($this->isNewRecord) {
        $this->generateAuthKey();
        $this->generateVerifyToken();
      }
      if($this->new_password!=null){
        $this->password = $this->new_password;
          $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
      }
      return true;
    }
    return false;
  }

  /**
  * @inheritdoc
  */
  public function afterSave($insert, $changedAttributes)
  {
    $url = $_SERVER['REQUEST_URI'];
    $path = parse_url($url, PHP_URL_PATH);
    $lastSegment = basename($path);

    if($lastSegment == "update-profile")
    {
          //Saving New Job title if its not in db
        if($this->job_title!=null && $this->job_title!=""){
          $jobTitleRow=JobTitle::find()->where(['title'=>trim($this->job_title)])->one();
          if($jobTitleRow==null){
            $jobTitleRow=new JobTitle;
            $jobTitleRow->title=$this->job_title;
            $jobTitleRow->status=1;
            if($jobTitleRow->save()){
              $this->job_title_id=$jobTitleRow->id;
            }
          }else{
            $this->job_title_id=$jobTitleRow->id;
          }
        }
        //Saving New Department if its not in db
        if($this->department_name!=null && $this->department_name!=""){
          $departmentRow=Department::find()->where(['title'=>trim($this->department_name)])->one();
          if($departmentRow==null){
            $departmentRow=new Department;
            $departmentRow->title=$this->department_name;
            $departmentRow->status=1;
            if($departmentRow->save()){
              $this->department_id=$departmentRow->id;
            }
          }else{
            $this->department_id=$departmentRow->id;
          }
        }

        if($this->unit_name!=null && $this->unit_name!=""){
            $unitRow=Units::find()->where(['title'=>trim($this->unit_name)])->one();
            if($unitRow==null){
                $unitRow=new Units();
                $unitRow->title=$this->unit_name;
                $unitRow->status=1;
                if($unitRow->save()){
                    $this->unit_id=$unitRow->id;
                }
            }else{
                $this->unit_id=$unitRow->id;
            }
        }

        $userProfileInfo = UserProfileInfo::find()->where(['user_id'=>$this->id])->one();
        if($userProfileInfo==null){
          $userProfileInfo=new UserProfileInfo;
          $userProfileInfo->user_id=$this->id;
        }
        //$userProfileInfo->primary_contact=($this->primary_contact!=null ? $this->primary_contact : 0);
        $userProfileInfo->primary_contact=($this->primary_contact!=null ? $this->primary_contact : 1);

        if($this->job_title_id!=null)$userProfileInfo->job_title_id=$this->job_title_id;
        if($this->department_id!=null)$userProfileInfo->department_id=$this->department_id;
        if($this->unit_id!=null)$userProfileInfo->unit_id=$this->unit_id;
        if($this->mobile!=null)$userProfileInfo->mobile=$this->mobile;
        if($this->phone1!=null)$userProfileInfo->phone1=$this->phone1;
        if($this->phone2!=null)$userProfileInfo->phone2=$this->phone2;
        if($this->address!=null)$userProfileInfo->address=$this->address;
    }else{
        //Saving New Job title if its not in db
        if($this->job_title!=null && $this->job_title!=""){
          $jobTitleRow=JobTitle::find()->where(['title'=>trim($this->job_title)])->one();
          if($jobTitleRow==null){
            $jobTitleRow=new JobTitle;
            $jobTitleRow->title=$this->job_title;
            $jobTitleRow->status=1;
            if($jobTitleRow->save()){
              $this->job_title_id=$jobTitleRow->id;
            }
          }else{
            $this->job_title_id=$jobTitleRow->id;
          }
        }
        //Saving New Department if its not in db
        if($this->department_name!=null && $this->department_name!=""){
          $departmentRow=Department::find()->where(['title'=>trim($this->department_name)])->one();
          if($departmentRow==null){
            $departmentRow=new Department;
            $departmentRow->title=$this->department_name;
            $departmentRow->status=1;
            if($departmentRow->save()){
              $this->department_id=$departmentRow->id;
            }
          }else{
            $this->department_id=$departmentRow->id;
          }
        }
        if($this->unit_name!=null && $this->unit_name!=""){
            $unitRow=Units::find()->where(['title'=>trim($this->unit_name)])->one();
            if($unitRow==null){
                $unitRow=new Units();
                $unitRow->title=$this->unit_name;
                $unitRow->status=1;
                if($unitRow->save()){
                    $this->unit_id=$unitRow->id;
                }
            }else{
                $this->unit_id=$unitRow->id;
            }
        }

        $actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $parsedUrl = parse_url($actual_link, PHP_URL_PATH);
        $pathParts = explode('/', trim($parsedUrl, '/'));
        $action = end($pathParts);
        if($action == "create")
        {  
          $permissionGroups = AdminGroupPermissions::find()->where(['group_id' => $this->permission_group_id])->all();
          foreach ($permissionGroups as $key => $value) {
              $permission = new AdminGroupPermissionsStaff;
              $permission->group_id = $this->id;
              $permission->menu_id = $value->menu_id;
              $permission->save();
          }
        }else{

        }

        $userProfileInfo = UserProfileInfo::find()->where(['user_id'=>$this->id])->one();
        if($userProfileInfo==null){
          $userProfileInfo=new UserProfileInfo;
          $userProfileInfo->user_id=$this->id;
        }
        //$userProfileInfo->primary_contact=($this->primary_contact!=null ? $this->primary_contact : 0);
        $userProfileInfo->primary_contact=($this->primary_contact!=null ? $this->primary_contact : 1);

        if($this->job_title_id!=null)$userProfileInfo->job_title_id=$this->job_title_id;
        if($this->department_id!=null)$userProfileInfo->department_id=$this->department_id;
        if($this->unit_id!=null)$userProfileInfo->unit_id=$this->unit_id;
        if($this->mobile!=null)$userProfileInfo->mobile=$this->mobile;
        if($this->phone1!=null)$userProfileInfo->phone1=$this->phone1;
        if($this->phone2!=null)$userProfileInfo->phone2=$this->phone2;
        if($this->address!=null)$userProfileInfo->address=$this->address;

        if($this->country_id!=null)$userProfileInfo->country_id=$this->country_id;
        if($this->zone_id!=null)$userProfileInfo->zone_id=$this->zone_id;
        if($this->city!=null)$userProfileInfo->city=$this->city;
        if($this->postal_code!=null)$userProfileInfo->postal_code=$this->postal_code;

        if($this->background_info!=null)$userProfileInfo->background_info=$this->background_info;
        if($this->lead_type!=null)$userProfileInfo->lead_type=$this->lead_type;
        if($this->lead_source!=null)$userProfileInfo->lead_source=$this->lead_source;
        if($this->lead_date!=null)$userProfileInfo->lead_date=$this->lead_date;
        if($this->lead_score!=null)$userProfileInfo->lead_score=$this->lead_score;
        if($this->expected_close_date!=null)$userProfileInfo->expected_close_date=$this->expected_close_date;
        if($this->close_date!=null)$userProfileInfo->close_date=$this->close_date;
        if($this->confidence!=null)$userProfileInfo->confidence=$this->confidence;
        if($this->deal_status!=null)$userProfileInfo->deal_status=$this->deal_status;

        if($this->valuer_qualifications!=null)$userProfileInfo->valuer_qualifications=$this->valuer_qualifications;
        if($this->valuer_status!=null)$userProfileInfo->valuer_status=$this->valuer_status;
        if($this->valuer_experience_expertise!=null)$userProfileInfo->valuer_experience_expertise=$this->valuer_experience_expertise;


        if(Yii::$app->request->post()['User']['gender']!=null)
        {
          $userProfileInfo->gender=Yii::$app->request->post()['User']['gender'];
        }else{
          $userProfileInfo->gender = NULL;
        }

        if(Yii::$app->request->post()['User']['middlename']!=null)
        {
          $userProfileInfo->middlename=Yii::$app->request->post()['User']['middlename'];
        }else{
          $userProfileInfo->middlename = NULL;
        }

        if(Yii::$app->request->post()['User']['country_id']!=null)
        {
          $userProfileInfo->country_id=Yii::$app->request->post()['User']['country_id'];
        }else{
          $userProfileInfo->country_id = NULL;
        }

        if(Yii::$app->request->post()['User']['nationality']!=null)
        {
          $userProfileInfo->nationality=Yii::$app->request->post()['User']['nationality'];
        }else{
          $userProfileInfo->nationality = NULL;
        }

        if(Yii::$app->request->post()['User']['passport_number']!=null)
        {
          $userProfileInfo->passport_number=Yii::$app->request->post()['User']['passport_number'];
        }else{
          $userProfileInfo->passport_number = NULL;
        }

        if(Yii::$app->request->post()['User']['birth_date']!=null)
        {
          $userProfileInfo->birth_date=Yii::$app->request->post()['User']['birth_date'];
        }else{
          $userProfileInfo->birth_date = NULL;
        }

        if(Yii::$app->request->post()['User']['marital_status']!=null)
        {
          $userProfileInfo->marital_status=Yii::$app->request->post()['User']['marital_status'];
        }else{
          $userProfileInfo->marital_status = NULL;
        }

        if(Yii::$app->request->post()['User']['marriage_date']!=null)
        {
          $userProfileInfo->marriage_date=Yii::$app->request->post()['User']['marriage_date'];
        }else{
          $userProfileInfo->marriage_date = NULL;
        }

        if(Yii::$app->request->post()['User']['no_of_children']!=null)
        {
          $userProfileInfo->no_of_children=Yii::$app->request->post()['User']['no_of_children'];
        }else{
          $userProfileInfo->no_of_children = NULL;
        }

        if(Yii::$app->request->post()['User']['mother_name']!=null)
        {
          $userProfileInfo->mother_name=Yii::$app->request->post()['User']['mother_name'];
        }else{
          $userProfileInfo->mother_name = NULL;
        }

        if(Yii::$app->request->post()['User']['mother_profession']!=null)
        {
          $userProfileInfo->mother_profession=Yii::$app->request->post()['User']['mother_profession'];
        }else{
          $userProfileInfo->mother_profession = NULL;
        }

        if(Yii::$app->request->post()['User']['father_name']!=null)
        {
          $userProfileInfo->father_name=Yii::$app->request->post()['User']['father_name'];
        }else{
          $userProfileInfo->father_name = NULL;
        }

        if(Yii::$app->request->post()['User']['father_profession']!=null)
        {
          $userProfileInfo->father_profession=Yii::$app->request->post()['User']['father_profession'];
        }else{
          $userProfileInfo->father_profession = NULL;
        }

        if(Yii::$app->request->post()['User']['apartment']!=null)
        {
          $userProfileInfo->apartment=Yii::$app->request->post()['User']['apartment'];
        }else{
          $userProfileInfo->apartment = NULL;
        }

        if(Yii::$app->request->post()['User']['street']!=null)
        {
          $userProfileInfo->street=Yii::$app->request->post()['User']['street'];
        }else{
          $userProfileInfo->street = NULL;
        }

        if(Yii::$app->request->post()['User']['community']!=null)
        {
          $userProfileInfo->community=Yii::$app->request->post()['User']['community'];
        }else{
          $userProfileInfo->community = NULL;
        }

        if(Yii::$app->request->post()['User']['city']!=null)
        {
          $userProfileInfo->city=Yii::$app->request->post()['User']['city'];
        }else{
          $userProfileInfo->city = NULL;
        }

        if(Yii::$app->request->post()['User']['ijari']!=null)
        {
          $userProfileInfo->ijari=Yii::$app->request->post()['User']['ijari'];
        }else{
          $userProfileInfo->ijari = NULL;
        }


        if(Yii::$app->request->post()['User']['origin_apartment']!=null)
        {
          $userProfileInfo->origin_apartment=Yii::$app->request->post()['User']['origin_apartment'];
        }else{
          $userProfileInfo->origin_apartment = NULL;
        }

        if(Yii::$app->request->post()['User']['origin_street']!=null)
        {
          $userProfileInfo->origin_street=Yii::$app->request->post()['User']['origin_street'];
        }else{
          $userProfileInfo->origin_street = NULL;
        }

        if(Yii::$app->request->post()['User']['origin_country']!=null)
        {
          $userProfileInfo->origin_country=Yii::$app->request->post()['User']['origin_country'];
        }else{
          $userProfileInfo->origin_country = NULL;
        }

        if(Yii::$app->request->post()['User']['origin_city']!=null)
        {
          $userProfileInfo->origin_city=Yii::$app->request->post()['User']['origin_city'];
        }else{
          $userProfileInfo->origin_city = NULL;
        }

        if(Yii::$app->request->post()['User']['emergency_uae']!=null)
        {
          $userProfileInfo->emergency_uae=Yii::$app->request->post()['User']['emergency_uae'];
        }else{
          $userProfileInfo->emergency_uae = NULL;
        }

        if(Yii::$app->request->post()['User']['emergency_home']!=null)
        {
          $userProfileInfo->emergency_home=Yii::$app->request->post()['User']['emergency_home'];
        }else{
          $userProfileInfo->emergency_home = NULL;
        }

        if(Yii::$app->request->post()['User']['whatsapp_groups']!=null)
        {
          $userProfileInfo->whatsapp_groups= json_encode(Yii::$app->request->post()['User']['whatsapp_groups']);
        }else{
          $userProfileInfo->whatsapp_groups = NULL;
        }

        if(Yii::$app->request->post()['User']['personal_email']!=null)
        {
          $userProfileInfo->personal_email=Yii::$app->request->post()['User']['personal_email'];
        }else{
          $userProfileInfo->personal_email = NULL;
        }


        if(Yii::$app->request->post()['User']['group_emails']!=null)
        {
          $userProfileInfo->group_emails = json_encode(Yii::$app->request->post()['User']['group_emails']);
        }else{
          $userProfileInfo->group_emails = NULL;
        }

        if(isset(Yii::$app->request->post()['User']['qualification']) && (Yii::$app->request->post()['User']['qualification'] != 0 || Yii::$app->request->post()['User']['qualification'] != "" ) ){
          $qualificationArray = Yii::$app->request->post()['User']['qualification'];
          $indexes = array_keys($qualificationArray);
          $latestIndex = min($indexes);
          UserDegreeInfo::deleteAll(['user_id' => Yii::$app->request->post()['User']['qualification'][$latestIndex]['user_id']]);

          foreach (Yii::$app->request->post()['User']['qualification'] as $key => $data) {

              $user_degree_info = new UserDegreeInfo();
              $user_degree_info->degree_name = $data['degree_name'];
              $user_degree_info->institute_name = $data['institute_name'];
              $user_degree_info->passing_year = $data['passing_year'];
              $user_degree_info->percentage = $data['percentage'];
              $user_degree_info->degree_attested = $data['degree_attested'];
              $user_degree_info->user_id = $data['user_id'];
              if( isset($data['degree_attachment']) &&  $data['degree_attachment'] != "" )
              {
                $user_degree_info->degree_attachment = $data['degree_attachment'];
              }else{
                if( $_FILES['User']['name']['qualification'][$key]["degree_attachement"] != "" ){
                  $user_degree_info->degree_attachment = $data['degree_attachment'];
                  if( $_FILES['User']['name']['qualification'][$key]["degree_attachement"] == UPLOAD_ERR_OK) {
  
                    $fileTmpPath = $_FILES['User']['tmp_name']['qualification'][$key]["degree_attachement"];
                    $fileName = $_FILES['User']['name']['qualification'][$key]["degree_attachement"];
                    $fileSize = $_FILES['User']['size']['qualification'][$key]["degree_attachement"];
                    $fileType = $_FILES['User']['type']['qualification'][$key]["degree_attachement"];
                    $uploadDir = 'uploads/hr-uploads/';
  
                    if (!file_exists($uploadDir)) {
                        mkdir($uploadDir, 0777, true);
                    }
                
                    $destPath_1 = $uploadDir . $fileName;
                    if (move_uploaded_file($fileTmpPath, $destPath_1)) {
                        Yii::$app->getSession()->addFlash('success', "Degree Uploaded Successfully");
                    } else {
                        Yii::$app->getSession()->addFlash('error', "Unable to upload Degree");
                    }
                  } else {
                      Yii::$app->getSession()->addFlash('error', "Degree Not Selected");
                  }
  
                  if( isset($destPath_1) )
                  {
                    $user_degree_info->degree_attachment = $destPath_1;
                  }else{
                  $user_degree_info->degree_attachment = NULL;
  
                  }
                }
              }
              $user_degree_info->save();
          }
        }

        if(isset(Yii::$app->request->post()['User']['experience']) && (Yii::$app->request->post()['User']['experience'] != 0 || Yii::$app->request->post()['User']['experience'] != "" ) ){
          $expArray = Yii::$app->request->post()['User']['experience'];
          $indexes = array_keys($expArray);
          $latestIndex = min($indexes);
          UserExperience::deleteAll(['user_id' => Yii::$app->request->post()['User']['experience'][$latestIndex]['user_id']]);

          foreach (Yii::$app->request->post()['User']['experience'] as $key => $data) {
              $user_experience = new UserExperience();
              $user_experience->company_name = $data['company_name'];
              $user_experience->position = $data['position'];
              $user_experience->department = $data['department'];
              $user_experience->start_date = $data['start_date'];
              $user_experience->end_date = $data['end_date'];
              $user_experience->gross_salary = $data['gross_salary'];
              $user_experience->leave_reason = $data['leave_reason'];
              $user_experience->user_id = $data['user_id'];
              $user_experience->save();
          }
        }

        if(Yii::$app->request->post()['User']['total_experience']!=null)
        {
          $userProfileInfo->total_experience=Yii::$app->request->post()['User']['total_experience'];
        }else{
          $userProfileInfo->total_experience = NULL;
        }

        if(Yii::$app->request->post()['User']['relevant_experience']!=null)
        {
          $userProfileInfo->relevant_experience=Yii::$app->request->post()['User']['relevant_experience'];
        }else{
          $userProfileInfo->relevant_experience = NULL;
        }


        if(Yii::$app->request->post()['User']['ncc']!=null)
        {
          $userProfileInfo->ncc=Yii::$app->request->post()['User']['ncc'];
        }else{
          $userProfileInfo->ncc = NULL;
        }

        if(Yii::$app->request->post()['User']['ncc_breakage_cost']!=null)
        {
          $userProfileInfo->ncc_breakage_cost=Yii::$app->request->post()['User']['ncc_breakage_cost'];
        }else{
          $userProfileInfo->ncc_breakage_cost = NULL;
        }

        if(Yii::$app->request->post()['User']['notice_period']!=null)
        {
          $userProfileInfo->notice_period=Yii::$app->request->post()['User']['notice_period'];
        }else{
          $userProfileInfo->notice_period = NULL;
        }

        if(Yii::$app->request->post()['User']['early_breakage_cost']!=null)
        {
          $userProfileInfo->early_breakage_cost=Yii::$app->request->post()['User']['early_breakage_cost'];
        }else{
          $userProfileInfo->early_breakage_cost = NULL;
        }

        if(Yii::$app->request->post()['User']['visa_type']!=null)
        {
          $userProfileInfo->visa_type=Yii::$app->request->post()['User']['visa_type'];
        }else{
          $userProfileInfo->visa_type = NULL;
        }

        if( Yii::$app->request->post()['User']['visa_emirate']!=null )
        {
          $userProfileInfo->visa_emirate=Yii::$app->request->post()['User']['visa_emirate'];
        }else{
          $userProfileInfo->visa_emirate = NULL;
        }

        if(Yii::$app->request->post()['User']['joining_date']!=null)
        {
          $userProfileInfo->joining_date=Yii::$app->request->post()['User']['joining_date'];
        }else{
          $userProfileInfo->joining_date = NULL;
        }

        if(Yii::$app->request->post()['User']['visa_start_date']!=null)
        {
          $userProfileInfo->visa_start_date=Yii::$app->request->post()['User']['visa_start_date'];
        }else{
          $userProfileInfo->visa_start_date = NULL;
        }

        if(Yii::$app->request->post()['User']['visa_end_date']!=null)
        {
          $userProfileInfo->visa_end_date=Yii::$app->request->post()['User']['visa_end_date'];
        }else{
          $userProfileInfo->visa_end_date = NULL;
        }


        if(Yii::$app->request->post()['User']['driving_license']!=null)
        {
          $userProfileInfo->driving_license=Yii::$app->request->post()['User']['driving_license'];
        }else{
          $userProfileInfo->driving_license = NULL;
        }

        if(Yii::$app->request->post()['User']['hobby']!=null)
        {
          $userProfileInfo->hobby = json_encode(Yii::$app->request->post()['User']['hobby']);
        }else{
          $userProfileInfo->hobby = NULL;
        }

        if(Yii::$app->request->post()['User']['language']!=null)
        {
          $userProfileInfo->language = json_encode(Yii::$app->request->post()['User']['language']);
        }else{
          $userProfileInfo->language = NULL;
        }

        if(isset(Yii::$app->request->post()['User']['reference']) && (Yii::$app->request->post()['User']['reference'] != 0 || Yii::$app->request->post()['User']['reference'] != "" ) ){
          $referenceArray = Yii::$app->request->post()['User']['reference'];
          $indexes = array_keys($referenceArray);
          $latestIndex = min($indexes);
          UserReference::deleteAll(['user_id' => Yii::$app->request->post()['User']['reference'][$latestIndex]['user_id']]);

          foreach (Yii::$app->request->post()['User']['reference'] as $key => $data) {

              $user_reference = new UserReference();
              $user_reference->first_name = $data['first_name'];
              $user_reference->last_name = $data['last_name'];
              $user_reference->nationality = $data['nationality'];
              $user_reference->company = $data['company'];
              $user_reference->department = $data['department'];
              $user_reference->position = $data['position'];
              $user_reference->office_phone = $data['office_phone'];
              $user_reference->mobile_phone = $data['mobile_phone'];
              $user_reference->user_id = $data['user_id'];
              if( isset($data['ref_attachment']) &&  $data['ref_attachment'] != "" )
              {
                $user_reference->ref_attachment = $data['ref_attachment'];
              }else{
                if( $_FILES['User']['name']['reference'][$key]["ref_attachment"] != "" ){
                  $user_reference->ref_attachment = $data['ref_attachment'];
                  if( $_FILES['User']['name']['reference'][$latestIndex]["ref_attachment"] == UPLOAD_ERR_OK) {
  
                    $fileTmpPath = $_FILES['User']['tmp_name']['reference'][$key]["ref_attachment"];
                    $fileName = $_FILES['User']['name']['reference'][$key]["ref_attachment"];
                    $fileSize = $_FILES['User']['size']['reference'][$key]["ref_attachment"];
                    $fileType = $_FILES['User']['type']['reference'][$key]["ref_attachment"];
                    $uploadDir = 'uploads/hr-uploads/';
  
                    if (!file_exists($uploadDir)) {
                        mkdir($uploadDir, 0777, true);
                    }
                
                    $destPath_1 = $uploadDir . $fileName;
                    if (move_uploaded_file($fileTmpPath, $destPath_1)) {
                        Yii::$app->getSession()->addFlash('success', "Reference Uploaded Successfully");
                    } else {
                        Yii::$app->getSession()->addFlash('error', "Unable to upload Reference");
                    }
                  } else {
                      Yii::$app->getSession()->addFlash('error', "Reference Not Selected");
                  }
  
                  if( isset($destPath_1) )
                  {
                    $user_reference->ref_attachment = $destPath_1;
                  }else{
                  $user_reference->ref_attachment = NULL;
  
                  }
                }
              }
              $user_reference->save();
          }
        }

        if(Yii::$app->request->post()['User']['basic_salary']!=null)
        {
          $userProfileInfo->basic_salary=Yii::$app->request->post()['User']['basic_salary'];
        }else{
          $userProfileInfo->basic_salary = NULL;
        }

        if(Yii::$app->request->post()['User']['other_allowance']!=null)
        {
          $userProfileInfo->other_allowance=Yii::$app->request->post()['User']['other_allowance'];
        }else{
          $userProfileInfo->other_allowance = NULL;
        }

        if(Yii::$app->request->post()['User']['house_allowance']!=null)
        {
          $userProfileInfo->house_allowance=Yii::$app->request->post()['User']['house_allowance'];
        }else{
          $userProfileInfo->house_allowance = NULL;
        }

        if(Yii::$app->request->post()['User']['transport_allowance']!=null)
        {
          $userProfileInfo->transport_allowance=Yii::$app->request->post()['User']['transport_allowance'];
        }else{
          $userProfileInfo->transport_allowance = NULL;
        }

        if(Yii::$app->request->post()['User']['last_gross_salary']!=null)
        {
          $userProfileInfo->last_gross_salary=Yii::$app->request->post()['User']['last_gross_salary'];
        }else{
          $userProfileInfo->last_gross_salary = NULL;
        }

        if(Yii::$app->request->post()['User']['tax_rate']!=null)
        {
          $userProfileInfo->tax_rate=Yii::$app->request->post()['User']['tax_rate'];
        }else{
          $userProfileInfo->tax_rate = NULL;
        }

        if(Yii::$app->request->post()['User']['net_salary']!=null)
        {
          $userProfileInfo->net_salary=Yii::$app->request->post()['User']['net_salary'];
        }else{
          $userProfileInfo->net_salary = NULL;
        }

        if(Yii::$app->request->post()['User']['windmills_position']!=null)
        {
          $userProfileInfo->windmills_position=Yii::$app->request->post()['User']['windmills_position'];
        }else{
          $userProfileInfo->windmills_position = NULL;
        }

        if(Yii::$app->request->post()['User']['reports_to']!=null)
        {
          $userProfileInfo->reports_to=Yii::$app->request->post()['User']['reports_to'];
        }else{
          $userProfileInfo->reports_to = NULL;
        }

        if(Yii::$app->user->identity->user_type == 10 || Yii::$app->user->identity->user_type == 20)
        {

          if (isset($_FILES["User"]["error"]["picture"]) && $_FILES["User"]["error"]["picture"] == UPLOAD_ERR_OK) {
            // Get file details
            $fileTmpPath = $_FILES["User"]["tmp_name"]["picture"];
            $fileName = $_FILES["User"]["name"]["picture"];
            $fileSize = $_FILES["User"]["size"]["picture"];
            $fileType = $_FILES["User"]["type"]["picture"];
        
            // Specify the directory where you want to save the uploaded file
            $uploadDir = 'uploads/hr-uploads/';
        
            // Create the directory if it doesn't exist
            if (!file_exists($uploadDir)) {
                mkdir($uploadDir, 0777, true);
            }
        
            // Move the uploaded file to the specified directory
            $destPath_pic = $uploadDir . $fileName;
            if (move_uploaded_file($fileTmpPath, $destPath_pic)) {
                Yii::$app->getSession()->addFlash('success', "Picture Uploaded Successfully");
            } else {
                Yii::$app->getSession()->addFlash('error', "Unable to upload Picture");
            }
          } else {
              Yii::$app->getSession()->addFlash('error', "Picture Not Selected");
          }
      
          if(isset($_FILES["User"]["error"]["picture"]))
          {
            if( isset($destPath_pic) )
            {
                $userProfileInfo->picture = $destPath_pic;
            }
          }else{
            $userProfileInfo->picture = NULL;
          }
      
      
          if (isset($_FILES["User"]["error"]["cv"]) && $_FILES["User"]["error"]["cv"] == UPLOAD_ERR_OK) {
            // Get file details
            $fileTmpPath = $_FILES["User"]["tmp_name"]["cv"];
            $fileName = $_FILES["User"]["name"]["cv"];
            $fileSize = $_FILES["User"]["size"]["cv"];
            $fileType = $_FILES["User"]["type"]["cv"];
        
            // Specify the directory where you want to save the uploaded file
            $uploadDir = 'uploads/hr-uploads/';
        
            // Create the directory if it doesn't exist
            if (!file_exists($uploadDir)) {
                mkdir($uploadDir, 0777, true);
            }
        
            // Move the uploaded file to the specified directory
            $destPath_2 = $uploadDir . $fileName;
            if (move_uploaded_file($fileTmpPath, $destPath_2)) {
                Yii::$app->getSession()->addFlash('success', "CV Uploaded Successfully");
            } else {
                Yii::$app->getSession()->addFlash('error', "Unable to upload CV");
            }
          } else {
              Yii::$app->getSession()->addFlash('error', "CV Not Selected");
          }
      
          if(isset($_FILES["User"]["error"]["cv"]))
          {
            if( isset($destPath_2) )
            {
                $userProfileInfo->cv = $destPath_2;
            }
          }else{
            $userProfileInfo->cv = NULL;
          }
      
          if (isset($_FILES["User"]["error"]["cover_letter"]) && $_FILES["User"]["error"]["cover_letter"] == UPLOAD_ERR_OK) {
            // Get file details
            $fileTmpPath = $_FILES["User"]["tmp_name"]["cover_letter"];
            $fileName = $_FILES["User"]["name"]["cover_letter"];
            $fileSize = $_FILES["User"]["size"]["cover_letter"];
            $fileType = $_FILES["User"]["type"]["cover_letter"];
        
            // Specify the directory where you want to save the uploaded file
            $uploadDir = 'uploads/hr-uploads/';
        
            // Create the directory if it doesn't exist
            if (!file_exists($uploadDir)) {
                mkdir($uploadDir, 0777, true);
            }
        
            // Move the uploaded file to the specified directory
            $destPath_3 = $uploadDir . $fileName;
            if (move_uploaded_file($fileTmpPath, $destPath_3)) {
                Yii::$app->getSession()->addFlash('success', "Cover Letter Uploaded Successfully");
            } else {
                Yii::$app->getSession()->addFlash('error', "Unable to upload Cover Letter");
            }
          } else {
              Yii::$app->getSession()->addFlash('error', "Cover Letter Not Selected");
          }
      
          if(isset($_FILES["User"]["error"]["cover_letter"]))
          {
            if( isset($destPath_3) )
            {
                $userProfileInfo->cover_letter = $destPath_3;
            }
          }else{
            $userProfileInfo->cover_letter = NULL;
          }
      
          if (isset($_FILES["User"]["error"]["emirates_id"]) && $_FILES["User"]["error"]["emirates_id"] == UPLOAD_ERR_OK) {
            // Get file details
            $fileTmpPath = $_FILES["User"]["tmp_name"]["emirates_id"];
            $fileName = $_FILES["User"]["name"]["emirates_id"];
            $fileSize = $_FILES["User"]["size"]["emirates_id"];
            $fileType = $_FILES["User"]["type"]["emirates_id"];
        
            // Specify the directory where you want to save the uploaded file
            $uploadDir = 'uploads/hr-uploads/';
        
            // Create the directory if it doesn't exist
            if (!file_exists($uploadDir)) {
                mkdir($uploadDir, 0777, true);
            }
        
            // Move the uploaded file to the specified directory
            $destPath_4 = $uploadDir . $fileName;
            if (move_uploaded_file($fileTmpPath, $destPath_4)) {
                Yii::$app->getSession()->addFlash('success', "emirates id Uploaded Successfully");
            } else {
                Yii::$app->getSession()->addFlash('error', "Unable to upload emirates id");
            }
          } else {
              Yii::$app->getSession()->addFlash('error', "emirates id Not Selected");
          }
      
          if(isset($_FILES["User"]["error"]["emirates_id"]))
          {
            if( isset($destPath_4) )
            {
                $userProfileInfo->emirates_id = $destPath_4;
            }
          }else{
            $userProfileInfo->emirates_id = NULL;
          }
      
          if (isset($_FILES["User"]["error"]["residence_visa"]) && $_FILES["User"]["error"]["residence_visa"] == UPLOAD_ERR_OK) {
            // Get file details
            $fileTmpPath = $_FILES["User"]["tmp_name"]["residence_visa"];
            $fileName = $_FILES["User"]["name"]["residence_visa"];
            $fileSize = $_FILES["User"]["size"]["residence_visa"];
            $fileType = $_FILES["User"]["type"]["residence_visa"];
        
            // Specify the directory where you want to save the uploaded file
            $uploadDir = 'uploads/hr-uploads/';
        
            // Create the directory if it doesn't exist
            if (!file_exists($uploadDir)) {
                mkdir($uploadDir, 0777, true);
            }
        
            // Move the uploaded file to the specified directory
            $destPath_5 = $uploadDir . $fileName;
            if (move_uploaded_file($fileTmpPath, $destPath_5)) {
                Yii::$app->getSession()->addFlash('success', "residence_visa Uploaded Successfully");
            } else {
                Yii::$app->getSession()->addFlash('error', "Unable to upload residence_visa");
            }
          } else {
              Yii::$app->getSession()->addFlash('error', "residence_visa Not Selected");
          }
      
          if(isset($_FILES["User"]["error"]["residence_visa"]))
          {
            if( isset($destPath_5) )
            {
                $userProfileInfo->residence_visa = $destPath_5;
            }
          }else{
            $userProfileInfo->residence_visa = NULL;
          }
      
          if (isset($_FILES["User"]["error"]["work_permit"]) && $_FILES["User"]["error"]["work_permit"] == UPLOAD_ERR_OK) {
            // Get file details
            $fileTmpPath = $_FILES["User"]["tmp_name"]["work_permit"];
            $fileName = $_FILES["User"]["name"]["work_permit"];
            $fileSize = $_FILES["User"]["size"]["work_permit"];
            $fileType = $_FILES["User"]["type"]["work_permit"];
        
            // Specify the directory where you want to save the uploaded file
            $uploadDir = 'uploads/hr-uploads/';
        
            // Create the directory if it doesn't exist
            if (!file_exists($uploadDir)) {
                mkdir($uploadDir, 0777, true);
            }
        
            // Move the uploaded file to the specified directory
            $destPath_6 = $uploadDir . $fileName;
            if (move_uploaded_file($fileTmpPath, $destPath_6)) {
                Yii::$app->getSession()->addFlash('success', "work_permit Uploaded Successfully");
            } else {
                Yii::$app->getSession()->addFlash('error', "Unable to upload work_permit");
            }
          } else {
              Yii::$app->getSession()->addFlash('error', "work_permit Not Selected");
          }
      
          if(isset($_FILES["User"]["error"]["work_permit"]))
          {
            if( isset($destPath_6) )
            {
                $userProfileInfo->work_permit = $destPath_6;
            }
          }else{
            $userProfileInfo->work_permit = NULL;
          }
      
          if (isset($_FILES["User"]["error"]["last_contract"]) && $_FILES["User"]["error"]["last_contract"] == UPLOAD_ERR_OK) {
            // Get file details
            $fileTmpPath = $_FILES["User"]["tmp_name"]["last_contract"];
            $fileName = $_FILES["User"]["name"]["last_contract"];
            $fileSize = $_FILES["User"]["size"]["last_contract"];
            $fileType = $_FILES["User"]["type"]["last_contract"];
        
            // Specify the directory where you want to save the uploaded file
            $uploadDir = 'uploads/hr-uploads/';
        
            // Create the directory if it doesn't exist
            if (!file_exists($uploadDir)) {
                mkdir($uploadDir, 0777, true);
            }
        
            // Move the uploaded file to the specified directory
            $destPath_7 = $uploadDir . $fileName;
            if (move_uploaded_file($fileTmpPath, $destPath_7)) {
                Yii::$app->getSession()->addFlash('success', "last_contract Uploaded Successfully");
            } else {
                Yii::$app->getSession()->addFlash('error', "Unable to upload last_contract");
            }
          } else {
              Yii::$app->getSession()->addFlash('error', "last_contract Not Selected");
          }
      
          if(isset($_FILES["User"]["error"]["last_contract"]))
          {
            if( isset($destPath_7) )
            {
                $userProfileInfo->last_contract = $destPath_7;
            }
          }else{
            $userProfileInfo->last_contract = NULL;
          }
      
          if (isset($_FILES["User"]["error"]["offer_letter"]) && $_FILES["User"]["error"]["offer_letter"] == UPLOAD_ERR_OK) {
            // Get file details
            $fileTmpPath = $_FILES["User"]["tmp_name"]["offer_letter"];
            $fileName = $_FILES["User"]["name"]["offer_letter"];
            $fileSize = $_FILES["User"]["size"]["offer_letter"];
            $fileType = $_FILES["User"]["type"]["offer_letter"];
        
            // Specify the directory where you want to save the uploaded file
            $uploadDir = 'uploads/hr-uploads/';
        
            // Create the directory if it doesn't exist
            if (!file_exists($uploadDir)) {
                mkdir($uploadDir, 0777, true);
            }
        
            // Move the uploaded file to the specified directory
            $destPath_8 = $uploadDir . $fileName;
            if (move_uploaded_file($fileTmpPath, $destPath_8)) {
                Yii::$app->getSession()->addFlash('success', "offer_letter Uploaded Successfully");
            } else {
                Yii::$app->getSession()->addFlash('error', "Unable to upload offer_letter");
            }
          } else {
              Yii::$app->getSession()->addFlash('error', "offer_letter Not Selected");
          }
      
          if(isset($_FILES["User"]["error"]["offer_letter"]))
          {
            if( isset($destPath_8) )
            {
                $userProfileInfo->offer_letter = $destPath_8;
            }
          }else{
            $userProfileInfo->offer_letter = NULL;
          }
      
        }


        //job description
        if (isset($_FILES["User"]["error"]["job_description"]) && $_FILES["User"]["error"]["job_description"] == UPLOAD_ERR_OK) {
            // Get file details
            $fileTmpPath = $_FILES["User"]["tmp_name"]["job_description"];
            $fileName = $_FILES["User"]["name"]["job_description"];
            $fileSize = $_FILES["User"]["size"]["job_description"];
            $fileType = $_FILES["User"]["type"]["job_description"];

            // Specify the directory where you want to save the uploaded file
            $uploadDir = 'uploads/hr-uploads/';

            // Create the directory if it doesn't exist
            if (!file_exists($uploadDir)) {
                mkdir($uploadDir, 0777, true);
            }

            // Move the uploaded file to the specified directory
            $destPath_9 = $uploadDir . $fileName;
            if (move_uploaded_file($fileTmpPath, $destPath_9)) {
                Yii::$app->getSession()->addFlash('success', "Job Descriptiont Uploaded Successfully");
            } else {
                Yii::$app->getSession()->addFlash('error', "Unable to upload Job Descriptiont");
            }
        } else {
            Yii::$app->getSession()->addFlash('error', "Job Descriptiont Not Selected");
        }

        if(isset($_FILES["User"]["error"]["job_description"]))
        {
            if( isset($destPath_9) )
            {
                $userProfileInfo->job_description = $destPath_9;
            }
        }else{
            $userProfileInfo->job_description = NULL;
        }


        //job kpi
        if (isset($_FILES["User"]["error"]["kpi"]) && $_FILES["User"]["error"]["kpi"] == UPLOAD_ERR_OK) {
            // Get file details
            $fileTmpPath = $_FILES["User"]["tmp_name"]["kpi"];
            $fileName = $_FILES["User"]["name"]["kpi"];
            $fileSize = $_FILES["User"]["size"]["kpi"];
            $fileType = $_FILES["User"]["type"]["kpi"];

            // Specify the directory where you want to save the uploaded file
            $uploadDir = 'uploads/hr-uploads/';

            // Create the directory if it doesn't exist
            if (!file_exists($uploadDir)) {
                mkdir($uploadDir, 0777, true);
            }

            // Move the uploaded file to the specified directory
            $destPath_10 = $uploadDir . $fileName;
            if (move_uploaded_file($fileTmpPath, $destPath_10)) {
                Yii::$app->getSession()->addFlash('success', "Job Descriptiont Uploaded Successfully");
            } else {
                Yii::$app->getSession()->addFlash('error', "Unable to upload Job Descriptiont");
            }
        } else {
            Yii::$app->getSession()->addFlash('error', "Job Descriptiont Not Selected");
        }

        if(isset($_FILES["User"]["error"]["kpi"]))
        {
            if( isset($destPath_10) )
            {
                $userProfileInfo->kpi = $destPath_10;
            }
        }else{
            $userProfileInfo->kpi = NULL;
        }

    }

    
    
    // echo $userProfileInfo->valuer_qualifications;
    // echo $userProfileInfo->valuer_status;
    // echo $userProfileInfo->valuer_status;


    //$this->signature_img_name=$this->generateRandomString();
  //  $this->signature_img_name=$this->generateRandomString();
    $this->signature_file= UploadedFile::getInstance($this,'signature_file');
//     if(!empty($this->signature_file)){
//      $this->signature_img_name=$this->signature_file->name;
//
//      // print_r($this->signature_img_name);
//      // die();
//
// }
    // $userProfileInfo->signature_img_name =$this->signature_img_name;
    //
    //   $uploadObject = Yii::$app->get('s3bucket')->upload('property/images/'.$randomName, $this->imageFile->tempName);


if ($this->validate()) {
    if ($this->signature_file !=null) {

      // Yii::$app->db->createCommand("update ".self::tableName()." set front_image=:fi where id=:id",[
      //   'fi'=>$this->front_image,
      //   ':id'=>$this->id,
      //   ])->execute();

      $uniqueName = trim((str_replace(' ', '', $this->signature_file->baseName) . uniqid()));
      $randomName = $string = preg_replace('/\s+/', '', $uniqueName) . '.' .$this->signature_file->extension;
      $userProfileInfo->signature_img_name =   $randomName;
      $uploadObject = Yii::$app->get('s3bucket')->upload('/images/'.$randomName, $this->signature_file->tempName);


      if ($uploadObject) {
          // check if CDN host is available then upload and get cdn URL.
          if (Yii::$app->get('s3bucket')->cdnHostname) {
              $url = Yii::$app->get('s3bucket')->getCdnUrl($randomName);
          } else {
           $url = Yii::$app->get('s3bucket')->getUrl('/images/'.$randomName);
          }
      }
      //
      // delete function when image is already exixts
      // $this->old_file = $this->front_image;
      // if($this->old_file!='' && $this->old_file!=$this->imagename &&
      // file_exists(Yii::$app->get('s3bucket')->getUrl('property/images/'.$this->old_file))){
      //   Yii::$app->get('s3bucket')->delete('property/images/'.$this->old_file);
      //     }
       }
     }else {
              return false;
              }

      
      if($this->is_save == 0){
        // dd($this->is_save);
        if($userProfileInfo->save()){
          // echo "userProfileInfo user model";
          // dd($userProfileInfo);
        }else{
          
          if ($userProfileInfo->hasErrors()) {
            foreach ($userProfileInfo->getErrors() as $error) {
                if (count($error) > 0) {
                    foreach ($error as $key => $val) {
                        // echo $val."<br>";
                        Yii::$app->getSession()->addFlash('error', $val);
                    }
                }
            }
            // die();
          }
    
        }
      }


		//Saving Managers
		if($this->manager_id!=null){
			ContactManager::deleteAll(['and',['contact_id'=>$this->id],['not in','staff_id',$this->manager_id]]);
			foreach($this->manager_id as $key=>$val){
				$managerRow=ContactManager::find()->where(['contact_id'=>$this->id,'staff_id'=>$val])->one();
				if($managerRow==null){
					$managerRow=new ContactManager;
					$managerRow->contact_id=$this->id;
					$managerRow->staff_id=$val;
					$managerRow->save();
				}
			}
		}
    $managerRow=ContactManager::find()->where(['contact_id'=>$this->id,'staff_id'=>$this->created_by]);
    if(!$managerRow->exists()){
      $managerRow=new ContactManager;
      $managerRow->contact_id=$this->id;
      $managerRow->staff_id=$this->created_by;
      $managerRow->save();
    }

    if ($this->file!== null && $this->image!=null) {
      if($this->oldfile!=null && $this->image!=$this->oldfile && file_exists(Yii::$app->fileHelperFunctions->memberImagePath['abs'].$this->oldfile)){
        unlink(Yii::$app->fileHelperFunctions->memberImagePath['abs'].$this->oldfile);
      }
      $this->file->saveAs(Yii::$app->fileHelperFunctions->memberImagePath['abs'].$this->image);
    }
     parent::afterSave($insert, $changedAttributes);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getCompany()
  {
    return $this->hasOne(Company::className(), ['id' => 'company_id']);
  }

	/**
	* Get Assigned Staff members
	* @return \yii\db\ActiveQuery
	*/
	public function getManagerIdz()
	{
		return ContactManager::find()->select(['staff_id'])->where(['contact_id'=>$this->id])->asArray()->all();
	}
}