<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

class ValuationForQuotation extends ActiveRecordFull
{
    public static function tableName()
    {
        return 'valuation';
    }
    public function rules()
    {
        return [
        [['client_id','purpose_of_valuation','building_info','property_id','property_category','community','sub_community',
        'tenure','city','unit_number','building_number','plot_number','street','floor_number'], 'required'],
        
            [['client_id', 'service_officer_name', 'building_info', 'property_id', 'property_category', 'tenure', 'unit_number', 'status', 'building_number', 'plot_number', 'floor_number', 'purpose_of_valuation', 'created_by', 'updated_by', 'trashed', 'trashed_by','no_of_owners'], 'integer'],
            [['instruction_date', 'target_date', 'created_at', 'updated_at', 'trashed_at','special_assumption','extent_of_investigation','market_summary','approval_id','approval_status','land_size','valuation_status'], 'safe'],
            [['payment_plan'], 'string'],
            [['reference_number', 'client_name_passport'], 'string', 'max' => 100],
            [['client_reference', 'street'], 'string', 'max' => 255],
        ];
    }
}
