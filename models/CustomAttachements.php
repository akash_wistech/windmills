<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "custom_attachements".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $quantity
 * @property int|null $valuation_id
 */
class CustomAttachements extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'custom_attachements';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['valuation_id'], 'integer'],
            [['name', 'quantity'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'quantity' => 'Quantity',
            'valuation_id' => 'Valuation ID',
        ];
    }
}
