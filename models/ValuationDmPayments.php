<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "valuation_dm_payments".
 *
 * @property int $id
 * @property string|null $installment_date
 * @property int|null $no_of_days
 * @property int|null $percentage
 * @property float|null $amount
 * @property float|null $present_value
 * @property int $index_id
 * @property int $valuation_id
 * @property int $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class ValuationDmPayments extends ActiveRecordFull
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'valuation_dm_payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['installment_date', 'created_at', 'updated_at', 'trashed_at'], 'safe'],
            [['no_of_days', 'index_id', 'valuation_id', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['amount', 'present_value', 'percentage'], 'number'],
            [['index_id', 'valuation_id'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'installment_date' => 'Installment Date',
            'no_of_days' => 'No Of Days',
            'percentage' => 'Percentage',
            'amount' => 'Amount',
            'present_value' => 'Present Value',
            'index_id' => 'Index ID',
            'valuation_id' => 'Valuation ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }


    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Developer Margin Payments';
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->id;
    }
}
