<?php

namespace app\models;
use app\modules\wisnotify\models\NotificationTemplate;
use yii\web\UploadedFile;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

use Yii;
use app\modules\wisnotify\listners\NotifyEvent;

/**
 * This is the model class for table "valuation_quotation".
 *
 * @property int $id
 * @property int|null $reference_number
 * @property string|null $client_name
 * @property string|null $client_customer_name
 * @property string|null $inquiry_date
 * @property string|null $expiry_date
 * @property string|null $purpose_of_valuation
 * @property int|null $no_of_properties
 * @property int|null $recommended_fee
 * @property int|null $final_fee_approved
 * @property string|null $turn_around_time
 * @property string|null $valuer_name
 * @property string|null $date
 * @property int $status
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 */
class ValuationQuotation extends \yii\db\ActiveRecord
{
  public $building,$property,$property_category,$tenure,$city,$community,$sub_community,$building_number,$owner_name;
  public $plot_number,$street,$unit_number,$floor_number,$land_size,$built_up_area,$valuation_type,$latitude,$longitude;
  public $no_of_floors,$no_of_units,$repeat_valuation,$valuation_date;
  public $complexity,$type_of_valuation,$number_of_comparables,$valuation_approach,$date_of_inspection;
  public $property_quotation_fee,$property_toe_fee,$property_quotation_tat,$property_toe_tat;
  public $existingRecordIdz;
  public $other,$required_documents,$documents;

  public $payment_slip_file,$toe_document_file;

  public $property_doc;

  public $recieve_docs=[];

  public  $calculate_all = false;



    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'valuation_quotation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['client_name','client_type','client_customer_name','purpose_of_valuation','inquiry_date','expiry_date',
            'advance_payment_terms','no_of_properties','relative_discount','turn_around_time','valuer_name','date','building','tenure','assumptions',
            'building_number','owner_name','plot_number', 'street', 'unit_number', 'floor_number', 'land_size','built_up_area',
            'latitude','longitude','complexity','type_of_valuation','number_of_comparables','valuation_approach','no_of_floors', 'no_of_units',
            'repeat_valuation','valuation_date', 'name','phone_number','email','quotation_recommended_fee','quotation_turn_around_time',
            'toe_final_fee','toe_turned_around_time','payment_slip','toe_document','other','required_documents','documents','toe_final_turned_around_time','date_of_inspection','property_quotation_fee','property_toe_fee','property_quotation_tat','property_toe_tat'], 'safe'],

            [[  'property', 'property_category', 'city', 'community', 'sub_community','status','recommended_fee', 'final_fee_approved',
            'quotation_status'], 'safe'],

            [[  'property', 'property_category', 'city', 'community', 'sub_community','status','recommended_fee', 'final_fee_approved',
            'quotation_status', 'recieve_docs'], 'safe'],

            [['created_by', 'updated_by', 'deleted_by'], 'integer'],
            [[ 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            
            [['reference_number','existingRecordIdz','related_to_buyer_reason','related_to_owner_reason','related_to_client_reason',
            'related_to_property_reason', 'related_to_buyer','related_to_owner','related_to_client','related_to_property'], 'safe'],

            [['payment_slip_file'], 'file'],
            [['toe_document_file'], 'file'],
            [['property_doc'], 'file'],


            [['phone_number'], 'integer'],

            [[
              'building','unit_number','floor_number','no_of_floors','built_up_area','plot_number','building_number','advance_payment_terms','client_name','client_type','advance_payment_terms','street','purpose_of_valuation','valuation_date','property_quotation_fee'], 'required', 'on' => 'update'],

            [['client_name', 'client_type','client_customer_name','purpose_of_valuation','advance_payment_terms','other_intended_users','scope_of_service'], 'required', 'on' => 'clientDetails'],
];
    }

public function afterSave($insert, $changedAttributes)
 {
   // $prop = $this->building;
   // echo "<pre>";
   // print_r($prop);
   // echo "</pre>";
   // die();
  if ($this->building !=null && $this->no_of_properties>0) {
    // echo "<pre>";
    // print_r($this->building);
    // echo "</pre>";
    // die();
    $savedIdz = [];
   foreach ($this->building as $key => $value) {
     // echo "<pre>";
     // print_r($value);
     // echo "</pre>";
     // die();
     if (isset($this->existingRecordIdz[$key]) && $this->existingRecordIdz[$key] !=null) {
          $child = ValuationQuotationPropertiesDetails::find()
                   ->where(['id'=>$this->existingRecordIdz[$key]])
                   ->one();
                 }else {
                    $child = new ValuationQuotationPropertiesDetails;
                    $child->valuation_quotation_id = $this->id;
                 }
                    $child->building = $this->building[$key];
                    $child->property = $this->property[$key];
                    $child->property_category = $this->property_category[$key];
                    $child->tenure = $this->tenure[$key];
                    $child->city = $this->city[$key];
                    $child->community = $this->community[$key];
                    $child->sub_community = $this->sub_community[$key];
                    $child->building_number = $this->building_number[$key];
                    $child->owner_name = $this->owner_name[$key];
                    $child->plot_number = $this->plot_number[$key];
                    $child->street = $this->street[$key];
                    $child->unit_number = $this->unit_number[$key];
                    $child->floor_number = $this->floor_number[$key];
                    $child->land_size = $this->land_size[$key];
                    $child->built_up_area = $this->built_up_area[$key];
                    // $child->valuation_type = $this->valuation_type[$key];
                    $child->latitude = $this->latitude[$key];
                    $child->longitude = $this->longitude[$key];
                    $child->date_of_inspection = $this->date_of_inspection[$key];
                    // $child->phone_number = $this->phone_number[$key];
                    // $child->email = $this->email[$key];
                    $child->no_of_floors = $this->no_of_floors[$key];
                    $child->no_of_units	 = $this->no_of_units	[$key];
                    $child->repeat_valuation = $this->repeat_valuation[$key];
                    $child->valuation_date = $this->valuation_date[$key];
                    $child->complexity = $this->complexity[$key];
                    $child->type_of_valuation = $this->type_of_valuation[$key];
                    $child->number_of_comparables = $this->number_of_comparables[$key];
                    $child->valuation_approach = $this->valuation_approach[$key];
                    $child->property_quotation_fee = $this->property_quotation_fee[$key];
                    $child->property_quotation_tat = $this->property_quotation_tat[$key];
                    $child->property_toe_fee = $this->property_toe_fee[$key];
                    $child->property_toe_tat = $this->property_toe_tat[$key];

                      // if(!empty($this->required_documents)){
                      //     $child->required_documents =  implode(",",$this->documents[$key]);
                      // }




                      if(!empty($this->recieve_docs[$key])){
                        foreach ($this->recieve_docs[$key] as $key1 => $value1) {
                          // print_r($key1.','.$value1);
                          // echo "<br>";
                          if ($value1!="") {
                          $child1 = ProposalDocs::find()
                                   ->where(['quotation_id'=>$this->id, 'building_id' => $this->building[$key], 'document_id' =>$key1 ])
                                   ->one();
                          if ($child1 ==null) {
                                      $child1 = new ProposalDocs;
                                      $child1->quotation_id = $this->id;
                                      }
                                       $child1->building_id = $this->building[$key];
                                       $child1->property_id = $this->property[$key];
                                       $child1->document_id = $key1;
                                       $child1->attachment = $value1;
                                       $child1->save();

                                      }
                                  }
                                       // die();
                              }





                    if ($child->save()) {
                        $savedIdz[] = $child->id;
                             yii::$app->getsession()->addFlash('success', yii::t('app', 'Uploaded successfully'));
                             }
                          else {
                             if ($child->hasErrors()) {
                                 foreach ($child->getErrors() as $errors) {

                                     foreach ($errors as $key => $val) {
                                         die($val);
                                         yii::$app->getSession()->addFlash('error', $val);
                                     }
                                 }
                             }
                         }

                       }

                       ValuationQuotationPropertiesDetails::deleteAll(['and',['valuation_quotation_id' => $this->id],['not in','id',$savedIdz]]);
                     }






    parent::afterSave($insert, $changedAttributes);
 }




public function beforeSave($insert)
 {
   // echo "<pre>";
   // print_r($_FILES);
   // echo "</pre>";
   // echo "<pre>";
   // print_r($_POST);
   // echo "</pre>";
   // die();
     if (parent::beforeSave($insert)) {

       //Save images in DB and Cloud
       $this->payment_slip_file = UploadedFile::getInstance($this, 'payment_slip_file');
       $this->toe_document_file = UploadedFile::getInstance($this, 'toe_document_file');

         if (!empty($this->payment_slip_file)) {
                 $uniqueName = trim((str_replace(' ', '', $this->payment_slip_file->baseName) . uniqid()));
                 $randomName = $string = preg_replace('/\s+/', '', $uniqueName) . '.' .$this->payment_slip_file->extension;
                 $this->payment_slip = 'quotation/images/'.$randomName;
                 $uploadObject = Yii::$app->get('s3bucket')->upload('quotation/images/'.$randomName, $this->payment_slip_file->tempName);
                 if ($uploadObject) {
                     // check if CDN host is available then upload and get cdn URL.
                     if (Yii::$app->get('s3bucket')->cdnHostname) {
                         $url = Yii::$app->get('s3bucket')->getCdnUrl($randomName);
                     } else {
                      $url = Yii::$app->get('s3bucket')->getUrl('quotation/images/'.$randomName);
                     }
                 }
         }


         if (!empty($this->toe_document_file)) {
                 $uniqueName = trim((str_replace(' ', '', $this->toe_document_file->baseName) . uniqid()));
                 $randomName = $string = preg_replace('/\s+/', '', $uniqueName) . '.' .$this->toe_document_file->extension;
                 $this->toe_document = 'quotation/images/'.$randomName;
                 $uploadObject = Yii::$app->get('s3bucket')->upload('quotation/images/'.$randomName, $this->toe_document_file->tempName);
                 if ($uploadObject) {
                     // check if CDN host is available then upload and get cdn URL.
                     if (Yii::$app->get('s3bucket')->cdnHostname) {
                         $url = Yii::$app->get('s3bucket')->getCdnUrl($randomName);
                     } else {
                      $url = Yii::$app->get('s3bucket')->getUrl('quotation/images/'.$randomName);
                     }
                 }
         }
        //Save images in DB and Cloud end



         return true;
     }
     return false;
 }




 public function getClient()
 {
     return $this->hasOne(Company::className(), ['id' => 'client_name']);
 }

 public function getChildData()
 {
     return $this->hasMany(ValuationQuotationPropertiesDetails::className(), ['valuation_quotation_id' => 'id']);
 }

 public function getBuilding()
 {
     return $this->hasOne(Buildings::className(), ['id' => 'building']);
 }

 public function getProperty()
 {
     return $this->hasOne(Properties::className(), ['id' => 'property_id']);
 }



public function QuotationPdf($condition)
{
  require_once( __DIR__ .'/../components/tcpdf/QuotationPdf.php');
  // create new PDF document
  $pdf = new \QuotationPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  // set document information
  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('Windmills');
  $pdf->SetTitle($this->reference_number);
  $pdf->SetSubject('Quotation');
  $pdf->SetKeywords($this->reference_number);

  // set default header data
  $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

  // set header and footer fonts
  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

  // set default monospaced font
  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

  // set margins
  $pdf->SetMargins(10, 35, 10);
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

  // set auto page breaks
  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
  $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

  // set default font subsetting mode
  $pdf->setFontSubsetting(true);

  // Set font
  // dejavusans is a UTF-8 Unicode font, if you only need to
  // print standard ASCII chars, you can use core fonts like
  // helvetica or times to reduce file size.
  $pdf->SetFont('times', '', 14, '', true);

  // Add a page
  // This method has several options, check the source code documentation for more information.
  $pdf->AddPage('P','A4');

  $qpdf=Yii::$app->controller->renderPartial('/valuation-quotation/quotation-new',['id'=>$this->id]);


  if ($condition==true) {
    $pdf->writeHTML($qpdf, true, false, false, false, '');
    $pdf->Output('Quotation: '.$this->reference_number, 'I');
    exit();

  }

  if ($condition==false) {
    $pdfFile = $this->reference_number.'.pdf';
    $fullPath = Yii::$app->params['quote_uploads_abs_path'].'/'.$pdfFile;
    $pdf->writeHTML($qpdf, true, false, false, false, '');
    $pdf->Output($fullPath, 'F');
    $attachments=[$fullPath];
     // $template = NotificationTemplate::findOne(1);
$notifyData = [
                    'client' => $this->client,
                    'attachments' => $attachments,
                    //'subject' => 'Windmills Valuation Services is keen to value for you',
                    'replacements'=>[
                      '{clientName}'=>$this->client->title,
                      // '{reference_number}'=>$this->reference_number
                    ],
                  ];
      NotifyEvent::fire('quotation.send', $notifyData);


  }
}




public function ToePdf($condition)
{
      // echo "toe model function";die();
  require_once( __DIR__ .'/../components/tcpdf/ProposalPdf.php');
  // create new PDF document
  $pdf = new \ProposalPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

  // set document information
  $pdf->SetCreator(PDF_CREATOR);
  $pdf->SetAuthor('Windmills');
  $pdf->SetTitle($this->reference_number);
  $pdf->SetSubject('Quotation');
  $pdf->SetKeywords($this->reference_number);

  $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

  $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
  $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

  $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

  $pdf->SetMargins(10, 35, 10);
  $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
  $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

  $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
  $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

  $pdf->setFontSubsetting(true);

  $pdf->SetFont('times', '', 14, '', true);

  $pdf->AddPage('P','A4');

  $qpdf=Yii::$app->controller->renderPartial('/valuation-quotation/toe-new',['id'=>$this->id]);

    if ($condition==true) {
      $pdf->writeHTML($qpdf, true, false, false, false, '');
      $pdf->Output('TOE '.$this->reference_number, 'I');
        exit();
    }

    if ($condition==false) {
        
        $pdfFile = $this->reference_number.'.pdf';
        $fullPath = Yii::$app->params['toe_uploads_abs_path'].'/'.$pdfFile;
        $pdf->writeHTML($qpdf, true, false, false, false, '');
        $pdf->Output($fullPath, 'F');
        $attachments=[$fullPath];

            $notifyData = [
                            'client' => $this->client,
                            'attachments' => $attachments,
                            'replacements'=>[
                              '{clientName}'=>$this->client->title,
                              // '{reference_number}'=>$this->reference_number
                            ],
                          ];

            NotifyEvent::fire('toe.send', $notifyData);
                

     
    

    }
}






    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reference_number' => 'Reference Number',
            'client_name' => 'Client Name',
            'client_customer_name' => 'Client Representative Name',
            'inquiry_date' => 'Inquiry Date',
            'expiry_date' => 'Expiry Date',
            'purpose_of_valuation' => 'Purpose Of Valuation',
            'no_of_properties' => 'No Of Properties',
            'recommended_fee' => 'Recommended Fee',
            'final_fee_approved' => 'Final Fee Approved',
            'turn_around_time' => 'Turn Around Time',
            'valuer_name' => 'Valuer Name',
            'date' => 'Client Report Expected Date',
            'assumptions' => 'Special Assumptions',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'deleted_by' => 'Deleted By',
            //property fields
            'building' => 'Project Name',
            'valuation_date' => 'Specific Valuation Date',
            'city' => 'City Of Property',
            'tenure' => 'Tenure Of Property',
            'community' => 'Community Of Property',
            'sub_community' => 'Sub Community Of Property',
            'building_number' => 'Building Number Of Property',
            'plot_number' => 'Plot Number Of Property',
            'street' => 'Street Of Property',
            'no_of_floors' => 'No Of Floors Of Property',
            'no_of_units' => 'No Of Units Of Property',
            'latitude' => 'Latitude Of Property',
            'longitude' => 'Longitude Of Property',
            'valuation_approach' => 'Valuation Approach Of Property',

        ];
    }


public function behaviors()
{
return [
      'timestamp' => [
      'class' => 'yii\behaviors\TimestampBehavior',
      'attributes' => [
      ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
      ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
    ],
      'value' => new Expression('NOW()'),
    ],
    'blameable' => [
      'class' => BlameableBehavior::className(),
      'createdByAttribute' => 'created_by',
      'updatedByAttribute' => 'updated_by',
    ],
    ];
}





}
