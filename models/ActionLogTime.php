<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%action_log_time}}".
*
* @property integer $action_log_id
* @property integer $start_dt
* @property integer $end_dt
* @property string $hours
* @property string $minutes
*/
class ActionLogTime extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%action_log_time}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['action_log_id', 'start_dt'], 'required'],
      [['action_log_id','hours'], 'integer'],
      [['start_dt','end_dt','minutes'], 'string'],
    ];
  }

  public static function primaryKey()
  {
  	return ['action_log_id','start_dt'];
  }
}
