<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "land_solds".
 *
 * @property int $id
 * @property string $transaction_type
 * @property string $subtype
 * @property string $sales_sequence
 * @property string $reidin_ref
 * @property string $transaction_date
 * @property string $community
 * @property string|null $property
 * @property string $property_type
 * @property string|null $unit
 * @property string|null $bedrooms
 * @property string|null $floor
 * @property string|null $parking
 * @property string|null $balcony_area
 * @property string $size_sqf
 * @property string $land_size
 * @property string $amount
 * @property string $sqf
 * @property string|null $developer
 * @property int|null $status
 */
class LandSolds extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'land_solds';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'transaction_type', 'subtype', 'sales_sequence', 'reidin_ref', 'transaction_date', 'community', 'property_type', 'size_sqf', 'land_size', 'amount', 'sqf'], 'required'],
            [['id', 'status'], 'integer'],
            [['transaction_type', 'reidin_ref', 'size_sqf', 'land_size', 'amount'], 'string', 'max' => 13],
            [['subtype'], 'string', 'max' => 34],
            [['sales_sequence'], 'string', 'max' => 9],
            [['transaction_date'], 'string', 'max' => 10],
            [['community'], 'string', 'max' => 33],
            [['property'], 'string', 'max' => 51],
            [['property_type'], 'string', 'max' => 27],
            [['unit'], 'string', 'max' => 12],
            [['bedrooms'], 'string', 'max' => 5],
            [['floor', 'parking', 'balcony_area'], 'string', 'max' => 30],
            [['sqf'], 'string', 'max' => 8],
            [['developer'], 'string', 'max' => 54],
            [['id'], 'unique'],
            [['new_building_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'transaction_type' => 'Transaction Type',
            'subtype' => 'Subtype',
            'sales_sequence' => 'Sales Sequence',
            'reidin_ref' => 'Reidin Ref',
            'transaction_date' => 'Transaction Date',
            'community' => 'Community',
            'property' => 'Property',
            'property_type' => 'Property Type',
            'unit' => 'Unit',
            'bedrooms' => 'Bedrooms',
            'floor' => 'Floor',
            'parking' => 'Parking',
            'balcony_area' => 'Balcony Area',
            'size_sqf' => 'Size Sqf',
            'land_size' => 'Land Size',
            'amount' => 'Amount',
            'sqf' => 'Sqf',
            'developer' => 'Developer',
            'status' => 'Status',
        ];
    }
}
