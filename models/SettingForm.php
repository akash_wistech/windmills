<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* SettingForm is the model behind the setting form.
*/
class SettingForm extends Model
{
  public $emirates_country_id,$service_list_id,$currency_sign,$quickbook_client_id,$quickbook_client_secret,$quickbook_access_token,$quickbook_refresh_token,$quickbook_redirect_uri,$quickbook_oauth_scope,$quickbook_realm_id,$quickbook_tax_id;

  public $save_bayut_building;
  public $sold_percentage_calculation,$comparables_percentage_calculation,$previous_percentage_calculation;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      //[['emirates_country_id','service_list_id','currency_sign'], 'required'],
      [[
        'emirates_country_id','service_list_id',
      ], 'integer'],
      [[
        'currency_sign','quickbook_access_token','quickbook_client_id','quickbook_client_secret','quickbook_refresh_token','quickbook_redirect_uri','quickbook_oauth_scope','quickbook_realm_id','quickbook_tax_id','save_bayut_building'
          ,'sold_percentage_calculation','comparables_percentage_calculation','previous_percentage_calculation'
      ], 'string'],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'emirates_country_id' => 'Emirates Country',
      'service_list_id' => 'Services List',
      'currency_sign' => 'Currency Sign',
    ];
  }

  /**
  * save the settings data.
  */
  public function save()
  {
    if ($this->validate()) {
      $post=Yii::$app->request->post();



      foreach($post['SettingForm'] as $key=>$val){
        $setting=Setting::find()->where("config_name='$key'")->one();
        if($setting==null){
          $setting=new Setting;
          $setting->config_name=$key;
        }
        $setting->config_value=$val;
        $setting->save();
      }
      return true;
    } else {
        echo "<pre>";
        print_r($this->errors);
        die;
      return false;
    }
  }
}
