<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* ALForms is the model behind the action log forms.
*/
class ALForms extends Model
{
  public $module_type,$module_id,$rec_type,$comments,$visibility;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['module_type','module_id','rec_type','comments','visibility'],'required'],
      [['module_type','comments'],'string'],
      [['module_id'],'integer'],
      [['comments'],'trim'],
    ];
  }

  public function getCheckAlready()
  {
    $checkTimeArr=Yii::$app->helperFunctions->getStartEndDateTimeFromNow("-5 second");
    return ActionLog::find()
    ->where([
      'module_type'=>$this->module_type,
      'module_id'=>$this->module_id,
      'rec_type'=>$this->rec_type,
      'created_by'=>Yii::$app->user->identity->id,
    ])
    ->andWhere(['between','created_at',$checkTimeArr['start'],$checkTimeArr['end']]);
  }
}
