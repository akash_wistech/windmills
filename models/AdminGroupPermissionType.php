<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%admin_group_permission_type}}".
*
* @property integer $id
* @property integer $group_id
* @property integer $menu_id
*/
class AdminGroupPermissionType extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%admin_group_permission_type}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['group_id', 'menu_id', 'controller_id', 'list_type'], 'required'],
      [['group_id', 'menu_id', 'list_type'], 'integer'],
      [['controller_id'], 'string'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'group_id' => Yii::t('app', 'Group ID'),
      'menu_id' => Yii::t('app', 'Menu ID'),
      'controller_id' => Yii::t('app', 'Controller'),
      'list_type' => Yii::t('app', 'Type'),
    ];
  }
}
