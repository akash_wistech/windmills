<?php 
// models/ClientValuation.php

namespace app\models;

use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "client_valuation".
 *
 * @property int $id
 * @property string|null $title
 * @property int|null $status_verified
 * @property string|null $status_verified_at
 * @property int|null $status_verified_by
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property string|null $deleted_at
 * @property int|null $deleted_by
 */

class ClientValuation extends ActiveRecordFull
{
    public $received_docs = [];
    public static function tableName()
    {
        return 'client_valuation';
    }

    public function rules()
    {
        return [
            [['inspection_type','purpose_of_valuation','urgency','client_customer_prefix','client_customer_fname','client_customer_lname','contact_person_prefix','contact_person_fname','contact_person_lname','contact_person_phone','plot_number','street','property_id','property_category','tenure','community'], 'required'],
            [['instructing_person_fname','instructing_person_lname','contact_person_fname','contact_person_lname'], 'string', 'max' => 255],
            [['client_customer_fname','client_customer_lname','instructing_person_fname','instructing_person_lname','contact_person_fname','contact_person_lname'], 'match', 'pattern' => '/^[A-Za-z ]+$/'],
            [['client_customer_phone', 'client_customer_landline', 'instructing_person_phone', 'instructing_person_landline', 'contact_person_phone', 'contact_person_landline'], 'match', 
                'pattern' => '/^\d{7,7}$/',
                'message' => 'The {attribute} must be 7 digits number.',
            ],
            [['client_customer_email','instructing_person_email','contact_person_email'], 'email'],
            [['client_customer_phone_code','instructing_person_landline_code','client_customer_landline_code','inspection_type','urgency','inspection_type','urgency','building_info','property_id','property_category','status_verified', 'status_verified_by', 'created_by', 'updated_by', 'deleted_by','tenure','floor_number','purpose_of_valuation','number_of_units','email_status_client','email_status_wm','client_id'], 'integer'],
            [['reference_number','client_reference','client_customer_phone','client_customer_landline','client_customer_email','instructing_person_phone_code','instructing_person_phone','instructing_person_landline','instructing_person_email','contact_person_prefix','instructing_person_prefix','client_customer_prefix','contact_person_phone_code','contact_person_phone','contact_person_landline_code','contact_person_landline','contact_person_email','status_verified_at', 'created_at', 'updated_at', 'deleted_at','plot_number','street','unit_number','valuation_date','inspection_date','inspection_time','building_number','community','sub_community','country','city','built_up_area_value','built_up_area_unit','built_up_area','net_leasable_area_value','net_leasable_area_unit','net_leasable_area','plot_area_value','plot_area_unit','plot_area','received_docs','development_type','extended','completion_status','location_pin','key','trustee_id'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reference_number' => 'Reference Number',
            'client_reference' => 'Client Reference',
            'client_id' => 'Client ID',
            'client_customer_prefix' => 'Customer Prefix',
            'client_customer_fname' => 'Customer First Name',
            'client_customer_lname' => 'Customer Last Name',
            'client_customer_phone' => 'Customer Phone',
            'client_customer_landline' => 'Customer Landline',
            'client_customer_email' => 'Customer Email',
            'instructing_person_prefix' => 'Instructing Person Prefix',
            'instructing_person_fname' => 'Instructing Person First Name',
            'instructing_person_lname' => 'Instructing Person Last Name',
            'instructing_person_phone' => 'Instructing Person Phone',
            'instructing_person_landline' => 'Instructing Person Landline',
            'instructing_person_email' => 'Instructing Person Email',
            'contact_person_prefix' => 'Contact Person Prefix',
            'contact_person_fname' => 'Contact Person First Name',
            'contact_person_lname' => 'Contact Person Last Name',
            'contact_person_phone' => 'Contact Person Phone',
            'contact_person_landline' => 'Contact Person Landline',
            'contact_person_email' => 'Contact Person Email',
            'inspection_type' => 'Inspection Type',
            'urgency' => 'Urgency',
            'purpose_of_valuation' => 'Purpose Of Valuation',
            'valuation_date' => 'Valuation Date (Only for back-dated valuations)',
            'inspection_date' => 'Preferred Inspection Date',
            'inspection_time' => 'Preferred Inspection Time',
            'property_id' => 'Property Type',
            'property_category' => 'Property Category',
            'tenure' => 'Tenure',
            'building_info' => 'Building/Project Name',
            'building_number' => 'Building Number',
            'plot_number' => 'Plot Number',
            'development_type' => 'Standard / Non-Standard',
            'extended' => 'Extended / Improved ?',
            'location_pin' => 'Location Pin',
            'completion_status' => 'Completion Status',
            'street' => 'Street',
            'community' => 'Community',
            'sub_community' => 'Sub Community',
            'city' => 'City',
            'country' => 'Country',
            'unit_number' => 'Unit Number',
            'floor_number' => 'Floor Number',
            'number_of_units' => 'Number Of Units',
            'built_up_area' => 'Built Up Area (Sq.Ft.)',
            'net_leasable_area' => 'Net Leasable Area (Sq.Ft.)',
            'plot_area' => 'Plot Area (Sq.Ft.)',
            'status_verified' => 'Status Verified',
            'status_verified_at' => 'Status Verified At',
            'status_verified_by' => 'Status Verified By',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
        ];
    }


    public function getBuilding()
    {
        return $this->hasOne(Buildings::className(), ['id' => 'building_info']);
    }

    public function getCommunities()
    {
        return $this->hasOne(Communities::className(), ['id' => 'community']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubCommunities()
    {
        return $this->hasOne(SubCommunities::className(), ['id' => 'sub_community']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Properties::className(), ['id' => 'property_id']);
    }

    public function getClient()
    {
        return $this->hasOne(Company::className(), ['id' => 'client_id']);
    }

    public function getKmImages()
    {
        return $this->hasMany(MeetingImages::class, ['meeting_id' => 'key'])->where(['image_type'=>'cli_val_image']);
    }

}
