<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%setting}}".
*
* @property integer $id
* @property string $config_name
* @property string $config_value
*/
class SettingVal extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%setting_val}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['config_name','config_value'],'safe'],
      [['config_name','config_value'],'string'],
      [['config_name','config_value'],'trim'],
        [['config_type'],'safe'],
    ];
  }
}
