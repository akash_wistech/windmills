<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
* This is the model class for table "{{%workflow_stage}}".
*
* @property integer $id
* @property integer $workflow_id
* @property string $title
* @property string $color_code
* @property string $descp
* @property integer $conversion_rate
* @property integer $value
* @property string $require_previous
* @property string $require_comment
* @property integer $sort_order
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
* @property integer $trashed
* @property string $trashed_at
* @property integer $trashed_by
*/
class WorkflowStage extends ActiveRecordFull
{
	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%workflow_stage}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['workflow_id','title'],'required'],
			[['created_at','updated_at','trashed_at'],'safe'],
			[[
				'workflow_id','require_previous','require_comment','sort_order','created_by','updated_by','trashed','trashed_by'
			],'integer'],
			[['conversion_rate','value'],'number'],
			[[
				'title','color_code','descp'
			],'string'],
      [['title','color_code'],'trim'],
      [['color_code'],'default','value'=>'#000'],
		];
	}

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'workflow_id' => Yii::t('app', 'Process'),
			'title' => Yii::t('app', 'Stage Name'),
			'descp' => Yii::t('app', 'Description'),
			'completion_day' => Yii::t('app', 'Completion Days'),
			'conversion_rate' => Yii::t('app', 'Conversion Rate'),
			'value' => Yii::t('app', 'Value'),
			'require_previous' => Yii::t('app', 'Require Previous'),
			'require_comment' => Yii::t('app', 'Require Comment'),
			'sort_order' => Yii::t('app', 'Sort Order'),
			'created_at' => Yii::t('app', 'Created At'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'updated_by' => Yii::t('app', 'Updated By'),
			'trashed' => Yii::t('app', 'Trashed'),
			'trashed_at' => Yii::t('app', 'Trashed At'),
			'trashed_by' => Yii::t('app', 'Trashed By'),
		];
	}

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecTitle()
  {
    return $this->title;
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecType()
  {
    return 'Workflow Stage';
  }

	/**
	* @return \yii\db\ActiveQuery
	*/
	public function getWorkflow()
	{
		return $this->hasOne(Workflow::className(), ['id' => 'workflow_id']);
	}
}
