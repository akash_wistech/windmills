<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Opportunity;
use yii\db\Expression;

/**
* OpportunitySearch represents the model behind the search form of `app\models\Opportunity`.
*/
class OpportunitySearch extends Opportunity
{
  public $pageSize,$wf,$wfi;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','rec_type','service_type','module_id','wf','created_by','updated_by','pageSize'],'integer'],
      [['title','module_type','expected_close_date','descp','created_at'],'string'],
      [['quote_amount'],'number'],
      [['wfi','status_verified'],'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $query = $this->generateQuery($params);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
      'sort' => false,//$this->defaultSorting,
    ]);

    return $dataProvider;
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function searchMy($params)
  {
    $query = $this->generateQuery($params);

    //Check Listing type allowed
    if(Yii::$app->menuHelperFunction->getListingTypeByController('contact')==2){
      $query->innerJoin(ModuleManager::tableName(),ModuleManager::tableName().".module_id=".Prospect::tableName().".id and ".ModuleManager::tableName().".module_type='".$this->moduleTypeId."'");
      $query->andFilterWhere([
        ModuleManager::tableName().'.staff_id' => Yii::$app->user->identity->id]);
    }

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
      'sort' => false,//$this->defaultSorting,
    ]);

    return $dataProvider;
  }

  /**
  * Generates query for the search
  *
  * @param array $params
  *
  * @return ActiveRecord
  */
  public function generateQuery($params)
  {
    $this->load($params);
    $query = Opportunity::find()
    // ->select([
    //   Opportunity::tableName().'.id',
    //   'fullname'=>'CONCAT('.Opportunity::tableName().'.firstname," ",'.Opportunity::tableName().'.lastname)',
    //   Opportunity::tableName().'.email',
    //   Opportunity::tableName().'.mobile',
    //   'company_name'=>Company::tableName().'.title',
    //   Opportunity::tableName().'.sales_stage',
    //   Opportunity::tableName().'.lead_source',
    //   Opportunity::tableName().'.expected_close_date',
    //   Opportunity::tableName().'.quote_amount',
    //   Opportunity::tableName().'.probability',
    //   Opportunity::tableName().'.created_at',
    // ])
    ->asArray();

    // grid filtering conditions
    $query->andFilterWhere([
      Opportunity::tableName().'.id' => $this->id,
      Opportunity::tableName().'.rec_type' => $this->rec_type,
      Opportunity::tableName().'.expected_close_date' => $this->expected_close_date,
      Opportunity::tableName().'.status_verified' => $this->status_verified,
    ]);
    $query->andWhere([
      'is',Opportunity::tableName().'.deleted_at',new Expression('null')
    ]);

    return $query;
  }

  /**
  * Default Sorting Options
  *
  * @param array $params
  *
  * @return Array
  */
  public function getDefaultSorting()
  {
    return [
      'defaultOrder' => ['created_at'=>SORT_DESC],
      // 'attributes' => [
      //   'fullname' => [
      //     'asc' => [Opportunity::tableName().'.firstname' => SORT_ASC, Opportunity::tableName().'.lastname' => SORT_ASC],
      //     'desc' => [Opportunity::tableName().'.firstname' => SORT_DESC, Opportunity::tableName().'.lastname' => SORT_DESC],
      //   ],
      //   'email',
      //   'mobile',
      //   'company_name' => [
      //     'asc' => [Company::tableName().'.title' => SORT_ASC],
      //     'desc' => [Company::tableName().'.title' => SORT_DESC],
      //   ],
      //   'sales_stage',
      //   'lead_source',
      //   'created_at',
      // ]
    ];
  }

  public function getProcess()
  {
    if (($model = Workflow::findOne($this->wf)) !== null) {
        return $model;
    }
    throw new NotFoundHttpException('The requested page does not exist.');
  }
}
