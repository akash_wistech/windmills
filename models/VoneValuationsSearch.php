<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\VoneValuations;

/**
 * VoneValuationsSearch represents the model behind the search form of `app\models\VoneValuations`.
 */
class VoneValuationsSearch extends VoneValuations
{
    public $client_customer_name,$property,$community,$building,$type,$valuation_status;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'wm_reference', 'reason', 'client_requirements', 'status_verified', 'status_verified_at', 'status_verified_by', 'created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by','client_reference'], 'safe'],
            [['client_customer_name','property','community','building','type','valuation_status','error_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$grid_view_id=null)
    {
        $query = VoneValuations::find()->where(['grid_view_id'=>$grid_view_id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'wm_reference' => $this->wm_reference,
            'client_reference' => $this->client_reference,
            'reason' => $this->reason,
            'client_requirements' => $this->client_requirements,
            'error_status' => $this->error_status,
            'status_verified' => $this->status_verified,
            'status_verified_at' => $this->status_verified_at,
            'status_verified_by' => $this->status_verified_by,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        return $dataProvider;
    }
}
