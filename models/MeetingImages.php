<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "meeting_images".
 *
 * @property int $id
 * @property int|null $meeting_id
 * @property string|null $attachment
 * @property string|null $image_type
 */
class MeetingImages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'meeting_images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['meeting_id'], 'integer'],
            [['attachment'], 'string'],
            [['image_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'meeting_id' => 'Meeting ID',
            'attachment' => 'Attachment',
            'image_type' => 'Image Type',
        ];
    }
}
