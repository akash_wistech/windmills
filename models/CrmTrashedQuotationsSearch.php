<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CrmQuotations;

/**
 * CrmTrashedQuotationsSearch represents the model behind the search form of `app\models\CrmQuotations`.
 */
class CrmTrashedQuotationsSearch extends CrmQuotations
{
    public $client_type;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'no_of_properties', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['reference_number', 'client_name', 'client_customer_name', 'scope_of_service','advance_payment_terms', 'related_to_buyer', 'related_to_owner', 'related_to_client', 'related_to_property', 'related_to_buyer_reason', 'related_to_owner_reason', 'related_to_client_reason', 'related_to_property_reason', 'name', 'phone_number', 'email', 'relative_discount', 'turn_around_time', 'valuer_name', 'date', 'quotation_status', 'payment_slip', 'toe_document', 'quotation_recommended_fee', 'quotation_turn_around_time', 'toe_final_fee', 'toe_final_turned_around_time', 'assumptions', 'created_at', 'updated_at', 'deleted_at','quotation_status','client_type'], 'safe'],
            [['recommended_fee', 'final_fee_approved'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CrmQuotations::find();
        $query = $query->innerJoin('company', '`company`.`id` = `crm_quotations`.`client_name`');
        $query = $query->where(['not', ['crm_quotations.trashed' => null]]);
        // $query = $query->where(['crm_quotations.trashed' => 1]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // echo "<pre>"; print_r($this); echo "</pre>"; die();

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'no_of_properties' => $this->no_of_properties,
            'recommended_fee' => $this->recommended_fee,
            'final_fee_approved' => $this->final_fee_approved,
            'status' => $this->status,
            'quotation_status'=> $this->quotation_status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
            'client_type' => $this->client_type,
        ]);

        $query->andFilterWhere(['like', 'reference_number', $this->reference_number])
            ->andFilterWhere(['like', 'client_name', $this->client_name])
            ->andFilterWhere(['like', 'client_customer_name', $this->client_customer_name])
            ->andFilterWhere(['like', 'scope_of_service', $this->scope_of_service])
            ->andFilterWhere(['like', 'advance_payment_terms', $this->advance_payment_terms])
            ->andFilterWhere(['like', 'related_to_buyer', $this->related_to_buyer])
            ->andFilterWhere(['like', 'related_to_owner', $this->related_to_owner])
            ->andFilterWhere(['like', 'related_to_client', $this->related_to_client])
            ->andFilterWhere(['like', 'related_to_property', $this->related_to_property])
            ->andFilterWhere(['like', 'related_to_buyer_reason', $this->related_to_buyer_reason])
            ->andFilterWhere(['like', 'related_to_owner_reason', $this->related_to_owner_reason])
            ->andFilterWhere(['like', 'related_to_client_reason', $this->related_to_client_reason])
            ->andFilterWhere(['like', 'related_to_property_reason', $this->related_to_property_reason])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'phone_number', $this->phone_number])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'relative_discount', $this->relative_discount])
            ->andFilterWhere(['like', 'turn_around_time', $this->turn_around_time])
            ->andFilterWhere(['like', 'valuer_name', $this->valuer_name])
            ->andFilterWhere(['like', 'date', $this->date])
            ->andFilterWhere(['like', 'quotation_status', $this->quotation_status])
            ->andFilterWhere(['like', 'payment_slip', $this->payment_slip])
            ->andFilterWhere(['like', 'toe_document', $this->toe_document])
            ->andFilterWhere(['like', 'quotation_recommended_fee', $this->quotation_recommended_fee])
            ->andFilterWhere(['like', 'quotation_turn_around_time', $this->quotation_turn_around_time])
            ->andFilterWhere(['like', 'toe_final_fee', $this->toe_final_fee])
            ->andFilterWhere(['like', 'toe_final_turned_around_time', $this->toe_final_turned_around_time])
            ->andFilterWhere(['like', 'assumptions', $this->assumptions]);

        return $dataProvider;
    }
}
