<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%action_log_manager}}".
*
* @property integer $action_log_id
* @property integer $staff_id
*/
class ActionLogManager extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%action_log_manager}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['action_log_id', 'staff_id'], 'required'],
      [['action_log_id', 'staff_id'], 'integer'],
    ];
  }

  public static function primaryKey()
  {
  	return ['action_log_id','staff_id'];
  }
}
