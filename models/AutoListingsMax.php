<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "auto_listings_max".
 *
 * @property int $id
 * @property int|null $too_less_records_limit_from
 * @property int|null $too_less_records_limit_to
 * @property int|null $too_less_bua_percentage
 * @property int|null $too_less_price_percentage
 * @property int|null $too_less_date
 * @property int|null $less_records_limit_from
 * @property int|null $less_records_limit_to
 * @property int|null $less_bua_percentage
 * @property int|null $less_price_percentage
 * @property int|null $less_date
 * @property int|null $moderate_records_limit_from
 * @property int|null $moderate_records_limit_to
 * @property int|null $moderate_bua_percentage
 * @property int|null $moderate_price_percentage
 * @property int|null $moderate_date
 * @property int|null $many_records_limit_from
 * @property int|null $many_records_limit_to
 * @property int|null $many_bua_percentage
 * @property int|null $many_price_percentage
 * @property int|null $many_date
 * @property int|null $too_many_records_limit_from
 * @property int|null $too_many_records_limit_to
 * @property int|null $too_many_bua_percentage
 * @property int|null $too_many_price_percentage
 * @property int|null $too_many_date
 * @property int $status
 * @property string|null $data_type
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $search_limit
 * @property string|null $status_verified_at
 * @property int|null $status_verified_by
 * @property int|null $city_id
 */
class AutoListingsMax extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auto_listings_max';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['too_less_records_limit_from', 'too_less_records_limit_to',
                 'too_less_date', 'less_records_limit_from', 'less_records_limit_to',
                 'less_date', 'moderate_records_limit_from',
                'moderate_records_limit_to',
                'moderate_date', 'many_records_limit_from', 'many_records_limit_to',
                'many_date', 'too_many_records_limit_from', 'too_many_records_limit_to',
                  'too_many_date', 'status', 'updated_by',
                'search_limit', 'status_verified_by', 'city_id'], 'integer'],
            [['data_type'], 'string'],
            [['updated_at', 'status_verified_at'], 'safe'],
            [['too_less_bua_price_percentage_1', 'too_less_bua_price_percentage_2','too_less_bua_price_percentage_3'], 'safe'],
            [['less_bua_price_percentage_1', 'less_bua_price_percentage_2','less_bua_price_percentage_3'], 'safe'],
            [['moderate_bua_price_percentage_1', 'moderate_bua_price_percentage_2','moderate_bua_price_percentage_3'], 'safe'],
            [['many_bua_price_percentage_1', 'many_bua_price_percentage_2','many_bua_price_percentage_3'], 'safe'],
            [['too_many_bua_price_percentage_1', 'too_many_bua_price_percentage_2','too_many_bua_price_percentage_3','search_limit_sc'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'too_less_records_limit_from' => 'Too Less Records Limit From',
            'too_less_records_limit_to' => 'Too Less Records Limit To',
            'too_less_bua_percentage' => 'Too Less Bua Percentage',
            'too_less_price_percentage' => 'Too Less Price Percentage',
            'too_less_date' => 'Too Less Date',
            'less_records_limit_from' => 'Less Records Limit From',
            'less_records_limit_to' => 'Less Records Limit To',
            'less_bua_percentage' => 'Less Bua Percentage',
            'less_price_percentage' => 'Less Price Percentage',
            'less_date' => 'Less Date',
            'moderate_records_limit_from' => 'Moderate Records Limit From',
            'moderate_records_limit_to' => 'Moderate Records Limit To',
            'moderate_bua_percentage' => 'Moderate Bua Percentage',
            'moderate_price_percentage' => 'Moderate Price Percentage',
            'moderate_date' => 'Moderate Date',
            'many_records_limit_from' => 'Many Records Limit From',
            'many_records_limit_to' => 'Many Records Limit To',
            'many_bua_percentage' => 'Many Bua Percentage',
            'many_price_percentage' => 'Many Price Percentage',
            'many_date' => 'Many Date',
            'too_many_records_limit_from' => 'Too Many Records Limit From',
            'too_many_records_limit_to' => 'Too Many Records Limit To',
            'too_many_bua_percentage' => 'Too Many Bua Percentage',
            'too_many_price_percentage' => 'Too Many Price Percentage',
            'too_many_date' => 'Too Many Date',
            'status' => 'Status',
            'data_type' => 'Data Type',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'search_limit' => 'Search Limit',
            'status_verified_at' => 'Status Verified At',
            'status_verified_by' => 'Status Verified By',
            'city_id' => 'City ID',
        ];
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->id;
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'AutoListingsMax';
    }

    public function getCityName()
    {
        return $this->hasOne(Zone::className(), ['id' => 'city_id']);
    }

}
