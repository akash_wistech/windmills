<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
* ContactSearch represents the model behind the search form of `app\models\User`.
*/
class ContactSearch extends User
{
  public $name,$cname,$mobile,$job_title,$department,$pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','permission_group','status','created_by','updated_by','pageSize'],'integer'],
      [['name','cname','email','mobile','job_title'],'string'],
      [['existing_valuation_contact','bilal_contact','valuation_contact','prospect_contact','property_owner_contact','inquiry_valuations_contact','icai_contact','broker_contact','developer_contact'],'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $query = $this->generateQuery($params);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
      'sort' => $this->defaultSorting,
    ]);

    return $dataProvider;
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function searchMy($params)
  {
    $query = $this->generateQuery($params);

    //Check Listing type allowed
    if(Yii::$app->menuHelperFunction->getListingTypeByController('contact')==2){
      $query->innerJoin(ContactManager::tableName(),ContactManager::tableName().".contact_id=".User::tableName().".id");
      $query->andFilterWhere([
        ContactManager::tableName().'.staff_id' => Yii::$app->user->identity->id]);
    }

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
      'sort' => $this->defaultSorting,
    ]);

    return $dataProvider;
  }
    public function generateQuery($params)
    {
        $this->load($params);
        $query = User::find()
            ->select([
                User::tableName().'.id',
                'image',
                'name'=>'CONCAT(firstname," ",lastname)',
                'email',
                UserProfileInfo::tableName().'.mobile',
                'job_title'=>JobTitle::tableName().'.title',
                'cname'=>Company::tableName().'.title',
                User::tableName().'.status',
            ])
            ->leftJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().'.user_id='.User::tableName().'.id')
            ->leftJoin(JobTitle::tableName(),JobTitle::tableName().'.id='.UserProfileInfo::tableName().'.job_title_id')
            ->leftJoin(Company::tableName(),Company::tableName().'.id='.User::tableName().'.company_id')
            // ->andWhere(Company::tableName().'.status!=2')
            //   ->andWhere(Company::tableName().'.bilal_contact=1')
            ->asArray();

        // grid filtering conditions
        $query->andFilterWhere([
            User::tableName().'.id' => $this->id,
            'user_type' => 0,
            User::tableName().'.status' => $this->status,
            User::tableName().'.trashed' => 0,
        ]);

        $query->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
            ->andFilterWhere(['like','email',$this->email])
            ->andFilterWhere(['like',JobTitle::tableName().'.title',$this->job_title])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',UserProfileInfo::tableName().'.mobile',$this->mobile]);

        return $query;
    }

    public function searchMyall($params)
    {
        $query = $this->generateQueryall($params);

        //Check Listing type allowed
        if(Yii::$app->menuHelperFunction->getListingTypeByController('contact')==2){
            $query->innerJoin(ContactManager::tableName(),ContactManager::tableName().".contact_id=".User::tableName().".id");
            $query->andFilterWhere([
                ContactManager::tableName().'.staff_id' => Yii::$app->user->identity->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
            ],
            'sort' => $this->defaultSorting,
        ]);

        return $dataProvider;
    }
    public function generateQueryall($params)
    {
        $this->load($params);
        $query = User::find()
            ->select([
                User::tableName().'.id',
                'image',
                'name'=>'CONCAT(firstname," ",lastname)',
                'email',
                UserProfileInfo::tableName().'.mobile',
                'job_title'=>JobTitle::tableName().'.title',
                'cname'=>Company::tableName().'.title',
                User::tableName().'.status',
            ])
            ->leftJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().'.user_id='.User::tableName().'.id')
            ->leftJoin(JobTitle::tableName(),JobTitle::tableName().'.id='.UserProfileInfo::tableName().'.job_title_id')
            ->leftJoin(Company::tableName(),Company::tableName().'.id='.User::tableName().'.company_id')
            // ->andWhere(Company::tableName().'.status!=2')
            //   ->andWhere(Company::tableName().'.bilal_contact=1')
            ->asArray();

        // grid filtering conditions
        $query->andFilterWhere([
            User::tableName().'.id' => $this->id,
            'user_type' => 0,
            User::tableName().'.status' => $this->status,
            User::tableName().'.trashed' => 0,
        ]);

        $query->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
            ->andFilterWhere(['like','email',$this->email])
            ->andFilterWhere(['like',JobTitle::tableName().'.title',$this->job_title])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',UserProfileInfo::tableName().'.mobile',$this->mobile]);

        return $query;
    }

    public function searchMyBilal($params)
    {
        $query = $this->generateQuery1($params);

        //Check Listing type allowed
        if(Yii::$app->menuHelperFunction->getListingTypeByController('contact')==2){
            $query->innerJoin(ContactManager::tableName(),ContactManager::tableName().".contact_id=".User::tableName().".id");
            $query->andFilterWhere([
                ContactManager::tableName().'.staff_id' => Yii::$app->user->identity->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
            ],
            'sort' => $this->defaultSorting,
        ]);

        return $dataProvider;
    }
    public function searchMyBank($params)
    {
        $query = $this->generateQuerybank($params);

        //Check Listing type allowed
        if(Yii::$app->menuHelperFunction->getListingTypeByController('contact')==2){
            $query->innerJoin(ContactManager::tableName(),ContactManager::tableName().".contact_id=".User::tableName().".id");
            $query->andFilterWhere([
                ContactManager::tableName().'.staff_id' => Yii::$app->user->identity->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
            ],
            'sort' => $this->defaultSorting,
        ]);

        return $dataProvider;
    }
    public function generateQuery1($params)
    {
        $this->load($params);
        $query = User::find()
            ->select([
                User::tableName().'.id',
                'image',
                'name'=>'CONCAT(firstname," ",lastname)',
                'email',
                UserProfileInfo::tableName().'.mobile',
                'job_title'=>JobTitle::tableName().'.title',
                'cname'=>Company::tableName().'.title',
                User::tableName().'.status',
            ])
            ->leftJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().'.user_id='.User::tableName().'.id')
            ->leftJoin(JobTitle::tableName(),JobTitle::tableName().'.id='.UserProfileInfo::tableName().'.job_title_id')
            ->leftJoin(Company::tableName(),Company::tableName().'.id='.User::tableName().'.company_id')
            // ->andWhere(Company::tableName().'.status!=2')
            ->andWhere('bilal_contact=1')
            ->asArray();

        // grid filtering conditions
        $query->andFilterWhere([
            User::tableName().'.id' => $this->id,
            'user_type' => 0,
            User::tableName().'.status' => $this->status,
            User::tableName().'.trashed' => 0,
        ]);

        $query->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
            ->andFilterWhere(['like','email',$this->email])
            ->andFilterWhere(['like',JobTitle::tableName().'.title',$this->job_title])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',UserProfileInfo::tableName().'.mobile',$this->mobile]);

        return $query;
    }
    public function generateQuerybank($params)
    {
        $this->load($params);
        $query = User::find()
            ->select([
                User::tableName().'.id',
                'image',
                'name'=>'CONCAT(firstname," ",lastname)',
                'email',
                UserProfileInfo::tableName().'.mobile',
                'job_title'=>JobTitle::tableName().'.title',
                'cname'=>Company::tableName().'.title',
                User::tableName().'.status',
            ])
            ->leftJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().'.user_id='.User::tableName().'.id')
            ->leftJoin(JobTitle::tableName(),JobTitle::tableName().'.id='.UserProfileInfo::tableName().'.job_title_id')
            ->leftJoin(Company::tableName(),Company::tableName().'.id='.User::tableName().'.company_id')
             ->andWhere(Company::tableName().'.client_type="bank"')
           // ->andWhere('bilal_contact=1')
            ->asArray();

        // grid filtering conditions
        $query->andFilterWhere([
            User::tableName().'.id' => $this->id,
            'user_type' => 0,
            User::tableName().'.status' => $this->status,
            User::tableName().'.trashed' => 0,
        ]);

        $query->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
            ->andFilterWhere(['like','email',$this->email])
            ->andFilterWhere(['like',JobTitle::tableName().'.title',$this->job_title])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',UserProfileInfo::tableName().'.mobile',$this->mobile]);

        return $query;
    }

    public function searchMyexisting($params)
    {
        $query = $this->generateQuery2($params);

        //Check Listing type allowed
        if(Yii::$app->menuHelperFunction->getListingTypeByController('contact')==2){
            $query->innerJoin(ContactManager::tableName(),ContactManager::tableName().".contact_id=".User::tableName().".id");
            $query->andFilterWhere([
                ContactManager::tableName().'.staff_id' => Yii::$app->user->identity->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
            ],
            'sort' => $this->defaultSorting,
        ]);

        return $dataProvider;
    }
    public function generateQuery2($params)
    {
        $this->load($params);
        $query = User::find()
            ->select([
                User::tableName().'.id',
                'image',
                'name'=>'CONCAT(firstname," ",lastname)',
                'email',
                UserProfileInfo::tableName().'.mobile',
                'job_title'=>JobTitle::tableName().'.title',
                'cname'=>Company::tableName().'.title',
                User::tableName().'.status',
            ])
            ->leftJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().'.user_id='.User::tableName().'.id')
            ->leftJoin(JobTitle::tableName(),JobTitle::tableName().'.id='.UserProfileInfo::tableName().'.job_title_id')
            ->leftJoin(Company::tableName(),Company::tableName().'.id='.User::tableName().'.company_id')
            // ->andWhere(Company::tableName().'.status!=2')
            ->andWhere('existing_valuation_contact=1')
            ->asArray();

        // grid filtering conditions
        $query->andFilterWhere([
            User::tableName().'.id' => $this->id,
            'user_type' => 0,
            User::tableName().'.status' => $this->status,
            User::tableName().'.trashed' => 0,
        ]);

        $query->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
            ->andFilterWhere(['like','email',$this->email])
            ->andFilterWhere(['like',JobTitle::tableName().'.title',$this->job_title])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',UserProfileInfo::tableName().'.mobile',$this->mobile]);

        return $query;
    }

    public function searchMyValuationContacts($params)
    {
        $query = $this->generateQuery3($params);

        //Check Listing type allowed
        if(Yii::$app->menuHelperFunction->getListingTypeByController('contact')==2){
            $query->innerJoin(ContactManager::tableName(),ContactManager::tableName().".contact_id=".User::tableName().".id");
            $query->andFilterWhere([
                ContactManager::tableName().'.staff_id' => Yii::$app->user->identity->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
            ],
            'sort' => $this->defaultSorting,
        ]);

        return $dataProvider;
    }
    public function generateQuery3($params)
    {
        $this->load($params);
        $query = User::find()
            ->select([
                User::tableName().'.id',
                'image',
                'name'=>'CONCAT(firstname," ",lastname)',
                'email',
                UserProfileInfo::tableName().'.mobile',
                'job_title'=>JobTitle::tableName().'.title',
                'cname'=>Company::tableName().'.title',
                User::tableName().'.status',
            ])
            ->leftJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().'.user_id='.User::tableName().'.id')
            ->leftJoin(JobTitle::tableName(),JobTitle::tableName().'.id='.UserProfileInfo::tableName().'.job_title_id')
            ->leftJoin(Company::tableName(),Company::tableName().'.id='.User::tableName().'.company_id')
            // ->andWhere(Company::tableName().'.status!=2')
            ->andWhere('valuation_contact=1')
            ->asArray();

        // grid filtering conditions
        $query->andFilterWhere([
            User::tableName().'.id' => $this->id,
            'user_type' => 0,
            User::tableName().'.status' => $this->status,
            User::tableName().'.trashed' => 0,
        ]);

        $query->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
            ->andFilterWhere(['like','email',$this->email])
            ->andFilterWhere(['like',JobTitle::tableName().'.title',$this->job_title])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',UserProfileInfo::tableName().'.mobile',$this->mobile]);

        return $query;
    }

    public function searchMyProspectiveContacts($params)
    {
        $query = $this->generateQuery4($params);

        //Check Listing type allowed
        if(Yii::$app->menuHelperFunction->getListingTypeByController('contact')==2){
            $query->innerJoin(ContactManager::tableName(),ContactManager::tableName().".contact_id=".User::tableName().".id");
            $query->andFilterWhere([
                ContactManager::tableName().'.staff_id' => Yii::$app->user->identity->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
            ],
            'sort' => $this->defaultSorting,
        ]);

        return $dataProvider;
    }
    public function generateQuery4($params)
    {
        $this->load($params);
        $query = User::find()
            ->select([
                User::tableName().'.id',
                'image',
                'name'=>'CONCAT(firstname," ",lastname)',
                'email',
                UserProfileInfo::tableName().'.mobile',
                'job_title'=>JobTitle::tableName().'.title',
                'cname'=>Company::tableName().'.title',
                User::tableName().'.status',
            ])
            ->leftJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().'.user_id='.User::tableName().'.id')
            ->leftJoin(JobTitle::tableName(),JobTitle::tableName().'.id='.UserProfileInfo::tableName().'.job_title_id')
            ->leftJoin(Company::tableName(),Company::tableName().'.id='.User::tableName().'.company_id')
            // ->andWhere(Company::tableName().'.status!=2')
            ->andWhere('prospect_contact=1')
            ->asArray();

        // grid filtering conditions
        $query->andFilterWhere([
            User::tableName().'.id' => $this->id,
            'user_type' => 0,
            User::tableName().'.status' => $this->status,
            User::tableName().'.trashed' => 0,
        ]);

        $query->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
            ->andFilterWhere(['like','email',$this->email])
            ->andFilterWhere(['like',JobTitle::tableName().'.title',$this->job_title])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',UserProfileInfo::tableName().'.mobile',$this->mobile]);

        return $query;
    }

    public function searchMyPropertyOwnersContacts($params)
    {
        $query = $this->generateQuery5($params);

        //Check Listing type allowed
        if(Yii::$app->menuHelperFunction->getListingTypeByController('contact')==2){
            $query->innerJoin(ContactManager::tableName(),ContactManager::tableName().".contact_id=".User::tableName().".id");
            $query->andFilterWhere([
                ContactManager::tableName().'.staff_id' => Yii::$app->user->identity->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
            ],
            'sort' => $this->defaultSorting,
        ]);

        return $dataProvider;
    }
    public function generateQuery5($params)
    {
        $this->load($params);
        $query = User::find()
            ->select([
                User::tableName().'.id',
                'image',
                'name'=>'CONCAT(firstname," ",lastname)',
                'email',
                UserProfileInfo::tableName().'.mobile',
                'job_title'=>JobTitle::tableName().'.title',
                'cname'=>Company::tableName().'.title',
                User::tableName().'.status',
            ])
            ->leftJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().'.user_id='.User::tableName().'.id')
            ->leftJoin(JobTitle::tableName(),JobTitle::tableName().'.id='.UserProfileInfo::tableName().'.job_title_id')
            ->leftJoin(Company::tableName(),Company::tableName().'.id='.User::tableName().'.company_id')
            // ->andWhere(Company::tableName().'.status!=2')
            ->andWhere('property_owner_contact=1')
            ->asArray();

        // grid filtering conditions
        $query->andFilterWhere([
            User::tableName().'.id' => $this->id,
            'user_type' => 0,
            User::tableName().'.status' => $this->status,
            User::tableName().'.trashed' => 0,
        ]);

        $query->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
            ->andFilterWhere(['like','email',$this->email])
            ->andFilterWhere(['like',JobTitle::tableName().'.title',$this->job_title])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',UserProfileInfo::tableName().'.mobile',$this->mobile]);

        return $query;
    }

    public function searchMyValuationInquiryContacts($params)
    {
        $query = $this->generateQuery6($params);

        //Check Listing type allowed
        if(Yii::$app->menuHelperFunction->getListingTypeByController('contact')==2){
            $query->innerJoin(ContactManager::tableName(),ContactManager::tableName().".contact_id=".User::tableName().".id");
            $query->andFilterWhere([
                ContactManager::tableName().'.staff_id' => Yii::$app->user->identity->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
            ],
            'sort' => $this->defaultSorting,
        ]);

        return $dataProvider;
    }
    public function generateQuery6($params)
    {
        $this->load($params);
        $query = User::find()
            ->select([
                User::tableName().'.id',
                'image',
                'name'=>'CONCAT(firstname," ",lastname)',
                'email',
                UserProfileInfo::tableName().'.mobile',
                'job_title'=>JobTitle::tableName().'.title',
                'cname'=>Company::tableName().'.title',
                User::tableName().'.status',
            ])
            ->leftJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().'.user_id='.User::tableName().'.id')
            ->leftJoin(JobTitle::tableName(),JobTitle::tableName().'.id='.UserProfileInfo::tableName().'.job_title_id')
            ->leftJoin(Company::tableName(),Company::tableName().'.id='.User::tableName().'.company_id')
            // ->andWhere(Company::tableName().'.status!=2')
            ->andWhere('inquiry_valuations_contact	=1')
            ->asArray();

        // grid filtering conditions
        $query->andFilterWhere([
            User::tableName().'.id' => $this->id,
            'user_type' => 0,
            User::tableName().'.status' => $this->status,
            User::tableName().'.trashed' => 0,
        ]);

        $query->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
            ->andFilterWhere(['like','email',$this->email])
            ->andFilterWhere(['like',JobTitle::tableName().'.title',$this->job_title])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',UserProfileInfo::tableName().'.mobile',$this->mobile]);

        return $query;
    }

    public function searchMyIciaContacts($params)
    {
        $query = $this->generateQuery7($params);

        //Check Listing type allowed
        if(Yii::$app->menuHelperFunction->getListingTypeByController('contact')==2){
            $query->innerJoin(ContactManager::tableName(),ContactManager::tableName().".contact_id=".User::tableName().".id");
            $query->andFilterWhere([
                ContactManager::tableName().'.staff_id' => Yii::$app->user->identity->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
            ],
            'sort' => $this->defaultSorting,
        ]);

        return $dataProvider;
    }
    public function generateQuery7($params)
    {
        $this->load($params);
        $query = User::find()
            ->select([
                User::tableName().'.id',
                'image',
                'name'=>'CONCAT(firstname," ",lastname)',
                'email',
                UserProfileInfo::tableName().'.mobile',
                'job_title'=>JobTitle::tableName().'.title',
                'cname'=>Company::tableName().'.title',
                User::tableName().'.status',
            ])
            ->leftJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().'.user_id='.User::tableName().'.id')
            ->leftJoin(JobTitle::tableName(),JobTitle::tableName().'.id='.UserProfileInfo::tableName().'.job_title_id')
            ->leftJoin(Company::tableName(),Company::tableName().'.id='.User::tableName().'.company_id')
            // ->andWhere(Company::tableName().'.status!=2')
            ->andWhere('icai_contact=1')
            ->asArray();

        // grid filtering conditions
        $query->andFilterWhere([
            User::tableName().'.id' => $this->id,
            'user_type' => 0,
            User::tableName().'.status' => $this->status,
            User::tableName().'.trashed' => 0,
        ]);

        $query->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
            ->andFilterWhere(['like','email',$this->email])
            ->andFilterWhere(['like',JobTitle::tableName().'.title',$this->job_title])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',UserProfileInfo::tableName().'.mobile',$this->mobile]);

        return $query;
    }

    public function searchMyBrokerContacts($params)
    {
        $query = $this->generateQuery8($params);

        //Check Listing type allowed
        if(Yii::$app->menuHelperFunction->getListingTypeByController('contact')==2){
            $query->innerJoin(ContactManager::tableName(),ContactManager::tableName().".contact_id=".User::tableName().".id");
            $query->andFilterWhere([
                ContactManager::tableName().'.staff_id' => Yii::$app->user->identity->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
            ],
            'sort' => $this->defaultSorting,
        ]);

        return $dataProvider;
    }
    public function generateQuery8($params)
    {
        $this->load($params);
        $query = User::find()
            ->select([
                User::tableName().'.id',
                'image',
                'name'=>'CONCAT(firstname," ",lastname)',
                'email',
                UserProfileInfo::tableName().'.mobile',
                'job_title'=>JobTitle::tableName().'.title',
                'cname'=>Company::tableName().'.title',
                User::tableName().'.status',
            ])
            ->leftJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().'.user_id='.User::tableName().'.id')
            ->leftJoin(JobTitle::tableName(),JobTitle::tableName().'.id='.UserProfileInfo::tableName().'.job_title_id')
            ->leftJoin(Company::tableName(),Company::tableName().'.id='.User::tableName().'.company_id')
            // ->andWhere(Company::tableName().'.status!=2')
            ->andWhere('broker_contact=1')
            ->asArray();

        // grid filtering conditions
        $query->andFilterWhere([
            User::tableName().'.id' => $this->id,
            'user_type' => 0,
            User::tableName().'.status' => $this->status,
            User::tableName().'.trashed' => 0,
        ]);

        $query->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
            ->andFilterWhere(['like','email',$this->email])
            ->andFilterWhere(['like',JobTitle::tableName().'.title',$this->job_title])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',UserProfileInfo::tableName().'.mobile',$this->mobile]);

        return $query;
    }

    public function searchMyDevelopersContacts($params)
    {
        $query = $this->generateQuery9($params);

        //Check Listing type allowed
        if(Yii::$app->menuHelperFunction->getListingTypeByController('contact')==2){
            $query->innerJoin(ContactManager::tableName(),ContactManager::tableName().".contact_id=".User::tableName().".id");
            $query->andFilterWhere([
                ContactManager::tableName().'.staff_id' => Yii::$app->user->identity->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
            ],
            'sort' => $this->defaultSorting,
        ]);

        return $dataProvider;
    }
    public function generateQuery9($params)
    {
        $this->load($params);
        $query = User::find()
            ->select([
                User::tableName().'.id',
                'image',
                'name'=>'CONCAT(firstname," ",lastname)',
                'email',
                UserProfileInfo::tableName().'.mobile',
                'job_title'=>JobTitle::tableName().'.title',
                'cname'=>Company::tableName().'.title',
                User::tableName().'.status',
            ])
            ->leftJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().'.user_id='.User::tableName().'.id')
            ->leftJoin(JobTitle::tableName(),JobTitle::tableName().'.id='.UserProfileInfo::tableName().'.job_title_id')
            ->leftJoin(Company::tableName(),Company::tableName().'.id='.User::tableName().'.company_id')
            // ->andWhere(Company::tableName().'.status!=2')
            ->andWhere('developer_contact=1')
            ->asArray();

        // grid filtering conditions
        $query->andFilterWhere([
            User::tableName().'.id' => $this->id,
            'user_type' => 0,
            User::tableName().'.status' => $this->status,
            User::tableName().'.trashed' => 0,
        ]);

        $query->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
            ->andFilterWhere(['like','email',$this->email])
            ->andFilterWhere(['like',JobTitle::tableName().'.title',$this->job_title])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',Company::tableName().'.title',$this->cname])
            ->andFilterWhere(['like',UserProfileInfo::tableName().'.mobile',$this->mobile]);

        return $query;
    }

  /**
  * Generates query for the search
  *
  * @param array $params
  *
  * @return ActiveRecord
  */



  /**
  * Default Sorting Options
  *
  * @param array $params
  *
  * @return Array
  */
  public function getDefaultSorting()
  {
    return [
      'defaultOrder' => ['name'=>SORT_ASC],
      'attributes' => [
        'name' => [
          'asc' => [User::tableName().'.firstname' => SORT_ASC, User::tableName().'.lastname' => SORT_ASC],
          'desc' => [User::tableName().'.firstname' => SORT_DESC, User::tableName().'.lastname' => SORT_DESC],
        ],
        'email',
        'mobile',
        'job_title' => [
          'asc' => [JobTitle::tableName().'.title' => SORT_ASC],
          'desc' => [JobTitle::tableName().'.title' => SORT_DESC],
        ],
        'cname' => [
          'asc' => [Company::tableName().'.title' => SORT_ASC],
          'desc' => [Company::tableName().'.title' => SORT_DESC],
        ],
        'status',
        User::tableName().'.created_at',
      ]
    ];
  }
}
