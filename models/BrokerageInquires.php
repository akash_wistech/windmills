<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "brokerage_inquires".
 *
 * @property int $id
 * @property string $reference_number
 * @property int $client_id
 * @property string|null $client_segment
 * @property string|null $referred_by
 * @property string|null $inquiry_date
 * @property int|null $inquiry_type
 * @property int|null $inquiry_importance
 * @property int|null $purpose_of_brokerage_service
 * @property int|null $building_info
 * @property int|null $property_id
 * @property int|null $property_category
 * @property int|null $community
 * @property int|null $sub_community
 * @property int $tenure
 * @property int|null $number_bed_rooms
 * @property int|null $city
 * @property int|null $vacant
 * @property string|null $vacancy_date
 * @property int|null $eviction_notice_served
 * @property float|null $current_rent_amount
 * @property float|null $service_charges
 * @property int|null $parking_spaces
 * @property int|null $listing_permitted
 * @property float|null $land_size
 * @property float|null $target_investment
 * @property float|null $preferred_return_of_investment
 * @property float $cash_mortgage_buyer
 * @property string|null $expected_time_of_buying
 * @property string|null $expected_exit_period
 * @property int|null $leasing_service_required
 * @property int|null $facility_management_service_required
 * @property int|null $property_management_service_required
 * @property int|null $property_status
 * @property int|null $created_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class BrokerageInquires extends ActiveRecordFull
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'brokerage_inquires';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reference_number', 'client_id', 'tenure', 'cash_mortgage_buyer'], 'required'],
            [['client_id', 'inquiry_type', 'inquiry_importance', 'purpose_of_brokerage_service', 'building_info', 'property_id', 'property_category','tenure', 'number_bed_rooms', 'vacant', 'eviction_notice_served', 'parking_spaces', 'listing_permitted', 'leasing_service_required', 'facility_management_service_required', 'property_management_service_required', 'property_status', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['inquiry_date', 'vacancy_date', 'expected_time_of_buying', 'expected_exit_period', 'created_at', 'updated_at', 'trashed_at','built_up_area','status_verified',
                'status_verified_at','status_verified_by','target_client','desired_rice_rent_amount','deal_possibility'], 'safe'],
            [['current_rent_amount', 'service_charges', 'land_size', 'target_investment', 'preferred_return_of_investment', 'cash_mortgage_buyer','built_up_area','desired_rice_rent_amount'], 'number'],
            [['reference_number'], 'string', 'max' => 100],
            [['client_segment', 'referred_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reference_number' => 'Reference Number',
            'client_id' => 'Client ID',
            'client_segment' => 'Client Segment',
            'referred_by' => 'Reffered By',
            'inquiry_date' => 'Inquiry Date',
            'inquiry_type' => 'Inquiry Type',
            'inquiry_importance' => 'Inquiry Importance',
            'purpose_of_brokerage_service' => 'Purpose Of Brokerage Service',
            'building_info' => 'Building Info',
            'property_id' => 'Property ID',
            'property_category' => 'Property Category',
            'community' => 'Community',
            'sub_community' => 'Sub Community',
            'tenure' => 'Tenure',
            'number_bed_rooms' => 'Number Bed Rooms',
            'city' => 'City',
            'vacant' => 'Vacant',
            'vacancy_date' => 'Vacancy Date',
            'eviction_notice_served' => 'Eviction Notice Served',
            'current_rent_amount' => 'Current Rent Amount',
            'service_charges' => 'Service Charges',
            'parking_spaces' => 'Parking Spaces',
            'listing_permitted' => 'Listing Permitted',
            'land_size' => 'Land Size',
            'target_investment' => 'Target Investment',
            'preferred_return_of_investment' => 'Preferred Return Of Investment',
            'cash_mortgage_buyer' => 'Cash Mortgage Buyer',
            'expected_time_of_buying' => 'Expected Time Of Buying',
            'expected_exit_period' => 'Expected Exit Period',
            'leasing_service_required' => 'Leasing Service Required',
            'facility_management_service_required' => 'Facility Management Service Required',
            'property_management_service_required' => 'Property Management Service Required',
            'property_status' => 'Property Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }



    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Brokerage Inquires';
    }
    public function getRecTitle()
    {
        return $this->reference_number;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Company::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuilding()
    {
        return $this->hasOne(Buildings::className(), ['id' => 'building_info']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Properties::className(), ['id' => 'property_id']);
    }

}
