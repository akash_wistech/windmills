<?php

namespace app\models;

use Yii;

/**
* AttachmentForm is the model behind the action log attachment form.
*/
class AttachmentForm extends ALForms
{
  public $attachment_file;
  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['module_type','module_id'],'required'],
      [['module_type'],'string'],
      [['attachment_file'],'each','rule'=>['string']],
      [['module_id'],'integer'],
    ];
  }

  /**
  * Save Action Log Comment Info
  */
  public function save()
  {
    if ($this->validate()) {
      $attachment = new ActionLog;
      $attachment->module_type=$this->module_type;
      $attachment->module_id=$this->module_id;
      $attachment->rec_type=$this->rec_type;
      $attachment->attachment_file=$this->attachment_file;

      $logMsg=''.Yii::$app->user->identity->name.' saved a '.$this->module_type.' type '.$this->rec_type.' for '.$this->module_id;
      if($attachment->save()){
        if($attachment->hasErrors()){
          foreach($attachment->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                $this->addError('',$val);
                return false;
              }
            }
          }
        }
      }
      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, $logMsg);
      return true;
    }
    return false;
  }
}
