<?php

namespace app\models;

use Yii;

/**
* ALCalendarEventForm is the model behind the action log calendar event form.
*/
class ALCalendarEventForm extends ALForms
{
  public $id,$event_start,$event_end,$priority,$color,$assign_to,$sub_type,$status;
  public $calendar_id,$allday;
  public $reminder,$reminder_to,$remind_time;
  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['module_type','module_id','comments'],'required'],
      [['module_type','comments','event_start','event_end','color','reminder_to'],'string'],
      [['id','module_id','calendar_id','priority','sub_type','status','allday','reminder','remind_time'],'integer'],
      [['comments'],'trim'],
      [['assign_to'],'each','rule'=>['integer']],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'comments' => 'Comments',
      'event_start' => 'Start Date',
      'event_end' => 'End Date',
      'sub_type' => 'Event Sub Type',
      'status' => 'Event Status',
      'visibility' => 'Visibility',
      'calendar_id' => 'Calendar',
      'allday' => 'All Day',
    ];
  }

  /**
  * Save Action Log Action Info
  */
  public function save()
  {
    if ($this->validate()) {
      $checkAlready=$this->checkAlready;
      if(!$checkAlready->exists()){
        if($this->id>0){
          $comment = ActionLog::findOne($this->id);
          $logMsg=''.Yii::$app->user->identity->name.' update a '.$this->module_type.' type '.$this->rec_type.' for '.$this->module_id.' with id '.$this->id;
        }else{
          $comment = new ActionLog;
          $comment->module_type=$this->module_type;
          $comment->module_id=$this->module_id;
          $comment->rec_type=$this->rec_type;
          $logMsg=''.Yii::$app->user->identity->name.' saved a '.$this->module_type.' type '.$this->rec_type.' for '.$this->module_id;
        }
        $comment->comments=$this->comments;
        $comment->assign_to=$this->assign_to;
        $comment->priority=$this->priority;
        $comment->visibility=$this->visibility;
        $comment->calendar_id=$this->calendar_id;
        if($comment->save()){
          $eventInfo=ActionLogEventInfo::find()->where(['action_log_id'=>$comment->id])->one();
          if($eventInfo==null){
            $eventInfo=new ActionLogEventInfo;
            $eventInfo->action_log_id=$comment->id;
          }
          $eventInfo->start_date=$this->event_start;
          $eventInfo->end_date=$this->event_end;
          $eventInfo->color_code=$this->color;
          $eventInfo->all_day=$this->allday;
          $eventInfo->sub_type=$this->sub_type;
          $eventInfo->status=$this->status;
          $eventInfo->save();
          if($this->reminder==1){
            $reminderInfo=ActionLogReminder::find()->where(['action_log_id'=>$comment->id])->one();
            if($reminderInfo==null){
              $reminderInfo=new ActionLogReminder;
              $reminderInfo->action_log_id=$comment->id;
            }
            $reminderInfo->notify_to=$this->reminder_to;
            $reminderInfo->notify_time=$this->remind_time;
            $reminderInfo->save();
          }else{
            $reminderInfo=ActionLogReminder::find()->where(['action_log_id'=>$comment->id])->one();
            if($reminderInfo!=null){
              $reminderInfo->delete();
            }
          }
        }else{
          if($comment->hasErrors()){
            foreach($comment->getErrors() as $error){
              if(count($error)>0){
                foreach($error as $key=>$val){
                  $this->addError('',$val);
                  return false;
                }
              }
            }
          }
        }
        ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, ''.Yii::$app->user->identity->name.' saved a '.$this->module_type.' type '.$this->rec_type.' for '.$this->module_id);
      }else{
        return true;
      }
      return true;
    }
    return false;
  }
}
