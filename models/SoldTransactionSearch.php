<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SoldTransaction;

/**
 * ListingsTransactionsSearch represents the model behind the search form of `app\models\ListingsTransactions`.
 */
class SoldTransactionSearch extends SoldTransaction
{
    public $date_from;
    public $date_to;
    public $building_info_data;
    public $property_id;
    public $property_category;
    public $bedroom_from;
    public $bedroom_to;
    public $landsize_from;
    public $landsize_to;
    public $bua_from;
    public $bua_to;
    public $price_from;
    public $price_to;
    public $price_psf_from;
    public $price_psf_to;
    public $selectedOnly,$unselectedOnly,$valuation_id;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'property_category', 'tenure', 'floor_number', 'number_of_levels', 'location', 'property_placement', 'property_visibility', 'property_exposure', 'listing_property_type', 'property_condition', 'no_of_bedrooms', 'upgrades', 'full_building_floors', 'parking_space', 'agent_phone_no', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by', 'building_info'], 'integer'],
            [[ 'building_info_data','property_id','property_category','property_id','bua_from','bua_to','price_from','price_to','price_psf_from','price_psf_to','bedroom_from','bedroom_to','landsize_from','landsize_to','date_from','date_to','transaction_date', 'unit_number', 'land_size', 'balcony_size', 'development_type', 'finished_status', 'pool', 'gym', 'play_area', 'completion_status', 'landscaping', 'white_goods', 'furnished',
                'utilities_connected', 'developer_margin', 'agent_name', 'agent_company', 'created_at', 'updated_at', 'trashed_at', 'other_facilities','type','community','sub_community','list_type','municipality','district','projectsub_comm','property_subtype','valuation_amount','duplicate_status','reidin_ref_number'], 'safe'],
            [['built_up_area', 'listings_price', 'listings_rent', 'price_per_sqt', 'final_price','municipality','district','projectsub_comm','property_subtype','sold_db_id','valuation_amount'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SoldTransaction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC, // Sort by 'id' in descending order
                ],
            ],
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'transaction_date' => $this->transaction_date,
            /*'property_listed' => $this->property_listed,*/
            'property_category' => $this->property_category,
            'building_info' => $this->building_info,
            'community' => $this->community,
            'sub_community' => $this->sub_community,
            'tenure' => $this->tenure,
            'floor_number' => $this->floor_number,
            'number_of_levels' => $this->number_of_levels,
            'location' => $this->location,
            'built_up_area' => $this->built_up_area,
            'property_placement' => $this->property_placement,
            'property_visibility' => $this->property_visibility,
            'property_exposure' => $this->property_exposure,
            'listing_property_type' => $this->listing_property_type,
            'property_condition' => $this->property_condition,
            'other_facilities' => $this->other_facilities,
            'no_of_bedrooms' => $this->no_of_bedrooms,
            'upgrades' => $this->upgrades,
            'full_building_floors' => $this->full_building_floors,
            'parking_space' => $this->parking_space,
            'listings_price' => $this->listings_price,
            'listings_rent' => $this->listings_rent,
            'price_per_sqt' => $this->price_per_sqt,
            'final_price' => $this->final_price,
            'agent_phone_no' => $this->agent_phone_no,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => $this->trashed,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
            'list_type' => $this->list_type,
            'duplicate_status' => $this->duplicate_status,
            'reidin_ref_number' => $this->reidin_ref_number,
        ]);

        $query->andFilterWhere(['like', 'unit_number', $this->unit_number])
            ->andFilterWhere(['like', 'land_size', $this->land_size])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'balcony_size', $this->balcony_size])
            ->andFilterWhere(['like', 'development_type', $this->development_type])
            ->andFilterWhere(['like', 'finished_status', $this->finished_status])
            ->andFilterWhere(['like', 'pool', $this->pool])
            ->andFilterWhere(['like', 'gym', $this->gym])
            ->andFilterWhere(['like', 'play_area', $this->play_area])
            ->andFilterWhere(['like', 'completion_status', $this->completion_status])
            ->andFilterWhere(['like', 'landscaping', $this->landscaping])
            ->andFilterWhere(['like', 'white_goods', $this->white_goods])
            ->andFilterWhere(['like', 'furnished', $this->furnished])
            ->andFilterWhere(['like', 'utilities_connected', $this->utilities_connected])
            ->andFilterWhere(['like', 'developer_margin', $this->developer_margin])
            ->andFilterWhere(['like', 'agent_name', $this->agent_name])
            ->andFilterWhere(['like', 'agent_company', $this->agent_company]);


        if(isset($this->sold_db_id ) && $this->sold_db_id ==1){
            $query->andFilterWhere(['is not', 'sold_db_id', new \yii\db\Expression('null')]);
        }else{
            $query->andFilterWhere(['is ', 'sold_db_id', new \yii\db\Expression('null')]);
        }

        return $dataProvider;
    }

    public function searchvaluation($params)
    {
        $user=User::findOne(\Yii::$app->user->id);
        if ($user['permission_group_id']==3) {
            $query = SoldTransactionSearch::find()->limit(\Yii::$app->helperFunctions->getTransactionData())->orderBy([
                'transaction_date' => SORT_DESC,
            ]);
        }else {
            $query = SoldTransactionSearch::find()->orderBy([
                'transaction_date' => SORT_DESC,
            ]);
        }
       // $query = SoldTransactionSearch::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'property_category' => $this->property_category,
            'property_id' => $this->property_id,
        ]);
        $query->andFilterWhere([
            'between', 'transaction_date', $this->date_from, $this->date_to
        ]);
        $buildings =array();
        if($this->building_info_data <> null){
            $buildings  = explode(",",$this->building_info_data);
        }
        $query->andFilterWhere(['IN', 'building_info', $buildings]);
        $query->andFilterWhere(['not in','duplicate_status',[8]]);
        $query->andFilterWhere([
            'between', 'no_of_bedrooms', $this->bedroom_from, $this->bedroom_to
        ]);
        $query->andFilterWhere([
            'between', 'land_size', $this->landsize_from, $this->landsize_to
        ]);
        $query->andFilterWhere([
            'between', 'built_up_area', $this->bua_from, $this->bua_to
        ]);
        $query->andFilterWhere([
            'between', 'listings_price', $this->price_from, $this->price_to
        ]);
        $query->andFilterWhere([
            'between', 'price_per_sqt', $this->price_psf_from, $this->price_psf_to
        ]);

        /* $query->andFilterWhere(['like', 'listings_reference', $this->listings_reference])
             ->andFilterWhere(['like', 'listing_website_link', $this->listing_website_link])
             ->andFilterWhere(['like', 'unit_number', $this->unit_number])
             ->andFilterWhere(['like', 'land_size', $this->land_size])
             ->andFilterWhere(['like', 'balcony_size', $this->balcony_size])
             ->andFilterWhere(['like', 'development_type', $this->development_type])
             ->andFilterWhere(['like', 'finished_status', $this->finished_status])
             ->andFilterWhere(['like', 'pool', $this->pool])
             ->andFilterWhere(['like', 'gym', $this->gym])
             ->andFilterWhere(['like', 'play_area', $this->play_area])
             ->andFilterWhere(['like', 'completion_status', $this->completion_status])
             ->andFilterWhere(['like', 'landscaping', $this->landscaping])
             ->andFilterWhere(['like', 'white_goods', $this->white_goods])
             ->andFilterWhere(['like', 'furnished', $this->furnished])
             ->andFilterWhere(['like', 'utilities_connected', $this->utilities_connected])
             ->andFilterWhere(['like', 'developer_margin', $this->developer_margin])
             ->andFilterWhere(['like', 'agent_name', $this->agent_name])
             ->andFilterWhere(['like', 'agent_company', $this->agent_company]);*/

        $selectedSold=ValuationSelectedLists::find()->where(['valuation_id'=>$this->valuation_id, 'type' => 'sold','search_type'=>0,'latest'=> 1])->select(['selected_id'])->asArray();
        if ($this->selectedOnly==true) {
            $query->andFilterWhere(['id'=>$selectedSold]);
        }
        if ($this->unselectedOnly==true) {
            $query->andFilterWhere(['not in','id',$selectedSold]);
        }
        return $dataProvider;
    }

    public function searchvaluationreport($params)
    {
        $user=User::findOne(\Yii::$app->user->id);
        if ($user['permission_group_id']==3) {
            $query = SoldTransactionSearch::find()->limit(\Yii::$app->helperFunctions->getTransactionData())->orderBy([
                'transaction_date' => SORT_DESC,
            ]);
        }else {
            $query = SoldTransactionSearch::find()->orderBy([
                'transaction_date' => SORT_DESC,
            ]);
        }
        // $query = SoldTransactionSearch::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $selectedSold=ValuationSelectedLists::find()->where(['valuation_id'=>$this->valuation_id, 'type' => 'sold','search_type'=>0,'latest'=> 1])->select(['selected_id'])->asArray();
        if ($this->selectedOnly==true) {
            $query->andFilterWhere(['id'=>$selectedSold]);
        }

        return $dataProvider;
    }

}
