<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "buaweightage".
 *
 * @property int $id
 * @property float $difference
 * @property float $bigger_sp
 * @property float $smaller_sp
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class Buaweightage extends ActiveRecordFull
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'buaweightage';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['difference', 'bigger_sp', 'smaller_sp'], 'required'],
            [['difference', 'bigger_sp', 'smaller_sp'], 'number'],
            [['created_at', 'updated_at', 'trashed_at'], 'safe'],
            [['created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['status_verified','status_verified_at','status_verified_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'difference' => 'Difference',
            'bigger_sp' => 'Bigger Sp',
            'smaller_sp' => 'Smaller Sp',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }
    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->title;
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'BUA Weightage';
    }
}
