<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CrmQuotations;

use yii;

/**
* CrmQuotationsSearch represents the model behind the search form of `app\models\CrmQuotations`.
*/
class CrmQuotationsSearch extends CrmQuotations
{
    public $client_type;
    public $dashboard_filter;
    public $date_range, $time_period,$time_period_compare, $custom_date_btw;
    public $active_quotation;
    public $date_period;
    public $city;
    public $widget_type, $status_type, $widget_view, $page_title, $list_columns;
    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['id', 'no_of_properties', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],

            [['reference_number', 'client_name', 'client_customer_name', 'scope_of_service','advance_payment_terms',
            'related_to_buyer', 'related_to_owner', 'related_to_client', 'related_to_property',
            'related_to_buyer_reason', 'related_to_owner_reason', 'related_to_client_reason',
            'related_to_property_reason', 'name', 'phone_number', 'email', 'relative_discount', 'turn_around_time',
            'valuer_name', 'date', 'payment_slip', 'toe_document', 'quotation_recommended_fee',
            'quotation_turn_around_time', 'toe_final_fee', 'toe_final_turned_around_time', 'assumptions',
            'created_at', 'updated_at', 'deleted_at','quotation_status','client_type','time_period','time_period_compare','custom_date_btw'], 'safe'],

            [['recommended_fee', 'final_fee_approved'], 'number'],

            [['dashboard_filter','date_range', 'date_period', 'toe_signed_and_received', 'active_quotation', 'client_reference','quotation_final_fee','total_time_days','total_time_hours','verify_time_days','total_time_hours'], 'safe'],
            [['status_change_date','inquiry_received_date','quotation_sent_date','toe_sent_date','payment_received_date','toe_signed_and_received','toe_signed_and_received_date','on_hold_date', 'grand_final_toe_fee','city','widget_type', 'status_type'], 'safe'],
            [['widget_view', 'page_title'], 'number'],
        ];
    }

    

    /**
    * {@inheritdoc}
    */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
    * Creates data provider instance with search query applied
    *
    * @param array $params
    *
    * @return ActiveDataProvider
    */
    public function search($params)
    {
        // dd($params['sort']);
        $query = CrmQuotations::find();
        $query = $query->leftJoin('company', '`company`.`id` = `crm_quotations`.`client_name`');
        $query = $query->where(['crm_quotations.trashed' => null]);

        if($params['sort'] == ''){
            $query = $query->orderBy([ 'id' => SORT_DESC ]);
        }


        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'sort' => ['attributes' => [
                'id','reference_number','client_name','client_type','client_reference','client_customer_name',
                'no_of_properties','quotation_final_fee','grand_final_toe_fee','toe_final_turned_around_time',
                'city','quotation_status','status_change_date','inquiry_received_date','quotation_sent_date',
                'toe_sent_date','payment_received_date','toe_signed_and_received','toe_signed_and_received_date','on_hold_date']
            ]
        ]);

        $this->load($params);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }



        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'no_of_properties' => $this->no_of_properties,
            'recommended_fee' => $this->recommended_fee,
            'final_fee_approved' => $this->final_fee_approved,
            'status' => $this->status,
            // 'quotation_status'=> $this->quotation_status,
            // 'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
            'client_type' => $this->client_type,
        ]);

        $startDate = date('Y-m-d').' 00:00:00';
        $endDate = date('Y-m-d').' 23:59:59';





        if ($this->date_period<>null AND $this->date_period!=9) {
          if ($this->date_period == 0) {
            $from_date = '';
            $to_date = '';
            $startDate = '';
            $endDate = '';
          }else{
            // echo $this->date_period; die;
              $date_array = Yii::$app->crmQuotationHelperFunctions->getFilterDates($this->date_period);
              $from_date = $date_array['start_date']." 00:00:00";
              $to_date = $date_array['end_date']." 23:59:59";
          }
        }else{
            if($this->date_range<>null){
                $to=$from='';
                $explodeDate = \explode('to', $this->date_range);
                $from_date = $explodeDate[0]." 00:00:00";
                $to_date = $explodeDate[1]." 23:59:59";
            }
        }



        if($this->active_quotation=='active_filter' AND $this->dashboard_filter=='yes'){
            $query->andFilterWhere([
                'not in', 'crm_quotations.quotation_status',  [6,11,12,13],
            ]);
            $query->andFilterWhere([
                'not in', 'crm_quotations.toe_signed_and_received' , 1,
            ]);
            $query->andFilterWhere([
                'between', 'crm_quotations.created_at', $startDate, $endDate,
            ]);
        }



        if($this->toe_signed_and_received==1 && $this->dashboard_filter=='yes'){
            $query->andFilterWhere(['toe_signed_and_received'=> 1]);
            if($to<>null AND $from<>null){
                $query->andFilterWhere([
                    'between', 'crm_quotations.created_at', $from_date, $to_date
                ]);
            }else{

                $query->andFilterWhere([
                    'between', 'crm_quotations.created_at', $startDate, $endDate
                ]);
            }
        }





        if($this->quotation_status<>null){

          if ($this->quotation_status==5 AND $this->dashboard_filter=='yes') {
            // echo $this->quotation_status. "<br>";

            $query->andFilterWhere(['toe_signed_and_received'=> 1]);
            if($from_date<>null AND $to_date<>null){
              // echo $from_date. "<br>";
              // echo $to_date. "<br>";
              // die();
                $query->andFilterWhere([
                    'between', 'crm_quotations.created_at', $from_date, $to_date
                ]);
            }else{

              $query->andFilterWhere([
                  'between', 'crm_quotations.created_at', $startDate, $endDate
              ]);
            }
          }

           /* else if($this->quotation_status ==6 AND $this->dashboard_filter=='yes'){
              // echo "<pre>"; print_r($this->quotation_status); echo "<pre>"; die;
              if($from_date<>null AND $to_date<>null){
                // die("hello");
                  $query->andFilterWhere([
                      'and',
                      ['quotation_status'=> 6],
                      'between', 'crm_quotations.created_at', $from_date, $to_date
                  ]);
                  $query->orFilterWhere([
                      'and',
                      ['toe_signed_and_received'=> 1],
                      'between', 'crm_quotations.created_at', $from_date, $to_date
                  ]);
              }
                else{

                  // die("hy");
                    $query->andFilterWhere([
                        'and',
                        ['quotation_status'=> [0,1,3,6,10,7,13,8,11,12]],
                        ['between', 'created_at', $startDate, $endDate]
                    ]);
                    $query->orFilterWhere([
                        'and',
                        ['toe_signed_and_received'=> 1],
                        ['between', 'created_at', $startDate, $endDate]
                    ]);
                }
            }*/
            
           /* else if($this->quotation_status== 7 AND $this->dashboard_filter=='yes'){
                if($from_date<>null AND $to_date<>null){
                    $query->andFilterWhere([
                        'and',
                        ['quotation_status'=> 7],
                        'between', 'crm_quotations.created_at', $from_date, $to_date
                    ]);
                    $query->orFilterWhere([
                        'and',
                        ['quotation_status'=> 13],
                        'between', 'crm_quotations.created_at', $from_date, $to_date
                    ]);
                }
                else{
                    $query->andFilterWhere([
                        'and',
                        ['quotation_status'=> 7],
                        ['between', 'quotation_rejected_date', $startDate, $endDate]
                    ]);
                    $query->orFilterWhere([
                        'and',
                        ['quotation_status'=> 13],
                        ['between', 'status_change_date', $startDate, $endDate]
                    ]);
                }

            }*/

            // else if($this->quotation_status<>null){
              /*  else if($this->quotation_status==8 AND $this->dashboard_filter=='yes'){
                    if($from_date<>null AND $to_date<>null){
                        $query->andFilterWhere([
                            'and',
                            ['quotation_status'=> 8],
                            'between', 'crm_quotations.quotation_rejected_date', $from_date, $to_date
                        ]);
                        $query->orFilterWhere([
                            'and',
                            ['quotation_status'=> 13],
                            'between', 'crm_quotations.status_change_date', $from_date, $to_date
                        ]);
                    }
                    else{
                        $query->andFilterWhere([
                            'and',
                            ['quotation_status'=> 8],
                            ['between', 'toe_rejected_date', $startDate, $endDate]
                        ]);
                        $query->orFilterWhere([
                            'and',
                            ['quotation_status'=> 13],
                            ['between', 'status_change_date', $startDate, $endDate]
                        ]);
                    }

                }*/
                // }

                else if($this->quotation_status==0 && $this->dashboard_filter=='yes'){
                    // echo "12367890"; die;
                    $query->andFilterWhere(['quotation_status'=>[0]]);

                    if($from_date<>null AND $to_date<>null){
                        // echo "24"; die;
                        $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $from_date, $to_date
                        ]);
                    }else{
                        // echo "56"; die;
                       /* $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $startDate, $endDate
                        ]);*/
                    }
                }


                else if($this->quotation_status==1 && $this->dashboard_filter=='yes'){
                   // $query->andFilterWhere(['quotation_status'=> [0,1,3,6,10,7,13,8,11,12]]);
                    $query->andFilterWhere(['quotation_status'=> [1]]);

                      // echo $this->quotation_status; echo "<br>";
                      // echo "form date:- ".$from_date. "<br>to date:- ". $to_date; die;

                    if($from_date<>null AND $to_date<>null){
                      // die("1234");
                        $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $from_date, $to_date
                        ]);
                    }else{
                      // die("5678");
                        $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $startDate, $endDate
                        ]);
                    }
                }
                else if($this->quotation_status==3 && $this->dashboard_filter=='yes'){
                    $query->andFilterWhere(['quotation_status'=> [3]]);
                    if($from_date<>null AND $to_date<>null){
                        $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $from_date, $to_date
                        ]);
                    }else{
                        $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $startDate, $endDate
                        ]);
                    }
                }
                else if($this->quotation_status==4 && $this->dashboard_filter=='yes'){
                    $query->andFilterWhere(['quotation_status'=> $this->quotation_status]);
                    if($from_date<>null AND $to_date<>null){
                        $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $from_date, $to_date
                        ]);
                    }else{
                        $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $startDate, $endDate
                        ]);
                    }
                }


                else if($this->quotation_status==6 && $this->dashboard_filter=='yes'){

                    $query->andFilterWhere(['quotation_status'=>  [6]]);
                    if($from_date<>null AND $to_date<>null){
                        $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $from_date, $to_date
                        ]);
                    }
                    else{
                        $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $startDate, $endDate
                        ]);
                    }
                }

                else if($this->quotation_status==10 && $this->dashboard_filter=='yes'){
                    $query->andFilterWhere(['quotation_status'=> [10]]);
                    if($from_date<>null AND $to_date<>null){
                        $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $from_date, $to_date
                        ]);
                    }
                    else{
                        $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $startDate, $endDate
                        ]);
                    }
                }
                else if($this->quotation_status==2 && $this->dashboard_filter=='yes'){
                       
                    $query->andFilterWhere(['quotation_status'=>2]);
                    if($from_date<>null AND $to_date<>null){
                        $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $from_date, $to_date
                        ]);
                    }
                    else{
                        $query->andFilterWhere(['like', 'crm_quotations.created_at', date('Y-m-d')]);
                    }
                }
                // echo "string"; die;
                else if($this->quotation_status==9 && $this->dashboard_filter=='yes'){
                    $query->andFilterWhere(['quotation_status'=> $this->quotation_status]);
                    if($from_date<>null AND $to_date<>null){
                        $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $from_date, $to_date
                        ]);
                    }
                    else{
                        $query->andFilterWhere(['between', 'crm_quotations.created_at', $startDate, $endDate]);
                    }
                }

                else if($this->quotation_status==8 && $this->dashboard_filter=='yes'){

                    $query->andFilterWhere(['quotation_status'=> 8]);
                    if($from_date<>null AND $to_date<>null){
                        $query->andFilterWhere([

                            'between', 'crm_quotations.created_at', $from_date, $to_date
                        ]);

                    }else{
                        $query->andFilterWhere(['between', 'crm_quotations.created_at', $startDate, $endDate]);
                    }
                }
                else if($this->quotation_status==11 && $this->dashboard_filter=='yes'){
                    // die('hello how are you');
                    $query->andFilterWhere(['quotation_status'=> $this->quotation_status]);
                    if($from_date<>null AND $to_date<>null){
                        $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $from_date, $to_date
                        ]);
                    }else{
                        $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $startDate, $endDate
                        ]);
                    }
                }

                else if($this->quotation_status==12 && $this->dashboard_filter=='yes'){

                    $query->andFilterWhere(['quotation_status'=> $this->quotation_status]);
                    if($from_date<>null AND $to_date<>null){
                        $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $from_date, $to_date
                        ]);
                    }else{
                        $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $startDate, $endDate
                        ]);
                    }
                }

                else if($this->quotation_status==7 && $this->dashboard_filter=='yes'){

                    $query->andFilterWhere(['quotation_status'=> 7]);
                    if($from_date<>null AND $to_date<>null){
                        $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $from_date, $to_date
                        ]);
                    }else{
                        $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $startDate, $endDate
                        ]);
                    }
                }
                else if($this->quotation_status==15 && $this->dashboard_filter=='yes'){
                    $query->andFilterWhere(['quotation_status'=> [0,1,2,3,6,7,8,10,11,12,13]]);
                    if($from_date<>null AND $to_date<>null){
                        $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $from_date, $to_date
                        ]);
                    }
                    else{
                        $query->andFilterWhere([
                            'between', 'crm_quotations.created_at', $startDate, $endDate
                        ]);
                    }
                }
                else{
                   /* echo $this->quotation_status;
                     die("in else");*/
                    $query->andFilterWhere([
                        'quotation_status'=> $this->quotation_status
                    ]);

                    $query->andFilterWhere(['like', 'quotation_status', $this->quotation_status]);

                }

            }



            $query->andFilterWhere(['like', 'reference_number', $this->reference_number])
            ->andFilterWhere(['=', 'client_name', $this->client_name])
            ->andFilterWhere(['like', 'client_customer_name', $this->client_customer_name])
            ->andFilterWhere(['like', 'scope_of_service', $this->scope_of_service])
            ->andFilterWhere(['like', 'advance_payment_terms', $this->advance_payment_terms])
            ->andFilterWhere(['like', 'related_to_buyer', $this->related_to_buyer])
            ->andFilterWhere(['like', 'related_to_owner', $this->related_to_owner])
            ->andFilterWhere(['like', 'related_to_client', $this->related_to_client])
            ->andFilterWhere(['like', 'related_to_property', $this->related_to_property])
            ->andFilterWhere(['like', 'related_to_buyer_reason', $this->related_to_buyer_reason])
            ->andFilterWhere(['like', 'related_to_owner_reason', $this->related_to_owner_reason])
            ->andFilterWhere(['like', 'related_to_client_reason', $this->related_to_client_reason])
            ->andFilterWhere(['like', 'related_to_property_reason', $this->related_to_property_reason])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'phone_number', $this->phone_number])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'relative_discount', $this->relative_discount])
            ->andFilterWhere(['like', 'turn_around_time', $this->turn_around_time])
            ->andFilterWhere(['like', 'valuer_name', $this->valuer_name])
            ->andFilterWhere(['like', 'date', $this->date])
            // ->andFilterWhere(['like', 'quotation_status', $this->quotation_status])
            ->andFilterWhere(['like', 'payment_slip', $this->payment_slip])
            ->andFilterWhere(['like', 'toe_document', $this->toe_document])
            ->andFilterWhere(['like', 'quotation_recommended_fee', $this->quotation_recommended_fee])
            ->andFilterWhere(['like', 'quotation_turn_around_time', $this->quotation_turn_around_time])
            ->andFilterWhere(['like', 'toe_final_fee', $this->toe_final_fee])
            ->andFilterWhere(['like', 'quotation_final_fee', $this->quotation_final_fee])
            ->andFilterWhere(['like', 'toe_final_turned_around_time', $this->toe_final_turned_around_time])
            ->andFilterWhere(['like', 'assumptions', $this->assumptions])
            ->andFilterWhere(['like', 'crm_quotations.client_reference', $this->client_reference])
            ->andFilterWhere(['like', 'status_change_date', $this->status_change_date])
            ->andFilterWhere(['like', 'inquiry_received_date', $this->inquiry_received_date])
            ->andFilterWhere(['like', 'quotation_sent_date', $this->quotation_sent_date])
            ->andFilterWhere(['like', 'toe_sent_date', $this->toe_sent_date])
            ->andFilterWhere(['like', 'payment_received_date', $this->payment_received_date])
            ->andFilterWhere(['like', 'toe_signed_and_received', $this->toe_signed_and_received])
            ->andFilterWhere(['like', 'toe_signed_and_received_date', $this->toe_signed_and_received_date])
            ->andFilterWhere(['like', 'on_hold_date', $this->on_hold_date])
            ->andFilterWhere(['like', 'grand_final_toe_fee', $this->grand_final_toe_fee])

            ;

            return $dataProvider;
        }
        
   






    public function dashboard_searche($params)
    {
        


        $today_date = date("Y-m-d");
        $q_tbl = CrmQuotations::tableName();

        $query = CrmQuotations::find();
        $query->where([$q_tbl.'.trashed' => null]);
        
        // Add other conditions and configurations to the query
        
        // Create the ActiveDataProvider with the query object
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => false, // Disable pagination
            // 'pagination' => [
            //     'pageSize' => 50, // Set the number of items per page here
            // ],
        ]);
        
        $this->load($params);


        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }


        }else{
            $from_date = '2020-04-28';
            $to_date = date('Y-m-d');
        }


        // dd($to_date);

        // Apply additional filters or conditions based on the search model's attributes
        $startDate = date('Y-m-d').' 00:00:00';
        $endDate = date('Y-m-d').' 23:59:59';

        if($this->widget_type=='today'){
            $this->widget_view = 'today_quotations_listing';
        }
        elseif($this->widget_type=='all'){
            $this->widget_view = 'all_quotations_listing';
        }
        elseif($this->widget_type=='pending'){
            $this->widget_view = 'pending_listing';
        }

        if($this->status_type=='inquiry_received'){
            if($this->widget_type=='today'){
                $this->widget_view = 'today_inquiry_received';
                $this->page_title = 'Today Inquiry Received';
                $query->andFilterWhere(['like', $q_tbl.'.created_at', $today_date]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total Inquiry Received';
                $query->andWhere(['quotation_status' => 0]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
            elseif($this->widget_type=='pending'){
                $this->widget_view = 'pending_inquiry_received';
                $this->page_title = 'Pending Inquiry Received';
                $this->list_columns = 'inquiry_received';
                $query->andWhere([$q_tbl.'.quotation_status' => 0]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere([$q_tbl.'.status_approve' => null]);

            }
        }elseif($this->status_type=='inquiry_overview'){
            if($this->widget_type=='all'){
                $this->page_title = 'Total Proposals Overview';
                $query->andWhere(['quotation_status' => [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $subQuery = \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]]);
                $query->andWhere(['not', [$q_tbl.'.id' => $subQuery]]);
            }
        }elseif($this->status_type=='performance_overview'){
            if($this->widget_type=='pending'){
                $this->page_title = 'Pending Proposals Overview';
                $this->list_columns = 'performance_overview';
                $query->andWhere(['in', $q_tbl.'.quotation_status', [17, 14, 2, 16, 10]]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
        }
        elseif($this->status_type=='quotation_recommended'){
            if($this->widget_type=='all'){
                $this->page_title = 'Total Proposals Recommended';
                $query->andWhere(['quotation_status' => 17]);
                $query->orWhere(['not', ['quotation_recommended_date' => null]]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere(['>=', $q_tbl . '.id', 2300]);
            }elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending Quotation Recommended';
                $this->list_columns = 'quotation_recommended';
                $query->andWhere([$q_tbl.'.quotation_status' => 17]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere([$q_tbl.'.status_approve' => 'Recommended']);
            }
        }
        elseif($this->status_type=='document_requested'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today Document Requested';
                $query->andWhere([$q_tbl.'.trashed' => "54678"]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total Documents Requested';
                $query->andWhere(['quotation_status' => 14]);
                $query->orWhere(['not', ['document_requested_date' => null]]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
            elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending Document Requested';
                $this->list_columns = 'document_requested';
                $query->andWhere([$q_tbl.'.quotation_status' => 14]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere([$q_tbl.'.email_status_docs' => 1]);
            }
            
        }
        elseif($this->status_type=='quotation_approved'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today Proposals Verified';
                $query->andFilterWhere(['like', $q_tbl.'.approved_date', $today_date]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total Proposals Verified';
                $query->andWhere(['quotation_status' => 2]);
                $query->orWhere(['not', [$q_tbl.'.approved_date' => null]]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
            elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending Quotation Verified';
                $this->list_columns = 'quotation_approved';
                $query->andWhere([$q_tbl.'.quotation_status' => 2]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere([$q_tbl.'.status_approve' => 'Approve']);
                // $query->andWhere([$q_tbl.'.email_status_q_approved' => 1]);
            }
        }
        elseif($this->status_type=='quotation_onhold'){
            if($this->widget_type=='all'){
                $this->page_title = 'Total Proposals On Hold';
                $query->andWhere(['quotation_status' => 10]);
                $query->andWhere(['not', [$q_tbl.'.on_hold_date' => null]]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending Quotation On Hold';
                $this->list_columns = 'quotation_onhold';
                $query->andWhere([$q_tbl.'.quotation_status' => 10]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere(['not', [$q_tbl.'.on_hold_date' => null]]);
            }
        }
        elseif($this->status_type=='quotation_accepted'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today Quotation Approved';
                $query->andWhere([$q_tbl.'.trashed' => "54678"]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total Proposals Approved';
                // $query->andWhere(['quotation_status' => 16]);
                $query->andWhere(['in', $q_tbl.'.quotation_status', [16, 3, 4, 5, 6, 8]]);
                $query->orWhere(['not', ['quotation_accepted_date' => null]]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
            elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending Quotation Approved';
                $this->list_columns = 'quotation_accepted';
                $query->andWhere([$q_tbl.'.quotation_status' => 16]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere(['not', [$q_tbl.'.quotation_accepted_date' => null]]);
            }
        }        
        elseif($this->status_type=='quotation_sent'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today Proposals Sent';
                $query->andFilterWhere(['like', $q_tbl.'.quotation_sent_date', $today_date]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total Quotation Sent';
                $query->andWhere(['quotation_status' => 1]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
            elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending Quotation Sent';
                $this->list_columns = 'quotation_sent';
                $query->andWhere([$q_tbl.'.quotation_status' => 1]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere(['not', [$q_tbl.'.quotation_sent_date' => null]]);

            }
        }
        elseif($this->status_type=='quotation_rejected'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today Quotation Rejected';
                $query->andFilterWhere(['like', $q_tbl.'.quotation_rejected_date', $today_date]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total Proposals Rejected';
                $query->where(['quotation_status' => 7]);
                $query->andWhere(['not', [$q_tbl.'.quotation_rejected_date' => null]]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
        }
        elseif($this->status_type=='quotation_cancelled'){
            if($this->widget_type=='all'){
                $this->page_title = 'Total Proposals Cancelled';
                $query->where(['quotation_status' => 11]);
                $query->andWhere(['not', [$q_tbl.'.cancelled_date' => null]]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending Quotation Cancelled';
                $this->list_columns = 'quotation_cancelled';
                $query->andWhere([$q_tbl.'.quotation_status' => 11]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere(['not', [$q_tbl.'.cancelled_date' => null]]);
            }
        }
        elseif($this->status_type=='toe_recommended'){
            if($this->widget_type=='all'){ 
                $this->page_title = 'Total TOE Recommended';
                // $query->andWhere(['toe_recommended' => 2]);
                $query->andWhere(['not', [$q_tbl.'.toe_recommended' => null]]);
                $query->orWhere(['not', [$q_tbl.'.toe_recommended_date' => null]]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                
            }
            elseif($this->widget_type=='pending'){
                // $this->widget_view = 'today_toe_recommended';
                $this->list_columns = 'toe_recommended';
                $this->page_title = 'Pending TOE Recommended';
                $query->andWhere(['toe_recommended' => 1]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
        }
        elseif($this->status_type=='toe_sent'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today TOE Sent';
                $query->andFilterWhere(['like', $q_tbl.'.toe_sent_date', $today_date]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total TOE Sent';
                $query->andWhere(['quotation_status' => 3]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
            elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending TOE Sent';
                $this->list_columns = 'toe_sent';
                $query->andWhere([$q_tbl.'.quotation_status' => 16]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere(['not', [$q_tbl.'.toe_sent_date' => null]]);
            }
        }
        elseif($this->status_type=='toe_signed_and_received'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today TOE Signed And Received';
                $query->andFilterWhere(['like', $q_tbl.'.toe_signed_and_received_date', $today_date]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total TOE Signed And Received';
                // $query->andWhere(['quotation_status' => 4]);
                $query->andWhere(['not', [$q_tbl.'.toe_signed_and_received_date' => null]]);
                $query->andWhere(['trashed' => null]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
            elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending TOE Signed And Received';
                $this->list_columns = 'toe_signed_and_received';
                $query->andWhere([$q_tbl.'.quotation_status' => 5]);
                $query->andWhere([$q_tbl.'.toe_signed_and_received' => 5]);
                $query->andWhere([$q_tbl.'.payment_received_date' => null]);
                $query->andWhere([$q_tbl.'.converted' => 1]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere(['not', [$q_tbl.'.toe_signed_and_received_date' => null]]);
            }
        }
        elseif($this->status_type=='toe_rejected'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today TOE Rejected';
                $query->andFilterWhere(['like', $q_tbl.'.toe_rejected_date', $today_date]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total TOE Rejected';
                $query->where([$q_tbl.'.quotation_status' => 8]);
                $query->andWhere(['not', [$q_tbl.'.toe_rejected_date' => null]]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
        }
        elseif($this->status_type=='payment_received'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today Payment Received';
                $query->andFilterWhere(['like', $q_tbl.'.payment_received_date', $today_date]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total Payment Received';
                $query->andWhere(['not', [$q_tbl.'.payment_received_date' => null]]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
            elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending Payment Received';
                $this->list_columns = 'payment_received';
                $query->andWhere([$q_tbl.'.quotation_status' => 6]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                // $query->andWhere(['not', [$q_tbl.'.payment_received_date' => null]]);
            }
        }
        elseif($this->status_type=='tat_performance'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today TAT Performance';
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total TAT Performance';
                $query->andWhere(['in', $q_tbl.'.quotation_status', [6]]);
                // $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
            elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending TAT Performance';
                $this->list_columns = 'tat_performance';
                $query->andWhere(['in', $q_tbl.'.quotation_status', [0, 9, 2, 14, 16, 10, 11]]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
            }
        }
        elseif($this->status_type=='regretted'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today Regretted';
                $query->andFilterWhere(['like', $q_tbl.'.regretted_date', $today_date]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total Windmills Regretted';
                $query->andWhere([$q_tbl.'.quotation_status' => 12]);
                $query->andWhere(['not', [$q_tbl.'.regretted_date' => null]]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
            elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending Regretted';
                $this->list_columns = 'regretted';
                $query->andWhere([$q_tbl.'.quotation_status' => 12]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere(['not', [$q_tbl.'.regretted_date' => null]]);
            }
        }



        return $dataProvider;
    }

    public function dashboard_search($params)
    {



        $today_date = date("Y-m-d");
        $q_tbl = CrmQuotations::tableName();
        $query = $this->generateQuery($params);

       /* $query = CrmQuotations::find();
        $query->where([$q_tbl.'.trashed' => null]);*/

        // Add other conditions and configurations to the query

        // Create the ActiveDataProvider with the query object
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => false, // Disable pagination
            // 'pagination' => [
            //     'pageSize' => 50, // Set the number of items per page here
            // ],
        ]);

        $this->load($params);


        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }


        }else{
            $from_date = '2020-04-28';
            $to_date = date('Y-m-d');
        }


        // dd($to_date);

        // Apply additional filters or conditions based on the search model's attributes
        $startDate = date('Y-m-d').' 00:00:00';
        $endDate = date('Y-m-d').' 23:59:59';

        if($this->widget_type=='today'){
            $this->widget_view = 'today_quotations_listing';
        }
        elseif($this->widget_type=='all'){
            $this->widget_view = 'all_quotations_listing';
        }
        elseif($this->widget_type=='pending'){
            $this->widget_view = 'pending_listing';
        }

        if($this->status_type=='inquiry_received'){
            if($this->widget_type=='today'){
                $this->widget_view = 'today_inquiry_received';
                $this->page_title = 'Today Inquiry Received';
                $query->andFilterWhere(['like', $q_tbl.'.created_at', $today_date]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'All Inquiry Received';
                $query->andWhere(['quotation_status' => 0]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
            elseif($this->widget_type=='pending'){
                $this->widget_view = 'pending_inquiry_received';
                $this->page_title = 'Pending Inquiry Received';
                $this->list_columns = 'inquiry_received';
                $query->andWhere([$q_tbl.'.quotation_status' => 0]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere([$q_tbl.'.status_approve' => null]);

            }
        }elseif($this->status_type=='inquiry_overview'){
            if($this->widget_type=='all'){
                $this->page_title = 'Total Proposals Overview';
                $query->andWhere(['quotation_status' => [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
        }elseif($this->status_type=='performance_overview'){
            if($this->widget_type=='pending'){
                $this->page_title = 'Pending Proposals Overview';
                $this->list_columns = 'performance_overview';
                $query->andWhere(['in', $q_tbl.'.quotation_status', [17, 14, 2, 16, 10]]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
        }
        elseif($this->status_type=='quotation_recommended'){
            if($this->widget_type=='all'){
                $this->page_title = 'Total Proposals Recommended';
                $query->andWhere(['quotation_status' => 17]);
                $query->orWhere(['not', ['quotation_recommended_date' => null]]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere(['>=', $q_tbl . '.id', 2300]);
            }elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending Quotation Recommended';
                $this->list_columns = 'quotation_recommended';
                $query->andWhere([$q_tbl.'.quotation_status' => 17]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere([$q_tbl.'.status_approve' => 'Recommended']);
            }
        }
        elseif($this->status_type=='quotation_approved'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today Quotation Verified';
                $query->andFilterWhere(['like', $q_tbl.'.approved_date', $today_date]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total Proposals Verified';
                $query->andWhere(['quotation_status' => 2]);
                $query->orWhere(['not', [$q_tbl.'.approved_date' => null]]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
            elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending Proposals Verified';
                $this->list_columns = 'quotation_approved';
                $query->andWhere([$q_tbl.'.quotation_status' => 2]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere([$q_tbl.'.status_approve' => 'Approve']);
                // $query->andWhere([$q_tbl.'.email_status_q_approved' => 1]);
            }
        }
        elseif($this->status_type=='document_requested'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today Document Requested';
                $query->andWhere([$q_tbl.'.trashed' => "54678"]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total Documents Requested';
                $query->andWhere(['quotation_status' => 14]);
                $query->orWhere(['not', ['document_requested_date' => null]]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
            elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending Document Requested';
                $this->list_columns = 'document_requested';
                $query->andWhere([$q_tbl.'.quotation_status' => 14]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere([$q_tbl.'.email_status_docs' => 1]);
            }

        }
        elseif($this->status_type=='quotation_sent'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today Quotation Sent';
                $query->andFilterWhere(['like', $q_tbl.'.quotation_sent_date', $today_date]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total Quotation sent';
                $query->andWhere(['quotation_status' => 1]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
            elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending Quotation Sent';
                $this->list_columns = 'quotation_sent';
                $query->andWhere([$q_tbl.'.quotation_status' => 1]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere(['not', [$q_tbl.'.quotation_sent_date' => null]]);

            }
        }
        elseif($this->status_type=='quotation_accepted'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today Quotation Approved';
                $query->andWhere([$q_tbl.'.trashed' => "54678"]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total Proposals Approved';
                // $query->andWhere(['quotation_status' => 16]);
                $query->andWhere(['in', $q_tbl.'.quotation_status', [16, 3, 4, 5, 6, 8]]);
                $query->orWhere(['not', ['quotation_accepted_date' => null]]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
            elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending Quotation Approved';
                $this->list_columns = 'quotation_accepted';
                $query->andWhere([$q_tbl.'.quotation_status' => 16]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere(['not', [$q_tbl.'.quotation_accepted_date' => null]]);
            }
        }
        elseif($this->status_type=='toe_recommended'){
            if($this->widget_type=='all'){ 
                $this->page_title = 'Total TOE Recommended';
                // $query->andWhere(['toe_recommended' => 2]);
                $query->andWhere(['not', [$q_tbl.'.toe_recommended' => null]]);
                $query->orWhere(['not', [$q_tbl.'.toe_recommended_date' => null]]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                
            }
            elseif($this->widget_type=='pending'){
                // $this->widget_view = 'today_toe_recommended';
                $this->list_columns = 'toe_recommended';
                $this->page_title = 'Pending TOE Recommended';
                $query->andWhere(['toe_recommended' => 1]);
                $query->andWhere(['status_approve' => "toe_verified"]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
        }
        elseif($this->status_type=='toe_sent'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today TOE Sent';
                $query->andFilterWhere(['like', $q_tbl.'.toe_sent_date', $today_date]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'All TOE Sent';
                $query->andWhere(['quotation_status' => 3]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
            elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending TOE Sent';
                $this->list_columns = 'toe_sent';
                $query->andWhere([$q_tbl.'.quotation_status' => 16]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere(['not', [$q_tbl.'.toe_sent_date' => null]]);
            }
        }
        elseif($this->status_type=='toe_signed_and_received'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today TOE Signed And Received';
                $query->andFilterWhere(['like', $q_tbl.'.toe_signed_and_received_date', $today_date]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total TOE Signed And Received';
                $query->andWhere(['quotation_status' => 4]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
            elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending TOE Signed And Received';
                $this->list_columns = 'toe_signed_and_received';
                $query->andWhere([$q_tbl.'.quotation_status' => 5]);
                $query->andWhere([$q_tbl.'.toe_signed_and_received' => 5]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                $query->andWhere([$q_tbl.'.payment_received_date' => null]);
                $query->andWhere([$q_tbl.'.converted' => 1]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere(['not', [$q_tbl.'.toe_signed_and_received_date' => null]]);
            }
        }
        elseif($this->status_type=='toe_rejected'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today TOE Rejected';
                $query->andFilterWhere(['like', $q_tbl.'.toe_rejected_date', $today_date]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total TOE Rejected';
                $query->where([$q_tbl.'.quotation_status' => 8]);
                $query->andWhere(['not', [$q_tbl.'.toe_rejected_date' => null]]);
                $query->andFilterWhere(['between', 'created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
        }
        elseif($this->status_type=='payment_received'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today Payment Received';
                $query->andFilterWhere(['like', $q_tbl.'.payment_received_date', $today_date]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total Payment Received';
                $query->andWhere(['not', [$q_tbl.'.payment_received_date' => null]]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
            elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending Payment Received';
                $this->list_columns = 'payment_received';
                $query->andWhere([$q_tbl.'.quotation_status' => 6]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                // $query->andWhere(['not', [$q_tbl.'.payment_received_date' => null]]);
            }
        }
        elseif($this->status_type=='quotation_onhold'){
            if($this->widget_type=='all'){
                $this->page_title = 'Total Proposals On Hold';
                $query->andWhere(['quotation_status' => 10]);
                $query->andWhere(['not', [$q_tbl.'.on_hold_date' => null]]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending Quotation On Hold';
                $this->list_columns = 'quotation_onhold';
                $query->andWhere([$q_tbl.'.quotation_status' => 10]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere(['not', [$q_tbl.'.on_hold_date' => null]]);
            }
        }
        elseif($this->status_type=='quotation_cancelled'){
            if($this->widget_type=='all'){
                $this->page_title = 'Total Proposals Cancelled';
                $query->andWhere(['quotation_status' => 11]);
                $query->orWhere(['quotation_status' => 7]);
                // $query->andWhere(['not', [$q_tbl.'.cancelled_date' => null]]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending Quotation Cancelled';
                $this->list_columns = 'quotation_cancelled';
                $query->andWhere([$q_tbl.'.quotation_status' => 11]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere(['not', [$q_tbl.'.cancelled_date' => null]]);
            }
        }
        elseif($this->status_type=='tat_performance'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today TAT Performance';
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total TAT Performance';
                $query->andWhere(['in', $q_tbl.'.quotation_status', [6]]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                // $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
            elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending TAT Performance';
                $this->list_columns = 'tat_performance';
                $query->andWhere(['in', $q_tbl.'.quotation_status', [0, 9, 2, 14, 16, 10, 11]]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
            }
        }
        elseif($this->status_type=='quotation_rejected'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today Quotation Rejected';
                $query->andFilterWhere(['like', $q_tbl.'.quotation_rejected_date', $today_date]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total Proposals Rejected';
                $query->andWhere(['not', [$q_tbl.'.quotation_rejected_date' => null]]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
        }
        elseif($this->status_type=='toe_rejected'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today TOE Rejected';
                $query->andFilterWhere(['like', $q_tbl.'.toe_rejected_date', $today_date]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'All TOE Rejected';
                $query->andWhere(['not', [$q_tbl.'.toe_rejected_date' => null]]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
        }
        elseif($this->status_type=='regretted'){
            if($this->widget_type=='today'){
                $this->page_title = 'Today Regretted';
                $query->andFilterWhere(['like', $q_tbl.'.regretted_date', $today_date]);
            }
            elseif($this->widget_type=='all'){
                $this->page_title = 'Total Windmills Regretted';
                $query->andWhere(['not', [$q_tbl.'.regretted_date' => null]]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            }
            elseif($this->widget_type=='pending'){
                $this->page_title = 'Pending Regretted';
                $this->list_columns = 'regretted';
                $query->andWhere([$q_tbl.'.quotation_status' => 12]);
                $query->andWhere([$q_tbl.'.trashed' => null]);
                $query->andWhere([$q_tbl.'.converted' => null]);
                $query->andFilterWhere(['between', 'crm_quotations.created_at', $from_date, $to_date]);
                $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
                // $query->andWhere(['not', [$q_tbl.'.regretted_date' => null]]);
            }
        }



        return $dataProvider;
    }
    

    public function generateQuery($params)
    {
        $this->load($params);
        $query = CrmQuotations::find()
            ->select([
                CrmQuotations::tableName().'.id',
                CrmQuotations::tableName().'.instruction_date',
                CrmQuotations::tableName().'.final_quoted_fee',
                CrmQuotations::tableName().'.inquiry_received_time',
                CrmQuotations::tableName().'.target_date',
                CrmQuotations::tableName().'.approved_date',
                CrmQuotations::tableName().'.quotation_accepted_date',
                CrmQuotations::tableName().'.toe_signed_and_received_date',
                CrmQuotations::tableName().'.toe_signed_and_received_date',
                Company::tableName().'.nick_name as client_nick_name',
                CrmQuotations::tableName().'.no_of_properties',
                CrmQuotations::tableName().'.final_quoted_fee',
                CrmQuotations::tableName().'.quotation_status',
                CrmQuotations::tableName().'.payment_received_date',
                CrmQuotations::tableName().'.reference_number',
                CrmQuotations::tableName().'.quotation_recommended_date',
                CrmQuotations::tableName().'.quotation_sent_date',
                CrmQuotations::tableName().'.toe_sent_date',
                CrmQuotations::tableName().'.toe_approved_date',
                CrmQuotations::tableName().'.total_time_days',
                CrmQuotations::tableName().'.total_time_hours',
                CrmQuotations::tableName().'.verify_time_days',
                CrmQuotations::tableName().'.verify_time_hours',
                CrmQuotations::tableName().'.cancelled_date_days',
                CrmQuotations::tableName().'.cancelled_date_hours',
                CrmQuotations::tableName().'.client_name',
                CrmQuotations::tableName().'.quotation_cancel_reason',
                User::tableName().'.firstname as crm_user_first_name',
                User::tableName().'.lastname as crm_user_last_name',
                UserProfileInfo::tableName().'.mobile as crm_user_mobile',
                UserProfileInfo::tableName().'.phone1 as crm_user_phone1',

            ])
            ->leftJoin(Company::tableName(),company::tableName().'.id='.CrmQuotations::tableName().'.client_name')
            ->leftJoin(User::tableName(),User::tableName().'.company_id='.company::tableName().'.id')
            ->leftJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().'.user_id='.User::tableName().'.id')
            ->where([CrmQuotations::tableName().'.trashed' => null])
            ->andWhere([UserProfileInfo::tableName().'.primary_contact' => 1]);

        // grid filtering conditions
     /*   $query->andFilterWhere([
            Communities::tableName().'.id' => $this->id,
            Communities::tableName().'.status' => $this->status,
            Communities::tableName().'.trashed' => 0,
        ]);

        $query->andFilterWhere(['like',Communities::tableName().'.title',$this->title])
            ->andFilterWhere(['like',Zone::tableName().'.title',$this->city]);*/

        return $query;
    }


    public function dashboard_dynamic_search($params)
    {
        $today_date = date("Y-m-d");
        $q_tbl = CrmQuotations::tableName();

        $query = CrmQuotations::find();
        $query->where([$q_tbl.'.trashed' => null]);
        
        // Add other conditions and configurations to the query
        
        // Create the ActiveDataProvider with the query object
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => false, // Disable pagination
        ]);

        $this->load($params);

        // Apply additional filters or conditions based on the search model's attributes
        $today_start_date = date('Y-m-d').' 00:00:00';
        $today_end_date = date('Y-m-d').' 23:59:59';
        // $today_start_date = '2024-02-23  00:00:00';
        // $today_end_date = '2024-02-23  23:59:59';

        $this->widget_view = 'today_dynamic_listing';

        if($this->status_type=='inquiry_overview'){
            $this->widget_view = 'today_inquiry_overview';
            $this->page_title = 'Today Proposals Overview';
            // $query->andFilterWhere(['like', $q_tbl.'.created_at', $today_date]);
            $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            $query->andFilterWhere(['between', 'created_at', $today_start_date, $today_end_date]);
        }

        elseif($this->status_type=='today_recommended'){
            $this->widget_view = 'today_proposals_recommended';
            $this->page_title = 'Today Proposals Recommended';     
            // $query->orWhere([$q_tbl.'.quotation_status' => 17]);
            // $query->orWhere([$q_tbl.'.status_approve' => 'Recommended']);
            // $query->andFilterWhere(['like', $q_tbl.'.quotation_recommended_date', $today_date]); 
            $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            $query->andFilterWhere(['between', 'quotation_recommended_date', $today_start_date, $today_end_date]);   
        }

        elseif($this->status_type=='quotation_approved'){
            $this->widget_view = 'today_proposals_approved';
            $this->page_title = 'Today Proposals Verified';
            // $query->orWhere([$q_tbl.'.quotation_status' => 2]);
            // $query->orWhere([$q_tbl.'.status_approve' => 'Approve']);
            // $query->andFilterWhere(['like', $q_tbl.'.approved_date', $today_date]);
            $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            $query->andFilterWhere(['between', 'approved_date', $today_start_date, $today_end_date]);   

        }

        elseif($this->status_type=='documents_requested'){
            $this->widget_view = 'today_documents_requested';
            $this->page_title = 'Today Documents Requested';
            // $query->orWhere([$q_tbl.'.quotation_status' => 14]);
            // $query->andFilterWhere(['like', $q_tbl.'.document_requested_date', $today_date]);
            $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            $query->andFilterWhere(['between', 'document_requested_date', $today_start_date, $today_end_date]);   

        }

        elseif($this->status_type=='inquiry_received'){
            $this->page_title = 'Today Inquiry Received (Dynamic)';
            // $query->andWhere([$q_tbl.'.quotation_status' => 0]);
            // $query->andFilterWhere(['like', $q_tbl.'.created_at', $today_date]);
            $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            $query->andFilterWhere(['between', 'created_at', $today_start_date, $today_end_date]);
        }

        elseif($this->status_type=='quotation_hold'){
            $this->widget_view = 'today_quotation_hold';
            $this->page_title = 'Today Proposals On Hold';
            // $query->orWhere([$q_tbl.'.quotation_status' => 10]);
            // $query->andFilterWhere(['like', $q_tbl.'.on_hold_date', $today_date]);
            $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            $query->andFilterWhere(['between', 'on_hold_date', $today_start_date, $today_end_date]);

        }

        elseif($this->status_type=='quotation_accepted'){
            $this->widget_view = 'today_quotation_accepted';
            $this->page_title = 'Today Proposals Approved';
            // $query->orWhere([$q_tbl.'.quotation_status' => 16]);
            // $query->andFilterWhere(['like', $q_tbl.'.quotation_accepted_date', $today_date]);
            $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            $query->andFilterWhere(['between', 'quotation_accepted_date', $today_start_date, $today_end_date]);

        }

        elseif($this->status_type=='quotation_rejected'){
            $this->widget_view = 'today_quotation_rejected';
            $this->page_title = 'Today Proposals Rejected';
            // $query->orWhere([$q_tbl.'.quotation_status' => 7]);
            // $query->andFilterWhere(['like', $q_tbl.'.quotation_rejected_date', $today_date]);
            $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            $query->andFilterWhere(['between', 'quotation_rejected_date', $today_start_date, $today_end_date]);

        }

        elseif($this->status_type=='toe_sent'){
            $this->page_title = 'Today TOE Sent (Dynamic)';
            // $query->orWhere([$q_tbl.'.quotation_status' => 3]);
            // $query->andFilterWhere(['like', $q_tbl.'.created_at', $today_date]);
            $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            $query->andFilterWhere(['between', 'toe_sent_date', $today_start_date, $today_end_date]);
        }

        elseif($this->status_type=='toe_signed'){
            $this->widget_view = 'today_toe_signed';
            $this->page_title = 'Today TOE Signed';
            // $query->orWhere([$q_tbl.'.quotation_status' => 5]);
            // $query->andFilterWhere(['like', $q_tbl.'.toe_signed_and_received_date', $today_date]);
            $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            $query->andFilterWhere(['between', 'toe_signed_and_received_date', $today_start_date, $today_end_date]);
        }
        
        elseif($this->status_type=='toe_recommended'){
            $this->widget_view = 'today_toe_recommended';
            $this->page_title = 'Today TOE Recommended';
            // $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            $query->andWhere(['toe_recommended' => 1]);
            $query->andFilterWhere(['between', 'status_change_date', $today_start_date, $today_end_date]);
        }

        // elseif($this->status_type=='pending_toe_recommended'){
        //     $this->widget_view = 'today_toe_recommended';
        //     $this->page_title = 'Pending TOE Recommended';
        //     $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
        //     $query->andWhere(['toe_recommended' => 1]);
        // }

        // elseif($this->status_type=='total_toe_recommended'){
        //     $this->widget_view = 'today_toe_recommended';
        //     $this->page_title = 'Total TOE Recommended';
        //     $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
        //     $query->andWhere(['toe_recommended' => 2]);
        // }

        elseif($this->status_type=='toe_rejected'){
            $this->widget_view = 'today_toe_rejected';
            $this->page_title = 'Today TOE Rejected';
            // $query->orWhere([$q_tbl.'.quotation_status' => 8]);
            // $query->andFilterWhere(['like', $q_tbl.'.toe_rejected_date', $today_date]);
            $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            $query->andFilterWhere(['between', 'toe_rejected_date', $today_start_date, $today_end_date]);

        }

        elseif($this->status_type=='payment_received'){
            $this->widget_view = 'today_payment_received';
            $this->page_title = 'Today Payment Received';
            // $query->orWhere([$q_tbl.'.quotation_status' => 6]);
            // $query->andFilterWhere(['like', $q_tbl.'.payment_received_date', $today_date]);
            $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            $query->andFilterWhere(['between', 'payment_received_date', $today_start_date, $today_end_date]);

        }

        elseif($this->status_type=='regretted'){
            $this->widget_view = 'today_proposals_regretted';
            $this->page_title = 'Today Windmills Regretted';
            // $query->orWhere([$q_tbl.'.quotation_status' => 12]);
            // $query->andFilterWhere(['like', $q_tbl.'.regretted_date', $today_date]);
            $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            $query->andFilterWhere(['between', 'regretted_date', $today_start_date, $today_end_date]);

        }

        elseif($this->status_type=='today_quotation_cancelled'){
            $this->widget_view = 'today_quotation_cancelled';
            $this->page_title = 'Today Proposals Cancelled';
            // $query->orWhere([$q_tbl.'.quotation_status' => 11]);
            // $query->andFilterWhere(['like', $q_tbl.'.cancelled_date', $today_date]);
            $query->andWhere(['not', [$q_tbl.'.id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]]);
            $query->andFilterWhere(['between', 'cancelled_date', $today_start_date, $today_end_date]);

        }

        // dd($this->widget_view);

        return $dataProvider;
    }



    public function search_pending_tat_reports($params)
    {
        $query = \app\models\CrmQuotations::find()
            ->select([
                \app\models\CrmQuotations::tableName().'.id',
                \app\models\CrmQuotations::tableName().'.reference_number',
                \app\models\CrmQuotations::tableName().'.instruction_date',
                \app\models\CrmQuotations::tableName().'.target_date',
                \app\models\CrmQuotations::tableName().'.created_at',
            ])
            ->where([\app\models\CrmQuotations::tableName().'.quotation_status' => 9])
            ->andWhere([\app\models\CrmQuotations::tableName().'.converted' => null])
            ->andWhere([\app\models\CrmQuotations::tableName().'.parent_id' => null]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        // $this->load($params);

        // if (!$this->validate()) {
        //     return $dataProvider;
        // }
        // if($this->time_period <> null && $this->time_period != 0 ) {
        //     if($this->time_period == 9){
        //         if($this->custom_date_btw <> null) {

        //             $Date=(explode(" - ",$this->custom_date_btw));
        //             $from_date = $Date[0];
        //             $to_date = $Date[1];
        //         }
        //     }else{
        //         $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
        //         $from_date = $date_array['start_date'];
        //         $to_date = $date_array['end_date'];
        //     }

        //     $query->andFilterWhere([
        //         'between', 'DATE(valuation_approvers_data.created_at)', $from_date, $to_date
        //     ]);
        // }else{
        //     if($this->target != 'all_aheadoftime'){
        //         $date_array = Yii::$app->appHelperFunctions->getFilterDates(8);
        //         $from_date = $date_array['start_date'];
        //         $to_date = $date_array['end_date'];
                
        //         $query->andFilterWhere([
        //             'between', 'DATE(valuation_approvers_data.created_at)', $from_date, $to_date
        //         ]);
        //     }
        // }
        // $pagelist=[1=>'20',2=>'50',3=>'1000',4=>'Show All'];
        // if ($this->pageSize<>null) {
        //     if ($this->pageSize==4) {

        //         $dataProvider->pagination->pageSize=false;
        //     }else {
        //         $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
        //     }

        // }
        // $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }

    
    
    
}
