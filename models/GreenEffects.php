<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "green_effects".
 *
 * @property int $id
 * @property int $status
 * @property int|null $created_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 * @property int|null $status_verified
 * @property int|null $status_verified_by
 * @property string|null $status_verified_at
 * @property int|null $valuation_id
 * @property int|null $green_efficient_certification
 * @property int|null $certifier_name
 * @property int|null $certification_level
 * @property int|null $source_of_green_certificate_information
 */
class GreenEffects extends ActiveRecordFull
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'green_effects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'created_by', 'updated_by', 'trashed', 'trashed_by', 'status_verified', 'status_verified_by', 'valuation_id', 'green_efficient_certification', 'certifier_name', 'certification_level', 'source_of_green_certificate_information'], 'integer'],
            [['created_at', 'updated_at', 'trashed_at', 'status_verified_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
            'status_verified' => 'Status Verified',
            'status_verified_by' => 'Status Verified By',
            'status_verified_at' => 'Status Verified At',
            'valuation_id' => 'Valuation ID',
            'green_efficient_certification' => 'Certification Done',
            'certifier_name' => 'Certifier Name',
            'certification_level' => 'Certification Level',
            'source_of_green_certificate_information' => 'Source of Certiification',
        ];
    }
    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Green effect';
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->id;
    }
    public function afterSave($insert, $changedAttributes)
    {

        $valuation = InspectProperty::findOne($this->valuation_id);
        Yii::$app->db->createCommand()->update('inspect_property',
            ['green_efficient_certification' =>  $this->green_efficient_certification,
                'certifier_name' =>  $this->certifier_name,
                'certification_level' =>  $this->certification_level,
                'source_of_green_certificate_information' =>  $this->source_of_green_certificate_information,
                ],
            ['valuation_id' => $this->valuation_id])->execute();



        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeSave($insert){

     
        // verify status reset after edit only something changed
        $isChanged = false;
        $ignoredAttributes = ['updated_at','status_verified_at']; 
        foreach ($this->attributes as $attribute => $value) {
            if (in_array($attribute, $ignoredAttributes)) {
                continue;
            }
            if ($this->getOldAttribute($attribute) != $value) {
                $isChanged = true;
                break;
            }
        }
        if ($isChanged) {
            if($this->status == 1 && $this->getOldAttribute('status') == 1){
                $this->status = 2;
            }elseif ($this->status == 2 && $this->getOldAttribute('status') == 1) {
                $this->status = 1;
            }
        }
        
        return parent::beforeSave($insert);
    }
}
