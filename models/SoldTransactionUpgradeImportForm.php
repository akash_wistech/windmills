<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * SoldTransactionImportForm is the model behind the sold transaction import form.
 */
class SoldTransactionUpgradeImportForm extends Model
{
    public $importfile;
    public $allowedFileTypes=['csv'];//'xls', 'xlsx',
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['importfile'], 'safe'],
            [['importfile'], 'file', 'extensions'=>implode(",",$this->allowedFileTypes)]
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'importfile' => Yii::t('app', 'File'),
        ];
    }

    /**
     * import the entries into table
     */
    public function save()
    {

        $saved=0;
        $unsaved=0;
        $errNames='';
        $change = array();
        if ($this->validate()) {
            if(UploadedFile::getInstance($this, 'importfile')){
                $importedFile = UploadedFile::getInstance($this, 'importfile');

                // if no image was uploaded abort the upload
                if (!empty($importedFile)) {
                    $pInfo=pathinfo($importedFile->name);

                    $ext = $pInfo['extension'];

                    if (in_array($ext,$this->allowedFileTypes)) {
                        // Check to see if any PHP files are trying to be uploaded
                        $content = file_get_contents($importedFile->tempName);

                        if (preg_match('/\<\?php/i', $content)) {

                        }else{
                            $this->importfile = Yii::$app->params['temp_abs_path'].$importedFile->name;
                            $importedFile->saveAs($this->importfile);

                            $csvFile = fopen($this->importfile, 'r');

                            $data = [];
                            $n=1;
                            while (($line = fgetcsv($csvFile)) !== FALSE) {

                                $transaction_type = $line[0];
                                $sub_type = $line[0];
                                $sales_sequence = $line[0];
                                $reidin_ref_number = $line[0];
                                $date = str_replace('/', '-', trim($line[5]));
                                // echo $date; die();
                                $date = date("Y-m-d",strtotime($date));
                                $reidin_community = $line[0];
                                $reidin_property = $line[0];
                                $reidin_property_type = $line[0];
                                $reidin_developer= $line[0];
                                $buildingName = $line[0];
                                $rooms = $line[1];
                                $bua = str_replace(",","",$line[2]);
                                $plotArea = str_replace(",","",$line[3]);
                                $type = $line[4];

                                $price = str_replace(",","",$line[6]);
                                $pricePerSqt = str_replace(",","",$line[7]);


                                if($n>1) {
                                    $buildingId = 0;
                                    $buildingRow = Buildings::find()->where(['=', 'title', trim($buildingName)])->asArray()->one();


                                    if ($buildingRow != null) {

                                        $comparison = SoldTransactionUpgrade::find()
                                            ->where(['building_info'=>$buildingRow['id'],
                                                'no_of_bedrooms'=>$rooms,
                                                'built_up_area'=>$bua,
                                                'land_size'=>$plotArea,
                                                'transaction_date' =>$date,
                                                'listings_price' =>$price,
                                                'price_per_sqt' =>$pricePerSqt ])
                                            // ->asArray()
                                            ->one();

                                        if ($comparison==null) {
                                            // echo "Record Found"; die();
                                            $buildingId = $buildingRow['id'];


                                            $model = new SoldTransactionUpgrade;
                                            $model->scenario = 'import';
                                            $model->building_info = $buildingId;
                                            $model->no_of_bedrooms = ($rooms != 'Unknown')? $rooms: 0;
                                            $model->built_up_area =  ($bua <> null && $bua !== '-') ? $bua : 0;
                                            $model->land_size =($plotArea <> null && $plotArea !== '-') ? $plotArea : 0; ;
                                            //$model->type=$type;
                                            $model->transaction_date = $date;
                                            $model->listings_price = ($price <> null && $price !== '-') ? $price : 0;

                                            $model->property_category = $buildingRow['property_category'];
                                            $model->location = $buildingRow['location'];
                                            $model->tenure = $buildingRow['tenure'];
                                            $model->utilities_connected = $buildingRow['utilities_connected'];
                                            $model->development_type = $buildingRow['development_type'];
                                            $model->property_placement = $buildingRow['property_placement'];
                                            $model->property_visibility = $buildingRow['property_visibility'];
                                            $model->property_exposure = $buildingRow['property_exposure'];
                                            $model->property_condition = $buildingRow['property_condition'];
                                            $model->pool = $buildingRow['pool'];
                                            $model->gym = $buildingRow['gym'];
                                            $model->play_area = $buildingRow['play_area'];
                                            // $model->other_facilities = $buildingRow['other_facilities'];
                                            $model->landscaping = $buildingRow['landscaping'];
                                            $model->white_goods = $buildingRow['white_goods'];
                                            $model->furnished = $buildingRow['furnished'];
                                            $model->finished_status = $buildingRow['finished_status'];


                                            $model->price_per_sqt=($pricePerSqt <> null && $pricePerSqt !== '#VALUE!') ? $pricePerSqt : 0;;

                                            /* echo "<pre>";
                                             print_r($model);
                                             die;*/
                                            if ($model->save()) {
                                                $saved++;
                                            } else {
                                                if ($model->hasErrors()) {
                                                    foreach ($model->getErrors() as $error) {
                                                        if (count($error) > 0) {
                                                            foreach ($error as $key => $val) {
                                                                echo $val;
                                                            }
                                                        }
                                                    }
                                                }
                                                die();
                                                $errNames .= '<br />' . $buildingName;
                                                $unsaved++;
                                            }


                                        }





                                    }else{
                                        $change[] = $buildingName;
                                    }
                                }
                                $n++;
                            }
                            // echo "No New Record All Records Are Already Exists";die();
// echo "<pre>";
// print_r($bua);
// echo "</pre>";
// die();
                            // echo "<pre>";
                            // print_r($change);
                            // die;
                            fclose($csvFile);
                            //Unlink File
                            unlink($this->importfile);
                        }
                    }
                }
            }
            if($saved>0){

                Yii::$app->getSession()->setFlash('success', $saved.' - '.Yii::t('app','rows saved successfully'));
            }
            if($unsaved>0){
                Yii::$app->getSession()->setFlash('error', $unsaved.' - '.Yii::t('app','rows were not saved!').$errNames);
            }
            return true;
        } else {
            return false;
        }
    }
}
