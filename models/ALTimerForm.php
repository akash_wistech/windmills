<?php

namespace app\models;

use Yii;

/**
* ALTimerForm is the model behind the action log time form.
*/
class ALTimerForm extends ALForms
{
  public $id,$hours,$minutes,$start_dt,$end_dt;
  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['module_type','module_id','comments','start_dt','end_dt'],'required'],
      [['module_type','comments','minutes','start_dt','end_dt'],'string'],
      [['id','module_id','hours','visibility'],'integer'],
      [['comments'],'trim'],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'comments' => 'Comments',
      'start_dt' => 'Time Started',
      'end_dt' => 'Time Ended',
      'visibility' => 'Visibility',
    ];
  }

  /**
  * Save Action Log Call Info
  */
  public function save()
  {
    if ($this->validate()) {
      $checkAlready=$this->checkAlready;
      if(!$checkAlready->exists()){
        if($this->id>0){
          $comment = ActionLog::findOne($this->id);
          $logMsg=''.Yii::$app->user->identity->name.' update a '.$this->module_type.' type '.$this->rec_type.' for '.$this->module_id.' with id '.$this->id;
        }else{
          $comment = new ActionLog;
          $comment->module_type=$this->module_type;
          $comment->module_id=$this->module_id;
          $comment->rec_type=$this->rec_type;
          $logMsg=''.Yii::$app->user->identity->name.' saved a '.$this->module_type.' type '.$this->rec_type.' for '.$this->module_id;
        }
        $comment->comments=$this->comments;
        $comment->hours=$this->hours;
        $comment->minutes=$this->minutes;
        $comment->start_dt=$this->start_dt;
        $comment->end_dt=$this->end_dt;
        $comment->visibility=$this->visibility;
        if(!$comment->save()){
          if($comment->hasErrors()){
            foreach($comment->getErrors() as $error){
              if(count($error)>0){
                foreach($error as $key=>$val){
                  $this->addError('',$val);
                  return false;
                }
              }
            }
          }
        }
        ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, ''.Yii::$app->user->identity->name.' saved a '.$this->module_type.' type '.$this->rec_type.' for '.$this->module_id);
      }else{
        return true;
      }
      return true;
    }
    return false;
  }
}
