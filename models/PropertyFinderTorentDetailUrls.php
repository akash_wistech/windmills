<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "property_finder_torent_detail_urls".
 *
 * @property int $id
 * @property string|null $url
 * @property int|null $status
 * @property string|null $property_category
 */
class PropertyFinderTorentDetailUrls extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'property_finder_torent_detail_urls';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['url', 'property_category'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'status' => 'Status',
            'property_category' => 'Property Category',
        ];
    }
}
