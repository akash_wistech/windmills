<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordTsBa;

/**
* This is the model class for table "{{%custom_field_google_map_info}}".
*
* @property int $id
* @property string $module_type
* @property int $module_id
* @property int $input_field_id
* @property string $map_location
* @property string $map_lat
* @property int $map_lat_org
* @property string $map_lng
* @property int $map_lng_org
* @property string $google_static_image
* @property string $created_at
* @property int $created_by
* @property string $updated_at
* @property int $updated_by
*/
class CustomFieldGoogleMapInfo extends ActiveRecordTsBa
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%custom_field_google_map_info}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['module_type','module_id'],'required'],
      [['created_at','updated_at'],'safe'],
      [['module_id','input_field_id','created_by','updated_by'],'integer'],
      [['map_lat_org','map_lng_org'],'number'],
      [['module_type','map_location','map_lat','map_lng','google_static_image'],'string'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'module_id' => Yii::t('app', 'Module'),
      'map_location' => Yii::t('app', 'Map Location'),
      'created_at' => Yii::t('app', 'Created'),
      'created_by' => Yii::t('app', 'Created By'),
      'updated_at' => Yii::t('app', 'Updated'),
      'updated_by' => Yii::t('app', 'Updated By'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function beforeSave($insert)
  {
    if (parent::beforeSave($insert)) {
      $this->map_lat_org=$this->map_lat;
      $this->map_lng_org=$this->map_lng;
      return true;
    }
  }
  /**
  * @inheritdoc
  */
  public function afterSave($insert, $changedAttributes)
  {
    if($this->map_lat!='' && $this->map_lng!=''){
      $googleImage=Yii::$app->fileHelperFunctions->copyGoogleImage($this->map_lat,$this->map_lng);
      $connection = \Yii::$app->db;
      $connection->createCommand(
        "update ".self::tableName()." set google_static_image=:google_static_image where id=:id",
        [':google_static_image'=>$googleImage,':id'=>$this->id]
        )->execute();
    }
    parent::afterSave($insert, $changedAttributes);
  }
}
