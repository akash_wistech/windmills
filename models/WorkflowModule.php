<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%workflow_module}}".
*
* @property integer $workflow_id
* @property string $module_type
*/
class WorkflowModule extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%workflow_module}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['workflow_id', 'module_type'], 'required'],
      [['workflow_id'], 'integer'],
      [['module_type'], 'string'],
    ];
  }

  public static function primaryKey()
  {
  	return ['workflow_id','module_type'];
  }
}
