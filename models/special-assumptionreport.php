<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\TinyMceAsset;
TinyMceAsset::register($this);

$this->registerJs('


tinymce.init({
  selector: ".editor",

  menubar: false,
  plugins: [
    "advlist autolink lists link image charmap print preview anchor",
    "searchreplace visualblocks code fullscreen",
    "insertdatetime media table paste code help wordcount"
  ],
  toolbar: "undo redo | formatselect | " +
  "bold italic backcolor | alignleft aligncenter " +
  "alignright alignjustify | bullist numlist outdent indent | " +
  "removeformat | help",
});





');



/* @var $this yii\web\View */
/* @var $model app\models\SpecialAssumptionreport */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="special-assumptionreport-form card card-outline card-primary">

    <?php $form = ActiveForm::begin(); ?>

    <header class="card-header">
        <h2 class="card-title"><?= $cardTitle?></h2>
    </header>

    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">

    <?= $form->field($model, 'vacant')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'owner_occupied')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'tenanted')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'gifted_granted')->textarea(['class'=>'editor']) ?>
  </div>
  <div class="col-sm-12">
    <?= $form->field($model, 'purchased')->textarea(['class'=>'editor']) ?>
</div>
    <div class="card-footer">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
