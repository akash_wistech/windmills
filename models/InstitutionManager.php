<?php

namespace app\models;

use Yii;

// dd($_REQUEST);

class InstitutionManager extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'institution_manager';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['institute_name', 'country_id' , 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'institute_name' => 'institute_name',
            'country_id' => 'country_id',
            'status' => 'status',
        ];
    }
}
