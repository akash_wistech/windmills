<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* SettingForm is the model behind the setting form.
*/
class SettingValForm extends Model
{
  public $emirates_country_id,$service_list_id,$currency_sign,$quickbook_client_id,$quickbook_client_secret,$quickbook_access_token,$quickbook_refresh_token,$quickbook_redirect_uri,$quickbook_oauth_scope,$quickbook_realm_id,$quickbook_tax_id;

  public $save_bayut_building;
  public $sold_percentage_calculation,$comparables_percentage_calculation,$previous_percentage_calculation;
  public $sold_percentage_calculation_2,$comparables_percentage_calculation_2,$previous_percentage_calculation_2;
  public $sold_percentage_calculation_3,$comparables_percentage_calculation_3,$previous_percentage_calculation_3;
  public $sold_percentage_calculation_4,$comparables_percentage_calculation_4,$previous_percentage_calculation_4;
  public $ajman_rera_followup_email_frequency;

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [[
        'sold_percentage_calculation','comparables_percentage_calculation','previous_percentage_calculation',
        'sold_percentage_calculation_2','comparables_percentage_calculation_2','previous_percentage_calculation_2',
        'sold_percentage_calculation_3','comparables_percentage_calculation_3','previous_percentage_calculation_3',
        'sold_percentage_calculation_4','comparables_percentage_calculation_4','previous_percentage_calculation_4',
        'ajman_rera_followup_email_frequency',
      ], 'safe'],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
    ];
  }

  /**
  * save the settings data.
  */
  public function save()
  {
    if ($this->validate()) {
      $post=Yii::$app->request->post();

   /*   echo "<pre>";
      print_r($post);
      die;*/



      foreach($post['SettingValForm'] as $key=>$val){
        $setting=SettingVal::find()->where("config_name='$key'")->one();
        if($setting==null){
          $setting=new SettingVal;
          $setting->config_name=$key;
        }
        $setting->config_value=$val;
        $setting->save();
      }
      return true;
    } else {
        echo "<pre>";
        print_r($this->errors);
        die;
      return false;
    }
  }
}
