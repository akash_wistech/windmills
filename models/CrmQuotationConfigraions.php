<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "crm_quotation_configraions".
 *
 * @property int $id
 * @property float|null $over_all_upgrade
 * @property int $valuation_id
 * @property string|null $checked_image
 * @property int $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class CrmQuotationConfigraions extends \yii\db\ActiveRecord
{
    public $config_bedrooms = [];
    public $config_bathrooms = [];
    public $config_kitchen= [];
    public $config_living_area= [];
    public $config_dining_area= [];
    public $config_maid_rooms= [];
    public $config_laundry_area= [];
    public $config_store= [];
    public $config_service_block= [];
    public $config_garage= [];
    public $config_balcony= [];
    public $config_flooring= [];
    public $config_ceiling= [];
    public $config_view= [];
    public $config_general_elevation= [];
    public $config_unit_tag= [];
    public $config_electricity_board = [];
    public $config_family_room=[];
    public $config_powder_room=[];
    public $config_study_room=[];
    public $no_of_room_type;
    public $image_checked;
    public $custom_fields=[];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_quotation_configraions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['over_all_upgrade'], 'number'],
            [['quotation_id'], 'required'],
            [['quotation_id','property_index','config_bedrooms', 'config_bathrooms','config_kitchen','config_living_area','config_dining_area',
                'config_maid_rooms','config_laundry_area','config_store','config_service_block','config_garage','config_balcony','config_flooring','config_ceiling','config_view','config_general_elevation','config_unit_tag','config_electricity_board','config_family_room','config_powder_room','config_study_room','image_checked','custom_fields'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'over_all_upgrade' => 'Over All Upgrade',
            'quotation_id' => 'Quotation ID',
        ];
    }
    public function afterSave($insert, $changedAttributes)
    {


        if (isset($this->config_bedrooms) && $this->config_bedrooms <> null) {

            $this->addCongurationsData('config_bedrooms', $this->config_bedrooms);
        }

        if (isset($this->config_bathrooms) && $this->config_bathrooms <> null) {

            $this->addCongurationsData('config_bathrooms', $this->config_bathrooms);
        }
        if (isset($this->config_kitchen) && $this->config_kitchen <> null) {

            $this->addCongurationsData('config_kitchen', $this->config_kitchen);
        }
        if (isset($this->config_living_area) && $this->config_living_area <> null) {

            $this->addCongurationsData('config_living_area', $this->config_living_area);
        }
        if (isset($this->config_dining_area) && $this->config_dining_area <> null) {

            $this->addCongurationsData('config_dining_area', $this->config_dining_area);
        }
        if (isset($this->config_maid_rooms) && $this->config_maid_rooms <> null) {

            $this->addCongurationsData('config_maid_rooms', $this->config_maid_rooms);
        }
        if (isset($this->config_laundry_area) && $this->config_laundry_area <> null) {

            $this->addCongurationsData('config_laundry_area', $this->config_laundry_area);
        }
        if (isset($this->config_store) && $this->config_store <> null) {

            $this->addCongurationsData('config_store', $this->config_store);
        }
        if (isset($this->config_service_block) && $this->config_service_block <> null) {

            $this->addCongurationsData('config_service_block', $this->config_service_block);
        }
        if (isset($this->config_garage) && $this->config_garage <> null) {

            $this->addCongurationsData('config_garage', $this->config_garage);
        }
        if (isset($this->config_balcony) && $this->config_balcony <> null) {

            $this->addCongurationsData('config_balcony', $this->config_balcony);
        }
        if (isset($this->config_family_room) && $this->config_family_room <> null) {

            $this->addCongurationsData('config_family_room', $this->config_family_room);
        }
        if (isset($this->config_powder_room) && $this->config_powder_room <> null) {

            $this->addCongurationsData('config_powder_room', $this->config_powder_room);
        }
        if (isset($this->config_study_room) && $this->config_study_room <> null) {

            $this->addCongurationsData('config_study_room', $this->config_study_room);
        }
        if (isset($this->config_view) && $this->config_view <> null) {

            $this->addCongurationsData('config_view', $this->config_view);
        }
        if (isset($this->config_general_elevation) && $this->config_general_elevation <> null) {

            $this->addCongurationsData('config_general_elevation', $this->config_general_elevation);
        }
        if (isset($this->config_unit_tag) && $this->config_unit_tag <> null) {

            $this->addCongurationsData('config_unit_tag', $this->config_unit_tag);
        }
        if (isset($this->config_electricity_board) && $this->config_electricity_board <> null) {

            $this->addCongurationsData('config_electricity_board', $this->config_electricity_board);
        }


        $upgrades_value = Yii::$app->appHelperFunctions->getUpgradeAttributes($this);
        if($upgrades_value <> null){
            \Yii::$app->db->createCommand("UPDATE crm_quotation_configraions SET over_all_upgrade=".$upgrades_value." WHERE id=".$this->id)->execute();
        }
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }

    public function addCongurationsData($type, $require_data)
    {

       //
        // CrmQuotationConfigraionsFiles::deleteAll(['quotation_id' => $this->quotation_id, 'type' => $type]);
        // Save all payments terms
        foreach ($require_data as $config_data) {
            $bed_detail = new CrmQuotationConfigraionsFiles();
            $bed_detail->type = $type;
            $bed_detail->floor = $config_data['floor'];
            $bed_detail->flooring = $config_data['flooring'];
            $bed_detail->ceilings = $config_data['ceilings'];
            $bed_detail->speciality = $config_data['speciality'];
            $bed_detail->upgrade = 3;
            $bed_detail->attachment = $config_data['attachment'];
            $bed_detail->index_id = $config_data['index_id'];
            $bed_detail->quotation_id = $config_data['quotation_id'];
            $bed_detail->property_index = $config_data['property_index'];
            if(!$bed_detail->save()){
                echo "<pre>";
                print_r($bed_detail->errors);
                die;
            }
        }
    }

}
