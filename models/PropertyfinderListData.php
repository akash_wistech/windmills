<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "propertyfinder_list_data".
*
* @property int $id
* @property string|null $listings_reference
* @property string|null $source
* @property string|null $listing_website_link
* @property string|null $listing_date
* @property string|null $building_info
* @property string|null $city_id
* @property string|null $property_type
* @property string|null $community
* @property string|null $sub_community
* @property string|null $property_category
* @property string|null $no_of_bedrooms
* @property string|null $built_up_area
* @property string|null $land_size
* @property string|null $listings_price
* @property string|null $listings_rent
* @property string|null $final_price
* @property string|null $status
* @property string|null $purpose
* @property int|null $move_to_listing
*/
class PropertyfinderListData extends \yii\db\ActiveRecord
{
    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'propertyfinder_list_data';
    }
    
    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['move_to_listing'], 'integer'],
            [['created_at'], 'safe'],
            [['listings_reference', 'source', 'listing_website_link', 'listing_date', 'building_info', 'city_id', 'property_type', 'community', 'sub_community', 'property_category', 'no_of_bedrooms', 'built_up_area', 'land_size', 'listings_price', 'listings_rent', 'final_price', 'status', 'purpose'], 'safe'],
        ];
    }
    
    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'listings_reference' => 'Listings Reference',
            'source' => 'Source',
            'listing_website_link' => 'Listing Website Link',
            'listing_date' => 'Listing Date',
            'building_info' => 'Building Info',
            'city_id' => 'City ID',
            'property_type' => 'Property Type',
            'community' => 'Community',
            'sub_community' => 'Sub Community',
            'property_category' => 'Property Category',
            'no_of_bedrooms' => 'No Of Bedrooms',
            'built_up_area' => 'Built Up Area',
            'land_size' => 'Land Size',
            'listings_price' => 'Listings Price',
            'listings_rent' => 'Listings Rent',
            'final_price' => 'Final Price',
            'status' => 'Status',
            'purpose' => 'Purpose',
            'move_to_listing' => 'Move To Listing',
        ];
    }
}
