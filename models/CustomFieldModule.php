<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%custom_field_module}}".
*
* @property integer $custom_field_id
* @property string $module_type
*/
class CustomFieldModule extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%custom_field_module}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['custom_field_id', 'module_type'], 'required'],
      [['custom_field_id'], 'integer'],
      [['module_type'], 'string'],
    ];
  }

  public static function primaryKey()
  {
  	return ['custom_field_id','module_type'];
  }
}
