<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CrmQuotations;
use app\models\Valuation;
use app\models\FeeDifference;

use yii;

/**
 * FeeDifferenceSearch represents the model behind the search form of `app\models\FeeDifference`.
 */
class FeeDifferenceSearch extends FeeDifference
{
    public $quotation_ref, $valuation_ref;
    public $date_range, $time_period,$time_period_compare, $custom_date_btw;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quotation_id','recommended_fee','approved_fee','fee_differ_amount','fee_differ_percent','quotation_ref','valuation_ref'], 'safe'],
            [['id','type', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at','deleted_at'], 'safe'],
            [['time_period','time_period_compare','custom_date_btw'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $type = null)
    {
        $request = Yii::$app->request;
        $sort = $request->get('sort');
        

        $query = FeeDifference::find();

        // add conditions that should always apply here

        // $dataProvider = new ActiveDataProvider([
        //     'query' => $query,
        // ]);
        if (!$sort) {
            $query->orderBy([ 'updated_at' => SORT_DESC ]);
        }

        if ($type !== null) {
            $query->andWhere(['type' => $type]);
        }

        

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'attributes' => [
                    'quotation_id',
                    'valuation_id',
                    'recommended_fee',
                    'approved_fee',
                    'fee_differ_amount',
                    'fee_differ_percent',
                    'created_at',
                    'updated_at',
                ],
            ],
        ]);

        $this->load($params);

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }


        }else{
            $from_date = '2020-04-28';
            $to_date = date('Y-m-d');
        }

        $query->andFilterWhere(['between', 'created_at', $from_date." 00:00:00", $to_date." 23:59:59"]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'quotation_id' => $this->quotation_id,
            'recommended_fee' => $this->recommended_fee,
            'approved_fee' => $this->approved_fee,
            'fee_differ_amount' => $this->fee_differ_amount,
            'fee_differ_percent' => $this->fee_differ_percent,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        // $query->andFilterWhere(['like', 'quotation_id', $this->quotation_id]);
        // $query->andFilterWhere(['like', 'recommended_fee', $this->recommended_fee]);
        // $query->andFilterWhere(['like', 'approved_fee', $this->approved_fee]);
        // $query->andFilterWhere(['like', 'fee_differ_amount', $this->fee_differ_amount]);
        // $query->andFilterWhere(['like', 'fee_differ_percent', $this->fee_differ_percent]);

        if($this->quotation_ref <> null){
            $quotationsIds = CrmQuotations::find()
                ->select('id')
                ->where(['like', 'reference_number', '%' . $this->quotation_ref . '%', false])
                ->column();
            $query->andFilterWhere(['in', 'quotation_id', $quotationsIds]);
        }
    
        // $query->joinWith(['quotation' => function ($q) {
        //     $q->from(['crm_quotations' => CrmQuotations::tableName()]);
        // }]);
        
        return $dataProvider;
    }
}
