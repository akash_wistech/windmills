<?php

namespace app\models;

use Yii;
use app\models\IpAddresses;
/**
 * This is the model class for table "ip_list".
 *
 * @property int $id
 * @property string $list_name
 */
class IpList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
   public $ips=[];
    public static function tableName()
    {
        return 'ip_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['list_name'], 'required'],
            [['list_name'], 'string', 'max' => 200],
            ['ips', 'each', 'rule' => ['string']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'list_name' => 'List Name',
            'ips' => 'IP Address',
          
        ];
    }

    public function afterSave($insert,$changedAttributes) {

            // echo "<pre>";
            // print_r($this->ips);
            // echo "</pre>";
            // die();

        if ($this->id!=null) {
            \Yii::$app->db->createCommand()
            ->delete('ip_addresses', ['list_id' => $this->id])
            ->execute();
            }

        foreach ($this->ips as $key => $ip) {
            $ipaddress= new IpAddresses;
            $ipaddress->ip=$ip;
            $ipaddress->list_id=$this->id;
            $ipaddress->save();
            }
    return parent::afterSave($insert,$changedAttributes);
}
}
