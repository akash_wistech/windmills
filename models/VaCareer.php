<?php

namespace app\models;

use Yii;

class VaCareer extends \yii\db\ActiveRecord
{
    public $status_verified;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'va_career';
    }
}
