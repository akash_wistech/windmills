<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Career;

/**
* StaffSearch represents the model behind the search form of `app\models\User`.
*/
class CareerSearch extends Career
{
  public $permission_group,$name,$mobile,$job_title,$department,$pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','permission_group','status','created_by','updated_by','pageSize'],'integer'],
      [['name','email','mobile','job_title','department'],'string'],
      [['client_approver'],'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {

    $this->load($params);
    $query = Career::find()
    ->select([
      career::tableName().'.id',
      'image',
      'name'=>'CONCAT(firstname," ",lastname)',
      'email',
      CareerProfileInfo::tableName().'.phone1',
      'department' => CareerProfileInfo::tableName().'.department_id',
      'job_title'=> CareerProfileInfo::tableName().'.job_title_id',
      career::tableName().'.status',
    ])
    ->leftJoin(CareerProfileInfo::tableName(),CareerProfileInfo::tableName().'.user_id='.career::tableName().'.id')
    ->asArray();



    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
      'sort' =>false,
    ]);

    // grid filtering conditions
    $query->andFilterWhere([
      Career::tableName().'.id' => $this->id,
      //'user_type' => 10,
        Career::tableName().'.status' => $this->status,
        Career::tableName().'.trashed' => 0,
    ]);

    $query->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
    ->andFilterWhere(['like','email',$this->email])
    ->andFilterWhere(['like',CareerProfileInfo::tableName().'.mobile',$this->mobile])
    ->andFilterWhere(['like',CareerProfileInfo::tableName().'.department_id',$this->department])
    ->andFilterWhere(['like',CareerProfileInfo::tableName().'.job_title_id',$this->job_title]);

    return $dataProvider;
  }
    public function search_tamleek($params)
    {
        $this->load($params);
        $query = career::find()
            ->select([
                career::tableName().'.id',
                'permission_group_id',
                'permission_group'=>AdminGroup::tableName().'.title',
                'job_title'=>JobTitle::tableName().'.title',
                'department'=>Department::tableName().'.title',
                'image',
                'name'=>'CONCAT(firstname," ",lastname)',
                'email',
                'client_approver',
                CareerProfileInfo::tableName().'.mobile',
                career::tableName().'.status',
            ])
            ->leftJoin(CareerProfileInfo::tableName(),CareerProfileInfo::tableName().'.user_id='.career::tableName().'.id')
            ->leftJoin(AdminGroup::tableName(),AdminGroup::tableName().'.id='.career::tableName().'.permission_group_id')
            ->leftJoin(JobTitle::tableName(),JobTitle::tableName().'.id='.CareerProfileInfo::tableName().'.job_title_id')
            ->leftJoin(Department::tableName(),Department::tableName().'.id='.CareerProfileInfo::tableName().'.department_id')
            ->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
            ],
            'sort' =>false,
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            career::tableName().'.id' => $this->id,
            'user_type' => 0,
            'permission_group_id' =>15,
            'client_approver' =>$this->client_approver,
            'company_id' =>$this->company_id,
            career::tableName().'.status' => $this->status,
            career::tableName().'.trashed' => 0,
        ]);

        $query->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
            ->andFilterWhere(['like','email',$this->email])
            ->andFilterWhere(['like',JobTitle::tableName().'.title',$this->job_title])
            ->andFilterWhere(['like',Department::tableName().'.title',$this->department])
            ->andFilterWhere(['like',CareerProfileInfo::tableName().'.mobile',$this->mobile]);

        return $dataProvider;
    }

    public function search_injaz($params)
    {
        $this->load($params);
        $query = career::find()
            ->select([
                career::tableName().'.id',
                'permission_group_id',
                'permission_group'=>AdminGroup::tableName().'.title',
                'job_title'=>JobTitle::tableName().'.title',
                'department'=>Department::tableName().'.title',
                'image',
                'name'=>'CONCAT(firstname," ",lastname)',
                'email',
                'client_approver',
                CareerProfileInfo::tableName().'.mobile',
                career::tableName().'.status',
            ])
            ->leftJoin(CareerProfileInfo::tableName(),CareerProfileInfo::tableName().'.user_id='.career::tableName().'.id')
            ->leftJoin(AdminGroup::tableName(),AdminGroup::tableName().'.id='.career::tableName().'.permission_group_id')
            ->leftJoin(JobTitle::tableName(),JobTitle::tableName().'.id='.CareerProfileInfo::tableName().'.job_title_id')
            ->leftJoin(Department::tableName(),Department::tableName().'.id='.CareerProfileInfo::tableName().'.department_id')
            ->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
            ],
            'sort' =>false,
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            career::tableName().'.id' => $this->id,
            'user_type' => 0,
            'permission_group_id' =>15,
            'client_approver' =>$this->client_approver,
            'company_id' =>$this->company_id,
            career::tableName().'.status' => $this->status,
            career::tableName().'.trashed' => 0,
        ]);

        $query->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
            ->andFilterWhere(['like','email',$this->email])
            ->andFilterWhere(['like',JobTitle::tableName().'.title',$this->job_title])
            ->andFilterWhere(['like',Department::tableName().'.title',$this->department])
            ->andFilterWhere(['like',CareerProfileInfo::tableName().'.mobile',$this->mobile]);

        return $dataProvider;
    }


    public function search_ontime($params)
    {
        $this->load($params);
        $query = career::find()
            ->select([
                career::tableName().'.id',
                'permission_group_id',
                'permission_group'=>AdminGroup::tableName().'.title',
                'job_title'=>JobTitle::tableName().'.title',
                'department'=>Department::tableName().'.title',
                'image',
                'name'=>'CONCAT(firstname," ",lastname)',
                'email',
                'client_approver',
                CareerProfileInfo::tableName().'.mobile',
                career::tableName().'.status',
            ])
            ->leftJoin(CareerProfileInfo::tableName(),CareerProfileInfo::tableName().'.user_id='.career::tableName().'.id')
            ->leftJoin(AdminGroup::tableName(),AdminGroup::tableName().'.id='.career::tableName().'.permission_group_id')
            ->leftJoin(JobTitle::tableName(),JobTitle::tableName().'.id='.CareerProfileInfo::tableName().'.job_title_id')
            ->leftJoin(Department::tableName(),Department::tableName().'.id='.CareerProfileInfo::tableName().'.department_id')
            ->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
            ],
            'sort' =>false,
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            career::tableName().'.id' => $this->id,
            'user_type' => 0,
            'permission_group_id' =>15,
            'client_approver' =>$this->client_approver,
            'company_id' =>$this->company_id,
            career::tableName().'.status' => $this->status,
            career::tableName().'.trashed' => 0,
        ]);

        $query->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
            ->andFilterWhere(['like','email',$this->email])
            ->andFilterWhere(['like',JobTitle::tableName().'.title',$this->job_title])
            ->andFilterWhere(['like',Department::tableName().'.title',$this->department])
            ->andFilterWhere(['like',CareerProfileInfo::tableName().'.mobile',$this->mobile]);

        return $dataProvider;
    }

    public function search_tabure($params)
    {
        $this->load($params);
        $query = career::find()
            ->select([
                career::tableName().'.id',
                'permission_group_id',
                'permission_group'=>AdminGroup::tableName().'.title',
                'job_title'=>JobTitle::tableName().'.title',
                'department'=>Department::tableName().'.title',
                'image',
                'name'=>'CONCAT(firstname," ",lastname)',
                'email',
                'client_approver',
                CareerProfileInfo::tableName().'.mobile',
                career::tableName().'.status',
            ])
            ->leftJoin(CareerProfileInfo::tableName(),CareerProfileInfo::tableName().'.user_id='.career::tableName().'.id')
            ->leftJoin(AdminGroup::tableName(),AdminGroup::tableName().'.id='.career::tableName().'.permission_group_id')
            ->leftJoin(JobTitle::tableName(),JobTitle::tableName().'.id='.CareerProfileInfo::tableName().'.job_title_id')
            ->leftJoin(Department::tableName(),Department::tableName().'.id='.CareerProfileInfo::tableName().'.department_id')
            ->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
            ],
            'sort' =>false,
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            career::tableName().'.id' => $this->id,
            'user_type' => 0,
            'permission_group_id' =>15,
            'client_approver' =>$this->client_approver,
            'company_id' =>$this->company_id,
            career::tableName().'.status' => $this->status,
            career::tableName().'.trashed' => 0,
        ]);

        $query->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
            ->andFilterWhere(['like','email',$this->email])
            ->andFilterWhere(['like',JobTitle::tableName().'.title',$this->job_title])
            ->andFilterWhere(['like',Department::tableName().'.title',$this->department])
            ->andFilterWhere(['like',CareerProfileInfo::tableName().'.mobile',$this->mobile]);

        return $dataProvider;
    }

    public function search_fasttrack($params)
    {
        $this->load($params);
        $query = career::find()
            ->select([
                career::tableName().'.id',
                'permission_group_id',
                'permission_group'=>AdminGroup::tableName().'.title',
                'job_title'=>JobTitle::tableName().'.title',
                'department'=>Department::tableName().'.title',
                'image',
                'name'=>'CONCAT(firstname," ",lastname)',
                'email',
                'client_approver',
                CareerProfileInfo::tableName().'.mobile',
                career::tableName().'.status',
            ])
            ->leftJoin(CareerProfileInfo::tableName(),CareerProfileInfo::tableName().'.user_id='.career::tableName().'.id')
            ->leftJoin(AdminGroup::tableName(),AdminGroup::tableName().'.id='.career::tableName().'.permission_group_id')
            ->leftJoin(JobTitle::tableName(),JobTitle::tableName().'.id='.CareerProfileInfo::tableName().'.job_title_id')
            ->leftJoin(Department::tableName(),Department::tableName().'.id='.CareerProfileInfo::tableName().'.department_id')
            ->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
            ],
            'sort' =>false,
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            career::tableName().'.id' => $this->id,
            'user_type' => 0,
            'permission_group_id' =>15,
            'client_approver' =>$this->client_approver,
            'company_id' =>$this->company_id,
            career::tableName().'.status' => $this->status,
            career::tableName().'.trashed' => 0,
        ]);

        $query->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
            ->andFilterWhere(['like','email',$this->email])
            ->andFilterWhere(['like',JobTitle::tableName().'.title',$this->job_title])
            ->andFilterWhere(['like',Department::tableName().'.title',$this->department])
            ->andFilterWhere(['like',CareerProfileInfo::tableName().'.mobile',$this->mobile]);

        return $dataProvider;
    }


    public function search_approvedpr($params)
    {
        $this->load($params);
        $query = career::find()
            ->select([
                career::tableName().'.id',
                'permission_group_id',
                'permission_group'=>AdminGroup::tableName().'.title',
                'job_title'=>JobTitle::tableName().'.title',
                'department'=>Department::tableName().'.title',
                'image',
                'name'=>'CONCAT(firstname," ",lastname)',
                'email',
                'client_approver',
                CareerProfileInfo::tableName().'.mobile',
                career::tableName().'.status',
            ])
            ->leftJoin(CareerProfileInfo::tableName(),CareerProfileInfo::tableName().'.user_id='.career::tableName().'.id')
            ->leftJoin(AdminGroup::tableName(),AdminGroup::tableName().'.id='.career::tableName().'.permission_group_id')
            ->leftJoin(JobTitle::tableName(),JobTitle::tableName().'.id='.CareerProfileInfo::tableName().'.job_title_id')
            ->leftJoin(Department::tableName(),Department::tableName().'.id='.CareerProfileInfo::tableName().'.department_id')
            ->asArray();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
            ],
            'sort' =>false,
        ]);

        // grid filtering conditions
        $query->andFilterWhere([
            career::tableName().'.id' => $this->id,
            'user_type' => 0,
            'permission_group_id' =>15,
            'client_approver' =>$this->client_approver,
            'company_id' =>$this->company_id,
            career::tableName().'.status' => $this->status,
            career::tableName().'.trashed' => 0,
        ]);

        $query->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
            ->andFilterWhere(['like','email',$this->email])
            ->andFilterWhere(['like',JobTitle::tableName().'.title',$this->job_title])
            ->andFilterWhere(['like',Department::tableName().'.title',$this->department])
            ->andFilterWhere(['like',CareerProfileInfo::tableName().'.mobile',$this->mobile]);

        return $dataProvider;
    }
}
