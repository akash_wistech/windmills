<?php

namespace app\models;

use Yii;

/**
* ALActionForm is the model behind the action log action form.
*/
class ALActionForm extends ALForms
{
  public $id,$subject,$due_date,$assign_to,$priority,$reminder,$reminder_to,$remind_time,$calendar_id;
  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['module_type','module_id','subject','comments'],'required'],
      [['module_type','comments','due_date','reminder_to'],'string'],
      [['id','module_id','priority','visibility','reminder','remind_time','calendar_id'],'integer'],
      [['comments'],'trim'],
      [['assign_to'],'each','rule'=>['integer']],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'comments' => 'Description',
      'due_date' => 'Due Date',
      'assign_to' => 'Assigned To',
      'visibility' => 'Visibility',
      'calendar_id' => 'Add to Calendar',
    ];
  }

  /**
  * Save Action Log Action Info
  */
  public function save()
  {
    if ($this->validate()) {
      $checkAlready=$this->checkAlready;
      if(!$checkAlready->exists()){
        if($this->id>0){
          $comment = ActionLog::findOne($this->id);
          $logMsg=''.Yii::$app->user->identity->name.' update a '.$this->module_type.' type '.$this->rec_type.' for '.$this->module_id.' with id '.$this->id;
        }else{
          $comment = new ActionLog;
          $comment->module_type=$this->module_type;
          $comment->module_id=$this->module_id;
          $comment->rec_type=$this->rec_type;
          $logMsg=''.Yii::$app->user->identity->name.' saved a '.$this->module_type.' type '.$this->rec_type.' for '.$this->module_id;
        }
        $comment->subject=$this->subject;
        $comment->due_date=$this->due_date;
        $comment->comments=$this->comments;
        $comment->assign_to=$this->assign_to;
        $comment->priority=$this->priority;
        $comment->visibility=$this->visibility;
        $comment->calendar_id=$this->calendar_id;
        if($comment->save()){
          if($this->reminder==1){
            $reminderInfo=ActionLogReminder::find()->where(['action_log_id'=>$comment->id])->one();
            if($reminderInfo==null){
              $reminderInfo=new ActionLogReminder;
              $reminderInfo->action_log_id=$comment->id;
            }
            $reminderInfo->notify_to=$this->reminder_to;
            $reminderInfo->notify_time=$this->remind_time;
            $reminderInfo->save();
          }else{
            $reminderInfo=ActionLogReminder::find()->where(['action_log_id'=>$comment->id])->one();
            if($reminderInfo!=null){
              $reminderInfo->delete();
            }
          }
        }else{
          if($comment->hasErrors()){
            foreach($comment->getErrors() as $error){
              if(count($error)>0){
                foreach($error as $key=>$val){
                  $this->addError('',$val);
                  return false;
                }
              }
            }
          }
        }
        ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, $logMsg);
      }else{
        return true;
      }
      return true;
    }
    return false;
  }
}
