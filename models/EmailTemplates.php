<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "email_templates".
 *
 * @property int $id
 * @property string $key
 * @property string $language
 * @property string $subject
 * @property string $body
 * @property string $hint
 */
class EmailTemplates extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'email_templates';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key', 'subject'], 'required'],
            [['body'], 'string'],
            [['key', 'language', 'subject'], 'string', 'max' => 255],
            [['hint'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Key',
            'subject' => 'Subject',
            'body' => 'Body',

        ];
    }
}
