<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%user_profile_info}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $primary_contact
 * @property int $job_title_id
 * @property int $department_id
 * @property string $mobile
 * @property string $phone1
 * @property string $phone2
 * @property int $country_id
 * @property int $zone_id
 * @property string $city
 * @property string $postal_code
 * @property string $address
 * @property string $background_info
 * @property int $lead_type
 * @property int $lead_source
 * @property string $lead_date
 * @property int $lead_score
 * @property string $expected_close_date
 * @property string $close_date
 * @property int $confidence
 * @property int $deal_status
 */
class CareerProfileInfo extends ActiveRecord
{

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return '{{%career_profile_info}}';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['user_id'], 'required'],
      [[
        'user_id','primary_contact','department_id','country_id','zone_id', 'nationality',
        'lead_type','lead_source','lead_score','confidence','deal_status','job_title_id'
      ], 'integer'],
      [['city','postal_code','address','background_info','lead_date','expected_close_date','close_date','valuer_qualifications','valuer_status','valuer_experience_expertise','signature_img_name','mobile','phone1','phone2'], 'string'],
      [['created_at','updated_at'], 'safe'],
      [['mobile','phone1','phone2','city','postal_code'], 'filter', 'filter' => 'trim'],

      [['gender' , 'middlename' , 'passport_number' , 'birth_date' , 'marital_status' , 'marriage_date',
      'no_of_children' , 'mother_name' , 'mother_profession' , 'father_name' , 'father_profession',
      'apartment' , 'street' , 'community' , 'ijari' , 'origin_apartment' , 'origin_street',
      'origin_country' , 'origin_city' , 'emergency_home' , 'emergency_uae' , 'whatsapp_groups',
      'personal_email' , 'group_emails' , 'degree_name' , 'institute_name' , 'passing_year' , 
      'percentage_obtained' , 'attested' , 'company' , 'position' , 'department',
      'start_date' , 'end_date' , 'gross_salary' , 'leave_reason' , 'total_experience' , 'relevant_experience',
      'ncc', 'ncc_breakage_cost' , 'notice_period' , 'early_breakage_cost' , 'visa_type' , 'visa_emirate' , 'joining_date',
      'visa_start_date', 'visa_end_date' , 'driving_license' , 'hobby' , 'language' , 'ref_fname',
      'ref_lname' , 'ref_nationality' , 'ref_company' , 'ref_dept' , 'ref_position' , 'ref_landline' , 
      'ref_mobile' , 'basic_salary' , 'house_allowance', 'transport_allowance' , 'last_gross_salary', 'other_allowance',
      'tax_rate' , 'net_salary' , 'windmills_position' , 'reports_to' , 'picture', 'cv', 'cover_letter','job_description','kpi'], 'safe'],
      
      [['degree_attached' , 'ref_letter' , 'emirates_id' , 'residence_visa' , 'work_permit' , 'last_contract',
      'offer_letter','job_description','kpi','tech_skills'], 'file'],



    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_id' => Yii::t('app', 'User'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUser()
  {
    return $this->hasOne(Career::className(), ['id' => 'user_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getJobTitle()
  {
    return $this->hasOne(JobTitle::className(), ['id' => 'job_title_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDepartment()
  {
    return $this->hasOne(Department::className(), ['id' => 'department_id']);
  }
}
