<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "merge_project_childs".
 *
 * @property int $id
 * @property int|null $building_id
 */
class MergeProjectChilds extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'merge_project_childs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['building_id','merge_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'building_id' => 'Building ID',
        ];
    }
}
