<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CrmQuotations;
use app\models\Valuation;
use app\models\CrmQuotationFollowupEmail;

use yii;


/**
 * CrmQuotationFollowupEmailSearch represents the model behind the search form of `app\models\CrmQuotationFollowupEmail`.
 */
class CrmQuotationFollowupEmailSearch extends CrmQuotationFollowupEmail
{
    public $quotation_ref, $no_of_reminder;
    public $date_range, $time_period,$time_period_compare, $custom_date_btw;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quotation_id','client_id','date','action'], 'safe'],
            [['id', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at','deleted_at','no_of_reminder','quotation_ref'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $request = Yii::$app->request;
        $sort = $request->get('sort');
        

        $query = CrmQuotationFollowupEmail::find()
        ->select(['quotation_id', 'client_id', 'date' => 'MAX(date)', 'no_of_reminder' => 'COUNT(id)'])
        ->groupBy('client_id')
        ->orderBy([ 'date' => SORT_DESC ]);

        // add conditions that should always apply here

        // if (!$sort) {
        //     $query->orderBy([ 'created_at' => SORT_DESC ]);
        // }


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'attributes' => [
                    'quotation_ref',
                    'no_of_reminder',
                    'client_id',
                    'date',
                    'created_at',
                ],
            ],
        ]);

        $this->load($params);

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }


        }else{
            $from_date = '2020-04-28';
            $to_date = date('Y-m-d');
        }

        $query->andFilterWhere(['between', 'created_at', $from_date." 00:00:00", $to_date." 23:59:59"]);
        

        if (!$this->validate()) {
            return $dataProvider;
        }

        

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'date' => $this->date,
            'created_at' => $this->created_at,
        ]);


        if($this->quotation_ref <> null){
            $quotationsIds = CrmQuotations::find()
                ->select('id')
                ->where(['like', 'reference_number', '%' . $this->quotation_ref . '%', false])
                ->column();
            $query->andFilterWhere(['in', 'quotation_id', $quotationsIds]);
        }

        if($this->no_of_reminder <> null){
            $query->andHaving(['no_of_reminder' => $this->no_of_reminder]);
        }
    
        // $query->joinWith(['quotation' => function ($q) {
        //     $q->from(['crm_quotations' => CrmQuotations::tableName()]);
        // }]);
        
        return $dataProvider;
    }
}
