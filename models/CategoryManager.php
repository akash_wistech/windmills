<?php

namespace app\models;

use Yii;

// dd($_REQUEST);

class CategoryManager extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category_manager';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_name' , 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'category_name' => 'category_name',
            'status' => 'status',
        ];
    }
}
