<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* LoginForm is the model behind the login form.
*
* @property User|null $user This property is read-only.
*
*/
class LoginForm extends Model
{
  public $username;
  public $password;
  public $rememberMe = true;

  private $_user = false;


  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      // username and password are both required
      [['username', 'password'], 'required'],
      [['username'], 'validateExpiration'],
      // rememberMe must be a boolean value
      ['rememberMe', 'boolean'],
      // password is validated by validatePassword()
      ['password', 'validatePassword'],
      ['username', 'filter', 'filter' => 'trim'],
    ];
  }

  /**
  * Validates the status.
  *
  * @param string $attribute the attribute currently being validated
  * @param array $params the additional name-value pairs given in the rule
  */
  public function validateExpiration($attribute, $params)
  {
    if (!$this->hasErrors()) {
      $user = $this->getUser();

      if ($user!=null) {
        if($user->status==0){
          //Pending
          $this->addError($attribute, Yii::t('app','Your account is not activated yet, Please contact administrator'));
        }elseif($user->status==2){
          //Blocked
          $this->addError($attribute, Yii::t('app','Your account has been disabled, Please contact administrator'));
        }
      }else{
        $this->addError($attribute, 'Incorrect username or password.');
      }
    }
  }

  /**
  * Validates the password.
  * This method serves as the inline validation for password.
  *
  * @param string $attribute the attribute currently being validated
  * @param array $params the additional name-value pairs given in the rule
  */
  public function validatePassword($attribute, $params)
  {
    if (!$this->hasErrors()) {
      $user = $this->getUser();

      if (!$user || !$user->validatePassword($this->password)) {
        $this->addError($attribute, 'Incorrect username or password.');
      }
    }
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'username' => Yii::t('app', 'Username'),
      'password' => Yii::t('app', 'Password'),
      'rememberMe' => Yii::t('app', 'Remember Me'),
    ];
  }

  /**
  * Logs in a user using the provided username and password.
  * @return bool whether the user is logged in successfully
  */
  public function login()
  {
    $member = $this->getUser();
    if ($this->validate()) {
      if(Yii::$app->user->login($member, $this->rememberMe ? 3600*24*30 : 0)){
        ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, $member->name.' logged in successfully');
        $loginTry=new UserLoginHistory;
        $loginTry->user_id=$member->id;
        $loginTry->ip=ip2long(Yii::$app->getRequest()->getUserIP());
        $loginTry->http_referrer=Yii::$app->request->referrer;
        $loginTry->created_at=date("Y-m-d H:i:s");
        $loginTry->login=1;
        $loginTry->save();
        return true;
      }else{
        $loginTry=new UserLoginHistory;
        $loginTry->user_id=$member->id;
        $loginTry->ip=ip2long(Yii::$app->getRequest()->getUserIP());
        $loginTry->http_referrer=Yii::$app->request->referrer;
        $loginTry->created_at=date("Y-m-d H:i:s");
        $loginTry->login=0;
        $loginTry->save();
        return false;
      }
    }else{
      if($member!=null){
        $loginTry=new UserLoginHistory;
        $loginTry->user_id=$member->id;
        $loginTry->ip=ip2long(Yii::$app->getRequest()->getUserIP());
        $loginTry->http_referrer=Yii::$app->request->referrer;
        $loginTry->created_at=date("Y-m-d H:i:s");
        $loginTry->login=0;
        $loginTry->save();
        return false;
      }
    }
    return false;
  }

  /**
  * Finds user by [[username]]
  *
  * @return User|null
  */
  public function getUser()
  {
    if ($this->_user === false) {
      $this->_user = User::findByUsername($this->username);
    }

    return $this->_user;
  }
}
