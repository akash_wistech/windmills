<?php

namespace app\models;
use app\components\models\ActiveRecordFull;

use Yii;

class BayutLinks extends ActiveRecordFull
{

	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%bayut_links}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['url'],'safe'],
		];
	}


}
