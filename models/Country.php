<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordSlugdFull;

/**
* This is the model class for table "{{%country}}".
*
* @property integer $id
* @property string $slug
* @property string $title
* @property string $iso_code_2
* @property string $iso_code_3
* @property string $phone_code
* @property string $phone_format
* @property string $mobile_format
* @property string $address_format
* @property string $postcode_required
* @property integer $status
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
* @property integer $trashed
* @property string $trashed_at
* @property integer $trashed_by
*/
class Country extends ActiveRecordSlugdFull
{

	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%country}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['title'],'required'],
			[['created_at','updated_at','trashed_at'],'safe'],
			[['postcode_required','status','created_by','updated_by','trashed','trashed_by'],'integer'],
			[['slug','title','iso_code_2','iso_code_3','phone_code','phone_format','mobile_format','address_format'],'string'],
      [['title','iso_code_2','iso_code_3','phone_code','phone_format','mobile_format'],'trim'],
		];
	}

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Title'),
			'iso_code_2' => Yii::t('app', 'ISO Code 2'),
			'iso_code_3' => Yii::t('app', 'ISO Code 3'),
			'phone_code' => Yii::t('app', 'Country Code'),
			'status' => Yii::t('app', 'Status'),
			'created_at' => Yii::t('app', 'Created At'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'updated_by' => Yii::t('app', 'Updated By'),
			'trashed' => Yii::t('app', 'Trashed'),
			'trashed_at' => Yii::t('app', 'Trashed At'),
			'trashed_by' => Yii::t('app', 'Trashed By'),
		];
	}

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecTitle()
  {
    return $this->title;
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecType()
  {
    return 'Country';
  }
}
