<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "windmills_docs".
 *
 * @property int $id
 * @property string|null $file
 * @property string|null $url
 * @property string|null $created_at
 */
class WindmillsDocs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'windmills_docs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at','created_by'], 'safe'],
            [['file', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file' => 'File',
            'url' => 'Url',
            'created_at' => 'Created At',
        ];
    }
}
