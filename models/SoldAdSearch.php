<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SoldAd;

/**
 * SoldAdSearch represents the model behind the search form of `app\models\SoldAd`.
 */
class SoldAdSearch extends SoldAd
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['reidin_ref', 'sales_type', 'sales_sequence', 'transaction_date', 't_date', 'municipality', 'district', 'community', 'projectsub_comm', 'building', 'property_subtype', 'floor', 'bedroom', 'sale_percent', 'unit_area_sqm', 'price', 'mortgage_value', 'average_price_sqm', 'valuation_amount', 'developer'], 'safe'],
            [['plot_area_sqm'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SoldAd::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'transaction_date' => $this->transaction_date,
            'plot_area_sqm' => $this->plot_area_sqm,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'reidin_ref', $this->reidin_ref])
            ->andFilterWhere(['like', 'sales_type', $this->sales_type])
            ->andFilterWhere(['like', 'sales_sequence', $this->sales_sequence])
            ->andFilterWhere(['like', 't_date', $this->t_date])
            ->andFilterWhere(['like', 'municipality', $this->municipality])
            ->andFilterWhere(['like', 'district', $this->district])
            ->andFilterWhere(['like', 'community', $this->community])
            ->andFilterWhere(['like', 'projectsub_comm', $this->projectsub_comm])
            ->andFilterWhere(['like', 'building', $this->building])
            ->andFilterWhere(['like', 'property_subtype', $this->property_subtype])
            ->andFilterWhere(['like', 'floor', $this->floor])
            ->andFilterWhere(['like', 'bedroom', $this->bedroom])
            ->andFilterWhere(['like', 'sale_percent', $this->sale_percent])
            ->andFilterWhere(['like', 'unit_area_sqm', $this->unit_area_sqm])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'mortgage_value', $this->mortgage_value])
            ->andFilterWhere(['like', 'average_price_sqm', $this->average_price_sqm])
            ->andFilterWhere(['like', 'valuation_amount', $this->valuation_amount])
            ->andFilterWhere(['like', 'developer', $this->developer]);

        return $dataProvider;
    }
}
