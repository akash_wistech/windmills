<?php

namespace app\models;
use yii\db\ActiveRecord;

use Yii;

class AciveZone extends ActiveRecord
{

	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%active_zone}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['country_id','zone_id'],'required'],
            [['status_verified','status_verified_at','status_verified_by'], 'safe'],
		];
	}

  public function attributeLabels()
  {
    return [
      'country_id' => Yii::t('app', 'Country'),
      'zone_id' => Yii::t('app', 'City'),
    ];
  }


}
