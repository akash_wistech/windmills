<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "re_cost_assets".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $quantity
 * @property int|null $valuation_id
 * @property int|null $type_id
 */
class ReCostAssets extends \yii\db\ActiveRecord
{

    public $electrical = [];
    public $fire_system = [];
    public $heat_ventilation = [];
    public $mechanical = [];
    public $security_system = [];
    public $water_system = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 're_cost_assets';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['valuation_id', 'type_id'], 'integer'],
            [['name', 'quantity'], 'string', 'max' => 255],
            [['electrical','fire_system','heat_ventilation','mechanical','security_system','water_system'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'quantity' => 'Quantity',
            'valuation_id' => 'Valuation ID',
            'type_id' => 'Type ID',
        ];
    }
}
