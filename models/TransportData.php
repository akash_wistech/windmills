<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;
/**
 * This is the model class for table "transport_data".
 *
 * @property int $id
 * @property int $status
 * @property int|null $created_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 * @property int|null $status_verified
 * @property int|null $status_verified_by
 * @property string|null $status_verified_at
 * @property int|null $valuation_id
 * @property int|null $mode_of_transport
 * @property float|null $start_kilometres
 * @property string|null $start_time
 * @property float|null $end_kilometres
 * @property string|null $end_time
 * @property string|null $car_details
 */
class TransportData extends ActiveRecordFull
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transport_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'created_by', 'updated_by', 'trashed', 'trashed_by', 'status_verified', 'status_verified_by', 'valuation_id', 'mode_of_transport'], 'integer'],
            [['created_at', 'updated_at', 'trashed_at', 'status_verified_at'], 'safe'],
            [['start_kilometres', 'end_kilometres'], 'number'],
            [['start_time', 'end_time', 'car_details'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
            'status_verified' => 'Status Verified',
            'status_verified_by' => 'Status Verified By',
            'status_verified_at' => 'Status Verified At',
            'valuation_id' => 'Valuation ID',
            'mode_of_transport' => 'Mode Of Transport',
            'start_kilometres' => 'Start Kilometres',
            'start_time' => 'Start Time',
            'end_kilometres' => 'End Kilometres',
            'end_time' => 'End Time',
            'car_details' => 'Car Details',
        ];
    }
    public function getRecType()
    {
        return 'Green effect';
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->id;
    }
    public function afterSave($insert, $changedAttributes)
    {

        $valuation = InspectProperty::findOne($this->valuation_id);
        Yii::$app->db->createCommand()->update('inspect_property',
            ['mode_of_transport' =>  $this->mode_of_transport,
                'start_kilometres' =>  $this->start_kilometres,
                'end_kilometres' =>  $this->end_kilometres,
            ],
            ['valuation_id' => $this->valuation_id])->execute();



        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeSave($insert){

        // verify status reset after edit only something changed
        $isChanged = false;
        $ignoredAttributes = ['updated_at','status_verified_at']; 
        foreach ($this->attributes as $attribute => $value) {
            if (in_array($attribute, $ignoredAttributes)) {
                continue;
            }
            if ($this->getOldAttribute($attribute) != $value) {
                $isChanged = true;
                break;
            }
        }
        if ($isChanged) {
            if($this->status == 1 && $this->getOldAttribute('status') == 1){
                $this->status = 2;
            }elseif ($this->status == 2 && $this->getOldAttribute('status') == 1) {
                $this->status = 1;
            }
        }
        
        return parent::beforeSave($insert);
    }
}
