<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordSlugdFull;

/**
* This is the model class for table "{{%zone}}".
*
* @property integer $id
* @property string $slug
* @property integer $country_id
* @property string $title
* @property string $code
* @property integer $status
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
* @property integer $trashed
* @property string $trashed_at
* @property integer $trashed_by
*/
class Zone extends ActiveRecordSlugdFull
{

	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%zone}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['title','country_id'],'required'],
			[['created_at','updated_at','trashed_at'],'safe'],
			[['country_id','status','created_by','updated_by','trashed','trashed_by'],'integer'],
			[['slug','title','code'],'string'],
      [['title','code'],'trim'],
		];
	}

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'country_id' => Yii::t('app', 'Country'),
			'title' => Yii::t('app', 'Title'),
			'code' => Yii::t('app', 'Code'),
			'created_at' => Yii::t('app', 'Created At'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'updated_by' => Yii::t('app', 'Updated By'),
			'trashed' => Yii::t('app', 'Trashed'),
			'trashed_at' => Yii::t('app', 'Trashed At'),
			'trashed_by' => Yii::t('app', 'Trashed By'),
		];
	}

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecTitle()
  {
    return $this->title;
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecType()
  {
    return 'State / Province';
  }
}
