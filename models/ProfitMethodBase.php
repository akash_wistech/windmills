<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "profit_method_base".
 *
 * @property int $id
 * @property float|null $discount_rate
 * @property float|null $growth_rate
 * @property float|null $reversion_year_yield_rate
 * @property float|null $final_estimated_pv_of_subject_property
 * @property float|null $final_estimated_bua
 * @property float|null $final_leasable_area
 * @property float|null $final_leasable_rooms
 * @property int $valuation_id
 * @property int $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 * @property int|null $status_verified
 * @property int|null $status_verified_by
 * @property string|null $status_verified_at
 */
class ProfitMethodBase extends ActiveRecordFull
{
    /**
     * {@inheritdoc}
     */
    public $calculations;
    public static function tableName()
    {
        return 'profit_method_base';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['discount_rate', 'growth_rate', 'reversion_year_yield_rate', 'final_estimated_pv_of_subject_property', 'final_estimated_bua', 'final_leasable_area', 'final_leasable_rooms'], 'number'],
            [['valuation_id'], 'required'],
            [['valuation_id', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by', 'status_verified', 'status_verified_by'], 'integer'],
            [['created_at', 'updated_at', 'trashed_at', 'status_verified_at','calculations'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'discount_rate' => 'Discount Rate',
            'growth_rate' => 'Growth Rate',
            'reversion_year_yield_rate' => 'Reversion Year Yield Rate',
            'final_estimated_pv_of_subject_property' => 'Final Estimated Pv Of Subject Property',
            'final_estimated_bua' => 'Final Estimated Bua',
            'final_leasable_area' => 'Final Leasable Area',
            'final_leasable_rooms' => 'Final Leasable Rooms',
            'valuation_id' => 'Valuation ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
            'status_verified' => 'Status Verified',
            'status_verified_by' => 'Status Verified By',
            'status_verified_at' => 'Status Verified At',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($this->calculations <> null){





            // dd($this->FeeStructure);
            \app\models\ProfitMethodYearly::deleteAll(['valuation_id' => $this->valuation_id ]);

            foreach($this->calculations as $key => $calculation){
                $model = new \app\models\ProfitMethodYearly;
                $model->number_of_rooms= $calculation['number_of_rooms'];
                $model->days_per_year= $calculation['days_per_year'];
                $model->nights_available= $calculation['number_of_rooms'] * $calculation['days_per_year'] ; //
                $model->occupancy= $calculation['occupancy'];
                $model->occupied_nights_per_year= $model->nights_available * $calculation['occupancy']; //
                $model->adr= $calculation['adr'];
                $model->rev_par= $model->nights_available * $calculation['adr']; //
                $model->rooms_revenue= $model->nights_available * $model->rev_par; //
                $model->f_and_b_revenue= $calculation['f_and_b_revenue'];
                $model->net_rental_income= $calculation['net_rental_income'];
                $model->other_operating_departments= $calculation['other_operating_departments'];
                $model->total_revenue= $model->rooms_revenue + $model->f_and_b_revenue + $model->net_rental_income + $model->other_operating_departments; //
                $model->total_revenue_percentage= 100; //

                $model->rooms_revenue_percentage= $model->rooms_revenue/$model->total_revenue; //
                $model->f_and_b_revenue_percentage= $model->f_and_b_revenue/$model->total_revenue;
                $model->net_rental_income_percentage= $model->net_rental_income/$model->total_revenue;
                $model->other_operating_departments_percentage= $model->other_operating_departments/$model->total_revenue;


                $model->rooms= $calculation['rooms'];
                $model->rooms_percentage= $model->rooms/$model->rooms_revenue;
                $model->f_and_b= $calculation['f_and_b'];
                $model->f_and_b_percentage= $model->f_and_b/$model->f_and_b_revenue;
                $model->net_rental_income_exp= $calculation['net_rental_income_exp'];
                $model->net_rental_income_exp_percentage= $model->net_rental_income_exp/$model->net_rental_income;
                $model->other_operating_departments_exp= $calculation['other_operating_departments_exp'];
                $model->other_operating_departments_exp_percentage= $model->other_operating_departments_exp/$model->other_operating_departments;
                $model->total_costs_and_expenses= $model->rooms + $model->f_and_b + $model->net_rental_income_exp + $model->other_operating_departments_exp; //
                $model->total_costs_and_expenses_percentage= $model->total_costs_and_expenses/$model->total_revenue; //
                $model->total_operating_dept_income= $model->total_revenue - $model->total_costs_and_expenses;
                $model->admin_and_general_percentage= $calculation['admin_and_general_percentage'];
                $model->admin_and_general= $model->total_operating_dept_income * $model->admin_and_general_percentage;
                $model->information_and_communication_percentage= $calculation['information_and_communication_percentage'];
                $model->information_and_communication= $model->total_operating_dept_income * $model->information_and_communication_percentage;
                $model->marketing_percentage= $calculation['marketing_percentage'];
                $model->marketing= $model->total_operating_dept_income * $model->marketing;
                $model->pom_percentage= $calculation['pom_percentage'];
                $model->pom= $model->total_operating_dept_income * $model->marketing;;
                $model->energy_percentage= $calculation['energy'];
                $model->energy= $model->total_operating_dept_income * $model->energy_percentage;
                $percentage = $model->admin_and_general_percentage + $model->information_and_communication_percentage +$model->marketing_percentage + $model->pom_percentage + $model->energy_percentage;
                $model->total_undistributed_expenses= $model->total_operating_dept_income * $percentage;
                $model->total_undistributed_expenses_percentage= $model->total_undistributed_expenses/$model->total_revenue;;
                $model->gross_operating_profit= $model->total_operating_dept_income - $model->total_undistributed_expenses;
                $model->gross_operating_profit_percentage= $model->gross_operating_profit/$model->total_revenue;;
                $model->management_fee_base= $model->total_revenue * 0.015;
                $model->management_fee_base_percentage= $model->management_fee_base/$model->total_revenue;
                $model->adjusted_gross_profit= $model->gross_operating_profit - $model->management_fee_base;
                $model->rental_expenses= $calculation['rental_expenses'];
                $model->rental_expenses_percentage= $model->rental_expenses/$model->total_revenue;
                $model->trade_license_fee= $calculation['trade_license_fee'];
                $model->trade_license_percentage= $model->trade_license_fee/$model->total_revenue;
                $model->insurance= $calculation['insurance'];
                $model->insurance_percentage= $model->insurance/$model->total_revenue;
                $model->capacity_charges= $calculation['capacity_charges'];
                $model->capacity_charges_percentage= $model->capacity_charges/$model->total_revenue;
                $model->master_community_charges= $calculation['master_community_charges'];
                $model->master_community_charges_percentage= $model->master_community_charges/$model->total_revenue;
                $model->incentive_fee_as_based_on_performance= $calculation['incentive_fee_as_based_on_performance'];
                $model->incentive_fee_as_based_on_performance_percentage=$model->incentive_fee_as_based_on_performance/$model->total_revenue;
                $model->other_expenses= $calculation['other_expenses'];
                $model->other_expenses_percentage= $model->other_expenses/$model->total_revenue;
                $total_expemse = $model->rental_expenses + $model->trade_license_fee + $model->insurance + $model->capacity_charges + $model->master_community_charges + $model->incentive_fee_as_based_on_performance + $model->other_expenses;
                $model->total_fixed_expenses= $total_expemse;
                $model->total_fixed_expenses_percentage= $total_expemse/$model->total_revenue;
                $model->leasehold_rent= $calculation['leasehold_rent'];
                $model->leasehold_rent_percentage= $model->leasehold_rent/$model->total_revenue;
                $model->reserve_for_replacement= $calculation['reserve_for_replacement'];
                $model->reserve_for_replacement_percentage= $model->reserve_for_replacement/$model->total_revenue;;
                $model->total_a_b= $model->leasehold_rent + $model->reserve_for_replacement;
                $model->total_a_b_percentage= $model->leasehold_rent_percentage + $model->reserve_for_replacement_percentage;
                $model->rent_expeneses_fhp= $calculation['rent_expeneses_fhp'];
                $model->rent_expeneses_fhp_percentage= $model->rent_expeneses_fhp/$model->total_revenue;
                $model->asset_management_fee= $calculation['asset_management_fee'];
                $model->asset_management_fee_percentage=  $model->asset_management_fee/$model->total_revenue;
                $model->ebida_noi= $model->adjusted_gross_profit - $model->total_fixed_expenses - $model->total_a_b -  $model->rent_expeneses_fhp - $model->asset_management_fee;
                $model->ebida_noi_percentage= $model->ebida_noi/$model->total_revenue;
                $model->pv_of_noi=  $model->ebida_noi / (1 + pow((10/100), $model->year_number));;
                //$model->pv_of_noi=  $model->ebida_noi / (1 + pow(($this->discount_rate/100), $model->year_number));;
                $model->pv_of_noi_percentage= $model->pv_of_noi/$model->total_revenue;
                $model->year_number = $key;
                $model->valuation_id  = $this->valuation_id;


                if(!$model->save()){
                    if($model->hasErrors()){
                        foreach($model->getErrors() as $error){
                            if(count($error)>0){
                                foreach($error as $key=>$val){
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                    }
                }


            }

            $profit = ProfitMethodYearly::find()->where(['year_number' => 10])->one();
            $ebida_noi_sum = ProfitMethodYearly::find()->where(['valuation_id' => $this->valuation_id])
                ->orderBy(['id' => SORT_DESC])
                ->sum('ebida_noi');

            $inspectProperty = InspectProperty::find()->where(['valuation_id' => $this->valuation_id])->one();
            $terminal_year = $profit->ebida_noi/ (2.5/100);
            $final_pv = $ebida_noi_sum + $terminal_year;
            $final_pv_bua = $final_pv/$inspectProperty->built_up_area;
            $final_pv_nla = $final_pv/$inspectProperty->nla;
            $final_pv_rooms = $final_pv/391;
            $query2 = Yii::$app->db->createCommand()
                ->update('profit_method_base', ['terminal_year' => $terminal_year,'final_estimated_pv_of_subject_property'=>$final_pv,'final_pv_bua'=>$final_pv_bua, 'final_pv_nla'=> $final_pv_nla,'final_pv_rooms'=>$final_pv_rooms], 'id = ' . $this->id . '')
                ->execute();


        }
        parent::afterSave($insert, $changedAttributes);
    }
}
