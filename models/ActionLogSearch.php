<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ActionLog;

/**
* ActionLogSearch represents the model behind the search form of `app\models\ActionLog`.
*/
class ActionLogSearch extends ActionLog
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['module_id','pageSize'],'integer'],
      [['module_type','rec_type'],'string'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $query = $this->generateQuery($params);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
      'sort' => $this->defaultSorting,
    ]);

    return $dataProvider;
  }

  /**
  * Generates query for the search
  *
  * @param array $params
  *
  * @return ActiveRecord
  */
  public function generateQuery($params)
  {
    $this->load($params);
    $query = ActionLog::find();

    // grid filtering conditions
    $query->andFilterWhere([
      ActionLog::tableName().'.module_type' => $this->module_type,
      ActionLog::tableName().'.module_id' => $this->module_id,
      ActionLog::tableName().'.rec_type' => $this->rec_type,
      ActionLog::tableName().'.trashed' => 0,
    ]);

    return $query;
  }

  /**
  * Default Sorting Options
  *
  * @param array $params
  *
  * @return Array
  */
  public function getDefaultSorting()
  {
    return [
      'defaultOrder' => ['created_at'=>SORT_DESC],
      'attributes' => [
        // 'fullname' => [
        //   'asc' => [Lead::tableName().'.firstname' => SORT_ASC, Lead::tableName().'.lastname' => SORT_ASC],
        //   'desc' => [Lead::tableName().'.firstname' => SORT_DESC, Lead::tableName().'.lastname' => SORT_DESC],
        // ],
        // 'email',
        // 'mobile',
        // 'company_name' => [
        //   'asc' => [Company::tableName().'.title' => SORT_ASC],
        //   'desc' => [Company::tableName().'.title' => SORT_DESC],
        // ],
        // 'sales_stage',
        // 'lead_source',
        'created_at',
      ]
    ];
  }
}
