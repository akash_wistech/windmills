<?php

namespace app\models;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;

use Yii;

/**
 * This is the model class for table "icai_contacts".
 *
 * @property int $id
 * @property string|null $company
 * @property string|null $firstname
 * @property string|null $lastname
 * @property string|null $email
 * @property string|null $phone1
 * @property string|null $phone2
 * @property string|null $jobtitle
 */
class IcapContacts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'icap_contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company', 'company_id', 'phone_code', 'phone', 'land_line_code', 'landline', 'jobtitle'], 'safe'],
            [['status_verified', 'status_verified_at', 'status_verified_by'], 'safe'],

            ['phone', 'match', 'pattern' => '/^\d{7}$/', 'message' => 'Please enter a valid 7-digit phone number.'],
            ['landline', 'match', 'pattern' => '/^\d{7}$/', 'message' => 'Please enter a valid 7-digit phone number.'],
            
            [['email', 'firstname', 'lastname'], 'required'],
            ['email','email'],
            ['email','unique'],
            ['password','safe'],

        ];
    }

    public function getClient()
    {
        return $this->hasOne(\app\models\Company::className(), ['id' => 'company_id']);
    }

    public function getJob()
    {
        return $this->hasOne(\app\models\JobTitle::className(), ['id' => 'jobtitle']);
    }



    public function behaviors()
    {
      return [
        'timestamp' => [
          'class' => TimestampBehavior::className(),
          'attributes' => [
            ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
            ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
          ],
          'value' => function($event) {
            return date("Y-m-d H:i:s");
          },
        ],
        'blameable' => [
          'class' => BlameableBehavior::className(),
          'createdByAttribute' => 'created_by',
          'updatedByAttribute' => 'updated_by',
        ],
      ];
    }



    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company' => 'Company',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'email' => 'Email',
            'phone1' => 'Phone 1',
            'phone2' => 'Phone 2',
            'jobtitle' => 'Jobtitle',
        ];
    }
}
