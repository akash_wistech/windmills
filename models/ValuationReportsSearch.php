<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Valuation;
use Yii;
use yii\db\Query;
use yii\data\ArrayDataProvider;

/**
 * ValuationSearch represents the model behind the search form of `app\models\Valuation`.
 */
class ValuationReportsSearch extends Valuation
{
    public $inspection_date,$inspection_time,$sub_community,$city,$valuer,$search_status_check,$include_signature;
    public $instruction_date_btw,$community,$target_date_btw,$valuation_status,$inspection_date_btw,$client_revenue,
        $pageSize,$total_valuations,$parent_estimated_market_value,$percentage_value,$estimated_market_value,$duplicates,
        $total_inspections,$city_filter,$mv_psf,$rv_psf,$total_valuations_count,$total_client_id,$per_day,$inspection_officer_name,$time_period_compare,$custom_date_btw,$custom_date_btw_compare,$year;
    public $client_type;
    public $valuation_approach;
    public $target,$client_business,$inspection_done_date_time,$total_listings;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'no_of_owners', 'service_officer_name', 'building_info','unit_number', 'status', 'floor_number', 'instruction_person', 'purpose_of_valuation', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['search_status_check','reference_number', 'client_reference', 'client_name_passport', 'instruction_date', 'target_date', 'payment_plan', 'created_at', 'updated_at', 'trashed_at','inspection_date','inspection_time','sub_community','city','valuer','valuation_status','approval_id','ban_date','include_signature',
                'instruction_date_btw','community','valuer','target_date_btw','inspection_date_btw','client_revenue','pageSize','property_id','total_valuations','zone_name','time_period','time_period_compare','parent_estimated_market_value','percentage_value','duplicates','total_inspections','city_filter','mv_psf','mv_psf','total_valuations_count','total_client_id','per_day','inspection_officer_name','custom_date_btw','custom_date_btw_compare','inspection_time','year' ,'tenure','client_type','valuation_approach','inspection_officer'], 'safe'],
            [['land_size'], 'number'],
            [['target','client_business','total_listings'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */

    public function search_all($params)
    {


        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                Valuation::tableName().'.client_id',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                Company::tableName().'.title',
            ])
            ->leftJoin(Company::tableName(),Valuation::tableName().'.client_id='.Company::tableName().'.id')
            ->where(['company.client_type' => 'bank'])
            ->andWhere(['valuation_status' => 5])
            ->groupBy(Valuation::tableName().'.client_id');





        /*    $query = Valuation::find();


            $query->joinWith(['scheduleInspection','client','building']);

            // add conditions that should always apply here

            if (Yii::$app->user->identity->permission_group_id==3) {
                $query->where(['valuation.created_by'=>Yii::$app->user->identity->id]);
            }*/



        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        /* $Date=(explode(" - ",$this->instruction_date_btw));
         $TargetDate=(explode(" - ",$this->target_date_btw));
         $InspectionDate=(explode(" - ",$this->inspection_date_btw));*/


        // grid filtering conditions
        $query->andFilterWhere([
            'valuation.id' => $this->id,
            'valuation.client_id' => $this->client_id,
            /* 'buildings.community' => $this->community,
             'approval_id' => $this->approver,
             'valuation.service_officer_name' => $this->valuer,
             'no_of_owners' => $this->no_of_owners,
             //'service_officer_name' => $this->service_officer_name,
             'instruction_date' => ($this->instruction_date <> null) ? date('Y-m-d',strtotime($this->instruction_date)) : $this->instruction_date,
             'target_date' => ($this->target_date <> null) ? date('Y-m-d',strtotime($this->target_date)) : $this->target_date,
             'building_info' => $this->building_info,
             'unit_number' => $this->unit_number,
             'valuation.status' => $this->status,
             'floor_number' => $this->floor_number,
             'instruction_person' => $this->instruction_person,
             'instruction_person' => $this->instruction_person,
             'land_size' => $this->land_size,
             'purpose_of_valuation' => $this->purpose_of_valuation,
             'created_by' => $this->created_by,
             'created_at' => $this->created_at,
             'updated_at' => $this->updated_at,
             'updated_by' => $this->updated_by,
             'trashed' => $this->trashed,
             'trashed_at' => $this->trashed_at,
             'trashed_by' => $this->trashed_by,
             'valuation.property_id' => $this->property_id,
             'schedule_inspection.inspection_date'=> ($this->inspection_date <> null) ? date('Y-m-d',strtotime($this->inspection_date)) : $this->inspection_date,
             'schedule_inspection.inspection_time'=>$this->inspection_time,
             'buildings.sub_community'=>$this->sub_community,
             'buildings.city'=>$this->city,
             'valuation_status'=>$this->valuation_status,*/

        ]);

        /* $query->andFilterWhere(['like', 'reference_number', $this->reference_number])
             ->andFilterWhere(['like', 'client_reference', $this->client_reference])
             ->andFilterWhere(['like', 'signature_img', $this->signature_img])
             ->andFilterWhere(['like', 'client_name_passport', $this->client_name_passport])
             ->andFilterWhere(['like', 'payment_plan', $this->payment_plan])
             ->andFilterWhere(['>=', 'instruction_date', $Date[0]])
             ->andFilterWhere(['<', 'instruction_date', $Date[1]])
             ->andFilterWhere(['>=', 'target_date', $TargetDate[0]])
             ->andFilterWhere(['<', 'target_date', $TargetDate[1]])
             ->andFilterWhere(['>=', 'schedule_inspection.valuation_report_date', $InspectionDate[0]])
             ->andFilterWhere(['<', 'schedule_inspection.valuation_report_date', $InspectionDate[1]]);*/

        $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
        if ($this->pageSize<>null) {
            if ($this->pageSize==4) {

                $dataProvider->pagination->pageSize=false;
            }else {
                $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
            }

        }
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }
    public function search_revenue_all($params)
    {


        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'client_revenue'=>'SUM('.Valuation::tableName().'.total_fee)',
                'client_type'=>Company::tableName().'.client_type',
            ])
            ->leftJoin(Company::tableName(),Valuation::tableName().'.client_id='.Company::tableName().'.id')
            ->andWhere(['valuation_status' => 5])
            ->orderBy([
                'client_revenue' => SORT_DESC
            ])
            ->groupBy('company.client_type');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            // 'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }
        /* $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
         if ($this->pageSize<>null) {
             if ($this->pageSize==4) {

                 $dataProvider->pagination->pageSize=false;
             }else {
                 $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
             }

         }*/
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }

    public function search_revenue_all_a($params)
    {


        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'client_revenue'=>'SUM('.Valuation::tableName().'.total_fee)',
                'client_type'=>Company::tableName().'.client_type',
            ])
            ->leftJoin(Company::tableName(),Valuation::tableName().'.client_id='.Company::tableName().'.id')
            ->andWhere(['valuation_status' => 5])
            ->orderBy([
                'client_revenue' => SORT_DESC
            ])
            ->groupBy('company.client_type');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            // 'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDatesCompare($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }
    public function search_revenue_all_a_data($params)
    {


        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'client_revenue'=>'SUM('.Valuation::tableName().'.total_fee)',
                'client_type'=>Company::tableName().'.client_type',
            ])
            ->leftJoin(Company::tableName(),Valuation::tableName().'.client_id='.Company::tableName().'.id')
            ->andWhere(['valuation_status' => 5])
            ->orderBy([
                'client_revenue' => SORT_DESC
            ])
            ->groupBy('company.client_type');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            // 'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDatesCompare($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }
        $all_data = $query->all();

        return $all_data;
    }
    public function search_revenue_all_b($params)
    {


        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'client_revenue'=>'SUM('.Valuation::tableName().'.total_fee)',
                'client_type'=>Company::tableName().'.client_type',
            ])
            ->leftJoin(Company::tableName(),Valuation::tableName().'.client_id='.Company::tableName().'.id')
            ->andWhere(['valuation_status' => 5])
            ->orderBy([
                'client_revenue' => SORT_DESC
            ])
            ->groupBy('company.client_type');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            // 'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDatesCompare($this->time_period);
                $from_date = $date_array['start_date_last'];
                $to_date = $date_array['end_date_last'];
            }

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }
        /* $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
         if ($this->pageSize<>null) {
             if ($this->pageSize==4) {

                 $dataProvider->pagination->pageSize=false;
             }else {
                 $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
             }

         }*/
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }
    public function search_revenue_all_b_data($params)
    {


        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'client_revenue'=>'SUM('.Valuation::tableName().'.total_fee)',
                'client_type'=>Company::tableName().'.client_type',
            ])
            ->leftJoin(Company::tableName(),Valuation::tableName().'.client_id='.Company::tableName().'.id')
            ->andWhere(['valuation_status' => 5])
            ->orderBy([
                'client_revenue' => SORT_DESC
            ])
            ->groupBy('company.client_type');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            // 'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDatesCompare($this->time_period);
                $from_date = $date_array['start_date_last'];
                $to_date = $date_array['end_date_last'];
            }

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }
        /* $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
         if ($this->pageSize<>null) {
             if ($this->pageSize==4) {

                 $dataProvider->pagination->pageSize=false;
             }else {
                 $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
             }

         }*/
        $all_data = $query->all();

        return $all_data;
    }
    public function search_banks_new($params)
    {
        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                Valuation::tableName().'.client_id',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'client_revenue'=>'SUM('.Valuation::tableName().'.total_fee)',
                Company::tableName().'.title',
            ])
            ->leftJoin(Company::tableName(),Valuation::tableName().'.client_id='.Company::tableName().'.id')
            ->where(['company.client_type' => 'bank'])
            ->andWhere(['valuation_status' => 5])
            ->orderBy([
                Company::tableName().'.title' => SORT_ASC,
            ])
            ->groupBy(Valuation::tableName().'.client_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }

    public function search_banks_old_april($params)
    {
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }
        }else{
            $from_date = '2021-04-28';
            $to_date = date('Y-m-d');
        }

        $existing_clients = \app\models\Clients::find()->where(['status' => 1])->andWhere(['client_type'=> 'bank'])->all();
        $existing_client_array=array();
        foreach ($existing_clients as $keyid =>$cname){

            $all_clients = \app\models\Company::find()
                ->where(['status' => 1])
                ->andWhere(['allow_for_valuation'=>1])
                ->andWhere(['main_client_id' => $cname->id])
                ->orderBy(['title' => SORT_ASC,])
                ->all();

            $client_array = array();
            foreach ($all_clients as $key => $client) {
                $all_valuations = \app\models\Valuation::find()
                    ->select([
                        'SUM(total_fee) AS total',
                        'COUNT(*) AS total_valuations',
                        'client_business' => 'SUM('.ValuationApproversData::tableName().'.estimated_market_value)',
                    ])
                    ->leftJoin(ValuationApproversData::tableName(), ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                    ->where(['valuation_status' => 5])
                    ->andWhere(['client_id' => $client->id])
                    ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
                    ->andFilterWhere([
                        'between', Valuation::tableName().'.submission_approver_date', $from_date. ' 00:00:00', $to_date. ' 23:59:59'
                    ])
                    ->all();

                if ($all_valuations[0]->total_valuations > 0 && $all_valuations[0]->total >= 0) {
                    if ($client->main_client_id !== null && $client->main_client_id != 0) {
                        $department_data = \app\models\ClientDepartments::find()->where(['id' => $client->client_department])->one();
                        $client_array[$client->id] = $client->title . ',' . ($all_valuations[0]->total_valuations) . '' . ',' . ($all_valuations[0]->total) . '' . ',' . ($all_valuations[0]->client_business) . '';
                    } else {
                        $client_array[$client->id] = 0;
                    }
                }
            }

            if(count($client_array) > 0) {
                $existing_client_array[$cname->title] = $client_array;
                // $existing_client_array[$total] = $client_array;
            }
        }

        // $query = Valuation::find()
        //     ->select([
        //         Valuation::tableName().'.id',
        //         Valuation::tableName().'.client_id',
        //         'total_valuations'=>'COUNT('.Valuation::tableName().'.id)',
        //       //  'total_client_id'=>'COUNT('.Company::tableName().'.id)',
        //         'client_revenue'=>'SUM('.Valuation::tableName().'.total_fee)',
        //         'client_business'=>'SUM('.ValuationApproversData::tableName().'.estimated_market_value)',
        //         Company::tableName().'.title',
        //     ])
        //     ->leftJoin(Company::tableName(),Valuation::tableName().'.client_id='.Company::tableName().'.id')
        //     ->leftJoin(Clients::tableName(),Valuation::tableName().'.client_id='.Clients::tableName().'.id')
        //     ->leftJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
        //     //->where([Valuation::tableName().'.valuation_status' => 5])

        //     ->where(['clients.client_type' => 'bank'])
        //     ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
        //     ->andWhere(['valuation_status' => 5])
        //     ->orderBy([
        //         'client_revenue' => SORT_DESC
        //     ])->groupBy(Valuation::tableName().'.client_id');

        // dd($query);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $existing_client_array,
            'key' => function ($model) {
                return key($model);
            },
            'pagination' => false, // Disable pagination if not needed
        ]);
        ksort($dataProvider->allModels);



        $dataProvider->pagination->pageSize=100;
        return $dataProvider;
    }
    public function search_banks($params)
    {
        $this->load($params);

        if (!$this->validate()) {
           // die('dd');
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }
        }else{
            $from_date = '2021-04-28';
            $to_date = date('Y-m-d');
        }

        $existing_clients = \app\models\Clients::find()->where(['status' => 1])->andWhere(['client_type'=> 'bank'])->all();
        $existing_client_array=array();
        foreach ($existing_clients as $keyid =>$cname){

            $all_clients = \app\models\Company::find()
                ->where(['status' => 1])
                ->andWhere(['allow_for_valuation'=>1])
                ->andWhere(['main_client_id' => $cname->id])
                ->orderBy(['title' => SORT_ASC,])
                ->all();

            $client_array = array();
            foreach ($all_clients as $key => $client) {
                $all_valuations = \app\models\Valuation::find()
                    ->select([
                        'SUM(total_fee) AS total',
                        'COUNT(*) AS total_valuations',
                        'client_business' => 'SUM('.ValuationApproversData::tableName().'.estimated_market_value)',
                    ])
                    ->leftJoin(ValuationApproversData::tableName(), ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                    ->where(['valuation_status' => 5])
                    ->andWhere(['client_id' => $client->id])
                    ->andWhere(['parent_id' => null])
                    ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
                    ->andFilterWhere([
                        'between', Valuation::tableName().'.submission_approver_date', $from_date. ' 00:00:00', $to_date. ' 23:59:59'
                    ])
                    ->all();

                if ($all_valuations[0]->total_valuations > 0 && $all_valuations[0]->total >= 0) {
                    if ($client->main_client_id !== null && $client->main_client_id != 0) {
                        $department_data = \app\models\ClientDepartments::find()->where(['id' => $client->client_department])->one();
                        $client_array[$client->id] = $client->title . ',' . ($all_valuations[0]->total_valuations) . '' . ',' . ($all_valuations[0]->total) . '' . ',' . ($all_valuations[0]->client_business) . '';
                    } else {
                        $client_array[$client->id] = 0;
                    }
                }
            }
            if(count($client_array) > 0) {
                $existing_client_array[$cname->title] = $client_array;
                // $existing_client_array[$total] = $client_array;
            }
        }

        // $query = Valuation::find()
        //     ->select([
        //         Valuation::tableName().'.id',
        //         Valuation::tableName().'.client_id',
        //         'total_valuations'=>'COUNT('.Valuation::tableName().'.id)',
        //       //  'total_client_id'=>'COUNT('.Company::tableName().'.id)',
        //         'client_revenue'=>'SUM('.Valuation::tableName().'.total_fee)',
        //         'client_business'=>'SUM('.ValuationApproversData::tableName().'.estimated_market_value)',
        //         Company::tableName().'.title',
        //     ])
        //     ->leftJoin(Company::tableName(),Valuation::tableName().'.client_id='.Company::tableName().'.id')
        //     ->leftJoin(Clients::tableName(),Valuation::tableName().'.client_id='.Clients::tableName().'.id')
        //     ->leftJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
        //     //->where([Valuation::tableName().'.valuation_status' => 5])

        //     ->where(['clients.client_type' => 'bank'])
        //     ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
        //     ->andWhere(['valuation_status' => 5])
        //     ->orderBy([
        //         'client_revenue' => SORT_DESC
        //     ])->groupBy(Valuation::tableName().'.client_id');

        // dd($existing_client_array);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $existing_client_array,
            'key' => function ($model) {
                return key($model);
            },
            'pagination' => false, // Disable pagination if not needed
        ]);
        ksort($dataProvider->allModels);



       // $dataProvider->pagination->pageSize=100;
        return $dataProvider;
    }

    public function search_banks_business($params)
    {
        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                Valuation::tableName().'.client_id',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'client_revenue'=>'SUM('.Valuation::tableName().'.total_fee)',
                Company::tableName().'.title',
            ])
            ->leftJoin(Company::tableName(),Valuation::tableName().'.client_id='.Company::tableName().'.id')
            ->where(['company.client_type' => 'bank'])
            ->andWhere(['valuation_status' => 5])
            ->orderBy([
                Company::tableName().'.title' => SORT_ASC,
            ])
            ->groupBy(Valuation::tableName().'.client_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }
    public function search_corporate($params)
    {


        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                Valuation::tableName().'.client_id',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'client_revenue'=>'SUM('.Valuation::tableName().'.total_fee)',
                Company::tableName().'.title',
            ])
            ->leftJoin(Company::tableName(),Valuation::tableName().'.client_id='.Company::tableName().'.id')
            ->where(['company.client_type' => 'corporate'])
            ->andWhere(['valuation_status' => 5])
            ->orderBy([
                'client_revenue' => SORT_DESC
            ])
            ->groupBy(Valuation::tableName().'.client_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //  'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }
        /* $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
         if ($this->pageSize<>null) {
             if ($this->pageSize==4) {

                 $dataProvider->pagination->pageSize=false;
             }else {
                 $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
             }

         }*/
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }
    public function search_individual($params)
    {

        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                Valuation::tableName().'.client_id',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'client_revenue'=>'SUM('.Valuation::tableName().'.total_fee)',
                Company::tableName().'.title',
            ])
            ->leftJoin(Company::tableName(),Valuation::tableName().'.client_id='.Company::tableName().'.id')
            ->where(['company.client_type' => 'individual'])
            ->andWhere(['valuation_status' => 5])
            ->orderBy([
                'client_revenue' => SORT_DESC
            ])
            ->groupBy(Valuation::tableName().'.client_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            // 'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }



            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }
        /* $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
         if ($this->pageSize<>null) {
             if ($this->pageSize==4) {

                 $dataProvider->pagination->pageSize=false;
             }else {
                 $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
             }

         }*/
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }
    public function search_revenue_cities($params)
    {


        $query = Valuation::find()
            ->select([
                'zone_name' =>Zone::tableName().'.title',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'client_revenue'=>'SUM('.Valuation::tableName().'.total_fee)',
            ])
            ->leftJoin(Buildings::tableName(),Valuation::tableName().'.building_info='.Buildings::tableName().'.id')
            ->leftJoin(Zone::tableName(),Buildings::tableName().'.city='.Zone::tableName().'.id')
            ->where(['valuation_status' => 5])
            ->orderBy([
                'client_revenue' => SORT_DESC
            ])
            ->groupBy(Zone::tableName().'.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }
        /* $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
         if ($this->pageSize<>null) {
             if ($this->pageSize==4) {

                 $dataProvider->pagination->pageSize=false;
             }else {
                 $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
             }

         }*/
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }
    public function search_revenue_valuers_old($params)
    {


        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.service_officer_name',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'client_revenue'=>'SUM('.Valuation::tableName().'.total_fee)',
                'tat'=> 'DATEDIFF("2017-06-25", "2017-06-15")'
            ])
            ->where(['valuation_status' => 5])
            ->groupBy(Valuation::tableName().'.service_officer_name');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }
        /* $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
         if ($this->pageSize<>null) {
             if ($this->pageSize==4) {

                 $dataProvider->pagination->pageSize=false;
             }else {
                 $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
             }

         }*/
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }

    public function search_revenue_valuers($params)
    {



        $this->load($params);
        $subquery_inspections = '';
        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }


        }else{
            $from_date = '2021-04-28';
            $to_date = date('Y-m-d');
        }

        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.service_officer_name',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'client_revenue'=>'SUM('.Valuation::tableName().'.total_fee)',
                'total_inspections'=>'(SELECT COUNT('.ScheduleInspection::tableName().'.id) from schedule_inspection WHERE schedule_inspection.inspection_officer=valuation.service_officer_name AND (schedule_inspection.created_at BETWEEN "'.$from_date.' 00:00:00" AND "'.$to_date.' 23:59:59"))',
                // 'tat'=> 'SUM(DATEDIFF(schedule_inspection.valuation_report_date,schedule_inspection.inspection_date))', //comment by usama
                //'tat'=> 'SUM( TOTAL_WEEKDAYS(schedule_inspection.valuation_report_date, schedule_inspection.inspection_date) )',

                //add by usama
                'tat' => 'SUM(
                    DATEDIFF(schedule_inspection.valuation_report_date, schedule_inspection.inspection_date) 
                    + 1 
                    - (
                        CASE
                            WHEN DATEDIFF(schedule_inspection.valuation_report_date, schedule_inspection.inspection_date) > 0
                            THEN (2 * (DATEDIFF(schedule_inspection.valuation_report_date, schedule_inspection.inspection_date) + 
                            WEEKDAY(schedule_inspection.inspection_date) + 1) / 7)
                            ELSE 0
                        END
                    )
                )',
                //end by usama
                'low_vals'=> 'SUM(CASE 
                                    WHEN valuation.revised_reason = "1" 
                                    THEN 1
                                    ELSE 0 
                              END)',
                'high_vals'=> 'SUM(CASE 
                                    WHEN valuation.revised_reason = 2 
                                    THEN 1 
                                    ELSE 0 
                              END)',
                'errors_vals'=> 'SUM(CASE 
                                    WHEN valuation.revised_reason = 3 
                                    THEN 1
                                    ELSE 0 
                              END)',
            ])
            ->innerJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->where(['valuation_status' => 5])
            // ->andwhere(['valuation_status' => 5])
            ->andwhere(['not', ['service_officer_name' => null]])
            ->orderBy([
                'client_revenue' => SORT_DESC
            ])
            ->groupBy(Valuation::tableName().'.service_officer_name');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);



        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'submission_approver_date', $from_date. ' 00:00:00', $to_date. ' 23:59:59'
            ]);
        }
        /* $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
         if ($this->pageSize<>null) {
             if ($this->pageSize==4) {

                 $dataProvider->pagination->pageSize=false;
             }else {
                 $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
             }

         }*/
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }
    public function search_valuers_performance($params)
    {
        $this->load($params);
        $subquery_inspections = '';

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0].' 00:00:00';
                    $to_date = $Date[1].' 23:59:59';
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'].' 00:00:00';

                $to_date = $date_array['end_date'] . ' 23:59:59';
                // $to_date = date('Y-m-d H:i:s', strtotime($endDateTime . ' +1 day'));
            }
        }else{
            $from_date = '2021-04-28 00:00:00';
            $to_date = date('Y-m-d H:i:s');
        }


        $subqueryTotalInspections = (new Query())
            ->select('COUNT(id)')
            ->from('schedule_inspection')
            ->where('inspection_officer = v.service_officer_name')
            ->andWhere(['between', 'created_at', $from_date, $to_date]);

        $subqueryHighVals = (new Query())
            ->select('COUNT(*)')
            ->from('valuation v2')
            ->where(['v2.revised_reason' => 2])
            ->andWhere(['v2.service_officer_name' => new \yii\db\Expression('v.service_officer_name')])
            ->andWhere(['v2.valuation_status' => 5])
            ->andWhere(['between', 'v2.submission_approver_date', $from_date, $to_date]);

        $subqueryErrorsVals = (new Query())
            ->select('COUNT(*)')
            ->from('valuation v3')
            ->where(['v3.revised_reason' => 3])
            ->andWhere(['v3.service_officer_name' => new \yii\db\Expression('v.service_officer_name')])
            ->andWhere(['v3.valuation_status' => 5])
            ->andWhere(['between', 'v3.submission_approver_date', $from_date, $to_date]);

        $subqueryLowVals = (new Query())
            ->select('COUNT(*)')
            ->from('valuation v1')
            ->where(['v1.revised_reason' => 1])
            ->andWhere(['v1.service_officer_name' => new \yii\db\Expression('v.service_officer_name')])
            ->andWhere(['v1.valuation_status' => 5])
            ->andWhere(['between', 'v1.submission_approver_date', $from_date, $to_date]);


        $query = Valuation::find()
            ->select([
                'v.service_officer_name',
                'total_valuations' => new \yii\db\Expression('COUNT(v.id)'),
                'client_revenue' => new \yii\db\Expression('SUM(v.total_fee)'),
                'total_inspections' => $subqueryTotalInspections,
                'low_vals' => $subqueryLowVals,
                'high_vals' => $subqueryHighVals,
                'errors_vals' => $subqueryErrorsVals,
                'client_business' => new \yii\db\Expression('SUM(CASE WHEN vone.reason = 8 THEN 1 ELSE 0 END)'),
            ])
            ->from('valuation v')
            ->leftJoin('vone_valuations vone', 'vone.valuation_id = v.id')
            ->andWhere(['v.valuation_status' => 5])
            ->andWhere('v.parent_id IS NULL')
            ->andWhere(['between', 'v.submission_approver_date', $from_date, $to_date])
            ->groupBy('v.service_officer_name')
            ->orderBy(['client_revenue' => SORT_DESC]);

        $results = $query->all();

        $totRev = 0;
        $totVal = 0;
        $totInsp = 0;
        $totRem = 0;
        $totLow = 0;
        $totHigh = 0;
        $totError = 0;
        $valCount = COUNT($results);
        foreach($results as $key => $data)
        {
            $totRev += $data->client_revenue;
            $totVal += $data->total_valuations;
            $totInsp += $data->total_inspections;
            $totRem += $data->client_business;
            $totLow += $data->low_vals;
            $totHigh += $data->high_vals;
            $totError += $data->errors_vals;
        }
        session_start();
        $_SESSION['total_revenue'] = $totRev;
        $_SESSION['total_valuations'] = $totVal;
        $_SESSION['total_inspections'] = $totInsp;
        $_SESSION['client_business'] = $totRem;
        $_SESSION['low_valuations'] = $totLow;
        $_SESSION['high_valuations'] = $totHigh;
        $_SESSION['error_valuations'] = $totError;
        $_SESSION['count_val'] = $valCount;


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);



        if (!$this->validate()) {
            return $dataProvider;
        }
        /* $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
         if ($this->pageSize<>null) {
             if ($this->pageSize==4) {

                 $dataProvider->pagination->pageSize=false;
             }else {
                 $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
             }

         }*/
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }
    public function search_valuers_performance_old_29($params)
    {
        $this->load($params);
        $subquery_inspections = '';

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0].' 00:00:00';
                    $to_date = $Date[1].' 23:59:59';
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'].' 00:00:00';

                $endDateTime = $date_array['end_date'] . ' 23:59:59';
                $to_date = date('Y-m-d H:i:s', strtotime($endDateTime . ' +1 day'));
            }
        }else{
            $from_date = '2021-04-28 00:00:00';
            $to_date = date('Y-m-d H:i:s');
        }

        $query = Valuation::find()
            ->select([
                'service_officer_name',
                'total_valuations' => new \yii\db\Expression('COUNT(valuation.id)'),
                'client_revenue' => new \yii\db\Expression('SUM(valuation.total_fee)'),
                'total_inspections' => new \yii\db\Expression('(SELECT COUNT(id) FROM ' . ScheduleInspection::tableName() . ' WHERE inspection_officer=service_officer_name AND (created_at BETWEEN "'.$from_date.'" AND "'.$to_date.'"))'),
                'low_vals' => new \yii\db\Expression('SUM(CASE WHEN revised_reason = 1 THEN 1 ELSE 0 END)'),
                'high_vals' => new \yii\db\Expression('SUM(CASE WHEN revised_reason = 2 THEN 1 ELSE 0 END)'),
                'errors_vals' => new \yii\db\Expression('SUM(CASE WHEN revised_reason = 3 THEN 1 ELSE 0 END)'),
                'client_business' => 'SUM(CASE WHEN vone_valuations.reason = 8 THEN 1 ELSE 0 END)',
            ])
            ->leftJoin(ScheduleInspection::tableName(), 'schedule_inspection.valuation_id = valuation.id')
            ->leftJoin('vone_valuations', 'vone_valuations.valuation_id = valuation.id')
            // ->where(['not', ['valuation.inspection_type' => 3]])
            ->andWhere(['valuation_status' => 5])
            ->andWhere(['parent_id'=>null])
            ->andWhere(['between', 'valuation.submission_approver_date', $from_date, $to_date])
            ->orderBy([
                'client_revenue' => SORT_DESC
            ])
            ->groupBy('service_officer_name');

        $results = $query->all();

        $totRev = 0;
        $totVal = 0;
        $totInsp = 0;
        $totRem = 0;
        $totLow = 0;
        $totHigh = 0;
        $totError = 0;
        $valCount = COUNT($results);
        foreach($results as $key => $data)
        {
            $totRev += $data->client_revenue;
            $totVal += $data->total_valuations;
            $totInsp += $data->total_inspections;
            $totRem += $data->client_business;
            $totLow += $data->low_vals;
            $totHigh += $data->high_vals;
            $totError += $data->errors_vals;
        }
        session_start();
        $_SESSION['total_revenue'] = $totRev;
        $_SESSION['total_valuations'] = $totVal;
        $_SESSION['total_inspections'] = $totInsp;
        $_SESSION['client_business'] = $totRem;
        $_SESSION['low_valuations'] = $totLow;
        $_SESSION['high_valuations'] = $totHigh;
        $_SESSION['error_valuations'] = $totError;
        $_SESSION['count_val'] = $valCount;


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);



        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'submission_approver_date', $from_date. ' 00:00:00', $to_date. ' 23:59:59'
            ]);
        }
        /* $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
         if ($this->pageSize<>null) {
             if ($this->pageSize==4) {

                 $dataProvider->pagination->pageSize=false;
             }else {
                 $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
             }

         }*/
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }

    public function search_approvers_performance($params)
    {
        $this->load($params);
        $subquery_inspections = '';
        if($this->time_period <> null && $this->time_period != 0 ) {
            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }


        }else{
            $from_date = '2021-04-28';
            $to_date = date('Y-m-d');
        }

        $from_date = "2024-01-01";
        $to_date = "2024-01-03";

        // print_r($from_date);
        // echo "<br>";
        // print_r($to_date);die;

        $ids_to_exclude = [27, 18, 42, 28];
        $query = ValuationApproversData::find()
            ->select([
                'valuation_approvers_data.created_by',
                'user.lastname',
                'total_valuations' => 'COUNT(valuation.id)',
                'client_revenue' => 'SUM(valuation.total_fee)',
                'low_vals' => 'SUM(CASE WHEN valuation.revised_reason = 1 THEN 1 ELSE 0 END)',
                'high_vals' => 'SUM(CASE WHEN valuation.revised_reason = 2 THEN 1 ELSE 0 END)',
                'errors_vals' => 'SUM(CASE WHEN valuation.revised_reason = 3 THEN 1 ELSE 0 END)',
                'client_reminders' => 'SUM(CASE WHEN vone_valuations.reason = 8 THEN 1 ELSE 0 END)',
            ])
            ->leftJoin('vone_valuations', 'vone_valuations.valuation_id = valuation.id')
            ->innerJoin('user', 'user.id = valuation_approvers_data.created_by')
            ->innerJoin('valuation', 'valuation.id = valuation_approvers_data.valuation_id')
            ->where([
                'valuation_approvers_data.approver_type' => 'approver',
                'valuation_approvers_data.created_by' => 21,
            ])
            ->andWhere(['between', 'valuation_approvers_data.created_at', '2024-01-01', '2024-01-03'])
            ->groupBy(['valuation_approvers_data.created_by', 'user.lastname']);

        $results = $query->all();

        dd($results);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);



        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {
            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            /* $query->andFilterWhere([
                 'between', 'instruction_date', $from_date, $to_date
             ]);*/
            $query->andFilterWhere([ 'between', ValuationApproversData::tableName().'.created_at', $from_date. ' 00:00:00', $to_date. ' 23:59:59' ]);
        }
        /* $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
         if ($this->pageSize<>null) {
             if ($this->pageSize==4) {

                 $dataProvider->pagination->pageSize=false;
             }else {
                 $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
             }

         }*/
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }

    public function search_revenue_valuers_inspection($params)
    {




        $subquery_inspections = '';


        $query = Valuation::find()
            ->select([
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'inspection_officer_name'=>ScheduleInspection::tableName().'.inspection_officer',
                'client_revenue'=>'SUM('.Valuation::tableName().'.total_fee)',
                //  'total_inspections'=>'(SELECT COUNT('.ScheduleInspection::tableName().'.id) from schedule_inspection)',
                'tat'=> 'SUM(DATEDIFF(schedule_inspection.valuation_report_date,schedule_inspection.inspection_date))',
                //'tat'=> 'SUM( TOTAL_WEEKDAYS(schedule_inspection.valuation_report_date, schedule_inspection.inspection_date) )',
            ])
            ->innerJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->where(['valuation_status' => 5])
            ->andWhere(['not', [ScheduleInspection::tableName().'.inspection_officer' => null]])
            ->andWhere(['not', [ScheduleInspection::tableName().'.inspection_officer' => 0]])
            ->orderBy([
                'client_revenue' => SORT_DESC
            ])
            ->groupBy(ScheduleInspection::tableName().'.inspection_officer');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }
        /* $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
         if ($this->pageSize<>null) {
             if ($this->pageSize==4) {

                 $dataProvider->pagination->pageSize=false;
             }else {
                 $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
             }

         }*/
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }

    public function search_operations_report($params)
    {




        $subquery_inspections = '';


        $query = Valuation::find()
            ->select([
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'inspection_officer_name'=>ScheduleInspection::tableName().'.inspection_officer',
                'client_revenue'=>'SUM('.Valuation::tableName().'.total_fee)',
                //  'total_inspections'=>'(SELECT COUNT('.ScheduleInspection::tableName().'.id) from schedule_inspection)',
                'tat'=> 'SUM(DATEDIFF(schedule_inspection.valuation_report_date,schedule_inspection.inspection_date))',
                //'tat'=> 'SUM( TOTAL_WEEKDAYS(schedule_inspection.valuation_report_date, schedule_inspection.inspection_date) )',
            ])
            ->innerJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->where(['valuation_status' => 5])
            ->andWhere(['not', [ScheduleInspection::tableName().'.inspection_officer' => null]])
            ->andWhere(['not', [ScheduleInspection::tableName().'.inspection_officer' => 0]])
            ->orderBy([
                'client_revenue' => SORT_DESC
            ])
            ->groupBy(ScheduleInspection::tableName().'.inspection_officer');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }
        /* $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
         if ($this->pageSize<>null) {
             if ($this->pageSize==4) {

                 $dataProvider->pagination->pageSize=false;
             }else {
                 $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
             }

         }*/
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }

    public function search_revenue_properties($params)
    {
        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.property_id',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'client_revenue'=>'SUM('.Valuation::tableName().'.total_fee)',
            ])
            ->where(['valuation_status' => 5])
            ->orderBy([
                'client_revenue' => SORT_DESC
            ])
            ->groupBy(Valuation::tableName().'.property_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }
        /* $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
         if ($this->pageSize<>null) {
             if ($this->pageSize==4) {

                 $dataProvider->pagination->pageSize=false;
             }else {
                 $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
             }

         }*/
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }

    public function search_low_high_valuations($params)
    {
        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                Valuation::tableName().'.parent_id',
                Valuation::tableName().'.client_id',
                Valuation::tableName().'.property_id',
                Valuation::tableName().'.building_info',
                Valuation::tableName().'.service_officer_name',
                'estimated_market_value'=>ValuationApproversData::tableName().'.estimated_market_value',
                'approver'=>ValuationApproversData::tableName().'.created_by',
            ])
            ->leftJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
            //->where([Valuation::tableName().'.valuation_status' => 5])
            ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
            ->andWhere([Valuation::tableName().'.revised_reason' => [1,2]]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }
        $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
        if ($this->pageSize<>null) {
            if ($this->pageSize==4) {

                $dataProvider->pagination->pageSize=false;
            }else {
                $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
            }

        }
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }

    public function search_modified_valuations($params)
    {
        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                Valuation::tableName().'.parent_id',
                Valuation::tableName().'.client_id',
                Valuation::tableName().'.property_id',
                'estimated_market_value'=>ValuationApproversData::tableName().'.estimated_market_value',
                'approver'=>ValuationApproversData::tableName().'.created_by',
            ])
            ->leftJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
            //->where([Valuation::tableName().'.valuation_status' => 5])
            ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
            ->andWhere([Valuation::tableName().'.revised_reason' => [5]]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }
        $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
        if ($this->pageSize<>null) {
            if ($this->pageSize==4) {

                $dataProvider->pagination->pageSize=false;
            }else {
                $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
            }

        }
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }

    public function search_errors_valuations($params)
    {
        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                Valuation::tableName().'.parent_id',
                Valuation::tableName().'.client_id',
                Valuation::tableName().'.property_id',
                'estimated_market_value'=>ValuationApproversData::tableName().'.estimated_market_value',
                'approver'=>ValuationApproversData::tableName().'.created_by',
            ])
            ->leftJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
            //->where([Valuation::tableName().'.valuation_status' => 5])
            ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
            ->andWhere([Valuation::tableName().'.revised_reason' => [3]]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }
        $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
        if ($this->pageSize<>null) {
            if ($this->pageSize==4) {

                $dataProvider->pagination->pageSize=false;
            }else {
                $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
            }

        }
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }

    public function search_sold_duplicates($params)
    {
        $query = SoldTransaction::find()
            ->select('a.*')
            ->from('sold_transaction AS a')
            ->innerJoin('(SELECT id,transaction_date, building_info,community,sub_community,no_of_bedrooms,land_size,built_up_area,price_per_sqt,listings_price,COUNT(*)
            FROM sold_transaction 
            GROUP BY transaction_date, building_info,community,sub_community,no_of_bedrooms,land_size,built_up_area,price_per_sqt,listings_price
            HAVING count(*) > 1 ) b','a.building_info = b.building_info
            AND a.transaction_date = b.transaction_date
            AND a.community = b.community
            AND a.sub_community = b.sub_community
            AND a.no_of_bedrooms = b.no_of_bedrooms
            AND a.land_size = b.land_size
            AND a.built_up_area = b.built_up_area
            AND a.price_per_sqt = b.price_per_sqt
            AND a.listings_price = b.listings_price')
            ->orderBy('a.transaction_date');



        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'a.transaction_date', $from_date, $to_date
            ]);
        }
        $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
        if ($this->pageSize<>null) {
            if ($this->pageSize==4) {

                $dataProvider->pagination->pageSize=false;
            }else {
                $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
            }

        }
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }

    public function search_list_duplicates($params)
    {
        $query = ListingsTransactions::find()
            ->select('a.*')
            ->from('listings_transactions AS a')
            ->innerJoin('(SELECT id,listing_date, building_info,community,sub_community,no_of_bedrooms,land_size,built_up_area,price_per_sqt,listings_price,property_placement,property_exposure,COUNT(*)
            FROM listings_transactions 
            GROUP BY listing_date, building_info,community,sub_community,no_of_bedrooms,land_size,built_up_area,price_per_sqt,listings_price
            HAVING count(*) > 1 ) b','a.building_info = b.building_info
            AND a.listing_date = b.listing_date
            AND a.community = b.community
            AND a.sub_community = b.sub_community
            AND a.no_of_bedrooms = b.no_of_bedrooms
            AND a.land_size = b.land_size
            AND a.built_up_area = b.built_up_area
            AND a.price_per_sqt = b.price_per_sqt
            AND a.listings_price = b.listings_price
            AND a.property_placement = b.property_placement
            AND a.property_exposure = b.property_exposure
            ')
            ->orderBy('a.building_info');



        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'a.listing_date', $from_date, $to_date
            ]);
        }
        $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
        if ($this->pageSize<>null) {
            if ($this->pageSize==4) {

                $dataProvider->pagination->pageSize=false;
            }else {
                $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
            }

        }
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }

    public function search_list_duplicates_reference($params)
    {
        $query = ListingsTransactions::find()
            ->select('a.*')
            ->from(' listings_transactions AS a')
            ->innerJoin('(SELECT id,listings_reference,COUNT(*)
            FROM  listings_transactions 
            GROUP BY listings_reference
            HAVING count(*) > 1 ) b','a.listings_reference = b.listings_reference
            ')
            ->orderBy('a.listings_reference');



        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'a.listing_date', $from_date, $to_date
            ]);
        }
        $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
        if ($this->pageSize<>null) {
            if ($this->pageSize==4) {

                $dataProvider->pagination->pageSize=false;
            }else {
                $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
            }

        }
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }

    public function search_tat_old($params)
    {
        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                Valuation::tableName().'.reference_number',
                Valuation::tableName().'.instruction_date',
                Valuation::tableName().'.service_officer_name',
                Valuation::tableName().'.building_info',
                Valuation::tableName().'.property_id',
                Valuation::tableName().'.tenure',
                Valuation::tableName().'.created_at',
                'submission_date' => ScheduleInspection::tableName().'.valuation_report_date',
                'inspection_date' => ScheduleInspection::tableName().'.inspection_date',
                'inspection_time' => ScheduleInspection::tableName().'.inspection_time',
                'inspection_officer_name' => ScheduleInspection::tableName().'.inspection_officer',
                Valuation::tableName().'.property_id',
                'approver'=>ValuationApproversData::tableName().'.created_by'
            ])
            ->innerJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->where([Valuation::tableName().'.valuation_status' => 5])
            ->andWhere([Valuation::tableName().'.parent_id' => null])
            ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }else{
            $date_array = Yii::$app->appHelperFunctions->getFilterDates(1);
            $from_date = $date_array['start_date'];
            $to_date = $date_array['end_date'];

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }
        $pagelist=[1=>'20',2=>'50',3=>'1000',4=>'Show All'];
        if ($this->pageSize<>null) {
            if ($this->pageSize==4) {

                $dataProvider->pagination->pageSize=false;
            }else {
                $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
            }

        }
        $dataProvider->pagination->pageSize=1000;

        return $dataProvider;
    }

    public function search_tat_ahead($params)
    {
        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                Valuation::tableName().'.reference_number',
                Valuation::tableName().'.instruction_date',
                Valuation::tableName().'.target_date',
                Valuation::tableName().'.service_officer_name',
                Valuation::tableName().'.total_fee',
                Valuation::tableName().'.client_reference',
                Valuation::tableName().'.submission_approver_date',
                'submission_date' => ValuationApproversData::tableName().'.created_at',
                // 'submission_date' => ScheduleInspection::tableName().'.valuation_report_date',
                //'inspection_date' => ScheduleInspection::tableName().'.inspection_date',
                'inspection_officer_name' => ScheduleInspection::tableName().'.inspection_officer',
                Valuation::tableName().'.property_id',
                'approver'=>ValuationApproversData::tableName().'.created_by',
                ScheduleInspection::tableName().'.inspection_date',
                Valuation::tableName().'.client_id',
            ])
            ->innerJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->where([Valuation::tableName().'.valuation_status' => 5])
            ->andwhere('DATE(target_date) > DATE(valuation_approvers_data.created_at)')
            ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        if($this->time_period <> null && $this->time_period != 0 ) {
            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'DATE(valuation_approvers_data.created_at)', $from_date, $to_date
            ]);
        }else{
            if(isset($params['ValuationReportsSearch']['time_period']) && $params['ValuationReportsSearch']['time_period'] > 0)
            {
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($params['ValuationReportsSearch']['time_period']);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];

                $query->andFilterWhere([
                    'between', 'DATE(valuation_approvers_data.created_at)', $from_date, $to_date
                ]);
            }

            if(isset($params['ValuationReportsSearch']['time_period']) && $params['ValuationReportsSearch']['time_period'] == "0")
            {
                $from_date = '2021-04-28 00:00:00';
                $to_date = date('Y-m-d').' 23:59:59';

                $query->andFilterWhere([
                    'between', 'DATE(valuation_approvers_data.created_at)', $from_date, $to_date
                ]);
            }

            if($this->target != 'all_aheadoftime' && $this->target != null){
                $date_array = Yii::$app->appHelperFunctions->getFilterDates(8);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];

                $query->andFilterWhere([
                    'between', 'DATE(valuation_approvers_data.created_at)', $from_date, $to_date
                ]);
            }

            if($this->target == null && $params['ValuationReportsSearch']['page_title'] != "Total Ahead of Time Reports"){
                $from_date = date('Y-m-d').' 00:00:00';
                $to_date = date('Y-m-d').' 23:59:59';

                $query->andFilterWhere([
                    'between', 'DATE(valuation_approvers_data.created_at)', $from_date, $to_date
                ]);
            }
        }

        $pagelist=[1=>'20',2=>'50',3=>'1000',4=>'Show All'];
        if ($this->pageSize<>null) {
            if ($this->pageSize==4) {

                $dataProvider->pagination->pageSize=false;
            }else {
                $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
            }

        }
        $dataProvider->pagination->pageSize=10000;

        return $dataProvider;
    }
    public function search_tat_ontime($params)
    {
        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                Valuation::tableName().'.reference_number',
                Valuation::tableName().'.instruction_date',
                Valuation::tableName().'.target_date',
                Valuation::tableName().'.service_officer_name',
                Valuation::tableName().'.total_fee',
                Valuation::tableName().'.client_reference',
                Valuation::tableName().'.submission_approver_date',
                'submission_date' => ValuationApproversData::tableName().'.created_at',
                // 'submission_date' => ScheduleInspection::tableName().'.valuation_report_date',
                //'inspection_date' => ScheduleInspection::tableName().'.inspection_date',
                'inspection_officer_name' => ScheduleInspection::tableName().'.inspection_officer',
                Valuation::tableName().'.property_id',
                'approver'=>ValuationApproversData::tableName().'.created_by'
            ])
            ->innerJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->where([Valuation::tableName().'.valuation_status' => 5])
            ->andwhere('DATE(target_date) = DATE(valuation_approvers_data.created_at)')
            ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'DATE(valuation_approvers_data.created_at)', $from_date, $to_date
            ]);
        }else{
            if(isset($params['ValuationReportsSearch']['time_period']) && $params['ValuationReportsSearch']['time_period'] > 0)
            {
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($params['ValuationReportsSearch']['time_period']);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];

                $query->andFilterWhere([
                    'between', 'DATE(valuation_approvers_data.created_at)', $from_date, $to_date
                ]);
            }

            if(isset($params['ValuationReportsSearch']['time_period']) && $params['ValuationReportsSearch']['time_period'] == "0")
            {
                $from_date = '2021-04-28 00:00:00';
                $to_date = date('Y-m-d').' 23:59:59';

                $query->andFilterWhere([
                    'between', 'DATE(valuation_approvers_data.created_at)', $from_date, $to_date
                ]);
            }

            if($this->target != 'all_ontime' && $this->target != null){
                $date_array = Yii::$app->appHelperFunctions->getFilterDates(8);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];

                $query->andFilterWhere([
                    'between', 'DATE(valuation_approvers_data.created_at)', $from_date, $to_date
                ]);
            }

            if($this->target == null && $params['ValuationReportsSearch']['page_title'] != "Total On Time"){
                $from_date = date('Y-m-d').' 00:00:00';
                $to_date = date('Y-m-d').' 23:59:59';

                $query->andFilterWhere([
                    'between', 'DATE(valuation_approvers_data.created_at)', $from_date, $to_date
                ]);
            }
        }


        $pagelist=[1=>'20',2=>'50',3=>'1000',4=>'Show All'];
        if ($this->pageSize<>null) {
            if ($this->pageSize==4) {

                $dataProvider->pagination->pageSize=false;
            }else {
                $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
            }

        }
        $dataProvider->pagination->pageSize=10000;

        return $dataProvider;
    }

    public function search_tat_delay($params)
    {
        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                Valuation::tableName().'.reference_number',
                Valuation::tableName().'.instruction_date',
                Valuation::tableName().'.target_date',
                Valuation::tableName().'.service_officer_name',
                Valuation::tableName().'.submission_approver_date',
                Valuation::tableName().'.total_fee',
                Valuation::tableName().'.client_reference',
                Valuation::tableName().'.client_id',
                'submission_date' => ValuationApproversData::tableName().'.created_at',
                // 'submission_date' => ScheduleInspection::tableName().'.valuation_report_date',
                //'inspection_date' => ScheduleInspection::tableName().'.inspection_date',
                'inspection_officer_name' => ScheduleInspection::tableName().'.inspection_officer',
                Valuation::tableName().'.property_id',
                'approver'=>ValuationApproversData::tableName().'.created_by'
            ])
            ->leftJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->where([Valuation::tableName().'.valuation_status' => 5])
            ->andWhere([Valuation::tableName().'.parent_id' => null])
            ->andwhere('DATE(target_date) < DATE(valuation_approvers_data.created_at)')
            ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }
            if($params<> null && $params['time'] == "pending")
            {

            }else{
                $query->andFilterWhere([
                    'between', 'DATE(valuation_approvers_data.created_at)', $from_date, $to_date
                ]);
            }

        }else{
            if(isset($params['ValuationReportsSearch']['time_period']) && $params['ValuationReportsSearch']['time_period'] > 0)
            {
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($params['ValuationReportsSearch']['time_period']);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];

                $query->andFilterWhere([
                    'between', 'DATE(valuation_approvers_data.created_at)', $from_date, $to_date
                ]);
            }

            if(isset($params['ValuationReportsSearch']['time_period']) && $params['ValuationReportsSearch']['time_period'] == "0")
            {
                $from_date = '2021-04-28 00:00:00';
                $to_date = date('Y-m-d').' 23:59:59';

                $query->andFilterWhere([
                    'between', 'DATE(valuation_approvers_data.created_at)', $from_date, $to_date
                ]);
            }



            if($params<> null && $params['time'] == "pending")
            {

            }else{
                if($this->target != 'all_delay' && $this->target != null){
                    $date_array = Yii::$app->appHelperFunctions->getFilterDates(8);
                    $from_date = $date_array['start_date'];
                    $to_date = $date_array['end_date'];

                    $query->andFilterWhere([
                        'between', 'DATE(valuation_approvers_data.created_at)', $from_date, $to_date
                    ]);
                }
            }

            if($this->target == null && $params['ValuationReportsSearch']['page_title'] != "Total Delay Reports" ){
                $from_date = date('Y-m-d').' 00:00:00';
                $to_date = date('Y-m-d').' 23:59:59';

                $query->andFilterWhere([
                    'between', 'DATE(valuation_approvers_data.created_at)', $from_date, $to_date
                ]);
            }
        }

        $pagelist=[1=>'20',2=>'50',3=>'1000',4=>'Show All'];
        if ($this->pageSize<>null) {
            if ($this->pageSize==4) {

                $dataProvider->pagination->pageSize=false;
            }else {
                $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
            }

        }
        $dataProvider->pagination->pageSize=10000;

        return $dataProvider;
    }

    public function search_gy_properties($params)
    {



        $this->load($params);
        if(isset($this->city_filter) && $this->city_filter <> null){
            /*    $query = Valuation::find()
                    ->select([
                        Valuation::tableName().'.property_id',
                        'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                        'mv_psf'=>'AVG('.ValuationApproversData::tableName().'.estimated_market_value_sqf)',
                        'rv_psf'=>'AVG('.ValuationApproversData::tableName().'.estimated_market_rent_sqf)',
                    ])
                    ->innerJoin(Buildings::tableName(),Valuation::tableName().'.building_info='.Buildings::tableName().'.id')
                    ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                    ->where(['valuation_status' => 5])
                    ->andWhere([Buildings::tableName().'.city' => $this->city_filter])
                    ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
                    ->groupBy(Valuation::tableName().'.property_id');

                $dataProvider = new ActiveDataProvider([
                    'query' => $query,
                    //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
                ]);*/

        }else{
            /* $query = Valuation::find()
                 ->select([
                     Valuation::tableName().'.property_id',
                     'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                     'mv_psf'=>'AVG('.ValuationApproversData::tableName().'.estimated_market_value_sqf)',
                     'rv_psf'=>'AVG('.ValuationApproversData::tableName().'.estimated_market_rent_sqf)',
                 ])
                 ->innerJoin(Buildings::tableName(),Valuation::tableName().'.building_info='.Buildings::tableName().'.id')
                 ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                 ->where(['valuation_status' => 5])
                 ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
                 ->groupBy(Valuation::tableName().'.property_id');

             $dataProvider = new ActiveDataProvider([
                 'query' => $query,
                 //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
             ]);*/

        }

        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.property_id',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'mv_psf'=>'AVG('.ValuationApproversData::tableName().'.estimated_market_value_sqf)',
                'rv_psf'=>'AVG('.ValuationApproversData::tableName().'.estimated_market_rent_sqf)',
            ])
            ->innerJoin(Buildings::tableName(),Valuation::tableName().'.building_info='.Buildings::tableName().'.id')
            ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->where(['valuation_status' => 5]);
        if(isset($this->city_filter) && $this->city_filter <> null) {
            $query->andWhere([Buildings::tableName() . '.city' => $this->city_filter]);
        }

        if(isset($this->community) && $this->community <> null) {
            $query->andWhere([Buildings::tableName() . '.community' => $this->community]);
        }
        $query->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
            ->groupBy(Valuation::tableName().'.property_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            //  echo $from_date.'<br>'.$to_date;

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }else{
            $date_array = Yii::$app->appHelperFunctions->getFilterDates(1);
            $from_date = $date_array['start_date'];
            $to_date = $date_array['end_date'];

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }

        /* $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
         if ($this->pageSize<>null) {
             if ($this->pageSize==4) {

                 $dataProvider->pagination->pageSize=false;
             }else {
                 $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
             }

         }*/
        $all_data = $query->all();

        $propert_array= array();


        $residential_unit_array= array();
        $residential_project_array= array();
        $residential_unit_land_array= array();
        $residential_project_land_array= array();

        $commercial_unit_array= array();
        $commercial_project_array= array();
        $commercial_unit_land_array= array();
        $commercial_project_land_array= array();

        $industrial_unit_array= array();
        $industrial_project_array= array();
        $industrial_unit_land_array= array();
        $industrial_project_land_array= array();

        $business_Project_array= array();
        $business_project_land_array= array();
        $other_array= array();

        $all_type_array= array();
        $all_avg_array= array();
        if(!empty($all_data)){
            foreach ($all_data as $key=> $record){


                $propert_array[$record->property_id] = $record;
                if(  $record->property->gross_category == 1){
                    $residential_unit_array[$key]['mv'] = $record->mv_psf;
                    $residential_unit_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $residential_unit_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $residential_unit_array[$key]['gy']  = 0;
                    }
                }

                if(  $record->property->gross_category == 2){
                    $residential_project_array[$key]['mv'] = $record->mv_psf;
                    $residential_project_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $residential_project_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $residential_project_array[$key]['gy']  = 0;
                    }
                }

                if(  $record->property->gross_category == 3){
                    $residential_unit_land_array[$key]['mv'] = $record->mv_psf;
                    $residential_unit_land_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $residential_unit_land_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $residential_unit_land_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 4){
                    $residential_project_land_array[$key]['mv'] = $record->mv_psf;
                    $residential_project_land_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $residential_project_land_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $residential_project_land_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 5){
                    $commercial_unit_array[$key]['mv'] = $record->mv_psf;
                    $commercial_unit_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $commercial_unit_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $commercial_unit_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 6){
                    $commercial_project_array[$key]['mv'] = $record->mv_psf;
                    $commercial_project_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $commercial_project_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $commercial_project_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 7){
                    $commercial_unit_land_array[$key]['mv'] = $record->mv_psf;
                    $commercial_unit_land_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $commercial_unit_land_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $commercial_unit_land_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 8){
                    $commercial_project_land_array[$key]['mv'] = $record->mv_psf;
                    $commercial_project_land_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $commercial_project_land_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $commercial_project_land_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 9){
                    $industrial_unit_array[$key]['mv'] = $record->mv_psf;
                    $industrial_unit_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $industrial_unit_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $industrial_unit_array[$key]['gy']  = 0;
                    }
                }

                if(  $record->property->gross_category == 10){
                    $industrial_project_array[$key]['mv'] = $record->mv_psf;
                    $industrial_project_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $industrial_project_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $industrial_project_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 11){
                    $industrial_unit_land_array[$key]['mv'] = $record->mv_psf;
                    $industrial_unit_land_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $industrial_unit_land_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $industrial_unit_land_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 12){
                    $industrial_project_land_array[$key]['mv'] = $record->mv_psf;
                    $industrial_project_land_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $industrial_project_land_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $industrial_project_land_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 13){
                    $business_Project_array[$key]['mv'] = $record->mv_psf;
                    $business_Project_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $business_Project_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $business_Project_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 14){
                    $business_project_land_array[$key]['mv'] = $record->mv_psf;
                    $business_project_land_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $business_project_land_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $business_project_land_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 15){
                    $other_array[$key]['mv'] = $record->mv_psf;
                    $other_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $other_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $other_array[$key]['gy']  = 0;
                    }
                }
            }

            $all_type_array[1]=$residential_unit_array;
            $all_type_array[2]=$residential_project_array;
            $all_type_array[3]=$residential_unit_land_array;
            $all_type_array[4]=$residential_project_land_array;
            $all_type_array[5]=$commercial_unit_array;
            $all_type_array[6]=$commercial_project_array;
            $all_type_array[7]=$commercial_unit_land_array;
            $all_type_array[8]=$commercial_project_land_array;
            $all_type_array[9]=$industrial_unit_array;
            $all_type_array[10]=$industrial_project_array;
            $all_type_array[11]=$industrial_unit_land_array;
            $all_type_array[12]=$industrial_project_land_array;
            $all_type_array[13]=$business_Project_array;
            $all_type_array[14]=$business_project_land_array;
            $all_type_array[15]=$other_array;

        }

        if(!empty($all_type_array)){

            foreach ($all_type_array as $item => $type_array){
                $mv=0;
                $rv=0;
                $gy=0;
                foreach ($type_array as $item_sub => $value){

                    $mv = $mv + $value['mv'];
                    $rv = $rv + $value['rv'];

                    if($value['mv'] > 0 && $value['rv'] > 0){
                        $gy= $gy +  number_format((($value['rv']/$value['mv'])* 100),2);
                    }

                }


                $all_avg_array[$item]['mv_sum'] = $mv;
                $all_avg_array[$item]['rv_sum'] = $rv;
                $all_avg_array[$item]['gy_sum'] = $gy;
                if(!empty($type_array) && count($type_array) > 0){
                    $all_avg_array[$item]['mv_avg'] = number_format($all_avg_array[$item]['mv_sum']/count($type_array),2);
                    $all_avg_array[$item]['rv_avg'] = number_format($all_avg_array[$item]['rv_sum']/count($type_array),2);
                    $all_avg_array[$item]['gy_avg'] = number_format($all_avg_array[$item]['gy_sum']/count($type_array),2);
                }else{
                    $all_avg_array[$item]['mv_avg']=0;
                    $all_avg_array[$item]['rv_avg']=0;
                    $all_avg_array[$item]['gy_avg']=0;
                }


            }
        }

        return  array('propert_array'=>$propert_array,'calculation_array'=>$all_avg_array);




        /* $dataProvider->pagination->pageSize=100;

         return $dataProvider;*/
    }

    public function search_gy_properties_compare($params)
    {
        /*  echo $this->city_filter;
          die;*/


        $this->load($params);
        if(isset($this->city_filter) && $this->city_filter <> null){
            /* $query = Valuation::find()
                 ->select([
                     Valuation::tableName().'.property_id',
                     'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                     'mv_psf'=>'AVG('.ValuationApproversData::tableName().'.estimated_market_value_sqf)',
                     'rv_psf'=>'AVG('.ValuationApproversData::tableName().'.estimated_market_rent_sqf)',
                 ])
                 ->innerJoin(Buildings::tableName(),Valuation::tableName().'.building_info='.Buildings::tableName().'.id')
                 ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                 ->where(['valuation_status' => 5])
                 ->andWhere([Buildings::tableName().'.city' => $this->city_filter])
                 ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
                 ->groupBy(Valuation::tableName().'.property_id');

             $dataProvider = new ActiveDataProvider([
                 'query' => $query,
                 //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
             ]);*/
        }else{
            /*  $query = Valuation::find()
                  ->select([
                      Valuation::tableName().'.property_id',
                      'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                      'mv_psf'=>'AVG('.ValuationApproversData::tableName().'.estimated_market_value_sqf)',
                      'rv_psf'=>'AVG('.ValuationApproversData::tableName().'.estimated_market_rent_sqf)',
                  ])
                  ->innerJoin(Buildings::tableName(),Valuation::tableName().'.building_info='.Buildings::tableName().'.id')
                  ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                  ->where(['valuation_status' => 5])
                  //->andWhere([Buildings::tableName().'.city' => $this->city_filter])
                  ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
                  ->groupBy(Valuation::tableName().'.property_id');

              $dataProvider = new ActiveDataProvider([
                  'query' => $query,
                  //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
              ]);*/
        }

        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.property_id',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'mv_psf'=>'AVG('.ValuationApproversData::tableName().'.estimated_market_value_sqf)',
                'rv_psf'=>'AVG('.ValuationApproversData::tableName().'.estimated_market_rent_sqf)',
            ])
            ->innerJoin(Buildings::tableName(),Valuation::tableName().'.building_info='.Buildings::tableName().'.id')
            ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->where(['valuation_status' => 5]);
        if(isset($this->city_filter) && $this->city_filter <> null) {
            $query->andWhere([Buildings::tableName() . '.city' => $this->city_filter]);
        }

        if(isset($this->community) && $this->community <> null) {
            $query->andWhere([Buildings::tableName() . '.community' => $this->community]);
        }
        $query->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
            ->groupBy(Valuation::tableName().'.property_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);


        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period_compare <> null && $this->time_period_compare != 0 ) {



            if($this->time_period_compare == 9){

                if($this->custom_date_btw_compare <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw_compare));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{

                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period_compare);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }
            //  echo '<br>'.$from_date.'y<br>'.$to_date;
            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }else{
            $date_array = Yii::$app->appHelperFunctions->getFilterDates(1);
            $from_date = $date_array['start_date'];
            $to_date = $date_array['end_date'];

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }
        $all_data = $query->all();

        $propert_array= array();


        $residential_unit_array= array();
        $residential_project_array= array();
        $residential_unit_land_array= array();
        $residential_project_land_array= array();

        $commercial_unit_array= array();
        $commercial_project_array= array();
        $commercial_unit_land_array= array();
        $commercial_project_land_array= array();

        $industrial_unit_array= array();
        $industrial_project_array= array();
        $industrial_unit_land_array= array();
        $industrial_project_land_array= array();

        $business_Project_array= array();
        $business_project_land_array= array();
        $other_array= array();

        $all_type_array= array();
        $all_avg_array= array();
        if(!empty($all_data)){
            foreach ($all_data as $key=> $record){


                $propert_array[$record->property_id] = $record;
                if(  $record->property->gross_category == 1){
                    $residential_unit_array[$key]['mv'] = $record->mv_psf;
                    $residential_unit_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $residential_unit_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $residential_unit_array[$key]['gy']  = 0;
                    }
                }

                if(  $record->property->gross_category == 2){
                    $residential_project_array[$key]['mv'] = $record->mv_psf;
                    $residential_project_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $residential_project_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $residential_project_array[$key]['gy']  = 0;
                    }
                }

                if(  $record->property->gross_category == 3){
                    $residential_unit_land_array[$key]['mv'] = $record->mv_psf;
                    $residential_unit_land_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $residential_unit_land_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $residential_unit_land_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 4){
                    $residential_project_land_array[$key]['mv'] = $record->mv_psf;
                    $residential_project_land_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $residential_project_land_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $residential_project_land_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 5){
                    $commercial_unit_array[$key]['mv'] = $record->mv_psf;
                    $commercial_unit_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $commercial_unit_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $commercial_unit_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 6){
                    $commercial_project_array[$key]['mv'] = $record->mv_psf;
                    $commercial_project_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $commercial_project_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $commercial_project_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 7){
                    $commercial_unit_land_array[$key]['mv'] = $record->mv_psf;
                    $commercial_unit_land_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $commercial_unit_land_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $commercial_unit_land_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 8){
                    $commercial_project_land_array[$key]['mv'] = $record->mv_psf;
                    $commercial_project_land_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $commercial_project_land_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $commercial_project_land_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 9){
                    $industrial_unit_array[$key]['mv'] = $record->mv_psf;
                    $industrial_unit_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $industrial_unit_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $industrial_unit_array[$key]['gy']  = 0;
                    }
                }

                if(  $record->property->gross_category == 10){
                    $industrial_project_array[$key]['mv'] = $record->mv_psf;
                    $industrial_project_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $industrial_project_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $industrial_project_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 11){
                    $industrial_unit_land_array[$key]['mv'] = $record->mv_psf;
                    $industrial_unit_land_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $industrial_unit_land_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $industrial_unit_land_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 12){
                    $industrial_project_land_array[$key]['mv'] = $record->mv_psf;
                    $industrial_project_land_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $industrial_project_land_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $industrial_project_land_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 13){
                    $business_Project_array[$key]['mv'] = $record->mv_psf;
                    $business_Project_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $business_Project_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $business_Project_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 14){
                    $business_project_land_array[$key]['mv'] = $record->mv_psf;
                    $business_project_land_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $business_project_land_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $business_project_land_array[$key]['gy']  = 0;
                    }
                }
                if(  $record->property->gross_category == 15){
                    $other_array[$key]['mv'] = $record->mv_psf;
                    $other_array[$key]['rv'] = $record->rv_psf;

                    if($record->rv_psf > 0 && $record->mv_psf > 0){
                        $other_array[$key]['gy']  =  number_format((($record->rv_psf/$record->mv_psf)* 100),2);
                    }else{
                        $other_array[$key]['gy']  = 0;
                    }
                }
            }

            $all_type_array[1]=$residential_unit_array;
            $all_type_array[2]=$residential_project_array;
            $all_type_array[3]=$residential_unit_land_array;
            $all_type_array[4]=$residential_project_land_array;
            $all_type_array[5]=$commercial_unit_array;
            $all_type_array[6]=$commercial_project_array;
            $all_type_array[7]=$commercial_unit_land_array;
            $all_type_array[8]=$commercial_project_land_array;
            $all_type_array[9]=$industrial_unit_array;
            $all_type_array[10]=$industrial_project_array;
            $all_type_array[11]=$industrial_unit_land_array;
            $all_type_array[12]=$industrial_project_land_array;
            $all_type_array[13]=$business_Project_array;
            $all_type_array[14]=$business_project_land_array;
            $all_type_array[15]=$other_array;

        }

        if(!empty($all_type_array)){

            foreach ($all_type_array as $item => $type_array){
                $mv=0;
                $rv=0;
                $gy=0;
                foreach ($type_array as $item_sub => $value){

                    $mv = $mv + $value['mv'];
                    $rv = $rv + $value['rv'];

                    if($value['mv'] > 0 && $value['rv'] > 0){
                        $gy= $gy +  number_format((($value['rv']/$value['mv'])* 100),2);
                    }

                }


                $all_avg_array[$item]['mv_sum'] = $mv;
                $all_avg_array[$item]['rv_sum'] = $rv;
                $all_avg_array[$item]['gy_sum'] = $gy;
                if(!empty($type_array) && count($type_array) > 0){
                    $all_avg_array[$item]['mv_avg'] = number_format($all_avg_array[$item]['mv_sum']/count($type_array),2);
                    $all_avg_array[$item]['rv_avg'] = number_format($all_avg_array[$item]['rv_sum']/count($type_array),2);
                    $all_avg_array[$item]['gy_avg'] = number_format($all_avg_array[$item]['gy_sum']/count($type_array),2);
                }else{
                    $all_avg_array[$item]['mv_avg']=0;
                    $all_avg_array[$item]['rv_avg']=0;
                    $all_avg_array[$item]['gy_avg']=0;
                }


            }
        }


        return  array('propert_array'=>$propert_array,'calculation_array'=>$all_avg_array);
    }


    public function search_gy_properties_18_07_2022($params)
    {
        /*  echo $this->city_filter;
          die;*/


        $this->load($params);
        if(isset($this->city_filter) && $this->city_filter <> null){

        }else{
            $this->city_filter = 3506;
        }
        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.property_id',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'mv_psf'=>'AVG('.ValuationApproversData::tableName().'.estimated_market_value_sqf)',
                'rv_psf'=>'AVG('.ValuationApproversData::tableName().'.estimated_market_rent_sqf)',
            ])
            ->innerJoin(Buildings::tableName(),Valuation::tableName().'.building_info='.Buildings::tableName().'.id')
            ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->where(['valuation_status' => 5])
            ->andWhere([Buildings::tableName().'.city' => $this->city_filter])
            ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
            ->groupBy(Valuation::tableName().'.property_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);



        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
            $from_date = $date_array['start_date'];
            $to_date = $date_array['end_date'];

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }else{
            $date_array = Yii::$app->appHelperFunctions->getFilterDates(1);
            $from_date = $date_array['start_date'];
            $to_date = $date_array['end_date'];

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }

        /* $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
         if ($this->pageSize<>null) {
             if ($this->pageSize==4) {

                 $dataProvider->pagination->pageSize=false;
             }else {
                 $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
             }

         }*/
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }

    public function search_gy_properties_compare_18_07_2022($params)
    {
        /*  echo $this->city_filter;
          die;*/


        $this->load($params);
        if(isset($this->city_filter) && $this->city_filter <> null){

        }else{
            $this->city_filter = 3506;
        }
        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.property_id',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'mv_psf'=>'AVG('.ValuationApproversData::tableName().'.estimated_market_value_sqf)',
                'rv_psf'=>'AVG('.ValuationApproversData::tableName().'.estimated_market_rent_sqf)',
            ])
            ->innerJoin(Buildings::tableName(),Valuation::tableName().'.building_info='.Buildings::tableName().'.id')
            ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->where(['valuation_status' => 5])
            ->andWhere([Buildings::tableName().'.city' => $this->city_filter])
            ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
            ->groupBy(Valuation::tableName().'.property_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);



        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period_compare <> null && $this->time_period_compare != 0 ) {

            $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period_compare);
            $from_date = $date_array['start_date'];
            $to_date = $date_array['end_date'];

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }else{
            $date_array = Yii::$app->appHelperFunctions->getFilterDates(1);
            $from_date = $date_array['start_date'];
            $to_date = $date_array['end_date'];

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
        }

        /* $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
         if ($this->pageSize<>null) {
             if ($this->pageSize==4) {

                 $dataProvider->pagination->pageSize=false;
             }else {
                 $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
             }

         }*/
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }

    public function search_tat($params)
    {
        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                Valuation::tableName().'.reference_number',
                Valuation::tableName().'.instruction_date',
                Valuation::tableName().'.service_officer_name',
                Valuation::tableName().'.building_info',
                Valuation::tableName().'.property_id',
                Valuation::tableName().'.tenure',
                Valuation::tableName().'.created_at',
                Valuation::tableName().'.submission_approver_date',
                'submission_date' => ScheduleInspection::tableName().'.valuation_report_date',
                'inspection_date' => ScheduleInspection::tableName().'.inspection_date',
                'inspection_time' => ScheduleInspection::tableName().'.inspection_time',
                'inspection_officer_name' => ScheduleInspection::tableName().'.inspection_officer',
                Valuation::tableName().'.property_id',
                'approver'=>ValuationApproversData::tableName().'.created_by'
            ])
            ->innerJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->innerJoin(Company::tableName(),Company::tableName().'.id='.Valuation::tableName().'.client_id')
            ->where([Valuation::tableName().'.valuation_status' => 5])
            ->andWhere([Valuation::tableName().'.parent_id' => null])
            ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', Valuation::tableName().'.submission_approver_date', $from_date." 00:00:00", $to_date." 23:59:59",
                // 'between', 'instruction_date', $from_date, $to_date
            ]);
        }else{
            $date_array = Yii::$app->appHelperFunctions->getFilterDates(1);
            $from_date = $date_array['start_date'];
            $to_date = $date_array['end_date'];

            $query->andFilterWhere([
                'between', Valuation::tableName().'.submission_approver_date', $from_date." 00:00:00", $to_date." 23:59:59",
                // 'between', 'instruction_date', $from_date, $to_date
            ]);
        }

        if($this->time_period <> null){
            if($this->property_id<>null){
                $query->andWhere([Valuation::tableName().'.property_id' => $this->property_id]);
            }
            if($this->tenure<>null){
                $query->andWhere([Valuation::tableName().'.tenure' => $this->tenure]);
            }
            if($this->client_type<>null){
                $query->andWhere([Company::tableName().'.client_type' => $this->client_type]);
            }
            if($this->valuation_approach<>null){
                $query->innerJoin(Properties::tableName(),Properties::tableName().'.id='.Valuation::tableName().'.property_id');
                $query->andWhere([Properties::tableName().'.valuation_approach' => $this->valuation_approach]);
            }
        }


        $pagelist=[1=>'20',2=>'50',3=>'1000',4=>'Show All'];
        if ($this->pageSize<>null) {
            if ($this->pageSize==4) {

                $dataProvider->pagination->pageSize=false;
            }else {
                $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
            }

        }
        $dataProvider->pagination->pageSize=1000;

        return $dataProvider;
    }

    public function search_tat_daily($params)
    {
        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                Valuation::tableName().'.reference_number',
                Valuation::tableName().'.instruction_date',
                Valuation::tableName().'.instruction_time',
                Valuation::tableName().'.service_officer_name',
                Valuation::tableName().'.building_info',
                Valuation::tableName().'.property_id',
                Valuation::tableName().'.tenure',
                Valuation::tableName().'.created_at',
                Valuation::tableName().'.submission_approver_date',
                Valuation::tableName().'.inspection_type',
                'submission_date' => ScheduleInspection::tableName().'.valuation_report_date',
                'inspection_date' => ScheduleInspection::tableName().'.inspection_date',
                'inspection_time' => ScheduleInspection::tableName().'.inspection_time',
                'inspection_officer_name' => ScheduleInspection::tableName().'.inspection_officer',
                'inspection_done_date_time' => InspectProperty::tableName().'.inspection_done_date',
                Valuation::tableName().'.property_id',
                'approver'=>ValuationApproversData::tableName().'.created_by'
            ])
            ->leftJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->innerJoin(InspectProperty::tableName(),InspectProperty::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->innerJoin(Company::tableName(),Company::tableName().'.id='.Valuation::tableName().'.client_id')
            ->where([Valuation::tableName().'.valuation_status' => 5])
            ->andWhere([Valuation::tableName().'.parent_id' => null])
            ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {


            $from_date = date('Y-m-d');
            $to_date = date('Y-m-d');

            $query->andFilterWhere([
                'between', Valuation::tableName().'.submission_approver_date', $from_date." 00:00:00", $to_date." 23:59:59",
                // 'between', 'instruction_date', $from_date, $to_date
            ]);
        }elseif($params['time'] == 'pending') {

        }else {
            $from_date = date('Y-m-d');
            $to_date = date('Y-m-d');

            $query->andFilterWhere([
                'between', Valuation::tableName().'.submission_approver_date', $from_date." 00:00:00", $to_date." 23:59:59",
                // 'between', 'instruction_date', $from_date, $to_date
            ]);
        }

        if($this->time_period <> null){
            if($this->property_id<>null){
                $query->andWhere([Valuation::tableName().'.property_id' => $this->property_id]);
            }
            if($this->tenure<>null){
                $query->andWhere([Valuation::tableName().'.tenure' => $this->tenure]);
            }
            if($this->client_type<>null){
                $query->andWhere([Company::tableName().'.client_type' => $this->client_type]);
            }
            if($this->valuation_approach<>null){
                $query->innerJoin(Properties::tableName(),Properties::tableName().'.id='.Valuation::tableName().'.property_id');
                $query->andWhere([Properties::tableName().'.valuation_approach' => $this->valuation_approach]);
            }
        }


        $pagelist=[1=>'20',2=>'50',3=>'1000',4=>'Show All'];
        if ($this->pageSize<>null) {
            if ($this->pageSize==4) {

                $dataProvider->pagination->pageSize=false;
            }else {
                $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
            }

        }
        $dataProvider->pagination->pageSize=1000;

        return $dataProvider;
    }


    public function search_revenue_approvers($params)
    {
        $this->load($params);
        $subquery_inspections = '';
        if($this->time_period <> null && $this->time_period != 0 ) {
            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }


        }else{
            $from_date = '2021-04-28';
            $to_date = date('Y-m-d');
        }

        $ids_to_exclude = [27, 18, 42, 28];
        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                Valuation::tableName().'.service_officer_name',
                ValuationApproversData::tableName().'.created_by',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'client_revenue'=>'SUM('.Valuation::tableName().'.total_fee)',
                'total_inspections'=>'(SELECT COUNT('.ScheduleInspection::tableName().'.id) from schedule_inspection WHERE schedule_inspection.inspection_officer=valuation.service_officer_name AND (schedule_inspection.created_at BETWEEN "'.$from_date.' 00:00:00" AND "'.$to_date.' 23:59:59"))',
                // 'tat'=> 'SUM(DATEDIFF(schedule_inspection.valuation_report_date,schedule_inspection.inspection_date))', // comment by usama
                //'tat'=> 'SUM( TOTAL_WEEKDAYS(schedule_inspection.valuation_report_date, schedule_inspection.inspection_date) )',
                'low_vals'=> 'SUM(CASE 
                                    WHEN valuation.revised_reason = "1" 
                                    THEN 1
                                    ELSE 0 
                              END)',
                'high_vals'=> 'SUM(CASE 
                                    WHEN valuation.revised_reason = 2 
                                    THEN 1 
                                    ELSE 0 
                              END)',
                'errors_vals'=> 'SUM(CASE 
                                    WHEN valuation.revised_reason = 3 
                                    THEN 1
                                    ELSE 0 
                              END)',
            ])
            ->innerJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->where(['valuation_status' => 5])
            ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
            ->andWhere(['NOT IN', ValuationApproversData::tableName().'.created_by', $ids_to_exclude])
            ->orderBy([
                'client_revenue' => SORT_DESC
            ])
            // ->groupBy(Valuation::tableName().'.service_officer_name');
            ->groupBy(ValuationApproversData::tableName().'.created_by');

        // dd($query);
        //join with val approver data (schedule inspection ki jaga) and group by arrpover data created by baki sab same

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);



        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {
            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            /* $query->andFilterWhere([
                 'between', 'instruction_date', $from_date, $to_date
             ]);*/
            $query->andFilterWhere([ 'between', 'submission_approver_date', $from_date. ' 00:00:00', $to_date. ' 23:59:59' ]);
        }
        /* $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
         if ($this->pageSize<>null) {
             if ($this->pageSize==4) {

                 $dataProvider->pagination->pageSize=false;
             }else {
                 $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
             }

         }*/
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }

    public function search_operations_data($params)
    {



        $this->load($params);
        $subquery_inspections = '';
        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }


        }else{
            $from_date = '2021-04-28';
            $to_date = date('Y-m-d');
        }

        $query = Valuation::find()
            ->select([
                Valuation::tableName().'.created_by',
                'total_valuations'=>'Count('.Valuation::tableName().'.id)',
                'client_revenue'=>'SUM('.Valuation::tableName().'.total_fee)',
            ])
            ->where(['valuation_status' => 5])
            ->andwhere(['not', [Valuation::tableName().'.created_by' => null]])
            ->andwhere(['not in','valuation.id', ['11920']])
            ->andwhere(['not in',Valuation::tableName().'.created_by', [28,14,27,25,26,9,40,43,42,11,31,21,41,37,39,19,206]])
            ->andWhere(['valuation.parent_id' => null])
            ->andWhere(['valuation.quotation_id' => null])
            ->andWhere(['valuation.client_module_id' => null])
            ->orderBy([
                'client_revenue' => SORT_DESC
            ])
            ->groupBy(Valuation::tableName().'.created_by');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);



        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'submission_approver_date', $from_date. ' 00:00:00', $to_date. ' 23:59:59'
            ]);
        }
        /* $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
         if ($this->pageSize<>null) {
             if ($this->pageSize==4) {

                 $dataProvider->pagination->pageSize=false;
             }else {
                 $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
             }

         }*/
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }

    public function search_data_team($params)
    {



        $this->load($params);
        $subquery_inspections = '';
        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }


        }else{
            $from_date = '2021-04-28';
            $to_date = date('Y-m-d');
        }
        $data_team_members = User::find()
            ->select(['id'])
            ->where(['permission_group_id' => 38])
            ->column();
        $data_team_members[]=6327;
        $data_team_members[]=111706;
        $data_team_members[]=111698;
        $query = History::find()
            ->select([
                History::tableName().'.created_by',
                'total_listings'=>'Count('.History::tableName().'.model_id)',
            ])
           // ->where(['action' => 'data_updated'])
            ->where(['or',
                ['action' => 'data_created'],
                ['action' => 'data_updated']
            ])

            ->andWhere(['model_name' => 'app\models\ListingsTransactions'])
            ->andwhere(['in',History::tableName().'.created_by', $data_team_members])
            ->distinct()
            ->groupBy(History::tableName().'.created_by');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);



        if (!$this->validate()) {
            return $dataProvider;
        }

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', 'created_at', $from_date. ' 00:00:00', $to_date. ' 23:59:59'
            ]);
        }
        /* $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
         if ($this->pageSize<>null) {
             if ($this->pageSize==4) {

                 $dataProvider->pagination->pageSize=false;
             }else {
                 $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
             }

         }*/
        $dataProvider->pagination->pageSize=100;

        return $dataProvider;
    }}