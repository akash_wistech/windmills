<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "re_towers_data".
 *
 * @property int $id
 * @property string|null $title
 * @property float|null $gfa
 * @property float|null $bua
 * @property string|null $floor_details
 * @property string|null $makani_number
 * @property string|null $unit_number
 * @property int $valuation_id
 * @property int $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class ReTowersData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 're_towers_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['gfa', 'bua'], 'number'],
            [['valuation_id'], 'required'],
            [['valuation_id', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['created_at', 'updated_at', 'trashed_at'], 'safe'],
            [['title', 'floor_details', 'makani_number', 'unit_number'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'gfa' => 'Gfa',
            'bua' => 'Bua',
            'floor_details' => 'Floor Details',
            'makani_number' => 'Makani Number',
            'unit_number' => 'Unit Number',
            'valuation_id' => 'Valuation ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }
}
