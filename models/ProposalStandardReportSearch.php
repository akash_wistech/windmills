<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProposalStandardReport;

/**
 * ProposalStandardReportSearch represents the model behind the search form of `app\models\ProposalStandardReport`.
 */
class ProposalStandardReportSearch extends ProposalStandardReport
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['payment_method', 'timings', 'rics_valuation_standards_departures', 'valuer_duties_supervision', 'internal_external_status_of_the_valuer', 'previous_involvement_and_conflict_of_interest', 'decleration_of_independence_and_objectivity', 'report_handover', 'currency', 'basis_of_value', 'market_value', 'statutory_definition_of_market_value', 'market_rent', 'investment_value', 'fair_value', 'valuation_date', 'property', 'assumptions_ext_of_investi_limi_scope_of_work', 'physical_inspection', 'desktop_or_driven_by_valuation', 'structural_and_technical_survey', 'conditions_state_repair_maintenance_property', 'contamination_ground_environmental_consideration', 'statutory_and_regulatory_requirements', 'title_tenancies_and_property_document', 'planning_and_highway_enquiries', 'plant_and_equipment', 'development_properties', 'disposal_costs_and_liabilities', 'insurance_reinstalement_cost', 'nature_source_information_documents_relied_upon', 'description_of_report', 'client_acceptance_of_the_valuation_report', 'restrictions_on_publications', 'third_party_liability', 'valuation_uncertaninty', 'complaints', 'rics_monitoring', 'rera_monitoring', 'limitions_on_liabity', 'liability_and_duty_of_care', 'extent_of_fee', 'indemnification', 'professional_indemmnity_insurance', 'clients_obligations', 'proposal_validity', 'jurisdiction', 'acceptance_of_terms_of_engagement'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProposalStandardReport::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'payment_method', $this->payment_method])
            ->andFilterWhere(['like', 'timings', $this->timings])
            ->andFilterWhere(['like', 'rics_valuation_standards_departures', $this->rics_valuation_standards_departures])
            ->andFilterWhere(['like', 'valuer_duties_supervision', $this->valuer_duties_supervision])
            ->andFilterWhere(['like', 'internal_external_status_of_the_valuer', $this->internal_external_status_of_the_valuer])
            ->andFilterWhere(['like', 'previous_involvement_and_conflict_of_interest', $this->previous_involvement_and_conflict_of_interest])
            ->andFilterWhere(['like', 'decleration_of_independence_and_objectivity', $this->decleration_of_independence_and_objectivity])
            ->andFilterWhere(['like', 'report_handover', $this->report_handover])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'basis_of_value', $this->basis_of_value])
            ->andFilterWhere(['like', 'market_value', $this->market_value])
            ->andFilterWhere(['like', 'statutory_definition_of_market_value', $this->statutory_definition_of_market_value])
            ->andFilterWhere(['like', 'market_rent', $this->market_rent])
            ->andFilterWhere(['like', 'investment_value', $this->investment_value])
            ->andFilterWhere(['like', 'fair_value', $this->fair_value])
            ->andFilterWhere(['like', 'valuation_date', $this->valuation_date])
            ->andFilterWhere(['like', 'property', $this->property])
            ->andFilterWhere(['like', 'assumptions_ext_of_investi_limi_scope_of_work', $this->assumptions_ext_of_investi_limi_scope_of_work])
            ->andFilterWhere(['like', 'physical_inspection', $this->physical_inspection])
            ->andFilterWhere(['like', 'desktop_or_driven_by_valuation', $this->desktop_or_driven_by_valuation])
            ->andFilterWhere(['like', 'structural_and_technical_survey', $this->structural_and_technical_survey])
            ->andFilterWhere(['like', 'conditions_state_repair_maintenance_property', $this->conditions_state_repair_maintenance_property])
            ->andFilterWhere(['like', 'contamination_ground_environmental_consideration', $this->contamination_ground_environmental_consideration])
            ->andFilterWhere(['like', 'statutory_and_regulatory_requirements', $this->statutory_and_regulatory_requirements])
            ->andFilterWhere(['like', 'title_tenancies_and_property_document', $this->title_tenancies_and_property_document])
            ->andFilterWhere(['like', 'planning_and_highway_enquiries', $this->planning_and_highway_enquiries])
            ->andFilterWhere(['like', 'plant_and_equipment', $this->plant_and_equipment])
            ->andFilterWhere(['like', 'development_properties', $this->development_properties])
            ->andFilterWhere(['like', 'disposal_costs_and_liabilities', $this->disposal_costs_and_liabilities])
            ->andFilterWhere(['like', 'insurance_reinstalement_cost', $this->insurance_reinstalement_cost])
            ->andFilterWhere(['like', 'nature_source_information_documents_relied_upon', $this->nature_source_information_documents_relied_upon])
            ->andFilterWhere(['like', 'description_of_report', $this->description_of_report])
            ->andFilterWhere(['like', 'client_acceptance_of_the_valuation_report', $this->client_acceptance_of_the_valuation_report])
            ->andFilterWhere(['like', 'restrictions_on_publications', $this->restrictions_on_publications])
            ->andFilterWhere(['like', 'third_party_liability', $this->third_party_liability])
            ->andFilterWhere(['like', 'valuation_uncertaninty', $this->valuation_uncertaninty])
            ->andFilterWhere(['like', 'complaints', $this->complaints])
            ->andFilterWhere(['like', 'rics_monitoring', $this->rics_monitoring])
            ->andFilterWhere(['like', 'rera_monitoring', $this->rera_monitoring])
            ->andFilterWhere(['like', 'limitions_on_liabity', $this->limitions_on_liabity])
            ->andFilterWhere(['like', 'liability_and_duty_of_care', $this->liability_and_duty_of_care])
            ->andFilterWhere(['like', 'extent_of_fee', $this->extent_of_fee])
            ->andFilterWhere(['like', 'indemnification', $this->indemnification])
            ->andFilterWhere(['like', 'professional_indemmnity_insurance', $this->professional_indemmnity_insurance])
            ->andFilterWhere(['like', 'clients_obligations', $this->clients_obligations])
            ->andFilterWhere(['like', 'proposal_validity', $this->proposal_validity])
            ->andFilterWhere(['like', 'jurisdiction', $this->jurisdiction])
            ->andFilterWhere(['like', 'acceptance_of_terms_of_engagement', $this->acceptance_of_terms_of_engagement]);

        return $dataProvider;
    }
}
