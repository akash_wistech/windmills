<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "valuation_agents".
 *
 * @property int $id
 * @property string $name
 * @property string|null $lastname
 * @property string $percentage
 * @property int $index_id
 * @property int $valuation_id
 * @property int $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class ValuationAgents extends ActiveRecordFull
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'valuation_agents';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'percentage', 'valuation_id'], 'required'],
            [['index_id', 'valuation_id', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['created_at', 'updated_at', 'trashed_at', 'index_id','prefix'], 'safe'],
            [['name', 'lastname', 'percentage'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'lastname' => 'Lastname',
            'percentage' => 'Percentage',
            'index_id' => 'Index ID',
            'valuation_id' => 'Valuation ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }
    public function getRecType()
    {
        return 'Valuation Agents';
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->id;
    }
}
