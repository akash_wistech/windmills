<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * SoldTransactionImportForm is the model behind the sold transaction import form.
 */
class PreviousTransactionsImportForm extends Model
{
    public $importfile;
    public $allowedFileTypes = ['csv'];//'xls', 'xlsx',

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['importfile'], 'safe'],
            [['importfile'], 'file', 'extensions' => implode(",", $this->allowedFileTypes)]
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'importfile' => Yii::t('app', 'File'),
        ];
    }

    /**
     * import the entries into table
     */
    public function save()
    {
        $saved = 0;
        $unsaved = 0;
        $errNames = '';
        $change = array();
        $match = array();
        if ($this->validate()) {
            if (UploadedFile::getInstance($this, 'importfile')) {
                $importedFile = UploadedFile::getInstance($this, 'importfile');
                // if no image was uploaded abort the upload
                if (!empty($importedFile)) {
                    $pInfo = pathinfo($importedFile->name);
                    $ext = $pInfo['extension'];
                    if (in_array($ext, $this->allowedFileTypes)) {
                        // Check to see if any PHP files are trying to be uploaded
                        $content = file_get_contents($importedFile->tempName);
                        if (preg_match('/\<\?php/i', $content)) {

                        } else {
                            $this->importfile = Yii::$app->params['temp_abs_path'] . $importedFile->name;
                            $importedFile->saveAs($this->importfile);

                            $csvFile = fopen($this->importfile, 'r');
                            $data = [];
                            $n = 1;
                            while (($line = fgetcsv($csvFile)) !== FALSE) {

                                if ($n > 1) {
                                    $buildingName = trim($line[21]);
                                    $instruction_date = trim($line[0]);
                                    if($instruction_date <> null) {
                                        $instruction_date = str_replace('/', '-', trim($line[0]));
                                        $instruction_date = date("Y-m-d", strtotime($instruction_date));
                                    }else{
                                        $instruction_date = '0000-00-00';
                                    }

                                    $inspection_date = trim($line[1]);
                                    if($inspection_date <> null) {
                                        $inspection_date = str_replace('/', '-', trim($line[1]));
                                        $inspection_date = date("Y-m-d", strtotime($inspection_date));
                                    }else{
                                        $inspection_date = '0000-00-00';
                                    }
                                    $target_date = trim($line[5]);
                                    if($target_date <> null) {
                                        $target_date = str_replace('/', '-', trim($line[5]));
                                        $target_date = date("Y-m-d", strtotime($target_date));
                                    }else{
                                        $target_date = '0000-00-00';
                                    }
                                    $due_date = trim($line[6]);
                                    if($due_date <> null) {
                                        $due_date = str_replace('/', '-', trim($line[6]));
                                        $due_date = date("Y-m-d", strtotime($due_date));
                                    }else{
                                        $due_date = '0000-00-00';
                                    }
                                    $date_submitted = trim($line[7]);
                                    if($date_submitted <> null) {
                                        $date_submitted = str_replace('/', '-', trim($line[7]));
                                        $date_submitted = date("Y-m-d", strtotime($date_submitted));
                                    }else{
                                        $date_submitted = '0000-00-00';
                                    }

                                   // $instruction_date= date("Y-m-d",strtotime(trim($line[0])));
                                   // $inspection_date= date("Y-m-d",strtotime(trim($line[1])));;
                                   // $target_date = date("Y-m-d",strtotime(trim($line[5])));
                                   // $due_date = date("Y-m-d",strtotime(trim($line[6])));
                                  //  $date_submitted = date("Y-m-d",strtotime(trim($line[7])));
                                    $property_category = array_search(trim($line[16]), Yii::$app->appHelperFunctions->propertiesCategoriesListArr);
                                    $property = Properties::find()->where(['like', 'title', trim($line[17])])->asArray()->one();
                                    if ($property <> null) {
                                    } else {
                                        $property_new = new Properties();
                                        $property_new->title = trim($line[17]);
                                        $property_new->category = 1;
                                        $property_new->valuation_approach = 1;
                                        $property_new->save();
                                        $property = $property_new;
                                    }
                                    $tenure = array_search(trim($line[18]), Yii::$app->appHelperFunctions->buildingTenureArr);
                                    $valuation_type = array_search(trim($line[25]), Yii::$app->appHelperFunctions->typesOfValuationArr);
                                    $valuation_method = array_search(trim($line[26]), Yii::$app->appHelperFunctions->methodsOfValuationArr);
                                  //  $location = array_search(trim($line[29]), Yii::$app->appHelperFunctions->locationConditionsListArr);
                                  //  $view = array_search(trim($line[30]), Yii::$app->appHelperFunctions->otherPropertyViewListArr);
                                    $property_condition = array_search(trim($line[29]), Yii::$app->appHelperFunctions->propertyConditionListArr);
                                    $property_placement = array_search(trim($line[30]), Yii::$app->appHelperFunctions->propertyPlacementListArr);
                                    $property_exposure = array_search(trim($line[32]), Yii::$app->appHelperFunctions->propertyExposureListArr);
                                    $no_of_bedrooms = str_replace(".00","",trim($line[34]));
                                    $no_of_bedrooms = trim($no_of_bedrooms);
                                    $purpose_of_valuation = array_search(trim($line[43]), Yii::$app->appHelperFunctions->purposeOfValuationArr);




                                    //$building_name = str_replace( 'Ê', '', trim($line[21]));
                                    //$building_name = str_replace( 'Ð', '', trim($line[21]));

                                    $buildingRow = Buildings::find()->where(['=', 'title', trim($line[21])])->asArray()->one();
                                    $fee = str_replace( ',', '', trim($line[4]));

                                    if (!empty($buildingRow) && $buildingRow <> null) {



                                        $model = new PreviousTransactions();
                                        $model->instruction_date = $instruction_date;
                                        $model->inspection_date = $inspection_date;
                                        $model->inspector = (trim($line[2]) <> null) ? trim($line[2]) : '';
                                        $model->approver = (trim($line[3]) <> null) ? trim($line[3]) : '';
                                        $model->fee = ($fee <> null) ? $fee : 0;
                                        $model->target_date = ($target_date <> null) ? $target_date : '';
                                        $model->due_date = ($due_date <> null) ? $due_date : '';
                                        $model->date_submitted =($date_submitted <> null) ? $date_submitted : '';
                                        $model->reference_number = (trim($line[8]) <> null) ? trim($line[8]) : '';
                                        $model->client_reference = (trim($line[9]) <> null) ? trim($line[9]) : '';
                                        $model->client_name = (trim($line[10]) <> null) ? trim($line[10]) : '';
                                        $model->client_type = (trim($line[11]) <> null) ? trim($line[11]) : '';
                                        $model->client_customer_name = (trim($line[12]) <> null) ? trim($line[12]) : '';
                                        $model->contact_person = (trim($line[13]) <> null) ? trim($line[13]) : '';
                                        $model->designation = (trim($line[14]) <> null) ? trim($line[14]) : '';
                                        $model->contact_number = (trim($line[15]) <> null) ? trim($line[15]) : '';
                                        $model->property_category = ($property_category <> null) ? $property_category : 0;
                                        $model->property_id = ($property <> null) ? $property['id'] : 0;
                                        $model->tenure = ($tenure <> null) ? $tenure : 0;
                                        $model->plot_number = (trim($line[19]) <> null) ? trim($line[19]) : '';
                                        $model->unit_number = (trim($line[20]) <> null) ? trim($line[20]) : '';
                                        $model->building_info = $buildingRow['id'];
                                        $model->valuation_type = ($valuation_type <> null) ? $valuation_type : 0;
                                        $model->valuation_method =($valuation_method <> null) ? $valuation_method : 0;
                                        $model->valuer = (trim($line[27]) <> null) ? trim($line[27]) : '';
                                        $model->taqyimee_number = (trim($line[28]) <> null) ? trim($line[28]) : 0;
                                       // $model->location = ($location <> null) ? $location : 0;
                                      //  $model->view = ($view <> null) ? $view : 0;
                                        $model->property_condition = ($property_condition <> null) ? $property_condition : 0;
                                        $model->property_placement = ($property_placement <> null) ? $property_placement : 0;
                                        $model->finished_status = (trim($line[31]) <> null) ? trim($line[31]) : '';
                                        $model->property_exposure =  ($property_exposure <> null) ? $property_exposure : 0;;
                                        $floor_number = str_replace( '.00', '', trim($line[33]));
                                        $model->floor_number = ($floor_number <> null && $floor_number != '-')? $floor_number: 0 ;
                                        $model->no_of_bedrooms = ($no_of_bedrooms <> null) ? $no_of_bedrooms : 0;
                                        $landsize = str_replace( ',', '', trim($line[35]));
                                        $model->land_size = ($landsize <> null && $landsize != '-') ? $landsize : 0 ;
                                        $market_value_of_land =str_replace( ',', '', trim($line[36]));
                                        $model->market_value_of_land =($market_value_of_land <> null && $market_value_of_land != '-') ? $market_value_of_land : 0 ;
                                        $built_up_area = str_replace( ',', '', $line[37]);
                                        $model->built_up_area = ($built_up_area <> null && $built_up_area != '-') ? $built_up_area : 0 ;
                                        $model->estimated_age = (trim($line[38]) <> null && trim($line[38]) != '-')?trim($line[38]): 0 ;;
                                        $transaction_price = str_replace( ',', '', $line[39]);
                                        $model->transaction_price = ($transaction_price <> null && $transaction_price != '-') ? $transaction_price : 0 ;
                                        $market_value = str_replace( ',', '', $line[41]);
                                        $model->market_value = ($market_value <> null)? $market_value: 0 ;
                                        $psf = str_replace( ',', '', $line[42]);
                                        $model->psf = ($psf <> null && $psf != '-')? $psf : 0 ;
                                        $model->purpose_of_valuation = ($purpose_of_valuation <> null)? $purpose_of_valuation: 0 ;
                                        $model->location_highway_drive = ($line[44] <> null) ? str_replace(' ','_',trim($line[44])) : '';
                                        $model->location_school_drive = ($line[45] <> null) ? str_replace(' ','_',trim($line[45])) : '';
                                        $model->location_mall_drive = ($line[46] <> null) ? str_replace(' ','_',trim($line[46])) : '';
                                        $model->location_sea_drive = ($line[47] <> null) ? str_replace(' ','_',trim($line[47])) : '';
                                        $model->location_park_drive = ($line[48] <> null) ? str_replace(' ','_',trim($line[48])) : '';
                                        $model->view_community = ($line[49] <> null) ? strtolower(trim($line[49])) : '';
                                        $model->view_pool = ($line[50] <> null) ? strtolower(trim($line[50])) : '';
                                        $model->view_park = ($line[51] <> null) ? strtolower(trim($line[51])) : '';
                                        $model->view_special = ($line[52] <> null) ? strtolower(trim($line[52])) : '';
                                        $model->view_burj = ($line[53] <> null) ? strtolower(trim($line[53])) : '';
                                        $model->view_sea = ($line[54] <> null) ? strtolower(trim($line[54])) : '';
                                        $model->view_marina = ($line[55] <> null) ? strtolower(trim($line[55])) : '';
                                        $model->view_mountains = ($line[56] <> null) ? strtolower(trim($line[56])) : '';
                                        $model->view_lake = ($line[57] <> null) ? strtolower(trim($line[57])) : '';
                                        $model->view_golf_course = ($line[58] <> null) ? strtolower(trim($line[58])) : '';
                                        $model->view_park = ($line[59] <> null) ? strtolower(trim($line[59])) : '';
                                        $model->view_special = ($line[60] <> null) ? strtolower(trim($line[60])) : '';


                                        if ($model->save()) {
                                            $saved++;
                                        } else {
                                            if ($model->hasErrors()) {

                                                foreach ($model->getErrors() as $error) {
                                                    if (count($error) > 0) {
                                                        foreach ($error as $key => $val) {
                                                            echo $val;
                                                        }
                                                    }
                                                }
                                                echo $n."<pre>";
                                                print_r($line);
                                                print_r($model);
                                                die;
                                                die();
                                                $errNames .= '<br />' . $buildingName;
                                            }

                                            $unsaved++;
                                        }
                                    }else{
                                        $change[] = $buildingName;
                                       /* if($buildingName == 'Al Sajaa Industrial land'){
                                            echo "<pre>";
                                            print_r($line);
                                            die;

                                        }*/
                                        $buildingRow = Buildings::find()->where(['like', 'title', trim($line[21])])->asArray()->one();
                                        $match[] = $buildingRow['title'];
                                    }


                                } else {
                                   // $change[] = $buildingName;

                                }
                                $n++;
                            }
                            echo "<pre>";
                            print_r($change);
                            print_r($match);
                            die('kk');
                            fclose($csvFile);
                            //Unlink File
                            unlink($this->importfile);
                        }
                    }
                }
            }
            if ($saved > 0) {

                Yii::$app->getSession()->setFlash('success', $saved . ' - ' . Yii::t('app', 'rows saved successfully'));
            }
            if ($unsaved > 0) {
                Yii::$app->getSession()->setFlash('error', $unsaved . ' - ' . Yii::t('app', 'rows were not saved!') . $errNames);
            }
            return true;
        } else {
            return false;
        }
    }
}
