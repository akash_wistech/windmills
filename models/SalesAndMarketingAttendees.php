<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sales_and_marketing_attendees".
 *
 * @property int $id
 * @property int|null $sales_and_marketing_id
 * @property int|null $sales_and_marketing_attendees_id
 */
class SalesAndMarketingAttendees extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sales_and_marketing_attendees';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sales_and_marketing_id', 'sales_and_marketing_attendees_id'], 'integer'],
            [['client_attendee_type', 'client_attendee_title', 'organization'], 'safe'],
        ];
    }


    public function getAttendee()
    {
        return $this->hasOne(User::class, ['id' => 'sales_and_marketing_attendees_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sales_and_marketing_id' => 'Sales And Marketing ID',
            'sales_and_marketing_attendees_id' => 'Sales And Marketing Attendees ID',
        ];
    }
}
