<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "propertyfinder_city_cron_logs".
 *
 * @property int $id
 * @property int|null $city_id
 * @property string|null $purpose
 * @property string|null $date
 * @property string|null $status
 * @property string|null $action
 */
class PropertyfinderCityCronLogs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'propertyfinder_city_cron_logs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id'], 'integer'],
            [['date'], 'safe'],
            [['purpose', 'status', 'action'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'purpose' => 'Purpose',
            'date' => 'Date',
            'status' => 'Status',
            'action' => 'Action',
        ];
    }
}
