<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "crm_received_docs".
 *
 * @property int $id
 * @property int $valuation_id
 */
class CrmReceivedDocs extends \yii\db\ActiveRecord
{
    public $owners_data = [];
    public $received_docs = [];

    public $sendDocsEmail = true;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_received_docs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quotation_id'], 'required'],
            [['quotation_id','property_index'], 'integer'],
            [['quotation_id','property_index','owners_data','received_docs'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'valuation_id' => 'Valuation ID',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        $crm_qt_data = \app\models\CrmQuotations::find()->where(['id'=>$this->quotation_id])->one();
        $last_property_index = $crm_qt_data->no_of_properties - 1;
        if (isset($this->owners_data) && $this->owners_data <> null) {
            CrmQuotationOwner::deleteAll(['quotation_id' => $this->quotation_id,'property_index' => $this->property_index]);

            // Save all payments terms
            foreach ($this->owners_data as $owner_data) {
                $owner_data_detail = new CrmQuotationOwner();
                $owner_data_detail->name = $owner_data['name'];
                $owner_data_detail->percentage = $owner_data['percentage'];
                $owner_data_detail->index_id = $owner_data['index_id'];
                $owner_data_detail->quotation_id = $owner_data['quotation_id'];
                $owner_data_detail->property_index = $this->property_index;
               $owner_data_detail->save();
            }
        }

        $empty_required = array();
        $empty_helpful = array();
        if (isset($this->received_docs) && $this->received_docs <> null) {


            CrmReceivedDocsFiles::deleteAll(['quotation_id' => $this->quotation_id,'property_index' => $this->property_index]);


            // Save all payments terms
            foreach ($this->received_docs as $received_doc) {
                if($received_doc['attachment'] <> null){

                }else{
                    if($received_doc['needed'] == 0) {
                        $empty_required[] = Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$received_doc['document_id']];
                    }else{
                        $empty_helpful[] = Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$received_doc['document_id']];
                    }
                }
                $owner_data_detail = new CrmReceivedDocsFiles();
                $owner_data_detail->document_id = $received_doc['document_id'];
                $owner_data_detail->attachment = $received_doc['attachment'];
                $owner_data_detail->quotation_id = $received_doc['quotation_id'];
                $owner_data_detail->property_index = $this->property_index;
                $owner_data_detail->needed =$received_doc['needed'] ;
                $owner_data_detail->save();
            }
        }
        //required docs email

        if($last_property_index == $this->property_index && $crm_qt_data->email_status_docs !=1 && $this->sendDocsEmail == true){

            $table ='';
            $table .='<table style="width:60%">';
            $table .='<tr>';
            $table .='<th style="text-align: left;">Serial #</th>';
            $table .='<th style="text-align: left;">Property</th>';
            $table .='<th style="text-align: left;">Documents</th>';
            $table .='<th style="text-align: left;">Received Status</th>';
            $table .='<th style="text-align: left;">Mandatory Status</th>';
            $table .='</tr>';
            $files = CrmReceivedDocsFiles::find()->where(['quotation_id' => $this->quotation_id])->all();

            $properties = CrmReceivedProperties::find()->where(['quotation_id' => $this->quotation_id])->all();;
            $counter =-1;
            foreach ($files as $file){
                if($crm_qt_data->client->client_type == 'bank'){
                    if($file->document_id != 1) {
                        $property_details = CrmReceivedProperties::find()->where(['quotation_id' => $this->quotation_id, 'property_index' => $file->property_index])->one();
                        if ($counter == $file->property_index) {
                            $serial_number = '';
                            $property_detail = '';
                        } else {
                            $serial_number = ($file->property_index + 1);
                            $property_detail = $property_details->property->title . '(' . $property_details->building->communities->title . ')';
                        }

                        if ($file->attachment <> null) {
                            $document_received_status = "Received";
                        } else {
                            $document_received_status = "Not Received";
                        }

                        if ($file->needed == 0) {
                            $document_required_status = "Mandatory";
                        } else {
                            $document_required_status = "Helpful";
                        }

                        $table .= '<tr>';
                        $table .= '<td>' . $serial_number . '</td>';
                        $table .= '<td>' . $property_detail . '</td>';
                        $table .= '<td>' . Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$file->document_id] . '</td>';
                        $table .= '<td>' . $document_received_status . '</td>';
                        $table .= '<td>' . $document_required_status . '</td>';
                        $table .= '</tr>';
                        $counter = $file->property_index;
                    }
                }else{

                        $property_details = CrmReceivedProperties::find()->where(['quotation_id' => $this->quotation_id, 'property_index' => $file->property_index])->one();
                        if ($counter == $file->property_index) {
                            $serial_number = '';
                            $property_detail = '';
                        } else {
                            $serial_number = ($file->property_index + 1);
                            $property_detail = $property_details->property->title . '(' . $property_details->building->communities->title . ')';
                        }

                        if ($file->attachment <> null) {
                            $document_received_status = "Received";
                        } else {
                            $document_received_status = "Not Received";
                        }

                        if ($file->needed == 0) {
                            $document_required_status = "Mandatory";
                        } else {
                            $document_required_status = "Helpful";
                        }

                        $table .= '<tr>';
                        $table .= '<td>' . $serial_number . '</td>';
                        $table .= '<td>' . $property_detail . '</td>';
                        $table .= '<td>' . Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$file->document_id] . '</td>';
                        $table .= '<td>' . $document_received_status . '</td>';
                        $table .= '<td>' . $document_required_status . '</td>';
                        $table .= '</tr>';
                        $counter = $file->property_index;

                }

            }
            $table .='</table>';

            if($crm_qt_data->email_status_docs !=1 && $this->sendDocsEmail == true) {

                $notifyData = [
                    'client' => $crm_qt_data->client,
                    'attachments' => [],
                    'subject' => $crm_qt_data->email_subject,
                    'uid' => 'crm'.$crm_qt_data->id,
                    'instructing_person_email' => $crm_qt_data->instructing_party_email,
                    'replacements' => [
                        '{documentsdata}' => $table,
                    ],
                ];
                if($crm_qt_data->client_name > 0 &&  $crm_qt_data->email_subject <> null) {
                    if($crm_qt_data->client_name != 112 && $crm_qt_data->client_name != 118945 && $crm_qt_data->client_name != 54514 && $crm_qt_data->client_name != 120186) {
                        \app\modules\wisnotify\listners\NotifyEvent::fire23('quotation.docs', $notifyData);
                    }
                    Yii::$app->db->createCommand()->update('crm_quotations', ['email_status_docs' => 1], 'id=' . $this->quotation_id . '')->execute();
                }
            }

        }

        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
}
