<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "valuation_detail".
 *
 * @property int $id
 * @property int|null $building_info
 * @property int|null $property_id
 * @property int|null $property_category
 * @property int|null $community
 * @property int|null $sub_community
 * @property int $tenure
 * @property string|null $unit_number
 * @property int|null $city
 * @property int|null $country
 * @property string|null $payment_plan
 * @property int $status
 * @property string $building_number
 * @property string|null $plot_number
 * @property string $street
 * @property int $floor_number
 * @property float|null $land_size
 * @property int|null $created_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 * @property int|null $no_of_towers
 * @property int|null $plot_area_source
 * @property int|null $bua_source
 * @property int|null $parking_source
 * @property int|null $parking_floors
 * @property int|null $status_verified
 * @property int|null $status_verified_by
 * @property string|null $status_verified_at
 */
class ValuationDetail extends ActiveRecordFull
{
    public $title_deed;
    public $green_efficient_certification;
    public $certifier_name;
    public $certification_level;
    public $source_of_green_certificate_information	;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'valuation_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['building_info', 'property_id', 'property_category', 'tenure',
                'status', 'floor_number', 'created_by', 'updated_by', 'trashed', 'trashed_by',
                'no_of_towers', 'plot_source', 'bua_source', 'nla_source', 'parking_source', 'parking_floors',
                'status_verified', 'status_verified_by'], 'integer'],
            [['building_info', 'property_id', 'property_category', 'tenure'], 'required'],
            [['no_of_towers', 'plot_source', 'bua_source', 'nla_source', 'parking_source', 'parking_floors','property_purpose','title_deed','latitude', 'longitude','location_pin',
                'arrived_date_time','extended','completion_status','green_efficient_certification','certifier_name','certification_level','source_of_green_certificate_information'], 'safe'],
            [['plot_number', 'street','building_number'], 'required'],
            [['payment_plan'], 'string'],
            [['land_size','built_up_area'], 'number'],
            [['created_at', 'updated_at', 'trashed_at', 'status_verified_at','valuation_id','country','bedrooms','floor_number', 'unit_number','land_size','listing_done','elevation_sea_level','listing_done_number'], 'safe'],
            [['unit_number', 'plot_number', 'street'], 'string', 'max' => 255],
            [['building_number'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'building_info' => 'Building Info',
            'property_id' => 'Property ID',
            'property_category' => 'Property Category',
            'community' => 'Community',
            'sub_community' => 'Sub Community',
            'tenure' => 'Tenure',
            'unit_number' => 'Unit Number',
            'city' => 'City',
            'country' => 'Country',
            'payment_plan' => 'Payment Plan',
            'status' => 'Status',
            'building_number' => 'Building Number',
            'plot_number' => 'Plot Number',
            'street' => 'Street',
            'floor_number' => 'Floor Number',
            'land_size' => 'Land Size',
            'extended' => 'Extended / Improved ?',
            'completion_status' => 'Completion Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
            'no_of_towers' => 'No Of Towers',
            'plot_area_source' => 'Plot Area Source',
            'bua_source' => 'Bua Source',
            'parking_source' => 'Parking Source',
            'parking_floors' => 'Parking Floors',
            'status_verified' => 'Status Verified',
            'status_verified_by' => 'Status Verified By',
            'status_verified_at' => 'Status Verified At',
        ];
    }



    public function getBuilding()
    {
        return $this->hasOne(Buildings::className(), ['id' => 'building_info']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Properties::className(), ['id' => 'property_id']);
    }


    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Valuation Detail';
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->id;
    }

    public function afterSave($insert, $changedAttributes)
    {

        $valuation = Valuation::findOne($this->valuation_id);
        $valuation->building_info = $this->building_info;
        $valuation->property_id = $this->property_id;
        $valuation->property_category = $this->property_category;
        $valuation->community = $this->community;
        $valuation->sub_community = $this->sub_community;
        $valuation->tenure = $this->tenure;
        $valuation->city = $this->city;
        $valuation->building_number = $this->building_number;
        $valuation->plot_number = $this->plot_number;
        $valuation->street = $this->street;
        $valuation->floor_number = $this->floor_number;
        $valuation->land_size = $this->land_size;
        $valuation->no_of_towers = $this->no_of_towers;
        $valuation->floor_number = $this->floor_number;
        $valuation->unit_number = $this->unit_number;
        $valuation->title_deed = $this->title_deed;
        //  $valuation->taqyeem_number = $this->taqyeem_number;
        // $valuation->plot_source = $this->plot_source;
        // $valuation->bua_source = $this->bua_source;
        // $valuation->parking_source = $this->parking_source;
        // $valuation->parking_floors = $this->parking_floors;
        //  $valuation->bedrooms = $this->bedrooms;
        //  $valuation->built_up_area = $this->built_up_area;

        $income_properties = [3,7,10,11,15,16,17,24,25,28,38,41,42,43,58,59,81,86,87];
        if (in_array($this->property_id, $income_properties)) {
            if($valuation->id != 10264) {
                $valuation->valuation_approach = 1;
            }else{
                $valuation->valuation_approach = 0;
            }
        } else {
            $valuation->valuation_approach = 0;
        }

        if($valuation->valuation_scope == 14){
            $valuation->valuation_approach = 0;
        }
        $valuation->save();


        $greenEffects = GreenEffects::find()->where(['valuation_id' => $this->valuation_id])->one();

        if ($greenEffects !== null) {
            $greenEffects->green_efficient_certification = $this->green_efficient_certification;
            $greenEffects->certifier_name = $this->certifier_name;
            $greenEffects->certification_level = $this->certification_level;
            $greenEffects->source_of_green_certificate_information = $this->source_of_green_certificate_information;
            $greenEffects->save();
        } else {
            $greenEffects = new GreenEffects();
            $greenEffects->green_efficient_certification = $this->green_efficient_certification;
            $greenEffects->certifier_name = $this->certifier_name;
            $greenEffects->certification_level = $this->certification_level;
            $greenEffects->source_of_green_certificate_information = $this->source_of_green_certificate_information;
            $greenEffects->save();
        }




        parent::afterSave($insert, $changedAttributes);
    }

    public function beforeSave($insert){

        
        // verify status reset after edit only something changed
        $isChanged = false;
        $ignoredAttributes = ['updated_at','status_verified_at']; 
        foreach ($this->attributes as $attribute => $value) {
            if (in_array($attribute, $ignoredAttributes)) {
                continue;
            }
            if ($this->getOldAttribute($attribute) != $value) {
                $isChanged = true;
                break;
            }
        }
        if ($isChanged) {
            if($this->status == 1 && $this->getOldAttribute('status') == 1){
                $this->status = 2;
            }elseif ($this->status == 2 && $this->getOldAttribute('status') == 1) {
                $this->status = 1;
            }
        }
        
        return parent::beforeSave($insert);
    }

}
