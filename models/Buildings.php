<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "buildings".
 *
 * @property int $id
 * @property string $title
 * @property int $community
 * @property int|null $sub_community
 * @property string $latitude
 * @property string $longitude
 * @property int $city
 * @property int $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class Buildings extends ActiveRecordFull
{
    public $status_verified;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'buildings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'community', 'city','developer_id','property_id','property_category'], 'required'],
            [['title'], 'unique'],
            [['community', 'city', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by','developer_id','number_of_basement','parking_floors','building_number','property_id', 'property_visibility', 'property_condition','no_of_units'], 'integer'],
            [['created_at', 'updated_at', 'trashed_at','developer_id','tenure','development_type', 'finished_status', 'pool', 'gym', 'play_area', 'completion_status', 'landscaping', 'white_goods', 'furnished', 'utilities_connected','other_facilities','development_type', 'finished_status', 'pool', 'gym', 'play_area', 'completion_status', 'landscaping', 'white_goods', 'furnished','latitude', 'longitude','no_of_units','service_charges_psf','estimated_age','location_highway_drive','location_school_drive','location_mall_drive','location_sea_drive','location_park_drive', 'property_exposure','year_of_construction','approved_by','approved_at','typical_floors','year_of_construction','vacancy','utilities_connected','development_type', 'finished_status'], 'safe'],
            [['title'], 'string', 'max' => 100],
            [['service_charges_psf','estimated_age','vacancy','completion_status', 'property_placement'], 'number'],
            [['latitude', 'longitude','plot_number','street','makani_number'], 'string', 'max' => 255],
            [['status_verified','status_verified_at','status_verified_by','auto_title','reidin_title','parent_project_name','bayut_title','reidin_title_2','bayut_title_2','parent_projects','bayut_title_3','bayut_title_4','bayut_title_5','reidin_title_3','reidin_title_4','reidin_title_5'], 'safe'],
            [['parking_space','jacuzzi', 'upgrades', 'mezzanine_floors', 'no_of_residential_units', 'no_of_commercial_units', 'no_of_retail_units', 'no_of_unit_types', 'meeting_rooms', 'no_of_health_club_spa', 'no_of_bbq_area', 'no_of_schools', 'no_of_clinics', 'no_of_sports_courts', 'no_of_mosques', 'restaurant', 'atms', 'no_of_coffee_shops', 'no_of_sign_boards', 'night_clubs', 'bars','sub_community','merge_id','estimated_age_month'], 'safe'],
            [['bayut_title_6','bayut_title_7','bayut_title_8','bayut_title_9','bayut_title_ten'], 'safe'],
            [['reidin_title_6','reidin_title_7','reidin_title_8','reidin_title_9','reidin_title_ten','municipality_number','dld_project_number','group_projects','gross_yield'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'community' => 'Community',
            'sub_community' => 'Sub Community',
            'property_id' => 'Property',
            'property_category' => 'Property Category',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'tenure' => 'Tenure',
            'estimated_age' => 'Estimated Age (Years)',
            'developer_id' => 'Developer',
            'city' => 'City',
            'number_of_basement	' => 'No. of Basement	',
            'parking_floors' => 'Parking Floors',
            'typical_floors' => 'Typical Floors',
            'building_number' => 'Building Number',
            'plot_number' => 'Plot Number',
            'makani_number' => 'Makani Number',
            'street' => 'Street Number/Name',
            'property_placement' => 'Property Placement',
            'property_visibility' => 'Property Visibility',
            'property_exposure' => 'Property Exposure',
            'property_condition' => 'Property Condition',
            'development_type' => 'Development Type',
            'finished_status' => 'Finished Status',
            'location_highway_drive' => 'Drive to Highway/Main Road and metro',
            'location_school_drive' => 'Drive to school',
            'location_mall_drive' => 'Drive to commercial area/Mall',
            'location_sea_drive' => 'Drive to special landmark, sea, marina',
            'location_park_drive' => 'Drive to pool and park',
            'pool' => 'Number of Swimming Pools',
            'gym' => 'Number of Gyms',
            'play_area' => 'Children Play Area',
            'other_facilities' => 'Other Facilities',
            'completion_status' => 'Completion Status',
            'landscaping' => 'Landscaping',
            'white_goods' => 'White Goods',
            'furnished' => 'Furnished',
            'utilities_connected' => 'Utilities Connected',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
            'parking_space' => 'Number of Parking Spaces',
            'jacuzzi' => 'Number of Jacuzzi',
            'upgrades' => 'Upgrades Star Rating',
            'mezzanine_floors' => 'Number of Mezzanine Floors',

        ];
    }

    public function beforeSave($insert)
    {

        if (!empty($this->other_facilities)) {
            $this->other_facilities = implode(",", $this->other_facilities);
        }
        if (!empty($this->group_projects)) {
            $this->group_projects = implode(",", $this->group_projects);
        }
        if($this->status == 1 &&  $this->approved_by == ''){
            $this->approved_by = Yii::$app->user->identity->id;
            $this->approved_at = date("Y-m-d H:i:s");
        }
        if($this->year_of_construction <> null){
            $this->estimated_age = date('Y') - $this->year_of_construction;
        }
        if($this->merge_id <> null){
            $this->status =6;
        }

        // verify status reset after edit only something changed
        $isChanged = false;
        $ignoredAttributes = ['updated_at','status_verified_at']; 
        foreach ($this->attributes as $attribute => $value) {
            if (in_array($attribute, $ignoredAttributes)) {
                continue;
            }
            if ($this->getOldAttribute($attribute) != $value) {
                $isChanged = true;
                break;
            }
        }
        if ($isChanged) {
            if($this->status == 1 && $this->getOldAttribute('status') == 1){
                $this->status = 2;
            }elseif ($this->status == 2 && $this->getOldAttribute('status') == 1) {
                $this->status = 1;
            }
        }

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        //listings
        $list_title = \app\models\ListData::find()
            ->where(['=', 'building_info', trim($this->title)])
            ->asArray()->one();
        if ($list_title != null) {
            Yii::$app->db->createCommand()->update('list_data', ['move_to_listing' => 0], ['building_info' => $this->title, 'move_to_listing'=> 2])->execute();
        }
        $list_title = \app\models\ListData::find()
            ->Where(['=', 'building_info', trim($this->bayut_title)])
            ->asArray()->one();
        if ($list_title != null) {
            Yii::$app->db->createCommand()->update('list_data', ['move_to_listing' => 0], ['building_info' => $this->bayut_title, 'move_to_listing'=> 2])->execute();
        }
        $list_title = \app\models\ListData::find()
            ->Where(['=', 'building_info', trim($this->bayut_title_2)])
            ->asArray()->one();
        if ($list_title != null) {
            Yii::$app->db->createCommand()->update('list_data', ['move_to_listing' => 0], ['building_info' => $this->bayut_title_2, 'move_to_listing'=> 2])->execute();
        }



        //abudabi sold
        $sold_title_1 = \app\models\SoldAd::find()
            ->where(['=', 'building', trim($this->title)])
            ->asArray()->one();
        if ($sold_title_1 != null) {
            Yii::$app->db->createCommand()->update('sold_ad', ['status' => 0], ['building' => $this->title, 'status'=> 3])->execute();
            Yii::$app->db->createCommand()->update('sold_ad', ['status' => 0], ['projectsub_comm' => $this->title, 'status'=> 3])->execute();
        }

        $sold_title_2 = \app\models\SoldAd::find()
            ->Where(['=', 'building', trim($this->reidin_title)])
            ->asArray()->one();
        if ($sold_title_2 != null) {
            Yii::$app->db->createCommand()->update('sold_ad', ['status' => 0], ['building' => $this->reidin_title, 'status'=> 3])->execute();
            Yii::$app->db->createCommand()->update('sold_ad', ['status' => 0], ['projectsub_comm' => $this->reidin_title, 'status'=> 3])->execute();
        }

        $sold_title_3 = \app\models\SoldAd::find()
            ->Where(['=', 'building', trim($this->reidin_title_2)])
            ->asArray()->one();
        if ($sold_title_3 != null) {
            Yii::$app->db->createCommand()->update('sold_ad', ['status' => 0], ['building' => $this->reidin_title_2, 'status'=> 3])->execute();
            Yii::$app->db->createCommand()->update('sold_ad', ['status' => 0], ['projectsub_comm' => $this->reidin_title_2, 'status'=> 3])->execute();
        }




        $sold_title_1 = \app\models\SoldAd::find()
            ->where(['=', 'community', trim($this->title)])
            ->orWhere(['=', 'projectsub_comm', trim($this->title)])
            ->orWhere(['=', 'community', trim($this->title)])
            ->asArray()->one();
        if ($sold_title_1 != null) {
            Yii::$app->db->createCommand()->update('sold_ad', ['status' => 0], ['building' => $this->title, 'status'=> 3])->execute();
            Yii::$app->db->createCommand()->update('sold_ad', ['status' => 0], ['projectsub_comm' => $this->title, 'status'=> 3])->execute();
            Yii::$app->db->createCommand()->update('sold_ad', ['status' => 0], ['community' => $this->title, 'status'=> 3])->execute();
        }

        $sold_title_2 = \app\models\SoldAd::find()
            ->Where(['=', 'building', trim($this->reidin_title)])
            ->orWhere(['=', 'projectsub_comm', trim($this->reidin_title)])
            ->asArray()->one();
        if ($sold_title_2 != null) {
            Yii::$app->db->createCommand()->update('sold_ad', ['status' => 0], ['building' => $this->reidin_title, 'status'=> 3])->execute();
            Yii::$app->db->createCommand()->update('sold_ad', ['status' => 0], ['projectsub_comm' => $this->reidin_title, 'status'=> 3])->execute();
        }

        $sold_title_3 = \app\models\SoldAd::find()
            ->Where(['=', 'building', trim($this->reidin_title_2)])
            ->orWhere(['=', 'projectsub_comm', trim($this->reidin_title_2)])
            ->asArray()->one();
        if ($sold_title_3 != null) {
            Yii::$app->db->createCommand()->update('sold_ad', ['status' => 0], ['building' => $this->reidin_title_2, 'status'=> 3])->execute();
            Yii::$app->db->createCommand()->update('sold_ad', ['status' => 0], ['projectsub_comm' => $this->reidin_title_2, 'status'=> 3])->execute();
        }



        //abudabi offplan
        $sold_off_title_1 = \app\models\SoldOffPlan::find()
            ->where(['=', 'property', trim($this->title)])
            ->asArray()->one();
        if ($sold_off_title_1 != null) {
            Yii::$app->db->createCommand()->update('sold_off_plan', ['status' => 0], ['property' => $this->title, 'status'=> 3])->execute();
        }

        $sold_off_title_2 = \app\models\SoldOffPlan::find()
            ->Where(['=', 'property', trim($this->reidin_title)])
            ->asArray()->one();
        if ($sold_off_title_2 != null) {
            Yii::$app->db->createCommand()->update('sold_off_plan', ['status' => 0], ['property' => $this->reidin_title, 'status'=> 3])->execute();
        }

        $sold_off_title_3 = \app\models\SoldOffPlan::find()
            ->Where(['=', 'property', trim($this->reidin_title_2)])
            ->asArray()->one();
        if ($sold_off_title_3 != null) {
            Yii::$app->db->createCommand()->update('sold_off_plan', ['status' => 0], ['property' => $this->reidin_title_2, 'status'=> 3])->execute();
        }

        //sold_land
        $sold_land_title_1 = \app\models\LandSolds::find()
            ->where(['=', 'community', trim($this->title)])
            ->asArray()->one();
        if ($sold_land_title_1 != null) {
            Yii::$app->db->createCommand()->update('land_solds', ['status' => 0], ['community' => $this->title, 'status'=> 3])->execute();
        }

        $sold_land_title_2 = \app\models\LandSolds::find()
            ->Where(['=', 'community', trim($this->reidin_title)])
            ->asArray()->one();
        if ($sold_land_title_2 != null) {
            Yii::$app->db->createCommand()->update('land_solds', ['status' => 0], ['community' => $this->reidin_title, 'status'=> 3])->execute();
        }

        $sold_land_title_3 = \app\models\LandSolds::find()
            ->Where(['=', 'community', trim($this->reidin_title_2)])
            ->asArray()->one();
        if ($sold_land_title_3 != null) {
            Yii::$app->db->createCommand()->update('land_solds', ['status' => 0], ['community' => $this->reidin_title_2, 'status'=> 3])->execute();
        }

        //abudabi offplan
        $sold_comm_title_1 = \app\models\CommercialSolds::find()
            ->where(['=', 'property', trim($this->title)])
            ->asArray()->one();
        if ($sold_comm_title_1 != null) {
            Yii::$app->db->createCommand()->update('commercial_solds', ['status' => 0], ['property' => $this->title, 'status'=> 3])->execute();
        }

        $sold_comm_title_2 = \app\models\CommercialSolds::find()
            ->Where(['=', 'property', trim($this->reidin_title)])
            ->asArray()->one();
        if ($sold_comm_title_2 != null) {
            Yii::$app->db->createCommand()->update('commercial_solds', ['status' => 0], ['property' => $this->reidin_title, 'status'=> 3])->execute();
        }

        $sold_comm_title_3 = \app\models\CommercialSolds::find()
            ->Where(['=', 'property', trim($this->reidin_title_2)])
            ->asArray()->one();
        if ($sold_comm_title_3 != null) {
            Yii::$app->db->createCommand()->update('commercial_solds', ['status' => 0], ['property' => $this->reidin_title_2, 'status'=> 3])->execute();
        }



        //sold import Auto
        $sold_import_title_1 = \app\models\DailySoldsImport::find()
            ->where(['=', 'property', trim($this->title)])
            ->asArray()->one();
        if ($sold_import_title_1 != null) {
            Yii::$app->db->createCommand()->update('daily_solds_import', ['status' => 0], ['property' => $this->title, 'status'=> 3])->execute();
        }

        $sold_import_title_2 = \app\models\DailySoldsImport::find()
            ->Where(['=', 'property', trim($this->reidin_title)])
            ->asArray()->one();
        if ($sold_import_title_2 != null) {
            Yii::$app->db->createCommand()->update('daily_solds_import', ['status' => 0], ['property' => $this->reidin_title, 'status'=> 3])->execute();
        }

        $sold_import_title_3 = \app\models\DailySoldsImport::find()
            ->Where(['=', 'property', trim($this->reidin_title_2)])
            ->asArray()->one();
        if ($sold_import_title_3 != null) {
            Yii::$app->db->createCommand()->update('daily_solds_import', ['status' => 0], ['property' => $this->reidin_title_2, 'status'=> 3])->execute();
        }






        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommunities()
    {
        return $this->hasOne(Communities::className(), ['id' => 'community']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubCommunities()
    {
        return $this->hasOne(SubCommunities::className(), ['id' => 'sub_community']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeveloper()
    {
        return $this->hasOne(Developers::className(), ['id' => 'developer_id']);
    }

    public function getProperty()
    {
        return $this->hasOne(Properties::className(), ['id' => 'property_id']);
    }
    
    public function getCityName()
    {
        return $this->hasOne(Zone::className(), ['id' => 'city']);
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->title;
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Buildings';
    }
}
