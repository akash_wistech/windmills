<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "crm_received_properties".
 *
 * @property int $id
 * @property int|null $no_of_owners
 * @property int|null $service_officer_name
 * @property string|null $instruction_date
 * @property string|null $target_date
 * @property int|null $building_info
 * @property int|null $property_id
 * @property int|null $property_category
 * @property int|null $community
 * @property int|null $sub_community
 * @property int $tenure
 * @property string|null $unit_number
 * @property int|null $city
 * @property string|null $payment_plan
 * @property int $building_number
 * @property string|null $plot_number
 * @property string $street
 * @property int $floor_number
 * @property int|null $instruction_person
 * @property float|null $land_size
 * @property int|null $purpose_of_valuation
 * @property int|null $valuation_scope
 * @property string|null $special_assumption
 * @property string|null $approval_status
 * @property int|null $quotation_id
 */
class CrmReceivedProperties extends \yii\db\ActiveRecord
{
    public $highest_fee;
    private $isSaving = false;
    public $lowest_fee;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_received_properties';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no_of_owners', 'service_officer_name', 'building_info', 'property_id', 'property_category', 'tenure', 'building_number', 'floor_number', 'instruction_person', 'purpose_of_valuation', 'valuation_scope', 'quotation_id','general_asumption'], 'integer'],
            [['instruction_date', 'target_date','client_name_passport','complexity','type_of_valuation','number_of_comparables','no_of_units','n','recommended_fee','quotation_fee','tat','valuation_approach','toe_fee','toe_tat','property_index','inspection_type','general_asumption','land_size'], 'safe'],
            [['payment_plan', 'special_assumption', 'approval_status'], 'string'],
            [['land_size','recommended_fee','quotation_fee','toe_fee','toe_tat','built_up_area'], 'number'],
            [['unit_number', 'plot_number', 'street'], 'string', 'max' => 255],
            [['city', 'community', 'sub_community','upgrades','auto_check'], 'safe'],
            [['status_verified','status_verified_by','status_verified_at','no_of_unit_types','number_of_types','number_of_rooms_building','restaurant','ballrooms','atms','retails_units','night_clubs','bars','health_club','meeting_rooms','spa','beach_access','parking_sale','repeat_valuation'], 'safe'],
            [['property_general_asumption','property_special_asumption','last_3_years_finance','projections_10_years', 'highest_fee', 'lowest_fee','scope_of_service','asset_category','no_of_asset','asset_complexity','working_days','asset_age','country_of_manufacturing','scope_of_service_saved','multiscope_id'], 'safe'],

            // [['building_info','city', 'community', 'sub_community', 'plot_number', 'street', 'property_id', 'property_category','tenure', 'repeat_valuation', ], 'required'],
            // [['scope_of_service', ], 'required'],


            [['parking_floors', 'parking_space', 'basement_floors', 'mezzanine_floors', 'typical_floors', 'swimming_pools', 'no_of_jacuzzi', 'net_leasable_area', 'furnished', 'ready', 'no_of_buildings', 'no_of_residential_units', 'no_of_commercial_units', 'no_of_coffee_shops', 'no_of_sign_boards', 'no_of_bbq_area', 'no_of_play_area', 'no_of_gyms', 'no_of_schools', 'no_of_clinics', 'no_of_sports_courts', 'no_of_mosques', 'no_of_health_club_spa', 'reference_fee_staff', 'reference_fee_3rdparty', 'aprvd_tat','drawings_available','civil_drawing_available', 'mechanical_drawing_available', 'electrical_drawing_available','plumbing_drawing_available','hvac_drawing_available', 'no_of_units_value', 'no_of_res_units_value', 'no_of_com_units_value', 'no_of_ret_units_value','no_of_warehouse_units_value', 'no_of_comparables_value','bo_recommended_fee','reviewed_fee','bo_recommended_tat','reviewed_tat'], 'safe'],


        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_of_owners' => 'No Of Owners',
            'service_officer_name' => 'Service Officer Name',
            'instruction_date' => 'Instruction Date',
            'target_date' => 'Target Date',
            'building_info' => 'Building Info',
            'building_number' => 'Building Number',
            'city' => 'City',
            'community' => 'Community',
            'sub_community' => 'Sub Community',
            'plot_number' => 'Plot Number',
            'street' => 'Street',
            'property_id' => 'Property Type',
            'property_category' => 'Property Category',
            'tenure' => 'Tenure',
            'complexity' => 'Property Standard/Complexity',
            'unit_number' => 'Unit Number',
            'floor_number' => 'Floor Number',
            'payment_plan' => 'Payment Plan',
            'instruction_person' => 'Instruction Person',
            'land_size' => 'Land Size',
            'purpose_of_valuation' => 'Purpose Of Valuation',
            'valuation_scope' => 'Valuation Scope',
            'special_assumption' => 'Special Assumption',
            'approval_status' => 'Approval Status',
            'quotation_id' => 'Quotation ID',
            'property_general_asumption' => 'General Asumptions',
            'property_special_asumption' => 'Special Asumptions',
        ];
    }
  /*  public function getBuildingWithTitle()
    {
        return $this->hasOne(Buildings::className(), ['title' => 'building_info']);
    }*/
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => function($event) {
                    return date("Y-m-d H:i:s");
                },
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    public function beforeSave($insert){

       

        if(!empty($this->property_general_asumption) and is_array($this->property_general_asumption)){
            $this->property_general_asumption =  implode(",",$this->property_general_asumption);
        }
        if(!empty($this->property_special_asumption) and is_array($this->property_special_asumption)){
            $this->property_special_asumption =  implode(",",$this->property_special_asumption);
        }
        if(!empty($this->drawings_available) and is_array($this->drawings_available)){
            $this->drawings_available =  implode(",",$this->drawings_available);
        }
        
        if(!empty($this->scope_of_service) and is_array($this->scope_of_service)){
            $this->scope_of_service =  implode(",",$this->scope_of_service);
        }

        $multiScopeProperty = CrmReceivedProperties::find()->where(['multiscope_id' => $this->multiscope_id])->all();
        

        if(!isset($this->id)){ 

            $values = explode(',', $this->scope_of_service);
            if(count($multiScopeProperty) < 1){
                // $this->scope_of_service_saved = $this->scope_of_service[0];
                $this->scope_of_service_saved = $values[0];
            }
            
        }

    
       
        // verify status reset after edit
        // if($this->getOldAttribute('status_verified') == 1){
        //     $this->status_verified = 2;
        // }

        // verify status reset after edit only something changed
        $isChanged = false;
        $ignoredAttributes = ['updated_at','status_verified_at']; 
        foreach ($this->attributes as $attribute => $value) {
            if (in_array($attribute, $ignoredAttributes)) {
                continue;
            }
            if ($this->getOldAttribute($attribute) != $value) {
                $isChanged = true;
                break;
            }
        }
        if ($isChanged) {
            if($this->status_verified == 1 && $this->getOldAttribute('status_verified') == 1){
                $this->status_verified = 2;
            }elseif ($this->status_verified == 2 && $this->getOldAttribute('status_verified') == 1) {
                $this->status_verified = 1;
            }
        }

        // $property = CrmReceivedProperties::find()->select('status_verified')->where(['id' => $this->id])->one();

        // if($property->status_verified == 1){
        //     $this->status_verified = 2;
        // }

        

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }


    public function afterSave($insert, $changedAttributes)
    {   
        $scopeOfService = explode(',', $this->scope_of_service);
              

        if ($insert) {
            if (count($scopeOfService) > 1) { 
                $property = CrmReceivedProperties::find()->where(['id' => $this->id])->one();              

                for ($i = 1; $i < count($scopeOfService); $i++) {
                    $result = CrmReceivedProperties::find()->where(['multiscope_id' => $this->multiscope_id, 'scope_of_service_saved' => $scopeOfService[$i]])->one();
                    if($result !== null){
                        
                    }else{
                        $multiScopeProperty = CrmReceivedProperties::find()->where(['multiscope_id' => $this->multiscope_id])->all();    
                        if(count($multiScopeProperty) < count($scopeOfService)){
                            $newProperty = new CrmReceivedProperties;
                            $newProperty->attributes = $property->attributes;
                            $newProperty->scope_of_service = $this->scope_of_service;
                            $newProperty->scope_of_service_saved = $scopeOfService[$i];
                            $newProperty->save();


                        }
                    }
                }
            }
        } else {

            if (count($scopeOfService) > 1) { 
                $property = CrmReceivedProperties::find()->where(['id' => $this->id])->one(); 
                
                if($changedAttributes['scope_of_service']){
                    CrmReceivedProperties::deleteAll(['multiscope_id' => $this->multiscope_id]);
                    // CrmReceivedProperties::deleteAll(['multiscope_id' => $this->multiscope_id, ['not', 'scope_of_service_saved' => $this->scope_of_service[0]]]);
                }
                
                
                for ($i = 1; $i < count($scopeOfService); $i++) {

                    // dd($this->scope_of_service);
                    
                    $result = CrmReceivedProperties::find()->where(['multiscope_id' => $this->multiscope_id, 'scope_of_service_saved' => $scopeOfService[$i]])->one();
                    if($result !== null){
                        
                        // CrmReceivedProperties::deleteAll(['multiscope_id' => $this->multiscope_id,'scope_of_service_saved' => $scopeOfService[$i]]);
                        

                        $multiScopeProperty = CrmReceivedProperties::find()->where(['multiscope_id' => $this->multiscope_id])->all();    
                        if(count($multiScopeProperty) < count($scopeOfService)){

                            $property = CrmReceivedProperties::find()->where(['multiscope_id' => $this->multiscope_id, 'scope_of_service_saved' => $scopeOfService[$i]])->one(); 

                            $newProperty = new CrmReceivedProperties;
                            $newProperty->attributes = $property->attributes;
                            $newProperty->scope_of_service = $this->scope_of_service;
                            $newProperty->scope_of_service_saved = $scopeOfService[$i];
                            $newProperty->save();
                        }
                        
                    }
                    else{ 
                        $multiScopeProperty = CrmReceivedProperties::find()->where(['multiscope_id' => $this->multiscope_id])->all();    
                        if(count($multiScopeProperty) < count($scopeOfService)){
                            $newProperty = new CrmReceivedProperties;
                            $newProperty->attributes = $property->attributes;
                            $newProperty->scope_of_service = $this->scope_of_service;
                            $newProperty->scope_of_service_saved = $scopeOfService[$i];
                            $newProperty->save();
                        }
                    }
                }
            }
        }

       

        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Company::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuilding()
    {
        return $this->hasOne(Buildings::className(), ['id' => 'building_info']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Properties::className(), ['id' => 'property_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspectProperty()
    {
        return $this->hasOne(InspectProperty::className(), ['quotation_id' => 'id']);
    }
    public function getScheduleInspection(){
        return $this->hasOne(ScheduleInspection::className(),['quotation_id'=>'id']);
    }

    public function getAssetCategory()
    {
        return $this->hasOne(AssetCategory::class, ['id' => 'asset_category']);
    }

    public function getLocation()
    {
        return $this->hasOne(Zone::className(), ['id' => 'location']);
    }

}
