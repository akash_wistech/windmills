<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "weightages".
 *
 * @property int $id
 * @property int $property_type
 * @property int $emirates
 * @property float $location
 * @property float $age
 * @property float $property_exposure
 * @property float $property_placement
 * @property float $finishing_status
 * @property float $bedrooom
 * @property float $view
 * @property float $quality
 * @property float $floor
 * @property float $land_size
 * @property float $bua
 * @property float $balcony_size
 * @property float $furnished
 * @property float $upgrades
 * @property float $parking
 * @property float $pool
 * @property float $landscape
 * @property float $white_goods
 * @property float $utilities_connected
 * @property float $tenure
 * @property float $number_of_levels
 * @property float $property_visibility
 * @property float $less_than_1_month
 * @property float $less_than_2_month
 * @property float $less_than_3_month
 * @property float $less_than_4_month
 * @property float $less_than_5_month
 * @property float $less_than_6_month
 * @property float $less_than_7_month
 * @property float $less_than_8_month
 * @property float $less_than_9_month
 * @property float $less_than_10_month
 * @property float $less_than_11_month
 * @property float $less_than_12_month
 * @property float $less_than_2_year
 * @property float $less_than_3_year
 * @property float $less_than_4_year
 * @property float $less_than_5_year
 * @property float $less_than_6_year
 * @property float $less_than_7_year
 * @property float $less_than_8_year
 * @property float $less_than_9_year
 * @property float $less_than_10_year
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $trashed_at
 * @property int|null $trashed
 * @property int|null $trashed_by
 */
class Weightages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'weightages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['property_type', 'emirates', 'location', 'age', 'property_exposure', 'property_placement', 'finishing_status', 'bedrooom', 'view', 'quality', 'floor', 'land_size', 'bua', 'balcony_size', 'furnished', 'upgrades', 'parking', 'pool', 'landscape', 'white_goods', 'utilities_connected', 'tenure', 'number_of_levels', 'property_visibility', 'less_than_1_month', 'less_than_2_month', 'less_than_3_month', 'less_than_4_month', 'less_than_5_month', 'less_than_6_month', 'less_than_7_month', 'less_than_8_month', 'less_than_9_month', 'less_than_10_month', 'less_than_11_month', 'less_than_12_month', 'less_than_2_year', 'less_than_3_year', 'less_than_4_year', 'less_than_5_year', 'less_than_6_year', 'less_than_7_year', 'less_than_8_year', 'less_than_9_year', 'less_than_10_year'], 'required'],
            [['property_type', 'emirates', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['location', 'age', 'property_exposure', 'property_placement', 'finishing_status', 'bedrooom', 'view', 'quality', 'floor', 'land_size', 'bua', 'balcony_size', 'furnished', 'upgrades', 'parking', 'pool', 'landscape', 'white_goods', 'utilities_connected', 'tenure', 'number_of_levels', 'property_visibility', 'less_than_1_month', 'less_than_2_month', 'less_than_3_month', 'less_than_4_month', 'less_than_5_month', 'less_than_6_month', 'less_than_7_month', 'less_than_8_month', 'less_than_9_month', 'less_than_10_month', 'less_than_11_month', 'less_than_12_month', 'less_than_2_year', 'less_than_3_year', 'less_than_4_year', 'less_than_5_year', 'less_than_6_year', 'less_than_7_year', 'less_than_8_year', 'less_than_9_year', 'less_than_10_year','location_avg','tenure_avg','view_avg','finished_status_avg','property_condition_avg','property_exposure_avg','property_placement_avg','upgrades_avg','number_of_levels_avg','parking_space_avg','pool_avg','landscaping_avg','white_goods_avg','utilities_connected_avg','developer_margin_avg','land_size_avg','balcony_size_avg'], 'number'],
            [['created_at', 'updated_at', 'trashed_at','location_avg','tenure_avg','view_avg','finished_status_avg','property_condition_avg','property_exposure_avg','property_placement_avg','upgrades_avg','number_of_levels_avg','parking_space_avg','pool_avg','landscaping_avg','white_goods_avg','utilities_connected_avg','developer_margin_avg','land_size_avg','balcony_size_avg'], 'safe'],
            [['status_verified','status_verified_at','status_verified_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_type' => 'Property Type',
            'emirates' => 'Emirates',
            'location' => 'Location',
            'age' => 'Age',
            'property_exposure' => 'Property Exposure',
            'property_placement' => 'Property Placement',
            'finishing_status' => 'Finishing Status',
            'bedrooom' => 'Bedrooom',
            'view' => 'View',
            'quality' => 'Quality',
            'floor' => 'Floor',
            'land_size' => 'Land Size',
            'bua' => 'Bua',
            'balcony_size' => 'Balcony Size',
            'furnished' => 'Furnished',
            'upgrades' => 'Upgrades',
            'parking' => 'Parking',
            'pool' => 'Pool',
            'landscape' => 'Landscape',
            'white_goods' => 'White Goods',
            'utilities_connected' => 'Utilities Connected',
            'tenure' => 'Tenure',
            'number_of_levels' => 'Number Of Levels',
            'property_visibility' => 'Property Visibility',
            'less_than_1_month' => 'Less Than 1 Month',
            'less_than_2_month' => 'Less Than 2 Month',
            'less_than_3_month' => 'Less Than 3 Month',
            'less_than_4_month' => 'Less Than 4 Month',
            'less_than_5_month' => 'Less Than 5 Month',
            'less_than_6_month' => 'Less Than 6 Month',
            'less_than_7_month' => 'Less Than 7 Month',
            'less_than_8_month' => 'Less Than 8 Month',
            'less_than_9_month' => 'Less Than 9 Month',
            'less_than_10_month' => 'Less Than 10 Month',
            'less_than_11_month' => 'Less Than 11 Month',
            'less_than_12_month' => 'Less Than 12 Month',
            'less_than_2_year' => 'Less Than 2 Year',
            'less_than_3_year' => 'Less Than 3 Year',
            'less_than_4_year' => 'Less Than 4 Year',
            'less_than_5_year' => 'Less Than 5 Year',
            'less_than_6_year' => 'Less Than 6 Year',
            'less_than_7_year' => 'Less Than 7 Year',
            'less_than_8_year' => 'Less Than 8 Year',
            'less_than_9_year' => 'Less Than 9 Year',
            'less_than_10_year' => 'Less Than 10 Year',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'trashed_at' => 'Trashed At',
            'trashed' => 'Trashed',
            'trashed_by' => 'Trashed By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Properties::className(), ['id' => 'property_type']);
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->title;
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Weightages';
    }
}
