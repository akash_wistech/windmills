<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "inspect_property".
 *
 * @property int $id
 * @property int $makani_number
 * @property int $municipality_number
 * @property int $location
 * @property string $latitude
 * @property string $longitude
 * @property int $property_placement
 * @property int $acquisition_method
 * @property int $property_visibility
 * @property int $property_exposure
 * @property int $property_category
 * @property int $property_condition
 * @property int $property_defect
 * @property string $development_type
 * @property string $finished_status
 * @property int $developer_id
 * @property float $estimated_age
 * @property float $estimated_remaining_life
 * @property float $balcony_size
 * @property float $service_area_size
 * @property float $built_up_area
 * @property float $net_built_up_area
 * @property int $number_of_basement
 * @property int $number_of_levels
 * @property string $pool
 * @property string $gym
 * @property string $play_area
 * @property string $other_facilities
 * @property string $completion_status
 * @property string $occupancy_status
 * @property int $no_of_warehouse
 * @property int $no_of_office
 * @property int $no_of_labour_camps
 * @property int $no_of_bedrooms
 * @property int $no_of_bathrooms
 * @property int $full_building_floors
 * @property int $parking_floors
 * @property int $view
 * @property string $landscaping
 * @property string $fridge
 * @property string $oven
 * @property string $cooker
 * @property string|null $furnished
 * @property int|null $ac_type
 * @property string $washing_machine
 * @property string $white_goods
 * @property string $utilities_connected
 * @property int $status
 * @property int|null $created_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 * @property int|null $updated_by
 */
class InspectProperty extends ActiveRecordFull
{
    public $step,$listing_property_type_options,$checkofc;
    public $customAttachments = [];
    public $customAttachments_p2 = [];
    public $customAttachments_p3 = [];
    public $towers_data = [];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inspect_property';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'valuation_id'], 'required'],

            [['acquisition_method', 'property_visibility', 'property_category', 'property_condition', 'property_defect', 'developer_id', 'number_of_basement', 'number_of_levels', 'no_of_bedrooms', 'no_of_bathrooms', 'parking_floors', 'status', 'created_by', 'trashed', 'trashed_by', 'updated_by','no_of_kitchen','no_of_living_area','no_of_dining_area','no_of_maid_rooms','no_of_laundry_area','no_of_store','no_of_service_block','no_of_garage','no_of_balcony', 'mode_of_transport','number_of_floors','total_units','residential_units','commercial_units','retail_units','no_party_hall','no_health_club_spa','no_elevators','no_studios','no_one_bedrooms','no_two_bedrooms','no_three_bedrooms','no_four_bedrooms','no_penthouse','no_of_shops','no_of_offices','no_of_restaurants','no_of_atm_machines','no_of_coffee_shops','no_of_signboards','no_of_bars','no_of_night_clubs'], 'integer'],

            [['development_type', 'finished_status', 'pool', 'occupancy_status', 'landscaping', 'fridge', 'oven', 'cooker', 'furnished', 'washing_machine', 'white_goods', 'utilities_connected','gated_community'], 'string'],

            [['estimated_age', 'estimated_remaining_life', 'balcony_size', 'service_area_size', 'built_up_area', 'net_built_up_area', 'completion_status', 'start_kilometres', 'end_kilometres','measurement','extension','property_placement'], 'number'],

            [['created_at', 'updated_at', 'trashed_at', 'other_facilities','estimated_remaining_life','location_highway_drive','location_school_drive','location_mall_drive','location_sea_drive','location_park_drive','view_community','view_pool','view_burj','view_sea','view_marina','view_mountains','view_lake','view_golf_course','view_park','view_special','listing_property_type', 'mode_of_transport', 'start_kilometres', 'end_kilometres','measurement','extension','bua_source','plot_area_source','extension_source','plot_source','parking_source','age_source','no_of_family_room','no_of_powder_room','no_of_study_room','step','listing_property_type_options','customAttachments','ac_type', 'property_exposure','inspection_done_date','location_pin','permitted_bua_check','permitted_bua', 'permitted_gfa', 'landscaping','number_of_building_on_land'], 'safe'],

            [['latitude', 'longitude','makani_number','municipality_number'], 'string', 'max' => 255],
            [['property_defect','location_remarks','building_remarks','location_pin','status_verified','status_verified_by','status_verified_at','makani_number','municipality_number','nla','estimated_age_month', 'full_building_floors','number_of_parking_floors'], 'safe'],
            [['gfa','gfa_basement_parking','gfa_multistory_parking','linear_area','covered_area','project_development_period','no_of_kitchen','no_of_living_area','no_of_dining_area','no_of_maid_rooms','no_of_laundry_area','no_of_store','no_of_service_block','no_of_garage','no_of_balcony','no_of_bedrooms', 'no_of_bathrooms','towers_data','location_pin'], 'safe'],
            [['building_floor_point_area','sealable_area','unit_sealable_balcony_area','retail_area','landscape_area'], 'safe'],
            [['green_efficient_certification', 'certifier_name', 'certification_level', 'source_of_green_certificate_information','sms','date_of_building_permit','date_of_completion_certificate'], 'safe'],
            [['developer_id_property', 'number_of_mezannines', 'dish_washer', 'no_of_gym_room','number_of_bbq_areas','children_play_area','jacuzzi','number_of_labor_rooms','balcony_covered_with_roof','extension_permision_document','extension_completion_document','no_childrens_play_area','no_of_swimming_pool','no_of_jacuzzi','no_of_school','no_of_clinic','no_of_sports_court','no_of_masjid','no_of_community_center','length_of_fence', 'built_up_area_m','checkofc','not_complete_reason'], 'safe'],
            [['plot_number_verify','street_verify','unit_number_verify','location_pin_verify','community_verify','sub_community_verify','city_verify','country_verify'], 'safe'],
            [['no_studios_nla','no_one_bedrooms_nla','no_two_bedrooms_nla','no_three_bedrooms_nla','no_four_bedrooms_nla','no_penthouse_nla','no_of_shops','no_of_offices','no_of_shops_nla','no_of_offices_nla','no_warehouses','no_warehouses_nla','warehouse_summary'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'makani_number' => 'Makani Number',
            'municipality_number' => 'Municipality Number',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'property_placement' => 'Property Placement',
            'acquisition_method' => 'Acquisition Method',
            'property_visibility' => 'Property Visibility',
            'property_exposure' => 'Property Exposure',
            'property_category' => 'Property Category',
            'property_condition' => 'Property Condition',
            'property_defect' => 'Property Defect',
            'development_type' => 'Development Type',
            'finished_status' => 'Finished Status',
            'developer_id' => 'Developer',
            'estimated_age' => 'Estimated Age',
            'estimated_remaining_life' => 'Estimated Remaining Life',
            'balcony_size' => 'Balcony Size',
            'service_area_size' => 'Service Area Size',
            'built_up_area' => 'Built Up Area',
            'net_built_up_area' => 'Net Built Up Area',
            'number_of_basement' => 'Number Of Basement',
            'number_of_levels' => 'Number Of Levels',
            'pool' => 'Pool',
            'other_facilities' => 'Other Facilities',
            'completion_status' => 'Completion Status',
            'occupancy_status' => 'Occupancy Status',
            'no_of_bedrooms' => 'No. Of Bedrooms',
            'no_of_bathrooms' => 'No. Of Bathrooms',
            'no_four_bedrooms' => 'Number Of Four Bedrooms',
            'no_penthouse' => 'Number Of Penthouse',
            'full_building_floors' => 'Full Building Floors',
            'parking_floors' => 'Parking Floors',
            'location_highway_drive' => 'Drive to Highway/Main Road',
            'location_school_drive' => 'Drive to School/University',
            'location_mall_drive' => 'Drive to Mall/Commercial Centrel',
            'location_sea_drive' => 'Drive to Special Landmark like Sea, Beach etc.',
            'location_park_drive' => 'Drive to Pool and Park',
            'view_community' => 'Community View',
            'view_pool' => 'Pool / Fountain View',
            'view_park' => 'Park View',
            'view_burj' => 'Burj View',
            'view_sea' => 'Sea View',
            'view_lake' => 'Lake View',
            'view_golf_course' => 'Golf Course View',
            'view_marina' => 'Marina / Creek View',
            'view_mountains' => 'Mountains View',
            'view_special' => 'Special View',
            'landscaping' => 'Landscaping',
            'fridge' => 'Number of Fridges',
            'oven' => 'Number of Ovens',
            'cooker' => 'Cooker',
            'furnished' => 'Furnished',
            'ac_type' => 'Ac Type',
            'washing_machine' => 'Washing Machine',
            'dish_washer' => 'Dish Washer',
            'white_goods' => 'White Goods',
            'utilities_connected' => 'Utilities Connected',
            'no_of_kitchen' => 'No. Of Kitchen',
            'no_of_living_area' => 'No. Of Living Area',
            'no_of_dining_area' => 'No. Of Dining Area',
            'no_of_maid_rooms' => 'No. Of Maid Rooms',
            'no_of_laundry_area' => 'No. Of Laundry Area',
            'no_of_store' => 'No. Of Store',
            'no_of_service_block' => 'No. Of Service Block',
            'no_of_garage' => 'No. Of Garage',
            'no_of_balcony' => 'No. Of Balcony',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
            'updated_by' => 'Updated By',
            'updated_by' => 'Updated By',
            'location_pin' => 'location_pin',
            'permitted_bua_check' => 'permitted_bua_check',
            'permitted_bua' => 'permitted_bua',
            'permitted_gfa' => 'permitted_gfa',
            'developer_id_property' => 'developer_id_property',
            'number_of_mezannines' => 'number_of_mezannines',
            'dish_washer' => 'dish_washer',
            'no_of_gym_room' => 'no_of_gym_room',
            'number_of_bbq_areas' => 'number_of_bbq_areas',
            'children_play_area' => 'children_play_area',
            'jacuzzi' => 'jacuzzi',
            'number_of_labor_rooms' => 'number_of_labor_rooms',
            'dish_washer' => 'dish_washer',
            'no_of_gym_room' => 'no_of_gym_room',
            'number_of_bbq_areas' => 'number_of_bbq_areas',
            'children_play_area' => 'children_play_area',
            'jacuzzi' => 'jacuzzi',
            'number_of_labor_rooms' => 'number_of_labor_rooms',
            'balcony_covered_with_roof' => 'balcony_covered_with_roof',
            'extension_permision_document' => 'extension_permision_document',
            'extension_completion_document' => 'extension_completion_document',
            'total_units' => 'total_units',
            'residential_units' => 'residential_units',
            'commercial_units' => 'commercial_units',
            'retail_units' => 'retail_units',
            'date_of_completion_certificate' => 'date_of_completion_certificate',
            'date_of_building_permit' => 'date_of_building_permit',
            'length_of_fence' => 'length_of_fence',
            'number_of_building_on_land' => 'number_of_building_on_land',
            'number_of_floors' => 'number_of_floors',
            'gated_community' => 'gated_community',
            'no_party_hall' => 'no_party_hall',
            'no_health_club_spa' => 'no_health_club_spa',
            'no_elevators' => 'no_elevators',
            'no_studios' => 'no_studios',
            'no_one_bedrooms' => 'no_one_bedrooms',
            'no_two_bedrooms' => 'no_two_bedrooms',
            'no_three_bedrooms' => 'no_three_bedrooms',
            'no_of_swimming_pool' => 'no_of_swimming_pool',
            'no_of_school' => 'no_of_school',
            'no_of_clinic' => 'no_of_clinic',
            'no_of_sports_court' => 'no_of_sports_court',
            'no_of_masjid' => 'no_of_masjid',
            'no_of_community_center' => 'no_of_community_center',
            'no_of_jacuzzi' => 'no_of_jacuzzi',
            'no_childrens_play_area' => 'no_childrens_play_area',
            'no_of_restaurants' => 'no_of_restaurants',
            'no_of_atm_machines' => 'no_of_atm_machines',
            'no_of_coffee_shops' => 'no_of_coffee_shops',
            'no_of_signboards' => 'no_of_signboards',
            'no_of_bars' => 'no_of_bars',
            'no_of_night_clubs' => 'no_of_night_clubs',
            'municipality_number' => 'municipality_number',
            'no_four_bedrooms' => 'no_four_bedrooms',
            'no_penthouse' => 'no_penthouse',
            'nla' => 'nla',
            'built_up_area_m' => 'built_up_area_m',
            'plot_number_verify' => 'plot_number_verify',
            'street_verify' => 'street_verify',
            'unit_number_verify' => 'unit_number_verify',
            'location_pin_verify' => 'location_pin_verify',
            'community_verify' => 'community_verify',
            'sub_community_verify' => 'sub_community_verify',
            'city_verify' => 'city_verify',
            'country_verify' => 'country_verify',
            'estimated_age_month' => 'estimated_age_month',
            'number_of_parking_floors' => 'number_of_parking_floors',
            'no_studios_nla' => 'no_studios_nla',
            'no_one_bedrooms_nla' => 'no_one_bedrooms_nla',
            'no_two_bedrooms_nla' => 'no_two_bedrooms_nla',
            'no_three_bedrooms_nla' => 'no_three_bedrooms_nla',
            'no_four_bedrooms_nla' => 'no_four_bedrooms_nla',
            'no_penthouse_nla' => 'no_penthouse_nla',
            'no_of_shops' => 'no_of_shops',
            'no_of_offices' => 'no_of_offices',
            'no_of_shops_nla' => 'no_of_shops_nla',
            'no_of_offices_nla' => 'no_of_offices_nla',
            'studios_summary' => 'studios_summary',
            'one_bedrooms_summary' => 'one_bedrooms_summary',
            'two_bedrooms_summary' => 'two_bedrooms_summary',
            'three_bedrooms_summary' => 'three_bedrooms_summary',
            'four_bedrooms_summary' => 'four_bedrooms_summary',
            'penthouse_summary' => 'penthouse_summary',
            'shops_summary' => 'shops_summary',
            'offices_summary' => 'offices_summary',
            'no_warehouses' => 'no_warehouses',
            'no_warehouses_nla' => 'no_warehouses_nla',
            'warehouse_summary' => 'warehouse_summary',
            'not_complete_reason' => 'not_complete_reason',
        ];
    }
    public function beforeSave($insert)
    {

        $valuation = Valuation::find()->where(['id' => $this->valuation_id])->one();
        $model_detail = ValuationDetail::find()->where(['valuation_id' => $this->valuation_id])->one();
        $address_detail_check = 0;
        if($valuation->parent_id <> null){

        }else {
            if ($model_detail->plot_number != $this->plot_number_verify) {
                if ($model_detail->plot_number <> null) {
                    $address_detail_check = 1;
                    $msg = 'Plot Number Not Correct';
                }
            } else if (trim($model_detail->street) != trim($this->street_verify)) {
                if (trim($model_detail->street) <> null) {
                    $address_detail_check = 1;
                    $msg = 'Street Not Correct';
                }

            } else if ($model_detail->unit_number != $this->unit_number_verify) {
                if ($model_detail->unit_number <> null) {
                    $address_detail_check = 1;
                    $msg = 'Unit Number Not Correct';
                } else {
                  //  Yii::$app->db->createCommand()->update('valuation_detail', ['unit_number' => $this->unit_number_verify], 'id=' . $model_detail->id . '')->execute();
                }

            } else if ($model_detail->building->community != $this->community_verify) {
                if ($model_detail->building->community <> null) {
                    $address_detail_check = 1;
                    $msg = 'Community Not Correct';
                }

            }  else if ($model_detail->building->city != $this->city_verify) {
                if ($model_detail->building->city <> null) {
                    $address_detail_check = 1;
                    $msg = 'City Not Correct';
                }
            } else if (221 != $this->country_verify) {

                    $address_detail_check = 1;
                    $msg = 'Country Not Correct';

            }
            if ($this->location_pin_verify <> null) {
                $lat_array = explode(',', $this->location_pin_verify);
                if (isset($lat_array[0])) {
                    $lat1 = trim($lat_array[0]);
                }
                if (isset($lat_array[1])) {
                    $lon1 = trim($lat_array[1]);
                }

                $lat2 = $model_detail->latitude; // Latitude of location 2
                $lon2 = $model_detail->longitude;
                if ($lat1 <> null && $lon1 <> null && $lat2 <> null && $lon2 <> null) {
                    $distance = $this->actionDistance($lat1, $lon1, $lat2, $lon2);

                    if ($distance > 300) {
                        $address_detail_check == 1;
                        $msg = 'Location Distance Is More Than 300m.';
                    }
                } else {
                    $address_detail_check == 1;
                    $msg = 'Location Pin Is Not Correct ';
                }

            } else {
                if ($model_detail->building->city <> null) {
                    $msg = 'Location Pin Is Not Correct';
                }
            }

            if ($address_detail_check == 0) {
            } else {
                // if(Yii::$app->user->identity->id == 6327 || Yii::$app->user->identity->id == 1) {
                Yii::$app->getSession()->addFlash('error', Yii::t('app', $msg));
                return false;
                //  }
            }
        }
       /* $this->estimated_age = $valuation->building->estimated_age;
        $this->estimated_remaining_life = $valuation->property->age - $valuation->building->estimated_age;*/
        $this->estimated_remaining_life = $valuation->property->age - $this->estimated_age;
        if($this->estimated_age_month > 0){
            $this->estimated_remaining_life = $this->estimated_remaining_life -1;
        }
        if (!empty($this->other_facilities)) {
            if(is_array($this->other_facilities)) {
                $this->other_facilities = implode(",", $this->other_facilities);
            }
        }

        if (!empty($this->ac_type)) {
            if(is_array($this->ac_type)) {
                $this->ac_type = implode(",", $this->ac_type);
            }
        }
       /* echo $this->ac_type ;
        die;*/


        
        // verify status reset after edit only something changed
        $isChanged = false;
        $ignoredAttributes = ['updated_at','status_verified_at']; 
        foreach ($this->attributes as $attribute => $value) {
            if (in_array($attribute, $ignoredAttributes)) {
                continue;
            }
            if ($this->getOldAttribute($attribute) != $value) {
                $isChanged = true;
                break;
            }
        }
        if ($isChanged) {
            if($this->status == 1 && $this->getOldAttribute('status') == 1){
                $this->status = 2;
            }elseif ($this->status == 2 && $this->getOldAttribute('status') == 1) {
                $this->status = 1;
            }
        }
        if($this->valuation_id > 13860 && $valuation->id > 13860 && $valuation->parent_id == '' ) {
            $nla = 0;
            $number_of_unites = 0;
            $number_of_unites_res = 0;
            $number_of_unites_comm = 0;
            $number_of_unites_retail = 0;
            if ($this->no_studios > 0 && $this->no_studios_nla > 0) {
                $nla = $nla + ($this->no_studios_nla * $this->no_studios);
                $number_of_unites_res = $number_of_unites_res + $this->no_studios;
            }

            if ($this->no_one_bedrooms > 0 && $this->no_one_bedrooms_nla > 0) {
                $nla = $nla + ($this->no_one_bedrooms_nla * $this->no_one_bedrooms);
                $number_of_unites_res = $number_of_unites_res + $this->no_one_bedrooms;
            }


            if ($this->no_two_bedrooms > 0 && $this->no_two_bedrooms_nla > 0) {
                $nla = $nla + ($this->no_two_bedrooms_nla * $this->no_two_bedrooms);
                $number_of_unites_res = $number_of_unites_res + $this->no_two_bedrooms;
            }


            if ($this->no_three_bedrooms > 0 && $this->no_three_bedrooms_nla > 0) {
                $nla = $nla + ($this->no_three_bedrooms_nla * $this->no_three_bedrooms);
                $number_of_unites_res = $number_of_unites_res + $this->no_three_bedrooms;
            }

            if ($this->no_four_bedrooms > 0 && $this->no_four_bedrooms_nla > 0) {
                $nla = $nla + ($this->no_four_bedrooms_nla * $this->no_four_bedrooms);
                $number_of_unites_res = $number_of_unites_res + $this->no_four_bedrooms;
            }

            if ($this->no_penthouse > 0 && $this->no_penthouse_nla > 0) {
                $nla = $nla + ($this->no_penthouse_nla * $this->no_penthouse);
                $number_of_unites_res = $number_of_unites_res + $this->no_penthouse;
            }

            if ($this->no_of_shops > 0 && $this->no_of_shops_nla > 0) {
                $nla = $nla + ($this->no_of_shops_nla * $this->no_of_shops);
                $number_of_unites_retail = $number_of_unites_retail + $this->no_of_shops;
            }

            if ($this->no_of_offices > 0 && $this->no_of_offices_nla > 0) {
                $nla = $nla + ($this->no_of_offices_nla * $this->no_of_offices);
                $number_of_unites_comm = $number_of_unites_comm + $this->no_of_offices;
            }
            if ($this->no_warehouses > 0 && $this->no_warehouses_nla > 0) {
                $nla = $nla + ($this->no_warehouses_nla * $this->no_warehouses);
                $number_of_unites_comm = $number_of_unites_comm + $this->no_warehouses;
            }
            $this->nla = $nla;
            $this->total_units = $number_of_unites_res + $number_of_unites_comm + $number_of_unites_retail;
            $this->residential_units = $number_of_unites_res;
            $this->commercial_units = $number_of_unites_comm;
            $this->retail_units = $number_of_unites_retail;
        }



        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }
    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->valuation_id;
    }

    public function afterSave($insert, $changedAttributes)
    {
        if(isset($this->towers_data) && ($this->towers_data <> null)) {
            ReTowersData::deleteAll(['valuation_id' => $this->valuation_id]);
            // Save ALL banners Images
            foreach ($this->towers_data as $tower_data) {

                $tower = new ReTowersData();
                $tower->title = $tower_data['title'];
                $tower->gfa = $tower_data['gfa'];
                $tower->bua = $tower_data['bua'];
                $tower->floor_details = $tower_data['floor_details'];
                $tower->makani_number = $tower_data['makani_number'];
                $tower->unit_number = $tower_data['unit_number'];
                $tower->valuation_id = $this->valuation_id;
                if(!$tower->save()){
                    echo "<pre>";
                    print_r($tower);
                    die;
                }

            }
        }
        if(isset($this->customAttachments) && ($this->customAttachments <> null)) {
          
           /* CustomAttachements::deleteAll(['valuation_id' => $this->valuation_id]);*/
            // Save ALL banners Images
            foreach ($this->customAttachments as $attachment) {
                if($attachment['name'] != '' && $attachment['quantity'] !='') {
                    $model_saved = CustomAttachements::findOne($attachment['id']);
                    if ($model_saved !== null) {
                        $model_saved->name = $attachment['name'];
                        $model_saved->quantity = $attachment['quantity'];
                        $model_saved->save();
                    } else {
                        $customAttachments = new CustomAttachements();
                        $customAttachments->name = $attachment['name'];
                        $customAttachments->quantity = $attachment['quantity'];
                        $customAttachments->valuation_id = $this->valuation_id;
                        $customAttachments->save();
                    }
                }
            }
        }

        $lon=  $this->longitude;
        $lan = $this->latitude;
        $map_image_src = "https://maps.googleapis.com/maps/api/staticmap?zoom=11&size=900x370&maptype=hybrid&markers=color:red%7Clabel:B%7C".trim($lan).",".trim($lon)."&key=AIzaSyBTynqp2XN1b-DqWAysJzwOoitsaD-BdPs";
        $Sateliteimage_src="https://maps.googleapis.com/maps/api/staticmap?zoom=11&size=900x370&maptype=roadmap&markers=color:red%7Clabel:B%7C".trim($lan).",".trim($lon)."&key=AIzaSyBTynqp2XN1b-DqWAysJzwOoitsaD-BdPs";

        $img_map = 'map/'.$this->valuation_id.'_m.png';
      //  if(!file_exists($img_map)){
            file_put_contents($img_map, file_get_contents($map_image_src));
       // }
        $img_satelite = 'map/'.$this->valuation_id.'_s.png';
       // if(!file_exists($img_satelite)){
            file_put_contents($img_satelite, file_get_contents($Sateliteimage_src));
        //}

        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }

    public function getCustomAttachements()
    {
        return $this->hasMany(CustomAttachements::className(), ['valuation_id' => 'valuation_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommunities()
    {
        return $this->hasOne(Communities::className(), ['id' => 'community_verify']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubCommunities()
    {
        return $this->hasOne(SubCommunities::className(), ['id' => 'sub_community_verify']);
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Inspect Property';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeveloper()
    {
        return $this->hasOne(Developers::className(), ['id' => 'developer_id']);
    }

    public function getPropertyDeveloper()
    {
        return $this->hasOne(Developers::className(), ['id' => 'developer_id_property']);
    }
    public function actionDistance($lat1, $lon1, $lat2, $lon2) {
        $earthRadius = 6371000; // Radius of the Earth in meters

        $lat1 = deg2rad($lat1);
        $lon1 = deg2rad($lon1);
        $lat2 = deg2rad($lat2);
        $lon2 = deg2rad($lon2);

        $dlat = $lat2 - $lat1;
        $dlon = $lon2 - $lon1;

        $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlon / 2) * sin($dlon / 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

        $distance = $earthRadius * $c; // Distance in meters

        return round($distance);
    }

}
