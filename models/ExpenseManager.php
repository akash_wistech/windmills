<?php

namespace app\models;

use Yii;
use app\models\AssetManager;

// dd($_REQUEST);

class ExpenseManager extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'expense_manager';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['action', 'name', 'purpose', 'category_id', 'category_name' , 'depriciation', 'priority', 'frequency',
            'first_supplier','second_supplier','third_supplier','image_1','image_2','image_3',
            'applicant','department','approved_by','approvers_dept','asset_owner','entered_date',
            'approved_date','purchasing_date','target_date','collection_date','meeting_date',
            'start_date','expiry_date','frequency_start','frequency_end','status','amount','asset_life'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'action' => 'action',
            'name' => 'name',
            'purpose' => 'purpose',
            'category_id' => 'category_id',
            'category_name' => 'category_name',
            'depriciation' => 'depriciation',
            'priority' => 'priority',
            'frequency' => 'frequency',
            'first_supplier' => 'first_supplier',
            'second_supplier' => 'second_supplier',
            'third_supplier' => 'third_supplier',
            'image_1' => 'image_1',
            'image_2' => 'image_2',
            'image_3' => 'image_3',
            'applicant' => 'applicant',
            'department' => 'department',
            'approved_by' => 'approved_by',
            'approvers_dept' => 'approvers_dept',
            'asset_owner' => 'asset_owner',
            'entered_date' => 'entered_date',
            'approved_date' => 'approved_date',
            'purchasing_date' => 'purchasing_date',
            'target_date' => 'target_date',
            'collection_date' => 'collection_date',
            'meeting_date' => 'meeting_date',
            'start_date' => 'start_date',
            'expiry_date' => 'expiry_date',
            'frequency_start' => 'frequency_start',
            'frequency_end' => 'frequency_end',
            'status' => 'status',
        ];
    }

    public function afterSave(){

        if( !empty($this->total_time) &&  !empty($this->start_time) &&  !empty($this->end_time) &&  $this->total_time > 0 &&  $this->status == "approved"  )
        {

            $user_detail = User::find()->where(['id' => $this->applicant])->one();
            $userName = $user_detail->lastname; 

            $userEmirate = UserProfileInfo::find()
            ->select(['visa_emirate'])
            ->where(['user_id' => $this->applicant])
            ->one();

            $branch_id = Yii::$app->appHelperFunctions->getSetting('zohobooks_dubai_id');
            //dubai
            if( $userEmirate->visa_emirate == "3510" ){
                $branch_id = Yii::$app->appHelperFunctions->getSetting('zohobooks_dubai_id');
            }
            //abu dhabi
            if( $userEmirate->visa_emirate == "3506" ){ 
                $branch_id = Yii::$app->appHelperFunctions->getSetting('zohobooks_abu_dhabi_id');
            }
            //ajman
            if( $userEmirate->visa_emirate == "3507" ){
                $branch_id = Yii::$app->appHelperFunctions->getSetting('zohobooks_ajman_id');
            }

            Yii::$app->appHelperFunctions->ZohoRefereshToken();

            $AccessToken = Yii::$app->appHelperFunctions->getSetting('zohobooks_access_token');
            $OrganizationID = Yii::$app->appHelperFunctions->getSetting('zohobooks_organization_id');
            
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://www.zohoapis.com/books/v3/chartofaccounts?organization_id='.$OrganizationID.'&filter_by=AccountType.Expense',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Zoho-oauthtoken' . ' ' . $AccessToken,
                ),
            ));
            
            $response = curl_exec($curl);
            curl_close($curl);
            $data = json_decode($response, true);

            if ($data['code'] === 0 && isset($data['chartofaccounts'])) {
                
                $filteredData = array_filter($data['chartofaccounts'], function ($account) {
                    return $account['parent_account_name'] === 'Overtime Expenses';
                });

                $filteredData = array_filter($filteredData, function ($account) use ($userName) {
                    return strpos($account['account_name'], $userName) !== false;
                });

                foreach ($filteredData as $account) {
                    $accountID = $account['account_id'];
                }

            } else {
                echo "No data found or an error occurred.";
            }

            $credit_account = "4631214000000102334";

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://www.zohoapis.com/books/v3/journals?organization_id='.$OrganizationID,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('JSONString' => '{
                    "journal_date": "'.date("Y-m-d").'",
                    "reference_number": "Overtime - ' . $userName . '-' .date("m", strtotime(date("Y-m-d"))).'/'.date("Y", strtotime(date("Y-m-d"))).'",
                    "notes": "'.$this->purpose.'",
                    "branch_id": '.$branch_id.',
                    "line_items": [
                        {
                            "account_id": "'.$accountID.'",
                            "description": "'.$userName.' - Overtime",
                            "amount": '.$this->val_amount.',
                            "debit_or_credit": "debit",
                        },
                        {
                            "account_id": "'.$credit_account.'",
                            "description": "'.$userName.' - Overtime",
                            "amount": '.$this->val_amount.',
                            "debit_or_credit": "credit",
                        }
                    ],

                    "status": "published",
                }'),
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Zoho-oauthtoken'.' '.$AccessToken,
                ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            $response = json_decode($response);

            if(!empty($response->journal->journal_id)){
                Yii::$app->getSession()->addFlash('success', Yii::t('app', $response->message));
            }else{
                Yii::$app->getSession()->addFlash('error', Yii::t('app', $response->message));
            }

        }if( isset($this->accounting_entry) && $this->accounting_entry == 'new' && $this->status == "approved" )
        {
            Yii::$app->appHelperFunctions->ZohoRefereshToken();
            $AccessToken = Yii::$app->appHelperFunctions->getSetting('zohobooks_access_token');
            $OrganizationID = Yii::$app->appHelperFunctions->getSetting('zohobooks_organization_id');
            $branch_id = Yii::$app->appHelperFunctions->getSetting('zohobooks_dubai_id');

            $dateTime = \DateTime::createFromFormat('Y/m/d H:i:s', $this->entered_date);
            $date = $dateTime->format('Y-m-d');

            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://www.zohoapis.com/books/v3/items?organization_id='.$OrganizationID,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array('JSONString' => '{
                "account_id": "'.$this->category_id.'",
                "branch_id": "'.$branch_id.'",
                "date": "'.$date.'",
                "amount": "'.$this->amount.'",
                "paid_through_account_id": "'.$this->pay_bank.'",
                "reference_number": "'.$this->purpose.'",
                "vendor_id": "'.$this->vendor_id.'",
            }'),
            CURLOPT_HTTPHEADER => array(
                'Authorization: Zoho-oauthtoken'.' '.$AccessToken,
            ),
            ));
    
            $response = curl_exec($curl);
    
            curl_close($curl);
            $response = json_decode($response);

            $query =  Yii::$app->db->createCommand()->update('expense_manager', ['zoho_expense_id' => $response->expense->expense_id], 'id = '.$this->id.'')->execute();
            if(!empty($response->expense->expense_id)){
                Yii::$app->getSession()->addFlash('success', Yii::t('app', $response->message));
            }else{
                Yii::$app->getSession()->addFlash('error', Yii::t('app', $response->message));
            }
        }else{
            Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information Uploaded Successfully!'));
        }


        // echo "<pre>";
        // print_r($this);
        // $valuation = Valuation::find()->where(['id' => $this->valuation_id])->one();
    }
}
