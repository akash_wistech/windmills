<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Invoice;

/**
 * This is the model class for table "valuation".
 *
 * @property int $id
 * @property string $reference_number
 * @property int $client_id
 * @property string|null $client_reference
 * @property string $client_name_passport
 * @property int $no_of_owners
 * @property int $service_officer_name
 * @property string $instruction_date
 * @property string $target_date
 * @property int $building_info
 * @property int $property_id
 * @property int $property_category
 * @property int $community
 * @property int $sub_community
 * @property int $tenure
 * @property int $unit_number
 * @property int $city
 * @property string $payment_plan
 * @property int $status
 * @property int $building_number
 * @property int $plot_number
 * @property string $street
 * @property int $floor_number
 * @property int $instruction_person
 * @property float $land_size
 * @property int $purpose_of_valuation
 * @property int|null $created_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class Valuation extends ActiveRecordFull
{
    public $step,$total,$total_valuations,$client_revenue,$allvalues,$client_type,$zone_name,$time_period,
        $tat,$low_vals,$high_vals,$errors_vals,$parent_estimated_market_value,$percentage_value,$estimated_market_value,$total_inspections,$years,$months,$month_number,
        $submission_date,$inspection_date,$city_filter,$mv_psf,$rv_psf,$step_number,$total_valuations_avg,$inspection_officer_name,$inspection_time,$valuation_month,$valuation_year;
        public $client_title,$quarter,$half_year,$year;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'valuation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reference_number', 'client_id', 'client_name_passport', 'service_officer_name', 'instruction_date', 'target_date', 'building_info', 'property_id', 'property_category', 'tenure', 'unit_number', 'building_number', 'plot_number', 'street', 'floor_number', 'land_size', 'purpose_of_valuation','no_of_owners','valuation_scope','email_subject','inspection_type','client_fixed_fee_check'], 'required'],
            [['client_id', 'service_officer_name', 'building_info', 'property_id', 'property_category', 'tenure', 'status', 'floor_number', 'purpose_of_valuation', 'created_by', 'updated_by', 'trashed', 'trashed_by','no_of_owners','other_instructing_person'], 'integer'],
            [['instruction_date', 'target_date', 'created_at', 'updated_at', 'trashed_at','special_assumption','extent_of_investigation','market_summary','approval_id','approval_status','root_id','signature_img', 'plot_number', 'unit_number', 'quotation_id','valuation_status','email_status','step','ban_date','cancel_by','cancelled_approved_by','email_subject','total','quotation_property','general_assumption','other_intended_users','revised_reason','total_valuations','fee', 'client_revenue','allvalues','time_period','inspection_type','total_inspections','city_filter','quickbooks_invoice_id','step_number', 'building_number','signature_doc','month_number','client_fixed_fee_check'], 'safe'],
            [['payment_plan'], 'string'],
            [['land_size'], 'number'],
            [['reference_number'], 'unique'],
            [['reference_number', 'client_name_passport'], 'string', 'max' => 100],
            [['client_reference', 'street'], 'string', 'max' => 255],
            [['link_to_scrape','link_to_scrape_status','b_to_rent_link_to_scrape','b_to_rent_link_to_scrape_status','pf_for_sale_scrape_url','pf_for_sale_scrape_url_status','pf_to_rent_scrape_url','pf_to_rent_scrape_url_status','client_invoice_type','title_deed','taqyeem_number'], 'safe'],
            [['client_title','quarter','half_year','year','total_fee','quickbooks_invoice_id_final','reference_number_final'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reference_number' => 'Reference Number',
            'client_id' => 'Client',
            'client_reference' => 'Client Reference',
            'client_name_passport' => 'Client Name (Passport)',
            'no_of_owners' => 'No Of Owners',
            'service_officer_name' => 'Service Officer Name',
            'instruction_date' => 'Instruction Date',
            'target_date' => 'Target Date',
            'building_info' => 'Building',
            'property_id' => 'Property',
            'property_category' => 'Property Category',
            'community' => 'Community',
            'sub_community' => 'Sub Community',
            'tenure' => 'Tenure',
            'unit_number' => 'Unit Number',
            'city' => 'City',
            'payment_plan' => 'Payment Plan',
            'status' => 'Status',
            'building_number' => 'Building Number',
            'plot_number' => 'Plot Number',
            'street' => 'Street',
            'floor_number' => 'Floor Number',
            //'instruction_person' => 'Instruction Person',
            'land_size' => 'Land Size',
            'purpose_of_valuation' => 'Purpose Of Valuation',
            'signature_img' => 'Add Report Signature Copy',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }

    public function beforeValidate()
    {
        if($this->client_id != 1){
            $this->client_fixed_fee_check = 1;
        }
        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    public static function getTotal($provider, $fieldName)
    {



        $total = 0;
        foreach ($provider as $item) {
           if($item[$fieldName] > 0) {
             //  echo $item[$fieldName].'<br>';

               $total +=  $item[$fieldName];
              // $total +=  (int)Yii::$app->appHelperFunctions->getClientRevenue($item[$fieldName]);
           }
        }
       // die;

        return $total;
    }
    public function getCompanyFeeStructure()
    {
        return $this->hasOne(CompanyFeeStructure::className(), ['company_id' => 'client_id']);
    }
    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Valuation';
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->reference_number;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Company::className(), ['id' => 'client_id']);
    }

    /**
 * @return \yii\db\ActiveQuery
 */
    public function getBuilding()
    {
        return $this->hasOne(Buildings::className(), ['id' => 'building_info']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Properties::className(), ['id' => 'property_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspectProperty()
    {
        return $this->hasOne(InspectProperty::className(), ['valuation_id' => 'id']);
    }
    public function getScheduleInspection(){
        return $this->hasOne(ScheduleInspection::className(),['valuation_id'=>'id']);
    }

    public function getCostDetails(){
        return $this->hasOne(CostDetails::className(),['valuation_id'=>'id']);
    }

    public function getValuationConfiguration(){
        return $this->hasOne(ValuationConfiguration::className(),['valuation_id'=>'id']);
    }

    public function getValuationDeveloperMargin(){
        return $this->hasOne(ValuationDeveloperDriveMargin::className(),['valuation_id'=>'id']);
    }
    public function upload($image='', $folderName='')
    {
        $uploadObject = Yii::$app->get('s3bucket')->upload($folderName.'/images/'. $image->baseName . '.' . $image->extension,$image->tempName);
        if ($uploadObject) {
            // check if CDN host is available then upload and get cdn URL.
            if (Yii::$app->get('s3bucket')->cdnHostname) {
                $url = Yii::$app->get('s3bucket')->getCdnUrl($image );
            } else {
                $url = Yii::$app->get('s3bucket')->getUrl($folderName.'/images/'.$image );
            }
        }
        return true;
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'approval_id']);
    }

    public function getApprover()
    {
        return $this->hasOne(User::className(), ['id' => 'service_officer_name']);
    }
    public function getApproverData(){
        return $this->hasOne(ValuationApproversData::className(),['valuation_id'=>'id'])->andWhere(['approver_type'=>'approver']);
    }

    public function getReviewerData(){
        return $this->hasOne(ValuationApproversData::className(),['valuation_id'=>'id'])->andWhere(['approver_type'=>'reviewer']);
    }
    public function getUserbycreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    public function getValuationParent(){
        return $this->hasOne(Valuation::className(),['id'=>'parent_id']);
    }
    public function getcancelReasons(){
        return $this->hasMany(InspectionCancelReasonsLog::className(),['valuation_id'=>'id']);
    }
    public function getcancelReasonsone(){
        return $this->hasOne(InspectionCancelReasonsLog::className(),['valuation_id'=>'id']);
    }

    public function getQuotaionData(){
        return $this->hasOne(CrmQuotations::className(),['id'=>'quotation_id']);
    }


    public function afterSave($insert, $changedAttributes)
    {

        if($this->id > 2163 ) {

            // $this->valuation_status = 1;
            if(isset($this->quotation_id) && $this->quotation_id <> null && $this->parent_id < 1 && $this->id != 5275) {

            }else {
                $sequence = array_search($this->valuation_status, Yii::$app->appHelperFunctions->getStepsSequence(), true);
              //  $rateAndItemAmount = Yii::$app->appHelperFunctions->getRateAndItemAmount($this->client->id, $this->building->city, $this->inspection_type, $this->tenure);
                if($this->client_fixed_fee_check == 2 && $this->client_id== 1){
                    $rateAndItemAmount = $this->client->client_fixed_fee;
                }else{
                    $rateAndItemAmount = Yii::$app->appHelperFunctions->getRateAndItemAmount($this->client->id, $this->building->city, $this->inspection_type, $this->tenure);
                }
                $query2 = Yii::$app->db->createCommand()
                    ->update('valuation', ['fee' => $rateAndItemAmount], 'id = ' . $this->id . '')
                    ->execute();
                $query2 = Yii::$app->db->createCommand()
                    ->update('valuation', ['total_fee' => $rateAndItemAmount], 'id = ' . $this->id . '')
                    ->execute();
            }
            if ($sequence == 1 && $this->email_status !== 1) {
                // echo 'why is it in?';
                // die();
                $notifyData = [
                    'client' => $this->client,
                    'attachments' => [],
                    'subject' => $this->email_subject,
                    'uid' => $this->id,
                    'valuer' => $this->approver->email,
                    'replacements' => [
                        '{clientName}' => $this->client->title,
                    ],
                ];
               /* if($this->id == 5637){
                    echo "<pre>";
                    print_r($this->client_id );
                    print_r($this->client->primaryContact->email);
                    print_r($notifyData);
                    die;
                }*/

                  $allow_properties = [1,2,5,6,12,4,20,24,28,37,39,17];
                 if (in_array($this->property_id, $allow_properties) && $this->client_id != 87) {
               // if(isset($this->quotation_id) && $this->quotation_id !=602) {
                     if($this->id == 5637) {
                         \app\modules\wisnotify\listners\NotifyEvent::firetest('Received.Valuation', $notifyData);
                     }else {
                         \app\modules\wisnotify\listners\NotifyEvent::fire1('Received.Valuation', $notifyData);
                     }
               // }
                }
                //   UPDATE (table name, column values, condition)
                Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 1], 'id=' . $this->id . '')->execute();
                Yii::$app->db->createCommand()->update('valuation', ['email_status' => 1], 'id=' . $this->id . '')->execute();

            }
            if((isset($this->quotation_id) && $this->quotation_id <> null && $this->parent_id < 1 && $this->id != 5275)  ) {

                //Normal Quickbooks process
                $ClientID = Yii::$app->appHelperFunctions->getSetting('quickbook_client_id');
                $ClientSecret = Yii::$app->appHelperFunctions->getSetting('quickbook_client_secret');
                $accessTokenKey = Yii::$app->appHelperFunctions->getSetting('quickbook_access_token');
                $refreshTokenKey = Yii::$app->appHelperFunctions->getSetting('quickbook_refresh_token');
                $QBORealmID = Yii::$app->appHelperFunctions->getSetting('quickbook_realm_id');

                $clientType = $this->client->client_type;
                if ($clientType == "bank") {
                    $addTwoMonth = date('d/m/Y', strtotime('+2 months'));
                    list($d, $m, $y) = explode("/", $addTwoMonth);
                    $dueDate = '01/' . $m . '/' . $y;
                    $dueDate = str_replace('/', '-', $dueDate);
                    $dueDate = date('Y-m-d', strtotime($dueDate));
                } else {
                    $dueDate = $this->instruction_date;
                }
                $type_service = "3";
                if ($this->inspection_type != 2) {
                    $type_service = "8";
                }
                // $invoiceDate = $this->instruction_date;
                     if($this->quotaionData->payment_status == 1){
                         $invoiceNumber = Yii::$app->appHelperFunctions->getInvoiceNumber($this).'-AP';
                     }else {
                         $invoiceNumber = Yii::$app->appHelperFunctions->getInvoiceNumber($this);
                     }
                     $invoiceDate = date('Y-m-d', strtotime($this->instruction_date));
                $memo = $this->client_reference;
                $item = Yii::$app->appHelperFunctions->inspectionTypeArr[$this->inspection_type];
                $customer = $this->client->title;
                $qty = "1";
                $type = $this->property->title;

                $saleLocation = $this->city;
                $valuerName = Yii::$app->appHelperFunctions->staffMemberListArr[$this->service_officer_name];
               // $taxCodeId = Yii::$app->appHelperFunctions->qbLocationsTax[$this->building->city];
                //$location = Yii::$app->appHelperFunctions->qbLocations[$this->building->city];

                if($this->building->city == '4260'){
                    $taxCodeId = 6;
                    $location = 'AbuDhabi';
                }else{
                    $taxCodeId = Yii::$app->appHelperFunctions->qbLocationsTax[$this->building->city];
                    $location = Yii::$app->appHelperFunctions->qbLocations[$this->building->city];
                }
                $vat_amount = 0;
                if(isset($this->quotation_id) && $this->quotation_id <> null){
                    $rateAndItemAmount = $this->fee;
                    $vat_amount = yii::$app->quotationHelperFunctions->getVatTotal_add( $this->fee);

                  //  if($this->client_id == 1) {
                    if($this->client_id == 1) {
                        $rateAndItemAmount = $rateAndItemAmount - $vat_amount;
                    }

                }else {

                    $rateAndItemAmount = Yii::$app->appHelperFunctions->getRateAndItemAmount($this->client->id, $this->building->city, $this->inspection_type, $this->tenure);

                    $vat_amount = $rateAndItemAmount * 0.05;
                }
                $valuer = (isset($this->service_officer_name) && ($this->service_officer_name <> null)) ? ($this->approver->firstname . ' ' . $this->approver->lastname) : '';
// print_r($rateAndItemAmount); die();

               if($this->quotation_id <> null){
                    $quotation_data = CrmQuotations::findOne($this->quotation_id);
                    $quotation_fee_total = Valuation::find()->where(['quotation_id' => $this->quotation_id])->sum('fee');
                    $valuations = Valuation::find()->where(['quotation_id' => $this->quotation_id])->all();
                    $discount =  yii::$app->quotationHelperFunctions->getDiscountRupee($quotation_fee_total,$quotation_data->relative_discount_toe);
                }else {

                        $quotation_fee_total = Yii::$app->appHelperFunctions->getClientRevenue($this->id);

                    $valuations = Valuation::find()->where(['id' => $this->id])->all();
                }




               $items = array();
                $quotation_fee_total = Valuation::find()->where(['quotation_id' => $this->quotation_id])->sum('fee');
                $vat_amount_total = yii::$app->quotationHelperFunctions->getVatTotal_add($quotation_fee_total);
                if ($this->client_id == 1) {
                    $rateAndItemAmount = $quotation_fee_total - $vat_amount_total;
                }else{
                    $rateAndItemAmount =  $quotation_fee_total;
                }
                $counter =0;
                    foreach ($valuations as $key => $valuation){
                        $counter++;
                        $fee_per_item = $valuation->fee;
                        $vat_amount = yii::$app->quotationHelperFunctions->getVatTotal_add($valuation->fee);

                        //  if($this->client_id == 1) {
                        if ($this->client_id == 1) {
                            $fee_per_item = $valuation->fee - $vat_amount;
                        }
                        $description = $valuation->reference_number . ', '.$valuation->property->title . ', ' . $valuation->building->title . ', ' . $valuation->community;

                        $item = array (
                            "Description" => $description,
                            "DetailType" => "SalesItemLineDetail",

                            "SalesItemLineDetail" => [
                                "TaxCodeRef" => [
                                    "value" => $taxCodeId
                                ],
                                "Qty" => 1,
                                "UnitPrice" => $fee_per_item,
                                "ServiceDate" => $invoiceDate,//"2013-03-04",
                                "ItemRef" => [
                                    "name" => "Service",
                                    "value" => $type_service
                                ]
                            ],
                            "LineNum" => $counter,
                            "Amount" => $fee_per_item,
                            "Id" => "$counter"
                        );
                        $items[]=$item;
                    }
                $items[]=array(
                    "DetailType" => "SubTotalLineDetail",
                    "Amount" => $rateAndItemAmount,
                    "SubTotalLineDetail" => []
                );
                $params = [
                    "TxnDate" => $invoiceDate, //additional
                    "DueDate" => $dueDate, //additional
                    "domain" => "QBO",
                    "DocNumber" => $invoiceNumber,
                    "CustomerMemo" => [
                        "value" => 'thankyou for contacting us!',//$memo, //additional
                    ],

                    "Line" => $items,
                    "TxnTaxDetail" => [
                        "TxnTaxCodeRef" => [
                            "value" => $taxCodeId,
                        ],
                        "TotalTax" => ($vat_amount),
                    ],
                    "PrivateNote" => ($this->client_reference <> null)? $this->client_reference : $this->client->title,

                    "CustomerRef" => [
                        "name" => $this->client->title,
                        "value" => $this->client->quickbooks_registration_id,
                    ],
                    "BillEmail" => [
                        "Address" => $this->client->primaryContact->email,
                    ],
                    "BillEmailCc" => [
                        "Address" => $this->client->primaryContact->email,
                    ],
                    "BillEmailBcc" => [
                        "Address" => $this->client->primaryContact->email,
                    ],
                    "CustomField" => [
                        [
                            "DefinitionId" => "1",
                            "StringValue" => $valuer,
                            "Type" => "StringType",
                            "Name" => "Valuer Name"
                        ],
                        [
                            "DefinitionId" => "1",
                            "StringValue" => $valuer,
                            "Type" => "StringType",
                            "Name" => "Valuer Name"
                        ],

                    ],
                    "TransactionLocationType" => $location

                ];

                //echo "<pre>"; print_r($params); echo "</pre>"; die();

                $dataService = Yii::$app->appHelperFunctions->getDataServiceObject();
                /*  if($this->id == 2234){
                   //   $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
                    //  $dataService->throwExceptionOnError(true);
                     // $dataService = Yii::$app->appHelperFunctions->refreshTokens($dataService);
                     // $invoice = $dataService->FindbyId('invoice', 8605);
                     // $theResourceObj = Invoice::create($params);
                     // $resultingObj = $dataService->Add($theResourceObj);
                      echo "<pre>";
                      print_r($params);
                      die;
                  }*/
                $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
                $dataService->throwExceptionOnError(true);
              /*   if ($this->quickbooks_invoice_id == '') {

                     $dataService = Yii::$app->appHelperFunctions->refreshTokens($dataService);
                     $invoice = $dataService->FindbyId('invoice', 8573);

                     echo "<pre>";
                     print_r($invoice);
                     die;
                 }else{
                     $dataService = Yii::$app->appHelperFunctions->refreshTokens($dataService);
                     $invoice = $dataService->FindbyId('invoice', 8573);

                     echo "<pre>";
                     print_r($invoice);
                     die;
                 }*/
                $quotation_id = Valuation::find()->where(['quotation_id' => $this->quotation_id])->andWhere(['not', ['quickbooks_invoice_id' => null]])->one();
                if($quotation_id->quickbooks_invoice_id <> null ){
                    $this->quickbooks_invoice_id = $quotation_id->quickbooks_invoice_id;
                }
                if ($this->quickbooks_invoice_id == '') {
                    // get new token
                    $dataService = Yii::$app->appHelperFunctions->refreshTokens($dataService);
                    //Add a new Invoice
                    $theResourceObj = Invoice::create($params);
                    $resultingObj = $dataService->Add($theResourceObj);
                } else {
                    // get new token
                    $dataService = Yii::$app->appHelperFunctions->refreshTokens($dataService);
                    // update an existing invoice
                    $invoice = $dataService->FindbyId('invoice', $this->quickbooks_invoice_id);
                    $theResourceObj = Invoice::update($invoice, $params);
                    $resultingObj = $dataService->Update($theResourceObj);
                }

                //get if any error
                $error = $dataService->getLastError();
                if ($error) {
                    // echo "<pre>"; print_r($error); echo "</pre>"; die();
                    echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
                    echo "<br>";
                    echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                    echo "<br>";
                    echo "The Response message is: " . $error->getResponseBody() . "\n";
                    echo "<br>";
                    die();
                } else {
                    if ($this->client_id == 178) {
                        /*  echo "<pre>";
                          print_r($resultingObj);
                          die;*/
                    }
                    $quotation_id = Valuation::find()->where(['quotation_id' => $this->quotation_id])->andWhere(['not', ['quickbooks_invoice_id' => null]])->one();
                    if($quotation_id->quickbooks_invoice_id <> null ){
                        $this->quickbooks_invoice_id = $quotation_id->quickbooks_invoice_id;
                        $query = Yii::$app->db->createCommand()
                            ->update('valuation', ['quickbooks_invoice_id' => $quotation_id->quickbooks_invoice_id], 'quotation_id = ' . $this->quotation_id . '')
                            ->execute();
                    }
                    if ($this->quickbooks_invoice_id== '') {

                            $query = Yii::$app->db->createCommand()
                                ->update('valuation', ['quickbooks_invoice_id' => $resultingObj->Id], 'quotation_id = ' . $this->quotation_id . '')
                                ->execute();

                        if (isset($this->quotation_id) && $this->quotation_id <> null) {

                        } else {
                            $query2 = Yii::$app->db->createCommand()
                                ->update('valuation', ['fee' => $rateAndItemAmount], 'id = ' . $this->id . '')
                                ->execute();
                        }
                        // echo "<pre>"; print_r($resultingObj); echo "</pre>"; die();
                        Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Invoice Created in QuickBooks'));
                    } else {
                        Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Invoice Updated in QuickBooks'));
                        if (isset($this->quotation_id) && $this->quotation_id <> null) {

                        } else {
                            $query = Yii::$app->db->createCommand()
                                ->update('valuation', ['fee' => $rateAndItemAmount], 'id = ' . $this->id . '')
                                ->execute();
                        }
                    }

                }
            }else{
                if($this->client_fixed_fee_check == 2 && $this->client_id== 1){
                    $rateAndItemAmount = $this->client->client_fixed_fee;
                }else{
                    $rateAndItemAmount = Yii::$app->appHelperFunctions->getRateAndItemAmount($this->client->id, $this->building->city, $this->inspection_type, $this->tenure);
                }
               // $rateAndItemAmount = Yii::$app->appHelperFunctions->getRateAndItemAmount($this->client->id, $this->building->city, $this->inspection_type, $this->tenure);
                $query2 = Yii::$app->db->createCommand()
                    ->update('valuation', ['fee' => $rateAndItemAmount], 'id = ' . $this->id . '')
                    ->execute();
                $query2 = Yii::$app->db->createCommand()
                    ->update('valuation', ['total_fee' => $rateAndItemAmount], 'id = ' . $this->id . '')
                    ->execute();
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }
}
