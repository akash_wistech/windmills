<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AciveZone;

/**
 * CommunitiesSearch represents the model behind the search form of `app\models\Communities`.
 */
class AciveZoneSearch extends AciveZone
{
    public $pageSize;
    public $city;
    public $country;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'country_id', 'zone_id'], 'integer'],
            [['city','country'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->generateQuery($params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
            ],
           // 'sort' => $this->defaultSorting,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        return $dataProvider;
    }
    /**
     * Generates query for the search
     *
     * @param array $params
     *
     * @return ActiveRecord
     */
    public function generateQuery($params)
    {
        $this->load($params);
        $query = AciveZone::find()
        ->select([
            AciveZone::tableName().'.id',
            Zone::tableName().'.title as city',
            Country::tableName().'.title as country',
        ])
        ->leftJoin(Zone::tableName(),Zone::tableName().'.id='.AciveZone::tableName().'.zone_id')
        ->leftJoin(Country::tableName(),Country::tableName().'.id='.AciveZone::tableName().'.country_id')
        ->asArray();


        // echo "<pre>"; print_r($this->city);  echo "<pre><br>";// die;
        // echo "<pre>"; print_r($this->country);  echo "<pre>"; die;

        // grid filtering conditions
        $query->andFilterWhere([
            AciveZone::tableName().'.id' => $this->id
        ])

        ->andFilterWhere(['like',Zone::tableName().'.title',$this->city])
        ->andFilterWhere(['like',Country::tableName().'.title',$this->country]);


        return $query;
    }
}
