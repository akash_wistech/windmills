<?php

namespace app\models;

use app\modules\wisnotify\listners\NotifyEvent;
use Yii;
use app\components\models\ActiveRecordFull;
use yii2tech\ar\softdelete\SoftDeleteBehavior;
use DateTime;
use DateInterval;
use DatePeriod;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;


/**
 * This is the model class for table "crm_quotations".
 *
 * @property int $id
 * @property string|null $reference_number
 * @property string|null $client_name
 * @property string|null $client_customer_name
 * @property string|null $scope_of_service
 * @property string|null $purpose_of_valuation
 * @property string|null $advance_payment_terms
 * @property int|null $no_of_properties
 * @property string|null $related_to_buyer
 * @property string|null $related_to_owner
 * @property string|null $related_to_client
 * @property string|null $related_to_property
 * @property string|null $related_to_buyer_reason
 * @property string|null $related_to_owner_reason
 * @property string|null $related_to_client_reason
 * @property string|null $related_to_property_reason
 * @property string|null $name
 * @property string|null $phone_number
 * @property string|null $email
 * @property float|null $recommended_fee
 * @property string|null $relative_discount
 * @property float|null $final_fee_approved
 * @property string|null $turn_around_time
 * @property string|null $valuer_name
 * @property string|null $date
 * @property string|null $quotation_status
 * @property string|null $payment_slip
 * @property string|null $toe_document
 * @property string|null $quotation_recommended_fee
 * @property string|null $quotation_turn_around_time
 * @property string|null $toe_final_fee
 * @property string|null $toe_final_turned_around_time
 * @property int $status
 * @property string|null $assumptions
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 */
class CrmQuotations extends ActiveRecordFull
{
    public $other;
    public $calculations;
    public $other_intended_users_check;
    public $total_records, $total_amount; // for today widgets dashboard
    public $client_nick_name,$crm_user_first_name,$crm_user_last_name,$crm_user_mobile,$crm_user_phone1; // for today widgets dashboard
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_quotations';
    }


    // public function behaviors()
    // {
    //     return [
    //         'softDeleteBehavior' => [
    //             'class' => SoftDeleteBehavior::className(),
    //             'softDeleteAttributeValues' => [
    //                 'deleted_at' => true
    //             ],
    //         ],

    //         // Other behaviors
    //         [
    //             'class' => TimestampBehavior::className(),
    //             'createdAtAttribute' => 'created_at',
    //             'updatedAtAttribute' => false,
    //             'value' => new Expression('NOW()'),
    //         ],
    //     ];
    // }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no_of_properties', 'status', 'created_by', 'updated_by', 'deleted_by', 'other_instructing_person', 'branch_id', 'instruction_person'], 'integer'],
            [['reference_number'], 'unique'],
            [['rec_advance_payment_terms', 'rec_no_of_units_same_building', 'rec_no_of_property_discount', 'rec_same_building_discount', 'rec_first_time_discount', 'rec_tat_requirements'], 'safe'],
            [['advance_payment_terms_final_amount', 'no_of_property_discount_final_amount', 'no_of_units_same_building_final_amount', 'first_time_discount_final_amount', 'tat_requirements_final_amount'], 'safe'],
            [['auto_save_advance_payment', 'auto_save_no_of_property_discount', 'auto_save_no_of_units_same_building', 'auto_save_first_time_discount', 'auto_save_tat_requirements'], 'safe'],
            [['related_to_buyer_reason', 'related_to_owner_reason', 'related_to_client_reason', 'related_to_property_reason'], 'string'],
            [['recommended_fee', 'final_fee_approved', 'quotation_turn_around_time', 'quotation_recommended_fee', 'toe_final_fee', 'toe_final_turned_around_time', 'turn_around_time'], 'number'],
            [['created_at', 'updated_at', 'deleted_at', 'client_reference', 'quotation_turn_around_time', 'quotation_recommended_fee', 'toe_final_fee', 'toe_final_turned_around_time', 'turn_around_time', 'toe_image', 'payment_image', 'quotation_status', 'calculations', 'status_approve', 'status_approve_toe', 'relative_discount_toe', 'other_intended_users', 'assumptions', 'payment_status', 'payment_image_2', 'first_half_payment', 'second_half_payment', 'same_building_discount', 'final_quoted_fee'], 'safe'],
            [['reference_number', 'client_name', 'client_customer_name', 'scope_of_service', 'advance_payment_terms', 'related_to_buyer', 'related_to_owner', 'related_to_client', 'related_to_property', 'name', 'phone_number', 'email', 'valuer_name', 'payment_slip', 'toe_document'], 'string', 'max' => 255],
            [['relative_discount', 'date', 'quotation_final_fee'], 'string', 'max' => 100],

            [
                [
                    'inquiry_received_date',
                    'quotation_sent_date',
                    'toe_sent_date',
                    'payment_received_date',
                    'toe_signed_and_received_date',
                    'toe_signed_and_received',  
                    'on_hold_date',
                    'approved_date',
                    'quotation_rejected_date',
                    'toe_rejected_date',
                    'cancelled_date',
                    'regretted_date',
                    'grand_final_toe_fee',
                    'tat_requirements',
                    'inspection_type','root_id','toe_recommended','toe_recommended_date','toe_verified_date',
                    'portfolio','total_time_days','total_time_hours','verify_time_days','verify_time_hours','cancelled_date_days','cancelled_date_hours',
                    'no_of_units_same_building', 'number_of_rooms_building', 'restaurant', 'ballrooms', 'atms', 'retails_units', 'night_clubs', 'bars', 'health_club', 'meeting_rooms', 'spa', 'beach_access', 'parking_sale',
                    'email_subject', 'email_status_rcv', 'email_status_docs', 'email_status_q_sent', 'email_status_q_approved', 'email_status_t_sent', 'email_status_t_approved',
                    'email_status_pr', 'email_status_q_hold', 'email_status_t_rejected', 'purpose_of_valuation', 'rec_relationship_discount', 'relationship_discount_final_amount', 'valuer_id',
                    'client_urgency','other_intended_users_check_1','instruction_person', 'client_is_owner', 'client_contact_person', 'aprvd_no_of_property_discount', 'aprvd_same_building_discount', 'aprvd_first_time_discount', 'aprvd_no_of_property_discount_amount', 'aprvd_same_building_discount_amount', 'aprvd_first_time_discount_amount','aprvd_tat_requirements_final_amount','reference_flag','quotation_recommended_date','document_requested_date','quotation_approved_by','toe_approved_by','bo_recommended_fee','reviewed_fee','bo_recommended_tat','reviewed_tat','quotaion_with_prop_fee','email_status_fee_diff','client_module_id','trustee_id','q_followup_email_sent','q_followup_email_sent_date',
                    'fee_crm','fee_crm_client_request'
                ],
                'safe'
            ],
            [[
                'special_assumptions', 'instruction_date', 'target_date','valuation_date', 'client_invoice_type', 'quotation_cancel_reason', 'instructing_party_position', 'instructing_party_company', 'instructing_party_phone', 'instructing_party_email',
                'toe_approved_date', 'quotation_accepted_date', 'reset_values','type_of_service','location','no_of_location'
            ], 'safe'],
            [['client_name', 'reference_number', 'client_customer_name', 'instruction_date', 'target_date','valuation_date', 'inquiry_received_time', 'inquiry_received_day', 'email_subject'], 'required', 'on' => 'step_0_1'],
            
            ['instruction_date', 'required', 'message' => 'Inquiry Received Date is required'],
            ['inquiry_received_time', 'required', 'message' => 'Inquiry Received Time is required'],
            ['client_name', 'required', 'message' => 'Client Name is required'],
            //['instruction_person', 'required', 'message' => 'Instruction Person Name is required', 'on' => 'step_0'],
            
            // Custom validation rule for 'target_date'
            ['target_date', 'required', 'when' => function ($model) {
                return $model->client_urgency == 1;
            }, 'whenClient' => "function (attribute, value) {
                return $('#client-urgency').val() == '1';
            }", 'message' => 'Client Valuation Target Date is required when Client Urgency is Yes', 'on' => 'step_0'],

            
            // Custom validation rule for 'other_intended_users'
            ['other_intended_users', 'required', 'when' => function ($model) {
                return $model->other_intended_users_check_1 === 'Yes';
            }, 'whenClient' => "function (attribute, value) {
                return $('#other_intended_users_check_1').val() === 'Yes';
            }",  'message' => 'Other Intended Users Value is required', 'on' => 'step_0'],

            // ['scope_of_service', 'required', 'message' => 'Scope Of Service is required', 'on' => 'step_0'],
            
            // [['no_of_units_same_building'], 'integer', 'min' => 1, 'message' => 'Units in same building must be no less than 1.', 'on' => 'step_0'],

            ['no_of_units_same_building', 'validateNoOfUnits', 'on' => 'step_0'],

        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['step_0_1'] = ['client_name', 'reference_number', 'client_customer_name', 'instruction_date', 'target_date','valuation_date', 'inquiry_received_time', 'email_subject', 'client_reference']; // Fields required for step 1
        
        $scenarios['step_0'] = ['other_intended_users_check_1', 'other_intended_users', 'scope_of_service', 'no_of_properties','inspection_type', 'no_of_units_same_building', 'purpose_of_valuation', 'advance_payment_terms', 'client_urgency', 'tat_requirements', 'target_date','valuation_date', 'valuer_id', 'instruction_person', 'reference_fee_3rdparty' , 'reference_fee_staff', 'reference_fee_3rdparty_check', 'reference_fee_3rdparty_bk' , 'reference_fee_staff_bk', 'reference_fee_staff_name','industry','location','no_of_location','fee_crm','fee_crm_client_request'];
        
        return $scenarios;
    }

    public function validateNoOfUnits($attribute, $params)
    {
        if ($this->$attribute > $this->no_of_properties) {
            $this->addError($attribute, 'Units in the same building cannot be greater than the number of properties.');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reference_number' => 'Reference Number',
            'client_name' => 'Client Name',
            'client_type' => 'Client Type',
            'client_customer_name' => 'Client Customer Name',
            'other_intended_users_check_1' => 'Other Intended Users',
            'other_intended_users' => 'Other Intended Users Value',
            'scope_of_service' => 'Scope Of Service',
            'inquiry_date' => 'Inquiry Date',
            'expiry_date' => 'Expiry Date',
            'purpose_of_valuation' => 'Purpose Of Valuation',
            'advance_payment_terms' => 'Advance Payment Terms',
            'no_of_properties' => 'No Of Properties',
            'related_to_buyer' => 'Related To Buyer',
            'related_to_owner' => 'Related To Owner',
            'related_to_client' => 'Related To Client',
            'related_to_property' => 'Related To Property',
            'related_to_buyer_reason' => 'Related To Buyer Reason',
            'related_to_owner_reason' => 'Related To Owner Reason',
            'related_to_client_reason' => 'Related To Client Reason',
            'related_to_property_reason' => 'Related To Property Reason',
            'name' => 'Name',
            'phone_number' => 'Phone Number',
            'email' => 'Email',
            'recommended_fee' => 'Recommended Fee',
            'relative_discount' => 'Relative Discount',
            'final_fee_approved' => 'Final Fee Approved',
            'turn_around_time' => 'Turn Around Time',
            'valuer_name' => 'Valuer Name',
            'date' => 'Date',
            'quotation_status' => 'Quotation Status',
            'payment_slip' => 'Payment Slip',
            'toe_document' => 'Toe Document',
            'quotation_recommended_fee' => 'Quotation Recommended Fee',
            'quotation_turn_around_time' => 'Quotation Turn Around Time',
            'toe_final_fee' => 'Toe Final Fee',
            'toe_final_turned_around_time' => 'Toe Final Turned Around Time',
            'status' => 'Status',
            'assumptions' => 'Assumptions',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'deleted_by' => 'Deleted By',
            'branch_id' => 'Branch',
            'assumptions' => 'General Assumptions',
            'payment_status' => 'Payment Status',
            'special_assumptions' => 'Special Assumptions',
            'instruction_person' => 'Instruction Person Name',
            'q_followup_email_sent' => 'Followup Email Sent',
            'q_followup_email_sent_date' => 'Followup Email Sent Date',
        ];
    }
    public function beforeSave($insert)
    {

        if ($this->status_approve <> null) {
        } else {
            unset($this->status_approve);
        }
        if ($this->status_approve_toe <> null) {
        } else {
            unset($this->status_approve_toe);
        }

        if (!empty($this->assumptions) and is_array($this->assumptions)) {
            $this->assumptions =  implode(",", $this->assumptions);
        }
        if (!empty($this->special_assumptions) and is_array($this->special_assumptions)) {
            $this->special_assumptions =  implode(",", $this->special_assumptions);
        }


        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {

        /* if($this->no_of_properties!=null){
            $properties_length = $this->no_of_properties;
            //  CustomFieldModule::deleteAll(['and',['custom_field_id'=>$this->id],['not in','module_type',$this->for_modules]]);

            for($i=0; $i<$properties_length; $i++){
                $property= new CrmProperties();
                $property->quotation_id = $this->id;
                $property->property_index = $i;
                $property->save();
            }
        }*/
        if ($this->calculations != null && !empty($this->calculations)) {
            // echo "<pre>"; print_r($this->calculations); echo "</pre>"; die();

            // dd($this->calculations);
            foreach ($this->calculations as $key => $calculation) {
                if (($this->quotation_status == 0) || ($this->quotation_status ==  14) || ($this->quotation_status == 9) || ($this->quotation_status == 17)) {

                    Yii::$app->db->createCommand()->update('crm_received_properties', ['quotation_fee' => $calculation['quotation_fee'], 'tat' => $calculation['tat'], 'aprvd_tat' => $calculation['aprvd_tat'], 'bo_recommended_tat' => $calculation['bo_recommended_tat'], 'bo_recommended_fee' => $calculation['bo_recommended_fee'], 'reviewed_tat' => $calculation['reviewed_tat'], 'reviewed_fee' => $calculation['reviewed_fee'], 'toe_fee' => $calculation['quotation_fee'], 'toe_tat' => $calculation['tat']], 'id=' . $key . '')->execute();

                }
                if ($this->quotation_status > 1 && $this->quotation_status <= 2  || ($this->quotation_status == 9) || ($this->quotation_status == 17)) {
                    if (isset($calculation['toe_fee']) && $calculation['toe_fee'] <> null) {
                        Yii::$app->db->createCommand()->update('crm_received_properties', ['toe_fee' => $calculation['toe_fee'], 'toe_tat' => $calculation['toe_tat']], 'id=' . $key . '')->execute();
                    }
                }
            }

            
        }
        if($this->approved_date <> null){
            $startDate = $this->instruction_date . ' ' . $this->inquiry_received_time;
            $endDate = $this->payment_received_date;
            // $endDate = date("Y-m-d  H:i:s");

            $startDateTime = new DateTime($startDate); // Start date and time
            $endDateTime = new DateTime($endDate);

            $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
            $workingDays = abs(number_format(($workingHours / 8.5), 1));

            \Yii::$app->db->createCommand("UPDATE crm_quotations SET verify_time_days='" . $workingDays . "' WHERE id=" . $this->id)->execute();
            \Yii::$app->db->createCommand("UPDATE crm_quotations SET verify_time_hours='" . $workingHours . "' WHERE id=" .$this->id)->execute();

        }
        if($this->payment_received_date <> null){
            $startDate = $this->instruction_date . ' ' . $this->inquiry_received_time;
            $endDate = $this->payment_received_date;
            // $endDate = date("Y-m-d  H:i:s");

            $startDateTime = new DateTime($startDate); // Start date and time
            $endDateTime = new DateTime($endDate);

            $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
            $workingDays = abs(number_format(($workingHours / 8.5), 1));
            \Yii::$app->db->createCommand("UPDATE crm_quotations SET total_time_days='" . $workingDays . "' WHERE id=" . $this->id)->execute();
            \Yii::$app->db->createCommand("UPDATE crm_quotations SET total_time_hours='" . $workingHours . "' WHERE id=" . $this->id)->execute();

        }

        if($this->quotation_status == 7 || $this->quotation_status == 11){
            $startDate = $this->instruction_date . ' ' . $this->inquiry_received_time;
            $endDate = $this->cancelled_date;
            // $endDate = date("Y-m-d  H:i:s");

            $startDateTime = new DateTime($startDate); // Start date and time
            $endDateTime = new DateTime($endDate);

            $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
            $workingDays = abs(number_format(($workingHours / 8.5), 1));
            \Yii::$app->db->createCommand("UPDATE crm_quotations SET cancelled_date_days='" . $workingDays . "' WHERE id=" . $this->id)->execute();
            \Yii::$app->db->createCommand("UPDATE crm_quotations SET cancelled_date_hours='" . $workingHours . "' WHERE id=" . $this->id)->execute();

        }
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub

    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->id;
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Quotations';
    }

    public function getClient()
    {
        return $this->hasOne(Company::className(), ['id' => 'client_name']);
    }

    public function getChildData()
    {
        return $this->hasMany(ValuationQuotationPropertiesDetails::className(), ['valuation_quotation_id' => 'id']);
    }

    public function getBuilding()
    {
        return $this->hasOne(Buildings::className(), ['id' => 'building']);
    }

    public function getProperty()
    {
        return $this->hasOne(Properties::className(), ['id' => 'property_id']);
    }

    public function getZeroIndexProperty()
    {
        return $this->hasOne(CrmReceivedProperties::className(), ['quotation_id' => 'id'])->where(['property_index' => 0]);
    }


    public function getUserData()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getApprovedUserData()
    {
        return $this->hasOne(User::className(), ['id' => 'quotation_approved_by']);
    }


    public function QuotationPdf($condition)
    {
        require_once(__DIR__ . '/../components/tcpdf/QuotationPdf.php');
        // create new PDF document
        $pdf = new \QuotationPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->model = $this;

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle($this->reference_number);
        $pdf->SetSubject('Quotation');
        $pdf->SetKeywords($this->reference_number);

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(10, 35, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('times', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P', 'A4');


        if($this->type_of_service == 3){
            $qpdf = Yii::$app->controller->renderPartial('/crm-quotations/quotation-bcs', ['id' => $this->id, 'model' => $this]);
        }else  if($this->type_of_service == 2){
            $qpdf = Yii::$app->controller->renderPartial('/crm-quotations/quotation-pme', ['id' => $this->id, 'model' => $this]);
        }else {
            $qpdf = Yii::$app->controller->renderPartial('/crm-quotations/quotation-new', ['id' => $this->id, 'model' => $this]);
        }
        // $qpdf = Yii::$app->controller->renderPartial('/crm-quotations/quotation-new', ['id' => $this->id, 'model' => $this]);


        if ($condition == true) {
            $pdf->writeHTML($qpdf, true, false, false, false, '');
            $pdf->Output('Quotation: ' . $this->reference_number, 'I');
            exit();
        }

        if ($condition == false) {
            $invoiceFullPath = $this->GetInvoicebank($this->id, $condition = false);
            $pdfFile = $this->reference_number . '.pdf';
            $fullPath = Yii::$app->params['quote_uploads_abs_path'] . '/' . $pdfFile;
            $pdf->writeHTML($qpdf, true, false, false, false, '');
            $pdf->Output($fullPath, 'F');

            $attachments = [$fullPath];
            if($this->client_name == 9166){
                $attachments = [$fullPath, $invoiceFullPath];
            }

            // $template = NotificationTemplate::findOne(1);
            $notifyData = [
                'client' => $this->client,
                'attachments' => $attachments,
                'subject' => $this->email_subject,
                'uid' => 'crm' . $this->id,
                'instructing_person_email' => $this->instructing_party_email,
                'replacements' => [
                    '{clientName}' => $this->client->title,
                    // '{reference_number}'=>$this->reference_number
                ],
            ];
            NotifyEvent::fire23('quotation.send', $notifyData);
        }
    }

    public function ToePdf($condition)
    {
        // echo "toe model function";die();
        require_once(__DIR__ . '/../components/tcpdf/ProposalPdf.php');
        // create new PDF document
        $pdf = new \ProposalPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->model = $this;

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle($this->reference_number);
        $pdf->SetSubject('Quotation');
        $pdf->SetKeywords($this->reference_number);

        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        $pdf->SetMargins(10, 35, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        $pdf->setFontSubsetting(true);

        $pdf->SetFont('times', '', 14, '', true);

        $pdf->AddPage('P', 'A4');

        
        if($this->type_of_service == 3){ 
            $qpdf = Yii::$app->controller->renderPartial('/crm-quotations/toe-bcs', ['id' => $this->id, 'model' => $this]);
        }else  if($this->type_of_service == 2){ 
            $qpdf = Yii::$app->controller->renderPartial('/crm-quotations/toe-pme', ['id' => $this->id, 'model' => $this]);
        }else {
            $qpdf = Yii::$app->controller->renderPartial('/crm-quotations/toe-new', ['id' => $this->id, 'model' => $this]);
        }
        // $qpdf = Yii::$app->controller->renderPartial('/crm-quotations/toe-new', ['id' => $this->id, 'model' => $this]);

        if ($condition == true) {
            $pdf->writeHTML($qpdf, true, false, false, false, '');
            $pdf->Output('TOE ' . $this->reference_number, 'I');
            exit();
        }

        if ($condition == false) {



            $invoiceFullPath = $this->GetInvoice($this->id, $condition = false);

            $pdfFile = $this->reference_number . '.pdf';
            $fullPath = Yii::$app->params['toe_uploads_abs_path'] . '/' . $pdfFile;
            $pdf->writeHTML($qpdf, true, false, false, false, '');
            $pdf->Output($fullPath, 'F');
            // $attachments=[$fullPath];
            $attachments = [$fullPath, $invoiceFullPath];

            $notifyData = [
                'client' => $this->client,
                'attachments' => $attachments,
                'subject' => $this->email_subject,
                'uid' => 'crm' . $this->id,
                'instructing_person_email' => $this->instructing_party_email,
                'replacements' => [
                    '{clientName}' => $this->client->title,
                    // '{reference_number}'=>$this->reference_number
                ],
            ];

            NotifyEvent::fire23('toe.send', $notifyData);
        }
    }


    public function GetInvoice($id = '', $condition)
    {
        $model = CrmQuotations::findOne($id);

        require_once(__DIR__ . '/../components/tcpdf/ProformaInvoice.php');
        // create new PDF document
        $pdf = new \ProformaInvoice(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle($this->reference_number);
        $pdf->SetSubject('Invoice');
        $pdf->SetKeywords($this->reference_number);

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(10, 35, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('times', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P', 'A4');

        $qpdf = Yii::$app->controller->renderPartial('/crm-quotations/qpdf', [
            'id' => $id,
            'model' => $model,
        ]);


        if ($condition == false) {
            $pdfFile = "Invoice-" . $this->reference_number . '.pdf';
            $fullPath = Yii::$app->params['invoice_abs_path'] . '/' . $pdfFile;
            $pdf->writeHTML($qpdf, true, false, false, false, '');
            $pdf->Output($fullPath, 'F');
            return $fullPath;
        } else {
            $pdf->writeHTML($qpdf, true, false, false, false, '');
            $pdf->Output('Invoice-' . $this->reference_number, 'I');
        }
    }
    public function GetInvoicebank($id = '', $condition)
    {
        $model = CrmQuotations::findOne($id);

        require_once(__DIR__ . '/../components/tcpdf/ProformaInvoice.php');
        // create new PDF document
        $pdf = new \ProformaInvoice(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle($this->reference_number);
        $pdf->SetSubject('Invoice');
        $pdf->SetKeywords($this->reference_number);

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(10, 35, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('times', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P', 'A4');

        $qpdf = Yii::$app->controller->renderPartial('/crm-quotations/qpdf_bank', [
            'id' => $id,
            'model' => $model,
        ]);


        if ($condition == false) {
            $pdfFile = "Invoice-" . $this->reference_number . '.pdf';
            $fullPath = Yii::$app->params['invoice_abs_path'] . '/' . $pdfFile;
            $pdf->writeHTML($qpdf, true, false, false, false, '');
            $pdf->Output($fullPath, 'F');
            return $fullPath;
        } else {
            $pdf->writeHTML($qpdf, true, false, false, false, '');
            $pdf->Output('Invoice-' . $this->reference_number, 'I');
        }
    }


    public function upload()
    {

        $uploadObject = Yii::$app->get('s3bucket')->upload('toe/images/' . $this->toe_image->baseName . '.' . $this->toe_image->extension, $this->toe_image->tempName);
        if ($uploadObject) {
            // check if CDN host is available then upload and get cdn URL.
            if (Yii::$app->get('s3bucket')->cdnHostname) {
                $url = Yii::$app->get('s3bucket')->getCdnUrl($this->toe_image);
            } else {
                $url = Yii::$app->get('s3bucket')->getUrl('toe/images/' . $this->toe_image);
            }
        }
        return true;
    }

    public function uploadpayment()
    {
        $uploadObject = Yii::$app->get('s3bucket')->upload('payments/images/' . $this->payment_image->baseName . '.' . $this->payment_image->extension, $this->payment_image->tempName);
        if ($uploadObject) {
            // check if CDN host is available then upload and get cdn URL.
            if (Yii::$app->get('s3bucket')->cdnHostname) {
                $url = Yii::$app->get('s3bucket')->getCdnUrl($this->payment_image);
            } else {
                $url = Yii::$app->get('s3bucket')->getUrl('payments/images/' . $this->payment_image);
            }
        }
        return true;
    }

    public function uploadpayment2()
    {
        $uploadObject = Yii::$app->get('s3bucket')->upload('payments/images/' . $this->payment_image_2->baseName . '.' . $this->payment_image_2->extension, $this->payment_image_2->tempName);
        if ($uploadObject) {
            // check if CDN host is available then upload and get cdn URL.
            if (Yii::$app->get('s3bucket')->cdnHostname) {
                $url = Yii::$app->get('s3bucket')->getCdnUrl($this->payment_image_2);
            } else {
                $url = Yii::$app->get('s3bucket')->getUrl('payments/images/' . $this->payment_image_2);
            }
        }
        return true;
    }


    public function sendProposalFeedbackFormMail($feedback_key, $client){ 
        
        
        $feedback_url = 'https://maxima.windmillsgroup.com/feedback/submit/?k='.$feedback_key;
        $feedback_form = '';
        $feedback_form .= '<p><br/><span style="padding-top:10px; padding-bottom:10px"><a href="'.$feedback_url.'" target="_black" style="padding-left:10px; padding-right:10px; padding-top:15px; padding-bottom:15px; text-align: center; background:#007BFF; color:#ffffff; text-decoration:none; font-wight:bold; font-size:14px" >';
        $feedback_form .= 'Submit Feedback';
        $feedback_form .= '</a></span><br/><br/></p>';
        $feedback_form .= '';

        

        $notifyData = [
            'client' => $this->client,
            'attachments' => [],
            'subject' => 'Tell us how happy you are',
            'uid' => Yii::$app->user->identity->id,
            'replacements' => [
                // '{feedback_key}' => $feedback_key,
                '{feedback_form}' => $feedback_form,
            ],
        ];

        
        NotifyEvent::fireFeedback('Proposal.Feedback.Form', $notifyData);

    }

    public function sendProposalFeedbackFormSms($feedback_key, $quotation){

        $feedback_url = 'https://maxima.windmillsgroup.com/feedback/submit/?k='.$feedback_key;
        

        $results = \app\models\User::find()
                    ->where(['company_id' => $quotation->client_name])
                    ->innerJoin(\app\models\UserProfileInfo::tableName(), \app\models\UserProfileInfo::tableName() . ".user_id=" . \app\models\User::tableName() . ".id")
                    ->andWhere([\app\models\UserProfileInfo::tableName() . '.primary_contact' => 1])
                    ->one();
        $profileData = \app\models\UserProfileInfo::find()->where(['user_id'=>$results->id])->one();

        if ($profileData->mobile != '') {
            // $str = ltrim($quotation->phone_code, '0');
            // $contact_number = $str . $quotation->instructing_party_phone;
            // $contact_number = '971' . $contact_number;
            $contact_number = $profileData->mobile;
            /* echo $contact_number;
             die;*/
            if ($quotation->client->client_type != 'bank') {

                $numbers = array();
                // $numbers[] = '971521547162';
                // $numbers[] = $contact_number;
                
                foreach ($numbers as $number) {
                    $text = "Dear Sir / Madam,
                    It has been our pleasure to present proposal for you. We would greatly appreciate it if you could spare a few moments to provide your feedback through the following link:
                    
                    ".$feedback_url."
                    
                    Thank you.
                                ";

                    

                    $ch = curl_init();

                    
                    curl_setopt($ch, CURLOPT_URL, "https://restapi.smscountry.com/v0.1/Accounts/GSzAf5xi3QzofjOMtlkl/SMSes/");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, FALSE);
                    curl_setopt($ch, CURLOPT_HEADER, FALSE);
                    
                    curl_setopt($ch, CURLOPT_POST, TRUE);
                    
                    curl_setopt($ch, CURLOPT_POSTFIELDS, "{
                        \"Text\":\"$text\",
                        \"Number\":\"$number\",
                        \"SenderId\": \"WINDMILLS\",
                        
                        \"DRNotifyHttpMethod\": \"POST\",
                        \"Tool\": \"API\"
                    }");
                    
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        "Content-Type: application/json",
                        'Authorization: Basic ' . base64_encode("GSzAf5xi3QzofjOMtlkl:m82nK16BVJ67vFfulSxosIfhlgwEJgx2XYaBUPHm")
                        
                        )
                    );

                    $response = curl_exec($ch);
                    
                    // Yii::$app->db->createCommand()->update('feedback', ['sms_status' => 1], 'id=' . $feedback->id . '')->execute();
                    // Yii::$app->db->createCommand()->update('feedback', ['sms_status' => 1], 'id=' . $quotation->id . '')->execute();
                    /*echo "<pre>";
                    print_r($response);
                    die;*/
                    curl_close($ch);

                    //var_dump($response);
                }

            }
        }
    }

}
