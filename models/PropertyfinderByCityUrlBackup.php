<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "propertyfinder_by_city_url_backup".
 *
 * @property int $id
 * @property string|null $url
 * @property int|null $city_id
 * @property string|null $purpose
 */
class PropertyfinderByCityUrlBackup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'propertyfinder_by_city_url_backup';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id'], 'integer'],
            [['url', 'purpose'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'city_id' => 'City ID',
            'purpose' => 'Purpose',
        ];
    }
}
