<?php

namespace app\models;
use yii\db\ActiveRecord;

use Yii;

class BuildingForSave extends ActiveRecord
{

	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%building_for_save}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['building_name','city','status'],'safe'],
		];
	}


}
