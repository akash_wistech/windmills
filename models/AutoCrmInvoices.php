<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auto_crm_invoices".
 *
 * @property int $id
 * @property int|null $qoutation_id
 * @property string|null $client_type
 * @property string|null $prefix
 * @property int|null $current_year
 * @property string|null $current_year_number
 * @property string|null $general_number
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 */
class AutoCrmInvoices extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auto_crm_invoices';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['qoutation_id', 'current_year', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at','current_year_number','general_number'], 'safe'],
            [['client_type', 'prefix'], 'string', 'max' => 100],
           // [[, 'general_number'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'qoutation_id' => 'Qoutation ID',
            'client_type' => 'Client Type',
            'prefix' => 'Prefix',
            'current_year' => 'Current Year',
            'current_year_number' => 'Current Year Number',
            'general_number' => 'General Number',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
