<?php 
// models/Feedback.php

namespace app\models;

// use app\components\models\ActiveRecordFull;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "feedback".
 *
 * @property int $id
 * @property int $clietn_id
 * @property int $valuation_id
 * @property int $quotation_id
 * @property string $feedback_key
 * @property int $quality_of_valuation
 * @property int $quality_of_inspection
 * @property int $quality_of_customer_service
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $status
 */

class Feedback extends ActiveRecord
{
    public static function tableName()
    {
        return 'feedback';
    }

    public function rules()
    {
        return [
            [['quality_of_valuation', 'quality_of_inspection','quality_of_valuation_service','quality_of_proposal_service', 'quality_of_customer_service','quality_of_tat','fee_rating','tat_rating','technical_capability_rating','quality_average','status','property_category','type'], 'integer'],
            [['feedback_key','appreciation_suggestion','created_at', 'updated_at','valuation_id','quotation_id','valuer','inspector','approver','proposer'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'valuation_id' => 'Valuation ID',
            'quotation_id' => 'Quotation ID',
            'feedback_key' => 'Feedback Key',
            'quality_of_valuation' => 'Quality Of Valuation',
            'quality_of_inspection' => 'Quality Of Inspection',
            'quality_of_valuation_service' => 'Quality Of Valuation Service',
            'quality_of_proposal_service' => 'Quality Of Proposal Service',
            'quality_of_tat' => 'Quality Of TAT',
            'fee_rating' => 'Fee Rating',
            'tat_rating' => 'Turn Around Time Rating',
            'technical_capability_rating' => 'Technical Capability Rating',
            'quality_of_customer_service' => 'Customer Service Rating',
            'appreciation_suggestion' => 'Appreciation And/Or Suggestion',
            'type' => 'Feedback Type',
            'valuer' => 'Valuer',
            'inspector' => 'Inspector',
            'approver' => 'Approver',
            'proposer' => 'Proposer',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }


    public function getClient()
    {
        return $this->hasOne(Company::className(), ['id' => 'client_id']);
    }

    public function getValuation()
    {
        return $this->hasOne(Valuation::className(), ['id' => 'valuation_id']);
    }

    public function getBuilding()
    {
        return $this->hasOne(Buildings::className(), ['id' => 'building_info']);
    }

    public function getPropertyCategory()
    {
        return $this->hasOne(PropertyCategories::className(), ['id' => 'property_category']);
    }

    public function getCityName()
    {
        return $this->hasOne(Zone::className(), ['id' => 'city']);
    }

    public function getQuotation()
    {
        return $this->hasOne(CrmQuotations::className(), ['id' => 'quotation_id']);
    }

    public function getQualityAverage()
    {
        if($this->type == 0){
            $qAvg =  ($this->quality_of_valuation + $this->quality_of_inspection + $this->quality_of_tat + $this->quality_of_valuation_service) / 4;
            return number_format($qAvg, 0);
        }
        elseif($this->type == 1){
            $qAvg = ($this->fee_rating + $this->tat_rating + $this->technical_capability_rating + $this->quality_of_customer_service + $this->quality_of_proposal_service) / 5;
            return number_format($qAvg, 0);
        }
    }

    // public function getValuationQualityAverage()
    // {
    //     return ($this->quality_of_valuation + $this->quality_of_inspection + $this->quality_of_tat + $this->quality_of_customer_service) / 4;
    // }
    // public function getProposalQualityAverage()
    // {
    //     return ($this->fee_satisfation + $this->timeline_satisfation + $this->quality_of_customer_service) / 3;
    // }
}
