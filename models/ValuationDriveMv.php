<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "valuation_drive_mv".
 *
 * @property int $id
 * @property float|null $average_location
 * @property float $average_age
 * @property float $average_tenure
 * @property float $average_view
 * @property float $average_	finished_status
 * @property float $average_property_condition
 * @property float $average_upgrades
 * @property float $average_furnished
 * @property float $average_property_exposure
 * @property float $average_property_placement
 * @property float $average_full_building_floors
 * @property float $average_number_of_levels
 * @property float $average_no_of_bedrooms
 * @property float $average_parking_space
 * @property float $average_pool
 * @property float $average_landscaping
 * @property float $average_white_goods
 * @property float $average_utilities_connected
 * @property float $average_developer_margin
 * @property float $average_land_size
 * @property float $average_balcony_size
 * @property float $average_built_up_area
 * @property float $average_date
 * @property float $average_sale_price
 * @property float $average_psf
 * @property float $weightage_location
 * @property float $weightage_age
 * @property float $weightage_tenure
 * @property float $weightage_finished_status
 * @property float $weightage_property_condition
 * @property float $weightage_upgrades
 * @property float $weightage_furnished
 * @property float $weightage_property_exposure
 * @property float $weightage_property_placement
 * @property float $weightage_full_building_floors
 * @property float $weightage_number_of_levels
 * @property float $weightage_no_of_bedrooms
 * @property float $weightage_parking_space
 * @property float $weightage_pool
 * @property float $weightage_landscaping
 * @property float $weightage_white_goods
 * @property float $weightage_utilities_connected
 * @property float $weightage_developer_margin
 * @property float $weightage_land_size
 * @property float $weightage_balcony_size
 * @property float $weightage_built_up_area
 * @property float $weightage_date
 * @property int $valuation_id
 * @property string|null $type
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class ValuationDriveMv extends ActiveRecordFull
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'valuation_drive_mv';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['average_location', 'average_age', 'average_tenure', 'average_view', 'average_finished_status', 'average_property_condition', 'average_upgrades', 'average_furnished', 'average_property_exposure', 'average_property_placement', 'average_full_building_floors', 'average_number_of_levels', 'average_no_of_bedrooms', 'average_parking_space', 'average_pool', 'average_landscaping', 'average_white_goods', 'average_utilities_connected', 'average_developer_margin', 'average_land_size', 'average_balcony_size', 'average_built_up_area', 'average_sale_price', 'average_psf', 'weightage_location', 'weightage_age', 'weightage_tenure', 'weightage_finished_status', 'weightage_property_condition', 'weightage_upgrades', 'weightage_furnished', 'weightage_property_exposure', 'weightage_property_placement', 'weightage_full_building_floors', 'weightage_number_of_levels', 'weightage_no_of_bedrooms', 'weightage_parking_space', 'weightage_pool', 'weightage_landscaping', 'weightage_white_goods', 'weightage_utilities_connected', 'weightage_developer_margin', 'weightage_land_size', 'weightage_balcony_size', 'weightage_built_up_area', 'weightage_date','weightage_view','sales_discount'], 'number'],
            [['valuation_id'], 'required'],
            [['valuation_id', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['type'], 'string'],
            [['created_at', 'updated_at', 'trashed_at','sales_discount','search_type','income_type'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'average_location' => 'Average Location',
            'average_age' => 'Average Age',
            'average_tenure' => 'Average Tenure',
            'average_view' => 'Average View',
            'average_finished_status' => 'Average Finished Status',
            'average_property_condition' => 'Average Property Condition',
            'average_upgrades' => 'Average Upgrades',
            'average_furnished' => 'Average Furnished',
            'average_property_exposure' => 'Average Property Exposure',
            'average_property_placement' => 'Average Property Placement',
            'average_full_building_floors' => 'Average Full Building Floors',
            'average_number_of_levels' => 'Average Number Of Levels',
            'average_no_of_bedrooms' => 'Average No Of Bedrooms',
            'average_parking_space' => 'Average Parking Space',
            'average_pool' => 'Average Pool',
            'average_landscaping' => 'Average Landscaping',
            'average_white_goods' => 'Average White Goods',
            'average_utilities_connected' => 'Average Utilities Connected',
            'average_developer_margin' => 'Average Developer Margin',
            'average_land_size' => 'Average Land Size',
            'average_balcony_size' => 'Average Balcony Size',
            'average_built_up_area' => 'Average Built Up Area',
            'average_date' => 'Average Date',
            'average_sale_price' => 'Average Sale Price',
            'average_psf' => 'Average Psf',
            'weightage_location' => 'Weightage Location',
            'weightage_age' => 'Weightage Age',
            'weightage_tenure' => 'Weightage Tenure',
            'weightage_finished_status' => 'Weightage Finished Status',
            'weightage_property_condition' => 'Weightage Property Condition',
            'weightage_upgrades' => 'Weightage Upgrades',
            'weightage_furnished' => 'Weightage Furnished',
            'weightage_property_exposure' => 'Weightage Property Exposure',
            'weightage_property_placement' => 'Weightage Property Placement',
            'weightage_full_building_floors' => 'Weightage Full Building Floors',
            'weightage_number_of_levels' => 'Weightage Number Of Levels',
            'weightage_no_of_bedrooms' => 'Weightage No Of Bedrooms',
            'weightage_parking_space' => 'Weightage Parking Space',
            'weightage_pool' => 'Weightage Pool',
            'weightage_landscaping' => 'Weightage Landscaping',
            'weightage_white_goods' => 'Weightage White Goods',
            'weightage_utilities_connected' => 'Weightage Utilities Connected',
            'weightage_developer_margin' => 'Weightage Developer Margin',
            'weightage_land_size' => 'Weightage Land Size',
            'weightage_balcony_size' => 'Weightage Balcony Size',
            'weightage_built_up_area' => 'Weightage Built Up Area',
            'weightage_date' => 'Weightage Date',
            'valuation_id' => 'Valuation ID',
            'type' => 'Type',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Developer Margin MV';
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->id;
    }
}
