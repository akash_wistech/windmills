<?php

namespace app\models;

use Yii;

class BayutToRentLinks extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'bayut_to_rent_links';
    }

    public function rules()
    {
        return [
            [['url', 'property_category'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'property_category' => 'Property Category',
        ];
    }
}
