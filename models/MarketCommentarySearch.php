<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MarketCommentary;

/**
 * MarketCommentarySearch represents the model behind the search form of `app\models\MarketCommentary`.
 */
class MarketCommentarySearch extends MarketCommentary
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['abu_dhabi_commentary', 'ajman_commentary', 'al_ain_commentary', 'dubai_commentary', 'fujairah_commentary', 'ras_al_khaimah_commentary', 'sharjah_commentary', 'umm_al_quwain_commentary', 'created_at', 'updated_at', 'trashed_at','status_verified','property_type'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MarketCommentary::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => $this->trashed,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
            'status_verified' => $this->status_verified,
            'property_type' => $this->property_type,
        ]);

        $query->andFilterWhere(['like', 'abu_dhabi_commentary', $this->abu_dhabi_commentary])
            ->andFilterWhere(['like', 'ajman_commentary', $this->ajman_commentary])
            ->andFilterWhere(['like', 'al_ain_commentary', $this->al_ain_commentary])
            ->andFilterWhere(['like', 'dubai_commentary', $this->dubai_commentary])
            ->andFilterWhere(['like', 'fujairah_commentary', $this->fujairah_commentary])
            ->andFilterWhere(['like', 'ras_al_khaimah_commentary', $this->ras_al_khaimah_commentary])
            ->andFilterWhere(['like', 'sharjah_commentary', $this->sharjah_commentary])
            ->andFilterWhere(['like', 'umm_al_quwain_commentary', $this->umm_al_quwain_commentary])
            ->andFilterWhere(['like', 'status_verified', $this->status_verified]);

        return $dataProvider;
    }
}
