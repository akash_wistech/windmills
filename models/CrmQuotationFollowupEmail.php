<?php

namespace app\models;

use yii\db\ActiveRecord;
use app\components\models\ActiveRecordFull;


use Yii;

/**
 * This is the model class for table "crm_quotation_followup_email".
 *
 * @property int $id
 * @property int|null $quotation_id
 * @property string|null $date
 */
class CrmQuotationFollowupEmail extends ActiveRecordFull
{
    public $no_of_reminder;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_quotation_followup_email';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quotation_id','client_id'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'quotation_id' => 'Quotation ID',
            'date' => 'Date',
        ];
    }

    public function getQuotation()
    {
        return $this->hasOne(CrmQuotations::className(), ['id' => 'quotation_id']);
    }

    public function getClient()
    {
        return $this->hasOne(Company::className(), ['id' => 'client_id']);
    }
}
