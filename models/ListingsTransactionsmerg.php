<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "listings_transactions".
 *
 * @property int $id
 * @property string $listings_reference
 * @property int $source
 * @property string $listing_website_link
 * @property string $listing_date
 * @property int $property_listed
 * @property int $property_category
 * @property int $tenure
 * @property string $unit_number
 * @property int $floor_number
 * @property int $number_of_levels
 * @property int $location
 * @property string|null $land_size
 * @property float $built_up_area
 * @property string|null $balcony_size
 * @property int $property_placement
 * @property int $property_visibility
 * @property int $property_exposure
 * @property int $listing_property_type
 * @property int $property_condition
 * @property string|null $development_type
 * @property string|null $finished_status
 * @property string|null $pool
 * @property string|null $gym
 * @property string|null $play_area
 * @property int $other_facilities
 * @property string|null $completion_status
 * @property int $no_of_bedrooms
 * @property int $upgrades
 * @property int $full_building_floors
 * @property int $parking_space
 * @property int $view
 * @property string|null $landscaping
 * @property string|null $white_goods
 * @property string|null $furnished
 * @property string|null $utilities_connected
 * @property string|null $developer_margin
 * @property float|null $listings_price
 * @property float|null $listings_rent
 * @property float|null $price_per_sqt
 * @property float|null $final_price
 * @property string|null $agent_name
 * @property int|null $agent_phone_no
 * @property string|null $agent_company
 * @property int $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class ListingsTransactionsmerg extends ActiveRecordFull
{
    public $status_verified;
    public $selected_data = [];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'listings_transactions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status_verified','status_verified_at','status_verified_by'], 'safe'],
            [['listings_reference'], 'required'],
            [['source', 'property_category', 'no_of_bedrooms', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by', 'building_info'], 'integer'],
            [['listing_date', 'created_at', 'updated_at', 'trashed_at', 'other_facilities','agent_phone_no', 'agent_company','community','sub_community','property_id','estimated_age','gross_yield','location_highway_drive','location_school_drive','location_mall_drive','location_sea_drive','location_park_drive','view_community','view_pool','view_burj','view_sea','view_marina','view_mountains','view_lake','view_golf_course','view_park','view_special', 'listing_property_type',
                'changed','listing_website_link_image','listing_website_url_image','approved_by','approved_at'], 'safe'],
            ['listing_date','validateDates'],
            [['built_up_area', 'listings_price', 'listings_rent', 'price_per_sqt', 'final_price','land_size','estimated_age','completion_status','gross_yield'], 'string'],
            [['development_type', 'finished_status', 'pool', 'gym', 'play_area', 'landscaping', 'white_goods', 'furnished', 'utilities_connected', 'developer_margin'], 'string'],
            //[['listings_reference'], 'unique'],
            [['listings_reference', 'listing_website_link', 'unit_number', 'balcony_size', 'agent_name'], 'string', 'max' => 255],

            [['floor_number','number_of_levels','property_visibility','property_exposure','property_condition','full_building_floors','property_placement', 'tenure', 'source', 'listing_website_link', 'property_category', 'unit_number', 'built_up_area', 'no_of_bedrooms', 'building_info', 'listings_price', 'listings_rent', 'final_price','change_from','change_to','change_reason','upgrades','parking_space','old_merge_id_building','old_merge_id','converted_m'], 'safe'],
        ];
    }
    public function validateDates(){
        if(strtotime($this->listing_date) > strtotime(date('Y-m-d'))){
            $this->addError('listing_date','Date should not be greater than today!');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'listings_reference' => 'Listings Reference',
            'source' => 'Source',
            'listing_website_link' => 'Listing Website Link',
            'listing_date' => 'Listing Date',
           /* 'property_listed' => 'Property Listed',*/
            'property_category' => 'Property Category',
            'building_info' => 'Building Info',
            'community' => 'Community',
            'sub_community' => 'Sub Community',
            'tenure' => 'Tenure',
            'unit_number' => 'Unit Number',
            'floor_number' => 'Floor Number',
            'number_of_levels' => 'Number Of Levels',
            'land_size' => 'Land Size',
            'built_up_area' => 'Built Up Area',
            'balcony_size' => 'Balcony Size',
            'property_placement' => 'Property Placement',
            'property_visibility' => 'Property Visibility',
            'property_exposure' => 'Property Exposure',
            'listing_property_type' => 'Listing Property Type',
            'property_condition' => 'Property Condition',
            'development_type' => 'Development Type',
            'finished_status' => 'Finished Status',
            'pool' => 'Pool',
            'gym' => 'Gym',
            'play_area' => 'Play Area',
            'other_facilities' => 'Other Facilities',
            'completion_status' => 'Completion Status',
            'no_of_bedrooms' => 'No Of Bedrooms',
            'upgrades' => 'Upgrades',
            'full_building_floors' => 'Full Building Floors',
            'parking_space' => 'Parking Space',
            'location_highway_drive' => 'Drive to Highway/Main Road and metro',
            'location_school_drive' => 'Drive to school',
            'location_mall_drive' => 'Drive to commercial area/Mall',
            'location_sea_drive' => 'Drive to special landmark, sea, marina',
            'location_park_drive' => 'Drive to pool and park',
            'view_community' => 'Community',
            'view_pool' => 'Pool / Fountain',
            'view_park' => 'Small Park',
            'view_special' => 'Special view',
            'landscaping' => 'Landscaping',
            'white_goods' => 'White Goods',
            'furnished' => 'Furnished',
            'utilities_connected' => 'Utilities Connected',
            'developer_margin' => 'Developer Margin',
            'listings_price' => 'Listings Price',
            'listings_rent' => 'Listings Rent',
            'price_per_sqt' => 'Price Per Sqt',
            'final_price' => 'Final Price',
            'agent_name' => 'Agent Name',
            'agent_phone_no' => 'Agent Phone No',
            'agent_company' => 'Agent Company',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
            'old_merge_id_building' => 'old_merge_id_building',
            'old_merge_id' => 'old_merge_id',
        ];
    }
    public function beforeValidate()
    {

        if($this->changed == 0){
            $this->change_from = 'No change';
            $this->change_to = 'No change';
            $this->change_reason = 'No change';

        }
        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Listings Transactions';
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->id;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuilding()
    {
        return $this->hasOne(Buildings::className(), ['id' => 'building_info']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommunities()
    {
        return $this->hasOne(Communities::className(), ['id' => 'community']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubCommunities()
    {
        return $this->hasOne(SubCommunities::className(), ['id' => 'sub_community']);
    }

    public function getUserCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUserUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
