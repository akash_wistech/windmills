<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ListData;

/**
* ListDataSearch represents the model behind the search form of `app\models\ListData`.
*/
class ListDataSearch extends ListData
{
    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['listings_reference', 'source', 'listing_website_link', 'listing_date', 'building_info', 
            'city_id', 'property_category', 'no_of_bedrooms', 'built_up_area', 'land_size', 'listings_price', 
            'listings_rent', 'final_price', 'status', 'property_type', 'community', 'sub_community',
            'purpose', 'move_to_listing','list_data_date'], 'safe'],
        ];
    }
    
    /**
    * {@inheritdoc}
    */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
    
    /**
    * Creates data provider instance with search query applied
    *
    * @param array $params
    *
    * @return ActiveDataProvider
    */
    public function search($params)
    {
        $query = ListData::find();
        
        // add conditions that should always apply here
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['list_data_date' => SORT_DESC]]
        ]);
        
        $this->load($params);
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        
        $query->andFilterWhere(['like', 'listings_reference', $this->listings_reference])
        ->andFilterWhere(['like', 'source', $this->source])
        ->andFilterWhere(['like', 'listing_website_link', $this->listing_website_link])
        ->andFilterWhere(['like', 'listing_date', $this->listing_date])
        ->andFilterWhere(['like', 'building_info', $this->building_info])
        ->andFilterWhere(['like', 'city_id', $this->city_id])
        ->andFilterWhere(['like', 'property_category', $this->property_category])
        ->andFilterWhere(['like', 'no_of_bedrooms', $this->no_of_bedrooms])
        ->andFilterWhere(['like', 'built_up_area', $this->built_up_area])
        ->andFilterWhere(['like', 'land_size', $this->land_size])
        ->andFilterWhere(['like', 'listings_price', $this->listings_price])
        ->andFilterWhere(['like', 'listings_rent', $this->listings_rent])
        ->andFilterWhere(['like', 'final_price', $this->final_price])
        ->andFilterWhere(['like', 'status', $this->status])
        ->andFilterWhere(['like', 'property_type', $this->property_type])
        ->andFilterWhere(['like', 'community', $this->community])
        ->andFilterWhere(['like', 'sub_community', $this->sub_community])
        ->andFilterWhere(['like', 'purpose', $this->purpose])
        ->andFilterWhere(['like', 'move_to_listing', $this->move_to_listing])
        ->andFilterWhere(['like', 'list_data_date', $this->list_data_date])
        ->andFilterWhere(['not like', 'built_up_area', '._05477e53'])
        ->andFilterWhere(['not like', 'listings_price', '._05477e53'])
        ->andFilterWhere(['not like', 'listings_rent', '._05477e53'])
        ->andFilterWhere(['not like', 'final_price', '._05477e53']);

        return $dataProvider;
    }
}
