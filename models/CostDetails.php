<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "cost_details".
 *
 * @property int $id
 * @property float $original_purchase_price
 * @property float $transaction_price
 * @property float $lands_price
 * @property float $parking_price
 * @property float $pool_price
 * @property float $landscape_price
 * @property float $utilities_connected_price
 * @property float $white_goods_price
 * @property int $valuation_id
 * @property int $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class CostDetails extends ActiveRecordFull
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cost_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'pool_price', 'landscape_price', 'white_goods_price', 'valuation_id'], 'required'],
            [['original_purchase_price', 'original_land_purchase_price', 'transaction_price', 'lands_price', 'parking_price', 'pool_price', 'landscape_price', 'utilities_connected_price', 'white_goods_price','land_lease_costs'], 'number'],
            [['valuation_id', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by','number_of_years'], 'integer'],
            [['created_at', 'updated_at', 'trashed_at','transaction_source','transaction_price_date','source_of_original_date_price','date_of_original_date_price'], 'safe'],
            [['original_purchase_price_available', 'original_land_purchase_price_available', 'original_purchase_date', 'original_land_purchase_date', 'source_of_original_purchase_details', 'source_of_original_land_purchase_details', 'acquisition_method', 'land_acquisition_method','current_transaction_price_available','current_transaction_price_date','source_of_transaction_details'], 'safe'],
            [['land_lease_cost_available', 'land_lease_start_date','land_lease_expiry_date','total_number_of_lease_years','total_no_of_lease_years','consultants_agreement_costs','contracting_costs','governmnet_authority_costs','dewa_sewa_fewa_costs','developers_master_costs' ], 'safe'],



        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'original_purchase_price' => 'Original Purchase Price',
            'transaction_price' => 'Transaction Price',
            'lands_price' => 'Lands Price',
            'parking_price' => 'Parking Price',
            'pool_price' => 'Pool Price',
            'landscape_price' => 'Landscape Price',
            'utilities_connected_price' => 'Utilities Connected Price',
            'white_goods_price' => 'White Goods Price',
            'valuation_id' => 'Valuation ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }
    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Cost Details';
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->valuation_id;
    }
}
