<?php

namespace app\models;

use Yii;

// dd($_REQUEST);

class KpiManager extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kpi_manager';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'employe',
                ],
                'integer'
            ],

            [
                [
                    'reference_number',
                    'description',
                    'department',
                    'start_date',
                    'end_date',
                    'unit',
                    'achievement_score'
                ],   'safe'
            ],
            
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'reference_number' => 'reference_number', 
            'description' => 'description', 
            'sequence' => 'sequence', 
            'department' => 'department', 
            'weightage' => 'weightage', 
            'start_date' => 'start_date', 
            'end_date' => 'end_date', 
            'achievement_score' => 'achievement_score', 
            'employe' => 'employe', 
            'approved_by' => 'approved_by', 
        ];
    }

    public function getDepartmentRelation()
    {
        return $this->hasOne(Department::class, ['id' => 'department']);
    }

    public function getUnitRelation()
    {
        return $this->hasOne(Units::class, ['id' => 'unit']);
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // Convert start_date from d-m-Y to Y-m-d
            if ($this->start_date) {
                $this->start_date = date('Y-m-d', strtotime($this->start_date));
            }

            // Convert end_date from d-m-Y to Y-m-d
            if ($this->end_date) {
                $this->end_date = date('Y-m-d', strtotime($this->end_date));
            }

            return true;
        }
        return false;
    }

    
}
