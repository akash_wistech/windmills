<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "income_approach".
 *
 * @property int $id
 * @property float $op_maintainance_costs
 * @property float $vacancy
 * @property float $land_lease_costs
 * @property float $utilities_cost
 * @property float $net_yield_applied
 * @property float $gross_yields
 * @property int $valuation_id
 * @property int $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class IncomeApproach extends ActiveRecordFull
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'income_approach';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['vacancy', 'land_lease_costs', 'utilities_cost', 'net_yield_applied', 'gross_source'], 'required'],
            [['vacancy', 'land_lease_costs', 'utilities_cost', 'net_yield_applied', 'gross_source'], 'number'],
            [['valuation_id', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['vacancy', 'land_lease_costs', 'utilities_cost', 'net_yield_applied', 'gross_source','created_at', 'updated_at', 'trashed_at','estimated_market_value'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'op_maintainance_costs' => 'Op Maintainance Costs',
            'vacancy' => 'Vacancy',
            'land_lease_costs' => 'Land Lease Costs',
            'utilities_cost' => 'Utilities Cost',
            'net_yield_applied' => 'Net Yield Applied',
            'gross_source' => 'Gross Source',
            'valuation_id' => 'Valuation ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'income_approach';
    }
    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->id;
    }
}
