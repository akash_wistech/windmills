<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Buildings;

/**
 * BuildingsSearch represents the model behind the search form of `app\models\Buildings`.
 */
class BuildingsSearch extends Buildings
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'community', 'sub_community', 'city', 'property_category', 'tenure', 'property_placement', 'property_visibility', 'property_exposure', 'property_condition', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by', 'developer_id', 'number_of_basement', 'parking_floors', 'typical_floors', 'building_number', 'makani_number', 'property_id'], 'integer'],
            [['title', 'latitude', 'longitude', 'created_at', 'updated_at', 'trashed_at', 'developer_id', 'other_facilities', 'development_type', 'finished_status', 'pool', 'gym', 'play_area', 'completion_status', 'landscaping', 'white_goods', 'furnished', 'utilities_connected'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Buildings::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'community' => $this->community,
            'sub_community' => $this->sub_community,
            'city' => $this->city,
            'developer_id' => $this->developer_id,
            'property_id' => $this->property_id,
            'property_category' => $this->property_category,
            'property_placement' => $this->property_placement,
            'property_visibility' => $this->property_visibility,
            'property_exposure' => $this->property_exposure,
            'property_condition' => $this->property_condition,
            'other_facilities' => $this->other_facilities,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => 0,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'latitude', $this->latitude])
            ->andFilterWhere(['like', 'longitude', $this->longitude])
            ->andFilterWhere(['like', 'development_type', $this->development_type])
            ->andFilterWhere(['like', 'finished_status', $this->finished_status])
            ->andFilterWhere(['like', 'pool', $this->pool])
            ->andFilterWhere(['like', 'gym', $this->gym])
            ->andFilterWhere(['like', 'play_area', $this->play_area])
            ->andFilterWhere(['like', 'completion_status', $this->completion_status])
            ->andFilterWhere(['like', 'landscaping', $this->landscaping])
            ->andFilterWhere(['like', 'white_goods', $this->white_goods])
            ->andFilterWhere(['like', 'furnished', $this->furnished])
            ->andFilterWhere(['like', 'utilities_connected', $this->utilities_connected]
            );

        return $dataProvider;
    }
}
