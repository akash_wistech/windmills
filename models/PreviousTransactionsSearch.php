<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PreviousTransactions;

/**
 * PreviousTransactionsSearch represents the model behind the search form of `app\models\PreviousTransactions`.
 */
class PreviousTransactionsSearch extends PreviousTransactions
{
    public $date_from;
    public $date_to;
    public $building_info_data;
    public $property_id;
    public $property_category;
    public $bedroom_from;
    public $bedroom_to;
    public $landsize_from;
    public $landsize_to;
    public $bua_from;
    public $bua_to;
    public $price_from;
    public $price_to;
    public $price_psf_from;
    public $price_psf_to;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'property_category', 'property_id', 'tenure', 'unit_number', 'building_info', 'valuation_type', 'valuation_method', 'taqyimee_number', 'property_condition', 'property_placement', 'property_exposure', 'floor_number', 'no_of_bedrooms', 'purpose_of_valuation', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['property_id','bua_from','bua_to','price_from','price_to','price_psf_from','price_psf_to','bedroom_from','bedroom_to','landsize_from','landsize_to','date_from','date_to','building_info_data','property_id','property_category','instruction_date', 'inspection_date', 'approver', 'target_date', 'due_date', 'date_submitted', 'reference_number', 'client_reference', 'client_name', 'client_type', 'client_customer_name', 'contact_person', 'designation', 'contact_number', 'plot_number', 'valuer', 'finished_status', 'land_size', 'created_at', 'updated_at', 'trashed_at','community','sub_community'], 'safe'],
            [['fee', 'market_value_of_land', 'built_up_area', 'estimated_age', 'transaction_price', 'market_value', 'psf'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PreviousTransactions::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'instruction_date' => $this->instruction_date,
            'inspection_date' => $this->inspection_date,
            'fee' => $this->fee,
            'target_date' => $this->target_date,
            'due_date' => $this->due_date,
            'date_submitted' => $this->date_submitted,
            'property_category' => $this->property_category,
            'property_id' => $this->property_id,
            'tenure' => $this->tenure,
            'unit_number' => $this->unit_number,
            'building_info' => $this->building_info,
            'community' => $this->community,
            'sub_community' => $this->sub_community,
            'valuation_type' => $this->valuation_type,
            'valuation_method' => $this->valuation_method,
            'taqyimee_number' => $this->taqyimee_number,
            'property_condition' => $this->property_condition,
            'property_placement' => $this->property_placement,
            'property_exposure' => $this->property_exposure,
            'floor_number' => $this->floor_number,
            'no_of_bedrooms' => $this->no_of_bedrooms,
            'market_value_of_land' => $this->market_value_of_land,
            'built_up_area' => $this->built_up_area,
            'estimated_age' => $this->estimated_age,
            'transaction_price' => $this->transaction_price,
            'market_value' => $this->market_value,
            'psf' => $this->psf,
            'purpose_of_valuation' => $this->purpose_of_valuation,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => $this->trashed,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
        ]);

        $query->andFilterWhere(['like', 'approver', $this->approver])
            ->andFilterWhere(['like', 'reference_number', $this->reference_number])
            ->andFilterWhere(['like', 'client_reference', $this->client_reference])
            ->andFilterWhere(['like', 'client_name', $this->client_name])
            ->andFilterWhere(['like', 'client_type', $this->client_type])
            ->andFilterWhere(['like', 'client_customer_name', $this->client_customer_name])
            ->andFilterWhere(['like', 'contact_person', $this->contact_person])
            ->andFilterWhere(['like', 'designation', $this->designation])
            ->andFilterWhere(['like', 'contact_number', $this->contact_number])
            ->andFilterWhere(['like', 'plot_number', $this->plot_number])
            ->andFilterWhere(['like', 'valuer', $this->valuer])
            ->andFilterWhere(['like', 'finished_status', $this->finished_status])
            ->andFilterWhere(['like', 'land_size', $this->land_size]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchvaluation($params)
    {
        $user=User::findOne(\Yii::$app->user->id);
        if ($user['permission_group_id']==3) {
            $query = PreviousTransactions::find()->limit(\Yii::$app->helperFunctions->getTransactionData())->orderBy([
               // 'date_submitted' => SORT_DESC,
                'id' => SORT_DESC,
            ]);
        }else {
            $query = PreviousTransactions::find()->orderBy([
               // 'date_submitted' => SORT_DESC,
                'id' => SORT_DESC,
            ]);
        }
        //$query = PreviousTransactions::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

       /* echo "<pre>";
        print_r($this);
        die;*/

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'property_category' => $this->property_category,
            'property_id' => $this->property_id,
        ]);
        $query->andFilterWhere([
            'between', 'date_submitted', $this->date_from, $this->date_to
        ]);
        $buildings =array();
        if($this->building_info_data <> null){
            $buildings  = explode(",",$this->building_info_data);
        }
         $query->andFilterWhere(['IN', 'building_info', $buildings]);
        $query->andFilterWhere([
            'between', 'no_of_bedrooms', $this->bedroom_from, $this->bedroom_to
        ]);
        $query->andFilterWhere([
            'between', 'land_size', $this->landsize_from, $this->landsize_to
        ]);
        $query->andFilterWhere([
            'between', 'built_up_area', $this->bua_from, $this->bua_to
        ]);
        $query->andFilterWhere([
            'between', 'market_value', $this->price_from, $this->price_to
        ]);
        $query->andFilterWhere([
            'between', 'psf', $this->price_psf_from, $this->price_psf_to
        ]);

        /* $query->andFilterWhere(['like', 'listings_reference', $this->listings_reference])
             ->andFilterWhere(['like', 'listing_website_link', $this->listing_website_link])
             ->andFilterWhere(['like', 'unit_number', $this->unit_number])
             ->andFilterWhere(['like', 'land_size', $this->land_size])
             ->andFilterWhere(['like', 'balcony_size', $this->balcony_size])
             ->andFilterWhere(['like', 'development_type', $this->development_type])
             ->andFilterWhere(['like', 'finished_status', $this->finished_status])
             ->andFilterWhere(['like', 'pool', $this->pool])
             ->andFilterWhere(['like', 'gym', $this->gym])
             ->andFilterWhere(['like', 'play_area', $this->play_area])
             ->andFilterWhere(['like', 'completion_status', $this->completion_status])
             ->andFilterWhere(['like', 'landscaping', $this->landscaping])
             ->andFilterWhere(['like', 'white_goods', $this->white_goods])
             ->andFilterWhere(['like', 'furnished', $this->furnished])
             ->andFilterWhere(['like', 'utilities_connected', $this->utilities_connected])
             ->andFilterWhere(['like', 'developer_margin', $this->developer_margin])
             ->andFilterWhere(['like', 'agent_name', $this->agent_name])
             ->andFilterWhere(['like', 'agent_company', $this->agent_company]);*/

        return $dataProvider;
    }
}
