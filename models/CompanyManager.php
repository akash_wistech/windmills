<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%company_manager}}".
*
* @property integer $company_id
* @property integer $staff_id
*/
class CompanyManager extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%company_manager}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['company_id', 'staff_id'], 'required'],
      [['company_id', 'staff_id'], 'integer'],
    ];
  }

  public static function primaryKey()
  {
  	return ['company_id','staff_id'];
  }
}
