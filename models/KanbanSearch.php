<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii\db\Expression;

/**
* KanbanSearch
*/
class KanbanSearch extends Model
{
  public $module,$wf,$wfi;

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['wf','pageSize'], 'integer'],
      [['module','wfi'],'safe'],
    ];
  }

  /**
  * Generates query for the search
  *
  * @param array $params
  *
  * @return ActiveRecord
  */
  public function generateQuery($params)
  {
    $this->load($params);

    $query = WorkflowDataItem::find()
    ->select([
      'wfsid'=>WorkflowDataItem::tableName().'.workflow_stage_id',
      WorkflowDataItem::tableName().'.module_id',
      Opportunity::tableName().'.id',
      Opportunity::tableName().'.rec_type',
      Opportunity::tableName().'.title',
      Opportunity::tableName().'.descp',
      // 'cp_name'=>'CONCAT('.User::tableName().'.firstname," ",'.User::tableName().'.lastname)',
      // 'cp_job_title'=>'jt.title',
      // 'cp_mobile'=>UserProfileInfo::tableName().'.mobile',
      // 'cp_email'=>User::tableName().'.email',
      // Company::tableName().'.status',
    ])
    ->leftJoin(Opportunity::tableName(),WorkflowDataItem::tableName().'.module_id='.Opportunity::tableName().'.id')
    ->asArray();

    $query->andFilterWhere([
      WorkflowDataItem::tableName().'.module_type' => $this->module,
      WorkflowDataItem::tableName().'.workflow_id' => $this->wf,
      WorkflowDataItem::tableName().'.workflow_stage_id' => $this->wfi,
    ]);
    $query->andWhere([
      'is',Opportunity::tableName().'.deleted_at',new Expression('null')
    ]);

    return $query;
  }
}
