<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reinstatement_costs_values".
 *
 * @property int $id
 * @property int|null $property_id
 * @property float|null $low
 * @property float|null $middle
 * @property float|null $high
 */
class ReinstatementCostsValues extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reinstatement_costs_values';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['property_id','parent_id'], 'integer'],
            [['low', 'middle', 'high','low_to', 'middle_to', 'high_to','low_m', 'middle_m', 'high_m'], 'number'],
            [['low', 'middle', 'high','low_to', 'middle_to', 'high_to','low_m', 'middle_m', 'high_m'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'property_id' => 'Property ID',
            'low' => 'Low',
            'middle' => 'Middle',
            'high' => 'High',
        ];
    }
}
