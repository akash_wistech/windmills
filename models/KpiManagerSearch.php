<?php

namespace app\models;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KpiManager;


// dd($_REQUEST);
/**
 * TaskmangerSearch represents the model behind the search form of `app\models\KpiManager`.
 */
class KpiManagerSearch extends KpiManager
{
    public $employe;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'employe',
                ],
                'integer'
            ],

            [
                [
                    'reference_number',
                    'description',
                    'department',
                    'start_date',
                    'end_date',
                    'unit',
                    'achievement_score'
                ],   'safe'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $user_id = Yii::$app->user->identity->id;

        // if( isset($user_id) && ($user_id == '1' || $user_id == '111811' || $user_id == '14' || $user_id == '110465' || $user_id == '6319' || $user_id == '110217' || $user_id == '21' || $user_id == '86' || $user_id == '53') )
        // {
        //     $query = KpiManager::find();
        // }else{
            // $query = KpiManager::find()->where(['employe' => $user_id]);
            $query = KpiManager::find();
        // }
    
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // $param = Yii::$app->request->queryParams['KpiManagerSearch'];
        // var_dump($param);

        $query->andFilterWhere(['employe' => $this->employe]);
        $query->andFilterWhere(['start_date' => $this->start_date]); 
        $query->andFilterWhere(['end_date' => $this->end_date]);    
        $query->andFilterWhere(['department' => $this->department]);
        $query->andFilterWhere(['unit' => $this->unit]);

        $query->andFilterWhere(['like', 'description', $this->description])
        ->andFilterWhere(['like','reference_number', $this->reference_number]);
        

        return $dataProvider;

    }

}