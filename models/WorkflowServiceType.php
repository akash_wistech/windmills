<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%workflow_service_type}}".
*
* @property integer $workflow_id
* @property string $service_type
*/
class WorkflowServiceType extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%workflow_service_type}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['workflow_id', 'service_type'], 'required'],
      [['workflow_id','service_type'], 'integer'],
    ];
  }

  public static function primaryKey()
  {
  	return ['workflow_id','service_type'];
  }
}
