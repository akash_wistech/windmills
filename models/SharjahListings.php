<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sharjah_listings".
 *
 * @property int $id
 * @property string|null $area
 * @property string|null $property_type
 * @property float|null $price
 * @property string|null $created_date
 */
class SharjahListings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sharjah_listings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price'], 'number'],
            [['created_date'], 'safe'],
            [['area', 'property_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'area' => 'Area',
            'property_type' => 'Property Type',
            'price' => 'Price',
            'created_date' => 'Created Date',
        ];
    }
}
