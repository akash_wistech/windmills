<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "listings_transactions".
 *
 * @property int $id
 * @property string $listings_reference
 * @property string $listing_website_link
 * @property string $transaction_date
 * @property int $property_listed
 * @property int $property_category
 * @property int $tenure
 * @property string $unit_number
 * @property int $floor_number
 * @property int $number_of_levels
 * @property int $location
 * @property string|null $land_size
 * @property float $built_up_area
 * @property string|null $balcony_size
 * @property int $property_placement
 * @property int $property_visibility
 * @property int $property_exposure
 * @property int $listing_property_type
 * @property int $property_condition
 * @property string|null $development_type
 * @property string|null $finished_status
 * @property string|null $pool
 * @property string|null $gym
 * @property string|null $play_area
 * @property int $other_facilities
 * @property string|null $completion_status
 * @property int $no_of_bedrooms
 * @property int $upgrades
 * @property int $full_building_floors
 * @property int $parking_space
 * @property int $view
 * @property string|null $landscaping
 * @property string|null $white_goods
 * @property string|null $furnished
 * @property string|null $utilities_connected
 * @property string|null $developer_margin
 * @property float|null $listings_price
 * @property float|null $listings_rent
 * @property float|null $price_per_sqt
 * @property float|null $final_price
 * @property string|null $agent_name
 * @property int|null $agent_phone_no
 * @property string|null $agent_company
 * @property int $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class SoldTransactionAd extends ActiveRecordFull
{
    public $status_verified;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sold_transaction_ad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'type', 'property_category', 'tenure', 'number_of_levels', 'location', 'built_up_area', 'property_placement', 'property_visibility', 'property_exposure', 'property_condition', 'no_of_bedrooms', 'upgrades', 'full_building_floors', 'parking_space', 'building_info'], 'required', 'on'=>'create'],
            [['building_info'], 'required', 'on'=>'import'],
            [['property_category', 'tenure', 'number_of_levels', 'location', 'property_visibility', 'property_condition', 'no_of_bedrooms', 'upgrades', 'full_building_floors', 'parking_space', 'agent_phone_no', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by', 'building_info'], 'integer'],
            [['transaction_date', 'created_at', 'updated_at', 'trashed_at', 'other_facilities', 'number_of_rooms', 'market_value','land_size','community','sub_community','property_id', 'listing_property_type', 'property_exposure','list_type','duplicate_status'], 'safe'],
            [['built_up_area', 'listings_price', 'listings_rent', 'price_per_sqt', 'final_price','land_size', 'completion_status', 'property_placement'], 'number'],
            [['development_type', 'finished_status', 'pool', 'gym', 'play_area', 'landscaping', 'white_goods', 'furnished', 'utilities_connected', 'developer_margin'], 'string'],
            [['unit_number', 'balcony_size', 'agent_name', 'agent_company'], 'string', 'max' => 255],
            [['status_verified','status_verified_at','status_verified_by','parking_space_number', 'floor_number', 'unit_number'], 'safe'],
            [['transaction_type','sub_type','sales_sequence','reidin_ref_number', 'reidin_community','reidin_property','reidin_property_type','reidin_developer','municipality','district','projectsub_comm','property_subtype','sold_db_id','valuation_amount'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'transaction_date' => 'Transaction Date',
            'property_category' => 'Property Category',
            'building_info' => 'Building Info',
            'community' => 'Community',
            'sub_community' => 'Sub Community',
            'tenure' => 'Tenure',
            'unit_number' => 'Unit Number',
            'floor_number' => 'Floor Number',
            'number_of_rooms' => 'No. of Rooms',
            'market_value' => 'Market Value',
            'number_of_levels' => 'Number Of Levels',
            'location' => 'Location',
            'land_size' => 'Land Size',
            'built_up_area' => 'Built Up Area',
            'balcony_size' => 'Balcony Size',
            'property_placement' => 'Property Placement',
            'property_visibility' => 'Property Visibility',
            'property_exposure' => 'Property Exposure',
            'listing_property_type' => 'Property Type',
            'property_condition' => 'Property Condition',
            'development_type' => 'Development Type',
            'finished_status' => 'Finished Status',
            'pool' => 'Pool',
            'gym' => 'Gym',
            'play_area' => 'Play Area',
            'other_facilities' => 'Other Facilities',
            'completion_status' => 'Completion Status',
            'no_of_bedrooms' => 'No Of Bedrooms',
            'upgrades' => 'Upgrades',
            'full_building_floors' => 'Full Building Floors',
            'parking_space' => 'Parking Space',
            'landscaping' => 'Landscaping',
            'white_goods' => 'White Goods',
            'furnished' => 'Furnished',
            'utilities_connected' => 'Utilities Connected',
            'developer_margin' => 'Developer Margin',
            'listings_price' => 'Price',
            'listings_rent' => ' Rent',
            'price_per_sqt' => 'Price Per Sqt',
            'final_price' => 'Final Price',
            'agent_name' => 'Agent Name',
            'agent_phone_no' => 'Agent Phone No',
            'agent_company' => 'Agent Company',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }

    public function beforeSave($insert)
    {

        if (!empty($this->other_facilities)) {
            $this->other_facilities = implode(",", $this->other_facilities);
        }
        if(!empty($this->building)){
            $this->community = $this->building->community;
            $this->sub_community = $this->building->sub_community;
            $this->property_id = $this->building->property_id;
        }

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Listings Transactions';
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->id;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuilding()
    {
        return $this->hasOne(Buildings::className(), ['id' => 'building_info']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCommunities()
    {
        return $this->hasOne(Communities::className(), ['id' => 'community']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubCommunities()
    {
        return $this->hasOne(SubCommunities::className(), ['id' => 'sub_community']);
    }
}
