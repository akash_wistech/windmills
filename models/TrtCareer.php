<?php

namespace app\models;

use Yii;

class TrtCareer extends \yii\db\ActiveRecord
{
    public $status_verified;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trt_career';
    }
}
