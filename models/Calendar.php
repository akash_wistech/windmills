<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
* This is the model class for table "{{%calendar}}".
*
* @property integer $id
* @property string $title
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
* @property integer $trashed
* @property string $trashed_at
* @property integer $trashed_by
*/
class Calendar extends ActiveRecordFull
{

	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%calendar}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['title'], 'required'],
			[['created_at', 'updated_at', 'trashed_at'], 'safe'],
			[['created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
			[['title'], 'string', 'max' => 255],
      [['title'],'trim'],
		];
	}

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Title'),
			'created_at' => Yii::t('app', 'Created At'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'updated_by' => Yii::t('app', 'Updated By'),
			'trashed' => Yii::t('app', 'Trashed'),
			'trashed_at' => Yii::t('app', 'Trashed At'),
			'trashed_by' => Yii::t('app', 'Trashed By'),
		];
	}

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecTitle()
  {
    return $this->title;
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecType()
  {
    return 'Calendar';
  }
}
