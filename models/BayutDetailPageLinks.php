<?php

namespace app\models;
use yii\db\ActiveRecord;

use Yii;

class BayutDetailPageLinks extends ActiveRecord
{

	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%bayut_detail_page_links}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['url','url_child','status'],'safe'],
		];
	}


}
