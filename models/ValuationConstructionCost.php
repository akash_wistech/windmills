<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "valuation_construction_cost".
 *
 * @property int $id
 * @property int|null $valuation_id
 * @property int|null $construction_factor
 * @property float|null $construction_size
 * @property float|null $construction_status
 * @property int|null $construction_calculations
 * @property int|null $total_value_by_cost
 * @property int|null $price_per_sf_bua
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property string|null $deleted_at
 * @property int|null $deleted_by
 * @property int|null $status
 */
class ValuationConstructionCost extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'valuation_construction_cost';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['valuation_id', 'construction_factor','created_by', 'updated_by', 'deleted_by', 'status'], 'integer'],
            [['construction_size', 'construction_status'], 'number'],
            [['created_at', 'updated_at', 'deleted_at','construction_calculations','total_value_by_cost','price_per_sf_bua'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'valuation_id' => 'Valuation ID',
            'construction_factor' => 'Construction Factor',
            'construction_size' => 'Construction Size',
            'construction_status' => 'Construction Status',
            'construction_calculations' => 'Construction Calculations',
            'total_value_by_cost' => 'Total Value By Cost',
            'price_per_sf_bua' => 'Price Per Sf Bua',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
            'status' => 'Status',
        ];
    }
}
