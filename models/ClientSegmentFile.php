<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client_segment_file".
 *
 * @property int $id
 * @property int|null $client_type
 * @property int|null $developer
 * @property int|null $family
 * @property int|null $publicly_listed
 * @property int|null $private_company
 * @property int|null $audit_firm
 * @property int|null $law_firm
 * @property int|null $quotation_followup_period__days
 * @property int|null $report_follow_update_days
 * @property int|null $client_services_feedback_days
 */
class ClientSegmentFile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_segment_file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_type', 'developer', 'family', 'publicly_listed', 'private_company', 'audit_firm', 'law_firm', 'quotation_followup_period__days', 'report_follow_update_days', 'client_services_feedback_days','quotation_followup_email_no','fee_crm'], 'safe'],
            [['status_verified', 'status_verified_at', 'status_verified_by'], 'safe'],
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'status_verified_by']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_type' => 'Client Type',
            'developer' => 'Developer',
            'family' => 'Family',
            'publicly_listed' => 'Publicly Listed',
            'private_company' => 'Private Company',
            'audit_firm' => 'Audit Firm',
            'law_firm' => 'Law Firm',
            'quotation_followup_email_no' => 'Total No of Follow Up Email',
            'quotation_followup_period__days' => 'Frequency in Working Days',
            'report_follow_update_days' => 'Report Follow Update Days',
            'client_services_feedback_days' => 'Client Services Feedback Days',
        ];
    }

    public function beforeSave($insert){

        
        // verify status reset after edit only something changed
        $isChanged = false;
        $ignoredAttributes = ['updated_at','status_verified_at']; 
        foreach ($this->attributes as $attribute => $value) {
            if (in_array($attribute, $ignoredAttributes)) {
                continue;
            }
            if ($this->getOldAttribute($attribute) != $value) {
                $isChanged = true;
                break;
            }
        }
        if ($isChanged) {
            if($this->status_verified == 1 && $this->getOldAttribute('status_verified') == 1){
                $this->status_verified = 2;
            }elseif ($this->status_verified == 2 && $this->getOldAttribute('status_verified') == 1) {
                $this->status_verified = 1;
            }
        }
        
        return parent::beforeSave($insert);
    }
}
