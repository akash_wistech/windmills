<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "temp_copmany".
 *
 * @property int $id
 * @property int $company_id
 */
class TempCopmany extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'temp_copmany';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id'], 'required'],
            [['company_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
        ];
    }
    public function AfterSave(){

    }
}
