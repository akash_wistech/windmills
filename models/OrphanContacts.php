<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orphan_contacts".
 *
 * @property int $id
 * @property string|null $type
 * @property string|null $name
 * @property string|null $email
 * @property string|null $mobile
 * @property string|null $company
 * @property string|null $designation
 * @property string|null $address
 * @property string|null $country
 * @property string|null $city
 */
class OrphanContacts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orphan_contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address'], 'string'],
            [['type', 'name', 'email', 'mobile', 'company', 'designation', 'country', 'city'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'name' => 'Name',
            'email' => 'Email',
            'mobile' => 'Mobile',
            'company' => 'Company',
            'designation' => 'Designation',
            'address' => 'Address',
            'country' => 'Country',
            'city' => 'City',
        ];
    }
}
