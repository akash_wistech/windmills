<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "cost_approach".
 *
 * @property int $id
 * @property float $land_market_value_factor
 * @property float $financing_charges_for_land_factor
 * @property float $construction_costs_actor
 * @property float $professional_charges_factor
 * @property float $contingency_margin_factor
 * @property float $number_of_years_factor
 * @property float $interest_rate_factor
 * @property float $effective_interest_rate_factor
 * @property float $depreciation_factor
 * @property float|null $obscolence_factor
 * @property float $developer_profit_factor
 * @property int $valuation_id
 * @property int $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class CostApproach extends ActiveRecordFull
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cost_approach';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['land_market_value_factor', 'financing_charges_for_land_factor', 'construction_costs_factor', 'professional_charges_factor', 'contingency_margin_factor', 'number_of_years_factor', 'interest_rate_factor', 'effective_interest_rate_factor', 'depreciation_factor', 'obscolence_factor', 'developer_profit_factor'], 'number'],
            [['valuation_id', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['created_at', 'updated_at', 'trashed_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'land_market_value_factor' => 'Land Market Value Factor',
            'financing_charges_for_land_factor' => 'Financing Charges For Land Factor',
            'construction_costs_factor' => 'Construction Costs Actor',
            'professional_charges_factor' => 'Professional Charges Factor',
            'contingency_margin_factor' => 'Contingency Margin Factor',
            'number_of_years_factor' => 'Number Of Years Factor',
            'interest_rate_factor' => 'Interest Rate Factor',
            'effective_interest_rate_factor' => 'Effective Interest Rate Factor',
            'depreciation_factor' => 'Depreciation Factor',
            'obscolence_factor' => 'Obscolence Factor',
            'developer_profit_factor' => 'Developer Profit Factor',
            'valuation_id' => 'Valuation ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'cost_approach';
    }
    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->id;
    }
}
