<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\LandSolds;

/**
 * LandSoldsSearch represents the model behind the search form of `app\models\LandSolds`.
 */
class LandSoldsSearch extends LandSolds
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['transaction_type', 'subtype', 'sales_sequence', 'reidin_ref', 'transaction_date', 'community', 'property', 'property_type', 'unit', 'bedrooms', 'floor', 'parking', 'balcony_area', 'size_sqf', 'land_size', 'amount', 'sqf', 'developer'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LandSolds::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'transaction_type', $this->transaction_type])
            ->andFilterWhere(['like', 'subtype', $this->subtype])
            ->andFilterWhere(['like', 'sales_sequence', $this->sales_sequence])
            ->andFilterWhere(['like', 'reidin_ref', $this->reidin_ref])
            ->andFilterWhere(['like', 'transaction_date', $this->transaction_date])
            ->andFilterWhere(['like', 'community', $this->community])
            ->andFilterWhere(['like', 'property', $this->property])
            ->andFilterWhere(['like', 'property_type', $this->property_type])
            ->andFilterWhere(['like', 'unit', $this->unit])
            ->andFilterWhere(['like', 'bedrooms', $this->bedrooms])
            ->andFilterWhere(['like', 'floor', $this->floor])
            ->andFilterWhere(['like', 'parking', $this->parking])
            ->andFilterWhere(['like', 'balcony_area', $this->balcony_area])
            ->andFilterWhere(['like', 'size_sqf', $this->size_sqf])
            ->andFilterWhere(['like', 'land_size', $this->land_size])
            ->andFilterWhere(['like', 'amount', $this->amount])
            ->andFilterWhere(['like', 'sqf', $this->sqf])
            ->andFilterWhere(['like', 'developer', $this->developer]);

        return $dataProvider;
    }
}
