<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Company;

/**
* CompanySearch represents the model behind the search form about `app\models\Company`.
*/
class CompanySearch extends Company
{
  public $cp_name,$cp_job_title,$cp_mobile,$cp_email,$pageSize;

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by','pageSize','data_type','segment_type'], 'integer'],
      [['title', 'cp_name', 'cp_job_title', 'cp_mobile', 'cp_email', 'created_at', 'updated_at', 'trashed_at','data_type','segment_type','instruction_via_quotation','fee_with_bua_check'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $query = $this->generateQuery($params);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
      'sort' => $this->defaultSorting,
    ]);

    return $dataProvider;
  }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchMy($params, $only_clients=null)
    {
        $query = $this->generateQuery($params, $only_clients);
        // echo "<pre>"; print_r($query); echo "</pre>"; die();
// print_r(Yii::$app->menuHelperFunction->getListingTypeByController('client')); die();
        //Check Listing type allowed
        if(Yii::$app->menuHelperFunction->getListingTypeByController('client')==2){
            // echo "string"; die();
            $query->innerJoin(CompanyManager::tableName(),CompanyManager::tableName().".company_id=".Company::tableName().".id");
            $query->andFilterWhere([
                CompanyManager::tableName().'.staff_id' => Yii::$app->user->identity->id]);
        }else{
            // die('hello');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
            ],
            'sort' => $this->defaultSorting,
        ]);

        return $dataProvider;
    }

    /**
     * Generates query for the search
     *
     * @param array $params
     *
     * @return ActiveRecord
     */
    public function generateQuery($params, $only_clients=null)
    {

        $this->load($params);
        // echo "<pre>"; print_r($params); echo "</pre>"; die();

        $query = Company::find()
            ->select([
                Company::tableName().'.id',
                Company::tableName().'.title',
                'cp_name'=>'CONCAT('.User::tableName().'.firstname," ",'.User::tableName().'.lastname)',
                'cp_job_title'=>JobTitle::tableName().'.title',
                'cp_mobile'=>UserProfileInfo::tableName().'.mobile',
                'cp_email'=>User::tableName().'.email',
                Company::tableName().'.status',
                Company::tableName().'.segment_type',
            ])
            ->leftJoin(User::tableName(),User::tableName().'.company_id='.Company::tableName().'.id')
            ->leftJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().'.user_id='.User::tableName().'.id')
            ->leftJoin(JobTitle::tableName(),JobTitle::tableName().'.id='.UserProfileInfo::tableName().'.job_title_id')
            ->asArray();

        $query->andFilterWhere([
            Company::tableName().'.id' => $this->id,
            Company::tableName().'.status' => $this->status,
            Company::tableName().'.trashed' => 0,
        ]);
        $query->andFilterWhere([
            UserProfileInfo::tableName().'.primary_contact' => 1
        ]);

        if ($only_clients<>null AND $only_clients=='only_clients') {
            $query->andWhere(['allow_for_valuation'=>1]);
            // $query->andWhere([
            //     'or',
            //     [Company::tableName().'.data_type' => 0],
            //     [Company::tableName().'.data_type' => null],
            // ]);
        }

        $query->andFilterWhere(['like', Company::tableName().'.title', $this->title])
            ->andFilterWhere(['=', Company::tableName().'.data_type', $this->data_type])
            ->andFilterWhere(['=', Company::tableName().'.segment_type', $this->segment_type])
            ->andFilterWhere(['or',['like',User::tableName().'.firstname',$this->cp_name],['like',User::tableName().'.lastname',$this->cp_name]])
            ->andFilterWhere(['like',User::tableName().'.email',$this->cp_email])
            ->andFilterWhere(['like',JobTitle::tableName().'.title',$this->cp_job_title])
            ->andFilterWhere(['like',UserProfileInfo::tableName().'.mobile',$this->cp_mobile]);

        return $query;
    }

    /**
     * Default Sorting Options
     *
     * @param array $params
     *
     * @return Array
     */
    public function getDefaultSorting()
    {
        return [
            'defaultOrder' => ['title'=>SORT_ASC],
            'attributes' => [
                'title',
                'cp_name' => [
                    'asc' => [User::tableName().'.firstname' => SORT_ASC, User::tableName().'.lastname' => SORT_ASC],
                    'desc' => [User::tableName().'.firstname' => SORT_DESC, User::tableName().'.lastname' => SORT_DESC],
                ],
                'cp_job_title' => [
                    'asc' => [JobTitle::tableName().'.title' => SORT_ASC],
                    'desc' => [JobTitle::tableName().'.title' => SORT_DESC],
                ],
                'cp_mobile' => [
                    'asc' => [UserProfileInfo::tableName().'.mobile' => SORT_ASC],
                    'desc' => [UserProfileInfo::tableName().'.mobile' => SORT_DESC],
                ],
                'cp_email' => [
                    'asc' => [User::tableName().'.email' => SORT_ASC],
                    'desc' => [User::tableName().'.email' => SORT_DESC],
                ],
                'status',
                'created_at',
            ]
        ];
    }
}
