<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "quotation_fee_master_file".
 *
 * @property int $id
 * @property string|null $heading
 * @property string|null $sub_heading
 * @property string|null $values
 * @property string|null $approach_type
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 */
class QuotationFeeMasterFile extends \yii\db\ActiveRecord
{
    public $client_type;
    public $subject_property;
    public $city;
    public $bcs_type;
    public $number_of_properties;
    public $payment_terms;
    public $tenure;
    public $complexity;
    public $drawings_available;
    public $new_repeat_valuation;
    public $built_up_area_of_subject_property;
    public $built_up_area_of_subject_property_b_land;
    public $net_leasable_area;
    public $types_of_property_unit;
    public $property_fee;
    public $number_of_units_land;
    public $type_of_valuation;
    public $number_of_comparables;
    public $valuation_approach;
    public $base_fee;
    public $relative_discount;
    public $land;
    public $number_of_units_building;
    public $no_of_property_discount;
    public $no_of_assignment_discount;
    public $base_fee_building;
    public $base_fee_others;
    public $first_time_discount;
    public $general_discount;
    public $no_of_units_same_building_discount;
    public $no_of_buildings_same_community_discount;
    public $urgency_fee;
    //  public $approach_type;
    public $upgrades_ratings;
    public $last_three_years_finance;
    public $ten_years_projections;
    public $other_intended_users;
    public $number_of_types;
    public $number_of_rooms_building;
    public $restaurant;
    public $ballrooms;
    public $atms;
    public $retails_units;
    public $night_clubs;
    public $bars;
    public $health_club;
    public $meeting_rooms;
    public $spa;
    public $beach_access;
    public $parking_sale;
    public $asset_category;
    public $no_of_asset;
    public $asset_age;
    public $asset_complexity;
    public $location;
    public $no_of_location;
    public $working_days;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quotation_fee_master_file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'client_type',
                    'subject_property',
                    'city',
                    'number_of_properties',
                    'payment_terms',
                    'tenure',
                    'complexity',
                    'types_of_property_unit',
                    'new_repeat_valuation',
                    'built_up_area_of_subject_property',
                    'number_of_units',
                    'type_of_valuation',
                    'net_leasable_area',
                    'property_fee',
                    'number_of_comparables',
                    'valuation_approach',
                    'base_fee',
                    'relative_discount',
                    'land',
                    'number_of_units_building',
                    'no_of_property_discount',
                    'base_fee_building',
                    'base_fee_others',
                    'number_of_units_land',
                    'drawings_available',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                    'first_time_discount',
                    'general_discount',
                    'no_of_units_same_building_discount',
                    'bcs_type',
                    'no_of_buildings_same_community_discount',
                    'no_of_assignment_discount',
                    'asset_category',
                    'no_of_asset',
                    'asset_age',
                    'asset_complexity',
                    'location',
                    'no_of_location',
                    'working_days'
                ],
                'safe'
            ],
            [['created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['heading', 'sub_heading', 'values', 'urgency_fee', 'approach_type', 'upgrades_ratings', 'last_three_years_finance', 'ten_years_projections', 'built_up_area_of_subject_property_b_land', 'other_intended_users', 'property_type'], 'safe'],

            [['status_verified', 'status_verified_at', 'status_verified_by', 'number_of_types', 'number_of_rooms_building', 'restaurant', 'ballrooms', 'atms', 'retails_units', 'night_clubs', 'bars', 'health_club', 'meeting_rooms', 'spa', 'beach_access', 'parking_sale'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'heading' => 'Heading',
            'sub_heading' => 'Sub Heading',
            'values' => 'Values',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'deleted_by' => 'Deleted By',
        ];
    }
}
