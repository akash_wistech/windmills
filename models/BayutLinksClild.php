<?php

namespace app\models;
use yii\db\ActiveRecord;

use Yii;

class BayutLinksClild extends ActiveRecord
{

	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%bayut_links_clild}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['link_id','url_fetch_fetch'],'safe'],
		];
	}


}
