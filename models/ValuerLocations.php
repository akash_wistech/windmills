<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "valuer_locations".
 *
 * @property int $id
 * @property int|null $valuer_id
 * @property int|null $valuation_id
 * @property string|null $current_latitude
 * @property string|null $current_longitude
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 * @property string|null $difference_in_meter
 * @property string|null $client_latitude
 * @property string|null $client_longitude
 * @property int|null $inpection_id
 * @property string|null $reached_date
 * @property string|null $left_date
 * @property int|null $status_on_location
 */
class ValuerLocations extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'valuer_locations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['valuer_id', 'valuation_id', 'created_by', 'updated_by', 'trashed', 'trashed_by', 'inpection_id', 'status_on_location'], 'integer'],
            [['created_at', 'updated_at', 'trashed_at', 'reached_date', 'left_date'], 'safe'],
            [['current_latitude', 'current_longitude', 'difference_in_meter', 'client_latitude', 'client_longitude'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'valuer_id' => 'Valuer ID',
            'valuation_id' => 'Valuation ID',
            'current_latitude' => 'Current Latitude',
            'current_longitude' => 'Current Longitude',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
            'difference_in_meter' => 'Difference Meter',
            'client_latitude' => 'Client Latitude',
            'client_longitude' => 'Client Longitude',
            'inpection_id' => 'Inpection ID',
            'reached_date' => 'Reached Date',
            'left_date' => 'Left Date',
            'status_on_location' => 'Status Here',
        ];
    }
}
