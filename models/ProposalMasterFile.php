<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proposal_master_file".
 *
 * @property int $id
 * @property string|null $heading
 * @property string|null $sub_heading
 * @property string|null $values
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 */
class ProposalMasterFile extends \yii\db\ActiveRecord
{
    public $client_type;
    public $subject_property;
    public $city;
    public $number_of_properties;
    public $payment_terms;
    public $tenure;
    public $complexity;
    public $new_repeat_valuation;
    public $built_up_area_of_subject_property;
    public $number_of_units_land;
    public $type_of_valuation;
    public $number_of_comparables;
    public $valuation_approach;
    public $base_fee;
    public $relative_discount;
    public $land;
    public $number_of_units_building;
    public $no_of_property_dis;
    public $base_fee_building;
    public $base_fee_others;
    public $first_time_discount;
    public $general_discount;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proposal_master_file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_type','subject_property','city','number_of_properties','payment_terms','tenure','complexity',
              'new_repeat_valuation','built_up_area_of_subject_property','number_of_units','type_of_valuation',
              'number_of_comparables','number_of_comparables','valuation_approach','base_fee','relative_discount','land','number_of_units_building','no_of_property_dis','base_fee_building','base_fee_others','number_of_units_land',
              'created_at', 'updated_at', 'deleted_at','first_time_discount','general_discount'], 'safe'],
            [['created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['heading', 'sub_heading', 'values'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'heading' => 'Heading',
            'sub_heading' => 'Sub Heading',
            'values' => 'Values',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'deleted_by' => 'Deleted By',
        ];
    }
}
