<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\FloorPrice;

/**
 * FloorPriceSearch represents the model behind the search form of `app\models\FloorPrice`.
 */
class FloorPriceSearch extends FloorPrice
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'client', 'property_type', 'sub_property_type', 'city', 'tenure', 'valuation_approach', 'inspection_type', 'priority_type', 'status_verified', 'status_verified_by', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['tat', 'status_verified_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['fee', 'revalidation_fee', 'independent_villa_fee', 'community_villa_fee', 'staff_fee'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FloorPrice::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client' => $this->client,
            'property_type' => $this->property_type,
            'sub_property_type' => $this->sub_property_type,
            'city' => $this->city,
            'tenure' => $this->tenure,
            'fee' => $this->fee,
            'valuation_approach' => $this->valuation_approach,
            'inspection_type' => $this->inspection_type,
            'priority_type' => $this->priority_type,
            'revalidation_fee' => $this->revalidation_fee,
            'independent_villa_fee' => $this->independent_villa_fee,
            'community_villa_fee' => $this->community_villa_fee,
            'staff_fee' => $this->staff_fee,
            'status_verified' => $this->status_verified,
            'status_verified_at' => $this->status_verified_at,
            'status_verified_by' => $this->status_verified_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'tat', $this->tat]);

        return $dataProvider;
    }
}
