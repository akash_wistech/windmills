<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Properties;

/**
 * PropertiesSearch represents the model behind the search form of `app\models\Properties`.
 */
class PropertiesSearch extends Properties
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'category', 'gross_category','valuation_approach', 'age', 'complete_years', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['title', 'approach_reason', 'required_documents', 'created_at', 'updated_at', 'trashed_at','bua_fee','upgrade_fee','land_size_fee','no_of_units_fee'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Properties::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'category' => $this->category,
            'gross_category' => $this->gross_category,
            'valuation_approach' => $this->valuation_approach,
            'age' => $this->age,
            'complete_years' => $this->complete_years,
            'status' =>1,
            'bua_fee' => $this->bua_fee,
            'upgrade_fee' => $this->upgrade_fee,
            'land_size_fee' => $this->land_size_fee,
            'no_of_units_fee' => $this->no_of_units_fee,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => $this->trashed,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'approach_reason', $this->approach_reason])
            ->andFilterWhere(['like', 'required_documents', $this->required_documents]);

        return $dataProvider;
    }
}
