<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DevelopmentCosts;

/**
 * DevelopmentsSearch represents the model behind the search form of `app\models\Developments`.
 */
class DevelopmentCostsSearch extends DevelopmentCosts
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'emirates', 'property_type', 'star1', 'star2', 'star3', 'star4', 'star5', 'parking_price', 'pool_price', 'landscape_price', 'whitegoods_price', 'utilities_connected_price', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['professional_charges', 'contingency_margin', 'obsolescence', 'developer_profit', 'developer_margin', 'interest_rate'], 'number'],
            [['created_at', 'updated_at', 'trashed_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DevelopmentCosts::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'emirates' => $this->emirates,
            'property_type' => $this->property_type,
            'star1' => $this->star1,
            'star2' => $this->star2,
            'star3' => $this->star3,
            'star4' => $this->star4,
            'star5' => $this->star5,
            'professional_charges' => $this->professional_charges,
            'contingency_margin' => $this->contingency_margin,
            'obsolescence' => $this->obsolescence,
            'developer_profit' => $this->developer_profit,
            'parking_price' => $this->parking_price,
            'pool_price' => $this->pool_price,
            'landscape_price' => $this->landscape_price,
            'whitegoods_price' => $this->whitegoods_price,
            'utilities_connected_price' => $this->utilities_connected_price,
            'developer_margin' => $this->developer_margin,
            'interest_rate' => $this->interest_rate,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'trashed_at' => $this->trashed_at,
            'trashed' => $this->trashed,
            'trashed_by' => $this->trashed_by,
        ]);

        return $dataProvider;
    }
}
