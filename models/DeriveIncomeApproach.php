<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "derive_income_approach".
 *
 * @property int $id
 * @property int $valuation_id
 * @property int $income_approach_type
 * @property int $factor
 * @property int $property_financial_source
 * @property int $value_per_square_feet
 * @property int $gross_yields
 * @property int $created_at
 * @property int $created_by
 * @property int $deleted_at
 * @property int $deleted_by
 * @property int $updated_at
 * @property int $updated_by
 * @property int $status
 */
class DeriveIncomeApproach extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'derive_income_approach';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'valuation_id', 'income_approach_type', 'factor', 'property_financial_source', 'value_per_square_feet', 'gross_yields', 'created_at', 'created_by', 'deleted_at', 'deleted_by', 'updated_at', 'updated_by', 'status'], 'safe'],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'valuation_id' => 'Valuation ID',
            'income_approach_type' => 'Income Approach Type',
            'factor' => 'Factor',
            'property_financial_source' => 'Property Financial Source',
            'value_per_square_feet' => 'Value Per Square Feet',
            'gross_yields' => 'Gross Yields',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'status' => 'Status',
        ];
    }
}
