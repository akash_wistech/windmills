<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "crm_quotation_configraions_files".
 *
 * @property int $id
 * @property string $type
 * @property int|null $floor
 * @property int|null $flooring
 * @property int|null $ceilings
 * @property int|null $speciality
 * @property int|null $upgrade
 * @property string|null $attachment
 * @property int $index_id
 * @property int $valuation_id
 * @property int|null $custom_field_id
 * @property int $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class CrmQuotationConfigraionsFiles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_quotation_configraions_files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'index_id', 'quotation_id'], 'required'],
            [['floor', 'flooring', 'ceilings', 'speciality', 'upgrade', 'index_id', 'quotation_id'], 'integer'],
            [['quotation_id','property_index'], 'safe'],
            [['type', 'attachment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'floor' => 'Floor',
            'flooring' => 'Flooring',
            'ceilings' => 'Ceilings',
            'speciality' => 'Speciality',
            'upgrade' => 'Upgrade',
            'attachment' => 'Attachment',
            'index_id' => 'Index ID',
            'valuation_id' => 'Valuation ID',
            'custom_field_id' => 'Custom Field ID',
        ];
    }
}
