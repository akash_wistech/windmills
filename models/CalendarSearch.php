<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Calendar;

/**
* CalendarSearch represents the model behind the search form about `app\models\Calendar`.
*/
class CalendarSearch extends Calendar
{
  public $pageSize;

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id', 'created_by', 'updated_by', 'trashed', 'trashed_by','pageSize'], 'integer'],
      [['title', 'created_at', 'updated_at', 'trashed_at'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);

    $query = Calendar::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
    ]);

    $query->andFilterWhere([
      'id' => $this->id,
      'trashed' => 0,
    ]);
    if(Yii::$app->user->identity->user_type!=20){
      $query->andFilterWhere([
        'created_by' => Yii::$app->user->identity->id,
      ]);
    }

    $query->andFilterWhere(['like', 'title', $this->title]);

    return $dataProvider;
  }
}
