<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "other_facilities".
 *
 * @property int $id
 * @property string $title
 * @property int $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class OtherFacilities extends \yii\db\ActiveRecord
{
    public $status_verified;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'other_facilities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['status', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['created_at', 'updated_at', 'trashed_at'], 'safe'],
            [['title'], 'string', 'max' => 100],
            [['status_verified','status_verified_at','status_verified_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }
}
