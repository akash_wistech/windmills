<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auto_links_sold".
 *
 * @property int $id
 * @property string $link
 * @property int|null $status
 */
class AutoLinksSold extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auto_links_sold';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'link' => 'Link',
            'status' => 'Status',
        ];
    }
}
