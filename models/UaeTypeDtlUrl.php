<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "uae_type_dtl_url".
 *
 * @property int $id
 * @property string|null $url
 * @property int|null $status
 * @property string|null $property_category
 * @property string|null $type
 */
class UaeTypeDtlUrl extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'uae_type_dtl_url';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['url', 'property_category', 'type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'status' => 'Status',
            'property_category' => 'Property Category',
            'type' => 'Type',
        ];
    }
}
