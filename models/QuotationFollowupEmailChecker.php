<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "quotation_followup_email_checker".
 *
 * @property int $id
 * @property int|null $quotation_id
 * @property string|null $date
 */
class QuotationFollowupEmailChecker extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quotation_followup_email_checker';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quotation_id'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'quotation_id' => 'Quotation ID',
            'date' => 'Date',
        ];
    }
}
