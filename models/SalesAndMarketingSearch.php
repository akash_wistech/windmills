<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SalesAndMarketing;

/**
 * SalesAndMarketingSearch represents the model behind the search form of `app\models\SalesAndMarketing`.
 */
class SalesAndMarketingSearch extends SalesAndMarketing
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'meeting_interface', 'purpose', 'calendar_invite', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['client_id', 'date', 'time', 'attendees', 'meeting_place', 'meeting_location_pin', 'created_at', 'updated_at', 'deleted_at','status_verified','meeting_status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SalesAndMarketing::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC, // Sort by 'id' in descending order
                ],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'meeting_interface' => $this->meeting_interface,
            'purpose' => $this->purpose,
            'calendar_invite' => $this->calendar_invite,
            'status_verified' => $this->status_verified,
            'meeting_status' => $this->meeting_status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'deleted_at' => $this->deleted_at,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'date', $this->date])
            ->andFilterWhere(['like', 'time', $this->time])
            ->andFilterWhere(['like', 'attendees', $this->attendees])
            ->andFilterWhere(['like', 'meeting_place', $this->meeting_place])
            ->andFilterWhere(['like', 'meeting_location_pin', $this->meeting_location_pin])
            ->andFilterWhere(['like', 'client_id', $this->client_id])
            ->andFilterWhere(['like', 'status_verified', $this->status_verified])
            ->andFilterWhere(['like', 'meeting_status', $this->meeting_status]);

        return $dataProvider;
    }
}
