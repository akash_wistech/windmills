<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "crm_schedule_inspection".
 *
 * @property int $id
 * @property int $inspection_type
 * @property string|null $valuation_date
 * @property string|null $inspection_date
 * @property string|null $inspection_time
 * @property string|null $client_deadline
 * @property int|null $inspection_officer
 * @property string|null $contact_person_name
 * @property string|null $contact_email
 * @property string|null $contact_phone_no
 * @property int $quotation_id
 * @property string|null $valuation_report_date
 */
class CrmScheduleInspection extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_schedule_inspection';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'quotation_id'], 'required'],
            [[ 'inspection_officer', 'quotation_id'], 'integer'],
            [['valuation_date', 'inspection_date', 'client_deadline', 'valuation_report_date','property_index'], 'safe'],
            [['inspection_time', 'contact_person_name', 'contact_email', 'contact_phone_no'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inspection_type' => 'Inspection Type',
            'valuation_date' => 'Valuation Date',
            'inspection_date' => 'Inspection Date',
            'inspection_time' => 'Inspection Time',
            'client_deadline' => 'Client Deadline',
            'inspection_officer' => 'Inspection Officer',
            'contact_person_name' => 'Contact Person Name',
            'contact_email' => 'Contact Email',
            'contact_phone_no' => 'Contact Phone No',
            'quotation_id' => 'Qoutation ID',
            'valuation_report_date' => 'Valuation Report Date',
        ];
    }
}
