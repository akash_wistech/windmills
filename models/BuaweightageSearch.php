<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Buaweightage;

/**
 * BuaweightageSearch represents the model behind the search form of `app\models\Buaweightage`.
 */
class BuaweightageSearch extends Buaweightage
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['difference', 'bigger_sp', 'smaller_sp'], 'number'],
            [['created_at', 'updated_at', 'trashed_at','status_verified'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Buaweightage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'difference' => $this->difference,
            'bigger_sp' => $this->bigger_sp,
            'smaller_sp' => $this->smaller_sp,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => $this->trashed,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
            'status_verified' => $this->status_verified,
        ]);

        return $dataProvider;
    }
}
