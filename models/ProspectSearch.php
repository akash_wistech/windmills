<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use app\models\Prospect;

/**
* ProspectSearch represents the model behind the search form about `app\models\Prospect`.
*/
class ProspectSearch extends Prospect
{
  public $wf,$wfi,$cp_name,$country_name,$pageSize;

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id', 'country_id', 'wf', 'created_by', 'updated_by', 'trashed', 'trashed_by','pageSize'], 'integer'],
      [['firstname', 'lastname', 'company_name', 'cp_name', 'city', 'country_name', 'wfi', 'created_at', 'updated_at', 'trashed_at'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $query = $this->generateQuery($params);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
      'sort' => false,//$this->defaultSorting,
    ]);

    return $dataProvider;
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function searchMy($params)
  {
    $query = $this->generateQuery($params);

    //Check Listing type allowed
    if(Yii::$app->menuHelperFunction->getListingTypeByController('prospect')==2){
      $query->innerJoin(ModuleManager::tableName(),ModuleManager::tableName().".module_id=".Prospect::tableName().".id and ".ModuleManager::tableName().".module_type='".$this->moduleTypeId."'");
      $query->andFilterWhere([
        ModuleManager::tableName().'.staff_id' => Yii::$app->user->identity->id]);
    }

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
      'sort' => $this->defaultSorting,
    ]);

    return $dataProvider;
  }

  /**
  * Generates query for the search
  *
  * @param array $params
  *
  * @return ActiveRecord
  */
  public function generateQuery($params)
  {
    $this->load($params);

    $query = Prospect::find()
    ->select([
      Prospect::tableName().'.id',
      'cp_name'=>'CONCAT('.Prospect::tableName().'.firstname," ",'.Prospect::tableName().'.lastname)',
      'company_name',
      Prospect::tableName().'.company_id',
      Prospect::tableName().'.user_id',
      Prospect::tableName().'.created_at',
    ])
    ->asArray();

    $query->andFilterWhere([
      Prospect::tableName().'.id' => $this->id,
      Prospect::tableName().'.trashed' => 0,
    ]);

    $query
    ->andFilterWhere([
      'or',
      ['like',Prospect::tableName().'.firstname',$this->cp_name],
      ['like',Prospect::tableName().'.lastname',$this->cp_name]
    ])
    ->andFilterWhere(['like', 'company_name', $this->company_name]);

    return $query;
  }

  /**
  * Default Sorting Options
  *
  * @param array $params
  *
  * @return Array
  */
  public function getDefaultSorting()
  {
    return [
      'defaultOrder' => ['id'=>SORT_DESC],
      'attributes' => [
        'id',
        'company_name',
        'cp_name' => [
          'asc' => [Prospect::tableName().'.firstname' => SORT_ASC, Prospect::tableName().'.lastname' => SORT_ASC],
          'desc' => [Prospect::tableName().'.firstname' => SORT_DESC, Prospect::tableName().'.lastname' => SORT_DESC],
        ],
        'country_name' => [
          'asc' => [Country::tableName().'.firstname' => SORT_ASC],
          'desc' => [Country::tableName().'.firstname' => SORT_DESC],
        ],
        'created_at',
      ]
    ];
  }

  public function getProcess()
  {
    if (($model = Workflow::findOne($this->wf)) !== null) {
        return $model;
    }
    throw new NotFoundHttpException('The requested page does not exist.');
  }
}
