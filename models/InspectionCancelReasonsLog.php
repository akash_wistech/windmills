<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "inspection_cancel_reasons_log".
 *
 * @property int $id
 * @property int|null $reason_id
 * @property int|null $valuation_id
 * @property int $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class InspectionCancelReasonsLog extends ActiveRecordFull
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inspection_cancel_reasons_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reason_id'], 'required'],
            [['reason_id', 'valuation_id', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['created_at', 'updated_at', 'trashed_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reason_id' => 'Reason',
            'valuation_id' => 'Valuation ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }

    public function getReason()
    {
        return $this->hasOne(InspectionCancelReasons::className(), ['id' => 'reason_id']);
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'inspection_cancel_reasons_log';
    }
    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->valuation_id;
    }
}
