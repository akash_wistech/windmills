<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CategoryManager;


// dd($_REQUEST);
/**
 * CategoryManagerSearch represents the model behind the search form of `app\models\CategoryManager`.
 */
class CategoryManagerSearch extends CategoryManager
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_name', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CategoryManager::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        // $query->andFilterWhere([
        //     'modification' => $this->modification,
        //     'description' => $this->description,
        //     'module' => $this->module,
        //     'frequency' => $this->frequency,
        //     'priority' => $this->priority,
        //     'start_date' => $this->start_date,
        //     'end_date' => $this->end_date,
        //     'completed_date' => $this->completed_date,
        //     'meeting_date' => $this->meeting_date,
        //     'entered_by' => $this->entered_by,
        //     'status' => $this->status,
        // ]);

        $query->andFilterWhere(['like', 'category_name', $this->category_name])
        ->andFilterWhere(['like', 'status', $this->status]);;


        return $dataProvider;
    }

}
