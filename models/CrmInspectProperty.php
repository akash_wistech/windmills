<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "crm_inspect_property".
 *
 * @property int $id
 * @property string $makani_number
 * @property string $latitude
 * @property string $longitude
 * @property float|null $property_placement
 * @property int $acquisition_method
 * @property int $property_visibility
 * @property int $property_exposure
 * @property int $property_category
 * @property int $property_condition
 * @property int $property_defect
 * @property string $development_type
 * @property string $finished_status
 * @property int $developer_id
 * @property float $estimated_age
 * @property float $estimated_remaining_life
 * @property float $balcony_size
 * @property float $service_area_size
 * @property float $built_up_area
 * @property float $net_built_up_area
 * @property int $number_of_basement
 * @property int $number_of_levels
 * @property string $pool
 * @property string $other_facilities
 * @property float|null $completion_status
 * @property string $occupancy_status
 * @property int $no_of_bedrooms
 * @property int $no_of_bathrooms
 * @property int $full_building_floors
 * @property int $parking_floors
 * @property string|null $view_community
 * @property string|null $view_pool
 * @property string|null $view_burj
 * @property string|null $view_sea
 * @property string|null $view_marina
 * @property string|null $view_mountains
 * @property string|null $view_lake
 * @property string|null $view_golf_course
 * @property string|null $view_park
 * @property string|null $view_special
 * @property string|null $location_highway_drive
 * @property string|null $location_school_drive
 * @property string|null $location_mall_drive
 * @property string|null $location_sea_drive
 * @property string|null $location_park_drive
 * @property string $landscaping
 * @property string|null $fridge
 * @property string|null $listing_property_type
 * @property string|null $oven
 * @property string|null $cooker
 * @property string|null $furnished
 * @property string|null $ac_type
 * @property string|null $washing_machine
 * @property string|null $white_goods
 * @property string $utilities_connected
 * @property int|null $no_of_kitchen
 * @property int $no_of_living_area
 * @property int $no_of_dining_area
 * @property int $no_of_maid_rooms
 * @property int $no_of_laundry_area
 * @property int $no_of_store
 * @property int $no_of_service_block
 * @property int $no_of_garage
 * @property int $no_of_balcony
 * @property int|null $mode_of_transport
 * @property float|null $start_kilometres
 * @property float|null $end_kilometres
 * @property int|null $plot_area_source
 * @property int|null $bua_source
 * @property float|null $measurement
 * @property float|null $extension
 * @property int|null $extension_source
 * @property int|null $plot_source
 * @property int|null $parking_source
 * @property int|null $age_source
 * @property int|null $no_of_family_room
 * @property int|null $no_of_powder_room
 * @property int|null $no_of_study_room
 * @property int $quotation_id
 * @property int $status
 * @property int|null $email_status
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 * @property int|null $updated_by
 */
class CrmInspectProperty extends \yii\db\ActiveRecord
{
    public $listing_property_type_options;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_inspect_property';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['makani_number', 'latitude', 'longitude', 'acquisition_method', 'property_visibility', 'property_exposure', 'property_category', 'property_condition', 'property_defect', 'development_type', 'finished_status', 'developer_id', 'estimated_age', 'estimated_remaining_life', 'balcony_size', 'service_area_size', 'built_up_area', 'net_built_up_area', 'number_of_basement', 'number_of_levels', 'pool', 'occupancy_status', 'no_of_bedrooms', 'no_of_bathrooms', 'full_building_floors', 'parking_floors', 'landscaping', 'utilities_connected'], 'required'],
            [['property_placement', 'estimated_age', 'estimated_remaining_life', 'balcony_size', 'service_area_size', 'built_up_area', 'net_built_up_area', 'completion_status', 'start_kilometres', 'end_kilometres', 'measurement', 'extension'], 'number'],
            [['acquisition_method', 'property_visibility', 'property_category', 'property_condition', 'property_defect', 'developer_id', 'number_of_basement', 'number_of_levels', 'no_of_bedrooms', 'no_of_bathrooms', 'full_building_floors', 'parking_floors', 'no_of_kitchen', 'no_of_living_area', 'no_of_dining_area', 'no_of_maid_rooms', 'no_of_laundry_area', 'no_of_store', 'no_of_service_block', 'no_of_garage', 'no_of_balcony', 'mode_of_transport', 'plot_area_source', 'bua_source', 'extension_source', 'plot_source', 'parking_source', 'age_source', 'no_of_family_room', 'no_of_powder_room', 'no_of_study_room', 'quotation_id','property_index'], 'integer'],
            [['development_type', 'finished_status', 'pool', 'occupancy_status', 'location_sea_drive', 'landscaping', 'fridge', 'oven', 'cooker', 'furnished', 'washing_machine', 'white_goods', 'utilities_connected'], 'string'],
            [['other_facilities', 'ac_type', 'property_exposure'], 'safe'],
            [['makani_number', 'latitude', 'longitude', 'view_community', 'view_pool', 'view_burj', 'view_sea', 'view_marina', 'view_mountains', 'view_lake', 'view_golf_course', 'view_park', 'view_special', 'location_highway_drive', 'location_school_drive', 'location_mall_drive', 'location_park_drive', 'listing_property_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'makani_number' => 'Makani Number',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'property_placement' => 'Property Placement',
            'acquisition_method' => 'Acquisition Method',
            'property_visibility' => 'Property Visibility',
            'property_exposure' => 'Property Exposure',
            'property_category' => 'Property Category',
            'property_condition' => 'Property Condition',
            'property_defect' => 'Property Defect',
            'development_type' => 'Development Type',
            'finished_status' => 'Finished Status',
            'developer_id' => 'Developer ID',
            'estimated_age' => 'Estimated Age',
            'estimated_remaining_life' => 'Estimated Remaining Life',
            'balcony_size' => 'Balcony Size',
            'service_area_size' => 'Service Area Size',
            'built_up_area' => 'Built Up Area',
            'net_built_up_area' => 'Net Built Up Area',
            'number_of_basement' => 'Number Of Basement',
            'number_of_levels' => 'Number Of Levels',
            'pool' => 'Pool',
            'other_facilities' => 'Other Facilities',
            'completion_status' => 'Completion Status',
            'occupancy_status' => 'Occupancy Status',
            'no_of_bedrooms' => 'No Of Bedrooms',
            'no_of_bathrooms' => 'No Of Bathrooms',
            'full_building_floors' => 'Full Building Floors',
            'parking_floors' => 'Parking Floors',
            'view_community' => 'View Community',
            'view_pool' => 'View Pool',
            'view_burj' => 'View Burj',
            'view_sea' => 'View Sea',
            'view_marina' => 'View Marina',
            'view_mountains' => 'View Mountains',
            'view_lake' => 'View Lake',
            'view_golf_course' => 'View Golf Course',
            'view_park' => 'View Park',
            'view_special' => 'View Special',
            'location_highway_drive' => 'Location Highway Drive',
            'location_school_drive' => 'Location School Drive',
            'location_mall_drive' => 'Location Mall Drive',
            'location_sea_drive' => 'Location Sea Drive',
            'location_park_drive' => 'Location Park Drive',
            'landscaping' => 'Landscaping',
            'fridge' => 'Fridge',
            'listing_property_type' => 'Listing Property Type',
            'oven' => 'Oven',
            'cooker' => 'Cooker',
            'furnished' => 'Furnished',
            'ac_type' => 'Ac Type',
            'washing_machine' => 'Washing Machine',
            'white_goods' => 'White Goods',
            'utilities_connected' => 'Utilities Connected',
            'no_of_kitchen' => 'No Of Kitchen',
            'no_of_living_area' => 'No Of Living Area',
            'no_of_dining_area' => 'No Of Dining Area',
            'no_of_maid_rooms' => 'No Of Maid Rooms',
            'no_of_laundry_area' => 'No Of Laundry Area',
            'no_of_store' => 'No Of Store',
            'no_of_service_block' => 'No Of Service Block',
            'no_of_garage' => 'No Of Garage',
            'no_of_balcony' => 'No Of Balcony',
            'mode_of_transport' => 'Mode Of Transport',
            'start_kilometres' => 'Start Kilometres',
            'end_kilometres' => 'End Kilometres',
            'plot_area_source' => 'Plot Area Source',
            'bua_source' => 'Bua Source',
            'measurement' => 'Measurement',
            'extension' => 'Extension',
            'extension_source' => 'Extension Source',
            'plot_source' => 'Plot Source',
            'parking_source' => 'Parking Source',
            'age_source' => 'Age Source',
            'no_of_family_room' => 'No Of Family Room',
            'no_of_powder_room' => 'No Of Powder Room',
            'no_of_study_room' => 'No Of Study Room',
            'quotation_id' => 'Valuation ID',
            'status' => 'Status',
            'email_status' => 'Email Status',
        ];
    }
    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->id;
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Quotation Properties';
    }
    public function beforeSave($insert)
    {

        $valuation = CrmReceivedProperties::find()->where(['quotation_id' => $this->quotation_id])->one();
        /* $this->estimated_age = $valuation->building->estimated_age;
         $this->estimated_remaining_life = $valuation->property->age - $valuation->building->estimated_age;*/
        $this->estimated_remaining_life = $valuation->property->age - $this->estimated_age;
        if (!empty($this->other_facilities)) {
            if(is_array($this->other_facilities)) {
                $this->other_facilities = implode(",", $this->other_facilities);
            }
        }

        if (!empty($this->ac_type)) {
            if(is_array($this->ac_type)) {
                $this->ac_type = implode(",", $this->ac_type);
            }
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }
}
