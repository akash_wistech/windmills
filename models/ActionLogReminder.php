<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%action_log_reminder}}".
*
* @property integer $action_log_id
* @property string $notify_to
* @property integer $notify_time
*/
class ActionLogReminder extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%action_log_reminder}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['action_log_id', 'notify_to', 'notify_time'], 'required'],
      [['action_log_id', 'notify_time'], 'integer'],
      [['notify_to'], 'string'],
    ];
  }

  public static function primaryKey()
  {
  	return ['action_log_id','notify_to','notify_time'];
  }
}
