<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "configuration_files".
 *
 * @property int $id
 * @property string $type
 * @property int|null $floor
 * @property int|null $upgrade
 * @property string|null $attachment
 * @property int $index_id
 * @property int $valuation_id
 * @property int $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class ConfigurationFiles extends ActiveRecordFull
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'configuration_files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'index_id', 'valuation_id'], 'required'],
            [['floor', 'upgrade', 'index_id', 'valuation_id', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by','flooring','ceilings','speciality'], 'integer'],
            [['created_at', 'updated_at', 'trashed_at','checked_image','custom_field_id','field_name','rep_image'], 'safe'],
            [['type', 'attachment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'floor' => 'Floor',
            'upgrade' => 'Upgrade',
            'attachment' => 'Attachment',
            'index_id' => 'Index ID',
            'valuation_id' => 'Valuation ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }
    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Valuation Configuration Files';
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->id;
    }
}
