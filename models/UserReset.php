<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordTsBa;

/**
 * This is the model class for table "{{%user_reset}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $password_reset_token
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class UserReset extends ActiveRecordTsBa
{

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return '{{%user_reset}}';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['user_id','password_reset_token'], 'required'],
      [['user_id','status','created_by','updated_by'], 'integer'],
      [['password_reset_token'], 'string'],
      [['created_at','updated_at'], 'safe'],
      [['status'],'default','value'=>0]
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_id' => Yii::t('app', 'User'),
      'password_reset_token' => Yii::t('app', 'Reset Token'),
      'status' => Yii::t('app', 'Status'),
    ];
  }

  /**
   * Removes password reset token
   */
  public function removePasswordResetToken()
  {
    $this->status = 1;
    $this->save();
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUser()
  {
      return $this->hasOne(User::className(), ['id' => 'user_id']);
  }
}
