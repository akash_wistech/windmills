<?php

namespace app\models;

use yii\db\ActiveRecord;
use app\components\models\ActiveRecordFull;


use Yii;

/**
 * This is the model class for table "holidays".
 *
 * @property int $id
 * @property int $quotation_id
 * @property float|null $recommended_fee
 * @property float|null $approved_fee
 * @property float|null $fee_differ_amount
 * @property float|null $fee_differ_percent
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property string|null $deleted_at
 * @property int|null $deleted_by

 */
class FeeDifference extends ActiveRecordFull
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fee_difference';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quotation_id','recommended_fee','approved_fee'], 'safe'],
            [['valuation_id'], 'safe'],
            [['fee_differ_amount','fee_differ_percent'], 'safe'],
            [['type','created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at','deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'quotation_id' => 'Quotation ID',
            'valuation_id' => 'Valuation ID',
            'recommended_fee' => 'Recommended Fee',
            'approved_fee' => 'Approved Fee',
            'fee_differ_amount' => 'Differece Amount',
            'fee_differ_percent' => 'Difference Percent',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
        ];
    }


    public function getQuotation()
    {
        return $this->hasOne(CrmQuotations::className(), ['id' => 'quotation_id']);
    }

    public function getValuation()
    {
        return $this->hasOne(Valuation::className(), ['id' => 'valuation_id']);
    }
}
