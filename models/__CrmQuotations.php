<?php

namespace app\models;

use app\modules\wisnotify\listners\NotifyEvent;
use Yii;
use app\components\models\ActiveRecordFull;
use yii2tech\ar\softdelete\SoftDeleteBehavior;

use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
* This is the model class for table "crm_quotations".
*
* @property int $id
* @property string|null $reference_number
* @property string|null $client_name
* @property string|null $client_customer_name
* @property string|null $scope_of_service
* @property string|null $purpose_of_valuation
* @property string|null $advance_payment_terms
* @property int|null $no_of_properties
* @property string|null $related_to_buyer
* @property string|null $related_to_owner
* @property string|null $related_to_client
* @property string|null $related_to_property
* @property string|null $related_to_buyer_reason
* @property string|null $related_to_owner_reason
* @property string|null $related_to_client_reason
* @property string|null $related_to_property_reason
* @property string|null $name
* @property string|null $phone_number
* @property string|null $email
* @property float|null $recommended_fee
* @property string|null $relative_discount
* @property float|null $final_fee_approved
* @property string|null $turn_around_time
* @property string|null $valuer_name
* @property string|null $date
* @property string|null $quotation_status
* @property string|null $payment_slip
* @property string|null $toe_document
* @property string|null $quotation_recommended_fee
* @property string|null $quotation_turn_around_time
* @property string|null $toe_final_fee
* @property string|null $toe_final_turned_around_time
* @property int $status
* @property string|null $assumptions
* @property string|null $created_at
* @property string|null $updated_at
* @property string|null $deleted_at
* @property int|null $created_by
* @property int|null $updated_by
* @property int|null $deleted_by
*/
class CrmQuotations extends ActiveRecordFull
{
    public $other;
    public $calculations;
    public $other_intended_users_check;
    public $total_records,$total_amount; // for today widgets dashboard
    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'crm_quotations';
    }


    // public function behaviors()
    // {
    //     return [
    //         'softDeleteBehavior' => [
    //             'class' => SoftDeleteBehavior::className(),
    //             'softDeleteAttributeValues' => [
    //                 'deleted_at' => true
    //             ],
    //         ],

    //         'blameable' => [
    //           'class' => BlameableBehavior::className(),
    //           'createdByAttribute' => 'created_by',
    //           'updatedByAttribute' => 'updated_by',
    //         ],

    //         // Other behaviors
    //         [
    //             'class' => TimestampBehavior::className(),
    //             'createdAtAttribute' => 'created_at',
    //             'updatedAtAttribute' => false,
    //             'value' => new Expression('NOW()'),
    //         ],
    //     ];
    // }

    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['no_of_properties', 'status', 'created_by', 'updated_by', 'deleted_by','other_instructing_person','branch_id'], 'integer'],
            [['reference_number'], 'unique'],
            [['related_to_buyer_reason', 'related_to_owner_reason', 'related_to_client_reason', 'related_to_property_reason'], 'string'],
            [['recommended_fee', 'final_fee_approved','quotation_turn_around_time', 'quotation_recommended_fee', 'toe_final_fee', 'toe_final_turned_around_time', 'turn_around_time'], 'number'],
            [['created_at', 'updated_at', 'deleted_at','client_reference','quotation_turn_around_time', 'quotation_recommended_fee', 'toe_final_fee', 'toe_final_turned_around_time', 'turn_around_time','toe_image','payment_image', 'quotation_status','calculations','status_approve','status_approve_toe','relative_discount_toe','other_intended_users', 'assumptions','payment_status','payment_image_2','first_half_payment','second_half_payment','same_building_discount','final_quoted_fee'], 'safe'],
            [['reference_number', 'client_name', 'client_customer_name', 'scope_of_service', 'advance_payment_terms', 'related_to_buyer', 'related_to_owner', 'related_to_client', 'related_to_property', 'name', 'phone_number', 'email', 'valuer_name', 'payment_slip', 'toe_document'], 'string', 'max' => 255],
            [[ 'relative_discount', 'date','quotation_final_fee'], 'string', 'max' => 100],

            [[
                'inquiry_received_date',
                'quotation_sent_date',
                'toe_sent_date',
                'payment_received_date',
                'toe_signed_and_received_date',
                'toe_signed_and_received',
                'on_hold_date',
                'approved_date',
                'quotation_rejected_date',
                'toe_rejected_date',
                'cancelled_date',
                'regretted_date',
                'grand_final_toe_fee',
                'tat_requirements',
                'inspection_type',
                'no_of_units_same_building'
                ,'number_of_rooms_building','restaurant','ballrooms','atms','retails_units','night_clubs','bars','health_club','meeting_rooms','spa','beach_access','parking_sale',
                'email_subject','email_status_rcv','email_status_docs','email_status_q_sent','email_status_q_approved','email_status_t_sent','email_status_t_approved',
                'email_status_pr','email_status_q_hold','email_status_t_rejected','purpose_of_valuation','final_quoted_fee'], 'safe'],
            [[ 'special_assumptions' ,'instruction_date', 'target_date','client_invoice_type','quotation_cancel_reason','instructing_party_position','instructing_party_company',
                'instructing_party_phone','instructing_party_email','quotation_accepted_date','toe_approved_date'], 'safe'],
            
            [[ 'client_name','reference_number','client_customer_name','instruction_date', 'target_date','inquiry_received_time','inquiry_received_day', 'email_subject'], 'required', 'on' => 'step_0_1'],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['step_0_1'] = ['client_name','reference_number','client_customer_name','instruction_date', 'target_date','inquiry_received_time', 'email_subject']; // Fields required for step 1
        return $scenarios;
    }

    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reference_number' => 'Reference Number',
            'client_name' => 'Client Name',
            'client_type' => 'Client Type',
            'client_customer_name' => 'Client Customer Name',
            'other_intended_users' => 'Other Intended Users',
            'scope_of_service' => 'Scope Of Service',
            'inquiry_date' => 'Inquiry Date',
            'expiry_date' => 'Expiry Date',
            'purpose_of_valuation' => 'Purpose Of Valuation',
            'advance_payment_terms' => 'Advance Payment Terms',
            'no_of_properties' => 'No Of Properties',
            'related_to_buyer' => 'Related To Buyer',
            'related_to_owner' => 'Related To Owner',
            'related_to_client' => 'Related To Client',
            'related_to_property' => 'Related To Property',
            'related_to_buyer_reason' => 'Related To Buyer Reason',
            'related_to_owner_reason' => 'Related To Owner Reason',
            'related_to_client_reason' => 'Related To Client Reason',
            'related_to_property_reason' => 'Related To Property Reason',
            'name' => 'Name',
            'phone_number' => 'Phone Number',
            'email' => 'Email',
            'recommended_fee' => 'Recommended Fee',
            'relative_discount' => 'Relative Discount',
            'final_fee_approved' => 'Final Fee Approved',
            'turn_around_time' => 'Turn Around Time',
            'valuer_name' => 'Valuer Name',
            'date' => 'Date',
            'quotation_status' => 'Quotation Status',
            'payment_slip' => 'Payment Slip',
            'toe_document' => 'Toe Document',
            'quotation_recommended_fee' => 'Quotation Recommended Fee',
            'quotation_turn_around_time' => 'Quotation Turn Around Time',
            'toe_final_fee' => 'Toe Final Fee',
            'toe_final_turned_around_time' => 'Toe Final Turned Around Time',
            'status' => 'Status',
            'assumptions' => 'Assumptions',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'deleted_by' => 'Deleted By',
            'branch_id' => 'Branch',
            'assumptions' => 'General Assumptions',
            'payment_status' => 'Payment Status',
            'special_assumptions' => 'Special Assumptions',
            'inquiry_received_day' => 'Inquiry Received Day',
        ];
    }
    public function beforeSave($insert)
    {

        if($this->status_approve <> null){

        }else{
            unset($this->status_approve);
        }
        if($this->status_approve_toe <> null){

        }else{
            unset($this->status_approve_toe);
        }

        if(!empty($this->assumptions) and is_array($this->assumptions)){
            $this->assumptions =  implode(",",$this->assumptions);
        }
        if(!empty($this->special_assumptions) and is_array($this->special_assumptions)){
            $this->special_assumptions =  implode(",",$this->special_assumptions);
        }


        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {

        /* if($this->no_of_properties!=null){
            $properties_length = $this->no_of_properties;
            //  CustomFieldModule::deleteAll(['and',['custom_field_id'=>$this->id],['not in','module_type',$this->for_modules]]);

            for($i=0; $i<$properties_length; $i++){
                $property= new CrmProperties();
                $property->quotation_id = $this->id;
                $property->property_index = $i;
                $property->save();
            }
        }*/
        if($this->calculations!=null && !empty($this->calculations)){
            // echo "<pre>"; print_r($this->calculations); echo "</pre>"; die();

            foreach ($this->calculations as $key => $calculation){

                if(($this->quotation_status >=0 && $this->quotation_status < 1) || ($this->quotation_status == 9)) {

                    Yii::$app->db->createCommand()->update('crm_received_properties', ['quotation_fee' => $calculation['quotation_fee'], 'tat' => $calculation['tat'], 'toe_fee' => $calculation['quotation_fee'], 'toe_tat' => $calculation['tat']], 'id=' . $key . '')->execute();
                }
                if($this->quotation_status >1 && $this->quotation_status <= 2  || ($this->quotation_status == 9)) {
                    if (isset($calculation['toe_fee']) && $calculation['toe_fee'] <> null) {
                        Yii::$app->db->createCommand()->update('crm_received_properties', ['toe_fee' => $calculation['toe_fee'], 'toe_tat' => $calculation['toe_tat']], 'id=' . $key . '')->execute();
                    }
                }
            }
          //  die('here');

        }
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub

    }

    /**
    * returns main title/name of row for status and delete loging
    */
    public function getRecTitle()
    {
        return $this->id;
    }

    /**
    * returns main title/name of row for status and delete loging
    */
    public function getRecType()
    {
        return 'Quotations';
    }

    public function getClient()
    {
        return $this->hasOne(Company::className(), ['id' => 'client_name']);
    }

    public function getChildData()
    {
        return $this->hasMany(ValuationQuotationPropertiesDetails::className(), ['valuation_quotation_id' => 'id']);
    }

    public function getBuilding()
    {
        return $this->hasOne(Buildings::className(), ['id' => 'building']);
    }

    public function getProperty()
    {
        return $this->hasOne(Properties::className(), ['id' => 'property_id']);
    }
    
    public function getZeroIndexProperty()
    {
        return $this->hasOne(CrmReceivedProperties::className(), ['quotation_id' => 'id'])->where(['property_index'=>0]);
    }


    public function getUserData()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    
    
    public function QuotationPdf($condition)
    {
        require_once( __DIR__ .'/../components/tcpdf/QuotationPdf.php');
        // create new PDF document
        $pdf = new \QuotationPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->model = $this;

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle($this->reference_number);
        $pdf->SetSubject('Quotation');
        $pdf->SetKeywords($this->reference_number);

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(10, 35, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('times', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P','A4');



        $qpdf=Yii::$app->controller->renderPartial('/crm-quotations/quotation-new',['id'=>$this->id, 'model' =>$this]);


        if ($condition==true) {
            $pdf->writeHTML($qpdf, true, false, false, false, '');
            $pdf->Output('Quotation: '.$this->reference_number, 'I');
            exit();

        }

        if ($condition==false) {
            $pdfFile = $this->reference_number.'.pdf';
            $fullPath = Yii::$app->params['quote_uploads_abs_path'].'/'.$pdfFile;
            $pdf->writeHTML($qpdf, true, false, false, false, '');
            $pdf->Output($fullPath, 'F');
            $attachments=[$fullPath];
            // $template = NotificationTemplate::findOne(1);
            $notifyData = [
                'client' => $this->client,
                'attachments' => $attachments,
                'subject' => $this->email_subject,
                'uid' => 'crm'.$this->id,
                'replacements'=>[
                    '{clientName}'=>$this->client->title,
                    // '{reference_number}'=>$this->reference_number
                ],
            ];
            NotifyEvent::fire23('quotation.send', $notifyData);


        }
    }




    public function ToePdf($condition)
    {
        // echo "toe model function";die();
        require_once( __DIR__ .'/../components/tcpdf/ProposalPdf.php');
        // create new PDF document
        $pdf = new \ProposalPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->model = $this;

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle($this->reference_number);
        $pdf->SetSubject('Quotation');
        $pdf->SetKeywords($this->reference_number);

        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        $pdf->SetMargins(10, 35, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        $pdf->setFontSubsetting(true);

        $pdf->SetFont('times', '', 14, '', true);

        $pdf->AddPage('P','A4');

        $qpdf=Yii::$app->controller->renderPartial('/crm-quotations/toe-new',['id'=>$this->id, 'model' => $this]);

        if ($condition==true) {
            $pdf->writeHTML($qpdf, true, false, false, false, '');
            $pdf->Output('TOE '.$this->reference_number, 'I');
            exit();
        }

        if ($condition==false) {



            $invoiceFullPath = $this->GetInvoice($this->id, $condition=false);

            $pdfFile = $this->reference_number.'.pdf';
            $fullPath = Yii::$app->params['toe_uploads_abs_path'].'/'.$pdfFile;
            $pdf->writeHTML($qpdf, true, false, false, false, '');
            $pdf->Output($fullPath, 'F');
            // $attachments=[$fullPath];
            $attachments=[$fullPath, $invoiceFullPath];

            $notifyData = [
                'client' => $this->client,
                'attachments' => $attachments,
                'subject' => $this->email_subject,
                'uid' => 'crm'.$this->id,
                'replacements'=>[
                    '{clientName}'=>$this->client->title,
                    // '{reference_number}'=>$this->reference_number
                ],
            ];

            NotifyEvent::fire23('toe.send', $notifyData);

        }
    }


    public function GetInvoice($id='', $condition)
    {
        $model = CrmQuotations::findOne($id);

        require_once( __DIR__ .'/../components/tcpdf/ProformaInvoice.php');
        // create new PDF document
        $pdf = new \ProformaInvoice(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle($this->reference_number);
        $pdf->SetSubject('Invoice');
        $pdf->SetKeywords($this->reference_number);

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(10, 35, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('times', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P','A4');

        $qpdf=Yii::$app->controller->renderPartial('/crm-quotations/qpdf',[
            'id'=>$id,
            'model'=>$model,
        ]);


        if ($condition==false) {
            $pdfFile = "Invoice-".$this->reference_number.'.pdf';
            $fullPath = Yii::$app->params['invoice_abs_path'].'/'.$pdfFile;
            $pdf->writeHTML($qpdf, true, false, false, false, '');
            $pdf->Output($fullPath, 'F');
            return $fullPath;
        }else{
            $pdf->writeHTML($qpdf, true, false, false, false, '');
            $pdf->Output('Invoice-'.$this->reference_number, 'I');
        }



    }


    public function upload()
    {

        $uploadObject = Yii::$app->get('s3bucket')->upload('toe/images/'. $this->toe_image->baseName . '.' . $this->toe_image->extension,$this->toe_image->tempName);
        if ($uploadObject) {
            // check if CDN host is available then upload and get cdn URL.
            if (Yii::$app->get('s3bucket')->cdnHostname) {
                $url = Yii::$app->get('s3bucket')->getCdnUrl($this->toe_image );
            } else {
                $url = Yii::$app->get('s3bucket')->getUrl('toe/images/'.$this->toe_image );
            }
        }
        return true;
    }

    public function uploadpayment()
    {
        $uploadObject = Yii::$app->get('s3bucket')->upload('payments/images/'. $this->payment_image->baseName . '.' . $this->payment_image->extension,$this->payment_image->tempName);
        if ($uploadObject) {
            // check if CDN host is available then upload and get cdn URL.
            if (Yii::$app->get('s3bucket')->cdnHostname) {
                $url = Yii::$app->get('s3bucket')->getCdnUrl($this->payment_image );
            } else {
                $url = Yii::$app->get('s3bucket')->getUrl('payments/images/'.$this->payment_image );
            }
        }
        return true;
    }

    public function uploadpayment2()
    {
        $uploadObject = Yii::$app->get('s3bucket')->upload('payments/images/'. $this->payment_image_2->baseName . '.' . $this->payment_image_2->extension,$this->payment_image_2->tempName);
        if ($uploadObject) {
            // check if CDN host is available then upload and get cdn URL.
            if (Yii::$app->get('s3bucket')->cdnHostname) {
                $url = Yii::$app->get('s3bucket')->getCdnUrl($this->payment_image_2 );
            } else {
                $url = Yii::$app->get('s3bucket')->getUrl('payments/images/'.$this->payment_image_2 );
            }
        }
        return true;
    }


}
