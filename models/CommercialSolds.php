<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "commercial_solds".
 *
 * @property int $id
 * @property string|null $transaction_type
 * @property string|null $subtype
 * @property string|null $sales_sequence
 * @property string|null $reidin_ref
 * @property string|null $transaction_date
 * @property string|null $community
 * @property string|null $property
 * @property string|null $property_type
 * @property string|null $unit
 * @property string|null $bedrooms
 * @property string|null $floor
 * @property string|null $parking
 * @property string|null $balcony_area
 * @property string|null $size_sqf
 * @property string|null $land_size
 * @property string|null $amount
 * @property string|null $sqf
 * @property string|null $developer
 * @property int|null $status
 */
class CommercialSolds extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'commercial_solds';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'status'], 'integer'],
            [['transaction_date'], 'safe'],
            [['transaction_type', 'unit'], 'string', 'max' => 16],
            [['subtype'], 'string', 'max' => 41],
            [['sales_sequence'], 'string', 'max' => 9],
            [['reidin_ref'], 'string', 'max' => 14],
            [['community'], 'string', 'max' => 33],
            [['property'], 'string', 'max' => 64],
            [['property_type'], 'string', 'max' => 21],
            [['bedrooms'], 'string', 'max' => 5],
            [['floor'], 'string', 'max' => 3],
            [['parking'], 'string', 'max' => 902],
            [['balcony_area'], 'string', 'max' => 6],
            [['size_sqf', 'land_size', 'sqf'], 'string', 'max' => 7],
            [['amount'], 'string', 'max' => 11],
            [['developer'], 'string', 'max' => 59],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'transaction_type' => 'Transaction Type',
            'subtype' => 'Subtype',
            'sales_sequence' => 'Sales Sequence',
            'reidin_ref' => 'Reidin Ref',
            'transaction_date' => 'Transaction Date',
            'community' => 'Community',
            'property' => 'Property',
            'property_type' => 'Property Type',
            'unit' => 'Unit',
            'bedrooms' => 'Bedrooms',
            'floor' => 'Floor',
            'parking' => 'Parking',
            'balcony_area' => 'Balcony Area',
            'size_sqf' => 'Size Sqf',
            'land_size' => 'Land Size',
            'amount' => 'Amount',
            'sqf' => 'Sqf',
            'developer' => 'Developer',
            'status' => 'Status',
        ];
    }
}
