<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sold_off_plan".
 *
 * @property int $id
 * @property string|null $transaction_type
 * @property string|null $subtype
 * @property string|null $sales_sequence
 * @property string|null $red_number
 * @property string|null $transanction_date
 * @property string|null $community
 * @property string|null $property
 * @property string|null $property_Type
 * @property string|null $unit_number
 * @property string|null $bedrooms
 * @property string|null $floor
 * @property string|null $parking
 * @property string|null $balcony_area
 * @property string|null $size_sqf
 * @property string|null $land_size
 * @property string|null $price
 * @property string|null $price_per_sqf
 * @property string|null $developer
 */
class SoldOffPlan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sold_off_plan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['transaction_type'], 'string', 'max' => 16],
            [['subtype'], 'string', 'max' => 41],
            [['sales_sequence', 'bedrooms'], 'string', 'max' => 9],
            [['red_number'], 'string', 'max' => 14],
            [['transanction_date'], 'string', 'max' => 10],
            [['community'], 'string', 'max' => 30],
            [['property'], 'string', 'max' => 49],
            [['property_Type'], 'string', 'max' => 24],
            [['unit_number'], 'string', 'max' => 23],
            [['floor'], 'string', 'max' => 2],
            [['parking'], 'string', 'max' => 64],
            [['balcony_area', 'land_size'], 'string', 'max' => 6],
            [['size_sqf'], 'string', 'max' => 7],
            [['price'], 'string', 'max' => 11],
            [['price_per_sqf'], 'string', 'max' => 8],
            [['developer'], 'string', 'max' => 72],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'transaction_type' => 'Transaction Type',
            'subtype' => 'Subtype',
            'sales_sequence' => 'Sales Sequence',
            'red_number' => 'Red Number',
            'transanction_date' => 'Transanction Date',
            'community' => 'Community',
            'property' => 'Property',
            'property_Type' => 'Property Type',
            'unit_number' => 'Unit Number',
            'bedrooms' => 'Bedrooms',
            'floor' => 'Floor',
            'parking' => 'Parking',
            'balcony_area' => 'Balcony Area',
            'size_sqf' => 'Size Sqf',
            'land_size' => 'Land Size',
            'price' => 'Price',
            'price_per_sqf' => 'Price Per Sqf',
            'developer' => 'Developer',
        ];
    }
}
