<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
* This is the model class for table "sales_and_marketing".
*
* @property int $id
* @property string|null $date
* @property string|null $time
* @property int|null $meeting_interface
* @property int|null $purpose
* @property int|null $calendar_invite
* @property string|null $attendees
* @property string|null $meeting_place
* @property string|null $meeting_location_pin
* @property string|null $created_at
* @property int|null $created_by
* @property string|null $updated_at
* @property int|null $updated_by
* @property string|null $deleted_at
* @property int|null $deleted_by
*/
class SalesAndMarketing extends ActiveRecordFull
{
    public $attendees;
    public $searchInput;
    public $location;
    public $lat;
    public $lng;
    public $client_attendee;
    public $wm_attendee;
    public $km_images,$parking_images;

    
    public static function tableName()
    {
        return 'sales_and_marketing';
    }
    

    public function rules()
    {
        return [
            // [['client_id','date','time','purpose','meeting_end_time','meeting_interface'], 'required'],
            [['created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at','date', 'time', 'meeting_place',
            'meeting_location_pin', 'attendees','client_id', 'meeting_interface', 'purpose',
            'calendar_invite','email_status','latitude', 'longitude', 'location', 'lat', 'lng',
            'meeting_end_date', 'meeting_end_time','description','location_url','status_verified',
            'status_verified_at','status_verified_by','client_attendee','wm_attendee','meeting_status',
            'rejected_reason','postpone_reason',
            'services_required','frequency_of_service_required','others_firms_client_have_worked_with',
            'send_corporate_profile','send_fee_tariff','arrange_recent_market_research_presentation',
            'send_market_research_reports_newsletter','arrange_nda','sign_client_nda','arrange_sla',
            'sign_client_sla','client_priority','number_of_total_properties_client_owns','senisitivity',
            'good_bad_client','date_of_birth_incorporation','wedding_date','is_client_happy',
            'was_our_last_valuation_amount_acceptable','is_fee_acceptable','is_tat_acceptable',
            'arrange_meeting_for_ceo','arrange_meeting_for_chairman','end_km','starting_km',
            'schedule_next_follow_up_by_call_required','schedule_next_follow_up_by_meeting',
            'procure_through_portal','deliver_business_cards_to_crm_team','parking_fee',
            'attach_km_photos','attach_parking_ticket','parking_images','km_images','feedback_email_status',
            'feedback_email_value','is_feedback_email_send'], 'safe'],
            
        ];
    }
    
    public function getSmAttendees()
    {
        return $this->hasMany(SalesAndMarketingAttendees::class, ['sales_and_marketing_id' => 'id'])->select('sales_and_marketing_attendees_id');
    }

    public function getClientMainAttendee()
    {
        return $this->hasOne(SalesAndMarketingAttendees::class, ['sales_and_marketing_id' => 'id'])->where(['client_attendee_type'=>1,'organization'=>'client']);
    }

    public function getClientOtherAttendees()
    {
        return $this->hasMany(SalesAndMarketingAttendees::class, ['sales_and_marketing_id' => 'id'])->where(['client_attendee_type'=>0,'organization'=>'client']);
    }

    public function getClientAllAttendees()
    {
        return $this->hasMany(SalesAndMarketingAttendees::class, ['sales_and_marketing_id' => 'id'])->where(['organization'=>'client']);
    }


    public function getWmAttendees()
    {
        return $this->hasMany(SalesAndMarketingAttendees::class, ['sales_and_marketing_id' => 'id'])->where(['organization'=>'windmills']);
    }


    public function getWmMainAttendee()
    {
        return $this->hasOne(SalesAndMarketingAttendees::class, ['sales_and_marketing_id' => 'id'])->where(['client_attendee_type'=>1,'organization'=>'windmills']);
    }

    public function getWmOtherAttendees()
    {
        return $this->hasMany(SalesAndMarketingAttendees::class, ['sales_and_marketing_id' => 'id'])->where(['client_attendee_type'=>0,'organization'=>'windmills']);
    }
    
    public function getClient()
    {
        return $this->hasOne(Company::class, ['id' => 'client_id']);
    }

    public function getScheduler()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }
    

    public function getKmImages()
    {
        return $this->hasMany(MeetingImages::class, ['meeting_id' => 'id'])->where(['image_type'=>'km_image']);
    }

    public function getParkingImages()
    {
        return $this->hasMany(MeetingImages::class, ['meeting_id' => 'id'])->where(['image_type'=>'parking_image']);
    }

    public function beforeSave($insert)
    {
                    
        $this->time = $this->time.':00';

        $this->meeting_end_date = $this->date;
        $this->meeting_end_time = $this->meeting_end_time.':00';

        $this->latitude = $this->lat;
        $this->longitude = $this->lng;

        

        // verify status reset after edit only something changed
        $isChanged = false;
        $ignoredAttributes = ['updated_at','status_verified_at']; 
        foreach ($this->attributes as $attribute => $value) {
            if (in_array($attribute, $ignoredAttributes)) {
                continue;
            }
            if ($this->getOldAttribute($attribute) != $value) {
                $isChanged = true;
                break;
            }
        }
        if ($isChanged) {
            if($this->status_verified == 1 && $this->getOldAttribute('status_verified') == 1){
                $this->status_verified = 2;
            }elseif ($this->status_verified == 2 && $this->getOldAttribute('status_verified') == 1) {
                $this->status_verified = 1;
            }
        }

        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }
    
    
    
    public function afterSave($insert, $changedAttributes)
    {
        // if(isset($this->attendees) && ($this->attendees <> null)) 
        // {
        //     SalesAndMarketingAttendees::deleteAll(['sales_and_marketing_id' => $this->id]);
        //     foreach($this->attendees as $key => $attendee)
        //     {
        //         $model = new SalesAndMarketingAttendees;
        //         $model->sales_and_marketing_id = $this->id;
        //         $model->sales_and_marketing_attendees_id = $attendee;
        //         $model->save();
        //     }
        // }

        if(isset($this->client_attendee) && $this->client_attendee<>null)
        {
            foreach($this->client_attendee as $key=>$attendee)
            {
                if($attendee['client_attendee_id']<>null){
                    if(isset($attendee['attendee_db_id']) && $attendee['attendee_db_id']<>null){
                        $model = SalesAndMarketingAttendees::find()->where(['id'=>$attendee['attendee_db_id']])->one();
                    }else{
                        $model = new SalesAndMarketingAttendees;
                        $model->sales_and_marketing_id = $this->id;
                    }
                    $model->sales_and_marketing_attendees_id = $attendee['client_attendee_id'];
                    $model->client_attendee_title = $attendee['client_attendee_title'];
                    $model->client_attendee_type = $attendee['client_attendee_type'];
                    $model->organization = 'client';
                    if(!$model->save()){
                        dd($model->getErrors());
                    }
                }
            }
        }


        if(isset($this->wm_attendee) && $this->wm_attendee<>null)
        {
            foreach($this->wm_attendee as $key=>$attendee)
            {
                if($attendee['wm_attendee_id']<>null){
                    if(isset($attendee['attendee_db_id']) && $attendee['attendee_db_id']<>null){
                        $model = SalesAndMarketingAttendees::find()->where(['id'=>$attendee['attendee_db_id']])->one();
                    }else{
                        $model = new SalesAndMarketingAttendees;
                        $model->sales_and_marketing_id = $this->id;
                    }
                    $model->sales_and_marketing_id = $this->id;
                    $model->sales_and_marketing_attendees_id = $attendee['wm_attendee_id'];
                    $model->client_attendee_title = $attendee['wm_attendee_title'];
                    $model->client_attendee_type = $attendee['wm_attendee_type'];
                    $model->organization = 'windmills';
                    if(!$model->save()){
                        dd($model->getErrors());
                    }
                }
            }
        }

        if(isset($this->km_images) && $this->km_images<>null)
        {
            foreach($this->km_images as $key=>$image)
            {
                if($image['attachment']<>null){
                    if(isset($image['db_id']) && $image['db_id']<>null){
                        $model = MeetingImages::find()->where(['id'=>$image['db_id']])->one();
                    }else{
                        $model = new MeetingImages;
                        $model->meeting_id = $this->id;
                    }
                    $model->attachment = $image['attachment'];
                    $model->image_type = 'km_image';
                    if(!$model->save()){
                        dd($model->getErrors());
                    }
                }
            }
        }

        if(isset($this->parking_images) && $this->parking_images<>null)
        {
            foreach($this->parking_images as $key=>$image)
            {
                if($image['attachment']<>null){
                    if(isset($image['db_id']) && $image['db_id']<>null){
                        $model = MeetingImages::find()->where(['id'=>$image['db_id']])->one();
                    }else{
                        $model = new MeetingImages;
                        $model->meeting_id = $this->id;
                    }
                    $model->attachment = $image['attachment'];
                    $model->image_type = 'parking_image';
                    if(!$model->save()){
                        dd($model->getErrors());
                    }
                }
            }
        }
        
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
    
    
    

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client Name',
            'date' => 'Meeting Date',
            'time' => 'Meeting Time',
            'meeting_interface' => 'Meeting Interface',
            'purpose' => 'Meeting Purpose',
            'calendar_invite' => 'Calendar Invite',
            'attendees' => 'Meeting Attendees',
            'meeting_place' => 'Meeting Place',
            'meeting_location_pin' => 'Meeting Location URL',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
            'searchInput' => 'Location',
            'meeting_end_date' => 'Meeting End Date',
            'meeting_end_time' => 'Meeting End Time',
            'postpone_reason' => 'Postpone Reason',
            'rejected_reason' => 'Rejected Reason',
            'meeting_status' => 'Meeting Status',
        ];
    }
}

