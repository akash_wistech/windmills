<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClientSegmentFile;

/**
 * ClientSegmentFileSearch represents the model behind the search form of `app\models\ClientSegmentFile`.
 */
class ClientSegmentFileSearch extends ClientSegmentFile
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'client_type', 'developer', 'family', 'publicly_listed', 'private_company', 'audit_firm', 'law_firm', 'quotation_followup_period__days', 'quotation_followup_email_no', 'report_follow_update_days', 'client_services_feedback_days'], 'safe'],
            [['status_verified', 'status_verified_at', 'status_verified_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ClientSegmentFile::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client_type' => $this->client_type,
            'developer' => $this->developer,
            'family' => $this->family,
            'publicly_listed' => $this->publicly_listed,
            'private_company' => $this->private_company,
            'audit_firm' => $this->audit_firm,
            'law_firm' => $this->law_firm,
            'quotation_followup_email_no' => $this->quotation_followup_email_no,
            'quotation_followup_period__days' => $this->quotation_followup_period__days,
            'report_follow_update_days' => $this->report_follow_update_days,
            'client_services_feedback_days' => $this->client_services_feedback_days,
            'status_verified' => $this->status_verified,
            'status_verified_at' => $this->status_verified_at,
            'status_verified_by' => $this->status_verified_by,
        ]);

        return $dataProvider;
    }
}
