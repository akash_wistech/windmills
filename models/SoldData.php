<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sold_data".
 *
 * @property int $id
 * @property string|null $building_title
 * @property string|null $type
 * @property string|null $bedrooms
 * @property string|null $bua
 * @property string|null $plot_area
 * @property string|null $unit_number
 * @property string|null $property_type
 * @property string|null $listing_date
 * @property string|null $price
 * @property string|null $created_at
 */
class SoldData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sold_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['building_title', 'bedrooms', 'bua', 'plot_area', 'unit_number', 'property_type', 'listing_date', 'price', 'created_at'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'building_title' => 'Building Title',
            'bedrooms' => 'Bedrooms',
            'bua' => 'Bua',
            'plot_area' => 'Plot Area',
            'unit_number' => 'Unit Number',
            'property_type' => 'Property Type',
            'listing_date' => 'Listing Date',
            'price' => 'Price',
            'created_at' => 'Created At',
        ];
    }
}
