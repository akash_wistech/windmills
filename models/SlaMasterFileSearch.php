<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SlaMasterFile;

/**
 * SlaMasterFileSearch represents the model behind the search form of `app\models\SlaMasterFile`.
 */
class SlaMasterFileSearch extends SlaMasterFile
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'client', 'property_type', 'city', 'tenure', 'inspection_type', 'status_verified', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['tat', 'created_at', 'updated_at', 'deleted_at','fee','valuation_approach'], 'safe'],
            [['bua_from','bua_to','plot_size_from','plot_size_to','no_of_units_from','no_of_units_to'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SlaMasterFile::find()->orderBy(['id' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client' => $this->client,
            'property_type' => $this->property_type,
            'city' => $this->city,
            'tenure' => $this->tenure,
            'fee' => $this->fee,
            'valuation_approach' => $this->valuation_approach,
            'inspection_type' => $this->inspection_type,
            'status_verified' => $this->status_verified,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'tat', $this->tat]);

        $query->andFilterWhere(['between', 'bua_from', 'bua_to', $this->bua_from]);
        $query->andFilterWhere(['between', 'plot_size_from', 'plot_size_to', $this->plot_size_from]);
        $query->andFilterWhere(['between', 'no_of_units_from', 'no_of_units_to', $this->no_of_units_from]);
        

        return $dataProvider;
    }
}
