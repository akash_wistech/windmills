<?php
namespace app\models;
use Yii;

class PropertyFinderUrls extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return '{{%property_finder_urls}}';
	}

	public function rules()
	{
		return [
			[['url'],'safe'],
		];
	}
}
