<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "standard_report".
 *
 * @property int $id
 * @property string|null $client_intended_users
 * @property string|null $subject_property
 * @property string|null $valuation_instructions
 * @property string|null $purpose_valuation
 * @property string|null $terms_engagement_agreed
 * @property string|null $value_duties_supervision
 * @property string|null $internal_external_status_valuer
 * @property string|null $declaration_independence_objectivity
 * @property string|null $assum_extent_inesti_limit_scope_work
 * @property string|null $structural_tecnical_survey
 * @property string|null $conditions_state_repair_matenence
 * @property string|null $contamination_ground_enviormental
 * @property string|null $natural_disasters
 * @property string|null $national_scenaiors_majeure_situation
 * @property string|null $statutory_regulatory_requirements
 * @property string|null $title_tenancies_property_document
 * @property string|null $planning_highway_enquires
 * @property string|null $plant_equipment
 * @property string|null $development_property
 * @property string|null $disposal_cost_libility
 * @property string|null $documentation_provided_clients
 * @property string|null $documentation_not_provided_clients
 * @property string|null $nature_sources_info_doc
 * @property string|null $rics_valuation
 * @property string|null $basis_value
 * @property string|null $market_value
 * @property string|null $statutory_definition_marketvalue
 * @property string|null $market_rent
 * @property string|null $investment_value
 * @property string|null $fair_value
 * @property string|null $valuation_date
 * @property string|null $comparable_analyses
 * @property string|null $comparable_properties
 * @property string|null $market_commentary
 * @property string|null $summary_key_inputs
 * @property string|null $valuation
 * @property string|null $general_uncertainties
 * @property string|null $cronavirus_related_contingencies
 * @property string|null $confidentinality
 * @property string|null $restrictions_use_destribution_publication
 * @property string|null $property_valuation_overview
 * @property string|null $limitation_attaching_document
 * @property string|null $limitation_of_liability
 */
class StandardReportIncome extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'standard_report_income';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_intended_users', 'subject_property', 'valuation_instructions', 'purpose_valuation', 'terms_engagement_agreed', 'value_duties_supervision', 'internal_external_status_valuer', 'declaration_independence_objectivity', 'assum_extent_inesti_limit_scope_work', 'structural_tecnical_survey', 'conditions_state_repair_matenence', 'contamination_ground_enviormental', 'natural_disasters', 'national_scenaiors_majeure_situation', 'statutory_regulatory_requirements', 'title_tenancies_property_document', 'planning_highway_enquires', 'plant_equipment', 'development_property', 'disposal_cost_libility', 'documentation_provided_clients', 'documentation_not_provided_clients', 'nature_sources_info_doc', 'rics_valuation', 'basis_value', 'market_value', 'statutory_definition_marketvalue', 'market_rent', 'investment_value', 'fair_value', 'valuation_date', 'comparable_analyses', 'comparable_properties', 'market_commentary', 'summary_key_inputs', 'valuation', 'general_uncertainties', 'cronavirus_related_contingencies', 'confidentinality', 'restrictions_use_destribution_publication', 'property_valuation_overview', 'limitation_attaching_document', 'limitation_of_liability','restricted_marketing_period'], 'string'],
            [['status_verified','status_verified_at','status_verified_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_intended_users' => 'Client Intended Users',
            'subject_property' => 'Subject Property',
            'valuation_instructions' => 'Valuation Instructions',
            'purpose_valuation' => 'Purpose Valuation',
            'terms_engagement_agreed' => 'Terms Engagement Agreed',
            'value_duties_supervision' => 'Value Duties Supervision',
            'internal_external_status_valuer' => 'Internal External Status Valuer',
            'declaration_independence_objectivity' => 'Declaration Independence Objectivity',
            'assum_extent_inesti_limit_scope_work' => 'Assum Extent Inesti Limit Scope Work',
            'structural_tecnical_survey' => 'Structural Tecnical Survey',
            'conditions_state_repair_matenence' => 'Conditions State Repair Matenence',
            'contamination_ground_enviormental' => 'Contamination Ground Enviormental',
            'natural_disasters' => 'Natural Disasters',
            'national_scenaiors_majeure_situation' => 'National Scenaiors Majeure Situation',
            'statutory_regulatory_requirements' => 'Statutory Regulatory Requirements',
            'title_tenancies_property_document' => 'Title Tenancies Property Document',
            'planning_highway_enquires' => 'Planning Highway Enquires',
            'plant_equipment' => 'Plant Equipment',
            'development_property' => 'Development Property',
            'disposal_cost_libility' => 'Disposal Cost Libility',
            'documentation_provided_clients' => 'Documentation Provided Clients',
            'documentation_not_provided_clients' => 'Documentation Not Provided Clients',
            'nature_sources_info_doc' => 'Nature Sources Info Doc',
            'rics_valuation' => 'Rics Valuation',
            'basis_value' => 'Basis Value',
            'market_value' => 'Market Value',
            'statutory_definition_marketvalue' => 'Statutory Definition Marketvalue',
            'market_rent' => 'Market Rent',
            'investment_value' => 'Investment Value',
            'fair_value' => 'Fair Value',
            'valuation_date' => 'Valuation Date',
            'comparable_analyses' => 'Comparable Analyses',
            'comparable_properties' => 'Comparable Properties',
            'market_commentary' => 'Market Commentary',
            'summary_key_inputs' => 'Summary Key Inputs',
            'valuation' => 'Valuation',
            'general_uncertainties' => 'General Uncertainties',
            'cronavirus_related_contingencies' => 'Cronavirus Related Contingencies',
            'confidentinality' => 'Confidentinality',
            'restrictions_use_destribution_publication' => 'Restrictions Use Destribution Publication',
            'property_valuation_overview' => 'Property Valuation Overview',
            'limitation_attaching_document' => 'Limitation Attaching Document',
            'limitation_of_liability' => 'Limitation Of Liability',
            'restricted_marketing_period' => 'Estimated Price under the restricted marketing period',
        ];
    }

    public function beforeSave($insert)
    {

        // verify status reset after edit only something changed
        $isChanged = false;
        $ignoredAttributes = ['status_verified_at']; 
        foreach ($this->attributes as $attribute => $value) {
            if (in_array($attribute, $ignoredAttributes)) {
                continue;
            }
            if ($this->getOldAttribute($attribute) != $value) {
                $isChanged = true;
                break;
            }
        }
        if ($isChanged) {
            if($this->status_verified == 1 && $this->getOldAttribute('status_verified') == 1){
                $this->status_verified = 2;
            }elseif ($this->status_verified == 2 && $this->getOldAttribute('status_verified') == 1) {
                $this->status_verified = 1;
            }
        }
        
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }
}
