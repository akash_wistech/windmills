<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use app\models\SoldDataUpgrade;

/**
 * SoldTransactionImportForm is the model behind the sold transaction import form.
 */
class SoldTransactionImportForm extends Model
{
    public $importfile;
    public $allowedFileTypes = ['csv'];//'xls', 'xlsx',

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['importfile'], 'safe'],
            [['importfile'], 'file', 'extensions' => implode(",", $this->allowedFileTypes)]
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'importfile' => Yii::t('app', 'File'),
        ];
    }

    /**
     * import the entries into table
     */
    public function save()
    {

        $saved = 0;
        $unsaved = 0;
        $errNames = '';
        $change = array();
        if ($this->validate()) {
           // ini_set('max_execution_time', '0');
            if (UploadedFile::getInstance($this, 'importfile')) {
                $importedFile = UploadedFile::getInstance($this, 'importfile');

                // if no image was uploaded abort the upload
                if (!empty($importedFile)) {
                    $pInfo = pathinfo($importedFile->name);

                    $ext = $pInfo['extension'];

                    if (in_array($ext, $this->allowedFileTypes)) {
                        // Check to see if any PHP files are trying to be uploaded
                        $content = file_get_contents($importedFile->tempName);

                        if (preg_match('/\<\?php/i', $content)) {

                        } else {
                            $this->importfile = Yii::$app->params['temp_abs_path'] . $importedFile->name;
                            $importedFile->saveAs($this->importfile);

                            $csvFile = fopen($this->importfile, 'r');

                            $data = [];
                            $n = 1;
                            while (($line = fgetcsv($csvFile)) !== FALSE) {


                                $transaction_type = $line[0];
                                $sub_type = $line[1];
                                $sales_sequence = $line[2];
                                $reidin_ref_number = $line[3];
                                $date = str_replace('/', '-', trim($line[4]));
                                $date = date("Y-m-d", strtotime($date));
                                $reidin_community = $line[5];
                                $buildingName = $line[6];
                                $reidin_property_type = $line[7];
                                $unit_number = $line[8];
                                $needle = '-';

                              /*  if (strpos($unit_number, $needle) !== false) {
                                    $explode_unit_value = explode('-', $unit_number);
                                    $unit_number = $explode_unit_value[1];
                                }*/
                                if (strpos($unit_number, $needle) !== false) {
                                    $explode_unit_value = explode('-', $unit_number);
                                    if($explode_unit_value <> null && count($explode_unit_value) > 0){
                                        $unit_number = str_replace($explode_unit_value[0].'-', "", $unit_number);
                                        $unit_number= trim($unit_number);
                                    }
                                }

                                $needle_bedroom = 'B/R';
                                if (strpos($line[9], $needle_bedroom) !== false) {
                                    $berooms = explode(' B/R', trim($line[9]));
                                    $rooms = trim($berooms[0]);
                                }else{
                                    $rooms = null;
                                }

                               /* $berooms = explode(' B/R', trim($line[9]));
                                $rooms = trim($berooms[0]);*/

                                $floor_number = $line[10];
                                if ($floor_number == 'G') {
                                    $floor_number = 0;
                                }
                                $parking_spaces = $line[11];
                                $balcony_size = $line[12];

                                $bua = str_replace(",", "", $line[13]);
                                $plotArea = str_replace(",", "", $line[14]);
                                $price = str_replace(",", "", $line[15]);
                                $pricePerSqt = str_replace(",", "", $line[16]);
                                $reidin_developer = $line[17];


                                if ($n > 1) {


                                    if($buildingName == ''){

                                        $subCommRow = SubCommunities::find()
                                            ->where(['=', 'title', trim($reidin_community)])->one();



                                        if ($subCommRow != null) {
                                            //echo $reidin_property_type;
                                            $getProperty_id =  Yii::$app->appHelperFunctions->propertyTypeImport[$reidin_property_type];

                                            if($getProperty_id != ''){
                                                $property = Properties::findOne($getProperty_id);
                                                $property_cat = $property->category;
                                            }else{
                                                $getProperty_id = 92;
                                                $property_cat = 6;
                                            }

                                            $buildingRow = Buildings::find()
                                                ->where(['=', 'title', trim($reidin_community)])
                                                ->orWhere(['=', 'reidin_title', trim($reidin_community)])
                                                ->orWhere(['=', 'reidin_title_2', trim($reidin_community)])
                                                ->asArray()->one();

                                            if($buildingRow !=''){

                                            }else {


                                                $newBuildings = new Buildings;
                                                $newBuildings->title = trim($reidin_community);
                                                $newBuildings->property_category = $property_cat;
                                                $newBuildings->property_id = $getProperty_id;
                                                $newBuildings->city = 3510;
                                                $newBuildings->tenure = 2;
                                                $newBuildings->community = $subCommRow->community;
                                                $newBuildings->sub_community = $subCommRow->id;
                                                $newBuildings->developer_id = 1;
                                                $newBuildings->vacancy = 1;
                                                $newBuildings->property_visibility = 3;
                                                $newBuildings->utilities_connected = 'Yes';
                                                $newBuildings->development_type = 'Standard';
                                                $newBuildings->property_condition = 3;
                                                $newBuildings->other_facilities = '';
                                                $newBuildings->completion_status = 100.00;
                                                $newBuildings->location_highway_drive = 'minutes_5';
                                                $newBuildings->location_school_drive = 'minutes_5';
                                                $newBuildings->location_mall_drive = 'minutes_5';
                                                $newBuildings->location_sea_drive = 'minutes_5';
                                                $newBuildings->location_park_drive = 'minutes_5';
                                                /* echo "<pre>";
                                                 print_r($subCommRow);
                                                 print_r($newBuildings);
                                                 die;*/
                                                if (!$newBuildings->save()) {
                                                    echo "<pre>";
                                                    print_r($newBuildings->errors);
                                                    die('sub');
                                                }
                                            }


                                        }else{
                                            $commRow = Communities::find()
                                                ->where(['=', 'title', trim($reidin_community)])
                                                ->one();

                                            if ($commRow != null) {
                                                $getProperty_id =  Yii::$app->appHelperFunctions->propertyTypeImport[$reidin_property_type];

                                                if($getProperty_id != ''){
                                                    $property = Properties::findOne($getProperty_id);
                                                    $property_cat = $property->category;
                                                }else{
                                                    $getProperty_id = 92;
                                                    $property_cat = 6;
                                                }
                                                $subCommRow = SubCommunities::find()
                                                    ->where(['=', 'community', $commRow->id])
                                                    ->one();
                                                $buildingRow = Buildings::find()
                                                    ->where(['=', 'title', trim($reidin_community)])
                                                    ->orWhere(['=', 'reidin_title', trim($reidin_community)])
                                                    ->orWhere(['=', 'reidin_title_2', trim($reidin_community)])
                                                    ->asArray()->one();

                                                if($buildingRow !=''){

                                                }else {


                                                    $newBuildings = new Buildings;
                                                    $newBuildings->title = trim($reidin_community);;
                                                    $newBuildings->property_category = $property_cat;
                                                    $newBuildings->property_id =$getProperty_id;
                                                    $newBuildings->city = 3510;
                                                    $newBuildings->tenure = 2;
                                                    $newBuildings->property_visibility = 3;
                                                    $newBuildings->community = $commRow->id;
                                                    $newBuildings->sub_community = $subCommRow->id;
                                                    $newBuildings->developer_id = 1;
                                                    $newBuildings->vacancy = 1;
                                                    $newBuildings->utilities_connected = 'Yes';
                                                    $newBuildings->development_type = 'Standard';
                                                    $newBuildings->property_condition = 3;
                                                    $newBuildings->other_facilities = '';
                                                    $newBuildings->completion_status = 100.00;
                                                    $newBuildings->location_highway_drive = 'minutes_5';
                                                    $newBuildings->location_school_drive = 'minutes_5';
                                                    $newBuildings->location_mall_drive = 'minutes_5';
                                                    $newBuildings->location_sea_drive = 'minutes_5';
                                                    $newBuildings->location_park_drive = 'minutes_5';
                                                    if (!$newBuildings->save()) {
                                                        echo "<pre>";
                                                        print_r($newBuildings);
                                                        die('com');
                                                    }
                                                }
                                            }
                                        }

                                        /*echo "<pre>";
                                        print_r($line);
                                        die;
                                        die('here1');*/
                                    }







                                    if($buildingName == ''){
                                        $buildingId = 0;
                                        $buildingRow = Buildings::find()
                                            ->where(['=', 'title', trim($reidin_community)])
                                            ->orWhere(['=', 'reidin_title', trim($reidin_community)])
                                            ->orWhere(['=', 'reidin_title_2', trim($reidin_community)])
                                            ->asArray()->one();
                                    }else {

                                        $buildingId = 0;
                                        $buildingRow = Buildings::find()
                                            ->where(['=', 'title', trim($buildingName)])
                                            ->orWhere(['=', 'reidin_title', trim($buildingName)])
                                            ->orWhere(['=', 'reidin_title_2', trim($buildingName)])
                                            ->asArray()->one();
                                    }


                                    if ($buildingRow != null) {



                                        $building = array(
                                            'building_info' => $buildingRow['id'],
                                            'no_of_bedrooms' => $rooms,
                                            'built_up_area' => $bua,
                                            //'unit_number' => $unit_number,
                                            'land_size' => $plotArea,
                                            'transaction_date' => $date,
                                            'listings_price' => $price,
                                            'price_per_sqt' => $pricePerSqt,
                                            'reidin_ref_number' => $reidin_ref_number,
                                        );




                                        $comparison = SoldTransaction::find()
                                            ->where(['building_info' => $buildingRow['id'],
                                                'no_of_bedrooms' =>  $rooms,
                                                'built_up_area' => $bua,
                                                //'unit_number' => $unit_number,
                                                'land_size' => $plotArea,
                                                'transaction_date' => $date,
                                                'listings_price' => $price,
                                                'price_per_sqt' => $pricePerSqt,
                                               // 'reidin_ref_number' => $reidin_ref_number,
                                            ])->one();



                                        if ($comparison == null) {
                                            // echo "Record Found"; die();
                                            $buildingId = $buildingRow['id'];


                                            $model = new SoldTransaction;
                                            $model->scenario = 'import';
                                            $model->building_info = $buildingId;
                                            if($rooms != 'Unknown' && $rooms != 'PENTHOUSE' && $rooms <> null){
                                                $model->no_of_bedrooms = $rooms;
                                            }
                                            $bua = trim($bua);
                                            //  $model->no_of_bedrooms = ($rooms <> null && ($rooms != 'Unknown')) ? $rooms : 0;
                                            $model->built_up_area = ($bua <> null && $bua !== '-') ?  (float)$bua : 0;
                                            $model->land_size = ($plotArea <> null && $plotArea !== '-') ? $plotArea : 0;;
                                            $model->unit_number = ($unit_number <> null && $unit_number !== '-') ? $unit_number : '';
                                            $model->floor_number = ($floor_number <> null && $floor_number !== '-') ? $floor_number : '';
                                            $model->balcony_size = ($balcony_size <> null && $balcony_size !== '-') ? $balcony_size : '';
                                            $model->parking_space_number = ($parking_spaces <> null && $parking_spaces !== '-') ? $parking_spaces : '';
                                            $model->transaction_type = ($transaction_type <> null) ? $transaction_type : '';
                                            $model->sub_type = ($sub_type <> null && $sub_type !== '-') ? $sub_type : '';
                                            $model->sales_sequence = ($sales_sequence <> null && $sales_sequence !== '-') ? $sales_sequence : '';
                                            $model->reidin_ref_number = ($reidin_ref_number <> null && $reidin_ref_number !== '-') ? $reidin_ref_number : '';
                                            $model->reidin_community = ($reidin_community <> null && $reidin_community !== '-') ? $reidin_community : '';
                                            $model->reidin_property_type = ($reidin_property_type <> null && $reidin_property_type !== '-') ? $reidin_property_type : '';
                                            $model->reidin_developer = ($reidin_developer <> null && $reidin_developer !== '-') ? $reidin_developer : '';

                                            $model->transaction_date = $date;
                                            $model->listings_price = ($price <> null && $price !== '-') ? $price : 0;

                                            $model->property_category = $buildingRow['property_category'];
                                            $model->location = $buildingRow['location'];
                                            $model->tenure = $buildingRow['tenure'];
                                            $model->utilities_connected = $buildingRow['utilities_connected'];
                                            $model->development_type = $buildingRow['development_type'];
                                            $model->property_placement = $buildingRow['property_placement'];
                                            $model->property_visibility = $buildingRow['property_visibility'];
                                            $model->property_exposure = $buildingRow['property_exposure'];
                                            $model->property_condition = $buildingRow['property_condition'];
                                            $model->pool = $buildingRow['pool'];
                                            $model->gym = $buildingRow['gym'];
                                            $model->play_area = $buildingRow['play_area'];
                                            // $model->other_facilities = $buildingRow['other_facilities'];
                                            $model->landscaping = $buildingRow['landscaping'];
                                            $model->white_goods = $buildingRow['white_goods'];
                                            $model->furnished = $buildingRow['furnished'];
                                            $model->finished_status = $buildingRow['finished_status'];


                                            $model->price_per_sqt = ($pricePerSqt <> null && $pricePerSqt !== '#VALUE!') ? $pricePerSqt : 0;;
                                            if($transaction_type == 'Sales - Off-Plan'){
                                                $model->type= 2;
                                            }else{
                                                $model->type= 1;
                                            }
                                           /*  echo "<pre>";
                                             print_r($model);
                                             die;*/
                                            if ($model->save()) {
                                                $saved++;
                                            } else {
                                                if ($model->hasErrors()) {
                                                    foreach ($model->getErrors() as $error) {
                                                        if (count($error) > 0) {
                                                            foreach ($error as $key => $val) {
                                                                echo $val.'<br>';
                                                                echo $pricePerSqt;
                                                            }
                                                        }
                                                    }
                                                }
                                               // die();
                                                $errNames .= '<br />' . $buildingName;
                                                $unsaved++;
                                            }


                                        }else{


                                            if($comparison->unit_number <> null){

                                            }else{
                                                Yii::$app->db->createCommand()->update('sold_transaction', ['unit_number' => $unit_number],  ['id' => $comparison->id])->execute();
                                            }

                                            $data = new SoldDataUpgrade();
                                            $data->building_name = $buildingName;
                                            $data->no_of_rooms = $rooms;
                                            $data->bua = $bua;
                                            $data->plot_area = $plotArea;
                                            $data->type = $transaction_type;
                                            $data->sold_date = $date;
                                            $data->price = $price;
                                            $data->price_sqf = $pricePerSqt;
                                            $data->unit_number = $unit_number;
                                            $data->floor_number = $floor_number;
                                            $data->balcony_size = $balcony_size;
                                            $data->parking_space_number = $parking_spaces;
                                            $data->transaction_type = ($transaction_type <> null) ? $transaction_type : '';
                                            $data->sub_type = ($parking_spaces <> null && $parking_spaces !== '-') ? $parking_spaces : '';
                                            $data->sales_sequence = ($sales_sequence <> null && $sales_sequence !== '-') ? $sales_sequence : '';
                                            $data->reidin_ref_number = ($reidin_ref_number <> null && $reidin_ref_number !== '-') ? $reidin_ref_number : '';
                                            $data->reidin_community = ($reidin_community <> null && $reidin_community !== '-') ? $reidin_community : '';
                                            $data->reidin_property_type = ($reidin_property_type <> null && $reidin_property_type !== '-') ? $reidin_property_type : '';
                                            $data->reidin_developer = ($reidin_developer <> null && $reidin_developer !== '-') ? $reidin_developer : '';
                                            $data->data_type = 1;
                                            $data->save();

                                        }


                                    } else {

                                        //comprison
                                        $comparison_one = SoldDataUpgrade::find()
                                            ->where(['building_name' => $buildingName,
                                                'no_of_rooms' => $rooms,
                                                'bua' => $bua,
                                              //  'unit_number' => $unit_number,
                                                'plot_area' => $plotArea,
                                                'sold_date' => $date,
                                                'price' => $price,
                                                'price_sqf' => $pricePerSqt])
                                            ->one();
                                        if ($comparison_one == null) {
                                            //Date
                                            $data = new SoldDataUpgrade();
                                            $data->building_name = $buildingName;
                                            $data->no_of_rooms = $rooms;
                                            $data->bua = $bua;
                                            $data->plot_area = $plotArea;
                                            $data->type = $transaction_type;
                                            $data->sold_date = $date;
                                            $data->price = $price;
                                            $data->price_sqf = $pricePerSqt;
                                            $data->unit_number = $unit_number;
                                            $data->floor_number = $floor_number;
                                            $data->balcony_size = $balcony_size;
                                            $data->parking_space_number = $parking_spaces;
                                            $data->transaction_type = ($transaction_type <> null) ? $transaction_type : '';
                                            $data->sub_type = ($parking_spaces <> null && $parking_spaces !== '-') ? $parking_spaces : '';
                                            $data->sales_sequence = ($sales_sequence <> null && $sales_sequence !== '-') ? $sales_sequence : '';
                                            $data->reidin_ref_number = ($reidin_ref_number <> null && $reidin_ref_number !== '-') ? $reidin_ref_number : '';
                                            $data->reidin_community = ($reidin_community <> null && $reidin_community !== '-') ? $reidin_community : '';
                                            $data->reidin_property_type = ($reidin_property_type <> null && $reidin_property_type !== '-') ? $reidin_property_type : '';
                                            $data->reidin_developer = ($reidin_developer <> null && $reidin_developer !== '-') ? $reidin_developer : '';
                                            $data->data_type = 0;
                                            $data->save();
                                        }

                                    }
                                }
                                $n++;
                            }
                           /* $connection = Yii::$app->getDb();
                            $command = $connection->createCommand("DELETE FROM sold_transaction WHERE transaction_date='1970-01-01'");
                            $command->queryAll();*/
                            // echo "No New Record All Records Are Already Exists";die();
// echo "<pre>";
// print_r($bua);fdie
// echo "</pre>";
// die();
                            // echo "<pre>";
                            // print_r($change);
                            // die;
                            fclose($csvFile);
                            //Unlink File
                            unlink($this->importfile);
                        }
                    }
                }
            }
            if ($saved > 0) {

                Yii::$app->getSession()->setFlash('success', $saved . ' - ' . Yii::t('app', 'rows saved successfully'));
            }
            if ($unsaved > 0) {
                Yii::$app->getSession()->setFlash('error', $unsaved . ' - ' . Yii::t('app', 'rows were not saved!') . $errNames);
            }
            return true;
        } else {
            return false;
        }
    }
}
