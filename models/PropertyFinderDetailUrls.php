<?php
namespace app\models;
use Yii;

class PropertyFinderDetailUrls extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return '{{%property_finder_detail_urls}}';
	}

	public function rules()
	{
		return [
			[['detail_url'],'safe'],
		];
	}
}
