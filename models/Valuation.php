<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Invoice;

use app\modules\wisnotify\listners\NotifyEvent;

/**
 * This is the model class for table "valuation".
 *
 * @property int $id
 * @property string $reference_number
 * @property int $client_id
 * @property string|null $client_reference
 * @property string $prefix_customer_name
 * @property string $client_name_passport
 * @property string $client_lastname_passport
 * @property int $no_of_owners
 * @property int $service_officer_name
 * @property string $instruction_date
 * @property string $target_date
 * @property int $building_info
 * @property int $property_id
 * @property int $property_category
 * @property int $community
 * @property int $sub_community
 * @property int $tenure
 * @property int $unit_number
 * @property int $city
 * @property string $payment_plan
 * @property int $status
 * @property int $building_number
 * @property int $plot_number
 * @property string $street
 * @property int $floor_number
 * @property int $instruction_person
 * @property float $land_size
 * @property int $purpose_of_valuation
 * @property int|null $created_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class Valuation extends ActiveRecordFull
{
    public $step,$total,$total_valuations,$client_revenue,$allvalues,$client_type,$zone_name,$time_period,
        $tat,$low_vals,$high_vals,$errors_vals,$parent_estimated_market_value,$percentage_value,$estimated_market_value,$total_inspections,$years,$months,$month_number,
        $submission_date,$inspection_date,$city_filter,$mv_psf,$rv_psf,$step_number,$total_valuations_avg,$inspection_officer_name,$inspection_time,$valuation_month,$valuation_year;
    public $client_title,$quarter,$half_year,$year,$client_business,$inspection_done_date_time,$valuation_type;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'valuation';
    }
    const SCENARIO_INQUIRY = 'register';
    const SCENARIO_DETAIL = 'profileUpdate';

    // Other model properties and methods...

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_INQUIRY] =
            [
                'reference_number', 'client_id', 'client_name_passport', 'service_officer_name', 'instruction_date', 'target_date', 'building_info',
                'property_id', 'property_category', 'tenure', 'unit_number', 'building_number',
                'plot_number', 'street', 'floor_number', 'land_size', 'purpose_of_valuation',
                'no_of_owners','valuation_scope','email_subject','inspection_type','client_fixed_fee_check',
                'status', 'created_by', 'updated_by', 'trashed', 'trashed_by',
                'other_instructing_person','proceed',
                'created_at', 'updated_at', 'trashed_at','special_assumption',
                'extent_of_investigation','market_summary','approval_id','approval_status','root_id','signature_img',
                'quotation_id','valuation_status','email_status','step','ban_date',
                'cancel_by','cancelled_approved_by','total','quotation_property','general_assumption',
                'other_intended_users','revised_reason','total_valuations','fee', 'client_revenue','allvalues','time_period',
                'total_inspections','city_filter','quickbooks_invoice_id','step_number',
                'signature_doc','month_number','portfolio','ref_portfolio',
                'payment_plan','no_of_towers','urgency','client_requirement_date',
                'client_reference','link_to_scrape','link_to_scrape_status','b_to_rent_link_to_scrape',
                'b_to_rent_link_to_scrape_status','pf_for_sale_scrape_url','pf_for_sale_scrape_url_status','pf_to_rent_scrape_url',
                'pf_to_rent_scrape_url_status','client_invoice_type','title_deed','taqyeem_number',
                'client_title','quarter','half_year','year','total_fee','quickbooks_invoice_id_final','reference_number_final',
                'client_business','land_line_code','phone_code','contact_person_name','contact_person_lastname', 'contact_email', 'contact_phone_no','client_lastname_passport','prefix_customer_name',
                'land_line_no', 'email','prefix','contact_person_lastname','status_verified','status_verified_by','status_verified_at','instruction_time','inspection_done_date_time','mobile_instructing_person','prefix_instructing_person','firstname_instructing_person','lastname_instructing_person','email_instructing_person','valuation_approach','ajman_email_status','ajman_approved','zoho_date_status','ajman_follow_up_date'
        ];
        $scenarios[self::SCENARIO_DETAIL] = [
            'reference_number', 'client_id', 'client_name_passport', 'client_lastname_passport', 'prefix_customer_name', 'service_officer_name', 'instruction_date', 'target_date', 'building_info',
            'property_id', 'property_category', 'tenure', 'unit_number', 'building_number',
            'plot_number', 'street', 'floor_number', 'land_size', 'purpose_of_valuation',
            'no_of_owners','valuation_scope','email_subject','inspection_type','client_fixed_fee_check',
            'status', 'created_by', 'updated_by', 'trashed', 'trashed_by',
            'other_instructing_person',
            'created_at', 'updated_at', 'trashed_at','special_assumption',
            'extent_of_investigation','market_summary','approval_id','approval_status','root_id','signature_img',
            'quotation_id','valuation_status','email_status','step','ban_date',
            'cancel_by','cancelled_approved_by','total','quotation_property','general_assumption',
            'other_intended_users','revised_reason','total_valuations','fee', 'client_revenue','allvalues','time_period',
            'total_inspections','city_filter','quickbooks_invoice_id','step_number',
            'signature_doc','month_number',
            'payment_plan','no_of_towers','portfolio','ref_portfolio',
            'client_reference','link_to_scrape','link_to_scrape_status','b_to_rent_link_to_scrape',
            'b_to_rent_link_to_scrape_status','pf_for_sale_scrape_url','pf_for_sale_scrape_url_status','pf_to_rent_scrape_url',
            'pf_to_rent_scrape_url_status','client_invoice_type','title_deed','taqyeem_number',
            'client_title','quarter','half_year','year','total_fee','quickbooks_invoice_id_final','reference_number_final',
            'client_business','land_line_code','phone_code','contact_person_name','contact_person_lastname', 'contact_email', 'contact_phone_no','ajman_email_status','ajman_approved','instruction_time','prefix_instructing_person','firstname_instructing_person','lastname_instructing_person','email_instructing_person','mobile_instructing_person','prefix',
            'land_line_no', 'email','zoho_date_status','valuation_approach','client_module_id','trustee_id'
        ];

        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            //Inquiry
            [['reference_number', 'client_id', 'instruction_date', 'target_date','instruction_time',
                'email_subject','inspection_type','client_fixed_fee_check','contact_person_name',
                'other_instructing_person',
                'prefix','contact_person_lastname','urgency','client_requirement_date'], 'required', 'on' => self::SCENARIO_INQUIRY],
            [['client_id', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by','other_instructing_person'], 'integer', 'on' => self::SCENARIO_INQUIRY],
            [['reference_number'], 'unique', 'on' => self::SCENARIO_INQUIRY],
            [['client_title','quarter','half_year','year','total_fee','quickbooks_invoice_id_final','reference_number_final',
                'portfolio','ref_portfolio',  'client_business','land_line_code','phone_code', 'client_name_passport','status_verified','status_verified_by','status_verified_at','client_lastname_passport','prefix_customer_name','contact_phone_no','land_line_no','special_assumption','general_assumption','instruction_time','inspection_done_date_time','mobile_instructing_person','prefix_instructing_person', 'firstname_instructing_person','lastname_instructing_person','email_instructing_person','contact_email','contact_phone_no','valuation_approach','prefix_instructing_person','contact_email','ajman_approved_image','taqyeem_certificate','payment_slip','ajman_follow_up_date','proceed','online_module_id'], 'safe', 'on' => self::SCENARIO_INQUIRY],
            [['contact_person_name', 'contact_email'], 'string', 'max' => 255, 'on' => self::SCENARIO_INQUIRY],
            ['contact_phone_no', 'match', 'pattern' => '/^\d{7}$/', 'message' => 'Please enter a valid 7-digit phone number.', 'on' => self::SCENARIO_INQUIRY],
            ['land_line_no', 'match', 'pattern' => '/^\d{7}$/', 'message' => 'Please enter a valid 7-digit phone number.', 'on' => self::SCENARIO_INQUIRY],
            ['contact_email', 'email', 'on' => self::SCENARIO_INQUIRY],

            //Detail
            [['reference_number', 'client_id', 'client_name_passport', 'service_officer_name', 'instruction_date', 'instruction_time', 'target_date', 'building_info', 'property_id', 'property_category', 'tenure', 'building_number', 'plot_number', 'street', 'land_size', 'purpose_of_valuation','no_of_owners','valuation_scope','email_subject','inspection_type','client_fixed_fee_check'], 'required', 'on' => self::SCENARIO_DETAIL],
            [['client_id', 'service_officer_name', 'building_info', 'property_id', 'property_category', 'tenure', 'status', 'purpose_of_valuation', 'created_by', 'updated_by', 'trashed', 'trashed_by','no_of_owners','other_instructing_person'], 'integer', 'on' => self::SCENARIO_DETAIL],
            [['instruction_date','portfolio','ref_portfolio', 'target_date', 'created_at', 'updated_at', 'trashed_at','special_assumption','extent_of_investigation', 'floor_number', 'unit_number','market_summary','approval_id','approval_status','root_id','signature_img', 'plot_number', 'unit_number', 'quotation_id','valuation_status','email_status','step','ban_date','cancel_by','cancelled_approved_by','email_subject','total','quotation_property','general_assumption','other_intended_users','revised_reason','total_valuations','fee', 'client_revenue','allvalues','time_period','inspection_type','total_inspections','city_filter','quickbooks_invoice_id','step_number', 'building_number','signature_doc','month_number','client_fixed_fee_check','zoho_date_status','ajman_approved_image','taqyeem_certificate','payment_slip','online_module_id'], 'safe', 'on' => self::SCENARIO_DETAIL],
            [['payment_plan'], 'string', 'on' => self::SCENARIO_DETAIL],
            [['land_size','no_of_towers'], 'number', 'on' => self::SCENARIO_DETAIL],
            [['reference_number'], 'unique', 'on' => self::SCENARIO_DETAIL],
            [['reference_number', 'client_name_passport','client_lastname_passport','prefix_customer_name'], 'string', 'max' => 100, 'on' => self::SCENARIO_DETAIL],
            [['client_reference', 'street'], 'string', 'max' => 255, 'on' => self::SCENARIO_DETAIL],
            [['link_to_scrape','link_to_scrape_status','b_to_rent_link_to_scrape','b_to_rent_link_to_scrape_status','pf_for_sale_scrape_url','pf_for_sale_scrape_url_status','pf_to_rent_scrape_url','pf_to_rent_scrape_url_status','client_invoice_type','title_deed','taqyeem_number','no_of_towers'], 'safe', 'on' => self::SCENARIO_DETAIL],
            [['client_title','quarter','half_year','year','total_fee','quickbooks_invoice_id_final','reference_number_final','client_business','land_line_code','phone_code','zoho_date_status','client_module_id','trustee_id'], 'safe', 'on' => self::SCENARIO_DETAIL],
            [['prefix','contact_person_name','contact_person_lastname', 'contact_email','prefix_instructing_person', 'firstname_instructing_person','lastname_instructing_person'], 'string', 'max' => 255, 'on' => self::SCENARIO_DETAIL],
            ['contact_phone_no', 'match', 'pattern' => '/^\d{7}$/', 'message' => 'Please enter a valid 7-digit phone number.', 'on' => self::SCENARIO_DETAIL],
            ['land_line_no', 'match', 'pattern' => '/^\d{7}$/', 'message' => 'Please enter a valid 7-digit phone number.', 'on' => self::SCENARIO_DETAIL],
            ['contact_email', 'email', 'on' => self::SCENARIO_DETAIL],

            ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reference_number' => 'Reference Number',
            'client_id' => 'Client',
            'client_reference' => 'Client Reference',
            'client_name_passport' => 'Client Name (Passport)',
            'no_of_owners' => 'No Of Owners',
            'service_officer_name' => 'Service Officer Name',
            'instruction_date' => 'Instruction Date',
            'target_date' => 'Target Date',
            'building_info' => 'Building',
            'property_id' => 'Property',
            'property_category' => 'Property Category',
            'community' => 'Community',
            'sub_community' => 'Sub Community',
            'tenure' => 'Tenure',
            'unit_number' => 'Unit Number',
            'city' => 'City',
            'payment_plan' => 'Payment Plan',
            'status' => 'Status',
            'building_number' => 'Building Number',
            'plot_number' => 'Plot Number',
            'street' => 'Street',
            'floor_number' => 'Floor Number',
            //'instruction_person' => 'Instruction Person',
            'land_size' => 'Land Size',
            'purpose_of_valuation' => 'Purpose Of Valuation',
            'signature_img' => 'Add Report Signature Copy',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }

    public function beforeValidate()
    {
        if($this->client_id != 1){
            $this->client_fixed_fee_check = 1;
        }

        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    public static function getTotal($provider, $fieldName)
    {



        $total = 0;
        foreach ($provider as $item) {
           if($item[$fieldName] > 0) {
             //  echo $item[$fieldName].'<br>';

               $total +=  $item[$fieldName];
              // $total +=  (int)Yii::$app->appHelperFunctions->getClientRevenue($item[$fieldName]);
           }
        }
       // die;

        return $total;
    }
    public function getCompanyFeeStructure()
    {
        return $this->hasOne(CompanyFeeStructure::className(), ['company_id' => 'client_id']);
    }
    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Valuation';
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->reference_number;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Company::className(), ['id' => 'client_id']);
    }

    /**
 * @return \yii\db\ActiveQuery
 */
    public function getBuilding()
    {
        return $this->hasOne(Buildings::className(), ['id' => 'building_info']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Properties::className(), ['id' => 'property_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspectProperty()
    {
        return $this->hasOne(InspectProperty::className(), ['valuation_id' => 'id']);
    }
    public function getScheduleInspection(){
        return $this->hasOne(ScheduleInspection::className(),['valuation_id'=>'id']);
    }

    public function getCostDetails(){
        return $this->hasOne(CostDetails::className(),['valuation_id'=>'id']);
    }
    public function getValuationDetail(){
        return $this->hasOne(ValuationDetail::className(),['valuation_id'=>'id']);
    }

    public function getValuationConfiguration(){
        return $this->hasOne(ValuationConfiguration::className(),['valuation_id'=>'id']);
    }

    public function getValuationDeveloperMargin(){
        return $this->hasOne(ValuationDeveloperDriveMargin::className(),['valuation_id'=>'id']);
    }
    public function upload($image='', $folderName='')
    {
        $uploadObject = Yii::$app->get('s3bucket')->upload($folderName.'/images/'. $image->baseName . '.' . $image->extension,$image->tempName);
        if ($uploadObject) {
            // check if CDN host is available then upload and get cdn URL.
            if (Yii::$app->get('s3bucket')->cdnHostname) {
                $url = Yii::$app->get('s3bucket')->getCdnUrl($image );
            } else {
                $url = Yii::$app->get('s3bucket')->getUrl($folderName.'/images/'.$image );
            }
        }
        return true;
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'approval_id']);
    }

    public function getApprover()
    {
        return $this->hasOne(User::className(), ['id' => 'service_officer_name']);
    }
    public function getApproverData(){
        return $this->hasOne(ValuationApproversData::className(),['valuation_id'=>'id'])->andWhere(['approver_type'=>'approver']);
    }

    public function getReviewerData(){
        return $this->hasOne(ValuationApproversData::className(),['valuation_id'=>'id'])->andWhere(['approver_type'=>'reviewer']);
    }
    public function getValuerData(){
        return $this->hasOne(ValuationApproversData::className(),['valuation_id'=>'id'])->andWhere(['approver_type'=>'valuer']);
    }
    public function getUserbycreated()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    public function getValuationParent(){
        return $this->hasOne(Valuation::className(),['id'=>'parent_id']);
    }
    public function getcancelReasons(){
        return $this->hasMany(InspectionCancelReasonsLog::className(),['valuation_id'=>'id']);
    }
    public function getcancelReasonsone(){
        return $this->hasOne(InspectionCancelReasonsLog::className(),['valuation_id'=>'id']);
    }

    public function getQuotaionData(){
        return $this->hasOne(CrmQuotations::className(),['id'=>'quotation_id']);
    }


    public function afterSave($insert, $changedAttributes)
    {

        if($this->id > 2163 ) {

            // $this->valuation_status = 1;
            if(isset($this->quotation_id) && $this->quotation_id <> null && $this->parent_id < 1 && $this->id != 5275) {

            }else {
                $sequence = array_search($this->valuation_status, Yii::$app->appHelperFunctions->getStepsSequence(), true);
              //  $rateAndItemAmount = Yii::$app->appHelperFunctions->getRateAndItemAmount($this->client->id, $this->building->city, $this->inspection_type, $this->tenure, $this->property_id,$this->urgency);
                if($this->client_fixed_fee_check == 2 && $this->client_id== 1){
                    $rateAndItemAmount = $this->client->client_fixed_fee;
                }else {
                    if ($this->online_module_id <> null) {
                       // $rateAndItemAmount =2000;
                    }else{
                        $rateAndItemAmount = Yii::$app->appHelperFunctions->getRateAndItemAmount($this->client->id, $this->building->city, $this->inspection_type, $this->tenure, $this->property_id, $this->urgency);
                    }
                }
                $query2 = Yii::$app->db->createCommand()
                    ->update('valuation', ['fee' => $rateAndItemAmount], 'id = ' . $this->id . '')
                    ->execute();
                $query2 = Yii::$app->db->createCommand()
                    ->update('valuation', ['total_fee' => $rateAndItemAmount], 'id = ' . $this->id . '')
                    ->execute();
            }
            if ($sequence == 1 && $this->email_status !== 1) {
                // echo 'why is it in?';
                // die();
                $uid= $this->id;
                if(isset($this->quotation_id) && $this->quotation_id <> null){
                    $uid = 'crm'.$this->quotation_id;
                }
                $notifyData = [
                    'client' => $this->client,
                    'attachments' => [],
                    'subject' => $this->email_subject,
                    'uid' => $uid,
                    'valuer' => $this->approver->email,
                    'inspector' => $this->approver->email,
                    'replacements' => [
                        '{clientName}' => $this->client->title,
                    ],
                ];
               /* if($this->id == 5637){
                    echo "<pre>";
                    print_r($this->client_id );
                    print_r($this->client->primaryContact->email);
                    print_r($notifyData);
                    die;
                }*/

                  $allow_properties = [1,2,5,6,12,4,20,24,28,37,39,17];
               //  if (in_array($this->property_id, $allow_properties) && $this->client_id != 87) {
               // if(isset($this->quotation_id) && $this->quotation_id !=602) {
                     if($this->id == 5637 && $this->client_id != 120081) {
                         \app\modules\wisnotify\listners\NotifyEvent::firetest('Received.Valuation', $notifyData);
                     }else {
                         if($this->parent_id != 10161 && $this->quotation_id !=2507 && $this->client_id != 120081 && $this->online_module_id == '') {
                             \app\modules\wisnotify\listners\NotifyEvent::fire1('Received.Valuation', $notifyData);
                         }
                  //   }
               // }
                }
                //   UPDATE (table name, column values, condition)
                Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 1], 'id=' . $this->id . '')->execute();
                Yii::$app->db->createCommand()->update('valuation', ['email_status' => 1], 'id=' . $this->id . '')->execute();

            }
            if((isset($this->quotation_id) && $this->quotation_id <> null && $this->parent_id < 1 && $this->id != 5275)  ) {
               /* if(isset($this->quotation_id) && $this->quotation_id <> null){
                    $rateAndItemAmount = $this->fee;
                    $vat_amount = yii::$app->quotationHelperFunctions->getVatTotal_add( $this->fee);

                    //  if($this->client_id == 1) {
                    if($this->client_id == 1) {
                        $rateAndItemAmount = $rateAndItemAmount - $vat_amount;
                    }

                }*/
            }else {
                if ($this->client_fixed_fee_check == 2 && $this->client_id == 1) {
                    $rateAndItemAmount = $this->client->client_fixed_fee;
                } else {
                    if ($this->online_module_id <> null) {
                       // $rateAndItemAmount =2000;
                    }else{
                        $rateAndItemAmount = Yii::$app->appHelperFunctions->getRateAndItemAmount($this->client->id, $this->building->city, $this->inspection_type, $this->tenure, $this->property_id, $this->urgency);
                    }
                }
                // $rateAndItemAmount = Yii::$app->appHelperFunctions->getRateAndItemAmount($this->client->id, $this->building->city, $this->inspection_type, $this->tenure, $this->property_id,$this->urgency);
                $query2 = Yii::$app->db->createCommand()
                    ->update('valuation', ['fee' => $rateAndItemAmount], 'id = ' . $this->id . '')
                    ->execute();
                $query2 = Yii::$app->db->createCommand()
                    ->update('valuation', ['total_fee' => $rateAndItemAmount], 'id = ' . $this->id . '')
                    ->execute();
            }
        }
        if($this->proceed==0 && $this->valuation_status == 13){
            Yii::$app->db->createCommand()
                ->update('valuation', ['valuation_status' => 3], 'id = ' . $this->id . '')
                ->execute();
        }

        parent::afterSave($insert, $changedAttributes);
    }
    public function getKmImages()
    {
        return $this->hasMany(MeetingImages::class, ['meeting_id' => 'id'])->where(['image_type'=>'val_image']);
    }

    public function sendValuationFeedbackFormMail($feedback_key, $client, $valuation){


        $feedback_url = 'https://maxima.windmillsgroup.com/feedback/submit/?k='.$feedback_key;
        $feedback_form = '';
        $feedback_form .= '<p><br/><span style="padding-top:10px; padding-bottom:10px"><a href="'.$feedback_url.'" target="_black" style="padding-left:10px; padding-right:10px; padding-top:15px; padding-bottom:15px; text-align: center; background:#007BFF; color:#ffffff; text-decoration:none; font-wight:bold; font-size:14px" >';
        $feedback_form .= 'Submit Feedback';
        $feedback_form .= '</a></span><br/><br/></p>';
        $feedback_form .= '';



        $notifyData = [
            'client' => $this->client,
            'attachments' => [],
            'subject' => 'Tell us how happy you are',
            'valuation' => $valuation,
            'uid' => Yii::$app->user->identity->id,
            'replacements' => [
                // '{feedback_key}' => $feedback_key,
                '{feedback_form}' => $feedback_form,
            ],
        ];


        \app\modules\wisnotify\listners\NotifyEvent::fireFeedback('Valuation.Feedback.Form', $notifyData);

    }

    public function sendValuationFeedbackFormSms($feedback_key, $valuation){

        $feedback_url = 'https://maxima.windmillsgroup.com/feedback/submit/?k='.$feedback_key;

        if ($valuation->contact_phone_no != '') {
            $str = ltrim($valuation->phone_code, '0');
            $contact_number = $str . $valuation->contact_phone_no;
            $contact_number = '971' . $contact_number;
            /* echo $contact_number;
             die;*/

            // if ($valuation->client->client_type != 'bank') {

                $numbers = array();
                // $numbers[] = '971522924832';
                $numbers[] = $contact_number;

                foreach ($numbers as $number) {
                    $text = "Dear Sir / Madam,
  It has been our pleasure valuing for you. We would greatly appreciate it if you could spare a few moments to provide your feedback through the following link:
                    
                    ".$feedback_url."
                    
                    Thank you.
                                ";


                    $ch = curl_init();


                    curl_setopt($ch, CURLOPT_URL, "https://restapi.smscountry.com/v0.1/Accounts/GSzAf5xi3QzofjOMtlkl/SMSes/");
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, FALSE);
                    curl_setopt($ch, CURLOPT_HEADER, FALSE);

                    curl_setopt($ch, CURLOPT_POST, TRUE);

                    curl_setopt($ch, CURLOPT_POSTFIELDS, "{
                        \"Text\":\"$text\",
                        \"Number\":\"$number\",
                        \"SenderId\": \"WINDMILLS\",
                        
                        \"DRNotifyHttpMethod\": \"POST\",
                        \"Tool\": \"API\"
                    }");

                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            "Content-Type: application/json",
                            'Authorization: Basic ' . base64_encode("GSzAf5xi3QzofjOMtlkl:m82nK16BVJ67vFfulSxosIfhlgwEJgx2XYaBUPHm")

                        )
                    );

                    $response = curl_exec($ch);

                    // Yii::$app->db->createCommand()->update('feedback', ['sms_status' => 1], 'id=' . $feedback->id . '')->execute();
                    // Yii::$app->db->createCommand()->update('feedback', ['sms_status' => 1], 'id=' . $quotation->id . '')->execute();
                    /*echo "<pre>";
                    print_r($response);
                    die;*/
                    curl_close($ch);

                    //var_dump($response);
                }

            // }
        }
    }

    public function beforeSave($insert){


        // verify status reset after edit only something changed
        $isChanged = false;
        $ignoredAttributes = ['updated_at','status_verified_at']; 
        foreach ($this->attributes as $attribute => $value) {
            if (in_array($attribute, $ignoredAttributes)) {
                continue;
            }
            if ($this->getOldAttribute($attribute) != $value) {
                $isChanged = true;
                break;
            }
        }
        if ($isChanged) {
            if($this->status == 1 && $this->getOldAttribute('status') == 1){
                $this->status = 2;
            }elseif ($this->status == 2 && $this->getOldAttribute('status') == 1) {
                $this->status = 1;
            }
        }
        
        return parent::beforeSave($insert);
    }

}
