<?php

namespace app\models;
use yii\db\ActiveRecord;

use Yii;

class QuotationStatusHistory extends ActiveRecord
{

	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%quotation_status_history}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['quotation_id','status_id','date'],'safe'],
		];
	}


}
