<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proposal_docs".
 *
 * @property int $id
 * @property int|null $quotation_id
 * @property int|null $property_id
 * @property int|null $document_id
 * @property string|null $attachment
 */
class ProposalDocs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proposal_docs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quotation_id', 'property_id', 'document_id','building_id'], 'integer'],
            [['attachment'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'quotation_id' => 'Quotation ID',
            'property_id' => 'Property ID',
            'document_id' => 'Document ID',
            'attachment' => 'Attachment',
        ];
    }
}
