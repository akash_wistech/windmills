<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "uae_list_data".
 *
 * @property int $id
 * @property string|null $listings_reference
 * @property string|null $source
 * @property string|null $listing_website_link
 * @property string|null $listing_date
 * @property string|null $building_info
 * @property string|null $community
 * @property string|null $sub_community
 * @property string|null $property_type
 * @property string|null $city_id
 * @property string|null $property_category
 * @property string|null $no_of_bedrooms
 * @property string|null $no_of_bathrooms
 * @property string|null $built_up_area
 * @property string|null $land_size
 * @property string|null $listings_price
 * @property string|null $listings_rent
 * @property string|null $final_price
 * @property string|null $status
 */
class UaeListData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'uae_list_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['listings_reference', 'source', 'listing_website_link', 'listing_date', 'building_info', 'community', 'sub_community', 'property_type', 'city_id', 'property_category', 'no_of_bedrooms', 'no_of_bathrooms', 'built_up_area', 'land_size', 'listings_price', 'listings_rent', 'final_price', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'listings_reference' => 'Listings Reference',
            'source' => 'Source',
            'listing_website_link' => 'Listing Website Link',
            'listing_date' => 'Listing Date',
            'building_info' => 'Building Info',
            'community' => 'Community',
            'sub_community' => 'Sub Community',
            'property_type' => 'Property Type',
            'city_id' => 'City ID',
            'property_category' => 'Property Category',
            'no_of_bedrooms' => 'No Of Bedrooms',
            'no_of_bathrooms' => 'No Of Bathrooms',
            'built_up_area' => 'Built Up Area',
            'land_size' => 'Land Size',
            'listings_price' => 'Listings Price',
            'listings_rent' => 'Listings Rent',
            'final_price' => 'Final Price',
            'status' => 'Status',
        ];
    }
}
