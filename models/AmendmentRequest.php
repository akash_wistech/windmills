<?php

namespace app\models;

use Yii;
use app\components\traits\AmendmentsTraits;

/**
 * This is the model class for table "{{%amendment_request}}".
 *
 * @property int $id
 * @property string|null $date
 * @property string|null $deadline
 * @property string|null $completion_date
 * @property int|null $applicant
 * @property string|null $change_explanation
 * @property int|null $vat
 * @property int|null $under_scope_of_agreement
 * @property int|null $number_of_hours_expected
 * @property int|null $priority:
 * @property string|null $reason_of_change
 * @property int|null $verify
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 */
class AmendmentRequest extends \yii\db\ActiveRecord
{
  use AmendmentsTraits;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%amendment_request}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'deadline', 'completion_date', 'start_date', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['applicant', 'under_scope_of_agreement', 'number_of_hours_expected', 'priority', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['reason_of_change','reference_no'], 'string'],
            [['change_explanation'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date' => Yii::t('app', 'Date'),
            'deadline' => Yii::t('app', 'Deadline'),
            'completion_date' => Yii::t('app', 'Verify Completion Date'),
            'applicant' => Yii::t('app', 'Applicant'),
            'change_explanation' => Yii::t('app', 'Change Explanation'),
            'under_scope_of_agreement' => Yii::t('app', 'Under Scope Of Agreement'),
            'number_of_hours_expected' => Yii::t('app', 'Number Of Hours Expected'),
            'priority' => Yii::t('app', 'Priority'),
            'reason_of_change' => Yii::t('app', 'Reason Of Change'),
            'start_date' => Yii::t('app', 'Verify Start Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return AmendmentRequestQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AmendmentRequestQuery(get_called_class());
    }
}
