<?php 
// models/ScopeOfServices.php

namespace app\models;

use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "scope_of_service".
 *
 * @property int $id
 * @property string|null $title
 * @property int|null $status_verified
 * @property string|null $status_verified_at
 * @property int|null $status_verified_by
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property string|null $deleted_at
 * @property int|null $deleted_by
 */

class ScopeOfService extends ActiveRecordFull
{
    public static function tableName()
    {
        return 'scope_of_service';
    }

    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
            [['status_verified', 'status_verified_by', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['status_verified_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['title','status_verified'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'status_verified' => 'Verification',
            'status_verified_at' => 'Status Verified At',
            'status_verified_by' => 'Status Verified By',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
        ];
    }

    public function beforeSave($insert){

        // verify status reset after edit only something changed
        $isChanged = false;
        $ignoredAttributes = ['updated_at','status_verified_at']; 
        foreach ($this->attributes as $attribute => $value) {
            if (in_array($attribute, $ignoredAttributes)) {
                continue;
            }
            if ($this->getOldAttribute($attribute) != $value) {
                $isChanged = true;
                break;
            }
        }
        if ($isChanged) {
            if($this->status_verified == 1 && $this->getOldAttribute('status_verified') == 1){
                $this->status_verified = 2;
            }elseif ($this->status_verified == 2 && $this->getOldAttribute('status_verified') == 1) {
                $this->status_verified = 1;
            }
        }

        // verify status reset after edit
        // if($this->getOldAttribute('status_verified') == 1){
        //     $this->status_verified = 2;
        // }
        
        return parent::beforeSave($insert);
    }
}
