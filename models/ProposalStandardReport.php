<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proposal_standard_report".
 *
 * @property int $id
 * @property string|null $payment_method
 * @property string|null $timings
 * @property string|null $rics_valuation_standards_departures
 * @property string|null $valuer_duties_supervision
 * @property string|null $internal_external_status_of_the_valuer
 * @property string|null $previous_involvement_and_conflict_of_interest
 * @property string|null $decleration_of_independence_and_objectivity
 * @property string|null $report_handover
 * @property string|null $currency
 * @property string|null $basis_of_value
 * @property string|null $market_value
 * @property string|null $statutory_definition_of_market_value
 * @property string|null $market_rent
 * @property string|null $investment_value
 * @property string|null $fair_value
 * @property string|null $valuation_date
 * @property string|null $property
 * @property string|null $assumptions_ext_of_investi_limi_scope_of_work
 * @property string|null $physical_inspection
 * @property string|null $desktop_or_driven_by_valuation
 * @property string|null $structural_and_technical_survey
 * @property string|null $conditions_state_repair_maintenance_property
 * @property string|null $contamination_ground_environmental_consideration
 * @property string|null $statutory_and_regulatory_requirements
 * @property string|null $title_tenancies_and_property_document
 * @property string|null $planning_and_highway_enquiries
 * @property string|null $plant_and_equipment
 * @property string|null $development_properties
 * @property string|null $disposal_costs_and_liabilities
 * @property string|null $insurance_reinstalement_cost
 * @property string|null $nature_source_information_documents_relied_upon
 * @property string|null $description_of_report
 * @property string|null $client_acceptance_of_the_valuation_report
 * @property string|null $restrictions_on_publications
 * @property string|null $third_party_liability
 * @property string|null $valuation_uncertaninty
 * @property string|null $complaints
 * @property string|null $rics_monitoring
 * @property string|null $rera_monitoring
 * @property string|null $limitions_on_liabity
 * @property string|null $liability_and_duty_of_care
 * @property string|null $extent_of_fee
 * @property string|null $indemnification
 * @property string|null $professional_indemmnity_insurance
 * @property string|null $clients_obligations
 * @property string|null $proposal_validity
 * @property string|null $jurisdiction
 * @property string|null $acceptance_of_terms_of_engagement
 */
class ProposalStandardReport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proposal_standard_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['payment_method', 'timings', 'rics_valuation_standards_departures', 'valuer_duties_supervision', 'internal_external_status_of_the_valuer', 'previous_involvement_and_conflict_of_interest', 'decleration_of_independence_and_objectivity', 'report_handover', 'currency', 'basis_of_value', 'market_value', 'statutory_definition_of_market_value', 'market_rent', 'investment_value', 'fair_value', 'valuation_date', 'property', 'assumptions_ext_of_investi_limi_scope_of_work', 'physical_inspection', 'desktop_or_driven_by_valuation', 'structural_and_technical_survey', 'conditions_state_repair_maintenance_property', 'contamination_ground_environmental_consideration', 'statutory_and_regulatory_requirements', 'title_tenancies_and_property_document', 'planning_and_highway_enquiries', 'plant_and_equipment', 'development_properties', 'disposal_costs_and_liabilities', 'insurance_reinstalement_cost', 'nature_source_information_documents_relied_upon', 'description_of_report', 'client_acceptance_of_the_valuation_report', 'restrictions_on_publications', 'third_party_liability', 'valuation_uncertaninty', 'complaints', 'rics_monitoring', 'rera_monitoring', 'limitions_on_liabity', 'liability_and_duty_of_care', 'extent_of_fee', 'indemnification', 'professional_indemmnity_insurance', 'clients_obligations', 'proposal_validity', 'jurisdiction', 'acceptance_of_terms_of_engagement','firstpage_content','firstpage_footer','quotation_last_paragraph',
                'valuation_approaches','income_approach','profit_approach','cost_approach'], 'safe'],
                [['status_verified','status_verified_at','status_verified_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_method' => 'Payment Method',
            'timings' => 'Timings',
            'rics_valuation_standards_departures' => 'Rics Valuation Standards Departures',
            'valuer_duties_supervision' => 'Valuer Duties Supervision',
            'internal_external_status_of_the_valuer' => 'Internal External Status Of The Valuer',
            'previous_involvement_and_conflict_of_interest' => 'Previous Involvement And Conflict Of Interest',
            'decleration_of_independence_and_objectivity' => 'Decleration Of Independence And Objectivity',
            'report_handover' => 'Report Handover',
            'currency' => 'Currency',
            'basis_of_value' => 'Basis Of Value',
            'market_value' => 'Market Value',
            'statutory_definition_of_market_value' => 'Statutory Definition Of Market Value',
            'market_rent' => 'Market Rent',
            'investment_value' => 'Investment Value',
            'fair_value' => 'Fair Value',
            'valuation_date' => 'Valuation Date',
            'property' => 'Property',
            'assumptions_ext_of_investi_limi_scope_of_work' => 'Assumptions Ext Of Investi Limi Scope Of Work',
            'physical_inspection' => 'Physical Inspection',
            'desktop_or_driven_by_valuation' => 'Desktop Or Driven By Valuation',
            'structural_and_technical_survey' => 'Structural And Technical Survey',
            'conditions_state_repair_maintenance_property' => 'Conditions State Repair Maintenance Property',
            'contamination_ground_environmental_consideration' => 'Contamination Ground Environmental Consideration',
            'statutory_and_regulatory_requirements' => 'Statutory And Regulatory Requirements',
            'title_tenancies_and_property_document' => 'Title Tenancies And Property Document',
            'planning_and_highway_enquiries' => 'Planning And Highway Enquiries',
            'plant_and_equipment' => 'Plant And Equipment',
            'development_properties' => 'Development Properties',
            'disposal_costs_and_liabilities' => 'Disposal Costs And Liabilities',
            'insurance_reinstalement_cost' => 'Insurance Reinstalement Cost',
            'nature_source_information_documents_relied_upon' => 'Nature Source Information Documents Relied Upon',
            'description_of_report' => 'Description Of Report',
            'client_acceptance_of_the_valuation_report' => 'Client Acceptance Of The Valuation Report',
            'restrictions_on_publications' => 'Restrictions On Publications',
            'third_party_liability' => 'Third Party Liability',
            'valuation_uncertaninty' => 'Valuation Uncertaninty',
            'complaints' => 'Complaints',
            'rics_monitoring' => 'Rics Monitoring',
            'rera_monitoring' => 'Rera Monitoring',
            'limitions_on_liabity' => 'Limitions On Liabity',
            'liability_and_duty_of_care' => 'Liability And Duty Of Care',
            'extent_of_fee' => 'Extent Of Fee',
            'indemnification' => 'Indemnification',
            'professional_indemmnity_insurance' => 'Professional Indemmnity Insurance',
            'clients_obligations' => 'Clients Obligations',
            'proposal_validity' => 'Proposal Validity',
            'jurisdiction' => 'Jurisdiction',
            'acceptance_of_terms_of_engagement' => 'Acceptance Of Terms Of Engagement',
        ];
    }

    public function beforeSave($insert){

        
        // verify status reset after edit only something changed
        $isChanged = false;
        $ignoredAttributes = ['updated_at','status_verified_at']; 
        foreach ($this->attributes as $attribute => $value) {
            if (in_array($attribute, $ignoredAttributes)) {
                continue;
            }
            if ($this->getOldAttribute($attribute) != $value) {
                $isChanged = true;
                break;
            }
        }
        if ($isChanged) {
            if($this->status_verified == 1 && $this->getOldAttribute('status_verified') == 1){
                $this->status_verified = 2;
            }elseif ($this->status_verified == 2 && $this->getOldAttribute('status_verified') == 1) {
                $this->status_verified = 1;
            }
        }
        
        return parent::beforeSave($insert);
    }
}
