<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
* ContactForm is the model behind the contact form.
*/
class ProfileForm extends Model
{
  public $firstname,$lastname,$mobile,$email,$job_title,$job_title_id,$department_name,$department_id,$phone1,$phone2;
  public $address,$country_id,$zone_id,$city,$postal_code;

  public $image,$imageFile,$oldImage;
  public $allowedImageSize = 1048576;
  public $allowedImageTypes=['jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'];

  /**
  * @param  string                          $token
  * @param  array                           $config name-value pairs that will be used to initialize the object properties
  * @throws \yii\base\InvalidParamException if token is empty or not valid
  */
  public function __construct($config = [])
  {
    $user = Yii::$app->user->identity;
    $this->firstname = $user->firstname;
    $this->lastname = $user->lastname;
    $this->email = $user->email;
    $this->mobile = $user->profileInfo->mobile;
    $this->job_title = $user->profileInfo->jobTitle!=null ? $user->profileInfo->jobTitle->title : '';
    $this->job_title_id = $user->profileInfo->job_title_id;
    $this->department_name = $user->profileInfo->department!=null ? $user->profileInfo->department->title : '';
    $this->department_id = $user->profileInfo->department_id;
    $this->phone1 = $user->profileInfo->phone1;
    $this->phone2 = $user->profileInfo->phone2;
    $this->address = $user->profileInfo->address;
    $this->country_id = $user->profileInfo->country_id;
    $this->zone_id = $user->profileInfo->zone_id;
    $this->city = $user->profileInfo->city;
    $this->postal_code = $user->profileInfo->postal_code;

    $this->image = $user->image;
    $this->oldImage = $user->image;
    parent::__construct($config);
  }

  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['firstname','lastname','mobile','email'],'required'],
      [['firstname','lastname','email','job_title','department_name','address','city','postal_code'],'string'],
      ['email','email'],
      [['mobile','job_title_id','department_id','phone1','phone2','country_id','zone_id'],'integer'],
      [['firstname','lastname','email','mobile','job_title','department_name','phone1','phone2','city','postal_code'],'trim'],
      [['image'], 'file', 'extensions'=>implode(",",$this->allowedImageTypes), 'maxSize' => $this->allowedImageSize, 'tooBig' => Yii::t('app','Image is too big, Maximum allowed size is 1MB')]
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'firstname' => 'First Name',
      'lastname' => 'Last Name',
      'phone1' => 'Phone 1',
      'phone2' => 'Phone 2',
      'country_id' => 'Country',
      'zone_id' => 'State / Province',
      'image' => 'Profile Photo',
    ];
  }

  /**
  * @inheritdoc
  */
  public function beforeValidate()
  {
    //Uploading Logo
    if(UploadedFile::getInstance($this, 'image')){
      $this->imageFile = UploadedFile::getInstance($this, 'image');
      // if no file was uploaded abort the upload
      if (!empty($this->imageFile)) {
        $pInfo=pathinfo($this->imageFile->name);
        $ext = $pInfo['extension'];
        if (in_array($ext,$this->allowedImageTypes)) {
          // Check to see if any PHP files are trying to be uploaded
          $content = file_get_contents($this->imageFile->tempName);
          if (preg_match('/\<\?php/i', $content)) {
            $this->addError('image', Yii::t('app', 'Invalid file provided!'));
            return false;
          }else{
            if (filesize($this->imageFile->tempName) <= $this->allowedImageSize) {
              // generate a unique file name
              $this->image = Yii::$app->fileHelperFunctions->generateName().".{$ext}";
            }else{
              $this->addError('image', Yii::t('app', 'File size is too big!'));
              return false;
            }
          }
        }else{
          $this->addError('image', Yii::t('app', 'Invalid file provided!'));
          return false;
        }
      }
    }
    if($this->image==null && $this->oldImage!=null){
      $this->image=$this->oldImage;
    }
    return parent::beforeValidate();
  }

  /**
  * Save Profile Info
  */
  public function save()
  {
    if ($this->validate()) {
      $user=User::findOne(Yii::$app->user->identity->id);
      $user->firstname=$this->firstname;
      $user->lastname=$this->lastname;
      $user->email=$this->email;
      $user->mobile=$this->mobile;
      $user->job_title=$this->job_title;
      $user->job_title_id=$this->job_title_id;
      $user->department_name=$this->department_name;
      $user->department_id=$this->department_id;
      $user->phone1=$this->phone1;
      $user->phone2=$this->phone2;
      $user->address=$this->address;
      $user->country_id=$this->country_id;
      $user->zone_id=$this->zone_id;
      $user->city=$this->city;
      $user->image=$this->image;

      if ($this->imageFile!== null && $this->image!=null) {
        if($this->oldImage!=null && $this->image!=$this->oldImage && file_exists(Yii::$app->params['member_uploads_abs_path'].$this->oldImage)){
          unlink(Yii::$app->params['member_uploads_abs_path'].$this->oldImage);
        }
        $this->imageFile->saveAs(Yii::$app->params['member_uploads_abs_path'].$this->image);
      }

      $user->save();

      ActivityLog::add(ActivityLog::LOG_STATUS_SUCCESS, ''.Yii::$app->user->identity->name.' updated profile');
      return true;
    }
    return false;
  }
}
