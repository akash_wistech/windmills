<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "vone_valuations".
 *
 * @property int $id
 * @property int|null $wm_reference
 * @property int|null $reason
 * @property int|null $client_requirements
 * @property int|null $status_verified
 * @property int|null $status_verified_at
 * @property int|null $status_verified_by
 * @property int|null $created_at
 * @property int|null $created_by
 * @property int|null $updated_at
 * @property int|null $updated_by
 * @property int|null $deleted_at
 * @property int|null $deleted_by
 */
class VoneValuations extends ActiveRecordFull
{
    
    public static function tableName()
    {
        return 'vone_valuations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['wm_reference', 'reason', 'error_status'], 'required'],
            [['client_requirements', 'status_verified', 'status_verified_at', 
            'status_verified_by', 'created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 
            'deleted_by','client_reference','client_reference','valuation_id','grid_view_id','email_status'], 'safe'],
        ];
    }


    public function getValuation()
    {
        return $this->hasOne(Valuation::className(), ['id' => 'valuation_id']);
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'wm_reference' => 'Wm Reference',
            'reason' => 'Reason',
            'client_requirements' => 'Client Requirements',
            'status_verified' => 'Status Verified',
            'status_verified_at' => 'Status Verified At',
            'status_verified_by' => 'Status Verified By',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
        ];
    }
}
