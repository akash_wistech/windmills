<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%action_log_event_info}}".
*
* @property integer $action_log_id
* @property string $start_date
* @property string $end_date
* @property string $color_code
* @property integer $all_day
* @property integer $sub_type
* @property integer $status
*/
class ActionLogEventInfo extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%action_log_event_info}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['action_log_id'], 'required'],
      [['action_log_id', 'all_day', 'sub_type', 'status'], 'integer'],
      [['start_date', 'end_date', 'color_code'], 'string'],
    ];
  }

  public static function primaryKey()
  {
  	return ['action_log_id'];
  }
}
