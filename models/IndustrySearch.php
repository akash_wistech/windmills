<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Industry;

/**
 * IndustrySearch represents the model behind the search form of `app\models\Industry`.
 */
class IndustrySearch extends Industry
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status_verified', 'status_verified_by', 'created_by', 'updated_by', 'trashed_by'], 'integer'],
            [['title','status_verified_at','status_verified_by', 'created_at', 'updated_at', 'trashed_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Industry::find()->orderBy(['title' => SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            // 'id' => $this->id,
            'status_verified' => $this->status_verified,
            // 'status_verified_at' => $this->status_verified_at,
            // 'status_verified_by' => $this->status_verified_by,
            // 'created_at' => $this->created_at,
            // 'created_by' => $this->created_by,
            // 'updated_at' => $this->updated_at,
            // 'updated_by' => $this->updated_by,
            // 'trashed_at' => $this->trashed_at,
            // 'trashed_by' => $this->trashed_by,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
