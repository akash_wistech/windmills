<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client_emails_settings".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $email
 * @property int $client_id
 */
class ClientEmailsSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_emails_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id'], 'required'],
            [['client_id'], 'integer'],
            [['name', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'client_id' => 'Client ID',
        ];
    }
}
