<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Workflow;

/**
* WorkflowSearch represents the model behind the search form of `app\models\Workflow`.
*/
class WorkflowSearch extends Workflow
{
  public $module_id,$stage_name,$service_type_id,$pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','module_id','is_financial','service_type_id','completion_day','created_by','updated_by','pageSize'],'integer'],
      [['title','stage_name','financial_module','created_at'],'string'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $query = $this->generateQuery($params);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
      'sort' => false,//$this->defaultSorting,
    ]);

    return $dataProvider;
  }

  /**
  * Generates query for the search
  *
  * @param array $params
  *
  * @return ActiveRecord
  */
  public function generateQuery($params)
  {
    $this->load($params);
    $query = Workflow::find();

    // grid filtering conditions
    $query->andFilterWhere([
      Workflow::tableName().'.id' => $this->id,
      Workflow::tableName().'.completion_day' => $this->completion_day,
      Workflow::tableName().'.trashed' => 0,
    ]);
    if($this->module_id!=null){
      $subQueryModules=WorkflowModule::find()->select(['workflow_id'])->where(['module_type'=>$this->module_id]);
      $query->andFilterWhere([Workflow::tableName().'.id' => $subQueryModules]);
    }
    if($this->service_type_id!=null){
      $subQueryServices=WorkflowServiceType::find()->select(['workflow_id'])->where(['service_type'=>$this->service_type_id]);
      $query->andFilterWhere([Workflow::tableName().'.id' => $subQueryServices]);
    }
    if($this->stage_name!=null){
      $subQueryStages=WorkflowStage::find()->select(['workflow_id'])->where(['like',WorkflowStage::tableName().'.title',$this->stage_name]);
      $query->andFilterWhere([Workflow::tableName().'.id' => $subQueryStages]);
    }

    $query->andFilterWhere(['like',Workflow::tableName().'.title',$this->title]);

    return $query;
  }

  /**
  * Default Sorting Options
  *
  * @param array $params
  *
  * @return Array
  */
  public function getDefaultSorting()
  {
    return [
      'defaultOrder' => ['created_at'=>SORT_DESC],
      'attributes' => [
        'title',
        'completion_day',
        'created_at',
      ]
    ];
  }
}
