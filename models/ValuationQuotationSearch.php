<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ValuationQuotation;

/**
 * ValuationQuotationSearch represents the model behind the search form of `app\models\ValuationQuotation`.
 */
class ValuationQuotationSearch extends ValuationQuotation
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'reference_number', 'no_of_properties', 'recommended_fee', 'final_fee_approved', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['client_name', 'client_customer_name', 'inquiry_date', 'expiry_date', 'purpose_of_valuation', 'turn_around_time', 'valuer_name', 'date', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ValuationQuotation::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'reference_number' => $this->reference_number,
            'inquiry_date' => $this->inquiry_date,
            'expiry_date' => $this->expiry_date,
            'no_of_properties' => $this->no_of_properties,
            'recommended_fee' => $this->recommended_fee,
            'final_fee_approved' => $this->final_fee_approved,
            'date' => $this->date,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'client_name', $this->client_name])
            ->andFilterWhere(['like', 'client_customer_name', $this->client_customer_name])
            ->andFilterWhere(['like', 'purpose_of_valuation', $this->purpose_of_valuation])
            ->andFilterWhere(['like', 'turn_around_time', $this->turn_around_time])
            ->andFilterWhere(['like', 'valuer_name', $this->valuer_name]);

        return $dataProvider;
    }
}
