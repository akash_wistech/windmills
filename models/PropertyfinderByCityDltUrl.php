<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "propertyfinder_by_city_dlt_url".
*
* @property int $id
* @property int|null $city_id
* @property string|null $url
* @property int|null $status
* @property string|null $purpose
*/
class PropertyfinderByCityDltUrl extends \yii\db\ActiveRecord
{
    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'propertyfinder_by_city_dlt_url';
    }
    
    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['city_id', 'status'], 'integer'],
            [['purpose'], 'string', 'max' => 255],
            [['url', ], 'safe'],
        ];
    }
    
    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'url' => 'Url',
            'status' => 'Status',
            'purpose' => 'Purpose',
        ];
    }
}
