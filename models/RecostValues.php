<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recost_values".
 *
 * @property int $id
 * @property float $demolition_costs_factor
 * @property float $debris_removal_factor
 * @property float $contracting_cost_factor
 * @property float $basement_parking_factor
 * @property float $multi_story_parking_factor
 * @property float $boundry_wall_length_factor
 * @property float $site_improvements_factor
 * @property float $pool_price_factor
 * @property float $whitegoods_price_factor
 * @property float|null $landscape_price_factor
 * @property float $land_department_municipality_fee_factor
 * @property float|null $developer_margin_factor
 * @property float|null $green_building_certification_fee_factor
 * @property float|null $demolition_fee_factor
 * @property float|null $professional_charges_factor
 * @property float|null $designing_and_architecture_costs_factor
 * @property float|null $project_management_costs_factor
 * @property float|null $interest_rate_factor
 * @property float|null $contingency_margin_factor
 * @property float|null $demolition_costs_values
 * @property float|null $debris_removal_values
 * @property float|null $contracting_cost_values
 * @property float|null $basement_parking_values
 * @property float|null $multi_story_parking_values
 * @property float|null $boundry_wall_length_values
 * @property float|null $site_improvements_values
 * @property float|null $pool_price_values
 * @property float|null $whitegoods_price_values
 * @property float|null $landscape_price_values
 * @property float|null $land_department_municipality_fee_values
 * @property float|null $developer_margin_values
 * @property float|null $green_building_certification_fee_values
 * @property float|null $demolition_fee_values
 * @property float|null $professional_charges_values
 * @property float|null $designing_and_architecture_costs_values
 * @property float|null $project_management_costs_values
 * @property float|null $interest_rate_values
 * @property float|null $contingency_margin_values
 * @property int $valuation_id
 * @property int $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class RecostValues extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recost_values';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['demolition_costs_factor', 'debris_removal_factor', 'contracting_cost_factor', 'basement_parking_factor', 'multi_story_parking_factor', 'boundry_wall_length_factor', 'site_improvements_factor', 'pool_price_factor', 'whitegoods_price_factor', 'landscape_price_factor', 'land_department_municipality_fee_factor', 'developer_margin_factor', 'green_building_certification_fee_factor', 'demolition_fee_factor', 'professional_charges_factor', 'designing_and_architecture_costs_factor', 'project_management_costs_factor', 'interest_rate_factor', 'contingency_margin_factor', 'demolition_costs_values', 'debris_removal_values', 'contracting_cost_values', 'basement_parking_values', 'multi_story_parking_values', 'boundry_wall_length_values', 'site_improvements_values', 'pool_price_values', 'whitegoods_price_values', 'landscape_price_values', 'land_department_municipality_fee_values', 'developer_margin_values', 'green_building_certification_fee_values', 'demolition_fee_values', 'professional_charges_values', 'designing_and_architecture_costs_values', 'project_management_costs_values', 'interest_rate_values', 'contingency_margin_values'], 'number'],
            [['demolition_costs_factor', 'debris_removal_factor', 'contracting_cost_factor', 'basement_parking_factor', 'multi_story_parking_factor', 'boundry_wall_length_factor', 'site_improvements_factor', 'pool_price_factor', 'whitegoods_price_factor', 'landscape_price_factor', 'land_department_municipality_fee_factor', 'developer_margin_factor', 'green_building_certification_fee_factor', 'demolition_fee_factor', 'professional_charges_factor', 'designing_and_architecture_costs_factor', 'project_management_costs_factor', 'interest_rate_factor', 'contingency_margin_factor', 'demolition_costs_values', 'debris_removal_values', 'contracting_cost_values', 'basement_parking_values', 'multi_story_parking_values', 'boundry_wall_length_values', 'site_improvements_values', 'pool_price_values', 'whitegoods_price_values', 'landscape_price_values', 'land_department_municipality_fee_values', 'developer_margin_values', 'green_building_certification_fee_values', 'demolition_fee_values', 'professional_charges_values', 'designing_and_architecture_costs_values', 'project_management_costs_values', 'interest_rate_values', 'contingency_margin_values'], 'safe'],
            [['valuation_id', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['valuation_id', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'safe'],
            [['created_at', 'updated_at', 'trashed_at','grand_total'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'demolition_costs_factor' => 'Demolition Costs Factor',
            'debris_removal_factor' => 'Debris Removal Factor',
            'contracting_cost_factor' => 'Contracting Cost Factor',
            'basement_parking_factor' => 'Basement Parking Factor',
            'multi_story_parking_factor' => 'Multi Story Parking Factor',
            'boundry_wall_length_factor' => 'Boundry Wall Length Factor',
            'site_improvements_factor' => 'Site Improvements Factor',
            'pool_price_factor' => 'Pool Price Factor',
            'whitegoods_price_factor' => 'Whitegoods Price Factor',
            'landscape_price_factor' => 'Landscape Price Factor',
            'land_department_municipality_fee_factor' => 'Land Department Municipality Fee Factor',
            'developer_margin_factor' => 'Developer Margin Factor',
            'green_building_certification_fee_factor' => 'Green Building Certification Fee Factor',
            'demolition_fee_factor' => 'Demolition Fee Factor',
            'professional_charges_factor' => 'Professional Charges Factor',
            'designing_and_architecture_costs_factor' => 'Designing And Architecture Costs Factor',
            'project_management_costs_factor' => 'Project Management Costs Factor',
            'interest_rate_factor' => 'Interest Rate Factor',
            'contingency_margin_factor' => 'Contingency Margin Factor',
            'demolition_costs_values' => 'Demolition Costs Values',
            'debris_removal_values' => 'Debris Removal Values',
            'contracting_cost_values' => 'Contracting Cost Values',
            'basement_parking_values' => 'Basement Parking Values',
            'multi_story_parking_values' => 'Multi Story Parking Values',
            'boundry_wall_length_values' => 'Boundry Wall Length Values',
            'site_improvements_values' => 'Site Improvements Values',
            'pool_price_values' => 'Pool Price Values',
            'whitegoods_price_values' => 'Whitegoods Price Values',
            'landscape_price_values' => 'Landscape Price Values',
            'land_department_municipality_fee_values' => 'Land Department Municipality Fee Values',
            'developer_margin_values' => 'Developer Margin Values',
            'green_building_certification_fee_values' => 'Green Building Certification Fee Values',
            'demolition_fee_values' => 'Demolition Fee Values',
            'professional_charges_values' => 'Professional Charges Values',
            'designing_and_architecture_costs_values' => 'Designing And Architecture Costs Values',
            'project_management_costs_values' => 'Project Management Costs Values',
            'interest_rate_values' => 'Interest Rate Values',
            'contingency_margin_values' => 'Contingency Margin Values',
            'valuation_id' => 'Valuation ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }
}
