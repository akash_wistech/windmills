<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "crm_received_docs_files".
 *
 * @property int $id
 * @property int $document_id
 * @property string|null $attachment
 * @property int|null $quotation_id
 */
class CrmReceivedDocsFiles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_received_docs_files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['document_id'], 'required'],
            [['document_id', 'quotation_id','property_index'], 'integer'],
            [['attachment'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'document_id' => 'Document ID',
            'attachment' => 'Attachment',
            'quotation_id' => 'Quotation ID',
        ];
    }
}
