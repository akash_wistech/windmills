<?php

namespace app\models;

use Yii;

class FetchBayutToRentLink extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'fetch_bayut_to_rent_link';
    }


    public function rules()
    {
        return [
            [['parent_link_id'], 'integer'],
            [['url_to_fetch', 'property_category'], 'string', 'max' => 255],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_link_id' => 'Parent Link ID',
            'url_to_fetch' => 'Url To Fetch',
            'property_category' => 'Property Category',
        ];
    }
}
