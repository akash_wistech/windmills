<?php
namespace app\models;
use yii\db\ActiveRecord;

use Yii;

class AutoMonthlyInvoiceNum extends ActiveRecord
{
	public static function tableName()
	{
		return '{{%auto_monthly_invoice_num}}';
	}
	
	public function rules()
	{
		return [
			[['type','module_type','client_id','client_current_inv_num','maxima_inv_num','year_inv_num','client_inv_no','year','month','city','created_at','created_by'],'safe'],
		];
	}
}
