<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
* This is the model class for table "{{%action_log}}".
*
* @property integer $id
* @property string $module_type
* @property integer $module_id
* @property string $rec_type
* @property string $comments
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
* @property integer $trashed
* @property string $trashed_at
* @property integer $trashed_by
*/
class ActionLog extends ActiveRecordFull
{
	public $subject,$due_date,$calendar_id,$assign_to;
	public $hours,$minutes,$start_dt,$end_dt;
	public $attachment_file;
	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%action_log}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['module_type','module_id','rec_type'],'required'],
			[['created_at','updated_at','trashed_at'],'safe'],
			[['module_id','priority','visibility','calendar_id','hours','created_by','updated_by','trashed','trashed_by'],'integer'],
			[['module_type','rec_type','subject','comments','due_date','minutes','start_dt','end_dt'],'string'],
			[['visibility'],'default','value'=>0],
			[['comments'],'trim'],
			[['assign_to'],'each','rule'=>['integer']],
      [['attachment_file'],'each','rule'=>['string']],
		];
	}

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'title' => Yii::t('app', 'Title'),
		];
	}

	/**
	* returns main title/name of row for status and delete loging
	*/
	public function getRecTitle()
	{
		$title='';
		$actionInfo=$this->actionInfo;
		if($actionInfo!=null){
			$title=$actionInfo['subject'];
		}else{
			$title=$this->comments;
		}
		return $title;
	}

	/**
	* returns main title/name of row for status and delete loging
	*/
	public function getRecType()
	{
		return 'Action Log';
	}

	/**
	* returns subject/due date info if any
	*/
	public function getActionInfo()
	{
		$str=[];
		$sbjRow=ActionLogSubject::find()->select(['subject','due_date','status'])->where(['action_log_id'=>$this->id])->asArray()->one();
		if($sbjRow!=null)$str=['subject'=>$sbjRow['subject'],'due_date'=>$sbjRow['due_date'],'status'=>$sbjRow['status']];
		return $str;
	}

	/**
	* returns reminder info if any
	*/
	public function getReminderInfo()
	{
		$arr=[];
		$reminderRow=ActionLogReminder::find()->select(['notify_to','notify_time'])->where(['action_log_id'=>$this->id])->asArray()->one();
		if($reminderRow!=null)$arr=['notify_to'=>$reminderRow['notify_to'],'notify_time'=>$reminderRow['notify_time']];
		return $arr;
	}

	/**
	* returns timer info string if any
	*/
	public function getTimeInfo()
	{
		$str=[];
		$timeRow=ActionLogTime::find()->select(['start_dt','end_dt','hours','minutes'])->where(['action_log_id'=>$this->id])->asArray()->one();
		if($timeRow!=null)$str=[
			'start_dt'=>$timeRow['start_dt'],
			'end_dt'=>$timeRow['end_dt'],
			'hours'=>$timeRow['hours'],
			'minutes'=>$timeRow['minutes'],
		];
		return $str;
	}

	/**
	* returns event info string if any
	*/
	public function getCalendarEventInfo()
	{
		$str=[];
		$eventRow=ActionLogEventInfo::find()->select(['start_date','end_date','color_code','all_day','sub_type','status'])->where(['action_log_id'=>$this->id])->asArray()->one();
		if($eventRow!=null)$str=[
			'start_date'=>$eventRow['start_date'],
			'end_date'=>$eventRow['end_date'],
			'color_code'=>$eventRow['color_code'],
			'all_day'=>$eventRow['all_day'],
			'sub_type'=>$eventRow['sub_type'],
			'status'=>$eventRow['status'],
		];
		return $str;
	}

	/**
	* returns reminder info if any
	*/
	public function getCalendarInfo()
	{
		$arr=[];
		$calendarRow=ActionLogCalendar::find()->select(['calendar_id'])->where(['action_log_id'=>$this->id])->asArray()->one();
		if($calendarRow!=null)$arr=['calendar_id'=>$calendarRow['calendar_id']];
		return $arr;
	}

	/**
	* returns attachments info if any
	*/
	public function getAttachmentInfo()
	{
		$arrFiles=[];
		$results=ActionLogAttachment::find()->where(['action_log_id'=>$this->id])->asArray()->all();
		if($results!=null){
			$otherFilesArr=['doc','docx','xls','xlsx','pdf'];
			foreach($results as $attachment){
				if($attachment['file_name']!=null && file_exists(Yii::$app->params['attachment_uploads_abs_path'].$attachment['file_name'])){
					$fpInfo=pathinfo(Yii::$app->params['attachment_uploads_abs_path'].$attachment['file_name']);
					$fileLink=$attachment['file_name'];
					if(in_array($fpInfo['extension'],$otherFilesArr)){
						$icon='images/icons/'.$fpInfo['extension'].'.jpg';
					}else{
						$icon=Yii::$app->fileHelperFunctions->getImagePath('attachment',$attachment['file_name'],'icon');
					}
					$arrFiles[]=[
						'icon'=>$icon,
						'title'=>$attachment['file_title'],
						'link'=>$fileLink,
					];
				}
			}
		}
		return $arrFiles;
	}

	/**
	* returns event info string if any
	*/
	public function getManagerIdz()
	{
		return ActionLogManager::find()->select(['staff_id'])->where(['action_log_id'=>$this->id])->asArray()->all();
	}

	/**
	* @inheritdoc
	*/
	public function afterSave($insert, $changedAttributes)
	{
		//Saving Attachment
		if($this->attachment_file!=null){
			foreach($this->attachment_file as $key=>$val){
				$alAttachmentRow=new ActionLogAttachment;
				$alAttachmentRow->action_log_id=$this->id;
				$pInfo=pathinfo($val);
				$fileName=Yii::$app->fileHelperFunctions->generateName.'.'.$pInfo['extension'];
				$alAttachmentRow->file_name=$fileName;
				$alAttachmentRow->file_title=$pInfo['filename'];
				if($alAttachmentRow->save()){
					rename(Yii::$app->params['temp_abs_path'].$val,Yii::$app->params['attachment_uploads_abs_path'].$fileName);
				}
			}
		}

		//Saving Subject of action
		if($this->subject!=null && $this->subject!=''){
			$alSbjRow=ActionLogSubject::find()->where(['action_log_id'=>$this->id])->one();
			if($alSbjRow==null){
				$alSbjRow=new ActionLogSubject;
				$alSbjRow->action_log_id=$this->id;
			}
			$alSbjRow->subject=$this->subject;
			$alSbjRow->due_date=$this->due_date;
			$alSbjRow->save();
		}

		//Saving Calendar
		if($this->calendar_id!=null && $this->calendar_id>0){
			$alCalRow=ActionLogCalendar::find()->where(['action_log_id'=>$this->id])->one();
			if($alCalRow==null){
				$alCalRow=new ActionLogCalendar;
				$alCalRow->action_log_id=$this->id;
			}
			$alCalRow->calendar_id=$this->calendar_id;
			$alCalRow->save();
		}

		//Saving Duration
		if($this->start_dt!=null && $this->start_dt!=''){
			$endDateTime=$this->end_dt;
			if($endDateTime=='')$endDateTime=$this->start_dt;
			$alSbjRow=ActionLogTime::find()->where(['action_log_id'=>$this->id])->one();
			if($alSbjRow==null){
				$alSbjRow=new ActionLogTime;
				$alSbjRow->action_log_id=$this->id;
			}
			$alSbjRow->start_dt=$this->start_dt;
			$alSbjRow->end_dt=$endDateTime;
			$alSbjRow->hours=$this->hours;
			$alSbjRow->minutes=$this->minutes;
			$alSbjRow->save();
		}

		//Saving Assigned To
		if($this->assign_to!=null){
			ActionLogManager::deleteAll(['and',['action_log_id'=>$this->id],['not in','staff_id',$this->assign_to]]);
			foreach($this->assign_to as $key=>$val){
				$managerRow=ActionLogManager::find()->where(['action_log_id'=>$this->id,'staff_id'=>$val])->one();
				if($managerRow==null){
					$managerRow=new ActionLogManager;
					$managerRow->action_log_id=$this->id;
					$managerRow->staff_id=$val;
					$managerRow->save();
				}
			}
		}
		$managerRow=ActionLogManager::find()->where(['action_log_id'=>$this->id,'staff_id'=>$this->created_by]);
		if(!$managerRow->exists()){
			$managerRow=new ActionLogManager;
			$managerRow->action_log_id=$this->id;
			$managerRow->staff_id=$this->created_by;
			$managerRow->save();
		}
		parent::afterSave($insert, $changedAttributes);
	}
}
