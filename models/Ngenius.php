<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;

//use common\models\Telr_transactions;

class Ngenius extends Model
{
    #same for live and test
    public static $merchantId = 200860164972;
    public static $currency_code = "AED";
    public static $payment_lang = "en"; // Payment page language ('en' or 'ar' only)
    public static $action = "SALE"; //what kind of transaction should be performed

    #test credentials
    #public static $apikey = "ODJhZGE3MmEtMWM3Mi00YTE1LWFjY2QtZWRlMGEyMzUyN2ExOjc5M2Q4ZGMxLTNhZWEtNDc1ZS04MDUyLTdkMDEyZDQyODQ0Mg==";
    #public static $outletRef = "d0e80613-6986-4ff9-8956-ef1e379c4f08";

    #live credentials
/*    public static $apikey = "ODJhZGE3MmEtMWM3Mi00YTE1LWFjY2QtZWRlMGEyMzUyN2ExOjc5M2Q4ZGMxLTNhZWEtNDc1ZS04MDUyLTdkMDEyZDQyODQ0Mg==";
    public static $outletRef = "d0e80613-6986-4ff9-8956-ef1e379c4f08";*/

    public static $apikey = "MDY2NzM3ZTUtYjgyMi00NDZjLTkxMDItZGQ3ZWI2YzkxODNlOjE4ZmE0NDg0LTYyODUtNDE1YS1iZWMxLTE2ZWE0ZTQ3NWYzNA==";
    public static $outletRef = "332619fd-01b0-4f23-a01e-e93a44379fae";


    public static function Check($amount, $orderReference)
    {
        #test credentials
      /*  $residServiceURL = "https://api-gateway-uat.ngenius-payments.com/transactions/outlets/" . self::$outletRef . "/orders/" . $orderReference;
        $idServiceURL = "https://identity-uat.ngenius-payments.com/auth/realms/ni/protocol/openid-connect/token";*/

        #live credentials
        $residServiceURL = "https://api-gateway.ngenius-payments.com/transactions/outlets/" . self::$outletRef . "/orders/" . $orderReference;
        $idServiceURL = "https://identity.ngenius-payments.com/auth/realms/NetworkInternational/protocol/openid-connect/token";

        $tokenHeaders = ["Authorization: Basic " . self::$apikey, "Content-Type: application/x-www-form-urlencoded"];
        $tokenResponse = self::invokeCurlRequest("POST", $idServiceURL, $tokenHeaders, http_build_query(['grant_type' => 'client_credentials']));
        $tokenResponse = json_decode($tokenResponse);

       /* echo "<pre>";
        print_r($tokenResponse);
        die;*/

        $access_token = $tokenResponse->access_token;

        $responseHeaders = ["Authorization: Bearer " . $access_token, "Content-Type: application/vnd.ni-payment.v2+json", "Accept: application/vnd.ni-payment.v2+json"];
        $orderResponse = self::invokeCurlRequest("GET", $residServiceURL, $responseHeaders, '');

        if ($orderResponse) {
            $orderResponse = json_decode($orderResponse);

            $ngenius_id = $orderResponse->merchantOrderReference;
            $ngenius_payref = $orderResponse->reference;

            if (($ngenius_payref == $orderReference) && (($amount) == $orderResponse->amount->value) && ($orderResponse->_embedded->payment[0]->state === 'CAPTURED')) {
                return $ngenius_id;
            }
        }

        return false;
        //echo self::prettyPrint($orderResponse) ;
    }

    public static function Pay($check, $amount, $type, $redirect_id)
    {

        #test credentials
       // $idServiceURL = "https://identity-uat.ngenius-payments.com/auth/realms/ni/protocol/openid-connect/token";
       // $txnServiceURL = "https://api-gateway-uat.ngenius-payments.com/transactions/outlets/" . self::$outletRef . "/orders";

        #live credentials
       $idServiceURL = "https://identity.ngenius-payments.com/auth/realms/NetworkInternational/protocol/openid-connect/token";
        $txnServiceURL = "https://api-gateway.ngenius-payments.com/transactions/outlets/" . self::$outletRef . "/orders";

        $tokenHeaders = ["Authorization: Basic " . self::$apikey, "Content-Type: application/x-www-form-urlencoded"];
        $tokenResponse = self::invokeCurlRequest("POST", $idServiceURL, $tokenHeaders, http_build_query(['grant_type' => 'client_credentials']));
        $tokenResponse = json_decode($tokenResponse);

       /* echo "<pre>";
        print_r($tokenResponse);
        die;*/

        $access_token = $tokenResponse->access_token;
        $vat = yii::$app->quotationHelperFunctions->getVatTotal($amount);

        $amount_with_vat= $amount + $vat;
        $amount_payment = $amount_with_vat * 100;
        $fee= $amount_with_vat;
        $order = [];
        $order['action'] = self::$action; // can be 'SALE';
        $order['MID'] = self::$merchantId;
        $order['amount']['currencyCode'] = self::$currency_code;
        $order['amount']['value'] = $amount_payment;
        $order['language'] = self::$payment_lang;
        $order['merchantOrderReference'] = time() . '-' . $type . '-' . $check->id;
        $order['billingAddress']['firstName'] = $check->client_customer_fname;
        $order['billingAddress']['lastName'] = (isset($check->client_customer_lname)) ? $check->client_customer_lname : "";
        $building = Buildings::findOne($check->building_info);
        $address = "";
        if (isset($building->communities->title)) $address .= $building->communities->title . ", ";
        if (isset($building->title)) $address .= $building->title . ", ";

        $order['billingAddress']['address1'] = $address;
        $order['billingAddress']['city'] = (isset($building->city)) ? Yii::$app->appHelperFunctions->emiratedListArr[$building->city] : "";

        $countryCode = "UAE";
       /* if (isset($check->country_code) && $check->country_code !== "ARE") {
            $countryCode = $check->country_code;
        }*/
        $order['billingAddress']['countryCode'] = $countryCode;

        $order['emailAddress'] = $check->client_customer_email;
        $order['merchantAttributes']['skipConfirmationPage'] = 1;
        $order['merchantAttributes']['redirectUrl'] = "https://maxima.windmillsgroup.com/online-valuation/order-confirmation?id=" . $redirect_id; // A redirect URL
        $order = json_encode($order);
        //var_dump($order); die();
        $orderCreateHeaders = ["Authorization: Bearer " . $access_token, "Content-Type: application/vnd.ni-payment.v2+json", "Accept: application/vnd.ni-payment.v2+json"];
        $orderCreateResponse = self::invokeCurlRequest("POST", $txnServiceURL, $orderCreateHeaders, $order);

        $orderCreateResponse = json_decode($orderCreateResponse);
       /* echo "<pre>";
        print_r($orderCreateResponse);
        die;*/
        if(isset($orderCreateResponse->errors[0])){
            echo  '<div class="alert alert-danger alert-dismissible">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  '.$orderCreateResponse->errors[0]->message.'
</div>';
           // echo  $orderCreateResponse->errors[0]->message;
        }else {
           /* var_dump($orderCreateResponse);
            die();*/
            $paymentLink = $orderCreateResponse->_links->payment->href; // the link to the payment page for redirection (either full-page redirect or iframe)
           // $orderReference = $orderCreateResponse->reference; // the reference to the order, which you should store in your records for future interaction with this order
            Yii::$app->db->createCommand()->update('online_valuation', ['payment_ref' => $orderCreateResponse->reference,'payment_amount'=>$fee], 'id=' . $redirect_id . '')->execute();
           // return Yii::$app->controller->redirect($paymentLink);
            header("Location:" . $paymentLink); // execute redirect --
            exit();
        }
       // echo $paymentLink;

    }

    private static function invokeCurlRequest($type, $url, $headers, $post)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($type == "POST") {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }
        $server_output = curl_exec($ch);
        curl_close($ch);
        return $server_output;
    }

    protected static function prettyPrint($json)
    {
        $result = '';
        $level = 0;
        $in_quotes = false;
        $in_escape = false;
        $ends_line_level = NULL;
        $json_length = strlen($json);

        for ($i = 0; $i < $json_length; $i++) {
            $char = $json[$i];
            $new_line_level = NULL;
            $post = "";
            if ($ends_line_level !== NULL) {
                $new_line_level = $ends_line_level;
                $ends_line_level = NULL;
            }
            if ($in_escape) {
                $in_escape = false;
            } else if ($char === '"') {
                $in_quotes = !$in_quotes;
            } else if (!$in_quotes) {
                switch ($char) {
                    case '}':
                    case ']':
                        $level--;
                        $ends_line_level = NULL;
                        $new_line_level = $level;
                        break;

                    case '{':
                    case '[':
                        $level++;
                    case ',':
                        $ends_line_level = $level;
                        break;

                    case ':':
                        $post = " ";
                        break;

                    case " ":
                    case "\t":
                    case "\n":
                    case "\r":
                        $char = "";
                        $ends_line_level = $new_line_level;
                        $new_line_level = NULL;
                        break;
                }
            } else if ($char === '\\') {
                $in_escape = true;
            }
            if ($new_line_level !== NULL) {
                $result .= "\n" . str_repeat("\t", $new_line_level);
            }
            $result .= $char . $post;
        }

        return $result;
    }

    public static function hashTheId($checkout_id)
    {
        $idhash = Yii::$app->getSecurity()->encryptByPassword($checkout_id, '30fj1vqn30fj1vqn###30fj1vqNNNS');
        $idhash = md5($idhash);
        $payhashes = Yii::$app->cache->get('payhashes');

        if (is_array($payhashes)) {
            foreach ($payhashes as $id => $hash) {
                if ($hash == $idhash) {
                    unset($payhashes[$id]);
                }
            }

            $payhashes[$checkout_id] = $idhash;
        } else {
            $payhashes = [];
            $payhashes[$checkout_id] = $idhash;
        }

        Yii::$app->cache->set('payhashes', $payhashes);
        return $idhash;
    }



    public static function setPaid($id, $type, $ref)
    {

        $order = OnlineValuation::find()->where(['id' => $id])->one();
        $amount = $order->payment_amount;
        $amount = $amount * 100;
        $wrong_url = Url::to(['online-valuation/order-not-confirmed', 'id' => $id, 'notpaid' => true], true);
        if ($order) {

                $check = self::Check($amount, $ref);
                if ($check) {
                    $order->payment_status = 1;
                    $order->payment_ref = $ref;
                    $order->ngenius_id = $check;
                    $order->save();

                    //all code
                } else {
                    return Yii::$app->getResponse()->redirect($wrong_url);
                }

        } else {
            return Yii::$app->getResponse()->redirect($wrong_url);
        }

    }

    /**
     * @param $amount
     * @return float|int
     */
    private static function getPaymentAmount($amount)
    {
        return $amount * 100;
    }

}   