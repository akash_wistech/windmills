<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SoldOffPlan;

/**
 * SoldOffPlanSearch represents the model behind the search form of `app\models\SoldOffPlan`.
 */
class SoldOffPlanSearch extends SoldOffPlan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['transaction_type', 'subtype', 'sales_sequence', 'red_number', 'transanction_date', 'community', 'property', 'property_Type', 'unit_number', 'bedrooms', 'floor', 'parking', 'balcony_area', 'size_sqf', 'land_size', 'price', 'price_per_sqf', 'developer','status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SoldOffPlan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'transaction_type', $this->transaction_type])
            ->andFilterWhere(['like', 'subtype', $this->subtype])
            ->andFilterWhere(['like', 'sales_sequence', $this->sales_sequence])
            ->andFilterWhere(['like', 'red_number', $this->red_number])
            ->andFilterWhere(['like', 'transanction_date', $this->transanction_date])
            ->andFilterWhere(['like', 'community', $this->community])
            ->andFilterWhere(['like', 'property', $this->property])
            ->andFilterWhere(['like', 'property_Type', $this->property_Type])
            ->andFilterWhere(['like', 'unit_number', $this->unit_number])
            ->andFilterWhere(['like', 'bedrooms', $this->bedrooms])
            ->andFilterWhere(['like', 'floor', $this->floor])
            ->andFilterWhere(['like', 'parking', $this->parking])
            ->andFilterWhere(['like', 'balcony_area', $this->balcony_area])
            ->andFilterWhere(['like', 'size_sqf', $this->size_sqf])
            ->andFilterWhere(['like', 'land_size', $this->land_size])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'price_per_sqf', $this->price_per_sqf])
            ->andFilterWhere(['like', 'developer', $this->developer])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
