<?php

namespace app\models;

use Yii;

class UserDegreeInfoCareer extends \yii\db\ActiveRecord
{
    public $status_verified;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_degree_info_career';
    }
}
