<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%module_email}}".
*
* @property string $module_type
* @property integer $module_id
* @property string $email
*/
class ModuleEmail extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%module_email}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['module_type','module_id','email'], 'required'],
      [['module_id'], 'integer'],
      [['module_type','email'], 'string'],
      [['email'], 'email'],
    ];
  }

  public static function primaryKey()
  {
  	return ['module_type','module_id','email'];
  }
}
