<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Clients;

/**
* CompanySearch represents the model behind the search form about `app\models\Company`.
*/
class ClientsSearch extends Clients
{

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by','data_type','segment_type'], 'integer'],
      [['title','created_at', 'updated_at', 'trashed_at','segment_type','pageSize','nick_name','client_type'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */


    public function search($params)
    {
        $query = Clients::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'segment_type' => $this->segment_type,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => $this->trashed,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere(['like', 'nick_name', $this->nick_name]);

        return $dataProvider;
    }

    /**
     * Generates query for the search
     *
     * @param array $params
     *
     * @return ActiveRecord
     */

}
