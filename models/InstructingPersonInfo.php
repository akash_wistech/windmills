<?php

namespace app\models;
use Yii;

class InstructingPersonInfo extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'instructing_person_info';
    }

    public function rules()
    {
        return [
            [['client_id','job_title_id'], 'integer'],
            [['firstname','lastname','email','mobile'], 'safe'],
        ];
    }

}
