<?php

namespace app\models;

use Yii;

class BayutToRentDetailLinks extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'bayut_to_rent_detail_links';
    }


    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['url', 'property_category'], 'string', 'max' => 255],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'status' => 'Status',
            'property_category' => 'Property Category',
        ];
    }
}
