<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "crm_quotation_owner".
 *
 * @property int $id
 * @property string $name
 * @property string $percentage
 * @property int $index_id
 * @property int $quotation_id
 */
class CrmQuotationOwner extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_quotation_owner';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'percentage', 'index_id', 'quotation_id'], 'required'],
            [['index_id', 'quotation_id','property_index'], 'integer'],
            [['name', 'percentage'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'percentage' => 'Percentage',
            'index_id' => 'Index ID',
            'quotation_id' => 'Quotation ID',
        ];
    }
}
