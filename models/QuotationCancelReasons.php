<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "Quotation_cancel_reasons".
 *
 * @property int $id
 * @property string $title
 * @property int $city
 * @property float|null $land_price
 * @property string|null $source
 * @property int $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 * @property int|null $approved_by
 * @property string|null $approved_at
 */
class QuotationCancelReasons extends ActiveRecordFull
{
    public $status_verified;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quotation_cancel_reasons';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['status', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['created_at', 'updated_at', 'trashed_at'], 'safe'],
            [['status_verified','status_verified_at','status_verified_by'], 'safe'],
          //  [['title'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }
    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'quotation_cancel_reasons';
    }
    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->title;
    }

    public function beforeSave($insert){

        // verify status reset after edit only something changed
        $isChanged = false;
        $ignoredAttributes = ['updated_at','status_verified_at']; 
        foreach ($this->attributes as $attribute => $value) {
            if (in_array($attribute, $ignoredAttributes)) {
                continue;
            }
            if ($this->getOldAttribute($attribute) != $value) {
                $isChanged = true;
                break;
            }
        }
        if ($isChanged) {
            if($this->status == 1 && $this->getOldAttribute('status') == 1){
                $this->status = 2;
            }elseif ($this->status == 2 && $this->getOldAttribute('status') == 1) {
                $this->status = 1;
            }
        }
        
        return parent::beforeSave($insert);
    }

}
