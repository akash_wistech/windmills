<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "daily_solds_import".
 *
 * @property int $id
 * @property string $transaction_type
 * @property string $subtype
 * @property string $sales_sequence
 * @property string $reidin_ref
 * @property string $transaction_date
 * @property string $community
 * @property string|null $property
 * @property string $property_type
 * @property string|null $unit
 * @property string|null $bedrooms
 * @property string|null $floor
 * @property string|null $parking
 * @property string|null $balcony_area
 * @property string $size_sqf
 * @property string $land_size
 * @property string $amount
 * @property string $sqf
 * @property string|null $developer
 * @property int|null $status
 * @property int|null $new_building_id
 */
class DailyRentsImport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'daily_rents_solds';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'transaction_type', 'subtype', 'sales_sequence', 'reidin_ref', 'transaction_date', 'community', 'property_type', 'size_sqf', 'land_size', 'amount', 'sqf'], 'safe'],
            [['id', 'status', 'new_building_id'], 'safe'],
            [['transaction_type', 'reidin_ref', 'size_sqf', 'land_size', 'amount'], 'safe'],
            [['subtype'], 'safe'],
            [['sales_sequence'], 'safe'],
            [['transaction_date'], 'safe'],
            [['community'], 'safe'],
            [['property'], 'safe'],
            [['property_type'], 'safe' ],
            [['unit'], 'safe'],
            [['bedrooms'], 'safe'],
            [['floor', 'parking', 'balcony_area'], 'safe'],
            [['sqf'], 'safe'],
            [['developer'], 'safe'],
            [['id'], 'safe'],
            [['new_building_id','created_at','start_date','end_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'transaction_type' => 'Transaction Type',
            'subtype' => 'Subtype',
            'sales_sequence' => 'Sales Sequence',
            'reidin_ref' => 'Reidin Ref',
            'transaction_date' => 'Transaction Date',
            'community' => 'Community',
            'property' => 'Property',
            'property_type' => 'Property Type',
            'unit' => 'Unit',
            'bedrooms' => 'Bedrooms',
            'floor' => 'Floor',
            'parking' => 'Parking',
            'balcony_area' => 'Balcony Area',
            'size_sqf' => 'Size Sqf',
            'land_size' => 'Land Size',
            'amount' => 'Amount',
            'sqf' => 'Sqf',
            'developer' => 'Developer',
            'status' => 'Status',
            'new_building_id' => 'New Building ID',
        ];
    }

    public function getNewBuilding(){
        return $this->hasOne(SoldTransaction::className(),['land_transaction_id'=>'id']);
    }
}
