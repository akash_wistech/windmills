<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
* This is the model class for table "{{%prospect}}".
*
* @property integer $id
* @property string $sfirstname
* @property string $lastname
* @property string $company_name
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
* @property integer $trashed
* @property string $trashed_at
* @property integer $trashed_by
*/
class Prospect extends ActiveRecordFull
{
	public $manager_id,$tags,$input_field;
	public $map_location,$map_lat,$map_lng;
  public $map_lat_tmp,$map_lng_tmp;
  public $map_location_tmp,$maploc_lat_tmp,$maploc_lng_tmp;
  public $emails,$phone_numbers;
	public $comp_row_id,$comp_id,$comp_name,$comp_role_id,$comp_role,$comp_job_title_id,$comp_job_title;
  public $add_id,$add_is_primary,$add_country_id,$add_zone_id,$add_city,$add_phone,$add_address;

	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%prospect}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['firstname'],'required'],
			[['created_at','updated_at','trashed_at'],'safe'],
			[[
				'add_is_primary','created_by','updated_by','trashed','trashed_by'
			],'integer'],
			[[
				'firstname','lastname','email','mobile','company_name','tags','map_location','map_lat_tmp','map_lng_tmp',
				'map_location_tmp','maploc_lat_tmp','maploc_lng_tmp'
			],'string'],
			[['firstname','lastname','company_name'],'trim'],
			[['emails'],'each','rule'=>['email']],
			[[
				'manager_id','phone_numbers','comp_row_id','comp_id','comp_role_id','comp_job_title_id','add_id',
				'add_country_id','add_zone_id'
			],'each','rule'=>['integer']],
			[[
				'comp_name','comp_role','comp_job_title','add_city','add_phone','add_address'
			],'each','rule'=>['string']],
      ['input_field','checkRequiredInputs'],
		];
	}

  /**
   * Validates the required inputs.
   * This method serves as the inline validation for required inputs.
   *
   * @param string $attribute the attribute currently being validated
   * @param array $params the additional name-value pairs given in the rule
   */
  public function checkRequiredInputs()
  {
    if (!$this->hasErrors()) {
			return Yii::$app->inputHelperFunctions->validateRequired($this);
    }
  }

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'firstname' => Yii::t('app', 'First Name'),
			'lastname' => Yii::t('app', 'Last Name'),
			'company_name' => Yii::t('app', 'Company'),
			'cp_name' => Yii::t('app', 'Name'),
			'country_id' => Yii::t('app', 'Country'),
			'country_name' => Yii::t('app', 'Country'),
			'city' => Yii::t('app', 'City'),
			'email' => Yii::t('app', 'Email'),
			'mobile' => Yii::t('app', 'Contact No.'),
			'emails' => Yii::t('app', 'Additional Emails'),
			'phone_numbers' => Yii::t('app', 'Additional Contact Nos.'),
			'address' => Yii::t('app', 'Address'),
			'manager_id' => Yii::t('app', 'Assign To'),
			'created_at' => Yii::t('app', 'Created At'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'updated_by' => Yii::t('app', 'Updated By'),
			'trashed' => Yii::t('app', 'Trashed'),
			'trashed_at' => Yii::t('app', 'Trashed At'),
			'trashed_by' => Yii::t('app', 'Trashed By'),
			'workflow_id' => Yii::t('app', 'Process Name'),
			'workflow_stage_id' => Yii::t('app', 'Stage'),
			'tags' => Yii::t('app', 'Tags'),
			'add_is_primary' => Yii::t('app', 'Primary'),
			'add_country_id' => Yii::t('app', 'Country'),
			'add_zone_id' => Yii::t('app', 'State / Province'),
			'add_city' => Yii::t('app', 'City'),
			'add_phone' => Yii::t('app', 'Contact Number'),
			'add_address' => Yii::t('app', 'Address'),

			'comp_name' => Yii::t('app', 'Name'),
			'comp_role' => Yii::t('app', 'Role'),
			'comp_job_title' => Yii::t('app', 'Job Title'),
		];
	}

	/**
	* returns class name
	*/
	public function getBaseClassName()
	{
		return 'Prospect';
	}

	/**
	* returns main title/name of row for status and delete loging
	*/
	public function getRecTitle()
	{
		return $this->name;
	}

	/**
	* returns main title/name of row for status and delete loging
	*/
	public function getRecType()
	{
		return 'Prospect';
	}

	/**
	* returns type of module for process and events
	*/
	public function getModuleTypeId()
	{
		return 'prospect';
	}

  /**
  * return full name
  */
  public function getName()
  {
    return trim($this->firstname.($this->lastname!='' ? ' '.$this->lastname : ''));
  }

	/**
	* Get Assigned Staff members
	* @return \yii\db\ActiveQuery
	*/
	public function getManagerIdz()
	{
		return ModuleManager::find()->select(['staff_id'])->where(['module_type'=>$this->moduleTypeId,'module_id'=>$this->id])->asArray()->all();
	}

	/**
	* @return \yii\db\ActiveQuery
	*/
  public function getTagsListArray()
  {
		$subQueryModuleTags=ModuleTag::find()->select(['tag_id'])->where(['module_type'=>$this->moduleTypeId,'module_id'=>$this->id]);
		$acRows=Tags::find()->select(['title'])->where(['id'=>$subQueryModuleTags])->asArray()->all();
		$tagList=[];
		foreach($acRows as $acRow){
			$tagList[]=$acRow['title'];
		}
    return $tagList;
  }

	/**
	* @inheritdoc
	*/
	public function afterSave($insert, $changedAttributes)
	{
		//Saving Custom Fields
		Yii::$app->inputHelperFunctions->saveCustomField($this);

		//Saving Multi Emails
		Yii::$app->inputHelperFunctions->saveMultiEmailNumbers($this);

		//Saving Multi Companies
		Yii::$app->inputHelperFunctions->saveMultiCompanies($this);

		//Saving Multi Address
		Yii::$app->inputHelperFunctions->saveMultiAddress($this);

		//Saving Tags
		Yii::$app->inputHelperFunctions->saveTags($this);

		//Saving Managers
		Yii::$app->inputHelperFunctions->saveManagers($this);

		parent::afterSave($insert, $changedAttributes);
	}
}
