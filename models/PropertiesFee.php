<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use app\components\behaviors\ActivityLogBehaviorUser;
use yii\db\ActiveRecord;

class PropertiesFee extends ActiveRecordFull
{
    
    public static function tableName()
    {
        return 'properties_fee';
    }
    
    public function rules()
    {
        return [
            [['property_id','ready','zero_comparables_available','scope_of_work','basis_of_values_fee','valuaiton_approach'], 'safe'],
            [['created_at','updated_at','updated_at','created_by','updated_by','deleted_by'], 'safe'],
        ];
    }
    
    
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => function($event) {
                    return date("Y-m-d H:i:s");
                },
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }
    
    public function beforeSave($insert)
    {
        return parent::beforeSave($insert); 
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
    }
    
}
