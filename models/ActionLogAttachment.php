<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%action_log_attachment}}".
*
* @property integer $action_log_id
* @property integer $file_name
* @property integer $file_title
*/
class ActionLogAttachment extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%action_log_attachment}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['action_log_id', 'file_name'], 'required'],
      [['action_log_id'], 'integer'],
      [['file_name','file_title'], 'string'],
    ];
  }

  public static function primaryKey()
  {
  	return ['action_log_id','file_name'];
  }
}
