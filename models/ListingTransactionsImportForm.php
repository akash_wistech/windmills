<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * SoldTransactionImportForm is the model behind the sold transaction import form.
 */
class ListingTransactionsImportForm extends Model
{
    public $importfile;
    public $allowedFileTypes = ['csv'];//'xls', 'xlsx',

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['importfile'], 'safe'],
            [['importfile'], 'file', 'extensions' => implode(",", $this->allowedFileTypes)]
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'importfile' => Yii::t('app', 'File'),
        ];
    }

    /**
     * import the entries into table
     */
    public function save()
    {
        $saved = 0;
        $unsaved = 0;
        $errNames = '';
        $change = array();
        if ($this->validate()) {
            if (UploadedFile::getInstance($this, 'importfile')) {
                $importedFile = UploadedFile::getInstance($this, 'importfile');
                // if no image was uploaded abort the upload
                if (!empty($importedFile)) {
                    $pInfo = pathinfo($importedFile->name);
                    $ext = $pInfo['extension'];
                    if (in_array($ext, $this->allowedFileTypes)) {
                        // Check to see if any PHP files are trying to be uploaded
                        $content = file_get_contents($importedFile->tempName);
                        if (preg_match('/\<\?php/i', $content)) {

                        } else {
                            $this->importfile = Yii::$app->params['temp_abs_path'] . $importedFile->name;
                            $importedFile->saveAs($this->importfile);

                            $csvFile = fopen($this->importfile, 'r');
                            $data = [];
                            $n = 1;
                            while (($line = fgetcsv($csvFile)) !== FALSE) {

                          /*  echo "<pre>";
                            print_r($line);
                            die;*/

                                if ($n > 1) {



                                    //$string = str_replace(' ', '-', $line[42]); // Replaces all spaces with hyphens.

                                    $compnay_name = preg_replace('/[^A-Za-z0-9\-]/', '', $line[40]); // Removes special chars.
                                   // $compnay_name = str_replace('-', ' ', $line[42]);

                                   // $date = date("Y-m-d",strtotime($line[3]));

                                    $date = trim($line[3]);
                                    if($date <> null) {
                                        $date = str_replace('/', '-', trim($line[3]));
                                        $date = date("Y-m-d", strtotime($date));
                                    }else{
                                        $date = '0000-00-00';
                                    }
                                    $source = array_search(trim($line[1]), Yii::$app->appHelperFunctions->propertySourceListArr);
                                    $property_category = array_search(trim($line[6]), Yii::$app->appHelperFunctions->propertiesCategoriesListArr);
                                   // $view = array_search(trim($line[27]), Yii::$app->appHelperFunctions->otherPropertyViewListArr);
                                    $listing_property_type = array_search(trim($line[27]), Yii::$app->appHelperFunctions->listingsPropertyTypeListArr);
                                   // $location = array_search(trim($line[23]), Yii::$app->appHelperFunctions->locationConditionsListArr);
                                    $tenure = array_search(trim($line[7]), Yii::$app->appHelperFunctions->buildingTenureArr);
                                    $property_placement = array_search(trim($line[8]), Yii::$app->appHelperFunctions->propertyPlacementListArr);
                                    $property_visibility = array_search(trim($line[9]), Yii::$app->appHelperFunctions->propertyVisibilityListArr);
                                    $property_exposure = array_search(trim($line[10]), Yii::$app->appHelperFunctions->propertyExposureListArr);
                                    $property_condition = array_search(trim($line[11]), Yii::$app->appHelperFunctions->propertyConditionListArr);

                                    $buildingRow = Buildings::find()->where(['=', 'title', $line[5]])->asArray()->one();


                                    if (!empty($buildingRow) && $buildingRow <> null) {



                                        $model = new ListingsTransactions();
                                        $model->listings_reference = $line[0];
                                        $model->source = $source;
                                        $model->listing_website_link = $line[2];
                                        $model->listing_date = $date;
                                        $model->unit_number = $line[4];
                                        $model->building_info = $buildingRow['id'];
                                        $model->property_category = $property_category;
                                        $model->tenure = $tenure;
                                        $model->property_placement = $property_placement;
                                        $model->property_visibility = $property_visibility;
                                        $model->property_exposure = $property_exposure;
                                        $model->property_condition = $property_condition;
                                        $model->development_type = $line[12];
                                        $model->utilities_connected = $line[13];
                                        $model->finished_status = (trim($line[14]) <> null) ? trim($line[14]) : '';
                                        $model->pool = $line[15];
                                        $model->gym = $line[16];
                                        $model->play_area =$line[17];
                                        $model->other_facilities = $line[18];
                                        $model->completion_status = $line[19];
                                        $model->white_goods = $line[20];
                                        $model->furnished = $line[21];
                                        $model->landscaping = $line[22];
                                       // $model->location = $location;
                                        $model->no_of_bedrooms = str_replace( '.00', '', $line[23]);
                                        $model->upgrades = $line[24];
                                        $model->full_building_floors = $line[25];
                                        $model->parking_space = $line[26];
                                       // $model->view =$view;
                                        $model->listing_property_type = ($listing_property_type <> null)? $listing_property_type: 0 ;
                                        $model->floor_number = $line[28];
                                        $model->number_of_levels = $line[29];
                                        $land_size = str_replace( ',', '', $line[30]);
                                        $model->land_size = ($land_size <> null) ? $land_size : 0;
                                        $model->built_up_area = str_replace( ',', '', $line[31]);
                                        $model->balcony_size = $line[32];
                                        $model->listings_price = str_replace( ',', '', $line[33]);
                                        $model->listings_rent = str_replace( ',', '', $line[34]);
                                        //$model->price_per_sqt = $line[35];
                                        $model->price_per_sqt = round( ($model->listings_price / $model->built_up_area),2) ;
                                        $model->final_price = str_replace( ',', '', $line[36]);
                                        $model->developer_margin = $line[37];
                                        $model->agent_name = $line[38];
                                        $model->agent_phone_no = str_replace( ',', '', $line[39]);
                                        $model->agent_company =$compnay_name;
                                        $model->location_highway_drive = ($line[41] <> null) ? str_replace(' ','_',trim($line[41])) : '';
                                        $model->location_school_drive = ($line[42] <> null) ? str_replace(' ','_',trim($line[42])) : '';
                                        $model->location_mall_drive = ($line[43] <> null) ? str_replace(' ','_',trim($line[43])) : '';
                                        $model->location_sea_drive = ($line[44] <> null) ? str_replace(' ','_',trim($line[44])) : '';
                                        $model->location_park_drive = ($line[45] <> null) ? str_replace(' ','_',trim($line[45])) : '';
                                        $model->view_community = ($line[46] <> null) ? strtolower(trim($line[46])) : '';
                                        $model->view_pool = ($line[47] <> null) ? strtolower(trim($line[47])) : '';
                                        $model->view_burj = ($line[48] <> null) ? strtolower(trim($line[48])) : '';
                                        $model->view_sea = ($line[49] <> null) ? strtolower(trim($line[49])) : '';
                                        $model->view_marina = ($line[50] <> null) ? strtolower(trim($line[50])) : '';
                                        $model->view_mountains = ($line[51] <> null) ? strtolower(trim($line[51])) : '';
                                        $model->view_lake = ($line[52] <> null) ? strtolower(trim($line[52])) : '';
                                        $model->view_golf_course = ($line[53] <> null) ? strtolower(trim($line[53])) : '';
                                        $model->view_park = ($line[54] <> null) ? strtolower(trim($line[54])) : '';
                                        $model->view_special = ($line[55] <> null) ? strtolower(trim($line[55])) : '';
                                        $model->estimated_age = ($line[56] <> null) ? trim($line[56]) : 0;
                                        $gross_yield = round( ($model->listings_rent / $model->listings_price) * 100,2);
                                        $model->gross_yield = ($gross_yield <> null )? $gross_yield : 0;

                                        if ($model->save()) {
                                            $saved++;
                                        } else {
                                            if ($model->hasErrors()) {

                                                foreach ($model->getErrors() as $error) {
                                                    if (count($error) > 0) {
                                                        foreach ($error as $key => $val) {
                                                            echo $val;
                                                        }
                                                    }
                                                }
                                                echo $n."<pre>";
                                                print_r($line);
                                                print_r($buildingRow);
                                                die;
                                                die();
                                                $errNames .= '<br />' . $buildingName;
                                            }

                                            $unsaved++;
                                        }
                                    }


                                } else {
                                   // $change[] = $buildingName;

                                }
                                $n++;
                            }
                            fclose($csvFile);
                            //Unlink File
                            unlink($this->importfile);
                        }
                    }
                }
            }
            if ($saved > 0) {

                Yii::$app->getSession()->setFlash('success', $saved . ' - ' . Yii::t('app', 'rows saved successfully'));
            }
            if ($unsaved > 0) {
                Yii::$app->getSession()->setFlash('error', $unsaved . ' - ' . Yii::t('app', 'rows were not saved!') . $errNames);
            }
            return true;
        } else {
            return false;
        }
    }
}
