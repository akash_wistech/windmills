<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SoldTransactionUpgrade;

/**
 * SoldTransactionUpgradeSearch represents the model behind the search form of `app\models\SoldTransactionUpgrade`.
 */
class SoldTransactionUpgradeSearch extends SoldTransactionUpgrade
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'type', 'source', 'property_category', 'building_info', 'community', 'sub_community', 'tenure', 'floor_number', 'number_of_levels', 'location', 'property_visibility', 'property_condition', 'no_of_bedrooms', 'number_of_rooms', 'upgrades', 'full_building_floors', 'parking_space', 'view', 'agent_phone_no', 'status', 'status_verified_by', 'created_by', 'updated_by', 'trashed', 'trashed_by', 'property_id', 'list_type'], 'integer'],
            [['transaction_date', 'unit_number', 'balcony_size', 'listing_property_type', 'development_type', 'finished_status', 'pool', 'gym', 'play_area', 'other_facilities', 'landscaping', 'white_goods', 'furnished', 'utilities_connected', 'developer_margin', 'agent_name', 'agent_company', 'status_verified_at', 'created_at', 'updated_at', 'trashed_at'], 'safe'],
            [['land_size', 'built_up_area', 'property_placement', 'property_exposure', 'completion_status', 'market_value', 'listings_price', 'listings_rent', 'price_per_sqt', 'final_price', 'estimated_age'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SoldTransactionUpgrade::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'source' => $this->source,
            'transaction_date' => $this->transaction_date,
            'property_category' => $this->property_category,
            'building_info' => $this->building_info,
            'community' => $this->community,
            'sub_community' => $this->sub_community,
            'tenure' => $this->tenure,
            'floor_number' => $this->floor_number,
            'number_of_levels' => $this->number_of_levels,
            'location' => $this->location,
            'land_size' => $this->land_size,
            'built_up_area' => $this->built_up_area,
            'property_placement' => $this->property_placement,
            'property_visibility' => $this->property_visibility,
            'property_exposure' => $this->property_exposure,
            'property_condition' => $this->property_condition,
            'completion_status' => $this->completion_status,
            'no_of_bedrooms' => $this->no_of_bedrooms,
            'number_of_rooms' => $this->number_of_rooms,
            'market_value' => $this->market_value,
            'upgrades' => $this->upgrades,
            'full_building_floors' => $this->full_building_floors,
            'parking_space' => $this->parking_space,
            'view' => $this->view,
            'listings_price' => $this->listings_price,
            'listings_rent' => $this->listings_rent,
            'price_per_sqt' => $this->price_per_sqt,
            'final_price' => $this->final_price,
            'agent_phone_no' => $this->agent_phone_no,
            'status' => $this->status,
            'status_verified_at' => $this->status_verified_at,
            'status_verified_by' => $this->status_verified_by,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => $this->trashed,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
            'property_id' => $this->property_id,
            'estimated_age' => $this->estimated_age,
            'list_type' => $this->list_type,
        ]);

        $query->andFilterWhere(['like', 'unit_number', $this->unit_number])
            ->andFilterWhere(['like', 'balcony_size', $this->balcony_size])
            ->andFilterWhere(['like', 'listing_property_type', $this->listing_property_type])
            ->andFilterWhere(['like', 'development_type', $this->development_type])
            ->andFilterWhere(['like', 'finished_status', $this->finished_status])
            ->andFilterWhere(['like', 'pool', $this->pool])
            ->andFilterWhere(['like', 'gym', $this->gym])
            ->andFilterWhere(['like', 'play_area', $this->play_area])
            ->andFilterWhere(['like', 'other_facilities', $this->other_facilities])
            ->andFilterWhere(['like', 'landscaping', $this->landscaping])
            ->andFilterWhere(['like', 'white_goods', $this->white_goods])
            ->andFilterWhere(['like', 'furnished', $this->furnished])
            ->andFilterWhere(['like', 'utilities_connected', $this->utilities_connected])
            ->andFilterWhere(['like', 'developer_margin', $this->developer_margin])
            ->andFilterWhere(['like', 'agent_name', $this->agent_name])
            ->andFilterWhere(['like', 'agent_company', $this->agent_company]);

        return $dataProvider;
    }
}
