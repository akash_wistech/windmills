<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%contact_manager}}".
*
* @property integer $contact_id
* @property integer $staff_id
*/
class ContactManager extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%contact_manager}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['contact_id', 'staff_id'], 'required'],
      [['contact_id', 'staff_id'], 'integer'],
    ];
  }

  public static function primaryKey()
  {
  	return ['contact_id','staff_id'];
  }
}
