<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ClientValuation;

use yii;

/**
 * ScopeOfServicesSearch represents the model behind the search form of `app\models\ClientValuation`.
 */
class ClientValuationSearch extends ClientValuation
{

    public $date_range, $time_period,$time_period_compare, $custom_date_btw;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_customer_fname','client_customer_lname','instructing_person_fname','instructing_person_lname','contact_person_fname','contact_person_lname'], 'string', 'max' => 255],
            [['client_customer_phone_code','inspection_type','urgency','inspection_type','urgency','building_info','property_id','property_category','status_verified', 'status_verified_by', 'created_by', 'updated_by', 'deleted_by','tenure','floor_number','purpose_of_valuation','number_of_units'], 'integer'],
            [['reference_number','client_reference','client_customer_phone','client_customer_email','instructing_person_phone_code','instructing_person_phone','instructing_person_email','contact_person_prefix','instructing_person_prefix','client_customer_prefix','contact_person_phone_code','contact_person_phone','contact_person_landline_code','contact_person_landline','contact_person_email','status_verified_at', 'created_at', 'updated_at', 'deleted_at','plot_number','street','unit_number','valuation_date','building_number','community','sub_community','country','city','time_period','time_period_compare','custom_date_btw','key','trustee_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $request = Yii::$app->request;
        $sort = $request->get('sort');

        $query = ClientValuation::find();

        if(Yii::$app->user->identity->client_approver == 1){
            $query->where(['client_id'=>Yii::$app->user->identity->company_id,'status_verified'=>2]);
        }else{
            $query->where(['created_by'=>Yii::$app->user->identity->id,'status_verified'=>2]);
        }
        
        if (!$sort) {
            $query->orderBy([ 'created_at' => SORT_DESC ]);
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50, 
            ],
            'sort' => [
                'attributes' => [
                    'created_at',
                    'client_reference',
                    'property_id',  
                    'building_info', 
                    'community',
                    'sub_community',
                    'city',
                    'valuation_date',
                    'status_verified',
                ],
            ],
        ]);


        

        $this->load($params);


        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'].' 00:00:00';
                $to_date = $date_array['end_date'];
                $to_date = date('Y-m-d', strtotime($to_date . ' +1 day')).' 23:59:00';
            }


        }else{
            $from_date = '2020-04-28 00:00:00';
            $to_date = date('Y-m-d 23:59:00');
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status_verified' => $this->status_verified,
            'status_verified_by' => $this->status_verified_by,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'building_info' => $this->building_info,
            'community' => $this->community,
            'sub_community' => $this->sub_community,
            'city' => $this->city,
        ]);

        $query->andFilterWhere([
            'between', 'created_at', $from_date, $to_date,
        ]);

        //$query->andFilterWhere(['like', 'reference_number', $this->reference_number]);

        return $dataProvider;
    }
}
