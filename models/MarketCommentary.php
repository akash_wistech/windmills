<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "market_commentary".
 *
 * @property int $id
 * @property string|null $abu_dhabi_commentary
 * @property string|null $ajman_commentary
 * @property string|null $al_ain_commentary
 * @property string|null $dubai_commentary
 * @property string|null $fujairah_commentary
 * @property string|null $ras_al_khaimah_commentary
 * @property string|null $sharjah_commentary
 * @property string|null $umm_al_quwain_commentary
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class MarketCommentary extends ActiveRecordFull
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'market_commentary';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['abu_dhabi_commentary', 'ajman_commentary', 'al_ain_commentary', 'dubai_commentary', 'fujairah_commentary', 'ras_al_khaimah_commentary', 'sharjah_commentary', 'umm_al_quwain_commentary','makkah_commentary','dammam_commentary'], 'string'],
            [['created_at', 'updated_at', 'trashed_at'], 'safe'],
            [['created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['status_verified','status_verified_at','status_verified_by','property_type'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'abu_dhabi_commentary' => 'Abu Dhabi Commentary',
            'ajman_commentary' => 'Ajman Commentary',
            'al_ain_commentary' => 'Al Ain Commentary',
            'dubai_commentary' => 'Dubai Commentary',
            'fujairah_commentary' => 'Fujairah Commentary',
            'ras_al_khaimah_commentary' => 'Ras Al Khaimah Commentary',
            'sharjah_commentary' => 'Sharjah Commentary',
            'umm_al_quwain_commentary' => 'Umm Al Quwain Commentary',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Communities';
    }
    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->title;
    }
}
