<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AmendmentRequest;

/**
 * AmendmentRequestSearch represents the model behind the search form of `app\models\AmendmentRequest`.
 */
class AmendmentRequestSearch extends AmendmentRequest
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'applicant', 'under_scope_of_agreement', 'number_of_hours_expected', 'priority', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['date', 'start_date', 'reference_no', 'deadline', 'completion_date', 'change_explanation', 'reason_of_change', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AmendmentRequest::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'date' => $this->reference_no,
            'deadline' => $this->deadline,
            'completion_date' => $this->completion_date,
            'applicant' => $this->applicant,
            'under_scope_of_agreement' => $this->under_scope_of_agreement,
            'number_of_hours_expected' => $this->number_of_hours_expected,
            'priority' => $this->priority,
            'start_date' => $this->start_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
        ]);

        $query->andFilterWhere(['like', 'change_explanation', $this->change_explanation])
            ->andFilterWhere(['like', 'reason_of_change', $this->reason_of_change]);

        return $dataProvider;
    }
}
