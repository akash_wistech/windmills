<?php

namespace app\models;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PtaskManager;


// dd($_REQUEST);
/**
 * TaskmangerSearch represents the model behind the search form of `app\models\PtaskManager`.
 */
class PtaskManagerSearch extends PtaskManager
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['modification', 'description', 'module', 'frequency', 'priority', 'start_date', 'end_date','completed_date','meeting_date','entered_by','status','department','created_at','responsibility','agreed_by','expiry_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $user_id = Yii::$app->user->identity->id;
        if( isset($user_id) && ($user_id == '1' || $user_id == '111811' || $user_id == '14' || $user_id == '110465' || $user_id == '6319' || $user_id == '110217' || $user_id == '21' || $user_id == '86' || $user_id == '53') )
        {
            $query = PtaskManager::find();
        }else{
            $query = PtaskManager::find()->where(['assigned_to' => $user_id]);
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        // $query->andFilterWhere([
        //     'modification' => $this->modification,
        //     'description' => $this->description,
        //     'module' => $this->module,
        //     'frequency' => $this->frequency,
        //     'priority' => $this->priority,
        //     'start_date' => $this->start_date,
        //     'end_date' => $this->end_date,
        //     'completed_date' => $this->completed_date,
        //     'meeting_date' => $this->meeting_date,
        //     'entered_by' => $this->entered_by,
        //     'status' => $this->status,
        // ]);

        $query->andFilterWhere(['like', 'modification', $this->modification])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'department', $this->department])
            ->andFilterWhere(['like', 'module', $this->module])
            ->andFilterWhere(['like', 'frequency', $this->frequency])
            ->andFilterWhere(['like', 'priority', $this->priority])
            ->andFilterWhere(['like', 'start_date', $this->start_date])
            ->andFilterWhere(['like', 'end_date', $this->end_date])
            ->andFilterWhere(['like', 'completed_date', $this->completed_date])
            ->andFilterWhere(['like', 'meeting_date', $this->meeting_date])
            ->andFilterWhere(['like', 'entered_by', $this->entered_by])
            ->andFilterWhere(['like', 'created_at', $this->created_at])
            ->andFilterWhere(['like', 'responsibility', $this->responsibility])
            ->andFilterWhere(['like', 'agreed_by', $this->agreed_by])
            ->andFilterWhere(['like', 'expiry_date', $this->expiry_date])
            ->andFilterWhere(['like', 'status', $this->status]);


        return $dataProvider;
    }

}
