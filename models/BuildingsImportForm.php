<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * SoldTransactionImportForm is the model behind the sold transaction import form.
 */
class BuildingsImportForm extends Model
{
    public $importfile;
    public $allowedFileTypes = ['csv'];//'xls', 'xlsx',

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['importfile'], 'safe'],
            [['importfile'], 'file', 'extensions' => implode(",", $this->allowedFileTypes)]
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'importfile' => Yii::t('app', 'File'),
        ];
    }

    /**
     * import the entries into table
     */
    public function save()
    {
        $saved = 0;
        $unsaved = 0;
        $errNames = '';
        $change = array();
        if ($this->validate()) {
            if (UploadedFile::getInstance($this, 'importfile')) {
                $importedFile = UploadedFile::getInstance($this, 'importfile');
                // if no image was uploaded abort the upload
                if (!empty($importedFile)) {
                    $pInfo = pathinfo($importedFile->name);
                    $ext = $pInfo['extension'];
                    if (in_array($ext, $this->allowedFileTypes)) {
                        // Check to see if any PHP files are trying to be uploaded
                        $content = file_get_contents($importedFile->tempName);
                        if (preg_match('/\<\?php/i', $content)) {

                        } else {
                            $this->importfile = Yii::$app->params['temp_abs_path'] . $importedFile->name;
                            $importedFile->saveAs($this->importfile);

                            $csvFile = fopen($this->importfile, 'r');
                            $data = [];
                            $n = 1;
                            while (($line = fgetcsv($csvFile)) !== FALSE) {

                                if ($n > 1) {
                                   /* echo "<pre>";
                                    print_r($line);
                                    die;*/

                                    $property_category = array_search($line[1], Yii::$app->appHelperFunctions->propertiesCategoriesListArr);
                                    $city = array_search($line[3], Yii::$app->appHelperFunctions->emiratedListArr);
                                    $location = array_search($line[6], Yii::$app->appHelperFunctions->locationConditionsListArr);
                                    $tenure = array_search($line[7], Yii::$app->appHelperFunctions->buildingTenureArr);
                                    $property_placement = array_search($line[17], Yii::$app->appHelperFunctions->propertyPlacementListArr);
                                    $property_visibility = array_search($line[18], Yii::$app->appHelperFunctions->propertyVisibilityListArr);
                                    $property_exposure = array_search($line[19], Yii::$app->appHelperFunctions->propertyExposureListArr);
                                    $property_condition = array_search($line[20], Yii::$app->appHelperFunctions->propertyConditionListArr);
                                    $property = Properties::find()->where(['like', 'title', trim($line[2])])->asArray()->one();
                                    if ($property <> null) {
                                    } else {
                                        $property_new = new Properties();
                                        $property_new->title = trim($line[2]);
                                        $property_new->category = 1;
                                        $property_new->valuation_approach = 1;
                                        $property_new->save();
                                        $property = $property_new;
                                    }
                                    $community = Communities::find()->where(['like', 'title', trim($line[5])])->asArray()->one();
                                    if ($community <> null) {
                                    } else {
                                        $community_new = new Communities();
                                        $community_new->title = trim($line[5]);
                                        $community_new->city = $city;
                                        $community_new->save();
                                        $community = $community_new;
                                    }

                                    $sub_community = SubCommunities::find()->where(['like', 'title', trim($line[4])])->asArray()->one();
                                    if ($sub_community <> null) {
                                    } else {
                                        $sub_community_new = new SubCommunities();
                                        $sub_community_new->title = trim($line[4]);
                                        $sub_community_new->community = $community['id'];
                                        if($sub_community_new->save()) {
                                            $sub_community = $sub_community_new;
                                        }else{
                                           /* echo "<pre>";
                                            print_r($community);
                                            die;*/
                                        }
                                    }
                                    $developer = Developers::find()->where(['like', 'title', trim($line[10])])->asArray()->one();
                                    if ($developer <> null) {
                                    } else {
                                        $developer_new = new Developers();
                                        $developer_new->title = trim($line[10]);
                                        $developer_new->save();
                                        $developer = $developer_new;
                                    }
                                    $buildingId = 0;
                                    $buildingName = $line[0];
                                    $buildingRow = Buildings::find()->where(['like', 'title', trim($line[0])])->asArray()->one();


                                    if ($buildingRow <> null) {
                                    } else {


                                        $model = new Buildings();
                                        $model->title = $line[0];
                                        $model->property_category = $property_category;
                                        $model->property_id = ($property <> null) ? $property['id'] : 0;
                                        $model->city = $city;
                                        $model->community = $community['id'];
                                        $model->sub_community = ($sub_community['id'] <> null ) ? $sub_community['id'] : 0;;
                                       // $model->location = ($location <> null ) ? $location : 0;
                                        $model->street = $line[6];
                                        $model->tenure = $tenure;
                                        $model->latitude = $line[8];
                                        $model->longitude = $line[9];
                                        $model->developer_id = $developer['id'];
                                        $model->building_number = $line[11];
                                        $model->plot_number = $line[12];
                                        $model->makani_number = $line[13];
                                        $model->estimated_age = ($line[14] <> null && $line[14] !== '#N/A' ) ? $line[14] : 0;
                                        $model->utilities_connected = $line[15];
                                        $model->development_type = $line[16];
                                        $model->property_placement = $property_placement;
                                        $model->property_visibility = $property_visibility;
                                        $model->property_exposure = $property_exposure;
                                        $model->property_condition = $property_condition;
                                        $model->finished_status = $line[21];
                                        $model->pool = ($line[22] <> null) ? $line[22] : 'Yes';
                                        $model->gym = ($line[23] <> null) ? $line[23] : 'Yes';
                                        $model->play_area = ($line[24] <> null) ? $line[24] : 'Yes';
                                        $model->other_facilities = $line[25];
                                        $model->completion_status = ($line[26] <> null) ? $line[26] : 'Ready';
                                        $model->white_goods = ($line[27] <> null) ? $line[27] : 'No';
                                        $model->furnished = ($line[28] <> null) ? $line[28] : 'No';
                                        $model->landscaping = $line[29];
                                        $model->number_of_basement = $line[30];
                                        $model->parking_floors = $line[31];
                                        $model->typical_floors = $line[32];
                                        $model->no_of_units = ($line[33] <> null && $line[33] !== '-' ) ? str_replace( ',', '', $line[33]) : 0;
                                        $model->service_charges_psf = ($line[34] <> null && $line[34] !== '-' ) ? $line[34] : 0;
                                        $model->vacancy = ($line[35] <> null) ? $line[35] : 0;
                                        $model->location_highway_drive = ($line[36] <> null) ? str_replace(' ','_',$line[36]) : '';
                                        $model->location_school_drive = ($line[37] <> null) ? str_replace(' ','_',$line[37]) : '';
                                        $model->location_mall_drive = ($line[38] <> null) ? str_replace(' ','_',$line[38]) : '';
                                        $model->location_sea_drive = ($line[39] <> null) ? str_replace(' ','_',$line[39]) : '';
                                        $model->location_park_drive = ($line[40] <> null) ? str_replace(' ','_',$line[40]) : '';

                                        if ($model->save()) {
                                            $saved++;
                                        } else {
                                            if ($model->hasErrors()) {

                                                foreach ($model->getErrors() as $error) {
                                                    if (count($error) > 0) {
                                                        foreach ($error as $key => $val) {
                                                            echo $val;
                                                        }
                                                    }
                                                }
                                            }
                                            echo "<pre>";
                                            print_r($line);
                                            die;
                                            die();
                                            $errNames .= '<br />' . $buildingName;
                                            $unsaved++;
                                        }
                                    }


                                } else {
                                    $change[] = $buildingName;

                                }
                                $n++;
                            }
                            fclose($csvFile);
                            //Unlink File
                            unlink($this->importfile);
                        }
                    }
                }
            }
            if ($saved > 0) {

                Yii::$app->getSession()->setFlash('success', $saved . ' - ' . Yii::t('app', 'rows saved successfully'));
            }
            if ($unsaved > 0) {
                Yii::$app->getSession()->setFlash('error', $unsaved . ' - ' . Yii::t('app', 'rows were not saved!') . $errNames);
            }
            return true;
        } else {
            return false;
        }
    }
}
