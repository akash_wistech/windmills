<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "developments".
 *
 * @property int $id
 * @property int $emirates
 * @property int $property_type
 * @property int $star1
 * @property int $star2
 * @property int $star3
 * @property int $star4
 * @property int $star5
 * @property float $professional_charges
 * @property float $contingency_margin
 * @property float $obsolescence
 * @property float $developer_profit
 * @property int $parking_price
 * @property int $pool_price
 * @property int $landscape_price
 * @property int $whitegoods_price
 * @property int $utilities_connected_price
 * @property float $developer_margin
 * @property float $interest_rate
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $trashed_at
 * @property int|null $trashed
 * @property int|null $trashed_by
 */
class Developments extends ActiveRecordFull
{
    public $status_verified;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'developments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emirates', 'property_type', 'star1', 'star2', 'star3', 'star4', 'star5', 'professional_charges', 'contingency_margin', 'obsolescence', 'developer_profit', 'parking_price', 'pool_price', 'landscape_price', 'whitegoods_price', 'utilities_connected_price', 'developer_margin', 'interest_rate'], 'required'],
            [['emirates', 'property_type', 'star1', 'star2', 'star3', 'star4', 'star5', 'parking_price', 'pool_price', 'landscape_price', 'whitegoods_price', 'utilities_connected_price', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['professional_charges', 'contingency_margin', 'obsolescence', 'developer_profit', 'developer_margin', 'interest_rate','utilities_cost'], 'number'],
            [['created_at', 'updated_at', 'trashed_at','approved_by','approved_at','status'], 'safe'],
            [['status_verified','status_verified_at','status_verified_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'emirates' => 'Emirates',
            'property_type' => 'Property Type',
            'star1' => 'Star1',
            'star2' => 'Star2',
            'star3' => 'Star3',
            'star4' => 'Star4',
            'star5' => 'Star5',
            'professional_charges' => 'Professional Charges',
            'contingency_margin' => 'Contingency Margin',
            'obsolescence' => 'Obsolescence',
            'developer_profit' => 'Developer Profit',
            'parking_price' => 'Parking Price',
            'pool_price' => 'Pool Price',
            'landscape_price' => 'Landscape Price',
            'whitegoods_price' => 'Whitegoods Price',
            'utilities_connected_price' => 'Utilities Connected Price',
            'developer_margin' => 'Developer Margin',
            'interest_rate' => 'Interest Rate',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'trashed_at' => 'Trashed At',
            'trashed' => 'Trashed',
            'trashed_by' => 'Trashed By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Properties::className(), ['id' => 'property_type']);
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->title;
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Developments';
    }
    public function beforeSave($insert)
    {
        if($this->status == 1 &&  $this->approved_by == ''){
            $this->approved_by = Yii::$app->user->identity->id;
            $this->approved_at = date("Y-m-d H:i:s");
        }

        // verify status reset after edit only something changed
        $isChanged = false;
        $ignoredAttributes = ['updated_at','status_verified_at']; 
        foreach ($this->attributes as $attribute => $value) {
            if (in_array($attribute, $ignoredAttributes)) {
                continue;
            }
            if ($this->getOldAttribute($attribute) != $value) {
                $isChanged = true;
                break;
            }
        }
        if ($isChanged) {
            if($this->status == 1 && $this->getOldAttribute('status') == 1){
                $this->status = 2;
            }elseif ($this->status == 2 && $this->getOldAttribute('status') == 1) {
                $this->status = 1;
            }
        }
        
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    
}
