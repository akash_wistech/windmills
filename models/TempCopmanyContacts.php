<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "temp_copmany_contacts".
 *
 * @property int $id
 * @property int $company_id
 * @property string|null $email
 * @property string|null $firstname
 * @property string|null $lastname
 * @property int|null $job_title_id
 * @property int|null $department_id
 * @property string|null $mobile
 * @property string|null $mobile_2
 * @property int|null $country_id
 * @property int|null $source_of_contact
 * @property string|null $land_line
 */
class TempCopmanyContacts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'temp_copmany_contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id'], 'required'],
            [['company_id', 'job_title_id', 'department_id', 'country_id', 'source_of_contact'], 'integer'],
            [['email', 'firstname', 'lastname', 'mobile', 'mobile_2', 'land_line'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'email' => 'Email',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'job_title_id' => 'Job Title ID',
            'department_id' => 'Department ID',
            'mobile' => 'Mobile',
            'mobile_2' => 'Mobile  2',
            'country_id' => 'Country ID',
            'source_of_contact' => 'Source Of Contact',
            'land_line' => 'Land Line',
        ];
    }
}
