<?php

namespace app\models;

use Yii;


/**
* This is the model class for table "list_data".
*
* @property int $id
* @property int $listings_reference
* @property int $source
* @property int $listing_website_link
* @property int $listing_date
* @property int $building_info
* @property int $property_category
* @property int $no_of_bedrooms
* @property int $built_up_area
* @property int $land_size
* @property int $listings_price
* @property int $listings_rent
* @property int $final_price
* @property int $status
*/
class ListData extends \yii\db\ActiveRecord
{
    /**
    * {@inheritdoc}
    */
    public static function tableName()
    {
        return 'list_data';
    }
    

    
    /**
    * {@inheritdoc}
    */
    public function rules()
    {
        return [
            [['listings_reference', 'source', 'listing_website_link', 'listing_date', 'building_info', 
            'property_category', 'no_of_bedrooms', 'built_up_area', 'land_size', 'listings_price', 
            'listings_rent', 'final_price', 'status', 'property_type', 'community', 'sub_community',
            'purpose', 'move_to_listing','created_at','list_data_date'], 'safe'],
        ];
    }
    
    /**
    * {@inheritdoc}
    */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'listings_reference' => 'Listings Reference',
            'source' => 'Source',
            'listing_website_link' => 'Listing Website Link',
            'listing_date' => 'Listing Date',
            'building_info' => 'Building Info',
            'property_category' => 'Property Category',
            'no_of_bedrooms' => 'No Of Bedrooms',
            'built_up_area' => 'Built Up Area',
            'land_size' => 'Land Size',
            'listings_price' => 'Listings Price',
            'listings_rent' => 'Listings Rent',
            'final_price' => 'Final Price',
            'status' => 'Status',
        ];
    }
}
