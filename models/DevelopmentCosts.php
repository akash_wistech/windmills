<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "developments".
 *
 * @property int $id
 * @property int $emirates
 * @property int $property_type
 * @property int $star1
 * @property int $star2
 * @property int $star3
 * @property int $star4
 * @property int $star5
 * @property float $professional_charges
 * @property float $contingency_margin
 * @property float $obsolescence
 * @property float $developer_profit
 * @property int $parking_price
 * @property int $pool_price
 * @property int $landscape_price
 * @property int $whitegoods_price
 * @property int $utilities_connected_price
 * @property float $developer_margin
 * @property float $interest_rate
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property string|null $trashed_at
 * @property int|null $trashed
 * @property int|null $trashed_by
 */
class DevelopmentCosts extends ActiveRecordFull
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'development_costs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['emirates', 'property_type', 'star1', 'star2', 'star3', 'star4', 'star5', 'land_department_municipality_fee','civil_defence_fee','developer_margin',
                'green_building_certification_fee', 'demolition_fee', 'utilities_connected_price', 'professional_charges', 'designing_and_architecture_costs', 'project_management_costs',
                'demolition_costs', 'contracting_cost', 'parking_price', 'whitegoods_price','site_improvements','pool_price','landscape_price',
                'staff_accomodation_costs','pre_opening_expenses','mock_ups','contingency_margin','interest_rate','physical_depreciation','obsolescence',
                'developer_profit'], 'safe'],
            [['emirates', 'property_type', 'star1', 'star2', 'star3', 'star4', 'star5', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
          //  [['emirates', 'property_type', 'star1', 'star2', 'star3', 'star4', 'star5', 'parking_price', 'pool_price', 'landscape_price', 'whitegoods_price', 'utilities_connected_price', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['land_department_municipality_fee','civil_defence_fee','developer_margin',
                'green_building_certification_fee', 'demolition_fee', 'utilities_connected_price', 'professional_charges', 'designing_and_architecture_costs', 'project_management_costs',
                'demolition_costs', 'contracting_cost', 'parking_price', 'whitegoods_price','site_improvements','pool_price','landscape_price',
                'staff_accomodation_costs','pre_opening_expenses','mock_ups','contingency_margin','interest_rate','physical_depreciation','obsolescence',
                'developer_profit'], 'number'],
            [['created_at', 'updated_at', 'trashed_at','approved_by','approved_at','status'], 'safe'],
            [['debris_removal', 'basement_parking', 'multi_story_parking','boundry_wall_length'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'emirates' => 'Emirates',
            'property_type' => 'Property Type',
            'star1' => 'Star1',
            'star2' => 'Star2',
            'star3' => 'Star3',
            'star4' => 'Star4',
            'star5' => 'Star5',
            'professional_charges' => 'Professional Charges',
            'contingency_margin' => 'Contingency Margin',
            'obsolescence' => 'Obsolescence',
            'developer_profit' => 'Developer Profit',
            'parking_price' => 'Parking Price',
            'pool_price' => 'Pool Price',
            'landscape_price' => 'Landscape Price',
            'whitegoods_price' => 'Whitegoods Price',
            'utilities_connected_price' => 'Utilities Connected Price',
            'developer_margin' => 'Developer Margin',
            'interest_rate' => 'Interest Rate',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'trashed_at' => 'Trashed At',
            'trashed' => 'Trashed',
            'trashed_by' => 'Trashed By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Properties::className(), ['id' => 'property_type']);
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->title;
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Development Costs';
    }
    public function beforeSave($insert)
    {
        if($this->status == 1 &&  $this->approved_by == ''){
            $this->approved_by = Yii::$app->user->identity->id;
            $this->approved_at = date("Y-m-d H:i:s");
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }
}
