<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
* This is the model class for table "{{%workflow}}".
*
* @property integer $id
* @property string $title
* @property integer $is_financial
* @property string $financial_module
* @property string $financial_module_field
* @property integer $completion_day
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
* @property integer $trashed
* @property string $trashed_at
* @property integer $trashed_by
*/
class Workflow extends ActiveRecordFull
{
	public $assigned_modules,$assigned_services;
	//Stage Attrs
	public $s_id,$s_name,$s_color,$s_descp,$s_sort_order;
	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%workflow}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['title'],'required'],
			[['s_sort_order','created_at','updated_at','trashed_at'],'safe'],
			[[
				'is_financial','completion_day','created_by','updated_by','trashed','trashed_by'
			],'integer'],
			[[
				'title','financial_module','financial_module_field'
			],'string'],
      [['title'],'trim'],
      [['s_id','assigned_services'],'each','rule'=>['integer']],
      [['assigned_modules','s_name','s_color','s_descp'],'each','rule'=>['string']],
		];
	}

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Process Name'),
			'is_financial' => Yii::t('app', 'Financial'),
			'completion_day' => Yii::t('app', 'Completion Days'),
			'financial_module' => Yii::t('app', 'Financial Module'),
			'financial_module_field' => Yii::t('app', 'Financial Module Field'),
			'assigned_modules' => Yii::t('app', 'Assigned to Modules'),
			'assigned_services' => Yii::t('app', 'Assigned to Services'),
			'created_at' => Yii::t('app', 'Created At'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'updated_by' => Yii::t('app', 'Updated By'),
			'trashed' => Yii::t('app', 'Trashed'),
			'trashed_at' => Yii::t('app', 'Trashed At'),
			'trashed_by' => Yii::t('app', 'Trashed By'),

			's_name' => Yii::t('app', 'Name'),
			's_color' => Yii::t('app', 'Color'),
			's_descp' => Yii::t('app', 'Description'),
		];
	}

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecTitle()
  {
    return $this->title;
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecType()
  {
    return 'Workflow';
  }

	/**
	* @inheritdoc
	*/
	public function afterSave($insert, $changedAttributes)
	{
		//Saving Assigned To
		if($this->assigned_modules!=null){
			WorkflowModule::deleteAll(['and',['workflow_id'=>$this->id],['not in','module_type',$this->assigned_modules]]);
			foreach($this->assigned_modules as $key=>$val){
				$managerRow=WorkflowModule::find()->where(['workflow_id'=>$this->id,'module_type'=>$val])->one();
				if($managerRow==null){
					$managerRow=new WorkflowModule;
					$managerRow->workflow_id=$this->id;
					$managerRow->module_type=$val;
					$managerRow->save();
				}
			}
		}
		//Saving Service types
		if($this->assigned_services!=null){
			WorkflowServiceType::deleteAll(['and',['workflow_id'=>$this->id],['not in','service_type',$this->assigned_services]]);
			foreach($this->assigned_services as $key=>$val){
				$managerRow=WorkflowServiceType::find()->where(['workflow_id'=>$this->id,'service_type'=>$val])->one();
				if($managerRow==null){
					$managerRow=new WorkflowServiceType;
					$managerRow->workflow_id=$this->id;
					$managerRow->service_type=$val;
					$managerRow->save();
				}
			}
		}
		if($this->s_name!=null){
			$r=1;
			foreach($this->s_name as $key=>$val){
				$stageName=$this->s_name[$key];
				$stageColor=$this->s_color[$key];
				$stageDescp=$this->s_descp[$key];
				if(isset($this->s_id[$key]) && $this->s_id[$key]>0){
					$stageRow=WorkflowStage::find()->where(['workflow_id'=>$this->id,'id'=>$this->s_id[$key]])->one();
				}else{
					$stageRow=new WorkflowStage;
					$stageRow->workflow_id=$this->id;
				}
				$stageRow->title=$stageName;
				$stageRow->color_code=$stageColor;
				$stageRow->descp=$stageDescp;
				$stageRow->sort_order=$r;
				$stageRow->save();
				$r++;
			}
		}
		parent::afterSave($insert, $changedAttributes);
	}

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getStages()
  {
    return $this->hasMany(WorkflowStage::className(), ['workflow_id' => 'id'])->where(['trashed'=>0])->orderBy(['sort_order'=>SORT_ASC]);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
	public function getWorkflowSelectedModules()
	{
		return WorkflowModule::find()->select(['module_type'])->where(['workflow_id'=>$this->id])->asArray()->all();
	}

  /**
  * @return \yii\db\ActiveQuery
  */
	public function getWorkflowSelectedServices()
	{
		return WorkflowServiceType::find()->select(['service_type'])->where(['workflow_id'=>$this->id])->asArray()->all();
	}
}
