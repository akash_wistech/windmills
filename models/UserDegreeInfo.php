<?php

namespace app\models;

use Yii;

class UserDegreeInfo extends \yii\db\ActiveRecord
{
    public $status_verified;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_degree_info';
    }
}
