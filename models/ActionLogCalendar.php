<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%action_log_calendar}}".
*
* @property integer $action_log_id
* @property integer $calendar_id
*/
class ActionLogCalendar extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%action_log_calendar}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['action_log_id', 'calendar_id'], 'required'],
      [['action_log_id', 'calendar_id'], 'integer'],
    ];
  }

  public static function primaryKey()
  {
  	return ['action_log_id','calendar_id'];
  }
}
