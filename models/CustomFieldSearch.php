<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use app\models\CustomField;

/**
* CustomFieldSearch represents the model behind the search form about `app\models\CustomField`.
*/
class CustomFieldSearch extends CustomField
{
  public $module_id,$pageSize;

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [[
        'id','is_required','selection_type','show_in_grid','sort_order','status',
        'created_by','updated_by','pageSize'
      ],'integer'],
      [['input_type','module_id','title','short_name','created_at','updated_at','trashed_at'],'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $query = $this->generateQuery($params);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
      'sort' => false,//$this->defaultSorting,
    ]);

    return $dataProvider;
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function searchMy($params)
  {
    $query = $this->generateQuery($params);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
      'sort' => false,//$this->defaultSorting,
    ]);

    return $dataProvider;
  }

  /**
  * Generates query for the search
  *
  * @param array $params
  *
  * @return ActiveRecord
  */
  public function generateQuery($params)
  {
    $this->load($params);

    $query = CustomField::find();

    $query->andFilterWhere([
      CustomField::tableName().'.id' => $this->id,
      CustomField::tableName().'.input_type' => $this->input_type,
      CustomField::tableName().'.is_required' => $this->is_required,
      CustomField::tableName().'.selection_type' => $this->selection_type,
      CustomField::tableName().'.show_in_grid' => $this->show_in_grid,
      CustomField::tableName().'.status' => $this->status,
      CustomField::tableName().'.trashed' => 0,
    ]);
    if($this->module_id!=null){
      $subQueryModules=CustomFieldModule::find()->select(['custom_field_id'])->where(['module_type'=>$this->module_id]);
      $query->andFilterWhere([CustomField::tableName().'.id' => $subQueryModules]);
    }

    $query->andFilterWhere(['like', CustomField::tableName().'.title', $this->title])
    ->andFilterWhere(['like', CustomField::tableName().'.short_name', $this->short_name]);

    return $query;
  }

  /**
  * Default Sorting Options
  *
  * @param array $params
  *
  * @return Array
  */
  public function getDefaultSorting()
  {
    return [
      'defaultOrder' => ['sort_order'=>SORT_ASC],
      'attributes' => [
        'input_type',
        'title',
        'short_name',
        'is_required',
        'selection_type',
        'show_in_grid',
        'sort_order',
        'status',
        'created_at',
      ]
    ];
  }
}
