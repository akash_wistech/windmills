<?php

namespace app\models;

use Yii;

class WhatsappGroups extends \yii\db\ActiveRecord
{
    public $status_verified;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'whatsapp_groups';
    }
}
