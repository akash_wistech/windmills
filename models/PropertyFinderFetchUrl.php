<?php
namespace app\models;
use Yii;

class PropertyFinderFetchUrl extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return '{{%property_finder_fetch_url}}';
	}

	public function rules()
	{
		return [
			[['property_finder_url_id'],'integer'],
			[['url'],'safe'],
		];
	}
}
