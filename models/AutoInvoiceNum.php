<?php
namespace app\models;
use yii\db\ActiveRecord;

use Yii;

class AutoInvoiceNum extends ActiveRecord
{
	public static function tableName()
	{
		return '{{%auto_invoice_num}}';
	}
	
	public function rules()
	{
		return [
			[['type','valuation_id','client_id','client_current_inv_num','maxima_inv_num','year_inv_num','client_inv_no','year','city','created_at','created_by','quotation_id'],'safe'],
		];
	}
}
