<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Gfaweightage;

/**
 * GfaweightageSearch represents the model behind the search form of `app\models\Gfaweightage`.
 */
class GfaweightageSearch extends Gfaweightage
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['difference','difference_2', 'bigger_sp', 'smaller_sp'], 'number'],
            [['created_at','difference_2', 'updated_at', 'trashed_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Gfaweightage::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'difference' => $this->difference,
            'difference_2' => $this->difference_2,
            'bigger_sp' => $this->bigger_sp,
            'smaller_sp' => $this->smaller_sp,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => $this->trashed,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
        ]);

        return $dataProvider;
    }
}
