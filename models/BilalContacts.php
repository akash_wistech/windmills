<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bilal_contacts".
 *
 * @property int $id
 * @property string|null $company
 * @property int|null $company_id
 * @property string|null $firstname
 * @property string|null $lastname
 * @property string|null $email
 * @property string|null $phone
 * @property string|null $phone_code
 * @property string|null $land_line_code
 * @property string|null $landline
 * @property string|null $jobtitle
 * @property int|null $jobtitle_id
 * @property string|null $linkedin
 * @property int|null $status_verified
 * @property string|null $status_verified_at
 * @property int|null $status_verified_by
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property string|null $deleted_at
 * @property int|null $deleted_by
 */
class BilalContacts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bilal_contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company', 'company_id', 'phone_code', 'phone', 'land_line_code', 'landline', 'jobtitle','linkedin'], 'safe'],
            [['status_verified', 'status_verified_at', 'status_verified_by','job_title_id'], 'safe'],

            ['phone', 'match', 'pattern' => '/^\d{7}$/', 'message' => 'Please enter a valid 7-digit phone number.'],
            ['landline', 'match', 'pattern' => '/^\d{7}$/', 'message' => 'Please enter a valid 7-digit phone number.'],

            [['email', 'firstname', 'lastname'], 'safe'],
            ['email','email'],
          //  ['email','unique'],
            ['password','safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company' => 'Company',
            'company_id' => 'Company ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'email' => 'Email',
            'phone' => 'Phone',
            'phone_code' => 'Phone Code',
            'land_line_code' => 'Land Line Code',
            'landline' => 'Landline',
            'jobtitle' => 'Jobtitle',
            'jobtitle_id' => 'Jobtitle ID',
            'linkedin' => 'Linkedin',
            'status_verified' => 'Status Verified',
            'status_verified_at' => 'Status Verified At',
            'status_verified_by' => 'Status Verified By',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'deleted_at' => 'Deleted At',
            'deleted_by' => 'Deleted By',
        ];
    }

    public function beforeSave($insert){

        
        // verify status reset after edit only something changed
        $isChanged = false;
        $ignoredAttributes = ['updated_at','status_verified_at']; 
        foreach ($this->attributes as $attribute => $value) {
            if (in_array($attribute, $ignoredAttributes)) {
                continue;
            }
            if ($this->getOldAttribute($attribute) != $value) {
                $isChanged = true;
                break;
            }
        }
        if ($isChanged) {
            if($this->status_verified == 1 && $this->getOldAttribute('status_verified') == 1){
                $this->status_verified = 2;
            }elseif ($this->status_verified == 2 && $this->getOldAttribute('status_verified') == 1) {
                $this->status_verified = 1;
            }
        }
        
        return parent::beforeSave($insert);
    }
}
