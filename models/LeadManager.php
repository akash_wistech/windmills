<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%lead_manager}}".
*
* @property integer $lead_id
* @property integer $staff_id
*/
class LeadManager extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%lead_manager}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['lead_id', 'staff_id'], 'required'],
      [['lead_id', 'staff_id'], 'integer'],
    ];
  }

  public static function primaryKey()
  {
  	return ['lead_id','staff_id'];
  }
}
