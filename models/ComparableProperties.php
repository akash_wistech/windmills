<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "comparable_properties".
 *
 * @property int $id
 * @property int $status
 * @property int|null $created_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 * @property int|null $status_verified
 * @property int|null $status_verified_by
 * @property string|null $status_verified_at
 * @property int|null $valuation_id
 * @property int|null $valuation_approach
 * @property int|null $avg_price_recent_sold
 * @property int|null $cavg_price_sqft_sold
 * @property int|null $avg_price_sold_adjusted
 * @property int|null $avg_price_sqft_sold_adjusted
 * @property int|null $avg_price_recent_market
 * @property int|null $avg_price_sqft_market
 * @property int|null $avg_price_market_adjusted
 * @property int|null $avg_price_sqft_market_adjusted
 */
class ComparableProperties extends ActiveRecordFull
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comparable_properties';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['avg_price_recent_sold', 'avg_price_sqft_sold', 'avg_price_sold_adjusted', 'avg_price_sqft_sold_adjusted', 'avg_price_recent_market', 'avg_price_sqft_market', 'avg_price_market_adjusted', 'avg_price_sqft_market_adjusted'], 'required'],
            [['status', 'created_by', 'updated_by', 'trashed', 'trashed_by', 'status_verified', 'status_verified_by', 'valuation_id', 'avg_price_recent_sold', 'avg_price_sqft_sold', 'avg_price_sold_adjusted', 'avg_price_sqft_sold_adjusted', 'avg_price_recent_market', 'avg_price_sqft_market', 'avg_price_market_adjusted', 'avg_price_sqft_market_adjusted'], 'integer'],
            [['created_at', 'updated_at', 'trashed_at', 'status_verified_at','valuation_approach'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
            'status_verified' => 'Status Verified',
            'status_verified_by' => 'Status Verified By',
            'status_verified_at' => 'Status Verified At',
            'valuation_id' => 'Valuation ID',
            'avg_price_recent_sold' => 'Average Transaction Price of the Recent Sold',
            'avg_price_sqft_sold' => 'Average Transaction Price/Sqft of the Sold',
            'avg_price_sold_adjusted' => 'Adjusted Average Transaction Price Recent Sold',
            'avg_price_sqft_sold_adjusted' => 'Adjusted Average Transaction Price/Sqft Sold',
            'avg_price_recent_market' => 'Average Transaction Price of the Recent Market',
            'avg_price_sqft_market' => 'Average Transaction Price/Sqft of the Market',
            'avg_price_market_adjusted' => 'Adjusted Average Transaction Price Market',
            'avg_price_sqft_market_adjusted' => 'Adjusted Average Transaction Price/Sqft Market',
        ];
    }
    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Comparable Properties';
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->id;
    }
    public function afterSave($insert, $changedAttributes)
    {

        // $valuation = InspectProperty::findOne($this->valuation_id);
        // Yii::$app->db->createCommand()->update('inspect_property',
        //     ['green_efficient_certification' =>  $this->green_efficient_certification,
        //         'certifier_name' =>  $this->certifier_name,
        //         'certification_level' =>  $this->certification_level,
        //         'source_of_green_certificate_information' =>  $this->source_of_green_certificate_information,
        //         ],
        //     ['valuation_id' => $this->valuation_id])->execute();



        // parent::afterSave($insert, $changedAttributes);
    }
}
