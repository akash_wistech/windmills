<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "merge_project_childs".
 *
 * @property int $id
 * @property int|null $building_id
 */
class MergeClientChilds extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'merge_client_childs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id','merge_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
        ];
    }
}
