<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AutoListingsMax;

/**
 * AutoListingsMaxSearch represents the model behind the search form of `app\models\AutoListingsMax`.
 */
class AutoListingsMaxSearch extends AutoListingsMax
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'too_less_records_limit_from',
                'too_less_records_limit_to', 'too_less_date', 'less_records_limit_from', 'many_date', 'too_many_records_limit_from', 'too_many_records_limit_to', 'too_many_date', 'status', 'updated_by', 'search_limit', 'status_verified_by', 'city_id'], 'integer'],
            [['data_type', 'updated_at', 'status_verified_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AutoListingsMax::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere(['like', 'data_type', $this->data_type]);

        return $dataProvider;
    }
}
