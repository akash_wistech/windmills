<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ActionLogAttachment;

/**
* ActionLogAttachmentSearch represents the model behind the search form about `app\models\ActionLogAttachment`.
*/
class ActionLogAttachmentSearch extends ActionLogAttachment
{
  public $module_type,$module_id,$pageSize;

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','module_id','status','pageSize'],'integer'],
      [['module_type'],'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);

    $query = ActionLog::find()
    ->innerJoin(ActionLogAttachment::tableName(),ActionLogAttachment::tableName().".action_log_id=".ActionLog::tableName().".id");

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
    ]);

    $query->andFilterWhere([
      ActionLog::tableName().'.rec_type' => 'attachment',
      ActionLog::tableName().'.module_type' => $this->module_type,
      ActionLog::tableName().'.module_id' => $this->module_id,
      ActionLog::tableName().'.trashed' => 0,
    ]);

    return $dataProvider;
  }
}
