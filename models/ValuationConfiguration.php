<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "valuation_configuration".
 *
 * @property int $id
 * @property int $valuation_id
 * @property int $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 * @property int $over_all_upgrade
 */
class ValuationConfiguration extends ActiveRecordFull
{
    public $config_studios = [];
    public $config_one_bedroom = [];
    public $config_two_bedrooms = [];
    public $config_three_bedrooms = [];
    public $config_four_bedrooms = [];
    public $config_penthouse = [];
    public $config_commercial_unit = [];
    public $config_retail_unit = [];
    public $config_bedrooms = [];
    public $config_bathrooms = [];
    public $config_kitchen= [];
    public $config_living_area= [];
    public $config_dining_area= [];
    public $config_maid_rooms= [];
    public $config_laundry_area= [];
    public $config_store= [];
    public $config_service_block= [];
    public $config_garage= [];
    public $config_balcony= [];
    public $config_flooring= [];
    public $config_ceiling= [];
    public $config_view= [];
    public $config_general_elevation= [];
    public $config_unit_tag= [];
    public $config_electricity_board = [];
    public $config_balcony_covered_with_roof = [];
    public $config_extension_permision_document = [];
    public $config_extension_completion_document = [];
    public $config_family_room=[];
    public $config_powder_room=[];
    public $config_study_room=[];
    public $no_of_room_type;
    public $image_checked;
    public $custom_fields=[];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'valuation_configuration';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['valuation_id'], 'required'],
            [['valuation_id', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['over_all_upgrade'], 'number'],
            [['created_at', 'updated_at', 'trashed_at', 'config_bedrooms','config_studios','config_one_bedroom','config_two_bedrooms','config_three_bedrooms','config_four_bedrooms','config_penthouse','config_bathrooms','config_kitchen','config_living_area','config_dining_area', 'config_commercial_unit', 'config_retail_unit',
                'config_maid_rooms','config_laundry_area','config_store','config_service_block','config_garage','config_balcony','config_flooring','config_ceiling','config_view','config_general_elevation','config_unit_tag','config_electricity_board','config_family_room','config_powder_room','config_study_room','image_checked','custom_fields','config_balcony_covered_with_roof','config_extension_permision_document','config_extension_completion_document'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'valuation_id' => 'Valuation ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
            'over_all_upgrade' => 'Over All Upgrade',
        ];
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Valuation Configuration';
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->id;
    }

    public function afterSave($insert, $changedAttributes)
    {

        

        if (isset($this->config_studios) && $this->config_studios <> null) {
            $this->addCongurationsData('config_studios', $this->config_studios);
        }
        if (isset($this->config_one_bedroom) && $this->config_one_bedroom <> null) {
            $this->addCongurationsData('config_one_bedroom', $this->config_one_bedroom);
        }
        if (isset($this->config_two_bedrooms) && $this->config_two_bedrooms <> null) {
            $this->addCongurationsData('config_two_bedrooms', $this->config_two_bedrooms);
        }
        if (isset($this->config_three_bedrooms) && $this->config_three_bedrooms <> null) {
            $this->addCongurationsData('config_three_bedrooms', $this->config_three_bedrooms);
        }
        if (isset($this->config_four_bedrooms) && $this->config_four_bedrooms <> null) {
            $this->addCongurationsData('config_four_bedrooms', $this->config_four_bedrooms);
        }
        if (isset($this->config_penthouse) && $this->config_penthouse <> null) {
            $this->addCongurationsData('config_penthouse', $this->config_penthouse);
        }
        if (isset($this->config_commercial_unit) && $this->config_commercial_unit <> null) {
            $this->addCongurationsData('config_commercial_unit', $this->config_commercial_unit);
        }
        if (isset($this->config_retail_unit) && $this->config_retail_unit <> null) {
            $this->addCongurationsData('config_retail_unit', $this->config_retail_unit);
        }

        if (isset($this->config_bedrooms) && $this->config_bedrooms <> null) {
            $this->addCongurationsData('config_bedrooms', $this->config_bedrooms);
        }
        if (isset($this->config_bathrooms) && $this->config_bathrooms <> null) {

            $this->addCongurationsData('config_bathrooms', $this->config_bathrooms);
        }
        if (isset($this->config_kitchen) && $this->config_kitchen <> null) {

            $this->addCongurationsData('config_kitchen', $this->config_kitchen);
        }
        if (isset($this->config_living_area) && $this->config_living_area <> null) {

            $this->addCongurationsData('config_living_area', $this->config_living_area);
        }
        if (isset($this->config_dining_area) && $this->config_dining_area <> null) {

            $this->addCongurationsData('config_dining_area', $this->config_dining_area);
        }
        if (isset($this->config_maid_rooms) && $this->config_maid_rooms <> null) {

            $this->addCongurationsData('config_maid_rooms', $this->config_maid_rooms);
        }
        if (isset($this->config_laundry_area) && $this->config_laundry_area <> null) {

            $this->addCongurationsData('config_laundry_area', $this->config_laundry_area);
        }
        if (isset($this->config_store) && $this->config_store <> null) {

            $this->addCongurationsData('config_store', $this->config_store);
        }
        if (isset($this->config_service_block) && $this->config_service_block <> null) {

            $this->addCongurationsData('config_service_block', $this->config_service_block);
        }
        if (isset($this->config_garage) && $this->config_garage <> null) {

            $this->addCongurationsData('config_garage', $this->config_garage);
        }
        if (isset($this->config_balcony) && $this->config_balcony <> null) {

            $this->addCongurationsData('config_balcony', $this->config_balcony);
        }
        if (isset($this->config_family_room) && $this->config_family_room <> null) {

            $this->addCongurationsData('config_family_room', $this->config_family_room);
        }
        if (isset($this->config_powder_room) && $this->config_powder_room <> null) {

            $this->addCongurationsData('config_powder_room', $this->config_powder_room);
        }
        if (isset($this->config_study_room) && $this->config_study_room <> null) {

            $this->addCongurationsData('config_study_room', $this->config_study_room);
        }
       /* if (isset($this->config_flooring) && $this->config_flooring <> null) {

            $this->addCongurationsData('config_flooring', $this->config_flooring);
        }
        if (isset($this->config_ceiling) && $this->config_ceiling <> null) {

            $this->addCongurationsData('config_ceiling', $this->config_ceiling);
        }*/
        if (isset($this->config_view) && $this->config_view <> null) {

            $this->addCongurationsData('config_view', $this->config_view);
        }
        if (isset($this->config_general_elevation) && $this->config_general_elevation <> null) {

            $this->addCongurationsData('config_general_elevation', $this->config_general_elevation);
        }
        if (isset($this->config_unit_tag) && $this->config_unit_tag <> null) {

            $this->addCongurationsData('config_unit_tag', $this->config_unit_tag);
        }
        if (isset($this->config_electricity_board) && $this->config_electricity_board <> null) {

            $this->addCongurationsData('config_electricity_board', $this->config_electricity_board);
        }
        if (isset($this->config_balcony_covered_with_roof) && $this->config_balcony_covered_with_roof <> null) {

            $this->addCongurationsData('config_balcony_covered_with_roof', $this->config_balcony_covered_with_roof);
        }
        if (isset($this->config_extension_permision_document) && $this->config_extension_permision_document <> null) {

            $this->addCongurationsData('config_extension_permision_document', $this->config_extension_permision_document);
        }
        if (isset($this->config_extension_completion_document) && $this->config_extension_completion_document <> null) {

            $this->addCongurationsData('config_extension_completion_document', $this->config_extension_completion_document);
        }

        if (isset($this->custom_fields) && $this->custom_fields <> null) {

            $this->addCongurationsDataCustom('custom_fields', $this->custom_fields);
        }


        $upgrades_value = Yii::$app->appHelperFunctions->getUpgradeAttributes($this);
        if($upgrades_value <> null){
            \Yii::$app->db->createCommand("UPDATE valuation_configuration SET over_all_upgrade=".$upgrades_value." WHERE id=".$this->id)->execute();
        }


/*
        $totalResults = (new \yii\db\Query())
            ->select('AVG(upgrade) as avg_upgrade')
            //->select('*')
            ->from('configuration_files')
            ->where(['valuation_id' => $this->valuation_id])
            ->one();

        if($totalResults <> null){
            \Yii::$app->db->createCommand("UPDATE valuation_configuration SET over_all_upgrade=".$totalResults['avg_upgrade']." WHERE id=".$this->id)->execute();
        }*/
        $configuration = \app\models\ConfigurationFiles::find()->select(['id','type','attachment','custom_field_id'])->where(['valuation_id' => $this->valuation_id, "checked_image"=>'on'])->asArray()->all();

        if($configuration <> null) {
            foreach ($configuration as $key => $value) {



                if ($value['attachment'] != null && $value['type'] != '') {



                    $image = Yii::$app->helperFunctions->resize_update($value['attachment']);
                    if($image <>  null) {

                        \Yii::$app->db->createCommand("UPDATE configuration_files SET rep_image='" . $image . "' WHERE id=" . $value['id'])->execute();
                    }
                }
            }
        }

        $upgrades_value = Yii::$app->appHelperFunctions->getUpgradeAttributes($this);
        if($upgrades_value <> null){
            \Yii::$app->db->createCommand("UPDATE valuation_configuration SET over_all_upgrade=".$upgrades_value." WHERE id=".$this->id)->execute();
        }
        $model_val = Valuation::find()->where(['id' => $this->valuation_id])->one();
        if($model_val->valuation_approach == 0 && $model_val->client_id == 9166 ) {
            $configuration = \app\models\ConfigurationFiles::find()->select(['id', 'type', 'attachment', 'custom_field_id'])->where(['valuation_id' => $model_val->id, 'attachment' => ''])->asArray()->all();
            if ($configuration <> null && !empty($configuration)) {
                Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 13,'proceed'=>1], 'id='.$model_val->id.'')->execute();
                Yii::$app->getSession()->addFlash('error', Yii::t('app', "In-complete Inspection, Status changed to Partial Inspection Valuation"));
                return false;
            }else {
                if ($model_val->valuation_status == 13) {
                    Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 2], 'id=' . $model_val->id . '')->execute();
                }
            }
        }


        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }

    public function addCongurationsData($type, $require_data)
    {

        ConfigurationFiles::deleteAll(['valuation_id' => $this->valuation_id, 'type' => $type]);
        // Save all payments terms

        foreach ($require_data as $config_data) {


            if($config_data['attachment'] <> null){
                $name_array = explode('/',$config_data['attachment']);
                if(file_exists('cache/' .end($name_array))){
                    $attachments = 'cache/' .end($name_array);
                }else{
                    $attachments=null;
                }
            }
            $bed_detail = new ConfigurationFiles();
            $bed_detail->type = $type;
            $bed_detail->floor = $config_data['floor'];
            $bed_detail->flooring = $config_data['flooring'];
            $bed_detail->ceilings = $config_data['ceilings'];
            $bed_detail->speciality = $config_data['speciality'];
            $bed_detail->upgrade = 3;
            $bed_detail->attachment = $config_data['attachment'];
            $bed_detail->index_id = $config_data['index_id'];
            $bed_detail->valuation_id = $config_data['valuation_id'];
            $bed_detail->checked_image = $config_data['image_checked'];
            $bed_detail->rep_image = $attachments;
            $bed_detail->save();
        }
    }

    public function addCongurationsDataCustom($type, $require_data)
    {

      /*  echo "<pre>";
        print_r($require_data);
        die;*/
        ConfigurationFiles::deleteAll(['valuation_id' => $this->valuation_id, 'type' => $type]);
        // Save all payments terms
        foreach ($require_data as $config_data) {
            if($config_data['attachment'] <> null){
                $name_array = explode('/',$config_data['attachment']);
                if(file_exists('cache/' .end($name_array))){
                    $attachments = 'cache/' .end($name_array);
                }else{
                    $attachments=null;
                }
            }
            $bed_detail = new ConfigurationFiles();
            $bed_detail->type = $type;
            $bed_detail->field_name = $config_data['field_name'];
            $bed_detail->floor = $config_data['floor'];
            $bed_detail->flooring = $config_data['flooring'];
            $bed_detail->ceilings = $config_data['ceilings'];
            $bed_detail->speciality = $config_data['speciality'];
            $bed_detail->upgrade = 3;
            $bed_detail->attachment = $config_data['attachment'];
            $bed_detail->index_id = $config_data['index_id'];
            $bed_detail->valuation_id = $config_data['valuation_id'];
            $bed_detail->checked_image = $config_data['image_checked'];
            $bed_detail->custom_field_id = $config_data['custom_field_id'];
            $bed_detail->rep_image = $attachments;
            $bed_detail->save();
        }
    }
}
