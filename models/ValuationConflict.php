<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "valuation_conflict".
 *
 * @property int $id
 * @property string $related_to_buyer
 * @property string $related_to_seller
 * @property string $related_to_client
 * @property string $related_to_property
 * @property int $valuation_id
 * @property int $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class ValuationConflict extends ActiveRecordFull
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'valuation_conflict';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['related_to_buyer', 'related_to_seller', 'related_to_client', 'related_to_property'], 'string'],
            [['valuation_id'], 'required'],
            [['valuation_id', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['created_at', 'updated_at', 'trashed_at','related_to_buyer_reason','related_to_seller_reason','related_to_client_reason','related_to_property_reason'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'related_to_buyer' => 'Related To Buyer',
            'related_to_seller' => 'Related To Seller',
            'related_to_client' => 'Related To Client',
            'related_to_property' => 'Related To Property',
            'valuation_id' => 'Valuation ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Valuation Conflict';
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->valuation_id;
    }
}
