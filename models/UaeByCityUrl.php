<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "uae_by_city_url".
 *
 * @property int $id
 * @property int|null $city_id
 * @property string|null $url
 */
class UaeByCityUrl extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'uae_by_city_url';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id'], 'integer'],
            [['url', 'purpose'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'url' => 'Url',
        ];
    }
}
