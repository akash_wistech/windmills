<?php

namespace app\models;

use Yii;

// dd($_REQUEST);

class AssetManager extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asset_manager';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['asset_name' , 'category_id' , 'depreciation' , 'transaction_type'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'asset_name' => 'asset_name',
            'category_id' => 'category_id',
            'depreciation' => 'depreciation',
            'transaction_type' => 'transaction_type',
        ];
    }
}
