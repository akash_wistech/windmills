<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "branch".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $address
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $email
 * @property int|null $mobile
 * @property int|null $status
 * @property string|null $bank_name
 * @property string|null $account_holder_name
 * @property int|null $account_number
 * @property int|null $branch_code
 * @property int|null $swift_code
 * @property int|null $iban_number
 */
class Branch extends \yii\db\ActiveRecord
{
    public $status_verified;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'branch';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address','office_phone','mobile','trn_number'], 'string'],
            [['status', 'account_number', 'branch_code', 'swift_code', 'zone_list'], 'integer'],
            [['title', 'first_name', 'last_name', 'email', 'bank_name', 'account_holder_name', 'iban_number','website','company'], 'string', 'max' => 255],
            [['status_verified','status_verified_at','status_verified_by'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'address' => Yii::t('app', 'Address'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'email' => Yii::t('app', 'Email'),
            'mobile' => Yii::t('app', 'Mobile'),
            'status' => Yii::t('app', 'Status'),
            'bank_name' => Yii::t('app', 'Bank Name'),
            'account_holder_name' => Yii::t('app', 'Account Holder Name'),
            'account_number' => Yii::t('app', 'Account Number'),
            'branch_code' => Yii::t('app', 'Branch Code'),
            'swift_code' => Yii::t('app', 'Swift Code'),
            'iban_number' => Yii::t('app', 'Iban Number'),
            'zone_list' => Yii::t('app', 'City'),
            'office_phone' => Yii::t('app', 'Office Phone'),
            'website' => Yii::t('app', 'Website'),
            'company' => Yii::t('app', 'Company'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return BranchQuery the active query used by this AR class.
     */
   /* public static function find()
    {
        return new BranchQuery(get_called_class());
    }*/
}
