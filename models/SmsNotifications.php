<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sms_notifications".
 *
 * @property int $id
 * @property string|null $sms_body
 * @property int|null $type
 * @property int|null $module
 */
class SmsNotifications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms_notifications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sms_body'], 'string'],
            [['type', 'module'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sms_body' => 'Sms Body',
            'type' => 'Type',
            'module' => 'Module',
        ];
    }
}
