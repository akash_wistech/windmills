<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "floor_price".
 *
 * @property int $id
 * @property int|null $client
 * @property int|null $property_type
 * @property int|null $sub_property_type
 * @property int|null $city
 * @property int|null $tenure
 * @property string|null $tat
 * @property float $fee
 * @property int|null $valuation_approach
 * @property int|null $inspection_type
 * @property int|null $priority_type
 * @property float|null $revalidation_fee
 * @property float|null $independent_villa_fee
 * @property float|null $community_villa_fee
 * @property float|null $staff_fee
 * @property int|null $status_verified
 * @property string|null $status_verified_at
 * @property int|null $status_verified_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 */
class FloorPrice extends ActiveRecordFull
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'floor_price';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client', 'property_type', 'sub_property_type', 'city', 'tenure', 'valuation_approach', 'inspection_type', 'priority_type', 'status_verified', 'status_verified_by', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['fee', 'revalidation_fee', 'independent_villa_fee', 'community_villa_fee', 'staff_fee'], 'number'],
            [['status_verified_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['tat'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client' => 'Client',
            'property_type' => 'Property Type',
            'sub_property_type' => 'Sub Property Type',
            'city' => 'City',
            'tenure' => 'Tenure',
            'tat' => 'Tat',
            'fee' => 'Fee',
            'valuation_approach' => 'Valuation Approach',
            'inspection_type' => 'Inspection Type',
            'priority_type' => 'Priority Type',
            'revalidation_fee' => 'Revalidation Fee',
            'independent_villa_fee' => 'Independent Villa Fee',
            'community_villa_fee' => 'Community Villa Fee',
            'staff_fee' => 'Staff Fee',
            'status_verified' => 'Status Verified',
            'status_verified_at' => 'Status Verified At',
            'status_verified_by' => 'Status Verified By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'deleted_by' => 'Deleted By',
        ];
    }

    public function getClientt()
    {
        return $this->hasOne(Company::className(), ['id' => 'client']);
    }

    public function getProperty()
    {
        return $this->hasOne(Properties::className(), ['id' => 'property_type']);
    }
}
