<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "uae_type_url".
 *
 * @property int $id
 * @property string|null $url
 * @property string|null $property_category
 * @property string|null $type
 * @property string|null $purpose
 */
class UaeTypeUrl extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'uae_type_url';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['url', 'property_category', 'type', 'purpose'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'property_category' => 'Property Category',
            'type' => 'Type',
            'purpose' => 'Purpose',
        ];
    }
}
