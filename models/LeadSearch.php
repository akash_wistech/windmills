<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Lead;

/**
* LeadSearch represents the model behind the search form of `app\models\Lead`.
*/
class LeadSearch extends Lead
{
  public $fullname,$company_name,$pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','rec_type','lead_id','company_id','sales_stage','lead_source','created_by','updated_by','pageSize'],'integer'],
      [['fullname','company_name','email','mobile','expected_close_date','descp','created_at'],'string'],
      [['quote_amount','probability'],'number'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $query = $this->generateQuery($params);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
      'sort' => $this->defaultSorting,
    ]);

    return $dataProvider;
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function searchMy($params)
  {
    $query = $this->generateQuery($params);

    //Check Listing type allowed
    if(Yii::$app->menuHelperFunction->getListingTypeByController('contact')==2){
      $query->innerJoin(LeadManager::tableName(),LeadManager::tableName().".lead_id=".Lead::tableName().".id");
      $query->andFilterWhere([
        LeadManager::tableName().'.staff_id' => Yii::$app->user->identity->id]);
    }

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
      'sort' => $this->defaultSorting,
    ]);

    return $dataProvider;
  }

  /**
  * Generates query for the search
  *
  * @param array $params
  *
  * @return ActiveRecord
  */
  public function generateQuery($params)
  {
    $this->load($params);
    $query = Lead::find()
    ->select([
      Lead::tableName().'.id',
      'fullname'=>'CONCAT('.Lead::tableName().'.firstname," ",'.Lead::tableName().'.lastname)',
      Lead::tableName().'.email',
      Lead::tableName().'.mobile',
      'company_name'=>Company::tableName().'.title',
      Lead::tableName().'.sales_stage',
      Lead::tableName().'.lead_source',
      Lead::tableName().'.expected_close_date',
      Lead::tableName().'.quote_amount',
      Lead::tableName().'.probability',
      Lead::tableName().'.created_at',
    ])
    ->leftJoin(Company::tableName(),Company::tableName().'.id='.Lead::tableName().'.company_id')
    ->asArray();

    // grid filtering conditions
    $query->andFilterWhere([
      Lead::tableName().'.id' => $this->id,
      Lead::tableName().'.rec_type' => $this->rec_type,
      Lead::tableName().'.lead_id' => $this->lead_id,
      Lead::tableName().'.sales_stage' => $this->sales_stage,
      Lead::tableName().'.lead_source' => $this->lead_source,
      Lead::tableName().'.expected_close_date' => $this->expected_close_date,
      Lead::tableName().'.trashed' => 0,
    ]);

    $query->andFilterWhere(['or',['like',Lead::tableName().'.firstname',$this->fullname],['like',Lead::tableName().'.lastname',$this->fullname]])
    ->andFilterWhere(['like',Lead::tableName().'.email',$this->email])
    ->andFilterWhere(['like',Company::tableName().'.title',$this->company_name])
    ->andFilterWhere(['like',Lead::tableName().'.mobile',$this->mobile]);

    return $query;
  }

  /**
  * Default Sorting Options
  *
  * @param array $params
  *
  * @return Array
  */
  public function getDefaultSorting()
  {
    return [
      'defaultOrder' => ['created_at'=>SORT_DESC],
      'attributes' => [
        'fullname' => [
          'asc' => [Lead::tableName().'.firstname' => SORT_ASC, Lead::tableName().'.lastname' => SORT_ASC],
          'desc' => [Lead::tableName().'.firstname' => SORT_DESC, Lead::tableName().'.lastname' => SORT_DESC],
        ],
        'email',
        'mobile',
        'company_name' => [
          'asc' => [Company::tableName().'.title' => SORT_ASC],
          'desc' => [Company::tableName().'.title' => SORT_DESC],
        ],
        'sales_stage',
        'lead_source',
        'created_at',
      ]
    ];
  }
}
