<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "schedule_inspection_history".
 *
 * @property int $id
 * @property int|null $inspection_type
 * @property string|null $inspection_date
 * @property string|null $inspection_time
 * @property int|null $valuation_id
 * @property int|null $inspection_officer
 * @property int $status
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class ScheduleInspectionHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'schedule_inspection_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['inspection_type', 'valuation_id', 'inspection_officer', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['inspection_date', 'created_at', 'updated_at', 'trashed_at'], 'safe'],
            [['inspection_time'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inspection_type' => 'Inspection Type',
            'inspection_date' => 'Inspection Date',
            'inspection_time' => 'Inspection Time',
            'valuation_id' => 'Valuation ID',
            'inspection_officer' => 'Inspection Officer',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }
}
