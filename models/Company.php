<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordSlugdFull;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;

/**
 * This is the model class for table "{{%company}}".
 *
 * @property integer $id
 * @property string $slug
 * @property integer $parent
 * @property string $title
 * @property string $website
 * @property integer $country_id
 * @property integer $zone_id
 * @property string $city
 * @property string $postal_code
 * @property string $address
 * @property string $descp
 * @property string $start_date
 * @property integer $lead_type
 * @property integer $lead_source
 * @property integer $lead_status
 * @property string $lead_date
 * @property integer $lead_score
 * @property integer $deal_status
 * @property string $interest
 * @property string $deal_value
 * @property string $expected_close_date
 * @property string $close_date
 * @property integer $confidence
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property integer $trashed
 * @property string $trashed_at
 * @property integer $trashed_by
 */
class Company extends ActiveRecordSlugdFull
{
    public $prospect_id,$job_title_id;
    public $firstname,$lastname,$job_title,$mobile,$email;
    public $fee_structure,$tat,$customEmailsStatus,$customEmailsAuto;
    public $other_instructing_person;
    public $InstructingPersonInfo;
    public $importfile;
    public $allowedFileTypes = ['csv'];
    public $status_verified;
    public $existing_valuation_contact_c;
    public $bilal_contact_c;
    public $valuation_contact_c;
    public $prospect_contact_c;
    public $property_owner_contact_c;
    public $inquiry_valuations_contact_c;
    public $icai_contact_c;
    public $broker_contact_c;
    public $developer_contact_c;
    //public $client_industry;
    public $p_o_box;
    public $country_id_nationality;
    public $land_line_number;
    public $client_priority;
    public $number_of_total_properties;
    public $sensitivity;
    public $good_bad_client;
    public $send_daily_report;
   // public $manager_id_back_up;
    public $dob_date;
    public $wedding_date;
    public $cname_with_nick;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%company}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'],'required','on'=>'simplecreate'],
            [['title'],'required','on'=>'normalcreate'],
            //['title','unique'],
            [['created_at','updated_at','trashed_at','data_type','quickbooks_registration_id','zohobooks_id','approved_by','approved_at','report_password'],'safe'],
            [[
                'parent','country_id','zone_id','job_title_id','mobile','lead_type','lead_source','lead_status','lead_score','deal_status',
                'confidence','status','created_by','updated_by','trashed','trashed_by','prospect_id'
            ],'integer'],
            [[
                'slug','title','website','city','postal_code','address','descp','start_date','firstname','lastname','job_title',
                'lead_date','interest','expected_close_date','close_date','deal_value'
            ],'string'],
            [['title','website','city','postal_code'],'trim'],
            [['email'],'email'],
            [['fee_structure','tat','client_reference','client_type','print_report','land_valutaion','land_age','relative_discount','customEmailsStatus','customEmailsAuto','other_instructing_person','InstructingPersonInfo'],'safe'],
          //  [['manager_id'],'each','rule'=>['integer']],
            [['importfile','signed_toe','vat','fee_master_file_vat','client_invoice_customer','trn_number','send_monthly_report','send_monthly_quotation_invoice','client_fixed_fee','nick_name'], 'safe'],
            [['importfile'], 'file', 'extensions' => implode(",", $this->allowedFileTypes)],
            [['status_verified','status_verified_at','status_verified_by'], 'safe'],
            [['existing_valuation_contact_c','bilal_contact_c','valuation_contact_c','valuation_contact_c','prospect_contact_c','property_owner_contact_c','inquiry_valuations_contact_c','icai_contact_c','broker_contact_c','developer_contact_c','allow_for_valuation'], 'safe'],
            [['client_industry','p_o_box','country_id_nationality','land_line_number','client_priority','number_of_total_properties','sensitivity','good_bad_client','send_daily_report','manager_id_back_up','allow_for_valuation','manager_id','sla','nda','fee_structure'], 'safe'],
            [['location','lat','lng','location_url','email_check','sms_check','whatsapp_check','linkedin_check'], 'safe'],
            [['nick_name'], 'required'],
            [['email_check','sms_check','whatsapp_check','linkedin_check','building_name','office_number','company_detail','linked_in','segment_type'], 'safe'],
            [['p_status_1','p_status_2','p_status_3','yearly_email','main_client_id','client_department','instruction_via_quotation','fee_with_bua_check','email_reminder_status','email_reminder_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Client Name'),
            'website' => Yii::t('app', 'Website'),
            'country_id' => Yii::t('app', 'Country'),
            'zone_id' => Yii::t('app', 'City'),
            'city' => Yii::t('app', 'City'),
            'postal_code' => Yii::t('app', 'Postal Code'),
            'address' => Yii::t('app', 'Street / Address'),
            'descp' => Yii::t('app', 'Description'),
            'manager_id' => Yii::t('app', 'Assign To'),
            'start_date' => Yii::t('app', 'Relationship Start Date'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'trashed' => Yii::t('app', 'Trashed'),
            'trashed_at' => Yii::t('app', 'Trashed At'),
            'trashed_by' => Yii::t('app', 'Trashed By'),
            'instruction_via_quotation' => Yii::t('app', 'Instruction Via Quotation'),
            'fee_with_bua_check' => Yii::t('app', 'Fee with BUA/Plot Size/Unite No Check in SLA'),
            // 'q_followup_email_number' => Yii::t('app', 'Follow Up Emails'),
            // 'q_followup_email_frequency' => Yii::t('app', 'Follow Up Emails Frequency(Days)'),
        ];
    }

    public function beforeValidate()
    {
        if(isset($this->id) && ($this->id <> null)){
            $company_data =Company::find()->where(['id'=>$this->id])->one();
            //$this->title = $company_data->title;
        }
        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->title;
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'Client';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * country name if found
     * @return String
     */
    public function getCountryName()
    {
        return $this->country!=null ? $this->country->title : '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZone()
    {
        return $this->hasOne(Zone::className(), ['id' => 'zone_id']);
    }

    /**
     * country name if found
     * @return String
     */
    public function getZoneName()
    {
        return $this->zone!=null ? $this->zone->title : '';
    }

    /**
     * Get Assigned Staff members
     * @return \yii\db\ActiveQuery
     */
    public function getManagerIdz()
    {
        return CompanyManager::find()->select(['staff_id'])->where(['company_id'=>$this->id])->asArray()->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllContacts()
    {
        return User::find()
            ->where(['company_id'=>$this->id])
            ->andWhere(['>','company_id',0])
            ->innerJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().".user_id=".User::tableName().".id")
            // ->asArray()
            ->all();
    }
    public function getPrimaryContact_old()
    {
        return User::find()
            ->where(['company_id'=>$this->id,'primary_contact'=>1])
            ->innerJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().".user_id=".User::tableName().".id")
            ->asArray()
            ->all();
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrimaryContact()
    {
        return User::find()
            ->where(['company_id'=>$this->id,'primary_contact'=>1])
            ->innerJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().".user_id=".User::tableName().".id")
           // ->asArray()
            ->one();
    }


    /**
     * @inheritdoc
     * Generates auth_key if it is a new user
     */
    public function beforeSave($insert)
    {
        if($this->status == 1 &&  $this->approved_by == ''){
            $this->approved_by = Yii::$app->user->identity->id;
            $this->approved_at = date("Y-m-d H:i:s");
        }
        if (parent::beforeSave($insert)) {
            $this->deal_value = str_replace(",","",$this->deal_value);


            // verify status reset after edit only something changed
            $isChanged = false;
            $ignoredAttributes = ['updated_at','status_verified_at']; 
            foreach ($this->attributes as $attribute => $value) {
                if (in_array($attribute, $ignoredAttributes)) {
                    continue;
                }
                if ($this->getOldAttribute($attribute) != $value) {
                    $isChanged = true;
                    break;
                }
            }
            if ($isChanged) {
                if($this->status == 1 && $this->getOldAttribute('status') == 1){
                    $this->status = 2;
                }elseif ($this->status == 2 && $this->getOldAttribute('status') == 1) {
                    $this->status = 1;
                }
            }


            return true;


           
                
        }


        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {

        // if (is_array($this->InstructingPersonInfo) && $this->InstructingPersonInfo<>null){
        //    // echo "<pre>"; print_r($this->InstructingPersonInfo); echo "</pre>"; die();
        //     \app\models\InstructingPersonInfo::deleteAll(['client_id' => $this->id]);
        //     foreach ($this->InstructingPersonInfo as $key => $value) {
        //        // print_r($value); echo "<br>";
        //         $saveNew = new \app\models\InstructingPersonInfo;
        //         $saveNew->client_id     = $this->id;
        //         $saveNew->firstname     = $value['firstname'];
        //         $saveNew->lastname      = $value['lastname'];
        //         $saveNew->job_title     = $value['job_title'];
        //         $saveNew->job_title_id  = $value['job_title_id'];
        //         $saveNew->mobile        = $value['mobile'];
        //         $saveNew->email         = $value['email'];
        //         if ($saveNew->save()){

        //         }
        //         if ($saveNew->hasErrors()) {
        //             foreach ($saveNew->getErrors() as $error) {
        //                 if (count($error) > 0) {
        //                     foreach ($error as $key => $val) {
        //                         Yii::$app->getSession()->addFlash('error', $val);
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }
        
        
        
        

        if ($this->other_instructing_person<>null) {
            \app\models\OtherInstructingPerson::deleteAll(['client_id' => $this->id]);
            foreach ($this->other_instructing_person as $key => $value) {
                $saveNew = new \app\models\OtherInstructingPerson;
                $saveNew->client_id = $this->id;
                $saveNew->detail = $value;
                $saveNew->save();
            }
        }



        $connection = \Yii::$app->db;
        if($this->prospect_id>0){
            $connection->createCommand("update ".Prospect::tableName()." set company_id=:company_id where id=:id",
                [':company_id'=>$this->id,':id'=>$this->prospect_id]
            )->execute();
        }




        if (is_array($this->InstructingPersonInfo) && $this->InstructingPersonInfo <>null){
            // echo "<pre>"; print_r($this->InstructingPersonInfo); echo "</pre>"; echo "<br>"; //die();


            foreach ($this->InstructingPersonInfo as $key => $value) {
                // echo "<pre>"; print_r($value['contant_id']); echo "</pre>"; echo "<br>"; //die();



                if (isset($value['contant_id']) && $value['contant_id'] <> null) {
                    $Contacts=User::find()
                        ->where(['company_id'=>$this->id,'user_type'=>0, User::tableName().".id" => $value['contant_id']])
                        ->innerJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().".user_id=".User::tableName().".id")
                        // ->asArray()
                        ->one();

                   //  echo "<pre>"; print_r($Contacts); echo "</pre>"; echo "<br>"; die();
                }
                else{
                   //  echo "contact id notset"; die();

                    $contacts = $Contacts=User::find()
                    ->where([User::tableName().".email" => $value['email']])
                    ->innerJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().".user_id=".User::tableName().".id")
                    ->one();
                    if($contacts == null){
                        $Contacts=new User;
                        $Contacts->user_type=0;
                        $Contacts->status=1;
                        $Contacts->company_id=$this->id;
                    }
                }

                $Contacts->company_id=$this->id;
                $Contacts->primary_contact =($key == 0) ? 1 : 2;
                $Contacts->firstname=$value['firstname'];
                $Contacts->lastname=$value['lastname'];
                $Contacts->job_title=$value['job_title'];
                $Contacts->job_title_id=$value['job_title_id'];
                $Contacts->mobile=$value['mobile'];
                $Contacts->email=$value['email'];
                $Contacts->land_line=$value['land_line'];
                $Contacts->mobile_1=$value['mobile_1'];
                $Contacts->linked_in_page=$value['linked_in_page'];
                $Contacts->existing_valuation_contact=$this->existing_valuation_contact_c;
                $Contacts->bilal_contact=$this->bilal_contact_c;
                $Contacts->valuation_contact=$this->valuation_contact_c;
                $Contacts->prospect_contact=$this->prospect_contact_c;
                $Contacts->property_owner_contact=$this->property_owner_contact_c;
                $Contacts->inquiry_valuations_contact=$this->inquiry_valuations_contact_c;
                $Contacts->icai_contact=$this->icai_contact_c;
                $Contacts->broker_contact=$this->broker_contact_c;
                $Contacts->developer_contact=$this->developer_contact_c;


                $Contacts->save();
                if(!$Contacts->save()){
                    echo "<pre>";
                    print_r($Contacts->errors);
                    die;
                }
                if ($Contacts->hasErrors()) {
                    foreach ($Contacts->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                echo $val; echo "<br>";
                            }

                        }
                    }
                }
                if($this->prospect_id>0 && $Contacts->id>0){
                    $connection->createCommand("update ".Prospect::tableName()." set user_id=:user_id where id=:id",
                        [':user_id'=>$Contacts->id,':id'=>$this->prospect_id]
                    )->execute();
                }
            }
            // die('end');

        }

       /* echo "<pre>";
        print_r($_SESSION['contacts_data']);
        die('ddd');*/
        if (isset( $_SESSION['contacts_data']) && is_array($_SESSION['contacts_data']) && $_SESSION['contacts_data'] <>null){
            // echo "<pre>"; print_r($this->InstructingPersonInfo); echo "</pre>"; echo "<br>"; //die();


            foreach ($_SESSION['contacts_data'] as $key => $value) {
                // echo "<pre>"; print_r($value['contant_id']); echo "</pre>"; echo "<br>"; //die();



                if (isset($value['contant_id']) && $value['contant_id'] <> null) {
                    $Contacts=User::find()
                        ->where(['company_id'=>$this->id,'user_type'=>0, User::tableName().".id" => $value['contant_id']])
                        ->innerJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().".user_id=".User::tableName().".id")
                        // ->asArray()
                        ->one();

                    //  echo "<pre>"; print_r($Contacts); echo "</pre>"; echo "<br>"; die();
                }
                else{
                    //  echo "contact id notset"; die();

                    $contacts = $Contacts=User::find()
                        ->where([User::tableName().".email" => $value['email']])
                        ->innerJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().".user_id=".User::tableName().".id")
                        ->one();
                    if($contacts == null){
                        $Contacts=new User;
                        $Contacts->user_type=0;
                        $Contacts->status=1;
                        $Contacts->company_id=$this->id;
                    }
                }



                $Contacts->company_id=$this->id;
                $Contacts->primary_contact =($key == 0) ? 1 : 2;
                $Contacts->firstname=$value['firstName'];
                $Contacts->lastname=$value['lastName'];
               // $Contacts->job_title=$value['postion'];
                $Contacts->job_title_id=$value['position'];
                $Contacts->department_id=$value['department'];
                $Contacts->mobile=$value['mobile1'];
                $Contacts->email=$value['email'];
                $Contacts->land_line=$value['landline'];
                $Contacts->mobile_1=$value['mobile2'];
                //$Contacts->linked_in_page=$value['linked_in'];
               // $Contacts->segment_type=11;
                $Contacts->bilal_contact=0;
                $Contacts->valuation_contact=0;
                $Contacts->prospect_contact=0;
                $Contacts->property_owner_contact=0;
                $Contacts->inquiry_valuations_contact=0;
                $Contacts->icai_contact=0;
                $Contacts->broker_contact=0;
                $Contacts->developer_contact=0;


                $Contacts->save();
                if(!$Contacts->save()){
                    echo "<pre>";
                    print_r($Contacts->errors);
                    die;
                }
                if ($Contacts->hasErrors()) {
                    foreach ($Contacts->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                echo $val; echo "<br>";
                            }

                        }
                    }
                }
                if($this->prospect_id>0 && $Contacts->id>0){
                    $connection->createCommand("update ".Prospect::tableName()." set user_id=:user_id where id=:id",
                        [':user_id'=>$Contacts->id,':id'=>$this->prospect_id]
                    )->execute();
                }
            }
            // die('end');

        }








        // //Saving Primary Contact
        // if($this->firstname!=null){
        // 	$contact=User::find()
        // 	->where(['company_id'=>$this->id,'user_type'=>0,'primary_contact'=>1])
        // 	->innerJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().".user_id=".User::tableName().".id")
        // 	->one();
        // 	if($contact==null){
        // 		$contact=new User;
        // 		$contact->user_type=0;
        // 		$contact->status=1;
        // 		$contact->company_id=$this->id;
        // 	}
        // 	$contact->primary_contact=1;
        // 	$contact->firstname=$this->firstname;
        // 	$contact->lastname=$this->lastname;
        // 	$contact->job_title=$this->job_title;
        // 	$contact->job_title_id=$this->job_title_id;
        // 	$contact->mobile=$this->mobile;
        // 	$contact->email=$this->email;
        // 	$contact->save();
        // 	if($this->prospect_id>0 && $contact->id>0){
        // 		$connection->createCommand("update ".Prospect::tableName()." set user_id=:user_id where id=:id",
        // 		[':user_id'=>$contact->id,':id'=>$this->prospect_id]
        // 		)->execute();
        // 	}
        // }





















        //Saving Fee Structure
        $emiratesListArr=Yii::$app->appHelperFunctions->emiratedListArr;
        //$feeStructureOptions=Yii::$app->appHelperFunctions->feeStructureOptionListArr;
        $feeStructureOptions=Yii::$app->appHelperFunctions->inspectionTypeArr;
        $feeStructureTypes=Yii::$app->helperFunctions->feeStructureTypeListArr;
        
        
        
        
        //Saving Managers
      /*  if($this->manager_id!=null){
            CompanyManager::deleteAll(['and',['company_id'=>$this->id],['not in','staff_id',$this->manager_id]]);
            foreach($this->manager_id as $key=>$val){
                $managerRow=CompanyManager::find()->where(['company_id'=>$this->id,'staff_id'=>$val])->one();
                if($managerRow==null){
                    $managerRow=new CompanyManager;
                    $managerRow->company_id=$this->id;
                    $managerRow->staff_id=$val;
                    $managerRow->save();
                }
            }
        }*/
        $managerRow=CompanyManager::find()->where(['company_id'=>$this->id,'staff_id'=>$this->created_by]);
        if(!$managerRow->exists()){
            $managerRow=new CompanyManager;
            $managerRow->company_id=$this->id;
            $managerRow->staff_id=$this->created_by;
            $managerRow->save();
        }


        // QuickBooks Add Call
      /*  if($this->developer_contact_c != 1){
             if($this->quickbooks_registration_id <> null) {
                  $addClientInQuickBooks = $this->postClientInQuickBooks();
             }else{
                if($this->data_type == 0) {
                    $addClientInQuickBooks = $this->postClientInQuickBooks();
                }
             }
        }*/

        // Zohobooks Add Call
        if($this->zohobooks_id == null || $this->zohobooks_id == '') {
            $addClientInZohoBooks = Yii::$app->appHelperFunctions->postClientInZohoBooks($this->id,$this->title, $this->title);
        }

        if(isset($this->customEmailsStatus) && ($this->customEmailsStatus <> null)) {


            ClientEmailsSettings::deleteAll(['client_id' => $this->id,'type'=>1]);
            // Save ALL banners Images
            foreach ($this->customEmailsStatus as $attachment) {
                if($attachment['name'] != '' && $attachment['email'] !='') {
                    $customAttachments = new ClientEmailsSettings();
                    $customAttachments->name = $attachment['name'];
                    $customAttachments->email = $attachment['email'];
                    $customAttachments->client_id = $this->id;
                    $customAttachments->type = 1;
                    if(!$customAttachments->save()){
                        echo "<pre>";
                        print_r($customAttachments->errors);
                        die;
                    }
                }
            }
        }
        if(isset($this->customEmailsAuto) && ($this->customEmailsAuto <> null)) {
            ClientEmailsSettings::deleteAll(['client_id' => $this->id,'type'=>2]);
            // Save ALL banners Images
            foreach ($this->customEmailsAuto as $emails) {
                if($emails['name'] != '' && $emails['email'] !='') {
                    $customEmails = new ClientEmailsSettings();
                    $customEmails->name = $emails['name'];
                    $customEmails->email = $emails['email'];
                    $customEmails->client_id = $this->id;
                    $customEmails->type = 2;
                    $customEmails->save();
                }
            }
        }



        unset($_SESSION['contacts_data']);


        parent::afterSave($insert, $changedAttributes);
    }

    public function getCustomEmails()
    {

        return $this->hasMany(ClientEmailsSettings::className(), ['client_id' => 'id']) ->andOnCondition(['type' => '1']);
    }
    public function getCustomEmailsVals()
    {
        return $this->hasMany(ClientEmailsSettings::className(), ['client_id' => 'id']) ->andOnCondition(['type' => '2']);
    }
    public function postClientInQuickBooks()
    {
        $params = [
            "BillAddr" => [
                "Line1" => $this->address,
                "City" => $this->zone->title,
                "Country" => $this->country->iso_code_3,
                "CountrySubDivisionCode" => "",
                "PostalCode" => "",
            ],

            // "SyncToken" => "0",
            // "domain" => "QBO",
            // "Active" => true,
            // "Taxable" => false,

            "Notes" => "Here are other details.",
            "Title" => "Mr",
            "GivenName" => $this->title,
            "MiddleName" => "",
            "FamilyName" => $this->title,
            "Suffix" => "",
            "FullyQualifiedName" => $this->title,
            "CompanyName" => $this->title,
            "DisplayName" => $this->title,

            "PrimaryPhone" => [
                "FreeFormNumber" => $this->mobile,
            ],

            "PrimaryEmailAddr" => [
                "Address" => $this->email,
            ]
        ];

        $dataService = Yii::$app->appHelperFunctions->getDataServiceObject();

        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        $dataService->throwExceptionOnError(true);

        if ($this->quickbooks_registration_id=='') {
            $dataService = Yii::$app->appHelperFunctions->refreshTokens($dataService);
            $theResourceObj = Customer::create($params);
            $resultingObj = $dataService->Add($theResourceObj);
        }else{
            // echo $this->quickbooks_registration_id; die();
            $dataService =  Yii::$app->appHelperFunctions->refreshTokens($dataService);
            $customer = $dataService->FindbyId('customer', $this->quickbooks_registration_id);
            $theResourceObj = Customer::update($customer, $params);
            $resultingObj = $dataService->Update($theResourceObj);
        }

        $error = $dataService->getLastError();
        if ($error) {
         //   Yii::$app->getSession()->addFlash('error', $error->getResponseBody());
         //   echo "The Status code is: " . $error->getHttpStatusCode() . "\n"; echo "<br>";
          //  echo "The Helper message is: " . $error->getOAuthHelperError() . "\n"; echo "<br>";
         //   echo "The Response message is: " . $error->getResponseBody() . "\n"; echo "<br>";
          //  die();
        }
        else {
            // die('no error');
            if ($this->quickbooks_registration_id=='') {
                $query =  Yii::$app->db->createCommand()->update('company', ['quickbooks_registration_id' => $resultingObj->Id], 'id = '.$this->id.'')->execute();
                Yii::$app->getSession()->addFlash('success', Yii::t('app','Customer Created in QuickBooks'));
            }else{
                Yii::$app->getSession()->addFlash('success', Yii::t('app','Customer Updated in QuickBooks'));
            }
        }
    }


    public function upload($image='', $folderName='')
    {
        $uploadObject = Yii::$app->get('s3bucket')->upload($folderName.'/images/'. $image->baseName . '.' . $image->extension,$image->tempName);
        if ($uploadObject) {
            // check if CDN host is available then upload and get cdn URL.
            if (Yii::$app->get('s3bucket')->cdnHostname) {
                $url = Yii::$app->get('s3bucket')->getCdnUrl($image );
            } else {
                $url = Yii::$app->get('s3bucket')->getUrl($folderName.'/images/'.$image );
            }
        }
        return true;
    }
}
