<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "crm_valuation_conflict".
 *
 * @property int $id
 * @property string $related_to_buyer
 * @property string $related_to_seller
 * @property string $related_to_client
 * @property string $related_to_property
 * @property int $quotation_id
 * @property string|null $related_to_buyer_reason
 * @property string|null $related_to_seller_reason
 * @property string|null $related_to_client_reason
 * @property string|null $related_to_property_reason
 * @property int|null $property_index
 */
class CrmValuationConflict extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_valuation_conflict';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['related_to_buyer', 'related_to_seller', 'related_to_client', 'related_to_property', 'related_to_buyer_reason', 'related_to_seller_reason', 'related_to_client_reason', 'related_to_property_reason'], 'string'],
            [['quotation_id'], 'required'],
            [['quotation_id', 'property_index'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'related_to_buyer' => 'Related To Buyer',
            'related_to_seller' => 'Related To Seller',
            'related_to_client' => 'Related To Client',
            'related_to_property' => 'Related To Property',
            'quotation_id' => 'Quotation ID',
            'related_to_buyer_reason' => 'Related To Buyer Reason',
            'related_to_seller_reason' => 'Related To Seller Reason',
            'related_to_client_reason' => 'Related To Client Reason',
            'related_to_property_reason' => 'Related To Property Reason',
            'property_index' => 'Property Index',
        ];
    }
}
