<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%workflow_data_item}}".
*
* @property string $module_type
* @property integer $module_id
* @property integer $workflow_id
* @property integer $workflow_stage_id
*/
class WorkflowDataItem extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%workflow_data_item}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['module_type','module_id','workflow_id','workflow_stage_id'],'required'],
      [['module_id','workflow_id','workflow_stage_id'],'integer'],
      [['module_type'],'string'],
    ];
  }

  public static function primaryKey()
  {
  	return ['module_type','module_id','workflow_id','workflow_stage_id'];
  }
}
