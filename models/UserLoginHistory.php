<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%user_login_history}}".
*
* @property integer $id
* @property integer $user_id
* @property integer $ip
* @property string $created_at
* @property integer $login
*/
class UserLoginHistory extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%user_login_history}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['user_id', 'ip', 'created_at'], 'required'],
      [['user_id', 'ip', 'login'], 'integer'],
      [['created_at'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_id' => Yii::t('app', 'User ID'),
      'ip' => Yii::t('app', 'Ip'),
      'created_at' => Yii::t('app', 'Created At'),
      'login' => Yii::t('app', 'Login'),
    ];
  }
}
