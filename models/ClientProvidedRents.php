<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "client_provided_rents".
 *
 * @property int $id
 * @property int|null $income_type
 * @property int|null $unit_number
 * @property string|null $nla
 * @property string|null $contract_date
 * @property string|null $contract_end_date
 * @property string|null $rent
 * @property string|null $rent_sqf
 * @property string|null $service_charges_unit
 * @property int|null $status
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 */
class ClientProvidedRents extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_provided_rents';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['valuation_id', 'income_type', 'unit_number', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['contract_start_date', 'contract_end_date', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['nla', 'rent', 'rent_sqf', 'service_charges_unit',], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'income_type' => 'Income Type',
            'unit_number' => 'Unit Number',
            'nla' => 'Nla',
            'contract_date' => 'Contract Date',
            'contract_end_date' => 'Contract End Date',
            'rent' => 'Rent',
            'rent_sqf' => 'Rent/Sqf',
            'service_charges_unit' => 'Service Chg/Unit',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'deleted_by' => 'Deleted By',
        ];
    }
}
