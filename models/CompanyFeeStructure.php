<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%company_fee_structure}}".
*
* @property integer $company_id
* @property integer $fee_structure_type_id
* @property integer $emirate_id
* @property integer $type_id
* @property integer $fee
* @property integer $tat
*/
class CompanyFeeStructure extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%company_fee_structure}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['company_id', 'fee_structure_type_id', 'emirate_id','type_id'], 'required'],
      [['company_id', 'fee_structure_type_id', 'emirate_id','type_id','tat'], 'integer'],
      [['fee'], 'number']
    ];
  }

  public static function primaryKey()
  {
  	return ['company_id','fee_structure_type_id','emirate_id','type_id'];
  }
}
