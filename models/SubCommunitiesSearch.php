<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SubCommunities;

/**
 * SubCommunitiesSearch represents the model behind the search form of `app\models\SubCommunities`.
 */
class SubCommunitiesSearch extends SubCommunities
{
    public $community_title;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'community', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['title', 'created_at', 'updated_at', 'trashed_at','community_title'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //$query = SubCommunities::find();
        $query = SubCommunities::find()
            ->select([
                SubCommunities::tableName().'.id',
                'title'=>SubCommunities::tableName().'.title',
                'community'=>Communities::tableName().'.title',
                SubCommunities::tableName().'.status',
            ])
            ->leftJoin(Communities::tableName(),SubCommunities::tableName().'.community='.Communities::tableName().'.id')
            ->asArray();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);
        $dataProvider->sort->attributes['title'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['TRIM(sub_communities.title)' => SORT_ASC],
            'desc' => ['TRIM(sub_communities.title)' => SORT_DESC],
        ];
        // Lets do the same with country now
        $dataProvider->sort->attributes['community'] = [
            'asc' => ['TRIM(communities.title)' => SORT_ASC],
            'desc' => ['TRIM(communities.title)' => SORT_DESC],
        ];
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'sub_communities.community' => $this->community,
          //  'city' => $this->city,
            'sub_communities.status' => $this->status,
   /*         'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => 0,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,*/
        ]);

        $query->andFilterWhere(['like', 'sub_communities.title', $this->title]);
        $query->andFilterWhere(['like', 'sub_communities.community', $this->community]);

        return $dataProvider;
    }
}
