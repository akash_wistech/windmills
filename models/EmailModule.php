<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "email_module".
 *
 * @property int $id
 * @property int|null $template_id
 * @property int|null $list_id
 * @property int|null $campaign_id
 */
class EmailModule extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'email_module';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['template_id', 'list_id', 'campaign_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'template_id' => 'Template ID',
            'list_id' => 'List ID',
            'campaign_id' => 'Campaign ID',
        ];
    }
}
