<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sold_data_upgrade".
 *
 * @property int $id
 * @property string|null $building_name
 * @property string|null $no_of_rooms
 * @property string|null $bua
 * @property string|null $plot_area
 * @property string|null $type
 * @property string|null $sold_date
 * @property string|null $price
 * @property string|null $price_sqf
 */
class SoldDataUpgradeRents extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sold_data_upgrade_rents';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['building_name', 'no_of_rooms', 'bua', 'plot_area', 'type', 'sold_date', 'price', 'price_sqf','unit_number','floor_number','balcony_size','parking_space_number'], 'string', 'max' => 255],
            [['building_name', 'no_of_rooms', 'bua', 'plot_area', 'type', 'sold_date', 'price', 'price_sqf','unit_number','floor_number','balcony_size','parking_space_number'], 'safe'],
            [['transaction_type','sub_type','sales_sequence','reidin_ref_number', 'reidin_community','reidin_property','reidin_property_type','reidin_developer','data_type','inserted'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'building_name' => 'Building Name',
            'no_of_rooms' => 'No Of Rooms',
            'bua' => 'Bua',
            'plot_area' => 'Plot Area',
            'type' => 'Type',
            'sold_date' => 'Sold Date',
            'price' => 'Price',
            'price_sqf' => 'Price Sqf',
        ];
    }
}
