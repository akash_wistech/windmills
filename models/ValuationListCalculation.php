<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "valuation_list_calculation".
 *
 * @property int $id
 * @property float|null $avg_bedrooms
 * @property float|null $avg_land_size
 * @property float|null $built_up_area_size
 * @property float $avg_listings_price_size
 * @property float $min_price
 * @property float $max_price
 * @property float $min_price_sqt
 * @property float $max_price_sqt
 * @property string|null $avg_listing_date
 * @property float $avg_psf
 * @property string|null $type
 * @property int $valuation_id
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 */
class ValuationListCalculation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'valuation_list_calculation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['avg_bedrooms', 'avg_land_size', 'built_up_area_size', 'avg_listings_price_size', 'min_price', 'max_price', 'min_price_sqt', 'max_price_sqt', 'avg_psf','avg_gross_yield'], 'number'],
            [['avg_listing_date', 'created_at', 'updated_at', 'trashed_at','avg_gross_yield','search_type','income_type'], 'safe'],
            [['type'], 'string'],
            [['valuation_id'], 'required'],
            [['valuation_id', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'avg_bedrooms' => 'Avg Bedrooms',
            'avg_land_size' => 'Avg Land Size',
            'built_up_area_size' => 'Built Up Area Size',
            'avg_listings_price_size' => 'Avg Listings Price Size',
            'min_price' => 'Min Price',
            'max_price' => 'Max Price',
            'min_price_sqt' => 'Min Price Sqt',
            'max_price_sqt' => 'Max Price Sqt',
            'avg_listing_date' => 'Avg Listing Date',
            'avg_psf' => 'Avg Psf',
            'type' => 'Type',
            'valuation_id' => 'Valuation ID',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
        ];
    }
}
