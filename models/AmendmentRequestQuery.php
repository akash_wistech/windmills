<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[AmendmentRequest]].
 *
 * @see AmendmentRequest
 */
class AmendmentRequestQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return AmendmentRequest[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return AmendmentRequest|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
