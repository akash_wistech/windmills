<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "valuation_selected_lists_auto".
 *
 * @property int $id
 * @property int $valuation_id
 * @property int $selected_id
 * @property string|null $type
 */
class ValuationSelectedListsAuto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'valuation_selected_lists_auto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['valuation_id', 'selected_id'], 'required'],
            [['valuation_id', 'selected_id'], 'integer'],
            [['type'], 'string'],
            [['created_by','search_type','selected_status','latest','income_type'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'valuation_id' => 'Valuation ID',
            'selected_id' => 'Selected ID',
            'type' => 'Type',
        ];
    }
}
