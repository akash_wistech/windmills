<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Communities;

/**
 * CommunitiesSearch represents the model behind the search form of `app\models\Communities`.
 */
class CommunitiesSearch extends Communities
{
    public $pageSize;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'city', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['title', 'created_at', 'updated_at', 'trashed_at,land_price'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->generateQuery($params);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
            ],
           // 'sort' => $this->defaultSorting,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        return $dataProvider;
    }
    /**
     * Generates query for the search
     *
     * @param array $params
     *
     * @return ActiveRecord
     */
    public function generateQuery($params)
    {
        $this->load($params);
        $query = Communities::find()
            ->select([
                Communities::tableName().'.id',
                Communities::tableName().'.title',
                Communities::tableName().'.land_price',
                Zone::tableName().'.title as city',
                Communities::tableName().'.status',
            ])
            ->leftJoin(Zone::tableName(),Zone::tableName().'.id='.Communities::tableName().'.city')
            ->asArray();

        // grid filtering conditions
        $query->andFilterWhere([
            Communities::tableName().'.id' => $this->id,
            Communities::tableName().'.status' => $this->status,
            Communities::tableName().'.trashed' => 0,
        ]);

        $query->andFilterWhere(['like',Communities::tableName().'.title',$this->title])
            ->andFilterWhere(['like',Zone::tableName().'.title',$this->city]);

        return $query;
    }
}
