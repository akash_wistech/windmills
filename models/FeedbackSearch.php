<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Feedback;

use yii;


/**
 * FeedbacksSearch represents the model behind the search form of `app\models\Feedback`.
 */
class FeedbackSearch extends Feedback
{
    public $quality_average;
    public $date_range, $time_period,$time_period_compare, $custom_date_btw;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'valuation_id', 'quotation_id', 'status','quality_of_valuation','quality_of_inspection','quality_of_valuation_service','quality_of_proposal_service','quality_of_customer_service','quality_of_tat','fee_rating','tat_rating','technical_capability_rating','status','type','city','property_category'], 'integer'],
            [[ 'created_at','quality_average','time_period','time_period_compare','custom_date_btw','client_name','valuer','inspector','approver','proposer'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $type = null)
    {
        
        $request = Yii::$app->request;
        $sort = $request->get('sort');

        $query = Feedback::find()->where(['status'=>1]);

        if (!$sort) {
            $query->orderBy([ 'updated_at' => SORT_DESC ]);
        }

        if ($type !== null) {
            $query->andWhere(['type' => $type]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 50, 
            ],
            'sort' => [
                'attributes' => [
                    'client_id','valuation_id','quotation_id',  
                    'quality_of_inspection','quality_of_valuation','quality_of_tat','quality_of_valuation_service',
                    'fee_rating','tat_rating','technical_capability_rating','quality_of_customer_service','quality_of_proposal_service',
                    'quality_average','property_category',
                    'city','valuer','inspector','approver','proposer',
                    'updated_at',
                ],
            ],
        ]);

        $this->load($params);

        

        if($this->time_period <> null && $this->time_period != 0 ) {

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {

                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'].' 00:00:00';
                $to_date = $date_array['end_date'];
                $to_date = date('Y-m-d', strtotime($to_date . ' +1 day')).' 23:59:00';
            }


        }else{
            $from_date = '2020-04-28 00:00:00';
            $to_date = date('Y-m-d 23:59:00');
        }
        
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'client_id' => $this->client_id,
            'quality_of_valuation' => $this->quality_of_valuation,
            'quality_of_inspection' => $this->quality_of_inspection,
            'quality_of_tat' => $this->quality_of_tat,
            'quality_of_valuation_service' => $this->quality_of_valuation_service,
            'fee_rating' => $this->fee_rating,
            'tat_rating' => $this->tat_rating,
            'technical_capability_rating' => $this->technical_capability_rating,
            'quality_of_customer_service' => $this->quality_of_customer_service,
            'quality_of_proposal_service' => $this->quality_of_proposal_service,
            'city' => $this->city,
            'proposer' => $this->proposer,
            'valuer' => $this->valuer,
            'inspector' => $this->inspector,
            'approver' => $this->approver,
            'property_category' => $this->property_category,
            
        ]);

        $query->andFilterWhere([
            'between', 'quality_average', $this->quality_average - 19, $this->quality_average,
        ]);
        


        $query->andFilterWhere([
            'between', 'created_at', $from_date, $to_date,
        ]);

        

        if($this->valuation_id <> null){
            $valuationsIds = Valuation::find()
            ->select('id')
            ->where(['like', 'reference_number', '%' . $this->valuation_id . '%', false])
            ->column();
            $query->andFilterWhere(['in', 'valuation_id', $valuationsIds]);
        }

        if($this->quotation_id <> null){
            $quotationsIds = CrmQuotations::find()
            ->select('id')
            ->where(['like', 'reference_number', '%' . $this->quotation_id . '%', false])
            ->column();
            $query->andFilterWhere(['in', 'quotation_id', $quotationsIds]);
        }

        return $dataProvider;
    }

    
}
