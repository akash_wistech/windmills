<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProposalBcsReport;

/**
 * ProposalBcsReportSearch represents the model behind the search form of `app\models\ProposalBcsReport`.
 */
class ProposalBcsReportSearch extends ProposalBcsReport
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [[                
                'payment_method', 'payment_terms', 'timings', 'exclusions', 'limitions_on_liabity', 'valuer_duties_supervision', 'internal_external_status_of_the_valuer',
                'previous_involvement_and_conflict_of_interest', 'decleration_of_independence_and_objectivity', 'required_documents', 'terms_versus_agreement', 'providing_the_service', 'liability', 'remuneration', 'communication', 'estimates', 'confidentiality', 'termination', 'miscellaneous', 'force_majeure', 'waiver_and_severance', 'no_responsibility', 'no_partnership', 'conflict', 'dispute_resolution', 'complaints_handling_procedure', 'jurisdiction', 'acceptance_of_terms_of_engagement', 'quotation_last_paragraph'
            ], 'safe'],
            [['rfs_sos_details', 'scs_sos_details', 'bca_sos_details', 'rica_sos_details', 'ts_sos_details', 'err_sos_details','arct_sos_details','rfv_sos_details'], 'safe'],
            [['assumptions_rfs','assumptions_scs','assumptions_bca','assumptions_rica','assumptions_ts','assumptions_err','assumptions_arct','assumptions_rfv'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProposalBcsReport::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'rfs_sos_details', $this->rfs_sos_details])
            ->andFilterWhere(['like', 'scs_sos_details', $this->scs_sos_details])
            ->andFilterWhere(['like', 'bca_sos_details', $this->bca_sos_details])
            ->andFilterWhere(['like', 'rica_sos_details', $this->rica_sos_details])
            ->andFilterWhere(['like', 'ts_sos_details', $this->ts_sos_details])
            ->andFilterWhere(['like', 'err_sos_details', $this->err_sos_details])
            ->andFilterWhere(['like', 'arct_sos_details', $this->arct_sos_details])
            ->andFilterWhere(['like', 'rfv_sos_details', $this->rfv_sos_details])
            ->andFilterWhere(['like', 'payment_method', $this->payment_method])
            ->andFilterWhere(['like', 'payment_terms', $this->payment_terms])
            ->andFilterWhere(['like', 'timings', $this->timings])
            ->andFilterWhere(['like', 'exclusions', $this->exclusions])
            ->andFilterWhere(['like', 'limitions_on_liabity', $this->limitions_on_liabity])
            ->andFilterWhere(['like', 'valuer_duties_supervision', $this->valuer_duties_supervision])
            ->andFilterWhere(['like', 'internal_external_status_of_the_valuer', $this->internal_external_status_of_the_valuer])
            ->andFilterWhere(['like', 'previous_involvement_and_conflict_of_interest', $this->previous_involvement_and_conflict_of_interest])
            ->andFilterWhere(['like', 'decleration_of_independence_and_objectivity', $this->decleration_of_independence_and_objectivity])
            ->andFilterWhere(['like', 'required_documents', $this->required_documents])
            ->andFilterWhere(['like', 'terms_versus_agreement', $this->terms_versus_agreement])
            ->andFilterWhere(['like', 'providing_the_service', $this->providing_the_service])
            ->andFilterWhere(['like', 'liability', $this->liability])
            ->andFilterWhere(['like', 'remuneration', $this->remuneration])
            ->andFilterWhere(['like', 'communication', $this->communication])
            ->andFilterWhere(['like', 'estimates', $this->estimates])
            ->andFilterWhere(['like', 'confidentiality', $this->confidentiality])
            ->andFilterWhere(['like', 'valuation_date', $this->valuation_date])
            ->andFilterWhere(['like', 'property', $this->property])
            ->andFilterWhere(['like', 'termination', $this->termination])
            ->andFilterWhere(['like', 'miscellaneous', $this->miscellaneous])
            ->andFilterWhere(['like', 'force_majeure', $this->force_majeure])
            ->andFilterWhere(['like', 'waiver_and_severance', $this->waiver_and_severance])
            ->andFilterWhere(['like', 'no_responsibility', $this->no_responsibility])
            ->andFilterWhere(['like', 'no_partnership', $this->no_partnership])
            ->andFilterWhere(['like', 'conflict', $this->conflict])
            ->andFilterWhere(['like', 'dispute_resolution', $this->dispute_resolution])
            ->andFilterWhere(['like', 'complaints_handling_procedure', $this->complaints_handling_procedure])
            ->andFilterWhere(['like', 'jurisdiction', $this->jurisdiction])
            ->andFilterWhere(['like', 'acceptance_of_terms_of_engagement', $this->acceptance_of_terms_of_engagement])
            ->andFilterWhere(['like', 'disposal_costs_and_liabilities', $this->disposal_costs_and_liabilities])
            ->andFilterWhere(['like', 'quotation_last_paragraph', $this->quotation_last_paragraph])
            ->andFilterWhere(['like', 'assumptions_rfs', $this->assumptions_rfs])
            ->andFilterWhere(['like', 'assumptions_scs', $this->assumptions_scs])
            ->andFilterWhere(['like', 'assumptions_bca', $this->assumptions_bca])
            ->andFilterWhere(['like', 'assumptions_rica', $this->assumptions_rica])
            ->andFilterWhere(['like', 'assumptions_ts', $this->assumptions_ts])
            ->andFilterWhere(['like', 'assumptions_err', $this->assumptions_err])
            ->andFilterWhere(['like', 'assumptions_arct', $this->assumptions_arct])
            ->andFilterWhere(['like', 'assumptions_rfv', $this->assumptions_rfv]);

        return $dataProvider;
    }
}
