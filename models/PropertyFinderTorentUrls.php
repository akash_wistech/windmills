<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "property_finder_torent_urls".
 *
 * @property int $id
 * @property string|null $url
 * @property string|null $property_category
 */
class PropertyFinderTorentUrls extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'property_finder_torent_urls';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['url', 'property_category'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'property_category' => 'Property Category',
        ];
    }
}
