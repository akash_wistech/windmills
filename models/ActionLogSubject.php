<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%action_log_subject}}".
*
* @property integer $action_log_id
* @property integer $subject
* @property integer $due_date
* @property integer $status
* @property integer $completed_at
*/
class ActionLogSubject extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%action_log_subject}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['action_log_id', 'subject'], 'required'],
      [['action_log_id','status'], 'integer'],
      [['subject','due_date','completed_at'], 'string'],
    ];
  }

  public static function primaryKey()
  {
  	return ['action_log_id','subject'];
  }
}
