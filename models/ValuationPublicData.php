<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "valuation_public_data".
 *
 * @property int $id
 * @property int $status
 * @property int|null $created_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $trashed
 * @property string|null $trashed_at
 * @property int|null $trashed_by
 * @property int|null $status_verified
 * @property int|null $status_verified_by
 * @property string|null $status_verified_at
 * @property int|null $valuation_id
 * @property string|null $attach_dld_property_status_document
 * @property string|null $attach_sharjah_municipality_data_document
 * @property string|null $attach_ajman_municipality_document
 * @property string|null $attach_umm_quwain_municipality_document
 * @property string|null $attach_rak_municipality_document
 * @property string|null $attach_fujairah_municipality_document
 * @property string|null $attach_abu_dhabi_municipality_document
 * @property string|null $attach_al_ain_municipality_document
 */
class ValuationPublicData extends \yii\db\ActiveRecord
{
    public $agents_data = [];
    public $received_docs = [];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'valuation_public_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'created_by', 'updated_by', 'trashed', 'trashed_by', 'status_verified', 'status_verified_by', 'valuation_id'], 'integer'],
            [['created_at', 'updated_at', 'trashed_at', 'status_verified_at','agents_data','received_docs'], 'safe'],
            [['attach_dld_property_status_document', 'attach_sharjah_municipality_data_document', 'attach_ajman_municipality_document', 'attach_umm_quwain_municipality_document', 'attach_rak_municipality_document', 'attach_fujairah_municipality_document', 'attach_abu_dhabi_municipality_document', 'attach_al_ain_municipality_document'], 'string', 'max' => 255],
            [['attach_dld_property_status_document', 'attach_sharjah_municipality_data_document', 'attach_ajman_municipality_document', 'attach_umm_quwain_municipality_document', 'attach_rak_municipality_document', 'attach_fujairah_municipality_document', 'attach_abu_dhabi_municipality_document', 'attach_al_ain_municipality_document'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'trashed' => 'Trashed',
            'trashed_at' => 'Trashed At',
            'trashed_by' => 'Trashed By',
            'status_verified' => 'Status Verified',
            'status_verified_by' => 'Status Verified By',
            'status_verified_at' => 'Status Verified At',
            'valuation_id' => 'Valuation ID',
            'attach_dld_property_status_document' => 'Attach DLD Property Status Document',
            'attach_sharjah_municipality_data_document' => 'Attach Sharjah Municipality Data Document',
            'attach_ajman_municipality_document' => 'Attach Ajman Municipality Document',
            'attach_umm_quwain_municipality_document' => 'Attach Umm Quwain Municipality Document',
            'attach_rak_municipality_document' => 'Attach RAK Municipality Document',
            'attach_fujairah_municipality_document' => 'Attach Fujairah Municipality Document',
            'attach_abu_dhabi_municipality_document' => 'Attach Abu Dhabi Municipality Document',
            'attach_al_ain_municipality_document' => 'Attach Al Ain Municipality Document',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {

        if (isset($this->agents_data) && $this->agents_data <> null) {
            ValuationAgents::deleteAll(['valuation_id' => $this->valuation_id]);

            // Save all payments terms
            foreach ($this->agents_data as $agent_data) {
                $agent_data_detail = new ValuationAgents();
                $agent_data_detail->prefix = $agent_data['prefix'];
                $agent_data_detail->name = $agent_data['name'];
                $agent_data_detail->lastname = $agent_data['lastname'];
                $agent_data_detail->percentage = $agent_data['percentage'];
                $agent_data_detail->index_id = $agent_data['index_id'];
                $agent_data_detail->valuation_id =$this->valuation_id;
                if(!$agent_data_detail->save()){
                    // echo "<pre>";
                    // print_r($agent_data_detail);
                    // die;
                }
            }
        }

        if (isset($this->received_docs) && $this->received_docs <> null) {
            // Save Documents
            foreach ($this->received_docs as $received_doc) {
                if ($received_doc['attachment'] <> null) {
                    $owner_data_detail = PublicDocsFiles::find()
                        ->where([
                            'document_id' => $received_doc['document_id'],
                            'valuation_id' => $received_doc['valuation_id']
                        ])->one();
                    if ($owner_data_detail==null) {
                        $owner_data_detail = new PublicDocsFiles();

                    }
                    $owner_data_detail->document_id = $received_doc['document_id'];
                    $owner_data_detail->attachment = $received_doc['attachment'];
                    $owner_data_detail->valuation_id = $received_doc['valuation_id'];
                    if(!$owner_data_detail->save()){
                        // echo "<pre>";
                        // print_r($owner_data_detail);
                        // die;
                    }
                }

            }
        }

        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }

    public function getValuationagents()
    {
        return $this->hasMany(ValuationAgents::className(), ['valuation_id' => 'valuation_id']);
    }

    public function beforeSave($insert){

        
        // verify status reset after edit only something changed
        $isChanged = false;
        $ignoredAttributes = ['updated_at','status_verified_at']; 
        foreach ($this->attributes as $attribute => $value) {
            if (in_array($attribute, $ignoredAttributes)) {
                continue;
            }
            if ($this->getOldAttribute($attribute) != $value) {
                $isChanged = true;
                break;
            }
        }
        if ($isChanged) {
            if($this->status == 1 && $this->getOldAttribute('status') == 1){
                $this->status = 2;
            }elseif ($this->status == 2 && $this->getOldAttribute('status') == 1) {
                $this->status = 1;
            }
        }
        
        return parent::beforeSave($insert);
    }
}
