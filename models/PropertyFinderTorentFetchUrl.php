<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "property_finder_torent_fetch_url".
 *
 * @property int $id
 * @property int|null $parent_link_id
 * @property string|null $url_to_fetch
 * @property string|null $property_category
 */
class PropertyFinderTorentFetchUrl extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'property_finder_torent_fetch_url';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_link_id'], 'integer'],
            [['url_to_fetch', 'property_category'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_link_id' => 'Parent Link ID',
            'url_to_fetch' => 'Url To Fetch',
            'property_category' => 'Property Category',
        ];
    }
}
