<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Valuation;
use Yii;

/**
 * ValuationSearch represents the model behind the search form of `app\models\Valuation`.
 */
class ValuationSearch extends Valuation
{
    public $inspection_date,$inspection_time,$sub_community,$city,$valuer,$search_status_check,$include_signature;
    public $instruction_date_btw,$community,$target_date_btw,$valuation_status,$inspection_date_btw,$client_revenue,$pageSize,$inspection_done_date,$challenge;
    public $custom_date_btw, $time_period, $target,$parent_target;
    public $client_invoice_type;
    public $reminder_id;
    public $today_doc;
    public $valuation_approach;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'no_of_owners', 'service_officer_name', 'building_info','unit_number', 'status', 'floor_number', 'instruction_person', 'purpose_of_valuation', 'created_by', 'updated_by', 'trashed', 'trashed_by'], 'integer'],
            [['search_status_check','reference_number', 'client_reference', 'client_name_passport', 'instruction_date', 'target_date', 'payment_plan', 'created_at', 'updated_at', 'trashed_at','inspection_date','inspection_time','sub_community','city','valuer','valuation_status','approval_id','ban_date','include_signature',
                'instruction_date_btw','community','valuer','target_date_btw','inspection_date_btw','client_revenue','pageSize','property_id','revised_reason','inspection_done_date','challenge','taqyeem_number','valuation_approach' , 'total_fee' , 'submission_approver_date'], 'safe'],
            [['land_size'], 'number'],
            [['custom_date_btw','time_period', 'target','parent_target'], 'safe'],
            [['client_invoice_type','reminder_id','today_doc'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$valuation_status=1)
    {
        // $query = Valuation::find()->joinWith(['scheduleInspection','building','user']);

        //new code
        if(isset(Yii::$app->request->queryParams['ValuationSearch']['time_period']) && Yii::$app->request->queryParams['ValuationSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationSearch']['custom_date_btw'];
                    $Date=(explode(" - ",$custome_date_btw));
                    $start_date = $Date[0].' 00:00:00';
                    $end_date = $Date[1].' 23:59:59';
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationSearch']['time_period']);
                $start_date = $date_array['start_date'].' 00:00:00';
                $end_date = $date_array['end_date'].' 23:59:59';
            }
        }else{
            $start_date = '2021-04-28 00:00:00';
            $end_date = date('Y-m-d').' 23:59:59';

        }
        //end new code
        

        if ($valuation_status==3) {
            $query = Valuation::find()->andWhere(['!=','valuation_status', 5])->joinWith(['scheduleInspection','building','user']);
        }else {
            $query = Valuation::find()->joinWith(['scheduleInspection','building','user']);

        }

        // this is the code where only service officer will see his own
        if(Yii::$app->request->queryParams['ValuationSearch']['target'] == "from_dashboard")
        {

        }else{
            if (Yii::$app->user->identity->permission_group_id==9 || Yii::$app->user->identity->permission_group_id==3 || Yii::$app->user->identity->permission_group_id==40) {
                $query->where(['valuation.service_officer_name'=>Yii::$app->user->identity->id]);
            }
        }
        $this->load($params);

        if(isset(Yii::$app->request->queryParams['ValuationSearch']['time_period']) && Yii::$app->request->queryParams['ValuationSearch']['time_period'] >= 0) 
        {
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
                'pagination' => false
            ]);

        }else{
            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            ]);
        }


        if(in_array($this->search_status_check , [1,2,3,4])){
            
        }else {
            if ($this->valuation_approach == 1) {
                $query->andWhere(['valuation.valuation_approach' => 1]);
            } else if ($this->valuation_approach == 2) {
                $query->andWhere(['valuation.valuation_approach' => 2]);
            } else if ($this->valuation_approach == 3) {
                $query->andWhere(['valuation.valuation_approach' => 3]);
            } else {
                $query->andWhere(['valuation.valuation_approach' => 0]);
            }
        }

        // else {
        //     $query->andWhere(['valuation.valuation_approach' => 0 ]);
        // }


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // dd($query);

        if($this->search_status_check == 1){
            $query->andFilterWhere(['IN', 'valuation_status', [5]])
            ->andWhere(['between', 'valuation.instruction_date', $start_date, $end_date]);
        }else if($this->search_status_check == 2){
            $query->andFilterWhere(['IN', 'valuation_status', [1,2,3,4,7,8]])
            ->andWhere(['between', 'valuation.instruction_date', $start_date, $end_date]);
        }else if($this->search_status_check == 3){
            $query->andFilterWhere(['IN', 'valuation_status', [1]])
            ->andWhere(['between', 'valuation.instruction_date', $start_date, $end_date]);
        }else if($this->search_status_check == 4){
            $query->andFilterWhere(['IN', 'valuation_status', [2]])
            ->andWhere(['between', 'valuation.instruction_date', $start_date, $end_date]);
        }else{
            $query->andFilterWhere(['IN', 'valuation_status', [6,9,10,11,12]])
            ->andWhere(['between', 'valuation.instruction_date', $start_date, $end_date]);
        }

        $emiratedListArr = Yii::$app->appHelperFunctions->emiratedListArr;
        $query->andFilterWhere(['like', 'reference_number', $this->reference_number])
            ->andFilterWhere(['like', 'client_reference', $this->client_reference])
            ->andFilterWhere(['like', 'signature_img', $this->signature_img])
            ->andFilterWhere(['like', 'client_name_passport', $this->client_name_passport])
            ->andFilterWhere(['=', 'client_id', $this->client_id])
            ->andFilterWhere(['=', 'valuation.property_id', $this->property_id])
            ->andFilterWhere(['=', 'valuation.building_info', $this->building_info])
            ->andFilterWhere(['like', 'valuation.community', $this->community])
            ->andFilterWhere(['in', 'valuation.city', $emiratedListArr[$this->city]])
            ->andFilterWhere(['=', 'valuation.total_fee', $this->total_fee])
            ->andFilterWhere(['like', 'valuation.submission_approver_date', $this->submission_approver_date])
            ->andFilterWhere(['like', 'payment_plan', $this->payment_plan]);

            // dd($query);

        $query->andFilterWhere([
            //  'id' => $this->id,
            'no_of_owners' => $this->no_of_owners,
            'service_officer_name' => $this->service_officer_name,
            'instruction_date' => ($this->instruction_date <> null) ? date('Y-m-d',strtotime($this->instruction_date)) : $this->instruction_date,
            'target_date' => ($this->target_date <> null) ? date('Y-m-d',strtotime($this->target_date)) : $this->target_date,
            'building_info' => $this->building_info,
            'unit_number' => $this->unit_number,
            'status' => $this->status,
            'floor_number' => $this->floor_number,
            'instruction_person' => $this->instruction_person,
            'land_size' => $this->land_size,
            'purpose_of_valuation' => $this->purpose_of_valuation,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => $this->trashed,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
            'schedule_inspection.inspection_date'=> ($this->inspection_date <> null) ? date('Y-m-d',strtotime($this->inspection_date)) : $this->inspection_date,
            'schedule_inspection.inspection_time'=>$this->inspection_time,
            'buildings.sub_community'=>$this->sub_community,
            'buildings.city'=>$this->city,
            // 'service_officer_name'=>$this->valuer,
            'valuation_status'=>$this->valuation_status,
            'taqyeem_number'=>$this->taqyeem_number,

        ]);

        return $dataProvider;
    }

    public function search_income($params,$valuation_status=1)
    {
        // $query = Valuation::find()->joinWith(['scheduleInspection','building','user']);

        if ($valuation_status==3) {
            $query = Valuation::find()->andWhere(['!=','valuation_status', 5])->joinWith(['scheduleInspection','building','user']);
        }else {
            $query = Valuation::find()->joinWith(['scheduleInspection','building','user']);

        }

        if (Yii::$app->user->identity->permission_group_id==3 || Yii::$app->user->identity->permission_group_id==40) {
            $query->where(['valuation.service_officer_name'=>Yii::$app->user->identity->id]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => false, // Disable pagination
        ]);

        $this->load($params);


            if ($this->valuation_approach == 1) {
                $query->andWhere(['valuation.valuation_approach' => 1]);
            } else if ($this->valuation_approach == 2) {
                $query->andWhere(['valuation.valuation_approach' => 2]);
            } else if ($this->valuation_approach == 3) {
                $query->andWhere(['valuation.valuation_approach' => 3]);
            } else {
                $query->andWhere(['valuation.valuation_approach' => 0]);
            }


        // else {
        //     $query->andWhere(['valuation.valuation_approach' => 0 ]);
        // }


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            //  'id' => $this->id,
            'client_id' => $this->client_id,
            'no_of_owners' => $this->no_of_owners,
            'service_officer_name' => $this->service_officer_name,
            'instruction_date' => ($this->instruction_date <> null) ? date('Y-m-d',strtotime($this->instruction_date)) : $this->instruction_date,
            'target_date' => ($this->target_date <> null) ? date('Y-m-d',strtotime($this->target_date)) : $this->target_date,
            'building_info' => $this->building_info,
            'unit_number' => $this->unit_number,
            'status' => $this->status,
            'floor_number' => $this->floor_number,
            'instruction_person' => $this->instruction_person,
            'land_size' => $this->land_size,
            'purpose_of_valuation' => $this->purpose_of_valuation,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => $this->trashed,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
            'schedule_inspection.inspection_date'=> ($this->inspection_date <> null) ? date('Y-m-d',strtotime($this->inspection_date)) : $this->inspection_date,
            'schedule_inspection.inspection_time'=>$this->inspection_time,
            'buildings.sub_community'=>$this->sub_community,
            'buildings.city'=>$this->city,
            // 'service_officer_name'=>$this->valuer,
            'valuation_status'=>$this->valuation_status,
            'taqyeem_number'=>$this->taqyeem_number,

        ]);


        if($this->search_status_check == 1){
            $query->andFilterWhere(['IN', 'valuation_status', [5]]);
        }else if($this->search_status_check == 2){
            $query->andFilterWhere(['IN', 'valuation_status', [1,2,3,4,7,8]]);
        }else if($this->search_status_check == 3){
            $query->andFilterWhere(['IN', 'valuation_status', [1]]);
        }else if($this->search_status_check == 4){
            $query->andFilterWhere(['IN', 'valuation_status', [2]]);
        }else{
            $query->andFilterWhere(['IN', 'valuation_status', [6,9,10,11,12]]);
        }
        $query->andFilterWhere(['like', 'reference_number', $this->reference_number])
            ->andFilterWhere(['like', 'client_reference', $this->client_reference])
            ->andFilterWhere(['like', 'signature_img', $this->signature_img])
            ->andFilterWhere(['like', 'client_name_passport', $this->client_name_passport])
            ->andFilterWhere(['like', 'payment_plan', $this->payment_plan]);

        return $dataProvider;
    }

    public function search_inspections($params)
    {
        // $query = Valuation::find()->joinWith(['scheduleInspection','building','user']);
        $valuation_status=3;
        if ($valuation_status==3) {

            $query = Valuation::find()->andWhere(['!=','valuation_status', 5])->joinWith(['scheduleInspection','building','user']);
        }else {
            $query = Valuation::find()->joinWith(['scheduleInspection1','building','user']);

        }

       // if (Yii::$app->user->identity->permission_group_id==3) {
            $query->where(['schedule_inspection.inspection_officer'=>Yii::$app->user->identity->id]);
      //  }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);
        // else {
        //     $query->andWhere(['valuation.valuation_approach' => 0 ]);
        // }


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            //  'id' => $this->id,
            'client_id' => $this->client_id,
            'no_of_owners' => $this->no_of_owners,
            'service_officer_name' => $this->service_officer_name,
            'instruction_date' => ($this->instruction_date <> null) ? date('Y-m-d',strtotime($this->instruction_date)) : $this->instruction_date,
            'target_date' => ($this->target_date <> null) ? date('Y-m-d',strtotime($this->target_date)) : $this->target_date,
            'building_info' => $this->building_info,
            'unit_number' => $this->unit_number,
            'status' => $this->status,
            'floor_number' => $this->floor_number,
            'instruction_person' => $this->instruction_person,
            'land_size' => $this->land_size,
            'purpose_of_valuation' => $this->purpose_of_valuation,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => $this->trashed,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
            'schedule_inspection.inspection_date'=> date('Y-m-d'),
            'schedule_inspection.inspection_time'=>$this->inspection_time,
            'buildings.sub_community'=>$this->sub_community,
            'buildings.city'=>$this->city,
            // 'service_officer_name'=>$this->valuer,
            'valuation_status'=>$this->valuation_status,
            'taqyeem_number'=>$this->taqyeem_number,

        ]);


        if($this->search_status_check == 1){
            $query->andFilterWhere(['IN', 'valuation_status', [5]]);
        }else if($this->search_status_check == 2){
            $query->andFilterWhere(['IN', 'valuation_status', [1,2,3,4,7,8]]);
        }else if($this->search_status_check == 3){
            $query->andFilterWhere(['IN', 'valuation_status', [1]]);
        }else if($this->search_status_check == 4){
            $query->andFilterWhere(['IN', 'valuation_status', [2]]);
        }else{
            $query->andFilterWhere(['IN', 'valuation_status', [6,9,10,11,12]]);
        }
        $query->andFilterWhere(['like', 'reference_number', $this->reference_number])
            ->andFilterWhere(['like', 'client_reference', $this->client_reference])
            ->andFilterWhere(['like', 'signature_img', $this->signature_img])
            ->andFilterWhere(['like', 'client_name_passport', $this->client_name_passport])
            ->andFilterWhere(['like', 'payment_plan', $this->payment_plan]);


        return $dataProvider;
    }

    public function search_signature($params,$valuation_status=1)
    {


        // print_r($sginature_img);
        // die();

        $query = Valuation::find();

        if ($valuation_status==3) {
            $query->where(['valuation_status'=>$valuation_status]);
        }
        if($this->include_signature==true){
            $query->where(['not', ['signature_img' => ['','.',null]]]);
        }




        $query->joinWith(['scheduleInspection','building','user']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if (Yii::$app->user->identity->permission_group_id==3) {
            $query->where(['valuation.service_officer_name'=>Yii::$app->user->identity->id]);
        }
        if($this->include_signature==true){
            $query->andWhere(['not', ['signature_img' => ['','.',null]]]);
        }
        if($this->include_signature==false){
            $query->andWhere(['or',['signature_img'=>null],['signature_img'=>''],['signature_img'=>'.']]);
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'no_of_owners' => $this->no_of_owners,
            'service_officer_name' => $this->service_officer_name,
            'instruction_date' => ($this->instruction_date <> null) ? date('Y-m-d',strtotime($this->instruction_date)) : $this->instruction_date,
            'target_date' => ($this->target_date <> null) ? date('Y-m-d',strtotime($this->target_date)) : $this->target_date,
            'building_info' => $this->building_info,
            'unit_number' => $this->unit_number,
            'status' => $this->status,
            'floor_number' => $this->floor_number,
            'instruction_person' => $this->instruction_person,
            'land_size' => $this->land_size,
            'purpose_of_valuation' => $this->purpose_of_valuation,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => $this->trashed,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
            'schedule_inspection.inspection_date'=> ($this->inspection_date <> null) ? date('Y-m-d',strtotime($this->inspection_date)) : $this->inspection_date,
            'schedule_inspection.inspection_time'=>$this->inspection_time,
            'buildings.sub_community'=>$this->sub_community,
            'buildings.city'=>$this->city,
            // 'service_officer_name'=>$this->valuer,
            'valuation_status'=>$this->valuation_status,

        ]);

        $query->andFilterWhere(['like', 'reference_number', $this->reference_number])
            ->andFilterWhere(['like', 'client_reference', $this->client_reference])
            ->andFilterWhere(['like', 'signature_img', $this->signature_img])
            ->andFilterWhere(['like', 'client_name_passport', $this->client_name_passport])
            ->andFilterWhere(['like', 'payment_plan', $this->payment_plan]);

        return $dataProvider;
    }

    public function search_all_reports($params)
    {
        /*  echo "<pre>";
          print_r($params);
          die;*/

        $query = Valuation::find();

        /* if ($valuation_status==3) {
             $query->where(['valuation_status'=>$valuation_status]);
         }
         if($this->include_signature==true){
             $query->where(['not', ['signature_img' => ['','.',null]]]);
         }
         if($this->include_signature==false){
             $query->where(['or',['signature_img'=>null],['signature_img'=>''],['signature_img'=>'.']]);
         }*/




        $query->joinWith(['scheduleInspection','building','inspectProperty']);
        // $query->innerJoinWith(['scheduleInspection','client','approver','building','user']);

        // add conditions that should always apply here

        if (Yii::$app->user->identity->permission_group_id==3) {
            $query->where(['valuation.created_by'=>Yii::$app->user->identity->id]);
        }



        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['instruction_date' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $Date=(explode(" - ",$this->instruction_date_btw));
        $TargetDate=(explode(" - ",$this->target_date_btw));
        $InspectionDate=(explode(" - ",$this->inspection_date_btw));


        // grid filtering conditions
        $query->andFilterWhere([
            'client_id' => $this->client_id,
            'buildings.community' => $this->community,
            'approval_id' => $this->approver,
            'valuation.service_officer_name' => $this->valuer,
            'no_of_owners' => $this->no_of_owners,
            //'service_officer_name' => $this->service_officer_name,
            'instruction_date' => ($this->instruction_date <> null) ? date('Y-m-d',strtotime($this->instruction_date)) : $this->instruction_date,
            'target_date' => ($this->target_date <> null) ? date('Y-m-d',strtotime($this->target_date)) : $this->target_date,
            'building_info' => $this->building_info,
            'unit_number' => $this->unit_number,
            'floor_number' => $this->floor_number,
            'instruction_person' => $this->instruction_person,
            'instruction_person' => $this->instruction_person,
            'land_size' => $this->land_size,
            'purpose_of_valuation' => $this->purpose_of_valuation,
            'valuation.property_id' => $this->property_id,
            'schedule_inspection.inspection_date'=> ($this->inspection_date <> null) ? date('Y-m-d',strtotime($this->inspection_date)) : $this->inspection_date,
            'schedule_inspection.inspection_time'=>$this->inspection_time,
            'buildings.sub_community'=>$this->sub_community,
            'buildings.city'=>$this->city,
            'valuation_status'=>$this->valuation_status,

        ]);

        $query->andFilterWhere(['like', 'reference_number', $this->reference_number])
            ->andFilterWhere(['like', 'client_reference', $this->client_reference])
            ->andFilterWhere(['like', 'signature_img', $this->signature_img])
            ->andFilterWhere(['like', 'client_name_passport', $this->client_name_passport])
            ->andFilterWhere(['like', 'payment_plan', $this->payment_plan])
            ->andFilterWhere(['>=', 'instruction_date', $Date[0]])
            ->andFilterWhere(['<', 'instruction_date', $Date[1]])
            ->andFilterWhere(['>=', 'target_date', $TargetDate[0]])
            ->andFilterWhere(['<', 'target_date', $TargetDate[1]])
            ->andFilterWhere(['>=', 'submission_approver_date', $InspectionDate[0].' 00:00:00'])
            ->andFilterWhere(['<=', 'submission_approver_date', $InspectionDate[1].' 23:59:59']);
        /*            ->andFilterWhere(['>=', 'schedule_inspection.valuation_report_date', $InspectionDate[0]])
                    ->andFilterWhere(['<', 'schedule_inspection.valuation_report_date', $InspectionDate[1]]);*/
        if(isset($this->inspection_date) && $this->inspection_date <> null){

            $query->andFilterWhere(['<>', 'valuation_status', 9]);
            $query->andFilterWhere(['<>', 'valuation_status', 6]);
        }
        // $query->andWhere(['not', ['quotation_id' => null]]);
        $query->andWhere(['is', 'parent_id', new \yii\db\Expression('null')]);
        // $query->andWhere(['is', 'quotation_id', new \yii\db\Expression('null')]);

        if(isset($this->revised_reason) && $this->revised_reason <> null){

            $query->andFilterWhere(['IN', 'revised_reason', [1,2]]);
        }

        if(isset($this->inspection_done_date) && $this->inspection_done_date <> null){

            $query->andFilterWhere(['like', 'inspect_property.inspection_done_date', $this->inspection_done_date]);
        }

        if(isset($this->challenge) && $this->challenge <> null){

            $query->andFilterWhere(['like', 'valuation.created_at', $this->challenge]);
        }


        $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
        if ($this->pageSize<>null) {
            if ($this->pageSize==4) {
                $dataProvider->pagination->pageSize=false;
            }else {
                $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
            }

        }

        return $dataProvider;
    }



    public function search_all($params)
    {
        // echo "<pre>";
        // print_r($params);
        // die;

        $query = Valuation::find();

        /* if ($valuation_status==3) {
             $query->where(['valuation_status'=>$valuation_status]);
         }
         if($this->include_signature==true){
             $query->where(['not', ['signature_img' => ['','.',null]]]);
         }
         if($this->include_signature==false){
             $query->where(['or',['signature_img'=>null],['signature_img'=>''],['signature_img'=>'.']]);
         }*/


         $query->joinWith(['building']);
        // $query->innerJoinWith(['scheduleInspection','client','approver','building','user']);

        // add conditions that should always apply here

        if (Yii::$app->user->identity->permission_group_id==3) {
            $query->where(['valuation.created_by'=>Yii::$app->user->identity->id]);
        }

        $query->where(['valuation_status' => [5]]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $Date=(explode(" - ",$this->instruction_date_btw));
        $TargetDate=(explode(" - ",$this->target_date_btw));
        $InspectionDate=(explode(" - ",$this->inspection_date_btw));


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'buildings.community' => $this->community,
            'approval_id' => $this->approver,
            'valuation.service_officer_name' => $this->valuer,
            'no_of_owners' => $this->no_of_owners,
            //'service_officer_name' => $this->service_officer_name,
            'instruction_date' => ($this->instruction_date <> null) ? date('Y-m-d',strtotime($this->instruction_date)) : $this->instruction_date,
            'target_date' => ($this->target_date <> null) ? date('Y-m-d',strtotime($this->target_date)) : $this->target_date,
            'building_info' => $this->building_info,
            'unit_number' => $this->unit_number,
            'valuation.status' => $this->status,
            'floor_number' => $this->floor_number,
            'instruction_person' => $this->instruction_person,
            'instruction_person' => $this->instruction_person,
            'land_size' => $this->land_size,
            'purpose_of_valuation' => $this->purpose_of_valuation,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => $this->trashed,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
            'valuation.property_id' => $this->property_id,
            /*   'schedule_inspection.inspection_date'=> ($this->inspection_date <> null) ? date('Y-m-d',strtotime($this->inspection_date)) : $this->inspection_date,
               'schedule_inspection.inspection_time'=>$this->inspection_time,*/
            'buildings.sub_community'=>$this->sub_community,
            'buildings.city'=>$this->city,
            'valuation_status'=>$this->valuation_status,

        ]);
        if(isset($InspectionDate[1])){
            $InspectionDate[1] = date('Y-m-d', strtotime('+1 day', strtotime($InspectionDate[1])));

        }

        $query->andFilterWhere(['like', 'reference_number', $this->reference_number])
            ->andFilterWhere(['like', 'client_reference', $this->client_reference])
            ->andFilterWhere(['like', 'signature_img', $this->signature_img])
            ->andFilterWhere(['like', 'client_name_passport', $this->client_name_passport])
            ->andFilterWhere(['like', 'payment_plan', $this->payment_plan])
            ->andFilterWhere(['>=', 'instruction_date', $Date[0]])
            ->andFilterWhere(['<', 'instruction_date', $Date[1]])
            ->andFilterWhere(['>=', 'target_date', $TargetDate[0]])
            ->andFilterWhere(['<', 'target_date', $TargetDate[1]])
            ->andFilterWhere(['>=', 'submission_approver_date', $InspectionDate[0]])
            ->andFilterWhere(['<', 'submission_approver_date', $InspectionDate[1]]);
        /*            ->andFilterWhere(['>=', 'schedule_inspection.valuation_report_date', $InspectionDate[0]])
                    ->andFilterWhere(['<', 'schedule_inspection.valuation_report_date', $InspectionDate[1]]);*/
        if(isset($this->inspection_date) && $this->inspection_date <> null){

            $query->andFilterWhere(['<>', 'valuation_status', 9]);
            $query->andFilterWhere(['<>', 'valuation_status', 6]);
        }

        if(isset($this->revised_reason) && $this->revised_reason <> null){

            $query->andFilterWhere(['IN', 'revised_reason', $this->revised_reason]);
        }

        if(isset($this->inspection_done_date) && $this->inspection_done_date <> null){

            $query->andFilterWhere(['like', 'inspect_property.inspection_done_date', $this->inspection_done_date]);
        }

        if(isset($this->challenge) && $this->challenge <> null){

            $query->andFilterWhere(['like', 'valuation.created_at', $this->challenge]);
        }
        $query->andFilterWhere(['like', 'taqyeem_number', $this->taqyeem_number]);


        if($this->client_invoice_type<>null){
            $query->andFilterWhere(['=', Valuation::tableName().'.client_invoice_type', $this->client_invoice_type]);
        }


        $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
        if ($this->pageSize<>null) {
            if ($this->pageSize==4) {
                $dataProvider->pagination->pageSize=false;
            }else {
                $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
            }
        }

        return $dataProvider;
    }

    public function searchTodayApproved($params,$ids)
    {
        $valuation_status=1;
        // $query = Valuation::find()->joinWith(['scheduleInspection','building','user']);

        if ($valuation_status==3) {
            $query = Valuation::find()->andWhere(['!=','valuation_status', 5])->joinWith(['scheduleInspection','building','user']);
        }else {
            $query = Valuation::find()->joinWith(['scheduleInspection','building','user']);

        }
        $query->andWhere(['valuation.parent_id' => null]);

        if (Yii::$app->user->identity->permission_group_id==3) {
            $query->where(['valuation.service_officer_name'=>Yii::$app->user->identity->id]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => false, // Disable pagination
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        // grid filtering conditions
        $query->andFilterWhere([
            //   'id' => $this->id,
            'client_id' => $this->client_id,
            'no_of_owners' => $this->no_of_owners,
            'service_officer_name' => $this->service_officer_name,
            'instruction_date' => ($this->instruction_date <> null) ? date('Y-m-d',strtotime($this->instruction_date)) : $this->instruction_date,
            'target_date' => ($this->target_date <> null) ? date('Y-m-d',strtotime($this->target_date)) : $this->target_date,
            'building_info' => $this->building_info,
            'unit_number' => $this->unit_number,
            'status' => $this->status,
            'floor_number' => $this->floor_number,
            'instruction_person' => $this->instruction_person,
            'land_size' => $this->land_size,
            'purpose_of_valuation' => $this->purpose_of_valuation,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => $this->trashed,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
            'schedule_inspection.inspection_date'=> ($this->inspection_date <> null) ? date('Y-m-d',strtotime($this->inspection_date)) : $this->inspection_date,
            'schedule_inspection.inspection_time'=>$this->inspection_time,
            'buildings.sub_community'=>$this->sub_community,
            'buildings.city'=>$this->city,
            // 'service_officer_name'=>$this->valuer,
            'valuation_status'=>$this->valuation_status,

        ]);

        $today_date = date("Y-m-d");
        $query->andFilterWhere(['IN', 'valuation.valuation_status', [5]]);
        // $query->andFilterWhere(['like', 'valuation.submission_approver_date', $today_date]);

        /* echo "<pre>";
         print_r($ids);
         die;*/
        $query->andWhere(['IN', 'valuation.id', $ids]);
        $query->andFilterWhere(['like', 'reference_number', $this->reference_number])
            ->andFilterWhere(['like', 'client_reference', $this->client_reference])
            ->andFilterWhere(['like', 'signature_img', $this->signature_img])
            ->andFilterWhere(['like', 'client_name_passport', $this->client_name_passport])
            ->andFilterWhere(['like', 'payment_plan', $this->payment_plan]);

        return $dataProvider;
    }
    public function searchTodayRecomended($params,$ids)
    {
        $valuation_status=1;

        // $query = Valuation::find()->joinWith(['scheduleInspection','building','user']);

        if ($valuation_status==3) {
            $query = Valuation::find()->andWhere(['!=','valuation_status', 5])->joinWith(['scheduleInspection','building','user']);
        }else {
            $query = Valuation::find()->joinWith(['scheduleInspection','building','user']);

        }

        if (Yii::$app->user->identity->permission_group_id==3) {
            $query->where(['valuation.service_officer_name'=>Yii::$app->user->identity->id]);
        }

        // add conditions that should always apply here

        //start by usama
        if(isset($params['ValuationSearch']['time_period'])){
            $this->time_period  = $params['ValuationSearch']['time_period'];
        }
        if(isset($params['ValuationSearch']['custom_date_btw'])){
            $this->custom_date_btw = $params['ValuationSearch']['custom_date_btw'];
        }

        if($this->time_period <> null && $this->time_period != 0 ) {
            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {
                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query->andFilterWhere([
                'between', Valuation::tableName().'.instruction_date', $from_date." 00:00:00", $to_date." 23:59:59"
            ]);
        }
        //end start by usama

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
            'pagination' => false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        // grid filtering conditions
        $query->andFilterWhere([
            //   'id' => $this->id,
            'client_id' => $this->client_id,
            'no_of_owners' => $this->no_of_owners,
            'service_officer_name' => $this->service_officer_name,
            'instruction_date' => ($this->instruction_date <> null) ? date('Y-m-d',strtotime($this->instruction_date)) : $this->instruction_date,
            'target_date' => ($this->target_date <> null) ? date('Y-m-d',strtotime($this->target_date)) : $this->target_date,
            'building_info' => $this->building_info,
            'unit_number' => $this->unit_number,
            'status' => $this->status,
            'floor_number' => $this->floor_number,
            'instruction_person' => $this->instruction_person,
            'land_size' => $this->land_size,
            'purpose_of_valuation' => $this->purpose_of_valuation,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => $this->trashed,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
            'schedule_inspection.inspection_date'=> ($this->inspection_date <> null) ? date('Y-m-d',strtotime($this->inspection_date)) : $this->inspection_date,
            'schedule_inspection.inspection_time'=>$this->inspection_time,
            'buildings.sub_community'=>$this->sub_community,
            'buildings.city'=>$this->city,
            // 'service_officer_name'=>$this->valuer,
            'valuation_status'=>$this->valuation_status,

        ]);

        // $query->andFilterWhere(['IN', 'valuation.valuation_status', [3,5]]);
        /* echo "<pre>";
         print_r($ids);
         die;*/
        $query->andWhere(['IN', 'valuation.id', $ids]);
        $query->andFilterWhere(['like', 'reference_number', $this->reference_number])
            ->andFilterWhere(['like', 'client_reference', $this->client_reference])
            ->andFilterWhere(['like', 'signature_img', $this->signature_img])
            ->andFilterWhere(['like', 'client_name_passport', $this->client_name_passport])
            ->andFilterWhere(['like', 'payment_plan', $this->payment_plan]);

        return $dataProvider;
    }
    public function searchTodayChallenged($params,$ids)
    {
        $valuation_status=1;
        // $query = Valuation::find()->joinWith(['scheduleInspection','building','user']);

        if ($valuation_status==3) {
            $query = Valuation::find()->andWhere(['!=','valuation_status', 5])->joinWith(['scheduleInspection','building','user']);
        }else {
            $query = Valuation::find()->joinWith(['scheduleInspection','building','user']);

        }

        if (Yii::$app->user->identity->permission_group_id==3) {
            $query->where(['valuation.service_officer_name'=>Yii::$app->user->identity->id]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        // grid filtering conditions
        $query->andFilterWhere([
            //   'id' => $this->id,
            'client_id' => $this->client_id,
            'no_of_owners' => $this->no_of_owners,
            'service_officer_name' => $this->service_officer_name,
            'instruction_date' => ($this->instruction_date <> null) ? date('Y-m-d',strtotime($this->instruction_date)) : $this->instruction_date,
            'target_date' => ($this->target_date <> null) ? date('Y-m-d',strtotime($this->target_date)) : $this->target_date,
            'building_info' => $this->building_info,
            'unit_number' => $this->unit_number,
            'status' => $this->status,
            'floor_number' => $this->floor_number,
            'instruction_person' => $this->instruction_person,
            'land_size' => $this->land_size,
            'purpose_of_valuation' => $this->purpose_of_valuation,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => $this->trashed,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
            'schedule_inspection.inspection_date'=> ($this->inspection_date <> null) ? date('Y-m-d',strtotime($this->inspection_date)) : $this->inspection_date,
            'schedule_inspection.inspection_time'=>$this->inspection_time,
            'buildings.sub_community'=>$this->sub_community,
            'buildings.city'=>$this->city,
            // 'service_officer_name'=>$this->valuer,
            'valuation_status'=>$this->valuation_status,

        ]);


        /* echo "<pre>";
         print_r($ids);
         die;*/
        $query->andWhere(['IN', 'valuation.id', $ids]);
        $query->andFilterWhere(['like', 'reference_number', $this->reference_number])
            ->andFilterWhere(['like', 'client_reference', $this->client_reference])
            ->andFilterWhere(['like', 'signature_img', $this->signature_img])
            ->andFilterWhere(['like', 'client_name_passport', $this->client_name_passport])
            ->andFilterWhere(['like', 'payment_plan', $this->payment_plan]);

        return $dataProvider;
    }

    public function searchTodayInspected($params,$ids)
    {
        $valuation_status=1;
        // $query = Valuation::find()->joinWith(['scheduleInspection','building','user']);

        if ($valuation_status==3) {
            $query = Valuation::find()->andWhere(['!=','valuation_status', 5])->joinWith(['scheduleInspection','building','user']);
        }else {
            $query = Valuation::find()->joinWith(['scheduleInspection','building','user','inspect_property']);

        }

        if (Yii::$app->user->identity->permission_group_id==3) {
            $query->where(['valuation.service_officer_name'=>Yii::$app->user->identity->id]);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        // grid filtering conditions
        $query->andFilterWhere([
            //   'id' => $this->id,
            'client_id' => $this->client_id,
            'no_of_owners' => $this->no_of_owners,
            'service_officer_name' => $this->service_officer_name,
            'instruction_date' => ($this->instruction_date <> null) ? date('Y-m-d',strtotime($this->instruction_date)) : $this->instruction_date,
            'target_date' => ($this->target_date <> null) ? date('Y-m-d',strtotime($this->target_date)) : $this->target_date,
            'building_info' => $this->building_info,
            'unit_number' => $this->unit_number,
            'status' => $this->status,
            'floor_number' => $this->floor_number,
            'instruction_person' => $this->instruction_person,
            'land_size' => $this->land_size,
            'purpose_of_valuation' => $this->purpose_of_valuation,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => $this->trashed,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
            'schedule_inspection.inspection_date'=> ($this->inspection_date <> null) ? date('Y-m-d',strtotime($this->inspection_date)) : $this->inspection_date,
            'schedule_inspection.inspection_time'=>$this->inspection_time,
            'inspect_property.inspection_done_date'=>($this->inspection_done_date <> null) ? date('Y-m-d',strtotime($this->inspection_done_date)) : $this->inspection_done_date,
            'buildings.sub_community'=>$this->sub_community,
            'buildings.city'=>$this->city,
            // 'service_officer_name'=>$this->valuer,
            'valuation_status'=>$this->valuation_status,

        ]);
        /* if($this->search_status_check == 1){
             $query->andFilterWhere(['IN', 'valuation_status', [5]]);
         }else if($this->search_status_check == 2){
             $query->andFilterWhere(['IN', 'valuation_status', [1,2,3,4,7,8]]);
         }else{
             $query->andFilterWhere(['IN', 'valuation_status', [6,9,10,11,12]]);
         }*/
        $query->andFilterWhere(['IN', 'valuation.valuation_status', [3]]);
        /* echo "<pre>";
         print_r($ids);
         die;*/
        $query->andFilterWhere(['IN', 'valuation.id', $ids]);
        $query->andFilterWhere(['like', 'reference_number', $this->reference_number])
            ->andFilterWhere(['like', 'client_reference', $this->client_reference])
            ->andFilterWhere(['like', 'signature_img', $this->signature_img])
            ->andFilterWhere(['like', 'client_name_passport', $this->client_name_passport])
            ->andFilterWhere(['like', 'payment_plan', $this->payment_plan]);

        return $dataProvider;
    }


    public function dashboard_search_all_oldd($params, $allowedId)
    {
        // echo "<pre>";
        // print_r($params);
        // die;

        $query = Valuation::find();
        $query->joinWith(['scheduleInspection','client','building','inspectProperty']);

        // add conditions that should always apply here

        if (Yii::$app->user->identity->permission_group_id==3) {
            $query->where(['valuation.created_by'=>Yii::$app->user->identity->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $Date=(explode(" - ",$this->instruction_date_btw));
        $TargetDate=(explode(" - ",$this->target_date_btw));
        $InspectionDate=(explode(" - ",$this->inspection_date_btw));

        // dd($this->instruction_date_btw);


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'buildings.community' => $this->community,
            'approval_id' => $this->approver,
            'valuation.service_officer_name' => $this->valuer,
            'no_of_owners' => $this->no_of_owners,
            //'service_officer_name' => $this->service_officer_name,
            'instruction_date' => ($this->instruction_date <> null) ? date('Y-m-d',strtotime($this->instruction_date)) : $this->instruction_date,
            'target_date' => ($this->target_date <> null) ? date('Y-m-d',strtotime($this->target_date)) : $this->target_date,
            'building_info' => $this->building_info,
            'unit_number' => $this->unit_number,
            'valuation.status' => $this->status,
            'floor_number' => $this->floor_number,
            'instruction_person' => $this->instruction_person,
            'instruction_person' => $this->instruction_person,
            'land_size' => $this->land_size,
            'purpose_of_valuation' => $this->purpose_of_valuation,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => $this->trashed,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
            'valuation.property_id' => $this->property_id,
            'schedule_inspection.inspection_date'=> ($this->inspection_date <> null) ? date('Y-m-d',strtotime($this->inspection_date)) : $this->inspection_date,
            'schedule_inspection.inspection_time'=>$this->inspection_time,
            'buildings.sub_community'=>$this->sub_community,
            'buildings.city'=>$this->city,
            'valuation_status'=>$this->valuation_status,

        ]);

        $query->andFilterWhere(['like', 'reference_number', $this->reference_number])
            ->andFilterWhere(['like', 'client_reference', $this->client_reference])
            ->andFilterWhere(['like', 'signature_img', $this->signature_img])
            ->andFilterWhere(['like', 'client_name_passport', $this->client_name_passport])
            ->andFilterWhere(['like', 'payment_plan', $this->payment_plan])
            ->andFilterWhere(['>=', 'instruction_date', $Date[0]])
            ->andFilterWhere(['<', 'instruction_date', $Date[1]])
            ->andFilterWhere(['>=', 'target_date', $TargetDate[0]])
            ->andFilterWhere(['<', 'target_date', $TargetDate[1]])
            ->andFilterWhere(['>=', 'submission_approver_date', $InspectionDate[0]])
            ->andFilterWhere(['<', 'submission_approver_date', $InspectionDate[1]]);
        /*            ->andFilterWhere(['>=', 'schedule_inspection.valuation_report_date', $InspectionDate[0]])
                    ->andFilterWhere(['<', 'schedule_inspection.valuation_report_date', $InspectionDate[1]]);*/
        if(isset($this->inspection_date) && $this->inspection_date <> null){

            $query->andFilterWhere(['<>', 'valuation_status', 9]);
            $query->andFilterWhere(['<>', 'valuation_status', 6]);
        }

        if(isset($this->revised_reason) && $this->revised_reason <> null){

            $query->andFilterWhere(['IN', 'revised_reason', [1,2]]);
        }

        if(isset($this->inspection_done_date) && $this->inspection_done_date <> null){

            $query->andFilterWhere(['like', 'inspect_property.inspection_done_date', $this->inspection_done_date]);
        }

        if(isset($this->challenge) && $this->challenge <> null){
            $startDate = date('Y-m-d') . ' 00:00:00';
            $EndDate = date('Y-m-d') . ' 23:59:59';
            $query->andFilterWhere([
                'between', Valuation::tableName().'.created_at', $startDate." 00:00:00", $EndDate." 23:59:59"
            ]);
            // $query->andFilterWhere(['like', 'valuation.created_at', $this->challenge]);
        }

        if($this->client_invoice_type<>null){
            $query->andFilterWhere(['=', Valuation::tableName().'.client_invoice_type', $this->client_invoice_type]);
        }

        if(isset($this->reminder_id) && $this->reminder_id <> null){
            $startDate = date('Y-m-d') . ' 00:00:00';
            $EndDate = date('Y-m-d') . ' 23:59:59';
            $query->innerJoin(VoneValuations::tableName(), 'valuation.id = vone_valuations.valuation_id')
                ->where(['vone_valuations.reason' => $this->reminder_id])
                ->andFilterWhere([
                    'between', VoneValuations::tableName().'.created_at', $startDate, $EndDate
                ]);

        }

        //start by usama
        if($this->time_period <> null && $this->time_period != 0 ) {

            // if($this->client_invoice_type<>null){
            //     $query->andFilterWhere(['=', Valuation::tableName().'.client_invoice_type', $this->client_invoice_type]);
            // }

            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {
                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            if($this->target == "instruction_date"){
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.instruction_date', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "today_challenged"){
                $query->andFilterWhere(['IN', 'revised_reason', [1,2]]);
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.created_at', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "today_mistakes"){
                $query->andFilterWhere(['IN', 'revised_reason', [3]]);
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.created_at', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "inspection_date"){
                $query->andFilterWhere(['<>', 'valuation_status', 9]);
                $query->andFilterWhere(['<>', 'valuation_status', 6]);
                $query->andFilterWhere([
                    'between', ScheduleInspection::tableName().'.inspection_date', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "inspection_done_date"){
                $query->andFilterWhere([
                    'between', InspectProperty::tableName().'.inspection_done_date', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "today_cancelled"){
                $query->andFilterWhere(['=', 'valuation_status', 9]);
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.instruction_date', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "today_reminders"){
                $query->innerJoin(VoneValuations::tableName(), 'valuation.id = vone_valuations.valuation_id')
                    ->where(['vone_valuations.reason' => $this->reminder_id]);
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.created_at', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "a_val_received"){
                $query->andFilterWhere(['=', 'valuation_status', 1]);
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.instruction_date', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "a_insp_req"){
                $query->andFilterWhere(['=', 'valuation_status', 2]);
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.instruction_date', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "a_val_challenged"){
                $query->andFilterWhere(['IN', 'revised_reason', [1,2]]);
                $query->andFilterWhere(['IN', 'valuation_status', [1,2,3,4,7,8]]);
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.instruction_date', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "a_val_mistakes"){
                $query->andFilterWhere(['IN', 'revised_reason', [3]]);
                $query->andFilterWhere(['IN', 'valuation_status', [1,2,3,4,7,8]]);
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.instruction_date', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "a_doc_requested"){
                $query->andFilterWhere(['=', 'valuation_status', 7]);
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.instruction_date', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "a_active_inspected"){
                $query->andFilterWhere(['=', 'valuation_status', 3]);
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.instruction_date', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            } else if($this->target == "a_pending_recommend"){
                $query->andFilterWhere(['IN', 'valuation_status', [1,2,3,4,7,8]]);
            }
        }
        //end start by usama

        $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
        if(in_array(Yii::$app->user->id, $allowedId)){
            $dataProvider->pagination->pageSize=false;
        }else{
            if ($this->pageSize<>null) {
                if ($this->pageSize==4) {
                    $dataProvider->pagination->pageSize=false;
                }else {
                    $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
                }
            }
        }

        return $dataProvider;
    }




    public function dashboard_search_all($params, $allowedId)
    {
        //new code
        if(isset(Yii::$app->request->queryParams['ValuationSearch']['time_period']) && Yii::$app->request->queryParams['ValuationSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationSearch']['custom_date_btw'];
                    $Date=(explode(" - ",$custome_date_btw));
                    $start_date = $Date[0].' 00:00:00';
                    $end_date = $Date[1].' 23:59:59';
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationSearch']['time_period']);
                $start_date = $date_array['start_date'].' 00:00:00';
                $end_date = $date_array['end_date'].' 23:59:59';
            }
        }else{
            $start_date = '2021-04-28 00:00:00';
            $end_date = date('Y-m-d').' 23:59:59';

        }
        //end new code
        $query = Valuation::find();
        $query->joinWith(['scheduleInspection','client','building','inspectProperty']);

        // add conditions that should always apply here

        if (Yii::$app->user->identity->permission_group_id==3) {
            $query->where(['valuation.created_by'=>Yii::$app->user->identity->id]);
        }

        if(isset($params['ValuationSearch']['page_title']) && $params['ValuationSearch']['page_title'] == 'Pending Inspected'){
            $query->select([
                Valuation::tableName().'.*',
            ])
                ->leftJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->where([Valuation::tableName().'.valuation_status' => 3])
                ->andWhere([Valuation::tableName().'.parent_id' => null])
                ->andWhere(['between', 'valuation.instruction_date', $start_date, $end_date])
                ->andWhere([ValuationApproversData::tableName().'.valuation_id' => null]);
        }
        if(isset($params['ValuationSearch']['page_title']) && $params['ValuationSearch']['page_title'] == 'Partial Inspected'){
            $query->select([
                Valuation::tableName().'.*',
            ])
                ->leftJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->where([Valuation::tableName().'.valuation_status' => 13])
                ->andWhere([Valuation::tableName().'.parent_id' => null])
                ->andWhere(['between', 'valuation.instruction_date', $start_date, $end_date])
                ->andWhere([ValuationApproversData::tableName().'.valuation_id' => null]);
        }

        if(isset($params['ValuationSearch']['page_title']) && $params['ValuationSearch']['page_title'] == 'Total Received'){
            $query->select([
                Valuation::tableName().'.*',
            ])
                ->where(['IN', 'valuation_status', [1,2,3,4,5,6,7,8,9,10,11,12]])
                ->andWhere(['between', 'valuation.instruction_date', $start_date, $end_date]);
        }

        if(isset($params['ValuationSearch']['page_title']) && $params['ValuationSearch']['page_title'] == 'Total Inspected'){
            $query->select([
                Valuation::tableName().'.*',
            ])
                ->where(['IS NOT', 'inspect_property.inspection_done_date', null])
                ->andWhere(['between', 'valuation.instruction_date', $start_date, $end_date]);
        }

        if(isset($params['ValuationSearch']['page_title']) && $params['ValuationSearch']['page_title'] == 'Total Approved'){
            $query->select([
                Valuation::tableName().'.*',
            ])
                ->where(['IN', 'valuation_status', [5]])
                ->andWhere(['between', 'valuation.instruction_date', $start_date, $end_date]);
        }

        if(isset($params['ValuationSearch']['page_title']) && $params['ValuationSearch']['page_title'] == 'Total Challenged'){
            $query->select([
                Valuation::tableName().'.*',
            ])
                ->where(['valuation.revised_reason' => [1, 2]])
                ->andWhere(['between', 'valuation.instruction_date', $start_date, $end_date]);
        }

        if(isset($params['ValuationSearch']['page_title']) && $params['ValuationSearch']['page_title'] == 'Total Mistakes'){
            $query->select([
                Valuation::tableName().'.*',
            ])
                ->where(['revised_reason' => [3]])
                ->andWhere(['between', 'valuation.instruction_date', $start_date, $end_date]);
        }

        if(isset($params['ValuationSearch']['page_title']) && $params['ValuationSearch']['page_title'] == 'Total Reminders'){
            
            $query->select([
                Valuation::tableName().'.*',
            ])
                ->innerJoin(VoneValuations::tableName(), 'valuation.id = vone_valuations.valuation_id')
                ->andWhere(['vone_valuations.reason' => 8])
                ->andWhere(['between', 'valuation.instruction_date', $start_date, $end_date]);
        }

        if(isset($params['ValuationSearch']['page_title']) && $params['ValuationSearch']['page_title'] == 'Total Cancelled'){
            
            $query->select([
                Valuation::tableName().'.*',
            ])
                ->where(['IN', 'valuation_status', [9]])
                ->andWhere(['between', 'valuation.instruction_date', $start_date, $end_date]);
        }

        $today_date = date("Y-m-d");
        if(isset($params['ValuationSearch']['page_title']) && $params['ValuationSearch']['page_title'] == 'Today Challenged'){
            $query->andFilterWhere(['IN', 'revised_reason', [1,2]]);
            $query->andFilterWhere([
                'between', Valuation::tableName().'.updated_at', $today_date." 00:00:00", $today_date." 23:59:59"
            ]);
        }

        if(isset($params['ValuationSearch']['page_title']) && $params['ValuationSearch']['page_title'] == 'Today Mistakes'){
            $query->andFilterWhere(['IN', 'revised_reason', [3]]);
            $query->andFilterWhere([
                'between', Valuation::tableName().'.updated_at', $today_date." 00:00:00", $today_date." 23:59:59"
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $Date=(explode(" - ",$this->instruction_date_btw));
        $TargetDate=(explode(" - ",$this->target_date_btw));
        $InspectionDate=(explode(" - ",$this->inspection_date_btw));

        // dd($this->instruction_date_btw);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'buildings.community' => $this->community,
            'approval_id' => $this->approver,
            'valuation.service_officer_name' => $this->valuer,
            'no_of_owners' => $this->no_of_owners,
            //'service_officer_name' => $this->service_officer_name,
            'instruction_date' => ($this->instruction_date <> null) ? date('Y-m-d',strtotime($this->instruction_date)) : $this->instruction_date,
            'target_date' => ($this->target_date <> null) ? date('Y-m-d',strtotime($this->target_date)) : $this->target_date,
            'building_info' => $this->building_info,
            'unit_number' => $this->unit_number,
            'valuation.status' => $this->status,
            'floor_number' => $this->floor_number,
            'instruction_person' => $this->instruction_person,
            'instruction_person' => $this->instruction_person,
            'land_size' => $this->land_size,
            'purpose_of_valuation' => $this->purpose_of_valuation,
            'created_by' => $this->created_by,
            //  'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'trashed' => $this->trashed,
            'trashed_at' => $this->trashed_at,
            'trashed_by' => $this->trashed_by,
            'valuation.property_id' => $this->property_id,
            'schedule_inspection.inspection_date'=> ($this->inspection_date <> null) ? date('Y-m-d',strtotime($this->inspection_date)) : $this->inspection_date,
            'schedule_inspection.inspection_time'=>$this->inspection_time,
            'buildings.sub_community'=>$this->sub_community,
            'buildings.city'=>$this->city,
            'valuation_status'=>$this->valuation_status,

        ]);

        $query->andFilterWhere(['like', 'reference_number', $this->reference_number])
            ->andFilterWhere(['like', 'client_reference', $this->client_reference])
            ->andFilterWhere(['like', 'signature_img', $this->signature_img])
            ->andFilterWhere(['like', 'client_name_passport', $this->client_name_passport])
            ->andFilterWhere(['like', 'payment_plan', $this->payment_plan])
            ->andFilterWhere(['>=', 'instruction_date', $Date[0]])
            ->andFilterWhere(['<', 'instruction_date', $Date[1]])
            ->andFilterWhere(['>=', 'target_date', $TargetDate[0]])
            ->andFilterWhere(['<', 'target_date', $TargetDate[1]])
            ->andFilterWhere(['>=', 'submission_approver_date', $InspectionDate[0]])
            ->andFilterWhere(['<', 'submission_approver_date', $InspectionDate[1]]);
        /*            ->andFilterWhere(['>=', 'schedule_inspection.valuation_report_date', $InspectionDate[0]])
                    ->andFilterWhere(['<', 'schedule_inspection.valuation_report_date', $InspectionDate[1]]);*/
        if(isset($this->inspection_date) && $this->inspection_date <> null){
            $query->andFilterWhere(['<>', 'valuation_status', 9]);
            $query->andFilterWhere(['<>', 'valuation_status', 6]);
        }

        // if(isset($this->revised_reason) && $this->revised_reason <> null){
        //     dd($this->revised_reason);
        //     $query->andFilterWhere(['IN', 'revised_reason', $this->revised_reason]);
        // }

        if(isset($this->inspection_done_date) && $this->inspection_done_date <> null){
            $query->andFilterWhere(['like', 'inspect_property.inspection_done_date', $this->inspection_done_date]);
        }

        if(isset($this->challenge) && $this->challenge <> null){
            $query->andFilterWhere(['like', 'valuation.created_at', $this->challenge]);
        }
        if(isset($this->created_at) && $this->created_at <> null && $this->parent_target != "today_amendment_overview"){
            $query->andWhere(['parent_id' => null]);
            $query->andWhere(['not', ['client_id' => 9166]]);
            $query->andFilterWhere(['like', 'valuation.created_at', $this->created_at]);
        }

        if($this->parent_target == "today_amendment_overview"){
            $query->andWhere(['not', ['parent_id' => null]]);
            $query->orWhere(['client_id' => 9166]);
            $query->andFilterWhere(['like', 'valuation.created_at', $this->created_at]);
        }

        if($this->client_invoice_type<>null){
            $query->andFilterWhere(['=', Valuation::tableName().'.client_invoice_type', $this->client_invoice_type]);
        }

        if(isset($this->reminder_id) && $this->target == 'today_reminders'){
            $query->innerJoin(VoneValuations::tableName(), 'valuation.id = vone_valuations.valuation_id')
                ->andWhere(['vone_valuations.reason' => $this->reminder_id[0]]);
            // $query->andWhere(['vone_valuations.error_status' => 'pending']);
            $query->andFilterWhere(['like', 'vone_valuations.created_at', date("Y-m-d")]);
        }
        if(isset($this->reminder_id) && $this->target == 'pending_reminders'){
            $query->innerJoin(VoneValuations::tableName(), 'valuation.id = vone_valuations.valuation_id')
                ->andWhere(['vone_valuations.reason' => $this->reminder_id[0]])
                ->andWhere(['vone_valuations.error_status' => 'pending']);
        }
        // if(isset($this->reminder_id) && $this->target == 'total_reminders'){
        //     $query->innerJoin(VoneValuations::tableName(), 'valuation.id = vone_valuations.valuation_id')
        //         ->andWhere(['vone_valuations.reason' => $this->reminder_id[0]]);
        // }

        if(isset($this->today_doc) && $this->today_doc == 'today_doc'){
            $today_start_date = date('Y-m-d') . ' 00:00:00';
            $today_end_date = date('Y-m-d') . ' 23:59:59';
            $query->innerJoin(ReceivedDocs::tableName(), 'valuation.id = received_docs.valuation_id')
                ->andWhere(['valuation.parent_id' => null])
                ->andWhere(['valuation.valuation_status' => 7])
                ->andFilterWhere(['between', 'received_docs.created_at', $today_start_date, $today_end_date]);

        }

        if(isset(Yii::$app->request->queryParams['ValuationSearch']['time_period']) && Yii::$app->request->queryParams['ValuationSearch']['time_period'] >= 0 && Yii::$app->request->queryParams['ValuationSearch']['page_title'] == "Pending Documents Requested" )
        {
            $query->innerJoin(Properties::tableName(), 'valuation.property_id = properties.id')
            ->where(['valuation.valuation_status' => 7])
            ->andWhere(['between', 'valuation.created_at', $start_date, $end_date]);
        }

        if(isset($this->today_doc) && $this->today_doc == 'pending_doc'){
            $query->innerJoin(Properties::tableName(), 'valuation.property_id = properties.id');
            // $query->andFilterWhere(['like', 'valuation.updated_at', '%'.date('Y-m-d').'%', false]);
        }

        //start by usama
        if($this->time_period <> null && $this->time_period != 0 ) {

            // if($this->client_invoice_type<>null){
            //     $query->andFilterWhere(['=', Valuation::tableName().'.client_invoice_type', $this->client_invoice_type]);
            // }
            if($this->time_period == 9){
                if($this->custom_date_btw <> null) {
                    $Date=(explode(" - ",$this->custom_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($this->time_period);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }
            if($this->target == "instruction_date"){
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.instruction_date', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "today_challenged"){
                $query->andFilterWhere(['IN', 'revised_reason', [1,2]]);
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.created_at', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "today_reminders"){
                $query->innerJoin(VoneValuations::tableName(), 'valuation.id = vone_valuations.valuation_id')
                    ->where(['vone_valuations.reason' => $this->reminder_id]);
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.created_at', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "today_mistakes"){
                $query->andFilterWhere(['IN', 'revised_reason', [3]]);
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.created_at', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "inspection_date"){
                $query->andFilterWhere(['<>', 'valuation_status', 9]);
                $query->andFilterWhere(['<>', 'valuation_status', 6]);
                $query->andFilterWhere([
                    'between', ScheduleInspection::tableName().'.inspection_date', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "inspection_done_date"){
                $query->andFilterWhere([
                    'between', InspectProperty::tableName().'.inspection_done_date', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "today_cancelled"){
                $query->andFilterWhere(['=', 'valuation_status', 9]);
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.instruction_date', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "a_val_received"){
                $query->andFilterWhere(['=', 'valuation_status', 1]);
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.instruction_date', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "a_insp_req"){
                $query->andFilterWhere(['=', 'valuation_status', 2]);
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.instruction_date', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "a_val_challenged"){
                $query->andFilterWhere(['IN', 'revised_reason', [1,2]]);
                $query->andFilterWhere(['IN', 'valuation_status', [1,2,3,4,7,8]]);
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.instruction_date', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "a_val_mistakes"){
                $query->andFilterWhere(['IN', 'revised_reason', [3]]);
                $query->andFilterWhere(['IN', 'valuation_status', [1,2,3,4,7,8]]);
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.instruction_date', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "a_doc_requested"){
                $query->andFilterWhere(['=', 'valuation_status', 7]);
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.instruction_date', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            }
            else if($this->target == "a_active_inspected"){
                $query->andFilterWhere(['=', 'valuation_status', 3]);
                $query->andFilterWhere([
                    'between', Valuation::tableName().'.instruction_date', $from_date." 00:00:00", $to_date." 23:59:59"
                ]);
            } else if($this->target == "a_pending_recommend"){
                $query->andFilterWhere(['IN', 'valuation_status', [1,2,3,4,7,8]]);
            }
        }
        //end start by usama

        $pagelist=[1=>'20',2=>'50',3=>'100',4=>'Show All'];
        if(in_array(Yii::$app->user->id, $allowedId)){
            $dataProvider->pagination->pageSize=false;
        }else{
            if ($this->pageSize<>null) {
                if ($this->pageSize==4) {
                    $dataProvider->pagination->pageSize=false;
                }else {
                    $dataProvider->pagination->pageSize=$pagelist[$this->pageSize];
                }
            }
        }

        return $dataProvider;
    }


}
