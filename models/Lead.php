<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
* This is the model class for table "{{%opportunity}}".
*
* @property integer $id
* @property string $reference_no
* @property string $rec_type
* @property string $title
* @property integer $source
* @property integer $service_type
* @property string $module_type
* @property integer $module_id
* @property string $module_keyword
* @property string $expected_close_date
* @property integer $quote_amount
* @property string $descp
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
* @property integer $trashed
* @property string $deleted_at
* @property integer $deleted_by
*/
class Lead extends Opportunity
{
	public $manager_id,$tags,$input_field;
	public $map_location,$map_lat,$map_lng;
  public $map_lat_tmp,$map_lng_tmp;
  public $map_location_tmp,$maploc_lat_tmp,$maploc_lng_tmp;
	public $workflow_id,$workflow_stage_id;

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['title','module_type','module_keyword'],'required'],
			[['expected_close_date','created_at','updated_at','deleted_at'],'safe'],
			[[
				'source','service_type','module_id','workflow_id','workflow_stage_id','created_by','updated_by','deleted_by'
			],'integer'],
			[[
				'title','module_type','module_keyword','expected_close_date','descp','quote_amount'
			],'string'],
      [['title'],'trim'],
      [['manager_id'],'each','rule'=>['integer']],
      ['input_field','checkRequiredInputs'],
		];
	}

  /**
   * Validates the required inputs.
   * This method serves as the inline validation for required inputs.
   *
   * @param string $attribute the attribute currently being validated
   * @param array $params the additional name-value pairs given in the rule
   */
  public function checkRequiredInputs()
  {
    if (!$this->hasErrors()) {
			return Yii::$app->inputHelperFunctions->validateRequired($this);
    }
  }

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Title'),
			'source' => Yii::t('app', 'Source'),
			'service_type' => Yii::t('app', 'Service'),
			'module_type' => Yii::t('app', 'Module'),
			'module_keyword' => Yii::t('app', 'Select'),
			'sales_stage' => Yii::t('app', 'Sales Stage'),
			'expected_close_date' => Yii::t('app', 'Expected Close Date'),
			'quote_amount' => Yii::t('app', 'Quote Amount'),
			'descp' => Yii::t('app', 'Description'),
			'workflow_id' => Yii::t('app', 'Process'),
			'workflow_stage_id' => Yii::t('app', 'Process Stage'),
			'manager_id' => Yii::t('app', 'Assign To'),
			'created_at' => Yii::t('app', 'Created At'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'updated_by' => Yii::t('app', 'Updated By'),
			'deleted_at' => Yii::t('app', 'Trashed At'),
			'deleted_by' => Yii::t('app', 'Trashed By'),
		];
	}

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecTitle()
  {
    return $this->title;
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecType()
  {
    return $this->rec_type=='o' ? 'Opportunity' : 'Lead';
  }

	/**
	* returns type of module for process and events
	*/
	public function getModuleTypeId()
	{
		return 'opportunity';
	}

	/**
	* Get Assigned Staff members
	* @return \yii\db\ActiveQuery
	*/
	public function getManagerIdz()
	{
		return ModuleManager::find()->select(['staff_id'])->where(['module_type'=>$this->moduleTypeId,'module_id'=>$this->id])->asArray()->all();
	}

	/**
	* @return \yii\db\ActiveQuery
	*/
  public function getTagsListArray()
  {
		$subQueryModuleTags=ModuleTag::find()->select(['tag_id'])->where(['module_type'=>$this->moduleTypeId,'module_id'=>$this->id]);
		$acRows=Tags::find()->select(['title'])->where(['id'=>$subQueryModuleTags])->asArray()->all();
		$tagList=[];
		foreach($acRows as $acRow){
			$tagList[]=$acRow['title'];
		}
    return $tagList;
  }

	/**
	* @return \yii\db\ActiveQuery
	*/
	public function getServiceType()
	{
		return $this->hasOne(PredefinedList::className(), ['id' => 'service_type']);
	}

  /**
  * @inheritdoc
  * Generates auth_key if it is a new user
  */
  public function beforeSave($insert)
  {
    if (parent::beforeSave($insert)) {
      $this->quote_amount = str_replace(",","",$this->quote_amount);
      return true;
    }
    return false;
  }

	/**
	* @inheritdoc
	*/
	public function afterSave($insert, $changedAttributes)
	{
		if($this->rec_type=='o')$pre='OP';
		if($this->rec_type=='l')$pre='LD';

		$reference=$pre.'-'.date('y').'-'.sprintf('%03d', $this->id);

		$connection = \Yii::$app->db;
		$connection->createCommand(
			"update ".self::tableName()." set reference_no=:reference_no where id=:id",
			[':reference_no'=>$reference,':id'=>$this->id]
			)->execute();

		//Saving Custom Fields
		Yii::$app->inputHelperFunctions->saveCustomField($this);

		//Saving Tags
		Yii::$app->inputHelperFunctions->saveTags($this);

		//Saving Managers
		Yii::$app->inputHelperFunctions->saveManagers($this);

		//Saving Workflow
		Yii::$app->workflowHelperFunctions->saveWorkFlow($this);

		parent::afterSave($insert, $changedAttributes);
	}
}
