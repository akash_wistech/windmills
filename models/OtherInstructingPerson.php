<?php

namespace app\models;
use Yii;

class OtherInstructingPerson extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'other_instructing_person';
    }

    public function rules()
    {
        return [
            [['client_id'], 'integer'],
            [['detail'], 'safe'],
        ];
    }

}
