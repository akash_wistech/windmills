<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%client_provided_rents}}`.
 */
class m220816_074135_create_client_provided_rents_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%client_provided_rents}}', [
            'id' => $this->primaryKey(),

            'valuation_id' => $this->integer()->null(),
            'income_type' => $this->integer()->null(),
            'unit_number' => $this->integer()->null(),
            'nla' => $this->string()->null(),
            'contract_date' => $this->dateTime()->null(),
            'contract_end_date' => $this->dateTime()->null(),
            'rent' => $this->string()->null(),
            'rent_sqf' => $this->string()->null(),
            'service_charges_unit' => $this->string()->null(),
            'status' => $this->integer()->null(),
            'created_at' => $this->dateTime()->null(),
            'updated_at' => $this->dateTime()->null(),
            'deleted_at' => $this->dateTime()->null(),
            'created_by' => $this->integer()->null(),
            'updated_by' => $this->integer()->null(),
            'deleted_by' => $this->integer()->null(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%client_provided_rents}}');
    }
}
