-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 10, 2020 at 08:01 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mastertajzi_windmillscrm`
--

-- --------------------------------------------------------

--
-- Table structure for table `weightages`
--

CREATE TABLE `weightages` (
  `id` int(11) NOT NULL,
  `property_type` int(11) NOT NULL,
  `emirates` int(11) NOT NULL,
  `location` decimal(10,2) NOT NULL,
  `age` decimal(10,2) NOT NULL,
  `property_exposure` decimal(10,2) NOT NULL,
  `property_placement` decimal(10,2) NOT NULL,
  `finishing_status` decimal(10,2) NOT NULL,
  `bedrooom` decimal(10,2) NOT NULL,
  `view` decimal(10,2) NOT NULL,
  `quality` decimal(10,2) NOT NULL,
  `floor` decimal(10,2) NOT NULL,
  `land_size` decimal(10,2) NOT NULL,
  `bua` decimal(10,2) NOT NULL,
  `balcony_size` decimal(10,2) NOT NULL,
  `furnished` decimal(10,2) NOT NULL,
  `upgrades` decimal(10,2) NOT NULL,
  `parking` decimal(10,2) NOT NULL,
  `pool` decimal(10,2) NOT NULL,
  `landscape` decimal(10,2) NOT NULL,
  `white_goods` decimal(10,2) NOT NULL,
  `utilities_connected` decimal(10,2) NOT NULL,
  `tenure` decimal(10,2) NOT NULL,
  `number_of_levels` decimal(10,2) NOT NULL,
  `property_visibility` decimal(10,2) NOT NULL,
  `less_than_1_month` decimal(10,2) NOT NULL,
  `less_than_2_month` decimal(10,2) NOT NULL,
  `less_than_3_month` decimal(10,2) NOT NULL,
  `less_than_4_month` decimal(10,2) NOT NULL,
  `less_than_5_month` decimal(10,2) NOT NULL,
  `less_than_6_month` decimal(10,2) NOT NULL,
  `less_than_7_month` decimal(10,2) NOT NULL,
  `less_than_8_month` decimal(10,2) NOT NULL,
  `less_than_9_month` decimal(10,2) NOT NULL,
  `less_than_10_month` decimal(10,2) NOT NULL,
  `less_than_11_month` decimal(10,2) NOT NULL,
  `less_than_12_month` decimal(10,2) NOT NULL,
  `less_than_2_year` decimal(10,2) NOT NULL,
  `less_than_3_year` decimal(10,2) NOT NULL,
  `less_than_4_year` decimal(10,2) NOT NULL,
  `less_than_5_year` decimal(10,2) NOT NULL,
  `less_than_6_year` decimal(10,2) NOT NULL,
  `less_than_7_year` decimal(10,2) NOT NULL,
  `less_than_8_year` decimal(10,2) NOT NULL,
  `less_than_9_year` decimal(10,2) NOT NULL,
  `less_than_10_year` decimal(10,2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `trashed_at` datetime DEFAULT NULL,
  `trashed` tinyint(1) DEFAULT 0,
  `trashed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `weightages`
--

INSERT INTO `weightages` (`id`, `property_type`, `emirates`, `location`, `age`, `property_exposure`, `property_placement`, `finishing_status`, `bedrooom`, `view`, `quality`, `floor`, `land_size`, `bua`, `balcony_size`, `furnished`, `upgrades`, `parking`, `pool`, `landscape`, `white_goods`, `utilities_connected`, `tenure`, `number_of_levels`, `property_visibility`, `less_than_1_month`, `less_than_2_month`, `less_than_3_month`, `less_than_4_month`, `less_than_5_month`, `less_than_6_month`, `less_than_7_month`, `less_than_8_month`, `less_than_9_month`, `less_than_10_month`, `less_than_11_month`, `less_than_12_month`, `less_than_2_year`, `less_than_3_year`, `less_than_4_year`, `less_than_5_year`, `less_than_6_year`, `less_than_7_year`, `less_than_8_year`, `less_than_9_year`, `less_than_10_year`, `created_at`, `updated_at`, `created_by`, `updated_by`, `trashed_at`, `trashed`, `trashed_by`) VALUES
(1, 2, 3510, '2.00', '21.00', '2.00', '2.00', '3.00', '3.00', '23.00', '3.00', '3.00', '4.00', '4.00', '23.00', '23.00', '3.00', '3.00', '4.00', '3.00', '4.00', '4.00', '3.00', '3.00', '3.00', '2.00', '2.00', '12.00', '6.00', '7.00', '9.00', '9.00', '2.00', '2.00', '4.00', '5.00', '4.00', '3.00', '2.00', '3.00', '3.00', '2.00', '3.00', '2.00', '3.00', '4.00', NULL, NULL, NULL, NULL, NULL, 0, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `weightages`
--
ALTER TABLE `weightages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `weightages`
--
ALTER TABLE `weightages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
