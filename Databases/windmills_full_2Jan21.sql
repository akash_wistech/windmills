-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jan 02, 2021 at 06:04 PM
-- Server version: 5.7.26
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yii2crm`
--

-- --------------------------------------------------------

--
-- Table structure for table `action_log`
--

CREATE TABLE `action_log` (
  `id` bigint(20) NOT NULL,
  `module_type` char(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module_id` bigint(20) NOT NULL,
  `rec_type` char(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comments` text COLLATE utf8mb4_unicode_ci,
  `priority` tinyint(1) DEFAULT '1',
  `visibility` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT NULL,
  `trashed` tinyint(1) DEFAULT '0',
  `trashed_at` datetime DEFAULT NULL,
  `trashed_by` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `action_log_calendar`
--

CREATE TABLE `action_log_calendar` (
  `action_log_id` bigint(20) NOT NULL DEFAULT '0',
  `calendar_id` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `action_log_event_info`
--

CREATE TABLE `action_log_event_info` (
  `action_log_id` bigint(20) NOT NULL DEFAULT '0',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `color_code` char(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `all_day` tinyint(1) DEFAULT '0',
  `sub_type` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `action_log_manager`
--

CREATE TABLE `action_log_manager` (
  `action_log_id` bigint(20) NOT NULL DEFAULT '0',
  `staff_id` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `action_log_reminder`
--

CREATE TABLE `action_log_reminder` (
  `action_log_id` bigint(20) NOT NULL,
  `notify_to` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notify_time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `action_log_subject`
--

CREATE TABLE `action_log_subject` (
  `action_log_id` bigint(20) NOT NULL DEFAULT '0',
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `due_date` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `completed_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `action_log_time`
--

CREATE TABLE `action_log_time` (
  `action_log_id` bigint(20) NOT NULL DEFAULT '0',
  `start_dt` datetime NOT NULL,
  `end_dt` datetime DEFAULT NULL,
  `hours` int(11) DEFAULT '0',
  `minutes` char(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` bigint(20) NOT NULL,
  `controller_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `user_ip` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `page_referrer` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_group`
--

CREATE TABLE `admin_group` (
  `id` int(11) NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `trashed` tinyint(1) DEFAULT '0',
  `trashed_at` datetime DEFAULT NULL,
  `trashed_by` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_group_permissions`
--

CREATE TABLE `admin_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin_group_permission_type`
--

CREATE TABLE `admin_group_permission_type` (
  `group_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `controller_id` varchar(50) NOT NULL,
  `list_type` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu`
--

CREATE TABLE `admin_menu` (
  `id` int(11) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `icon` varchar(35) NOT NULL,
  `title` varchar(50) NOT NULL,
  `controller_id` varchar(50) NOT NULL,
  `action_id` varchar(50) NOT NULL,
  `param1` varchar(15) NOT NULL,
  `value1` varchar(15) NOT NULL,
  `active_condition` text,
  `show_in_menu` tinyint(1) NOT NULL DEFAULT '0',
  `module_id` varchar(32) DEFAULT NULL,
  `ask_list_type` tinyint(1) DEFAULT '0',
  `rank` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `buaweightage`
--

CREATE TABLE `buaweightage` (
  `id` int(11) NOT NULL,
  `difference` decimal(10,2) NOT NULL,
  `bigger_sp` decimal(10,2) NOT NULL,
  `smaller_sp` decimal(10,2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `trashed` tinyint(1) DEFAULT '0',
  `trashed_at` datetime DEFAULT NULL,
  `trashed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `calendar`
--

CREATE TABLE `calendar` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `trashed` tinyint(1) DEFAULT '0',
  `trashed_at` datetime DEFAULT NULL,
  `trashed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `communities`
--

CREATE TABLE `communities` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `trashed` tinyint(1) DEFAULT '0',
  `trashed_at` datetime DEFAULT NULL,
  `trashed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` bigint(20) NOT NULL,
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent` bigint(20) DEFAULT '0',
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT '0',
  `zone_id` int(11) DEFAULT '0',
  `city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_code` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `descp` text COLLATE utf8mb4_unicode_ci,
  `start_date` date DEFAULT NULL,
  `lead_type` tinyint(1) DEFAULT '0',
  `lead_source` tinyint(1) DEFAULT '0',
  `lead_status` tinyint(1) DEFAULT '0',
  `lead_date` date DEFAULT NULL,
  `lead_score` tinyint(1) DEFAULT '0',
  `deal_status` tinyint(1) DEFAULT '0',
  `interest` text COLLATE utf8mb4_unicode_ci,
  `deal_value` decimal(10,2) DEFAULT '0.00',
  `expected_close_date` date DEFAULT NULL,
  `close_date` date DEFAULT NULL,
  `confidence` tinyint(1) DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `trashed` tinyint(1) DEFAULT '0',
  `trashed_at` datetime DEFAULT NULL,
  `trashed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `company_fee_structure`
--

CREATE TABLE `company_fee_structure` (
  `company_id` bigint(20) NOT NULL,
  `emirate_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `fee` decimal(10,2) DEFAULT NULL,
  `tat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `company_manager`
--

CREATE TABLE `company_manager` (
  `company_id` bigint(20) NOT NULL DEFAULT '0',
  `staff_id` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contact_manager`
--

CREATE TABLE `contact_manager` (
  `contact_id` bigint(20) NOT NULL DEFAULT '0',
  `staff_id` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `slug` varchar(75) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `iso_code_2` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `iso_code_3` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `phone_code` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_format` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_format` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_format` text COLLATE utf8_unicode_ci NOT NULL,
  `postcode_required` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT '0',
  `trashed` tinyint(1) DEFAULT '0',
  `trashed_at` datetime DEFAULT NULL,
  `trashed_by` bigint(20) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `trashed` tinyint(1) DEFAULT '0',
  `trashed_at` datetime DEFAULT NULL,
  `trashed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `developments`
--

CREATE TABLE `developments` (
  `id` int(11) NOT NULL,
  `emirates` int(11) NOT NULL,
  `property_category` int(11) NOT NULL,
  `property_type` int(11) NOT NULL,
  `star1` int(11) NOT NULL,
  `star2` int(11) NOT NULL,
  `star3` int(11) NOT NULL,
  `star4` int(11) NOT NULL,
  `star5` int(11) NOT NULL,
  `professional_charges` decimal(10,2) NOT NULL,
  `contingency_margin` decimal(10,2) NOT NULL,
  `obsolescence` decimal(10,2) NOT NULL,
  `developer_profit` decimal(10,2) NOT NULL,
  `parking_price` int(11) NOT NULL,
  `pool_price` int(11) NOT NULL,
  `landscape_price` int(11) NOT NULL,
  `whitegoods_price` int(11) NOT NULL,
  `utilities_connected_price` int(11) NOT NULL,
  `developer_margin` decimal(10,2) NOT NULL,
  `interest_rate` decimal(10,2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `trashed_at` datetime DEFAULT NULL,
  `trashed` tinyint(1) DEFAULT '0',
  `trashed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `job_title`
--

CREATE TABLE `job_title` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `trashed` tinyint(1) DEFAULT '0',
  `trashed_at` datetime DEFAULT NULL,
  `trashed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lead`
--

CREATE TABLE `lead` (
  `id` bigint(20) NOT NULL,
  `rec_type` tinyint(1) DEFAULT '1',
  `lead_id` bigint(20) DEFAULT '0',
  `firstname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sales_stage` tinyint(1) DEFAULT '0',
  `company_id` bigint(20) DEFAULT '0',
  `lead_source` tinyint(1) DEFAULT '0',
  `expected_close_date` date DEFAULT NULL,
  `quote_amount` decimal(10,2) DEFAULT '0.00',
  `probability` decimal(10,2) DEFAULT '0.00',
  `descp` text COLLATE utf8mb4_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT '0',
  `trashed` tinyint(1) DEFAULT '0',
  `trashed_at` datetime DEFAULT NULL,
  `trashed_by` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lead_manager`
--

CREATE TABLE `lead_manager` (
  `lead_id` bigint(20) NOT NULL DEFAULT '0',
  `staff_id` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` int(11) NOT NULL,
  `valuation_approach` int(11) NOT NULL,
  `approach_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `complete_years` int(11) DEFAULT NULL,
  `required_documents` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `trashed` tinyint(1) DEFAULT '0',
  `trashed_at` datetime DEFAULT NULL,
  `trashed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sale_discount`
--

CREATE TABLE `sale_discount` (
  `id` int(11) NOT NULL,
  `upto_1_million` decimal(10,2) NOT NULL,
  `upto_2_million` decimal(10,2) NOT NULL,
  `upto_3_million` decimal(10,2) NOT NULL,
  `upto_4_million` decimal(10,2) NOT NULL,
  `upto_5_million` decimal(10,2) NOT NULL,
  `upto_20_million` decimal(10,2) NOT NULL,
  `upto_50_million` decimal(10,2) NOT NULL,
  `upto_100_million` decimal(10,2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `trashed` tinyint(1) DEFAULT '0',
  `trashed_at` datetime DEFAULT NULL,
  `trashed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `config_name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `config_value` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `social_media`
--

CREATE TABLE `social_media` (
  `id` int(11) NOT NULL,
  `slug` varchar(75) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` char(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fa_class` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rank` int(11) DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT '0',
  `trashed` tinyint(1) DEFAULT '0',
  `trashed_at` datetime DEFAULT NULL,
  `trashed_by` bigint(20) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `user_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=>Contact,10=>Staff, 20=>Super Admin',
  `company_id` bigint(20) DEFAULT '0',
  `slug` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission_group_id` int(11) NOT NULL DEFAULT '0',
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `auth_key` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verify_token` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `verified` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT '0',
  `trashed` tinyint(1) DEFAULT '0',
  `trashed_at` datetime DEFAULT NULL,
  `trashed_by` bigint(20) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_login_history`
--

CREATE TABLE `user_login_history` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `ip` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `http_referrer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `login` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_profile_info`
--

CREATE TABLE `user_profile_info` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `primary_contact` tinyint(1) NOT NULL DEFAULT '0',
  `job_title_id` int(11) DEFAULT '0',
  `department_id` int(11) DEFAULT '0',
  `mobile` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone1` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone2` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT '0',
  `zone_id` int(11) DEFAULT '0',
  `city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_code` char(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `background_info` text COLLATE utf8mb4_unicode_ci,
  `lead_type` tinyint(1) DEFAULT '0',
  `lead_source` tinyint(1) DEFAULT '0',
  `lead_date` date DEFAULT NULL,
  `lead_score` tinyint(1) DEFAULT '0',
  `expected_close_date` date DEFAULT NULL,
  `close_date` date DEFAULT NULL,
  `confidence` tinyint(1) DEFAULT '0',
  `deal_status` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_reset`
--

CREATE TABLE `user_reset` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `password_reset_token` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_social_media`
--

CREATE TABLE `user_social_media` (
  `user_id` bigint(20) NOT NULL,
  `social_media_id` int(11) NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `weightages`
--

CREATE TABLE `weightages` (
  `id` int(11) NOT NULL,
  `property_type` int(11) NOT NULL,
  `emirates` int(11) NOT NULL,
  `location` decimal(10,2) NOT NULL,
  `age` decimal(10,2) NOT NULL,
  `property_exposure` decimal(10,2) NOT NULL,
  `property_placement` decimal(10,2) NOT NULL,
  `finishing_status` decimal(10,2) NOT NULL,
  `bedrooom` decimal(10,2) NOT NULL,
  `view` decimal(10,2) NOT NULL,
  `quality` decimal(10,2) NOT NULL,
  `floor` decimal(10,2) NOT NULL,
  `land_size` decimal(10,2) NOT NULL,
  `bua` decimal(10,2) NOT NULL,
  `balcony_size` decimal(10,2) NOT NULL,
  `furnished` decimal(10,2) NOT NULL,
  `upgrades` decimal(10,2) NOT NULL,
  `parking` decimal(10,2) NOT NULL,
  `pool` decimal(10,2) NOT NULL,
  `landscape` decimal(10,2) NOT NULL,
  `white_goods` decimal(10,2) NOT NULL,
  `utilities_connected` decimal(10,2) NOT NULL,
  `tenure` decimal(10,2) NOT NULL,
  `number_of_levels` decimal(10,2) NOT NULL,
  `property_visibility` decimal(10,2) NOT NULL,
  `less_than_1_month` decimal(10,2) NOT NULL,
  `less_than_2_month` decimal(10,2) NOT NULL,
  `less_than_3_month` decimal(10,2) NOT NULL,
  `less_than_4_month` decimal(10,2) NOT NULL,
  `less_than_5_month` decimal(10,2) NOT NULL,
  `less_than_6_month` decimal(10,2) NOT NULL,
  `less_than_7_month` decimal(10,2) NOT NULL,
  `less_than_8_month` decimal(10,2) NOT NULL,
  `less_than_9_month` decimal(10,2) NOT NULL,
  `less_than_10_month` decimal(10,2) NOT NULL,
  `less_than_11_month` decimal(10,2) NOT NULL,
  `less_than_12_month` decimal(10,2) NOT NULL,
  `less_than_2_year` decimal(10,2) NOT NULL,
  `less_than_3_year` decimal(10,2) NOT NULL,
  `less_than_4_year` decimal(10,2) NOT NULL,
  `less_than_5_year` decimal(10,2) NOT NULL,
  `less_than_6_year` decimal(10,2) NOT NULL,
  `less_than_7_year` decimal(10,2) NOT NULL,
  `less_than_8_year` decimal(10,2) NOT NULL,
  `less_than_9_year` decimal(10,2) NOT NULL,
  `less_than_10_year` decimal(10,2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `trashed_at` datetime DEFAULT NULL,
  `trashed` tinyint(1) DEFAULT '0',
  `trashed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `zone`
--

CREATE TABLE `zone` (
  `id` int(11) NOT NULL,
  `slug` varchar(75) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `created_by` bigint(20) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` bigint(20) DEFAULT '0',
  `trashed` tinyint(1) DEFAULT '0',
  `trashed_at` datetime DEFAULT NULL,
  `trashed_by` bigint(20) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `action_log`
--
ALTER TABLE `action_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `action_log_calendar`
--
ALTER TABLE `action_log_calendar`
  ADD KEY `action_log_id` (`action_log_id`),
  ADD KEY `calendar_id` (`calendar_id`);

--
-- Indexes for table `action_log_event_info`
--
ALTER TABLE `action_log_event_info`
  ADD KEY `action_log_id` (`action_log_id`);

--
-- Indexes for table `action_log_manager`
--
ALTER TABLE `action_log_manager`
  ADD KEY `action_log_id` (`action_log_id`),
  ADD KEY `staff_id` (`staff_id`);

--
-- Indexes for table `action_log_reminder`
--
ALTER TABLE `action_log_reminder`
  ADD KEY `action_log_id` (`action_log_id`),
  ADD KEY `notify_to` (`notify_to`),
  ADD KEY `notify_time` (`notify_time`);

--
-- Indexes for table `action_log_subject`
--
ALTER TABLE `action_log_subject`
  ADD KEY `action_log_id` (`action_log_id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `action_log_time`
--
ALTER TABLE `action_log_time`
  ADD KEY `action_log_id` (`action_log_id`);

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_group`
--
ALTER TABLE `admin_group`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `admin_group_permissions`
--
ALTER TABLE `admin_group_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_group_permission_type`
--
ALTER TABLE `admin_group_permission_type`
  ADD KEY `group_id` (`group_id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `controller_id` (`controller_id`);

--
-- Indexes for table `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buaweightage`
--
ALTER TABLE `buaweightage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `calendar`
--
ALTER TABLE `calendar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trashed` (`trashed`);

--
-- Indexes for table `communities`
--
ALTER TABLE `communities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`),
  ADD KEY `status` (`status`),
  ADD KEY `trashed` (`trashed`);

--
-- Indexes for table `company_manager`
--
ALTER TABLE `company_manager`
  ADD KEY `company_id` (`company_id`),
  ADD KEY `staff_id` (`staff_id`);

--
-- Indexes for table `contact_manager`
--
ALTER TABLE `contact_manager`
  ADD KEY `contact_id` (`contact_id`),
  ADD KEY `staff_id` (`staff_id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trashed` (`trashed`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `developments`
--
ALTER TABLE `developments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_title`
--
ALTER TABLE `job_title`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `lead`
--
ALTER TABLE `lead`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rec_type` (`rec_type`),
  ADD KEY `lead_id` (`lead_id`);

--
-- Indexes for table `lead_manager`
--
ALTER TABLE `lead_manager`
  ADD KEY `lead_id` (`lead_id`),
  ADD KEY `staff_id` (`staff_id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `sale_discount`
--
ALTER TABLE `sale_discount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `config_name` (`config_name`);

--
-- Indexes for table `social_media`
--
ALTER TABLE `social_media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trashed` (`trashed`),
  ADD KEY `status` (`status`),
  ADD KEY `rank` (`rank`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_type` (`user_type`),
  ADD KEY `status` (`status`),
  ADD KEY `verified` (`verified`),
  ADD KEY `created_at` (`created_at`),
  ADD KEY `trashed` (`trashed`);

--
-- Indexes for table `user_login_history`
--
ALTER TABLE `user_login_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user_profile_info`
--
ALTER TABLE `user_profile_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `zone_id` (`zone_id`),
  ADD KEY `job_title_id` (`job_title_id`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `primary_contact` (`primary_contact`);

--
-- Indexes for table `user_reset`
--
ALTER TABLE `user_reset`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`),
  ADD KEY `created_at` (`created_at`),
  ADD KEY `updated_at` (`updated_at`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user_social_media`
--
ALTER TABLE `user_social_media`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `social_media_id` (`social_media_id`);

--
-- Indexes for table `weightages`
--
ALTER TABLE `weightages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zone`
--
ALTER TABLE `zone`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `trashed` (`trashed`),
  ADD KEY `status` (`status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `action_log`
--
ALTER TABLE `action_log`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_group`
--
ALTER TABLE `admin_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_group_permissions`
--
ALTER TABLE `admin_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `buaweightage`
--
ALTER TABLE `buaweightage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `calendar`
--
ALTER TABLE `calendar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `communities`
--
ALTER TABLE `communities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `developments`
--
ALTER TABLE `developments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `job_title`
--
ALTER TABLE `job_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `lead`
--
ALTER TABLE `lead`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sale_discount`
--
ALTER TABLE `sale_discount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `social_media`
--
ALTER TABLE `social_media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_login_history`
--
ALTER TABLE `user_login_history`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_profile_info`
--
ALTER TABLE `user_profile_info`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_reset`
--
ALTER TABLE `user_reset`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `weightages`
--
ALTER TABLE `weightages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `zone`
--
ALTER TABLE `zone`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
