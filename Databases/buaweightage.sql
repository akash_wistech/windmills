-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 10, 2020 at 02:03 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mastertajzi_windmillscrm`
--

-- --------------------------------------------------------

--
-- Table structure for table `buaweightage`
--

CREATE TABLE `buaweightage` (
  `id` int(11) NOT NULL,
  `difference` decimal(10,2) NOT NULL,
  `bigger_sp` decimal(10,2) NOT NULL,
  `smaller_sp` decimal(10,2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `trashed` tinyint(1) DEFAULT 0,
  `trashed_at` datetime DEFAULT NULL,
  `trashed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `buaweightage`
--

INSERT INTO `buaweightage` (`id`, `difference`, `bigger_sp`, `smaller_sp`, `created_at`, `created_by`, `updated_at`, `updated_by`, `trashed`, `trashed_at`, `trashed_by`) VALUES
(1, '100.00', '85.00', '115.00', NULL, NULL, '2020-12-10 17:02:52', 1, 0, NULL, NULL),
(2, '90.00', '86.50', '113.50', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(3, '80.00', '88.00', '112.00', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(4, '70.00', '89.50', '110.50', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(5, '60.00', '91.00', '109.00', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(6, '50.00', '92.50', '107.50', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(7, '40.00', '94.00', '106.00', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(8, '30.00', '95.50', '104.50', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(9, '20.00', '97.00', '103.00', NULL, NULL, NULL, NULL, 0, NULL, NULL),
(10, '10.00', '98.50', '101.50', NULL, NULL, NULL, NULL, 0, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buaweightage`
--
ALTER TABLE `buaweightage`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buaweightage`
--
ALTER TABLE `buaweightage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
