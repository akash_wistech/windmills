-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 07, 2020 at 03:10 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mastertajzi_windmillscrm`
--

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` int(11) NOT NULL,
  `valuation_approach` int(11) NOT NULL,
  `approach_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `complete_years` int(11) DEFAULT NULL,
  `required_documents` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `trashed` tinyint(1) DEFAULT 0,
  `trashed_at` datetime DEFAULT NULL,
  `trashed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `title`, `category`, `valuation_approach`, `approach_reason`, `age`, `complete_years`, `required_documents`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `trashed`, `trashed_at`, `trashed_by`) VALUES
(1, 'Eligendi mollitia voluptatem Deserunt Nam ut illo quis', 4, 2, 'Dolorum tenetur nostrum rerum fugiat eum tempor impedit quia facilis asperiores provident', 243, 2018, '1,2,3,4,14,32', 1, '2020-12-07 16:14:40', 1, '2020-12-07 18:02:02', 1, 0, NULL, NULL),
(2, 'Laudantium aliquid fugiat deleniti in elit incidunt sed a iusto', 3, 2, 'Nisi consequatur animi quo facilis molestiae ad natus debitis enim doloribus quisquam repellendus Aliquam nihil reprehenderit illo ex itaque cupidatat', 10, 20, '31,32,Autem', 1, '2020-12-07 17:16:22', 1, '2020-12-07 17:16:22', 1, 0, NULL, NULL),
(3, 'Odit accusantium aliqua Vel in non delectus amet dolor sequi occaecat nisi nihil duis corrupti a', 4, 1, 'Id velit excepturi non sint et pariatur Ut aute quo ipsum illum est eum eaque', 23, 2323, '2,4', 1, '2020-12-07 18:04:21', 1, '2020-12-07 18:04:21', 1, 0, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
