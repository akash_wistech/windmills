ALTER TABLE `action_log` ADD `visibility` TINYINT(1) NULL DEFAULT '1' AFTER `comments`;
ALTER TABLE `action_log` ADD `priority` TINYINT(1) NULL DEFAULT '1' AFTER `comments`;

--
-- Table structure for table `action_log_manager`
--

CREATE TABLE `action_log_manager` (
  `action_log_id` bigint(20) NOT NULL DEFAULT '0',
  `staff_id` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `action_log_manager`
--
ALTER TABLE `action_log_manager`
  ADD KEY `action_log_id` (`action_log_id`),
  ADD KEY `staff_id` (`staff_id`);


  --
  -- Table structure for table `action_log_calendar`
  --

  CREATE TABLE `action_log_calendar` (
    `action_log_id` bigint(20) NOT NULL DEFAULT '0',
    `calendar_id` bigint(20) NOT NULL DEFAULT '0'
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

  -- --------------------------------------------------------

  --
  -- Table structure for table `action_log_reminder`
  --

  CREATE TABLE `action_log_reminder` (
    `action_log_id` bigint(20) NOT NULL,
    `notify_to` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
    `notify_time` int(11) NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

  -- --------------------------------------------------------

  --
  -- Table structure for table `action_log_subject`
  --

  CREATE TABLE `action_log_subject` (
    `action_log_id` bigint(20) NOT NULL DEFAULT '0',
    `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

  --
  -- Indexes for dumped tables
  --

  --
  -- Indexes for table `action_log_calendar`
  --
  ALTER TABLE `action_log_calendar`
    ADD KEY `action_log_id` (`action_log_id`),
    ADD KEY `calendar_id` (`calendar_id`);

  --
  -- Indexes for table `action_log_reminder`
  --
  ALTER TABLE `action_log_reminder`
    ADD KEY `action_log_id` (`action_log_id`),
    ADD KEY `notify_to` (`notify_to`),
    ADD KEY `notify_time` (`notify_time`);

  --
  -- Indexes for table `action_log_subject`
  --
  ALTER TABLE `action_log_subject`
    ADD KEY `action_log_id` (`action_log_id`);

ALTER TABLE `action_log_subject` ADD `due_date` DATETIME NULL AFTER `subject`;

--
-- Table structure for table `calendar`
--

CREATE TABLE `calendar` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `trashed` tinyint(1) DEFAULT '0',
  `trashed_at` datetime DEFAULT NULL,
  `trashed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `calendar`
--
ALTER TABLE `calendar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trashed` (`trashed`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `calendar`
--
ALTER TABLE `calendar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
