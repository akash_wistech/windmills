-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 09, 2020 at 08:17 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mastertajzi_windmillscrm`
--

-- --------------------------------------------------------

--
-- Table structure for table `developments`
--

CREATE TABLE `developments` (
  `id` int(11) NOT NULL,
  `emirates` int(11) NOT NULL,
  `property_type` int(11) NOT NULL,
  `star1` int(11) NOT NULL,
  `star2` int(11) NOT NULL,
  `star3` int(11) NOT NULL,
  `star4` int(11) NOT NULL,
  `star5` int(11) NOT NULL,
  `professional_charges` decimal(10,2) NOT NULL,
  `contingency_margin` decimal(10,2) NOT NULL,
  `obsolescence` decimal(10,2) NOT NULL,
  `developer_profit` decimal(10,2) NOT NULL,
  `parking_price` int(11) NOT NULL,
  `pool_price` int(11) NOT NULL,
  `landscape_price` int(11) NOT NULL,
  `whitegoods_price` int(11) NOT NULL,
  `utilities_connected_price` int(11) NOT NULL,
  `developer_margin` decimal(10,2) NOT NULL,
  `interest_rate` decimal(10,2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `trashed_at` datetime DEFAULT NULL,
  `trashed` tinyint(1) DEFAULT 0,
  `trashed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `developments`
--

INSERT INTO `developments` (`id`, `emirates`, `property_type`, `star1`, `star2`, `star3`, `star4`, `star5`, `professional_charges`, `contingency_margin`, `obsolescence`, `developer_profit`, `parking_price`, `pool_price`, `landscape_price`, `whitegoods_price`, `utilities_connected_price`, `developer_margin`, `interest_rate`, `created_at`, `updated_at`, `created_by`, `updated_by`, `trashed_at`, `trashed`, `trashed_by`) VALUES
(1, 3507, 3, 10, 20, 30, 40, 50, '5.00', '6.00', '7.00', '8.00', 1000, 898, 52, 305, 625, '9.00', '10.00', NULL, NULL, NULL, NULL, NULL, 0, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `developments`
--
ALTER TABLE `developments`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `developments`
--
ALTER TABLE `developments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
