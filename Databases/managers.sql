--
-- Table structure for table `company_manager`
--

CREATE TABLE `company_manager` (
  `company_id` bigint(20) NOT NULL DEFAULT '0',
  `staff_id` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contact_manager`
--

CREATE TABLE `contact_manager` (
  `contact_id` bigint(20) NOT NULL DEFAULT '0',
  `staff_id` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `lead_manager`
--

CREATE TABLE `lead_manager` (
  `lead_id` bigint(20) NOT NULL DEFAULT '0',
  `staff_id` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `company_manager`
--
ALTER TABLE `company_manager`
  ADD KEY `company_id` (`company_id`),
  ADD KEY `staff_id` (`staff_id`);

--
-- Indexes for table `contact_manager`
--
ALTER TABLE `contact_manager`
  ADD KEY `contact_id` (`contact_id`),
  ADD KEY `staff_id` (`staff_id`);

--
-- Indexes for table `lead_manager`
--
ALTER TABLE `lead_manager`
  ADD KEY `lead_id` (`lead_id`),
  ADD KEY `staff_id` (`staff_id`);

ALTER TABLE `lead` ADD `rec_type` TINYINT(1) NULL DEFAULT '1' AFTER `id`, ADD `lead_id` BIGINT NOT NULL DEFAULT '0' AFTER `rec_type`, ADD INDEX (`rec_type`), ADD INDEX (`lead_id`);
