-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Dec 15, 2020 at 06:08 PM
-- Server version: 5.7.26
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yii2crm`
--

-- --------------------------------------------------------

--
-- Table structure for table `action_log_event_info`
--

CREATE TABLE `action_log_event_info` (
  `action_log_id` bigint(20) NOT NULL DEFAULT '0',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `color_code` char(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `all_day` tinyint(1) DEFAULT '0',
  `sub_type` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `action_log_time`
--

CREATE TABLE `action_log_time` (
  `action_log_id` bigint(20) NOT NULL DEFAULT '0',
  `start_dt` datetime NOT NULL,
  `end_dt` datetime DEFAULT NULL,
  `hours` int(11) DEFAULT '0',
  `minutes` char(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `action_log_event_info`
--
ALTER TABLE `action_log_event_info`
  ADD KEY `action_log_id` (`action_log_id`);

--
-- Indexes for table `action_log_time`
--
ALTER TABLE `action_log_time`
  ADD KEY `action_log_id` (`action_log_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
