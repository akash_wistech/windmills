ALTER TABLE `action_log_subject` ADD `status` TINYINT(1) NULL DEFAULT '0' AFTER `due_date`, ADD INDEX (`status`);
ALTER TABLE `action_log_subject` ADD `completed_at` DATETIME NULL AFTER `status`;
