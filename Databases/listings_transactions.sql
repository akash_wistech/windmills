-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 26, 2020 at 10:46 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mastertajzi_windmillscrm_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `listings_transactions`
--

CREATE TABLE `listings_transactions` (
  `id` int(11) NOT NULL,
  `listings_reference` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source` int(11) NOT NULL,
  `listing_website_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `listing_date` date NOT NULL,
  `property_listed` int(11) NOT NULL,
  `property_category` int(11) NOT NULL,
  `building_info` int(11) DEFAULT NULL,
  `tenure` int(11) NOT NULL,
  `unit_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `floor_number` int(11) NOT NULL,
  `number_of_levels` int(11) NOT NULL,
  `location` int(11) NOT NULL,
  `land_size` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `built_up_area` decimal(10,2) NOT NULL,
  `balcony_size` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `property_placement` int(11) NOT NULL,
  `property_visibility` int(11) NOT NULL,
  `property_exposure` int(11) NOT NULL,
  `listing_property_type` int(11) NOT NULL,
  `property_condition` int(11) NOT NULL,
  `development_type` enum('Standard','Non-Standard') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `finished_status` enum('Shell & Core','Fitted') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pool` enum('Yes','No') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gym` enum('Yes','No') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `play_area` enum('Yes','No') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_facilities` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `completion_status` enum('Ready','Not Ready') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_bedrooms` int(11) NOT NULL,
  `upgrades` int(11) NOT NULL,
  `full_building_floors` int(11) NOT NULL,
  `parking_space` int(11) NOT NULL,
  `view` int(11) NOT NULL,
  `landscaping` enum('Yes','No','Semi-Landscape') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `white_goods` enum('Yes','No') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `furnished` enum('Yes','No','Semi-Furnished') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `utilities_connected` enum('Yes','No') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `developer_margin` enum('Yes','No') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `listings_price` decimal(10,2) DEFAULT NULL,
  `listings_rent` decimal(10,2) DEFAULT NULL,
  `price_per_sqt` decimal(10,2) DEFAULT NULL,
  `final_price` decimal(10,2) DEFAULT NULL,
  `agent_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agent_phone_no` int(11) DEFAULT NULL,
  `agent_company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `trashed` tinyint(1) DEFAULT 0,
  `trashed_at` datetime DEFAULT NULL,
  `trashed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `listings_transactions`
--

INSERT INTO `listings_transactions` (`id`, `listings_reference`, `source`, `listing_website_link`, `listing_date`, `property_listed`, `property_category`, `building_info`, `tenure`, `unit_number`, `floor_number`, `number_of_levels`, `location`, `land_size`, `built_up_area`, `balcony_size`, `property_placement`, `property_visibility`, `property_exposure`, `listing_property_type`, `property_condition`, `development_type`, `finished_status`, `pool`, `gym`, `play_area`, `other_facilities`, `completion_status`, `no_of_bedrooms`, `upgrades`, `full_building_floors`, `parking_space`, `view`, `landscaping`, `white_goods`, `furnished`, `utilities_connected`, `developer_margin`, `listings_price`, `listings_rent`, `price_per_sqt`, `final_price`, `agent_name`, `agent_phone_no`, `agent_company`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `trashed`, `trashed_at`, `trashed_by`) VALUES
(1, 'REF-2020-01', 2, 'http://local.windmills/listings-transactions/create', '2020-03-20', 1, 2, 2, 3, '1213', 4, 3, 1, '344', '333.00', '223', 2, 2, 2, 2, 2, 'Non-Standard', '', 'No', 'Yes', 'No', '1,3', 'Not Ready', 243, 234, 324, 234, 2, 'Yes', 'No', 'No', 'No', 'No', '43.00', '434.00', '55.00', '44.00', 'sfdf', 343, 'dfdg', 1, '2020-12-22 19:01:19', 1, '2020-12-23 17:07:06', 1, 0, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `listings_transactions`
--
ALTER TABLE `listings_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `listings_transactions`
--
ALTER TABLE `listings_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
