-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 30, 2020 at 02:54 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mastertajzi_windmillscrm_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `buildings`
--

CREATE TABLE `buildings` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `community` int(11) NOT NULL,
  `sub_community` int(11) DEFAULT NULL,
  `property_id` int(11) NOT NULL,
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estimated_age` decimal(10,2) NOT NULL,
  `developer_id` int(11) DEFAULT NULL,
  `city` int(11) NOT NULL,
  `number_of_basement` int(11) DEFAULT NULL,
  `parking_floors` int(11) DEFAULT NULL,
  `typical_floors` int(11) DEFAULT NULL,
  `building_number` int(11) DEFAULT NULL,
  `plot_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `makani_number` int(11) DEFAULT NULL,
  `street` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `property_category` int(11) NOT NULL,
  `utilities_connected` enum('Yes','No') COLLATE utf8mb4_unicode_ci NOT NULL,
  `tenure` int(11) NOT NULL,
  `property_placement` int(11) NOT NULL,
  `property_visibility` int(11) NOT NULL,
  `property_exposure` int(11) NOT NULL,
  `property_condition` int(11) NOT NULL,
  `development_type` enum('Standard','Non-Standard') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `finished_status` enum('Shell & Core','Fitted') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pool` enum('Yes','No') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gym` enum('Yes','No') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `play_area` enum('Yes','No') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other_facilities` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `completion_status` enum('Ready','Not Ready') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `landscaping` enum('Yes','No','Semi-Landscape') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `white_goods` enum('Yes','No') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `furnished` enum('Yes','No','Semi-Furnished') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` int(11) DEFAULT NULL,
  `listing_property_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `trashed_at` datetime DEFAULT NULL,
  `trashed_by` int(11) DEFAULT NULL,
  `trashed` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `buildings`
--

INSERT INTO `buildings` (`id`, `title`, `community`, `sub_community`, `property_id`, `latitude`, `longitude`, `estimated_age`, `developer_id`, `city`, `number_of_basement`, `parking_floors`, `typical_floors`, `building_number`, `plot_number`, `makani_number`, `street`, `property_category`, `utilities_connected`, `tenure`, `property_placement`, `property_visibility`, `property_exposure`, `property_condition`, `development_type`, `finished_status`, `pool`, `gym`, `play_area`, `other_facilities`, `completion_status`, `landscaping`, `white_goods`, `furnished`, `location`, `listing_property_type`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`, `trashed_at`, `trashed_by`, `trashed`) VALUES
(1, 'The Dubai creek Residences', 172, 31, 1, '25.206322', '55.343962', '1.00', 101, 3510, 2, NULL, 30, 3, '114', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-24 11:06:22', 9, '2020-12-27 13:03:39', 9, NULL, NULL, 0),
(2, 'MAG318', 8, 156, 1, '25°11\'26.9\"N ', '55°17\'00.2\"E ', '0.00', 162, 3510, 2, NULL, 22, NULL, '133', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-24 11:09:04', 9, '2020-12-27 13:03:50', 9, NULL, NULL, 0),
(3, 'Zahra Square', 125, 140, 1, '25.008258903550654', '55.29338041148681', '0.00', 190, 3510, NULL, NULL, NULL, NULL, '32', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-24 13:06:20', 9, '2020-12-27 13:04:05', 9, NULL, NULL, 0),
(4, 'Lago Vista Tower C', 104, 213, 1, '25°02\'06.0\"N ', '55°12\'05.0\"E', '13.00', 77, 3510, NULL, NULL, NULL, NULL, '', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2020-12-24 13:20:20', 9, '2020-12-29 18:40:51', 9, NULL, NULL, 0),
(5, 'Al Thamam 22', 55, 26, 1, '25.0050523,', '55.2498981', '13.00', 29, 3510, NULL, NULL, 5, NULL, '43', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-24 13:26:09', 9, '2020-12-27 13:04:46', 9, NULL, NULL, 0),
(6, 'The Pulse Boulevard Apartments', 126, 248, 1, '24.942609', '55.220944', '0.00', 94, 3510, NULL, NULL, NULL, NULL, '', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-24 13:47:57', 9, '2020-12-27 13:04:58', 9, NULL, NULL, 0),
(7, 'Jumeirah Lakes Towers', 44, 120, 0, '25.079589', '55.150740', '28.00', 82, 3510, NULL, NULL, 34, NULL, '66', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2020-12-24 13:56:19', 9, '2020-12-27 13:05:47', 9, NULL, NULL, 0),
(8, 'Al Khour Towers', 57, 291, 1, '25.394104', '55.458619', '10.00', 134, 3507, NULL, NULL, NULL, NULL, '', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-24 14:05:39', 9, '2020-12-27 13:06:06', 9, NULL, NULL, 0),
(9, 'World Trade Center Residences', 251, 319, 1, '25.226846', '55.289572', '0.00', 151, 3510, NULL, NULL, 39, NULL, '51', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-24 14:11:10', 9, '2020-12-27 13:06:29', 9, NULL, NULL, 0),
(10, 'Jumeirah Beach Residence', 37, 256, 1, '25.076101', '55.132946', '20.00', 91, 3510, NULL, NULL, 32, NULL, '173', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2020-12-24 14:22:40', 9, '2020-12-27 13:06:49', 9, NULL, NULL, 0),
(11, 'Bluewaters Residences', 37, 256, 1, '25.079523,', '55.122314', '1.00', 169, 3510, NULL, NULL, 13, 7, '3641', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-24 15:12:48', 9, '2020-12-27 13:07:03', 9, NULL, NULL, 0),
(12, 'Ocean Heights', 37, 256, 1, '25.090860', '55.148810', '10.00', 77, 3510, NULL, NULL, 80, NULL, '47', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-24 15:16:14', 9, '2020-12-27 13:07:20', 9, NULL, NULL, 0),
(13, 'Acacia (Tower B) at Park Heights', 220, 174, 1, '25.110536', '55.245347 ', '0.00', 101, 3510, NULL, NULL, 9, NULL, '', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-24 15:29:22', 9, '2020-12-27 13:07:40', 9, NULL, NULL, 0),
(14, 'Al Ramth 37', 55, 26, 1, '25.0006868', '55.2546063', '6.00', 91, 3510, NULL, NULL, NULL, NULL, '43', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-24 15:38:03', 9, '2020-12-27 13:07:59', 9, NULL, NULL, 0),
(15, 'Centurion Residence', 18, 182, 1, '24.990627', '55.201257', '8.00', 214, 3510, NULL, NULL, NULL, NULL, '249', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-24 15:41:16', 9, '2020-12-27 13:08:23', 9, NULL, NULL, 0),
(16, 'Garden Apartments', 40, 180, 1, '25.0060781', '55.1651470', '16.00', 266, 3510, NULL, NULL, NULL, NULL, '356', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-24 15:49:34', 9, '2020-12-27 13:08:49', 9, NULL, NULL, 0),
(17, 'Aces Chateau', 1, 335, 1, '25.067177', '55.211330', '1.00', 6, 3510, NULL, NULL, 4, NULL, '34', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-24 16:26:36', 9, '2020-12-29 17:05:40', 9, NULL, NULL, 0),
(18, 'Bluewaters Residences 4', 37, 256, 1, '25.077821,', '55.121856', '42.00', 169, 3510, NULL, NULL, 7, NULL, '', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-24 16:48:01', 9, '2020-12-27 13:37:55', 9, NULL, NULL, 0),
(19, 'Murjan 5', 37, 258, 1, '25.082019', '55.140299', '13.00', 91, 3510, 1, 2, 39, NULL, '170', 392551, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-24 17:05:31', 9, '2020-12-27 13:38:23', 9, NULL, NULL, 0),
(20, 'Green Lakes Tower 2', 44, 120, 1, '25.078355', '55.148673', '13.00', 54, 3510, 2, NULL, 41, NULL, '61', 1327074915, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-24 17:13:03', 9, '2020-12-27 13:38:16', 9, NULL, NULL, 0),
(21, 'Sunshine Residence', 80, 282, 1, '25.123341,', '55.375050', '3.00', 241, 3510, 1, NULL, 8, NULL, '732', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-27 09:59:04', 9, '2020-12-27 13:38:35', 9, NULL, NULL, 0),
(22, 'Al Haseer', 24, 231, 1, '25.112076', '55.143976', '11.00', 183, 3510, 2, NULL, 10, 7, '550', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-27 10:05:19', 9, '2020-12-27 13:38:54', 9, NULL, NULL, 0),
(23, 'Beverly Residence', 293, 230, 1, '25°03\'00.4\"N ', '55°12\'50.2\"E', '1.00', 122, 3510, NULL, NULL, 5, NULL, '235', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-27 10:22:21', 9, '2020-12-27 15:26:44', 9, NULL, NULL, 0),
(24, 'Al Dana Tower', 65, 39, 0, '25°19\'56.4\"N ', '55°22\'34.1\"E', '12.00', 82, 3509, NULL, 3, 40, NULL, '525/622', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-27 10:30:45', 9, '2020-12-27 10:30:45', 9, NULL, NULL, 0),
(25, 'Rimal 4', 37, 258, 0, '25°04\'33.9\"N ', '55°08\'00.6\"E', '15.00', 91, 3510, NULL, NULL, 35, NULL, '173', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-27 10:35:12', 9, '2020-12-27 10:35:12', 9, NULL, NULL, 0),
(26, 'Celestia B', 126, 248, 1, '24°56\'53.8\"N 55°12\'10.3\"E', '55°12\'10.3\"E', '1.00', 77, 3510, NULL, 3, 8, NULL, '', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-28 10:41:35', 9, '2020-12-28 10:41:35', 9, NULL, NULL, 0),
(27, 'Churchill Tower 2', 8, 156, 1, '25°10\'52.9\"N ', '55°15\'43.6\"E', '10.00', 82, 3510, 3, 3, 56, NULL, '33', 346563, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-28 10:48:05', 9, '2020-12-28 10:48:05', 9, NULL, NULL, 0),
(28, 'The Polo Residence C7', 81, 334, 1, '25°09\'04.0\"N ', '55°17\'49.6\"E', '4.00', 146, 3510, NULL, 1, 4, NULL, '289', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-28 10:57:54', 9, '2020-12-28 10:57:54', 9, NULL, NULL, 0),
(29, 'The Polo Residence C7', 81, 334, 1, '25°09\'04.0\"N ', '55°17\'49.6\"E', '4.00', 146, 3510, NULL, 1, 4, NULL, '289', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, '2020-12-28 11:00:23', 9, '2020-12-28 12:09:00', 9, NULL, NULL, 0),
(30, 'La Riviera Tower', 37, 256, 1, '25°04\'25.3\"N ', '55°08\'19.3\"E', '15.00', 57, 3510, 3, NULL, 33, NULL, '149', 392461, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-28 11:06:21', 9, '2020-12-28 11:06:21', 9, NULL, NULL, 0),
(31, 'Laya Residences', 1, 335, 1, '25°03\'59.9\"N ', '55°12\'33.1\"E', '3.00', 10, 3510, 1, NULL, 4, NULL, '354', 1934273502, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-28 11:17:20', 9, '2020-12-29 17:49:12', 9, NULL, NULL, 0),
(32, 'The Manhattan', 1, 335, 1, '25°04\'08.3\"N ', '55°12\'26.6\"E', '10.00', 19, 3510, 3, NULL, 10, NULL, '437', 6811051, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-28 11:25:12', 9, '2020-12-29 17:50:06', 9, NULL, NULL, 0),
(33, 'The Address Fountain Views Tower 2', 304, 173, 1, '25°11\'41.8\"N ', '55°16\'57.0\"E', '2.00', 101, 3510, NULL, 4, 52, NULL, '', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-28 11:36:05', 9, '2020-12-28 11:36:05', 9, NULL, NULL, 0),
(34, 'Hayat Boulevard 2A', 125, 140, 1, '25°00\'16.8\"N ', '55°17\'49.2\"E', '1.00', 190, 3510, NULL, 1, 7, NULL, '218', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-28 11:44:59', 9, '2020-12-28 11:44:59', 9, NULL, NULL, 0),
(35, 'Marina Quay West', 37, 256, 1, '25°04\'35.3\"N ', '55°08\'10.0\"E', '12.00', 101, 3510, NULL, 3, 38, NULL, '191', 1202974721, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-29 15:50:05', 9, '2020-12-29 15:50:05', 9, NULL, NULL, 0),
(36, 'Serenia Residences C', 24, 231, 1, '25°07\'55.9\"N ', ' 55°09\'06.1\"E', '2.00', 198, 3510, 2, NULL, 9, NULL, '2334', 1369780877, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-29 16:10:13', 9, '2020-12-29 16:10:13', 9, NULL, NULL, 0),
(37, 'Glamz Residence Tower 1', 25, 202, 1, '1\'59.9\"N ', '55°08\'28.6\"E', '1.00', 79, 3510, 1, 2, 15, NULL, '2619', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-29 16:16:00', 9, '2020-12-29 16:16:00', 9, NULL, NULL, 0),
(38, 'Icon 1', 44, 119, 1, '25°03\'53.9\"N ', '55°08\'19.2\"E ', '12.00', 28, 3510, 2, NULL, 37, NULL, '992', 393979, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-29 16:26:47', 9, '2020-12-29 16:26:47', 9, NULL, NULL, 0),
(39, 'Two Towers A', 13, 103, 1, '25°05\'41.4\"N ', '55°10\'43.0\"E', '11.00', 208, 3510, 3, NULL, 25, NULL, '67', 383278, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-29 16:39:48', 9, '2020-12-29 16:39:48', 9, NULL, NULL, 0),
(40, 'Bennett House 1', 263, 265, 1, '25°02\'45.7\"N ', '55°14\'45.2\"E', '10.00', 266, 3510, 2, NULL, 3, NULL, '11', 674299, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-29 16:52:52', 9, '2020-12-29 16:52:52', 9, NULL, NULL, 0),
(41, 'Botanica Tower -1', 37, 256, 1, '25.084955, 55.142349', '55.142349', '9.00', 225, 3510, 3, NULL, 41, NULL, '102', 1268075678, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-29 17:15:37', 9, '2020-12-29 17:15:37', 9, NULL, NULL, 0),
(42, 'Prive', 8, 156, 1, '25.184698, ', '55.275256', '1.00', 77, 3510, NULL, NULL, NULL, NULL, '', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-29 17:47:35', 9, '2020-12-29 17:47:35', 9, NULL, NULL, 0),
(43, 'Genesis Residences', 321, 336, 1, '25.066813, ', '55.246046', '1.00', 170, 3510, NULL, NULL, 10, NULL, '', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 09:59:26', 9, '2020-12-30 09:59:26', 9, NULL, NULL, 0),
(44, 'Imperial Residence - Tower B', 108, 19, 1, '25.047659,', '55.195349', '6.00', 246, 3510, 1, NULL, 28, NULL, '', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 10:08:39', 9, '2020-12-30 10:08:39', 9, NULL, NULL, 0),
(45, 'Skycourt Towers - E', 321, 336, 1, '25.090919, ', '55.385268', '9.00', 281, 3510, 1, 3, 24, NULL, '', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 10:21:09', 9, '2020-12-30 10:21:09', 9, NULL, NULL, 0),
(46, 'Al Sirhi Tower', 65, 39, 1, '25°20\'01.2\"N ', '55°22\'28.3\"E', '10.00', 188, 3509, NULL, NULL, 15, NULL, '66/640', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 10:33:15', 9, '2020-12-30 10:33:15', 9, NULL, NULL, 0),
(47, 'The Crescent C, Building No 3', 180, 260, 1, '25°02\'01.8\"N ', '55°12\'06.6\"E', '12.00', 77, 3510, NULL, 2, 18, NULL, '1', 1858469885, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 10:41:05', 9, '2020-12-30 10:41:05', 9, NULL, NULL, 0),
(48, 'The Residences 1 at Business Central', 8, 156, 1, '25°11\'21.1\"N ', '55°17\'20.7\"E', '7.00', 77, 3510, 1, 1, 22, NULL, '10', 346671, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 10:46:31', 9, '2020-12-30 10:46:31', 9, NULL, NULL, 0),
(49, 'Al Habtoor City - Noura Tower', 8, 157, 1, '25.182685, ', '55.255355', '2.00', 21, 3510, 2, 6, 74, NULL, '', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 10:59:51', 9, '2020-12-30 10:59:51', 9, NULL, NULL, 0),
(50, 'Sky Gardens', 14, 172, 1, '25°12\'43.4\"N ', '55°16\'57.4\"E', '12.00', 28, 3510, NULL, NULL, NULL, NULL, '', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 11:06:31', 9, '2020-12-30 11:06:31', 9, NULL, NULL, 0),
(51, 'The Bridges Tower 1', 226, 73, 1, '24°30\'35.8\"N ', '54°24\'25.3\"E ', '1.00', 282, 3506, 2, NULL, 25, NULL, '', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 11:15:44', 9, '2020-12-30 11:15:44', 9, NULL, NULL, 0),
(52, 'Elite Residence', 37, 256, 1, '25°05\'21.7\"N ', '55°08\'53.0\"E', '8.00', 246, 3510, NULL, NULL, 58, NULL, '26', 1319376170, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 11:26:13', 9, '2020-12-30 11:26:13', 9, NULL, NULL, 0),
(53, 'Celestia A', 322, 338, 1, '24°56\'54.3\"N ', '55°12\'10.5\"E', '1.00', 77, 3510, 2, NULL, 7, NULL, '', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 11:40:17', 9, '2020-12-30 11:40:17', 9, NULL, NULL, 0),
(54, 'Al Bashri - B1, Building No. 1', 24, 232, 1, '25°06\'25.4\"N ', '55°09\'03.5\"E', '11.00', 183, 3510, 2, NULL, 11, NULL, '1122', 1355678057, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 11:46:37', 9, '2020-12-30 11:46:37', 9, NULL, NULL, 0),
(55, 'Icon Tower 2', 44, 119, 1, '25°03\'55.8\"N 55°08\'23.7\"E', '55°08\'23.7\"E', '7.00', 28, 3510, NULL, NULL, 40, NULL, '983', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 11:52:54', 9, '2020-12-30 11:52:54', 9, NULL, NULL, 0),
(56, 'Town Square Safi 2A', 125, 141, 1, '25°00\'44.2\"N ', '55°17\'20.0\"E', '1.00', 190, 3510, 1, NULL, 7, NULL, '219', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 11:59:00', 9, '2020-12-30 11:59:00', 9, NULL, NULL, 0),
(57, 'Samar 4', 223, 200, 1, '25°05\'32.0\"N ', '55°10\'22.7\"E', '17.00', 101, 3510, NULL, NULL, 4, NULL, '23', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 12:03:58', 9, '2020-12-30 12:03:58', 9, NULL, NULL, 0),
(58, 'Villa Myra', 1, 335, 1, '25°03\'28.7\"N ', '55°11\'57.9\"E', '5.00', 234, 3510, NULL, NULL, 4, NULL, '141', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 12:09:55', 9, '2020-12-30 12:09:55', 9, NULL, NULL, 0),
(59, 'Elite Sports Residences 6', 147, 24, 1, '25°02\'29.6\"N ', '55°13\'03.3\"E', '11.00', 262, 3510, 1, 4, 21, NULL, '84', 682139, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 12:14:57', 9, '2020-12-30 12:14:57', 9, NULL, NULL, 0),
(60, 'Bridges Tower 2', 226, 73, 1, '24°30\'32.4\"N ', '4°24\'27.7\"E', '1.00', 282, 3506, 2, NULL, 26, NULL, '', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 12:18:26', 9, '2020-12-30 12:18:26', 9, NULL, NULL, 0),
(61, 'Diamond Views 4', 1, 335, 1, '25°03\'51.0\"N ', '55°12\'59.0\"E', '8.00', 85, 3510, NULL, NULL, 4, NULL, '1293', 681734, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 12:36:08', 9, '2020-12-30 12:36:08', 9, NULL, NULL, 0),
(62, 'Royal Oceanic 1', 37, 256, 1, '25°05\'03.6\"N ', '55°08\'31.5\"E', '11.00', 92, 3510, 2, NULL, 32, NULL, '218', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 12:40:51', 9, '2020-12-30 12:40:51', 9, NULL, NULL, 0),
(63, 'Al Nakheel 1', 171, 317, 1, '25°05\'38.1\"N ', '55°10\'09.5\"E', '18.00', 101, 3510, NULL, NULL, 3, NULL, '', NULL, '2', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 12:45:28', 9, '2020-12-30 12:45:28', 9, NULL, NULL, 0),
(64, 'Azizi Farishta Residence', 53, 220, 1, '25°01\'26.5\"N ', '55°08\'20.4\"E', '1.00', 56, 3510, 1, 2, 12, NULL, '2636', 1224068913, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 12:50:00', 9, '2020-12-30 12:50:00', 9, NULL, NULL, 0),
(65, 'Burj Al Nakhala 1 (Palm Tower 1)', 323, 339, 1, '25°18\'58.0\"N ', '55°21\'54.5\"E', '9.00', 283, 3509, NULL, NULL, 48, NULL, '969/1247', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 13:01:51', 9, '2020-12-30 13:01:51', 9, NULL, NULL, 0),
(66, 'Skycourts Tower B ', 122, 323, 1, '25°05\'28.5\"N ', '55°23\'09.2\"E', '9.00', 186, 3510, NULL, NULL, 21, NULL, '272', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 13:11:33', 9, '2020-12-30 13:11:33', 9, NULL, NULL, 0),
(67, 'The Wings B', 106, 18, 1, '25.060019,', '55.241503', '1.00', 29, 3510, NULL, NULL, NULL, NULL, '278', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 13:18:13', 9, '2020-12-30 13:18:13', 9, NULL, NULL, 0),
(68, 'Al Thamam 22', 55, 26, 1, '25°00\'10.8\"N ', '55°14\'55.8\"E', '8.00', 176, 3510, NULL, NULL, 5, NULL, '43', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 14:04:20', 9, '2020-12-30 14:04:20', 9, NULL, NULL, 0),
(69, 'Marina Diamond 5', 37, 256, 1, '25.081264, ', '55.146254', '13.00', 85, 3510, 2, NULL, 19, NULL, '90', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 14:09:35', 9, '2020-12-30 14:09:35', 9, NULL, NULL, 0),
(70, 'Ansam 3', 133, 340, 1, '24°28\'59.2\"N ', '54°36\'03.0\"E', '2.00', 282, 3506, 1, NULL, 6, NULL, 'C6', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 14:15:50', 9, '2020-12-30 14:15:50', 9, NULL, NULL, 0),
(71, 'Aamna Residency', 300, 341, 1, '25°08\'39.9\"N ', '55°24\'17.5\"E', '1.00', 183, 3510, NULL, NULL, 8, NULL, '386', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 14:32:19', 9, '2020-12-30 14:32:19', 9, NULL, NULL, 0),
(72, 'Azizi Tulip', 53, 220, 1, '25°01\'43.5\"N ', '55°09\'13.6\"E', '2.00', 56, 3510, NULL, NULL, 11, NULL, '2118', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 14:37:06', 9, '2020-12-30 14:37:06', 9, NULL, NULL, 0),
(73, 'Blvd Central 1', 15, 152, 1, '25.191813, 55.273752 ', '55.273752', '8.00', 101, 3510, 3, NULL, 24, NULL, '191', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 14:48:29', 9, '2020-12-30 14:48:29', 9, NULL, NULL, 0),
(74, 'Princess Tower', 37, 256, 1, '25.08854120,', '55.14683629', '8.00', 246, 3510, NULL, NULL, 97, NULL, '22', 392192, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 15:06:30', 9, '2020-12-30 15:06:30', 9, NULL, NULL, 0),
(75, 'MED 51', 53, 219, 1, '25°02\'39.1\"N ', '55°08\'09.2\"E', '10.00', 183, 3510, NULL, NULL, 4, NULL, '72', NULL, '4', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 16:07:24', 9, '2020-12-30 16:07:24', 9, NULL, NULL, 0),
(76, 'South Ridge 1', 15, 155, 1, '25°11\'13.3\"N ', '55°16\'36.1\"E', '12.00', 101, 3510, NULL, NULL, 39, NULL, '201', NULL, '', 0, 'Yes', 0, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2020-12-30 16:11:54', 9, '2020-12-30 16:11:54', 9, NULL, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buildings`
--
ALTER TABLE `buildings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buildings`
--
ALTER TABLE `buildings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
