<?php

return [
    'class' => 'yii\db\Connection',
   // 'dsn' => 'mysql:host=localhost;dbname=wmtestlive',
    'dsn' => 'mysql:host=localhost;dbname=dec_4',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8mb4',

    // Schema cache options (for production environment)
    'enableSchemaCache' => false,
    'schemaCacheDuration' => 60,
    'schemaCache' => 'cache',
];
