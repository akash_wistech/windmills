<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$mailer = require __DIR__ . '/mailer.php';
$mailerb = require __DIR__ . '/mailerb.php';
$urlManager = require __DIR__ . '/urlmanager.php';

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@tests' => '@app/tests',
    ],
    'modules' => [
      'wisnotify' => [
       'class' => 'app\modules\wisnotify\Module',
   ],
],
'components' => [
    
    's3bucket' => [
        'class' => \frostealth\yii2\aws\s3\Storage::className(),
        'region' => 'eu-central-1',
            'credentials' => [ // Aws\Credentials\CredentialsInterface|array|callable
            'key' => 'AKIAX5FCDO55U22GFOUH',
            'secret' => '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',
        ],
        'bucket' => 'winsmills-wiz-prod',
        'cdnHostname' => false,
        'defaultAcl' => \frostealth\yii2\aws\s3\Storage::ACL_PUBLIC_READ,
            'debug' => false, // bool|array
        ],

        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'helperFunctions' => [
          'class' => 'app\components\helpers\HelperFunctions',
      ],
      'appHelperFunctions' => [
          'class' => 'app\components\helpers\AppHelperFunction',
      ],
    'propertyFinderHelperFunctions' => [
        'class' => 'app\components\helpers\PropertyFinderHelperFunctions',
    ],

      'log' => [
        'targets' => [
            [
                'class' => 'yii\log\FileTarget',
                'levels' => ['error', 'warning'],
            ],
        ],
    ],
    'mailer' => $mailer,
    'mailerb' => $mailerb,
    'db' => $db,
    'urlManager' => $urlManager,
],
'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
