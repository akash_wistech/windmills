<?php

return [
    'baseDir' => '/bulk-mailing/',
    'appName' => 'Bulk Mail System',
    'fromAppName' => 'Mailing System',
    'supportEmail' => 'akash@wistech.biz',

    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'bouncedEmail' => 'bounced@example.com',

    'companyName' => 'Mailing',

    'temp_abs_path' => '',
  'uploads_abs_path' => dirname(dirname(__DIR__)) . '/uploads/template/',
  'uploads_rel_path' => '/uploads/template/',
  'thumbnail_abs_path' => dirname(dirname(__DIR__)) . '/uploads/thumbnails/',
  'thumbnail_rel_path' => '/uploads/thumbnails/',
  'static_abs_path' => dirname(dirname(__DIR__)) . '/uploads/static/',
  'static_rel_path' => '/uploads/static/',
  'bodyClasses' => [],
];
