<?php
date_default_timezone_set('Asia/Dubai');

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$mailer = require __DIR__ . '/mailer.php';
$urlManager = require __DIR__ . '/urlmanager.php';
$logs = require __DIR__ . '/logs.php';

$config = [
  'id' => 'yii2MailingSystemConsole',
  'basePath' => dirname(__DIR__),
  'bootstrap' => ['log'],
  'controllerNamespace' => 'app\commands',
  'aliases' => [
    '@bower' => '@vendor/bower-asset',
    '@npm'   => '@vendor/npm-asset',
    '@tests' => '@app/tests',
  ],
  'components' => [
    'formatter' => [
      'class' => 'yii\i18n\Formatter',
      'thousandSeparator' => ',',
      'dateFormat' => 'php:D, jS M Y',
      'datetimeFormat' => 'php:j M, Y / h:i a',
      'timeFormat' => 'php:h:i a',
      'timeZone' => 'Asia/Dubai',
      'defaultTimeZone' => 'Asia/Dubai',
    ],
    'jsFunctions' => [
      'class' => 'app\components\helpers\JavaScriptFunctions',
    ],
    'statsFunctions' => [
      'class' => 'app\components\helpers\StatsFunctions',
    ],
    'appHelperFunctions' => [
      'class' => 'app\components\helpers\AppHelperFunction',
    ],
    'menuHelperFunction' => [
      'class' => 'app\components\helpers\MenuHelperFunction',
    ],
    'activityHelper' => [
      'class' => 'app\components\helpers\ActivityHelper',
    ],
    'notificationHelper' => [
      'class' => 'app\components\helpers\NotificationHelper',
    ],
    'helperFunctions' => [
      'class' => 'app\components\helpers\HelperFunctions',
    ],
    'fileHelperFunctions' => [
      'class' => 'app\components\helpers\FileHelperFunctions',
    ],
    'inputHelperFunctions' => [
      'class' => 'app\components\helpers\InputHelperFunctions',
    ],
    'cache' => [
      'class' => 'yii\caching\FileCache',
    ],
    'log' => $logs,
    'db' => $db,
    'mailer' => $mailer,
    'urlManager' => $urlManager,
  ],
  'params' => $params,
];

if (YII_ENV_DEV) {
  // configuration adjustments for 'dev' environment
  $config['bootstrap'][] = 'gii';
  $config['modules']['gii'] = [
    'class' => 'yii\gii\Module',
  ];
}

return $config;
