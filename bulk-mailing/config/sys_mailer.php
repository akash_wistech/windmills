<?php

return [
  'class' => 'app\components\helpers\MyMailer',
  'testmode' => false,
  'useFileTransport' => true,
];
