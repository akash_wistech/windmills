<?php

return [
  'enablePrettyUrl' => true,
  'showScriptName' => false,
  'baseUrl' => 'http://local.windmills/bulk-mailing/',
  'rules' => [
    '' => 'site/index',
    'campaign-image' => 'file-manager/image',
    'save-mosiaco-template' => 'template/save-html',
    '<controller:\w+>/<id:\d+>' => '<controller>/view',
    '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
    '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
  ],
];
