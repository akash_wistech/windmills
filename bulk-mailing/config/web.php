<?php
date_default_timezone_set('Asia/Dubai');

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$sysMailer = require __DIR__ . '/sys_mailer.php';
$mailer = require __DIR__ . '/mailer.php';
$urlManager = require __DIR__ . '/urlmanager.php';
$logs = require __DIR__ . '/logs.php';

$config = [
  'id' => 'yii2MailingSystem',
  'timezone' => 'Asia/Dubai',
  'language' => 'en',
  'basePath' => dirname(__DIR__),
  'bootstrap' => ['log'],
  'aliases' => [
    '@bower' => '@vendor/bower-asset',
    '@npm'   => '@vendor/npm-asset',
  ],
  'components' => [
    'formatter' => [
      'class' => 'yii\i18n\Formatter',
      'thousandSeparator' => ',',
      'dateFormat' => 'php:D, jS M Y',
      'datetimeFormat' => 'php:j M, Y / h:i a',
      'timeFormat' => 'php:h:i a',
      'timeZone' => 'Asia/Dubai',
      'defaultTimeZone' => 'Asia/Dubai',
    ],
    'jsFunctions' => [
      'class' => 'app\components\helpers\JavaScriptFunctions',
    ],
    'statsFunctions' => [
      'class' => 'app\components\helpers\StatsFunctions',
    ],
    'appHelperFunctions' => [
      'class' => 'app\components\helpers\AppHelperFunction',
    ],
    'menuHelperFunction' => [
      'class' => 'app\components\helpers\MenuHelperFunction',
    ],
    'activityHelper' => [
      'class' => 'app\components\helpers\ActivityHelper',
    ],
    'notificationHelper' => [
      'class' => 'app\components\helpers\NotificationHelper',
    ],
    'helperFunctions' => [
      'class' => 'app\components\helpers\HelperFunctions',
    ],
    'fileHelperFunctions' => [
      'class' => 'app\components\helpers\FileHelperFunctions',
    ],
    'inputHelperFunctions' => [
      'class' => 'app\components\helpers\InputHelperFunctions',
    ],
    'session' => [
      'name' => 'PHPY2MAILSYSSESSID',
    ],
    'request' => [
      // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
      'cookieValidationKey' => 'ko5_uL_evOEjeW8PyGbizgc1NeI9Sh-a',
    ],
    'cache' => [
      'class' => 'yii\caching\FileCache',
    ],
    'user' => [
      'identityClass' => 'app\models\User',
      'enableAutoLogin' => true,
    ],
    'errorHandler' => [
      'errorAction' => 'site/error',
    ],
   /* 's3bucket' => [
        'class' => 'frostealth\yii2\aws\s3\Service',
        'credentials' => [ // Aws\Credentials\CredentialsInterface|array|callable
          'key' => 'AKIA34O2PWQV55LA7SVH',
          'secret' => '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',
        ],
        'region' => 'ap-southeast-1',
        'defaultBucket' => 'manal-bucket-latest',
        'defaultAcl' => 'public-read',
    ],*/
      's3bucket' => [
          'class' => 'frostealth\yii2\aws\s3\Service',
          'credentials' => [ // Aws\Credentials\CredentialsInterface|array|callable
              'key' => 'AKIA542C2DWGGLEMMMWC',
              'secret' => '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',
          ],
          'region' => 'eu-central-1',
          'defaultBucket' => 'maxima-media',
          'defaultAcl' => 'public-read',
      ],
     /* 's3bucket' => [
          'class' =>  \frostealth\yii2\aws\s3\Storage::className(),
          'region' => 'eu-central-1',
          'credentials' => [ // Aws\Credentials\CredentialsInterface|array|callable
              'key' => 'AKIA542C2DWGGLEMMMWC',
              'secret' => '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',
          ],
          'bucket' => 'maxima-media',
          'cdnHostname' => false,
          'defaultAcl' => \frostealth\yii2\aws\s3\Storage::ACL_PUBLIC_READ,
          'debug' => false, // bool|array
      ],*/
    'sysMailer' => $sysMailer,
    'mailer' => $mailer,
    'log' => $logs,
    'db' => $db,
    'urlManager' => $urlManager,
    'assetManager' => [
      'appendTimestamp' => true,
      'bundles' => [
        // 'yii\web\JqueryAsset' => [
        //   'sourcePath' => null,   // do not publish the bundle
        //   'baseUrl' => '@web',
        //   'js' => [
        //     'themes/ubold/assets/js/jquery.min.js',
        //   ]
        // ],
        'yii\bootstrap\BootstrapAsset' => [
          'sourcePath' => null,   // do not publish the bundle
          'baseUrl' => '@web',
          'css' => [
            'css/adminlte.css',
          ],
          'js' => [
            'plugins/bootstrap/js/bootstrap.bundle.min.js',
          ]
        ],
      ],
    ],
    'view' => [
      'class' => '\rmrevin\yii\minify\View',
      'enableMinify' => false,
      'concatCss' => true, // concatenate css
      'minifyCss' => true, // minificate css
      'concatJs' => true, // concatenate js
      'minifyJs' => true, // minificate js
      'minifyOutput' => true, // minificate result html page
      'webPath' => '@web', // path alias to web base
      'basePath' => '@webroot', // path alias to web base
      'minifyPath' => '@webroot/minify', // path alias to save minify result
      'jsPosition' => [ \yii\web\View::POS_END ], // positions of js files to be minified
      'forceCharset' => 'UTF-8', // charset forcibly assign, otherwise will use all of the files found charset
      'expandImports' => true, // whether to change @import on content
      'compressOptions' => ['extra' => true], // options for compress
      'excludeFiles' => [
        'jquery.js',
      ],
      'excludeBundles' => [
        \app\assets\TinyMceAsset::class,
      ],
    ]
  ],
  'params' => $params,
];

if (YII_ENV_DEV) {
  // configuration adjustments for 'dev' environment
  //$config['bootstrap'][] = 'debug';
  //$config['modules']['debug'] = [
    //'class' => 'yii\debug\Module',
    // uncomment the following to add your IP if you are not connecting from localhost.
    //'allowedIPs' => ['127.0.0.1', '::1'],
  //];

  //$config['bootstrap'][] = 'gii';
  //$config['modules']['gii'] = [
    //'class' => 'yii\gii\Module',
    // uncomment the following to add your IP if you are not connecting from localhost.
    //'allowedIPs' => ['127.0.0.1', '::1'],
  //];
}

return $config;
