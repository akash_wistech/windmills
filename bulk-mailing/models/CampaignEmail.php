<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
* This is the model class for table "{{%campaign_email}}".
*
* @property integer $campaign_id
* @property integer $list_id
* @property string $module_type
* @property integer $module_id
* @property integer $smtp_id
* @property string $email
* @property integer $status
* @property string $distributed_at
* @property integer $delivered
* @property integer $open
* @property integer $click
* @property integer $soft_bounced
* @property integer $hard_bounced
* @property integer $spam_complain
* @property string $created_at
* @property string $updated_at
*/
class CampaignEmail extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%campaign_email}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['campaign_id', 'module_id', 'smtp_id', 'email'], 'required'],
      [[
        'campaign_id','list_id','module_id','smtp_id','status','delivered','open','click',
        'soft_bounced','hard_bounced','spam_complain'
      ], 'integer'],
      [['module_type', 'email', 'distributed_at'], 'string'],
      [['created_at', 'updated_at'], 'safe'],
    ];
  }

  public static function primaryKey()
  {
  	return ['campaign_id','email'];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
    ];
  }

  public function getEmployee()
  {
    return Employee::findOne(['id'=>$this->module_id]);
  }

  public function getProspect()
  {
    return Prospect::findOne(['id'=>$this->module_id]);
  }

  public function getFullname()
  {
    $fullname='';
    if($this->module_type=='employee'){
      $fullname=$this->employee->name;
    }elseif($this->module_type=='prospect'){
      $fullname=$this->prospect->name;
    }
    return $fullname;
  }

  public function getEmail()
  {
    $email='';
    if($this->module_type=='employee'){
      $email=$this->employee->email;
    }elseif($this->module_type=='prospect'){
      $email=$this->prospect->email;
    }
    return $email;
  }
}
