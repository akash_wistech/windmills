<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* SettingForm is the model behind the setting form.
*/
class SettingForm extends Model
{
  public $cust_perm_id; //Customer Permission Group
  public $currency_sign,$cron_server_diff;
  public $country_list_id,$zone_list_id,$role_list_id,$job_title_list_id,$department_list_id;


  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [[
        'cust_perm_id','currency_sign','role_list_id','job_title_list_id','department_list_id'
      ], 'required'],
      [[
        'cust_perm_id','country_list_id','zone_list_id','role_list_id','job_title_list_id','department_list_id'
      ], 'integer'],
      [[
        'currency_sign','cron_server_diff',
      ], 'string'],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'cust_perm_id' => 'Customer Permission Group',
      'role_list_id' => 'Roles List',
      'job_title_list_id' => 'Job Titles List',
      'country_list_id' => 'Countries List',
      'zone_list_id' => 'State / Province List',
      'department_list_id' => 'Departments List',
      'currency_sign' => 'Currency Sign',
      'cron_server_diff' => 'Server Difference (Cron)',
    ];
  }

  /**
  * save the settings data.
  */
  public function save()
  {
    if ($this->validate()) {
      $post=Yii::$app->request->post();
      foreach($post['SettingForm'] as $key=>$val){
        $setting=Setting::find()->where("config_name='$key'")->one();
        if($setting==null){
          $setting=new Setting;
          $setting->config_name=$key;
        }
        $setting->config_value=$val;
        $setting->save();
      }
      return true;
    } else {
      return false;
    }
  }
}
