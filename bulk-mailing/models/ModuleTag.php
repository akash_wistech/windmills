<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%module_tag}}".
*
* @property string $module_type
* @property integer $module_id
* @property string $tag_id
*/
class ModuleTag extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%module_tag}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['module_type','module_id','tag_id'], 'required'],
      [['module_id','tag_id'], 'integer'],
      [['module_type'], 'string'],
    ];
  }

  public static function primaryKey()
  {
  	return ['module_type','module_id','tag_id'];
  }
}
