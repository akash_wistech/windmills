<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\components\models\ActiveRecordTsOnly;

/**
* This is the model class for table "{{%cron_job_syncapi}}".
*
* @property integer $id
* @property integer $api_source_id
* @property string $started_at
* @property string $finished_at
* @property string $next_page
* @property integer $status
*/
class CronJobSyncApi extends ActiveRecordTsOnly
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%cron_job_syncapi}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['api_source_id'], 'required'],
      ['next_page','string'],
      [['started_at','finished_at','created_at','updated_at'], 'safe'],
      [['api_source_id','total_records','done_records','status'],'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'api_source_id' => Yii::t('app', 'Api Source'),
      'started_at' => Yii::t('app', 'Started'),
      'finished_at' => Yii::t('app', 'Finished'),
      'next_page' => Yii::t('app', 'Last saved page'),
      'status' => Yii::t('app', 'Status'),
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getApiSource()
  {
    return $this->hasOne(ApiSource::className(), ['id' => 'api_source_id']);
  }
}
