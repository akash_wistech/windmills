<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Notification;
use yii\db\Expression;

/**
* NotificationSearch represents the model behind the search form about `app\models\Notification`.
*/
class NotificationSearch extends Notification
{
	public $pageSize, $sort_by;
	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['id', 'target_id', 'sender_id', 'created_by', 'updated_by', 'parent_id'], 'integer'],
			[['category', 'action_type', 'created_at', 'updated_at','groups'], 'safe'],
		];
	}

	/**
	* @inheritdoc
	*/
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	* Creates data provider instance with search query applied
	*
	* @param array $params
	*
	* @return ActiveDataProvider
	*/
	public function search($params)
	{
		$query = $this->generateQuery($params);

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id'=>SORT_DESC],]
		]);

		return $dataProvider;
	}

	public function generateQuery($params)
	{
		$this->load($params);
		$query = Notification::find()
		->innerJoin(NotificationReceipient::tableName(),NotificationReceipient::tableName().".notification_id=".Notification::tableName().".id");

		$query->andFilterWhere([
			'receipient_id' => Yii::$app->user->identity->id,
			'category' => $this->category,
			'action_type' => $this->action_type,
			'target_id' => $this->target_id,
			'sender_id' => $this->sender_id,
			'created_at' => $this->created_at,
			'created_by' => $this->created_by,
			'parent_id' => $this->parent_id,
		]);
    $query->andWhere([
        'is','deleted_at',new Expression('null')
    ]);
		return $query;
	}
}
