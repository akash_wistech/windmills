<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;
use yii\db\Expression;

/**
* StaffSearch represents the model behind the search form of `app\models\User`.
*/
class StaffSearch extends User
{
  public $permission_group,$name,$mobile,$job_title,$department,$pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','permission_group','status','created_by','updated_by','pageSize'],'integer'],
      [['name','email','mobile','job_title','department'],'string'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = User::find()
    ->select([
      User::tableName().'.id',
      'permission_group_id',
      'permission_group'=>AdminGroup::tableName().'.title',
      'job_title'=>'jt.title',
      'department'=>'dept.title',
      'image',
      'name'=>'CONCAT(firstname," ",lastname)',
      'email',
      UserProfileInfo::tableName().'.mobile',
      User::tableName().'.status',
    ])
    ->leftJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().'.user_id='.User::tableName().'.id')
    ->leftJoin(AdminGroup::tableName(),AdminGroup::tableName().'.id='.User::tableName().'.permission_group_id')
    ->leftJoin(PredefinedList::tableName().' as jt','jt.id='.UserProfileInfo::tableName().'.job_title_id')
    ->leftJoin(PredefinedList::tableName().' as dept','dept.id='.UserProfileInfo::tableName().'.department_id')
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
      'sort' =>false,
    ]);

    // grid filtering conditions
    $query->andFilterWhere([
      User::tableName().'.id' => $this->id,
      User::tableName().'.company_id' => $this->company_id,
      'user_type' => [10,1],
      'permission_group_id' => $this->permission_group,
      User::tableName().'.status' => $this->status,
    ]);
    $query->andWhere([
        'is',User::tableName().'.deleted_at',new Expression('null')
    ]);

    $query->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
    ->andFilterWhere(['like','email',$this->email])
    ->andFilterWhere(['like','jt.title',$this->job_title])
    ->andFilterWhere(['like','dept.title',$this->department])
    ->andFilterWhere(['like',UserProfileInfo::tableName().'.mobile',$this->mobile]);

    return $dataProvider;
  }
}
