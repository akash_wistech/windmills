<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\components\models\ActiveRecordFull;

/**
* This is the model class for table "{{%template}}".
*
* @property integer $id
* @property integer $company_id
* @property string $title
* @property string $descp
* @property string $template_css
* @property string $email_body
* @property string $sms_body
* @property integer $status
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
* @property string $deleted_at
* @property integer $deleted_by
*/
class Template extends ActiveRecordFull
{
  public $module_type,$module_ids;
  /**
  * @inheritdoc
  * @return string of table name
  */
  public static function tableName()
  {
    return '{{%template}}';
  }

  /**
  * @inheritdoc
  * @return array of rules
  */
  public function rules()
  {
    return [
      [['company_id','title'], 'required'],
      [['status','created_by','updated_by','deleted_by'], 'integer'],
      [['title','descp','template_css','email_body','sms_body'], 'string'],
      [['created_at','updated_at','deleted_at'], 'safe'],
      [['module_type','module_ids'],'string'],
    ];
  }

  /**
  * @inheritdoc
  * @return array of attribute lables
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'title' => Yii::t('app', 'Name'),
      'descp' => Yii::t('app', 'Template Body'),
      'template_css' => Yii::t('app', 'Template Css'),
      'email_body' => Yii::t('app', 'Email Body'),
      'sms_body' => Yii::t('app', 'Sms Body'),
      'status' => Yii::t('app', 'Status'),
    ];
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecTitle()
  {
    return $this->title;
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecType()
  {
    return 'Template';
  }
}
