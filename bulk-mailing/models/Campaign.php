<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "campaign".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $subject
 * @property integer $from_id
 * @property string $reply_to
 * @property integer $template_id
 * @property string $descp
 * @property string $distribution_date
 * @property integer $per_min_email
 * @property integer $status
 * @property integer $sent
 * @property string $sent_date_time
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class Campaign extends ActiveRecordFull
{
  public $list_id,$smtp_id,$attachment_file;
    /**
     * @inheritdoc
     * @return string of table name
     */
    public static function tableName()
    {
        return 'campaign';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['company_id','subject','smtp_id'], 'required'],
            [['from_id','template_id','per_min_email','status','sent','created_by','updated_by','deleted_by'], 'integer'],
            [['subject','reply_to','descp','distribution_date'], 'string'],
            [['sent_date_time','created_at','updated_at','deleted_at','sms_text'], 'safe'],
            [['list_id','smtp_id'], 'each', 'rule'=>['integer']],
            [['attachment_file'], 'each', 'rule'=>['string']],
        ];
    }

    /**
     * @inheritdoc
     * @return array of attribute lables
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'subject' => Yii::t('app', 'Subject'),
            'from_id' => Yii::t('app', 'Send From'),
            'reply_to' => Yii::t('app', 'Reply to email'),
            'template_id' => Yii::t('app', 'Template'),
            'list_id' => Yii::t('app', 'List'),
            'smtp_id' => Yii::t('app', 'Smtps to Use'),
            'descp' => Yii::t('app', 'Message'),
            'distribution_date' => Yii::t('app', 'Distribution Date'),
            'per_min_email' => Yii::t('app', 'Per Min Limit'),
            'delivered' => Yii::t('app', 'Delivered'),
            'open' => Yii::t('app', 'Opened'),
            'click' => Yii::t('app', 'Clicked'),
            'soft_bounced' => Yii::t('app', 'Soft Bounced'),
            'hard_bounced' => Yii::t('app', 'Bounced'),
            'spam_complain' => Yii::t('app', 'Spam Complaint'),
        ];
    }

    /**
    * returns main title/name of row for status and delete loging
    */
    public function getRecTitle()
    {
      return $this->subject;
    }

    /**
    * returns main title/name of row for status and delete loging
    */
    public function getRecType()
    {
      return 'Campaign';
    }

  /**
   * @inheritdoc
   */
  public function afterSave($insert, $changedAttributes)
  {
    if($this->list_id!=null){
      CampaignList::deleteAll(['and',['campaign_id'=>$this->id],['not in','list_id',$this->list_id]]);
      foreach($this->list_id as $key=>$val){
				$listItem=CampaignList::find()->where(['campaign_id'=>$this->id,'list_id'=>$val])->one();
				if($listItem==null){
					$listItem=new CampaignList;
					$listItem->campaign_id=$this->id;
					$listItem->list_id=$val;
					$listItem->save();
				}
			}
		}
    if($this->smtp_id!=null){
      CampaignSmtp::deleteAll(['and',['campaign_id'=>$this->id],['not in','smtp_id',$this->smtp_id]]);
      foreach($this->smtp_id as $key=>$val){
				$listItem=CampaignSmtp::find()->where(['campaign_id'=>$this->id,'smtp_id'=>$val])->one();
				if($listItem==null){
					$listItem=new CampaignSmtp;
					$listItem->campaign_id=$this->id;
					$listItem->smtp_id=$val;
					$listItem->save();
				}
			}
		}
    //Saving Attachment files
		if($this->attachment_file!=null){
			$n=0;
			foreach($this->attachment_file as $key=>$val){
				if($val!=null && file_exists(Yii::$app->fileHelperFunctions->tempImagePath['abs'].$val)){
					$tmpfile=pathinfo($val);
					$fileTitle=$tmpfile['basename'];
					$fileName=Yii::$app->fileHelperFunctions->generateName.".".$tmpfile['extension'];
          $bucketUrl = Yii::$app->fileHelperFunctions->moveLocalToAws(Yii::$app->fileHelperFunctions->tempImagePath['abs'],$val,'campaign/',$fileName);
          if($bucketUrl!=''){
  					$attRow=new CampaignAttachment;
  					$attRow->campaign_id=$this->id;
  					$attRow->file_title=$fileTitle;
  					$attRow->file_name=$bucketUrl;
  					$attRow->save();
          }
				}
				$n++;
			}
		}
    parent::afterSave($insert, $changedAttributes);
  }

	/**
	* Get selectec lists
	* @return \yii\db\ActiveQuery
	*/
	public function getListIdz()
	{
		return CampaignList::find()->select(['list_id'])->where(['campaign_id'=>$this->id])->asArray()->all();
	}

	/**
	* Get selectec lists
	* @return \yii\db\ActiveQuery
	*/
	public function getSmtpIdz()
	{
		return CampaignSmtp::find()->select(['smtp_id'])->where(['campaign_id'=>$this->id])->asArray()->all();
	}

	/**
	* @return array attachments
	*/
	public function getAttachmentInfo()
	{
		$arrFiles=[];
		$results=CampaignAttachment::find()->where(['and',['campaign_id'=>$this->id],['is','deleted_at',new Expression('null')]])->asArray()->all();
		if($results!=null){
			$otherFilesArr=['doc','docx','xls','xlsx','pdf'];
			foreach($results as $attachment){
				if($attachment['file_name']!=null){
					$fpInfo=pathinfo($attachment['file_name']);
					if(in_array($fpInfo['extension'],$otherFilesArr)){
						$icon='images/icons/'.$fpInfo['extension'].'.jpg';
					}else{
						$icon=$attachment['file_name'];
					}
					$arrFiles[]=[
						'campaign_id'=>$attachment['campaign_id'],
						'file_title'=>$attachment['file_title'],
						'file_name'=>$attachment['file_name'],
						'iconPath'=>$icon,
						'link'=>$attachment['file_name'],
					];
				}
			}
		}
		return $arrFiles;
	}

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getSentBy()
  {
    return $this->hasOne(User::className(), ['id' => 'from_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getTemplate()
  {
    return $this->hasOne(Template::className(), ['id' => 'template_id']);
  }

  /**
   * total unique opened
   * @return integer
   */
  public function getUniqueOpened()
  {
    return CampaignEmail::find()->where(['and',['campaign_id'=>$this->id],['>','open',1]])->count('email');
  }

  /**
   * total unique clicked
   * @return integer
   */
  public function getUniqueClicked()
  {
    return CampaignEmail::find()->where(['and',['campaign_id'=>$this->id],['>','click',1]])->count('email');
  }
}
