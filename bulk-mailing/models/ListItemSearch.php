<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ListItem;
use app\models\User;
use yii\db\Expression;

/**
 * ListItemSearch represents the model behind the search form about `app\models\ListItem`.
 */
class ListItemSearch extends ListItem
{
  public $contact_name,$email;
    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['list_id','module_id'], 'integer'],
            [['module_type','contact_name','email'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     * @return object of model scenarios
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params is for search model
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ListItem::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'list_id' => $this->list_id,
            'module_type' => $this->module_type,
        ]);
        if($this->contact_name!=null){
          $subQueryMatchingEmployees=User::find()
          ->select(['id'])
          ->where([
            'and',
            ['user_type'=>[0,1]],
            ['is','deleted_at',new Expression('null')],
            [
              'or',
              ['like','firstname',$this->contact_name],
              ['like','lastname',$this->contact_name],
            ],
            ['like','email',$this->email]
          ]);
          $subQueryMatchingProspects=Prospect::find()
          ->select(['id'])
          ->where([
            'and',
            ['is','deleted_at',new Expression('null')],
            [
              'or',
              ['like','firstname',$this->contact_name],
              ['like','lastname',$this->contact_name],
            ],
            ['like','email',$this->email]
          ]);
          $query->andFilterWhere([
            'or',
            ['module_type'=>'prospect','module_id' => $subQueryMatchingProspects],
            ['module_type'=>'employee','module_id' => $subQueryMatchingEmployees],
            //['module_type'=>'tenant','module_id' => $subQueryMatchingTenants],
          ]);
        }

        //$query->andFilterWhere(['like', 'title', $this->contact_name]);

        return $dataProvider;
    }
}
