<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApiSource;
use yii\db\Expression;

/**
* ApiSourceSearch represents the model behind the search form about `app\models\ApiSource`.
*/
class ApiSourceSearch extends ApiSource
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','company_id','status','pageSize'], 'integer'],
      [['title','url','sync_frequency'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = ApiSource::find()
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
    ]);

    $query->andFilterWhere([
      'id' => $this->id,
      'company_id' => $this->company_id,
      'sync_frequency' => $this->sync_frequency,
      'status' => $this->status,
    ]);
    $query->andWhere([
        'is','deleted_at',new Expression('null')
    ]);

    $query->andFilterWhere(['like', 'title', $this->title])
    ->andFilterWhere(['like', 'url', $this->url]);

    return $dataProvider;
  }
}
