<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;
use yii\db\Expression;

/**
 * This is the model class for table "{{%custom_field}}".
 *
 * @property int $id
 * @property int $company_id
 * @property string $input_type
 * @property int $show_in_grid
 * @property string $title
 * @property string $short_name
 * @property string $colum_width
 * @property string $hint_text
 * @property string $descp
 * @property string $autocomplete_text
 * @property string $predefined_list_id
 * @property int $is_required
 * @property int $selection_type
 * @property int $sort_order
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property string $deleted_at
 * @property int $deleted_by
 */
class CustomField extends ActiveRecordFull
{
  public $for_modules,$suboption_title;
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return '{{%custom_field}}';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['company_id','input_type','title','for_modules'],'required'],
      [['title'],'filter','filter' => 'trim'],
      [['created_at','updated_at','deleted_at'],'safe'],
      [[
        'show_in_grid','autocomplete_text','is_required','selection_type','colum_width','sort_order','status','created_by','updated_by','deleted_by'
      ],'integer'],
      [[
        'input_type','title','short_name','hint_text','descp','predefined_list_id'
      ],'string'],
      [['title'], 'uniqueTitle'],
      [['suboption_title','for_modules'], 'each', 'rule'=>['string']],
    ];
  }

  /**
   * Validates the dates.
   * This method serves as the inline validation for dates.
   *
   * @param string $attribute the attribute currently being validated
   * @param array $params the additional name-value pairs given in the rule
   */
  public function uniqueTitle($attribute, $params)
  {
    if (!$this->hasErrors()) {
      $already=self::find()->where(['and',['title'=>$this->title],['is','deleted_at',new Expression('null')]])->andFilterWhere(['!=','id',$this->id]);
      if($already->exists()){
        $this->addError($attribute, $this->title.' already exists');
        return false;
      }
    }
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'show_in_grid' => Yii::t('app', 'Show In Grid'),
      'input_type' => Yii::t('app', 'Input Type'),
      'title' => Yii::t('app', 'Name'),
      'hint_text' => Yii::t('app', 'Hint / Info Text'),
      'descp' => Yii::t('app', 'Description'),
      'predefined_list_id' => Yii::t('app', 'Predefined list'),
      'status' => Yii::t('app', 'Status'),
      'created_at' => Yii::t('app', 'Created'),
      'created_by' => Yii::t('app', 'Created By'),
      'updated_at' => Yii::t('app', 'Updated'),
      'updated_by' => Yii::t('app', 'Updated By'),
      'deleted_at' => Yii::t('app', 'Deleted At'),
      'deleted_by' => Yii::t('app', 'Deleted By'),
			'for_modules' => Yii::t('app', 'Assigned to Modules'),
    ];
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecTitle()
  {
    return $this->title;
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecType()
  {
		return 'Custom Field';
  }

  public function getFieldSubOptions()
  {
    return $this->hasMany(CustomFieldSubOption::className(), ['custom_field_id' => 'id'])->andWhere(['is','deleted_at',new Expression('null')]);
  }

  /**
   * @inheritdoc
   */
	public function afterSave($insert, $changedAttributes)
	{
		//Saving Assigned To
		if($this->for_modules!=null){
			CustomFieldModule::deleteAll(['and',['custom_field_id'=>$this->id],['not in','module_type',$this->for_modules]]);
			foreach($this->for_modules as $key=>$val){
				$managerRow=CustomFieldModule::find()->where(['custom_field_id'=>$this->id,'module_type'=>$val])->one();
				if($managerRow==null){
					$managerRow=new CustomFieldModule;
					$managerRow->custom_field_id=$this->id;
					$managerRow->module_type=$val;
					$managerRow->save();
				}
			}
		}
    if($this->suboption_title!=null){
      CustomFieldSubOption::deleteAll(['and', 'custom_field_id = :custom_field_id', ['not in', 'title', $this->suboption_title]], [':custom_field_id' => $this->id]);
      $n=1;
      foreach($this->suboption_title as $key=>$val){
				$childRow=CustomFieldSubOption::find()->where(['custom_field_id'=>$this->id,'title'=>$val]);
				if(!$childRow->exists()){
					$childRow=new CustomFieldSubOption;
					$childRow->custom_field_id=$this->id;
					$childRow->title=$val;
					$childRow->sort_order=$n;
  				$childRow->save();
				}
			}
    }
		parent::afterSave($insert, $changedAttributes);
	}

  /**
  * @return \yii\db\ActiveQuery
  */
	public function getSelectedModules()
	{
		return CustomFieldModule::find()->select(['module_type'])->where(['custom_field_id'=>$this->id])->asArray()->all();
	}
}
