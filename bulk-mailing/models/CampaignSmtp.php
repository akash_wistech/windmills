<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%campaign_smtp}}".
*
* @property integer $campaign_id
* @property integer $smtp_id
*/
class CampaignSmtp extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%campaign_smtp}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['campaign_id', 'smtp_id'], 'required'],
      [['campaign_id', 'smtp_id'], 'integer'],
    ];
  }

  public static function primaryKey()
  {
  	return ['campaign_id','smtp_id'];
  }
}
