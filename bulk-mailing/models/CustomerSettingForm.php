<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* CustomerSettingForm is the model behind the customer setting form.
*/
class CustomerSettingForm extends Model
{
  public $company_id;
  public $time_zone,$default_smtp;


  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [[
        'time_zone','default_smtp',
      ], 'required'],
       [[
        'default_smtp',
      //   'cust_perm_id','job_title_list_id','department_list_id'
       ], 'integer'],
      [[
        'time_zone',
      ], 'string'],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'time_zone' => 'Time Zone',
      'default_smtp' => 'Default Smtp',
    ];
  }

  /**
  * save the settings data.
  */
  public function save()
  {
    if ($this->validate()) {
      $post=Yii::$app->request->post();
      foreach($post['CustomerSettingForm'] as $key=>$val){
        $setting=CustomerSetting::find()->where(['company_id'=>$this->company_id,'config_name'=>$key])->one();
        if($setting==null){
          $setting=new CustomerSetting;
          $setting->company_id=$this->company_id;
          $setting->config_name=$key;
        }
        $setting->config_value=$val;
        $setting->save();
      }
      return true;
    } else {
      return false;
    }
  }
}
