<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AdminGroup;
use yii\db\Expression;

/**
* AdminGroupSearch represents the model behind the search form about `app\models\AdminGroup`.
*/
class AdminGroupSearch extends AdminGroup
{
  public $pageSize;

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id', 'status', 'created_by', 'updated_by', 'deleted_by','pageSize'], 'integer'],
      [['title', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);

    $query = AdminGroup::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
    ]);

    $query->andFilterWhere([
      'id' => $this->id,
      'status' => $this->status,
    ]);
    $query->andWhere([
        'is','deleted_at',new Expression('null')
    ]);

    $query->andFilterWhere(['like', 'title', $this->title]);

    return $dataProvider;
  }
}
