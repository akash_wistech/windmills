<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "{{%lists}}".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $title
 * @property integer $status
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 * @property string $deleted_at
 * @property integer $deleted_by
 */
class Lists extends ActiveRecordFull
{
  public $module_type,$module_ids;
    /**
     * @inheritdoc
     * @return string of table name
     */
    public static function tableName()
    {
        return '{{%lists}}';
    }

    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['company_id','title'], 'required'],
            [['status','created_by','updated_by','deleted_by'], 'integer'],
            [['title'], 'string'],
            [['created_at','updated_at','deleted_at'], 'safe'],
            [['module_type','module_ids'],'string'],
        ];
    }

    /**
     * @inheritdoc
     * @return array of attribute lables
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
    * returns main title/name of row for status and delete loging
    */
    public function getRecTitle()
    {
      return $this->title;
    }

    /**
    * returns main title/name of row for status and delete loging
    */
    public function getRecType()
    {
      return 'List';
    }

  /**
   * @inheritdoc
   */
  public function afterSave($insert, $changedAttributes)
  {
    if($this->module_ids!=null){
      $moduleIdz=explode(",",$this->module_ids);
      foreach($moduleIdz as $key=>$val){
				$listItem=ListItem::find()->where(['list_id'=>$this->id,'module_id'=>$val,'module_type'=>$this->module_type])->one();
				if($listItem==null){
					$listItem=new ListItem;
					$listItem->list_id=$this->id;
					$listItem->module_id=$val;
					$listItem->module_type=$this->module_type;
					$listItem->save();
				}
			}
		}
    parent::afterSave($insert, $changedAttributes);
  }

  public function getItemCount()
  {
    return ListItem::find()->where(['list_id'=>$this->id])->count('module_id');
  }
}
