<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%user_profile_info}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $department_id
 * @property int $job_title_id
 * @property string $background_info
 * @property int $lead_type
 * @property int $lead_source
 * @property string $lead_date
 * @property int $lead_score
 * @property string $expected_close_date
 * @property string $close_date
 * @property int $confidence
 * @property int $deal_status
 */
class UserProfileInfo extends ActiveRecord
{

  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return '{{%user_profile_info}}';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['user_id'], 'required'],
      [[
        'user_id','department_id','job_title_id','lead_type','lead_source','lead_score','confidence','deal_status',
      ], 'integer'],
      [[
        'lead_date','expected_close_date','close_date','mobile','phone1','phone2','address'
      ], 'string'],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_id' => Yii::t('app', 'User'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getUser()
  {
    return $this->hasOne(User::className(), ['id' => 'user_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getDepartment()
  {
    return $this->hasOne(PredefinedList::className(), ['id' => 'department_id']);
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getJobTitle()
  {
    return $this->hasOne(PredefinedList::className(), ['id' => 'job_title_id']);
  }
}
