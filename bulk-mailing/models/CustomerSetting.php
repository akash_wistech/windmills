<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%customer_setting}}".
*
* @property integer $id
* @property string $config_name
* @property string $config_value
*/
class CustomerSetting extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%customer_setting}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['company_id','config_name','config_value'],'required'],
      [['company_id'],'integer'],
      [['config_name','config_value'],'string'],
      [['config_name','config_value'],'trim'],
    ];
  }
}
