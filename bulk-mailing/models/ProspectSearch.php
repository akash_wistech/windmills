<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use app\models\Prospect;
use yii\db\Expression;

/**
* ProspectSearch represents the model behind the search form about `app\models\Prospect`.
*/
class ProspectSearch extends Prospect
{
  public $keyword,$input_field,$wf,$wfi,$cp_name,$country_name,$pageSize;

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id', 'company_id', 'status', 'wf', 'created_by', 'updated_by', 'trashed', 'trashed_by','pageSize'], 'integer'],
      [['keyword','firstname', 'lastname', 'cp_name','email','input_field', 'wfi', 'created_at', 'updated_at', 'trashed_at'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $query = $this->generateQuery($params);

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
      'sort' => $this->defaultSorting,
    ]);

    return $dataProvider;
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function searchMy($params)
  {
    $query = $this->generateQuery($params);

    //Check Listing type allowed
    if(Yii::$app->menuHelperFunction->getListingTypeByController($this->moduleTypeId)==2){
      $query->innerJoin(ModuleManager::tableName(),ModuleManager::tableName().".module_id=".Prospect::tableName().".id and ".ModuleManager::tableName().".module_type='".$this->moduleTypeId."'");
      $query->andFilterWhere([
        ModuleManager::tableName().'.staff_id' => Yii::$app->user->identity->id]);
    }

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
      'sort' => $this->defaultSorting,
    ]);

    return $dataProvider;
  }

  /**
  * Generates query for the search
  *
  * @param array $params
  *
  * @return ActiveRecord
  */
  public function generateQuery($params)
  {
    $this->load($params);

    $query = Prospect::find()
    ->select([
      Prospect::tableName().'.id',
      Prospect::tableName().'.company_id',
      'cp_name'=>'CONCAT('.Prospect::tableName().'.firstname," ",'.Prospect::tableName().'.lastname)',
      Prospect::tableName().'.email',
      Prospect::tableName().'.status',
      Prospect::tableName().'.created_at',
    ])
    ->asArray();

    $query->andFilterWhere([
      Prospect::tableName().'.id' => $this->id,
      Prospect::tableName().'.company_id' => $this->company_id,
      Prospect::tableName().'.status' => $this->status,
    ]);
    $query->andWhere([
        'is','deleted_at',new Expression('null')
    ]);

    if(
      isset($_GET['columns']) &&
      isset($_GET['columns'][6]) &&
      isset($_GET['columns'][6]['search']) &&
      isset($_GET['columns'][6]['search']['value']) &&
      $_GET['columns'][6]['search']['value']>0
    ){
      $query->andWhere(['status'=>$_GET['columns'][6]['search']['value']]);
    }

    $query
    ->andFilterWhere([
      'and',
      [
        'or',
        ['like',Prospect::tableName().'.firstname',$this->cp_name],
        ['like',Prospect::tableName().'.lastname',$this->cp_name]
      ],
      ['like',Prospect::tableName().'.email',$this->email],
    ]);
    if($this->input_field!=null){
      $inputFields=Yii::$app->inputHelperFunctions->getInputTypesByModule($this->moduleTypeId);
      if($inputFields!=null){
        $queryCFSCond = [];
        $queryCFSCond[] = 'and';
        foreach($inputFields as $inputField){
          if(
            (
              $inputField['input_type']=='textarea' ||
              $inputField['input_type']=='tinymce' ||
              $inputField['input_type']=='text' ||
              $inputField['input_type']=='qtyField' ||
              $inputField['input_type']=='numberinput' ||
              $inputField['input_type']=='websiteinput' ||
              $inputField['input_type']=='emailinput' ||
              $inputField['input_type']=='autocomplete' ||
              $inputField['input_type']=='radio' ||
              ($inputField['input_type']=='select' && $inputField['selection_type']==1)
            ) &&
            $this->input_field[$inputField['id']]!=''
          ){
            $subQueryCFData = CustomFieldData::find()
            ->select([CustomFieldData::tableName().'.module_id'])
            ->where([
              'and',
              [CustomFieldData::tableName().'.module_type'=>$this->moduleTypeId],
              [
                'or',
                ['like',CustomFieldData::tableName().'.input_value',$this->input_field[$inputField['id']]],
                ['like',CustomFieldData::tableName().'.input_org_value',$this->input_field[$inputField['id']]],
              ]
            ]);
            $queryCFSCond[]=[Prospect::tableName().'.id' => $subQueryCFData];
          }
          if(
            (
              $inputField['input_type']=='select' ||
              $inputField['input_type']=='checkbox'
            ) &&
            $inputField['selection_type']==2 &&
            $this->input_field[$inputField['id']]!=''
          ){
            $subQueryCFData = CustomFieldDataMultiple::find()
            ->select([CustomFieldDataMultiple::tableName().'.module_id'])
            ->where([
              'and',
              [CustomFieldDataMultiple::tableName().'.module_type'=>$this->moduleTypeId],
              ['like',CustomFieldDataMultiple::tableName().'.option_id_value',$this->input_field[$inputField['id']]]
            ]);
            $queryCFSCond[]=[Prospect::tableName().'.id' => $subQueryCFData];
          }
        }
        if($queryCFSCond!=null && count($queryCFSCond)>1){
          $query->andFilterWhere($queryCFSCond);
        }
      }
    }

    if($this->keyword!=null){
      $keyword = $this->keyword;
      $keywordSearchCond = [];
      $keywordSearchCond[] = 'or';
      $keywordSearchCond[] = ['like',Prospect::tableName().'.firstname',$keyword];
      $keywordSearchCond[] = ['like',Prospect::tableName().'.lastname',$keyword];
      $keywordSearchCond[] = ['like',Prospect::tableName().'.email',$keyword];

      //Tags Search
      $subQueryTags = Tags::find()
      ->select([Tags::tableName().'.id'])
      ->where(['like',Tags::tableName().'.title',$keyword]);
      $subQueryTagedIdz = ModuleTag::find()
      ->select([ModuleTag::tableName().'.module_id'])
      ->where([ModuleTag::tableName().'.module_type'=>$this->moduleTypeId,ModuleTag::tableName().'.tag_id'=>$subQueryTags]);

    //   $keywordSearchCond[] = ['id'=>$subQueryTagedIdz];

      //Custom Fields Search
      $gridViewColumns=Yii::$app->inputHelperFunctions->getGridViewColumns($this->moduleTypeId);
      if($gridViewColumns!=null){
        foreach($gridViewColumns as $gridViewColumn){
          if($gridViewColumn->input_type=='select' || $gridViewColumn->input_type=='checkbox' || $gridViewColumn->input_type=='radio'){
            $subQueryFromCustomFieldMultiData = CustomFieldDataMultiple::find()
            ->select([CustomFieldDataMultiple::tableName().'.module_id'])
            ->where([
              'and',
              [CustomFieldDataMultiple::tableName().'.module_type'=>$this->moduleTypeId],
              ['like',CustomFieldDataMultiple::tableName().'.option_id_value',$keyword]
            ]);
            $keywordSearchCond[] = ['id'=>$subQueryFromCustomFieldMultiData];
          }else{
            $subQueryFromCustomFieldData = CustomFieldData::find()
            ->select([CustomFieldData::tableName().'.module_id'])
            ->where([
              'and',
              [CustomFieldData::tableName().'.module_type'=>$this->moduleTypeId],
              [
                'or',
                ['like',CustomFieldData::tableName().'.input_value',$keyword],
                ['like',CustomFieldData::tableName().'.input_org_value',$keyword],
              ]
            ]);
            $keywordSearchCond[] = ['id'=>$subQueryFromCustomFieldData];
          }
        }
      }
      //For spaced keywords and double quoted words
      $query->andFilterWhere($keywordSearchCond);
    }

    return $query;
  }

  /**
  * Default Sorting Options
  *
  * @param array $params
  *
  * @return Array
  */
  public function getDefaultSorting()
  {
    return [
      'defaultOrder' => ['id'=>SORT_DESC],
      'attributes' => [
        'id',
        'cp_name' => [
          'asc' => [Prospect::tableName().'.firstname' => SORT_ASC, Prospect::tableName().'.lastname' => SORT_ASC],
          'desc' => [Prospect::tableName().'.firstname' => SORT_DESC, Prospect::tableName().'.lastname' => SORT_DESC],
        ],
        'created_at',
      ]
    ];
  }

  public function getProcess()
  {
    if (($model = Workflow::findOne($this->wf)) !== null) {
        return $model;
    }
    throw new NotFoundHttpException('The requested page does not exist.');
  }
}
