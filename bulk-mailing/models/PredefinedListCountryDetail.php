<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\SluggableBehavior;

/**
* This is the model class for table "{{%predefined_list_country_detail}}".
*
* @property integer $id
* @property string $slug
* @property string $iso_code_2
* @property string $iso_code_3
* @property string $phone_code
* @property string $phone_format
* @property string $mobile_format
* @property string $address_format
* @property integer $postcode_required
*/
class PredefinedListCountryDetail extends ActiveRecord
{
  public $title;
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%predefined_list_country_detail}}';
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'sluggable' => [
        'class' => SluggableBehavior::className(),
        'attribute' => 'title',
        'ensureUnique'=>true,
      ],
    ];
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id'], 'required'],
      [['id','postcode_required'], 'integer'],
      [['slug','iso_code_2','iso_code_3','phone_code','phone_format','mobile_format','address_format'], 'string'],
    ];
  }
}
