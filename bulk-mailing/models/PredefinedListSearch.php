<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PredefinedList;
use yii\db\Expression;

/**
* PredefinedListSearch represents the model behind the search form of `app\models\PredefinedList`.
*/
class PredefinedListSearch extends PredefinedList
{
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [[
        'id','parent','status','postcode_required','country_id','created_by','updated_by','deleted_by'
      ],'integer'],
      [[
        'title','other_name','descp','iso_code_2','iso_code_3','phone_code','phone_format',
        'mobile_format','address_format','code','created_at','updated_at','deleted_at'
      ],'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);

    $set_country_id=Yii::$app->appHelperFunctions->getSetting('country_list_id');
    $set_zone_id=Yii::$app->appHelperFunctions->getSetting('zone_list_id');

    $query = PredefinedList::find()
    ->select([
      PredefinedList::tableName().'.id',
      'parent',
      'title',
      'other_name',
      'status',
    ])
    ->asArray();

    //Country Additional Cols
    if($this->parent==$set_country_id){
      $query->innerJoin(PredefinedListCountryDetail::tableName().' cntry','cntry.id='.PredefinedList::tableName().'.id');
      $query->addSelect(['[[cntry]].[[iso_code_2]]','[[cntry]].[[iso_code_3]]','[[cntry]].[[phone_code]]']);
    }
    //Zone Additional Cols
    if($this->parent==$set_zone_id){
      $query->innerJoin(PredefinedListZoneDetail::tableName().' stz','[[stz]].[[id]]='.PredefinedList::tableName().'.[[id]]');
      $query->addSelect(['[[stz]].[[country_id]]','[[stz]].[[code]]']);
    }

    // add conditions that should always apply here

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'sort' => $this->defaultSorting,
    ]);

    // grid filtering conditions
    $query->andFilterWhere([
      PredefinedList::tableName().'.id' => $this->id,
      PredefinedList::tableName().'.parent' => $this->parent,
      PredefinedList::tableName().'.status' => $this->status,
      '[[stz]].country_id' => $this->country_id,
    ]);
    $query->andWhere([
      'is',PredefinedList::tableName().'.deleted_at',new Expression('null')
    ]);

    $query->andFilterWhere(['like', PredefinedList::tableName().'.title', $this->title])
    ->andFilterWhere(['like', PredefinedList::tableName().'.descp', $this->descp])
    ->andFilterWhere(['like', '[[cntry]].iso_code_2', $this->iso_code_2])
    ->andFilterWhere(['like', '[[cntry]].iso_code_3', $this->iso_code_3])
    ->andFilterWhere(['like', '[[cntry]].phone_code', $this->phone_code])
    ->andFilterWhere(['like', '[[stz]].code', $this->code]);

    return $dataProvider;
  }

  /**
  * Default Sorting Options
  *
  * @param array $params
  *
  * @return Array
  */
  public function getDefaultSorting()
  {
    return [
      'defaultOrder' => ['title'=>SORT_ASC],
      'attributes' => [
        'title',
        'other_name',
        'short_name',
        'iso_code_2',
        'iso_code_3',
        'phone_code',
        'country_id',
        'code',
        'status',
        'created_at',
      ]
    ];
  }
}
