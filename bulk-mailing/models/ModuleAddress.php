<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
* This is the model class for table "{{%module_address}}".
*
* @property string $id
* @property string $module_type
* @property integer $module_id
* @property integer $is_primary
* @property integer $country_id
* @property integer $zone_id
* @property string $city
* @property string $phone
* @property string $address
*/
class ModuleAddress extends ActiveRecordFull
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%module_address}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['module_type','module_id'], 'required'],
      [['module_id','is_primary','country_id','zone_id'], 'integer'],
      [['module_type','city','phone','address'], 'string'],
    ];
  }

	/**
	* returns main title/name of row for status and delete loging
	*/
	public function getRecTitle()
	{
		return 'Address';
	}

	/**
	* returns main title/name of row for status and delete loging
	*/
	public function getRecType()
	{
		return 'Address';
	}
}
