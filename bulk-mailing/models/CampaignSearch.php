<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Campaign;
use yii\db\Expression;

/**
 * CampaignSearch represents the model behind the search form about `app\models\Campaign`.
 */
class CampaignSearch extends Campaign
{
    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'from_id', 'status', 'sent'], 'integer'],
            [['subject','reply_to','descp','distribution_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     * @return object of model scenarios
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params is for search model
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Campaign::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'from_id' => $this->from_id,
            'template_id' => $this->template_id,
            'distribution_date' => $this->distribution_date,
            'status' => $this->status,
            'sent' => $this->sent,
        ]);
        $query->andWhere([
            'is','deleted_at',new Expression('null')
        ]);

        $query->andFilterWhere(['like', 'subject', $this->subject])
        ->andFilterWhere(['like', 'reply_to', $this->reply_to])
        ->andFilterWhere(['like', 'descp', $this->descp]);

        return $dataProvider;
    }
}
