<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "{{%notification_receipient}}".
*
* @property integer $id
* @property integer $notification_id
* @property integer $receipient_id
* @property integer $is_read
* @property string $read_at
*/
class NotificationReceipient extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%notification_receipient}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['notification_id', 'receipient_id'], 'required'],
      [['notification_id', 'receipient_id', 'is_read'], 'integer'],
      [['read_at'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'notification_id' => Yii::t('app', 'Nofitication'),
      'receipient_id' => Yii::t('app', 'Receipient'),
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getNotification()
  {
    return $this->hasOne(Notification::className(), ['id' => 'notification_id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getRecipient()
  {
    return $this->hasOne(User::className(), ['id' => 'receipient_id']);
  }
}
