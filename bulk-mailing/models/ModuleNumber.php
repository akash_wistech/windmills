<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%module_number}}".
*
* @property string $module_type
* @property integer $module_id
* @property string $phone
*/
class ModuleNumber extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%module_number}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['module_type','module_id','phone'], 'required'],
      [['module_id'], 'integer'],
      [['module_type','phone'], 'string'],
    ];
  }

  public static function primaryKey()
  {
  	return ['module_type','module_id','phone'];
  }
}
