<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;
use yii\db\Expression;

/**
 * This is the model class for table "{{%custom_field_sub_option}}".
 *
 * @property int $id
 * @property int $custom_field_id
 * @property string $title
 * @property int $sort_order
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 * @property int $deleted
 * @property string $deleted_at
 * @property int $deleted_by
 */
class CustomFieldSubOption extends ActiveRecordFull
{
  /**
   * @inheritdoc
   */
  public static function tableName()
  {
    return '{{%custom_field_sub_option}}';
  }

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      [['custom_field_id','title'],'required'],
      [['title'],'filter','filter' => 'trim'],
      [['created_at','updated_at','deleted_at'],'safe'],
      [['custom_field_id','sort_order','status','created_by','updated_by','deleted_by'],'integer'],
      [['title'],'string'],
      [['title'], 'uniqueTitle'],
    ];
  }

  /**
   * Validates the dates.
   * This method serves as the inline validation for dates.
   *
   * @param string $attribute the attribute currently being validated
   * @param array $params the additional name-value pairs given in the rule
   */
  public function uniqueTitle($attribute, $params)
  {
    if (!$this->hasErrors()) {
      $already=self::find()->where(['and',['custom_field_id'=>$this->custom_field_id,'title'=>$this->title],['is','deleted_at',new Expression('null')]]);
      if($already->exists()){
        $this->addError($attribute, 'Already exists');
        return false;
      }
    }
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'custom_field_id' => Yii::t('app', 'Custom Field'),
      'input_type' => Yii::t('app', 'Input Type'),
      'title' => Yii::t('app', 'Name'),
      'descp' => Yii::t('app', 'Description'),
      'status' => Yii::t('app', 'Status'),
      'created_at' => Yii::t('app', 'Created'),
      'created_by' => Yii::t('app', 'Created By'),
      'updated_at' => Yii::t('app', 'Updated'),
      'updated_by' => Yii::t('app', 'Updated By'),
      'deleted_at' => Yii::t('app', 'Deleted At'),
      'deleted_by' => Yii::t('app', 'Deleted By'),
    ];
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecTitle()
  {
    return $this->title;
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecType()
  {
		return 'Custom Field Sub Option';
  }
}
