<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%module_manager}}".
*
* @property string $module_type
* @property integer $module_id
* @property string $staff_id
*/
class ModuleManager extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%module_manager}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['module_type','module_id','staff_id'], 'required'],
      [['module_id','staff_id'], 'integer'],
      [['module_type'], 'string'],
    ];
  }

  public static function primaryKey()
  {
  	return ['module_type','module_id','staff_id'];
  }
}
