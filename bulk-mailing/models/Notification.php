<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;
use yii\helpers\Html;

/**
* This is the model class for table "{{%notification}}".
*
* @property integer $id
* @property string $category
* @property integer $parent_id
* @property string $action_type
* @property integer $target_id
* @property integer $sender_id
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
* @property string $deleted_at
* @property integer $deleted_by
*/
class Notification extends ActiveRecordFull
{
	public $receipient_id=[];
	public $showFullDetail=false;
	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%notification}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['action_type', 'sender_id', 'target_id', 'category'], 'required'],
			[['target_id', 'sender_id', 'parent_id'], 'integer'],
			[['category','action_type'], 'string'],
			[['receipient_id'], 'each', 'rule'=>['integer']]
		];
	}

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'category' =>  Yii::t('app', 'Category'),
			'action_type' => Yii::t('app', 'Type'),
			'target_id' => Yii::t('app', 'Target'),
			'detail' => Yii::t('app', 'Notification'),
			'parent_id' => Yii::t('app', 'Parent'),
		];
	}

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecTitle()
  {
    return $this->category;
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecType()
  {
		$this->category;
  }

	/**
	* @return \yii\db\ActiveQuery
	*/
	public function getRecipients()
	{
		return $this->hasMany(NotificationReceipient::className(), ['notification_id' => 'id']);
	}

	/**
	* @return \yii\db\ActiveQuery
	*/
	public function getMeRecipient()
	{
		return $this->hasOne(NotificationReceipient::className(), ['notification_id' => 'id'])->andWhere(['receipient_id'=>Yii::$app->user->identity->id]);
	}

	/**
	* @inheritdoc
	*/
	public function afterSave($insert, $changedAttributes)
	{
		if($this->receipient_id!=null){
			foreach($this->receipient_id as $key=>$val){
				$childRow=NotificationReceipient::find()->where(['notification_id'=>$this->id,'receipient_id'=>$val])->one();
				if($childRow==null){
					$childRow=new NotificationReceipient;
					$childRow->notification_id=$this->id;
					$childRow->receipient_id=$val;
					$childRow->is_read=0;
					$childRow->save();
				}
			}
		}
		parent::afterSave($insert, $changedAttributes);
	}

	/**
	* @return \yii\db\ActiveQuery
	*/
	public function getSentBy()
	{
		return $this->hasOne(User::className(), ['id' => 'sender_id']);
	}

	/**
	* @return \yii\db\ActiveQuery
	*/
	public function getTarget()
	{
		if($this->category=='inquiry'){
			return GeneralInquiry::findOne($this->target_id);
		}
	}

	/**
	* @return \yii\db\ActiveQuery
	*/
	public function getInfoTitle()
	{
		$title = '';
		if(in_array($this->action_type,Yii::$app->activityHelper->actionLogTypesArrKeys)){
			$model = ActionLog::findOne($this->target_id);
			$actionInfo=$model->actionInfo;
			$calendarInfo=$model->calendarEventInfo;
			if($actionInfo!=null){
				$title = $actionInfo['subject'];
			}
			if($model->rec_type=='calendar' && $calendarInfo!=null){
				$title = $model->comments.' <small>('.Yii::$app->formatter->asDateTime($calendarInfo['start_date']).')</small>';
			}
		}
		return $title;
	}

	public function getDescription()
	{
		return Yii::$app->notificationHelper->getDetail($this);
	}
}
