<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\SluggableBehavior;

/**
* This is the model class for table "{{%predefined_list_zone_detail}}".
*/
class Zone extends ActiveRecord
{
  public $title;
  /**
  * @inheritdoc
  * @return string of table name
  */
  public static function tableName()
  {
    return '{{%predefined_list_zone_detail}}';
  }

  /**
  * @inheritdoc
  * @return array of rules
  */
  public function rules()
  {
    return [
      [['title','country_id'], 'required'],
      [['country_id'], 'integer'],
      [[
        'title','slug','code'
      ], 'string'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'sluggable' => [
        'class' => SluggableBehavior::className(),
        'attribute' => 'title',
        'ensureUnique'=>true,
      ],
    ];
  }
}
