<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
* This is the model class for table "{{%campaign_attachment}}".
*
* @property integer $campaign_id
* @property integer $file_title
* @property integer $file_name
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
* @property string $deleted_at
* @property integer $deleted_by
*/
class CampaignAttachment extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%campaign_attachment}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['campaign_id', 'file_name'], 'required'],
      [['campaign_id'], 'integer'],
      [['file_title', 'file_name'], 'string'],
    ];
  }

  public static function primaryKey()
  {
  	return ['campaign_id','file_name'];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
    ];
  }

  /**
  * Mark record as deleted
  * @return boolean
  */
  public function softdelete()
  {
    $connection = \Yii::$app->db;
    $connection->createCommand("update ".self::tableName()." set deleted_at=:deleted_at,deleted_by=:deleted_by where campaign_id=:campaign_id and file_name=:file_name",
    [
      ':deleted_at' => date("Y-m-d H:i:s"),
      ':deleted_by' => Yii::$app->user->identity->id,
      ':campaign_id' => $this->campaign_id,
      ':file_name' => $this->file_name,
    ])
    ->execute();
    return true;
  }
}
