<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%list_item}}".
*
* @property integer $list_id
* @property integer $module_id
* @property string $module_type
*/
class ListItem extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%list_item}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['list_id', 'module_id', 'module_type'], 'required'],
      [['list_id', 'module_id'], 'integer'],
      [['module_type'], 'string'],
    ];
  }

  /**
   * @inheritdoc
   * @return array of attributes that will used at view and index page
   */
  public function attributeLabels()
  {
      return [
          'id' => Yii::t('app', 'ID'),
          'list_id' => Yii::t('app', 'List'),
          'module_type' => Yii::t('app', 'Type'),
      ];
  }

  public static function primaryKey()
  {
  	return ['list_id','module_id','module_type'];
  }

  public function getEmployee()
  {
    return Employee::findOne(['id'=>$this->module_id]);
  }

  public function getProspect()
  {
    return Prospect::findOne(['id'=>$this->module_id]);
  }

  public function getFullname()
  {
    $fullname='';
    if($this->module_type=='employee'){
      $fullname=$this->employee->name;
    }elseif($this->module_type=='prospect'){
      $fullname=$this->prospect->name;
    }
    return $fullname;
  }

  public function getEmail()
  {
    $email='';
    if($this->module_type=='employee'){
      $email=$this->employee->email;
    }elseif($this->module_type=='prospect'){
      $email=$this->prospect->email;
    }
    return $email;
  }
}
