<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\SluggableBehavior;

/**
* This is the model class for table "{{%predefined_list_country_detail}}".
*/
class Country extends ActiveRecord
{
  public $title;
  /**
  * @inheritdoc
  * @return string of table name
  */
  public static function tableName()
  {
    return '{{%predefined_list_country_detail}}';
  }

  /**
  * @inheritdoc
  * @return array of rules
  */
  public function rules()
  {
    return [
      [['title'], 'required'],
      [['postcode_required'], 'integer'],
      [[
        'title','slug','iso_code_2','iso_code_3','phone_code','phone_format','mobile_format','address_format'
      ], 'string'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'sluggable' => [
        'class' => SluggableBehavior::className(),
        'attribute' => 'title',
        'ensureUnique'=>true,
      ],
    ];
  }
}
