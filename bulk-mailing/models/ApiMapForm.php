<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* ApiMapForm is the model behind the Api Mapping
*/
class ApiMapForm extends Model
{
  public $api_source_id,$target_column;
  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['api_source_id'],'required'],
      [['api_source_id'],'integer'],
      [['target_column'],'each','rule'=>['string']],
    ];
  }

  /**
   * @inheritdoc
   * @return array of attribute lables
   */
  public function attributeLabels()
  {
      return [
          'api_source_id' => Yii::t('app', 'Api Source'),
          'target_column' => Yii::t('app', 'Target'),
      ];
  }

  /**
  * Save Action Log Comment Info
  */
  public function save()
  {
    if ($this->validate()) {
      if($this->target_column!=null){
        foreach($this->target_column as $key=>$val){
          $model = ApiSourceMaping::find()
          ->where([
            'api_source_id'=>$this->api_source_id,
            'source_column'=>$key,
          ])
          ->one();
          if($model==null){
            $model = new ApiSourceMaping;
            $model->api_source_id = $this->api_source_id;
            $model->source_column = $key;
          }
          $model->target_column = $val;
          $model->save();
        }
      }
      return true;
    }
    return false;
  }
}
