<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;
use yii\db\Expression;

/**
* CustomerSearch represents the model behind the search form of `app\models\User`.
*/
class CustomerSearch extends User
{
  public $permission_group,$name,$mobile,$job_title,$department,$pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','status','created_by','updated_by','pageSize'],'integer'],
      [['name','email','mobile','job_title','department'],'string'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function searchMy($params)
  {
    $this->load($params);
    $query = User::find()
    ->select([
      User::tableName().'.id',
      'permission_group_id',
      'job_title'=>'jt.title',
      'department'=>'dept.title',
      'image',
      'name'=>'CONCAT(firstname," ",lastname)',
      'email',
      UserProfileInfo::tableName().'.mobile',
      User::tableName().'.status',
    ])
    ->leftJoin(UserProfileInfo::tableName(),UserProfileInfo::tableName().'.user_id='.User::tableName().'.id')
    ->leftJoin(AdminGroup::tableName(),AdminGroup::tableName().'.id='.User::tableName().'.permission_group_id')
    ->leftJoin(PredefinedList::tableName().' as jt','jt.id='.UserProfileInfo::tableName().'.job_title_id')
    ->leftJoin(PredefinedList::tableName().' as dept','dept.id='.UserProfileInfo::tableName().'.department_id')
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
      'sort' =>false,
    ]);

    //Check Listing type allowed
    if(Yii::$app->menuHelperFunction->getListingTypeByController('customer')==2){
      $query->innerJoin(ModuleManager::tableName(),ModuleManager::tableName().".module_id=".User::tableName().".id and ".ModuleManager::tableName().".module_type='".$this->moduleTypeId."'");
      $query->andFilterWhere([
        ModuleManager::tableName().'.staff_id' => Yii::$app->user->identity->id]);
    }

    // grid filtering conditions
    $query->andFilterWhere([
      User::tableName().'.id' => $this->id,
      'user_type' => 0,
      'permission_group_id' => Yii::$app->appHelperFunctions->getSetting('cust_perm_id'),
      User::tableName().'.status' => $this->status,
    ]);
    $query->andWhere([
        'is',User::tableName().'.deleted_at',new Expression('null')
    ]);

    $query->andFilterWhere(['or',['like','firstname',$this->name],['like','lastname',$this->name]])
    ->andFilterWhere(['like','email',$this->email])
    ->andFilterWhere(['like','jt.title',$this->job_title])
    ->andFilterWhere(['like','dept.title',$this->department])
    ->andFilterWhere(['like',UserProfileInfo::tableName().'.mobile',$this->mobile]);

    return $dataProvider;
  }
}
