<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Lists;

/**
* ListSelectionForm is the model behind the list selection form
*/
class ListSelectionForm extends Model
{
  public $list_id,$module_type,$module_ids;
  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['list_id','module_type','module_ids'],'required'],
      [['list_id'],'integer'],
      [['module_type','module_ids'],'string'],
    ];
  }

  /**
   * @inheritdoc
   * @return array of attribute lables
   */
  public function attributeLabels()
  {
      return [
          'list_id' => Yii::t('app', 'Select List to save selection to'),
      ];
  }

  /**
  * Save Action Log Comment Info
  */
  public function save()
  {
    if ($this->validate()) {
      $list=Lists::find()->where(['id'=>$this->list_id])->one();
      if($list!=null){
        $list->module_type=$this->module_type;
        $list->module_ids=$this->module_ids;
        $list->save();
      }else{
        $this->addError('',Yii::t('app','Please select a list'));
        return false;
      }
      return true;
    }
    return false;
  }
}
