<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\components\models\ActiveRecordTsBa;

/**
* This is the model class for table "{{%customer_mail_smtp}}".
*
* @property integer $id
* @property integer $company_id
* @property string $service_type
* @property string $smtp_host
* @property string $smtp_username
* @property string $smtp_password
* @property integer $smtp_port
* @property string $smtp_encryption
* @property string $mail_from
* @property integer $status
*/
class EmailSmtpDetails extends ActiveRecordTsBa
{
  public $is_default;
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%customer_mail_smtp}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [[
        'company_id','service_type','smtp_host','smtp_username','smtp_password','smtp_port','smtp_encryption',
        'mail_from'
      ], 'required'],
      [['service_type','smtp_host','smtp_username','smtp_password','smtp_port','smtp_encryption','mail_from'], 'string'],
      [['service_type','smtp_host','smtp_username','smtp_password','smtp_port','smtp_encryption','mail_from'], 'trim'],
      [['company_id','status','is_default'],'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'service_type' => Yii::t('app', 'Type'),
      'smtp_host' => Yii::t('app', 'Host'),
      'smtp_username' => Yii::t('app', 'Username'),
      'smtp_password' => Yii::t('app', 'Password'),
      'smtp_port' => Yii::t('app', 'Port'),
      'smtp_encryption' => Yii::t('app', 'Encryption'),
      'mail_from' => Yii::t('app', 'From Email'),
      'is_default' => Yii::t('app', 'Default'),
    ];
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecTitle()
  {
    return $this->smtp_host;
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecType()
  {
    return 'SMTP Info';
  }

	/**
	* @inheritdoc
	*/
	public function afterSave($insert, $changedAttributes)
	{
    if($this->is_default==1){
      $setting=CustomerSetting::find()->where(['company_id'=>$this->company_id,'config_name'=>'default_smtp'])->one();
      if($setting!=null){
        $setting->config_value=(string)$this->id;
        $setting->save();
      }
    }
		parent::afterSave($insert, $changedAttributes);
	}
}
