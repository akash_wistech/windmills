<?php
namespace app\models;

use Yii;
use app\models\User;
use yii\base\Model;
use yii\db\Expression;

/**
 * Forget Password form
 */
class ForgetPasswordForm extends Model
{
  public $email;

  /**
   * @inheritdoc
   */
  public function rules()
  {
    return [
      ['email', 'filter', 'filter' => 'trim'],
      ['email', 'required'],
      ['email', 'email','message'=>'Email address is invalid'],
      [['email'], 'filter', 'filter' => 'trim'],
      ['email', 'exist',
        'targetClass' => '\app\models\User',
        'targetAttribute' => 'email',
        'filter' => ['and',['status' => 1],['is','deleted_at',new Expression('null')]],
        'message' => 'The email address is incorrect'
      ],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels()
  {
    return [
      'email' => Yii::t('app', 'Email'),
    ];
  }

  /**
   * Sends an email with a link, for resetting the password.
   *
   * @return boolean whether the email was send
   */
  public function sendEmail()
  {
    /* @var $user User */
    $user = User::findOne([
      'and',
      [
        'status' => 1,
        'email' => $this->email,
      ],
      ['is','deleted_at',new Expression('null')]
    ]);
    if ($user) {
      if ($user->resetInfo==null || !User::isPasswordResetTokenValid($user->passwordResetToken)) {
        $user->generatePasswordResetToken();
      }
      if ($user->save()) {
        $message = Yii::$app->mailer->compose(['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'], ['user' => $user]);
        $message->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['companyName']]);
        $message->setTo($this->email);
        $message->setSubject('Password reset for ' . Yii::$app->params['companyName']);
        return $message->send();
      }
    }
    return false;
  }
}
