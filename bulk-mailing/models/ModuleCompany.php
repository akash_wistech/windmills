<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%module_company}}".
*
* @property string $module_type
* @property integer $module_id
* @property integer $company_id
* @property integer $role_id
* @property integer $job_title_id
* @property integer $is_primary
*/
class ModuleCompany extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%module_company}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['module_type','module_id','company_id'], 'required'],
      [['module_id','company_id','role_id','job_title_id','is_primary'], 'integer'],
      [['module_type'], 'string'],
    ];
  }
}
