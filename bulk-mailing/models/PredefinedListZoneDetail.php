<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\SluggableBehavior;

/**
* This is the model class for table "{{%predefined_list_zone_detail}}".
*
* @property integer $id
* @property string $slug
* @property integer $country_id
* @property string $code
*/
class PredefinedListZoneDetail extends ActiveRecord
{
  public $title;
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%predefined_list_zone_detail}}';
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'sluggable' => [
        'class' => SluggableBehavior::className(),
        'attribute' => 'title',
        'ensureUnique'=>true,
      ],
    ];
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id','country_id'], 'required'],
      [['id','country_id'], 'integer'],
      [['slug','code'], 'string'],
    ];
  }
}
