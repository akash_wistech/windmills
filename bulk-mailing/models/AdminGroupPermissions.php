<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%admin_group_permissions}}".
*
* @property integer $id
* @property integer $group_id
* @property integer $menu_id
*/
class AdminGroupPermissions extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%admin_group_permissions}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['group_id', 'menu_id'], 'required'],
      [['group_id', 'menu_id'], 'integer']
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'group_id' => Yii::t('app', 'Group ID'),
      'menu_id' => Yii::t('app', 'Menu ID'),
    ];
  }
}
