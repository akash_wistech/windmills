<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;
use yii\db\Expression;

/**
* This is the model class for table "{{%prospect}}".
*
* @property integer $id
* @property integer $company_id
* @property string $sfirstname
* @property string $lastname
* @property string $city
* @property integer $country_id
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
* @property string $deleted_at
* @property integer $deleted_by
*/
class Prospect extends ActiveRecordFull
{
	public $manager_id,$tags,$input_field;
	public $map_location,$map_lat,$map_lng;
  public $map_lat_tmp,$map_lng_tmp;
  public $map_location_tmp,$maploc_lat_tmp,$maploc_lng_tmp;
  public $emails,$phone_numbers;
	public $comp_row_id,$comp_id,$comp_name,$comp_role_id,$comp_role,$comp_job_title_id,$comp_job_title;
  public $add_id,$add_is_primary,$add_country_id,$add_zone_id,$add_city,$add_phone,$add_address;

	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%prospect}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['company_id','firstname','email'],'required'],
// 			[['email'],'unique'],
// 			['email', 'unique', 'targetAttribute' => ['email', 'company_id'],'message'=>'Email already exists'],
			[['created_at','updated_at','deleted_at','list_type'],'safe'],
			[[
				'add_is_primary','status','created_by','updated_by','deleted_by'
			],'integer'],
			[[
				'firstname','lastname','tags','map_location','map_lat_tmp','map_lng_tmp',
				'map_location_tmp','maploc_lat_tmp','maploc_lng_tmp'
			],'string'],
			[['firstname','lastname'],'trim'],
			[['emails'],'each','rule'=>['email']],
			[[
				'manager_id','phone_numbers','comp_row_id','comp_id','comp_role_id','comp_job_title_id','add_id',
				'add_country_id','add_zone_id'
			],'each','rule'=>['integer']],
			[[
				'comp_name','comp_role','comp_job_title','add_city','add_phone','add_address'
			],'each','rule'=>['string']],
     // ['email','uniqueCompany'],
      ['input_field','checkRequiredInputs'],
		];
	}

  /**
   * Validates the required inputs.
   * This method serves as the inline validation for required inputs.
   *
   * @param string $attribute the attribute currently being validated
   * @param array $params the additional name-value pairs given in the rule
   */
  public function uniqueCompany()
  {
    if (!$this->hasErrors()) {
			$result = self::find()->where(['company_id'=>$this->company_id,'email'=>$this->email,'deleted_at'=>null])->andFilterWhere(['!=','id',$this->id])->one();
			if($result!=null){
				$this->addError('email',Yii::t('app','Email already exists'));
				return false;
			}
    }
  }

  /**
   * Validates the required inputs.
   * This method serves as the inline validation for required inputs.
   *
   * @param string $attribute the attribute currently being validated
   * @param array $params the additional name-value pairs given in the rule
   */
  public function checkRequiredInputs()
  {
    if (!$this->hasErrors()) {
			return Yii::$app->inputHelperFunctions->validateRequired($this);
    }
  }

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'firstname' => Yii::t('app', 'First Name'),
			'lastname' => Yii::t('app', 'Last Name'),
			'cp_name' => Yii::t('app', 'Name'),
			'country_id' => Yii::t('app', 'Country'),
			'country_name' => Yii::t('app', 'Country'),
			'city' => Yii::t('app', 'City'),
			'emails' => Yii::t('app', 'Additional Email(s)'),
			'phone_numbers' => Yii::t('app', 'Contact No.'),
			'address' => Yii::t('app', 'Address'),
			'manager_id' => Yii::t('app', 'Assign To'),
			'created_at' => Yii::t('app', 'Created At'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'updated_by' => Yii::t('app', 'Updated By'),
			'deleted_at' => Yii::t('app', 'Trashed At'),
			'deleted_by' => Yii::t('app', 'Trashed By'),
			'workflow_id' => Yii::t('app', 'Process Name'),
			'workflow_stage_id' => Yii::t('app', 'Stage'),
			'tags' => Yii::t('app', 'Tags'),
			'add_is_primary' => Yii::t('app', 'Primary'),
			'add_country_id' => Yii::t('app', 'Country'),
			'add_zone_id' => Yii::t('app', 'State / Province'),
			'add_city' => Yii::t('app', 'City'),
			'add_phone' => Yii::t('app', 'Contact Number'),
			'add_address' => Yii::t('app', 'Address'),

			'comp_name' => Yii::t('app', 'Name'),
			'comp_role' => Yii::t('app', 'Role'),
			'comp_job_title' => Yii::t('app', 'Job Title'),
		];
	}

	/**
	* returns class name
	*/
	public function getBaseClassName()
	{
		return 'Prospect';
	}

	/**
	* returns main title/name of row for status and delete loging
	*/
	public function getRecTitle()
	{
		return $this->name;
	}

	/**
	* returns main title/name of row for status and delete loging
	*/
	public function getRecType()
	{
		return 'Prospect';
	}

	/**
	* returns type of module for process and events
	*/
	public function getModuleTypeId()
	{
		return 'prospect';
	}

  /**
  * return full name
  */
  public function getName()
  {
    return trim($this->firstname.($this->lastname!='' ? ' '.$this->lastname : ''));
  }

	/**
	* Get Assigned Staff members
	* @return \yii\db\ActiveQuery
	*/
	public function getManagerIdz()
	{
		return ModuleManager::find()->select(['staff_id'])->where(['module_type'=>$this->moduleTypeId,'module_id'=>$this->id])->asArray()->all();
	}

	/**
	* @return \yii\db\ActiveQuery
	*/
  public function getTagsListArray()
  {
		$subQueryModuleTags=ModuleTag::find()->select(['tag_id'])->where(['module_type'=>$this->moduleTypeId,'module_id'=>$this->id]);
		$acRows=Tags::find()->select(['title'])
		->where([
			'and',
			['id'=>$subQueryModuleTags],
			['is','deleted_at',new Expression('null')]
		])
		->asArray()->all();
		$tagList=[];
		foreach($acRows as $acRow){
			$tagList[]=$acRow['title'];
		}
    return $tagList;
  }

	/**
	* @return \yii\db\ActiveQuery
	*/
  public function getPreviousTags()
  {
		$subQueryModuleTags=ModuleTag::find()->select(['tag_id'])->where(['module_type'=>$this->moduleTypeId,'module_id'=>$this->id]);
		$acRows=Tags::find()->select(['title'])
		->where([
			'and',
			['id'=>$subQueryModuleTags],
			['is','deleted_at',new Expression('null')]
		])
		->asArray()->all();
		$tagList=[];
		foreach($acRows as $acRow){
			$tagList[]=$acRow['title'];
		}
    return implode(",",$tagList);
  }

	/**
	* @inheritdoc
	*/
	public function afterSave($insert, $changedAttributes)
	{
		//Saving Custom Fields
		Yii::$app->inputHelperFunctions->saveCustomField($this);

		//Saving Multi Emails
		Yii::$app->inputHelperFunctions->saveMultiEmailNumbers($this);

		//Saving Multi Companies
		Yii::$app->inputHelperFunctions->saveMultiCompanies($this);

		//Saving Multi Address
		Yii::$app->inputHelperFunctions->saveMultiAddress($this);

		//Saving Tags
		Yii::$app->inputHelperFunctions->saveTags($this);

		//Saving Managers
		Yii::$app->inputHelperFunctions->saveManagers($this);

		parent::afterSave($insert, $changedAttributes);
	}
}
