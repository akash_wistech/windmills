<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
* ImportRawDataForm is the model behind the sold transaction import form.
*/
class ImportRawDataForm extends Model
{
  public $importfile,$tags;
  public $allowedFileTypes=['csv'];//'xls', 'xlsx',
  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [['importfile','tags'], 'safe'],
      [['importfile'], 'file', 'extensions'=>implode(",",$this->allowedFileTypes)]
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'importfile' => Yii::t('app', 'File'),
      'tags' => Yii::t('app', 'Tags'),
    ];
  }

  /**
  * import the entries into table
  */
  public function save()
  {
    $saved=0;
    $unsaved=0;
    $errNames='';
    if ($this->validate()) {
      if(UploadedFile::getInstance($this, 'importfile')){
        $importedFile = UploadedFile::getInstance($this, 'importfile');
        // if no image was uploaded abort the upload
        if (!empty($importedFile)) {
          $pInfo=pathinfo($importedFile->name);
          $ext = $pInfo['extension'];
          if (in_array($ext,$this->allowedFileTypes)) {
            // Check to see if any PHP files are trying to be uploaded
            $content = file_get_contents($importedFile->tempName);
            if (preg_match('/\<\?php/i', $content)) {

            }else{
              $this->importfile = Yii::$app->fileHelperFunctions->tempImagePath['abs'].$importedFile->name;
              $importedFile->saveAs($this->importfile);

              $csvFile = fopen($this->importfile, 'r');
              $data = [];
              $n=1;
              while (($line = fgetcsv($csvFile)) !== FALSE) {
                if($n>1){
                  $name = $line[0];
                  $email = $line[1];
                  $rtags = $line[2];
                  $model=Prospect::find()
                  ->where([
                    'company_id'=>Yii::$app->user->identity->company_id,
                    'email'=>$email,
                  ])
                  ->one();
                  if($model==null){
                    $model=new Prospect;
                    $model->company_id=Yii::$app->user->identity->company_id;
                    $model->firstname=$name;
                    $model->email=$email;
                  }
                  $tags = $this->tags;
                  if($model!=null){
                    $previousTags = $model->previousTags;
                    if($previousTags!=''){
                      if($tags!='')$tags.=',';
                      $tags.=$previousTags;
                    }
                  }
                  $tags.= ($tags!='' ? ',' : '').$rtags;

                  if($tags!=''){
                    $model->tags=$tags;
                  }
                  if($model->save()){
                    $saved++;
                  }else{
                    if($model->hasErrors()){
                      foreach($model->getErrors() as $error){
                        if(count($error)>0){
                          foreach($error as $key=>$val){
                            echo $val;
                          }
                        }
                      }
                    }
                    $errNames.='<br />'.$buildingName;
                    $unsaved++;
                  }
                }
                $n++;
              }
              fclose($csvFile);
              //Unlink File
              unlink($this->importfile);
            }
          }
        }
      }
      if($saved>0){
        Yii::$app->getSession()->setFlash('success', $saved.' - '.Yii::t('app','rows saved successfully'));
      }
      if($unsaved>0){
        Yii::$app->getSession()->setFlash('error', $unsaved.' - '.Yii::t('app','rows were not saved!').$errNames);
      }
      return true;
    } else {
      return false;
    }
  }
}
