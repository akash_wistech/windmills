<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CampaignEmail;
use yii\db\Expression;

/**
 * CampaignSearch represents the model behind the search form about `app\models\Campaign`.
 */
class CampaignEmailSearch extends CampaignEmail
{
    /**
     * @inheritdoc
     * @return array of rules
     */
    public function rules()
    {
        return [
            [[
              'company_id','module_id','smtp_id','sent','delivered','open','click','soft_bounced','hard_bounced',
              'spam_complain'
            ], 'integer'],
            [['email','module_type'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     * @return object of model scenarios
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params is for search model
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CampaignEmail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'campaign_id' => $this->campaign_id,
            'module_id' => $this->module_id,
      'smtp_id' => $this->smtp_id,
            'email' => $this->email,
            'delivered' => $this->delivered,
      //      'sent' => $this->sent,
            'open' => $this->open,
            'click' => $this->click,
            'soft_bounced' => $this->soft_bounced,
            'hard_bounced' => $this->hard_bounced,
            'spam_complain' => $this->spam_complain,
            'module_type' => $this->module_type,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
