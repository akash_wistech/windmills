<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordSlugdFull;
use yii\web\IdentityInterface;
use yii\web\UploadedFile;
use yii\db\Expression;

/**
* User model
*
* @property integer $id
* @property integer $user_type
* @property integer $permission_group_id
* @property integer $active_contract_id
* @property integer $active_package_id
* @property string $username
* @property string $firstname
* @property string $lastname
* @property string $email
* @property string $auth_key
* @property string $password_hash
* @property string $image
* @property integer $status
* @property string $start_date
* @property string $end_date
*/
class User extends ActiveRecordSlugdFull implements IdentityInterface
{
  public $file,$oldfile;
  public $allowedImageSize = 1048576;
  public $allowedImageTypes=['jpg', 'jpeg', 'png', 'gif', 'JPG', 'JPEG', 'PNG', 'GIF'];

  public $new_password,$job_title_id,$job_title,$department_id,$department_name;
  public $mobile,$phone1,$phone2,$address,$background_info;
  public $lead_type,$lead_source,$lead_date,$lead_score,$expected_close_date,$close_date,$confidence,$deal_status;

  public $company_name,$manager_id,$tags,$input_field;
	public $workflow_id,$workflow_stage_id;
	public $map_location,$map_lat,$map_lng;
  public $map_lat_tmp,$map_lng_tmp;
  public $map_location_tmp,$maploc_lat_tmp,$maploc_lng_tmp;

  public $emails,$phone_numbers;
	public $comp_row_id,$comp_id,$comp_name,$comp_role_id,$comp_role,$comp_job_title_id,$comp_job_title,$is_primary;
  public $add_id,$add_is_primary,$add_country_id,$add_zone_id,$add_city,$add_phone,$add_address;

  const STATUS_ACTIVE = '1';
  const STATUS_HOLD = '2';
  const STATUS_CANCEL = '3';
  const STATUS_PENDIND = '20';
  const STATUS_DELETED = '0';

  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%user}}';
  }


  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'user_type' => Yii::t('app', 'Member Type'),
      'permission_group_id' => Yii::t('app', 'Permission Group'),
      'firstname' => Yii::t('app', 'First Name'),
      'lastname' => Yii::t('app', 'Last Name'),
      'email' => Yii::t('app', 'Primary Email'),
      'new_password' => Yii::t('app', 'Password'),
      'job_title_id' => Yii::t('app', 'Job Title'),
      'job_title' => Yii::t('app', 'Job Title'),
      'department_id' => Yii::t('app', 'Department'),
      'department_name' => Yii::t('app', 'Department'),

      'country_id' => Yii::t('app', 'Country'),
      'zone_id' => Yii::t('app', 'State / Province'),
      'city' => Yii::t('app', 'City'),
      'postal_code' => Yii::t('app', 'Postal Code'),

      'mobile' => Yii::t('app', 'Mobile'),
      'phone1' => Yii::t('app', 'Phone 1'),
      'phone2' => Yii::t('app', 'Phone 2'),
      'address' => Yii::t('app', 'Address'),
      'background_info' => Yii::t('app', 'Background Info'),

      'image' => Yii::t('app', 'Photo'),
			'manager_id' => Yii::t('app', 'Assign To'),
			'workflow_id' => Yii::t('app', 'Process Name'),
			'workflow_stage_id' => Yii::t('app', 'Stage'),
			'tags' => Yii::t('app', 'Tags'),

			'emails' => Yii::t('app', 'Other Emails'),
			'add_is_primary' => Yii::t('app', 'Primary'),
			'add_country_id' => Yii::t('app', 'Country'),
			'add_zone_id' => Yii::t('app', 'State / Province'),
			'add_city' => Yii::t('app', 'City'),
			'add_phone' => Yii::t('app', 'Contact Number'),
			'add_address' => Yii::t('app', 'Address'),

			'comp_name' => Yii::t('app', 'Name'),
			'comp_role' => Yii::t('app', 'Role'),
			'comp_job_title' => Yii::t('app', 'Job Title'),
    ];
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['email','firstname', 'lastname'], 'required'],
      ['email','email'],
      ['email','unique'],
      [['map_lat','map_lng'],'safe'],
      [[
        'firstname','lastname','new_password','job_title','department_name','address','background_info',
        'lead_date','expected_close_date','close_date','tags','map_location','map_lat_tmp','map_lng_tmp',
				'map_location_tmp','maploc_lat_tmp','maploc_lng_tmp','mobile','phone1','phone2','company_name'
      ],'string'],
      [['firstname','lastname'], 'trim'],
			[['emails'],'each','rule'=>['email']],
			[[
				'manager_id','phone_numbers','comp_row_id','comp_id','comp_role_id','comp_job_title_id','add_id',
				'add_country_id','add_zone_id','is_primary'
			],'each','rule'=>['integer']],
			[[
				'comp_name','comp_role','comp_job_title','add_city','add_phone','add_address'
			],'each','rule'=>['string']],
      [[
        'department_id','permission_group_id','lead_type','lead_source','workflow_id','workflow_stage_id',
        'lead_score','confidence','deal_status','add_is_primary'
      ], 'integer'],
      [['image'], 'file', 'extensions'=>implode(",",$this->allowedImageTypes), 'maxSize' => $this->allowedImageSize, 'tooBig' => 'Image is too big, Maximum allowed size is 1MB'],
      ['input_field','checkRequiredInputs'],
    ];
  }

  /**
   * Validates the required inputs.
   * This method serves as the inline validation for required inputs.
   *
   * @param string $attribute the attribute currently being validated
   * @param array $params the additional name-value pairs given in the rule
   */
  public function checkRequiredInputs()
  {
    if (!$this->hasErrors()) {
			return Yii::$app->inputHelperFunctions->validateRequired($this);
    }
  }

  /**
  * @inheritdoc
  */
  public static function findIdentity($id)
  {
    return static::findOne(['and',['id' => $id, 'status' => 1, 'verified' => 1],['is','deleted_at',new Expression('null')]]);
  }

  /**
  * {@inheritdoc}
  */
  public static function findIdentityByAccessToken($token, $type = null)
  {
    return static::findOne(['and',['auth_key' => $token, 'status' => 1, 'verified' => 1],['is','deleted_at',new Expression('null')]]);
  }

  /**
  * Finds user by username
  *
  * @param string $username
  * @return static|null
  */
  public static function findByUsername($username)
  {
    return static::find()->where(['and',['email' => $username],['is','deleted_at',new Expression('null')]])->one();
  }

  /**
  * {@inheritdoc}
  */
  public function getId()
  {
    return $this->getPrimaryKey();
  }

  /**
  * {@inheritdoc}
  */
  public function getAuthKey()
  {
    return $this->auth_key;
  }

  /**
  * {@inheritdoc}
  */
  public function validateAuthKey($authKey)
  {
    return $this->auth_key === $authKey;
  }

  /**
  * Validates password
  *
  * @param string $password password to validate
  * @return bool if password provided is valid for current user
  */
  public function validatePassword($password)
  {
    return Yii::$app->security->validatePassword($password, $this->password_hash);
  }

  /**
  * Generates password hash from password and sets it to the model
  *
  * @param string $password
  */
  public function setPassword($password)
  {
    $this->password_hash = Yii::$app->security->generatePasswordHash($password);
  }

  /**
  * Generates "remember me" authentication key
  */
  public function generateAuthKey()
  {
    $this->auth_key = Yii::$app->security->generateRandomString();
  }

  /**
  * Generates "login from backend" authentication key
  */
  public function generateVerifyToken()
  {
    $this->verify_token = Yii::$app->security->generateRandomString();
  }

  /**
  * Generates new password reset token
  */
  public function generatePasswordResetToken()
  {
    $connection = \Yii::$app->db;
    $connection->createCommand("update ".UserReset::tableName()." set status=:status where user_id=:user_id",[
      ':status' => 10,':user_id' => $this->id,
    ])
    ->execute();

    $newResetInfo = new UserReset;
    $newResetInfo->user_id = $this->id;
    $newResetInfo->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    $newResetInfo->status = 0;
    $newResetInfo->save();
  }

  public function getPasswordResetToken()
  {
    $token = '';
    $resetInfo=UserReset::find()->where(['user_id'=>$this->id])->orderBy(['created_at'=>SORT_DESC])->one();
    if($resetInfo!=null){
      $token = $resetInfo->password_reset_token;
    }
    return $token;
  }

  /**
  * Finds user by password reset token
  *
  * @param string $token password reset token
  * @return static|null
  */
  public static function findByPasswordResetToken($token)
  {
    if (!static::isPasswordResetTokenValid($token)) {
      return null;
    }
    return UserReset::find()
    ->innerJoin("user",User::tableName().".id=".UserReset::tableName().".user_id")
    ->where([
      UserReset::tableName().'.password_reset_token' => $token,
      UserReset::tableName().'.status' => 0,
      User::tableName().'.status' => 1,
    ])
    ->one();
  }

  /**
  * Finds out if password reset token is valid
  *
  * @param string $token password reset token
  * @return boolean
  */
  public static function isPasswordResetTokenValid($token)
  {
    if (empty($token) || $token==null) {
      return false;
    }
    $expire = Yii::$app->params['user.passwordResetTokenExpire'];
    $parts = explode('_', $token);
    $timestamp = (int) end($parts);
    return $timestamp + $expire >= time();
  }

  /**
  * return full name
  */
  public function getName()
  {
    return trim($this->firstname.($this->lastname!='' ? ' '.$this->lastname : ''));
  }

	/**
	* returns class name
	*/
	public function getBaseClassName()
	{
		return 'User';
	}

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecTitle()
  {
    return $this->name;
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecType()
  {
    if($this->user_type==0){
      return 'Customer';
    }else if($this->user_type==1){
      return 'Employee';
    }else if($this->user_type==10){
      return 'Staff Member';
    }else if($this->user_type==20){
      return 'Super Admin';
    }
  }

	/**
	* returns type of module for process and events
	*/
	public function getModuleTypeId()
	{
    if($this->user_type==0){
      return 'customer';
    }else if($this->user_type==1){
      return 'employee';
    }else if($this->user_type==10){
      return 'staff';
    }else if($this->user_type==20){
      return 'superadmin';
    }
	}

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getProfileInfo()
  {
    return $this->hasOne(UserProfileInfo::className(), ['user_id' => 'id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getResetInfo()
  {
    return $this->hasOne(UserReset::className(), ['user_id' => 'id'])->where(['status'=>0]);
  }

  public function getLastLoginInfo()
  {
    $history=UserLoginHistory::find()
    ->select(['created_at'])
    ->where(['user_id'=>$this->id,'login'=>1])
    ->orderBy(['created_at'=>SORT_DESC])
    ->offset(1)
    ->asArray()->one();
    if($history!=null){
      return 'on '.Yii::$app->formatter->asDate($history['created_at']);
    }else{
      return ' first time';
    }
  }

  /**
  * @return boolean
  */
  public function getPrivilege($keyword)
  {
    $permission=false;
    $services=ArrayHelper::map(PackageOption::find()->all(),'keyword','id');
    $result=PackageAllowedOptions::find()->where(['package_id'=>$this->activeContract->package_id,'service_id'=>$services[$keyword]])->one();
    if($result!=null){
      $permission=true;
    }

    return $permission;
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCompany()
  {
    return $this->hasOne(Company::className(), ['id' => 'company_id']);
  }

  /**
  * @inheritdoc
  */
  public function beforeValidate()
  {
    //Uploading Logo
    if(UploadedFile::getInstance($this, 'image')){
      $this->file = UploadedFile::getInstance($this, 'image');
      // if no file was uploaded abort the upload
      if (!empty($this->file)) {
        $pInfo=pathinfo($this->file->name);
        $ext = $pInfo['extension'];
        if (in_array($ext,$this->allowedImageTypes)) {
          // Check to see if any PHP files are trying to be uploaded
          $content = file_get_contents($this->file->tempName);
          if (preg_match('/\<\?php/i', $content)) {
            $this->addError('image', Yii::t('app', 'Invalid file provided!'));
            return false;
          }else{
            if (filesize($this->file->tempName) <= $this->allowedImageSize) {
              // generate a unique file name
              $this->image = Yii::$app->fileHelperFunctions->generateName().".{$ext}";
            }else{
              $this->addError('image', Yii::t('app', 'File size is too big!'));
              return false;
            }
          }
        }else{
          $this->addError('image', Yii::t('app', 'Invalid file provided!'));
          return false;
        }
      }
    }
    if($this->image==null && $this->oldfile!=null){
      $this->image=$this->oldfile;
    }
    return parent::beforeValidate();
  }

  /**
  * @inheritdoc
  * Generates auth_key if it is a new user
  */
  public function beforeSave($insert)
  {
    if (parent::beforeSave($insert)) {
      if ($this->isNewRecord) {
        $this->generateAuthKey();
        $this->generateVerifyToken();
      }
      if($this->new_password!=null){
        $this->password = $this->new_password;
      }
      //Saving New Company if not in db
      if($this->company_name!=null && $this->company_name!=""){
        $companyRow=Company::find()->where(['title'=>trim($this->company_name)])->one();
        if($companyRow==null){
          $companyRow=new Company;
          $companyRow->title=$this->company_name;
          if($companyRow->save()){
            $this->company_id=$companyRow->id;
          }
        }else{
          $this->company_id=$companyRow->id;
        }
      }
      return true;
    }
    return false;
  }

  /**
  * @inheritdoc
  */
  public function afterSave($insert, $changedAttributes)
  {
		//Saving Custom Fields
		Yii::$app->inputHelperFunctions->saveCustomField($this);

		//Saving Multi Emails
		Yii::$app->inputHelperFunctions->saveMultiEmailNumbers($this);

		//Saving Multi Companies
		Yii::$app->inputHelperFunctions->saveMultiCompanies($this);

		//Saving Multi Address
		Yii::$app->inputHelperFunctions->saveMultiAddress($this);

		//Saving Tags
		Yii::$app->inputHelperFunctions->saveTags($this);

		//Saving Managers
		Yii::$app->inputHelperFunctions->saveManagers($this);

		//Saving Workflow
		if($this->workflow_id!=null || $this->workflow_stage_id!=null){
			$workflowRow=WorkflowDataItem::find()->where(['module_type'=>$this->moduleTypeId,'module_id'=>$this->id])->one();
			if($workflowRow==null){
				$workflowRow=new WorkflowDataItem;
				$workflowRow->module_type=$this->moduleTypeId;
				$workflowRow->module_id=$this->id;
			}
			$workflowRow->workflow_id=$this->workflow_id;
			$workflowRow->workflow_stage_id=$this->workflow_stage_id;
			$workflowRow->save();
		}
    //Saving New Job title if its not in db
    if($this->job_title!=null && $this->job_title!=""){
      $jtListId=Yii::$app->appHelperFunctions->getSetting('job_title_list_id');
      $jobTitleRow=PredefinedList::find()->where(['parent'=>$jtListId,'title'=>trim($this->job_title)])->one();
      if($jobTitleRow==null){
        $jobTitleRow=new PredefinedList;
        $jobTitleRow->parent=$jtListId;
        $jobTitleRow->title=$this->job_title;
        $jobTitleRow->status=1;
        if($jobTitleRow->save()){
          $this->job_title_id=$jobTitleRow->id;
        }
      }else{
        $this->job_title_id=$jobTitleRow->id;
      }
    }

    //Saving New Department if its not in db
    if($this->department_name!=null && $this->department_name!=""){
      $deptListId=Yii::$app->appHelperFunctions->getSetting('department_list_id');
      $departmentRow=PredefinedList::find()->where(['parent'=>$deptListId,'title'=>trim($this->department_name)])->one();
      if($departmentRow==null){
        $departmentRow=new PredefinedList;
        $departmentRow->parent=$deptListId;
        $departmentRow->title=$this->department_name;
        $departmentRow->status=1;
        if($departmentRow->save()){
          $this->department_id=$departmentRow->id;
        }
      }else{
        $this->department_id=$departmentRow->id;
      }
    }

    $userProfileInfo = UserProfileInfo::find()->where(['user_id'=>$this->id])->one();
    if($userProfileInfo==null){
      $userProfileInfo=new UserProfileInfo;
      $userProfileInfo->user_id=$this->id;
    }
    //$userProfileInfo->primary_contact=($this->primary_contact!=null ? $this->primary_contact : 0);
    if($this->job_title_id!=null)$userProfileInfo->job_title_id=$this->job_title_id;
    if($this->department_id!=null)$userProfileInfo->department_id=$this->department_id;
    if($this->mobile!=null)$userProfileInfo->mobile=$this->mobile;
    if($this->phone1!=null)$userProfileInfo->phone1=$this->phone1;
    if($this->phone2!=null)$userProfileInfo->phone2=$this->phone2;
    //if($this->country_id!=null)$userProfileInfo->country_id=$this->country_id;
    //if($this->zone_id!=null)$userProfileInfo->zone_id=$this->zone_id;
    //if($this->city!=null)$userProfileInfo->city=$this->city;
    //if($this->postal_code!=null)$userProfileInfo->postal_code=$this->postal_code;
    if($this->address!=null)$userProfileInfo->address=$this->address;
    if($this->lead_type!=null)$userProfileInfo->lead_type=$this->lead_type;
    if($this->lead_source!=null)$userProfileInfo->lead_source=$this->lead_source;
    if($this->lead_date!=null)$userProfileInfo->lead_date=$this->lead_date;
    if($this->lead_score!=null)$userProfileInfo->lead_score=$this->lead_score;
    if($this->expected_close_date!=null)$userProfileInfo->expected_close_date=$this->expected_close_date;
    if($this->close_date!=null)$userProfileInfo->close_date=$this->close_date;
    if($this->confidence!=null)$userProfileInfo->confidence=$this->confidence;
    if($this->deal_status!=null)$userProfileInfo->deal_status=$this->deal_status;
    $userProfileInfo->save();

    if ($this->file!== null && $this->image!=null) {
      if($this->oldfile!=null && $this->image!=$this->oldfile && file_exists(Yii::$app->fileHelperFunctions->memberImagePath['abs'].$this->oldfile)){
        unlink(Yii::$app->fileHelperFunctions->memberImagePath['abs'].$this->oldfile);
      }
      $this->file->saveAs(Yii::$app->fileHelperFunctions->memberImagePath['abs'].$this->image);
    }
    parent::afterSave($insert, $changedAttributes);
  }

	/**
	* Get Assigned Staff members
	* @return \yii\db\ActiveQuery
	*/
	public function getManagerIdz()
	{
		return ModuleManager::find()->select(['staff_id'])->where(['module_type'=>$this->moduleTypeId,'module_id'=>$this->id])->asArray()->all();
	}

	/**
	* @return \yii\db\ActiveQuery
	*/
  public function getTagsListArray()
  {
		$subQueryModuleTags=ModuleTag::find()->select(['tag_id'])->where(['module_type'=>$this->moduleTypeId,'module_id'=>$this->id]);
		$acRows=Tags::find()->select(['title'])->where(['id'=>$subQueryModuleTags])->asArray()->all();
		$tagList=[];
		foreach($acRows as $acRow){
			$tagList[]=$acRow['title'];
		}
    return $tagList;
  }
}
