<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordSlugdFull;

/**
* This is the model class for table "{{%company}}".
*
* @property integer $id
* @property integer $company_id
* @property string $title
* @property string $website
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
* @property string $deleted_at
* @property integer $deleted_by
*/
class Company extends ActiveRecordSlugdFull
{

	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%company}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['title'], 'required'],
			[['created_at', 'updated_at', 'deleted_at'], 'safe'],
			[['company_id','created_by', 'updated_by', 'deleted_by'], 'integer'],
			[['title','website'], 'string', 'max' => 255],
      [['title','website'],'trim'],
			['website', 'url', 'defaultScheme' => 'http'],
		];
	}

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'title' => Yii::t('app', 'Title'),
			'website' => Yii::t('app', 'Website'),
			'created_at' => Yii::t('app', 'Created At'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'updated_by' => Yii::t('app', 'Updated By'),
			'deleted_at' => Yii::t('app', 'Deleted At'),
			'deleted_by' => Yii::t('app', 'Deleted By'),
		];
	}

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecTitle()
  {
    return $this->title;
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecType()
  {
    return 'Company';
  }
}
