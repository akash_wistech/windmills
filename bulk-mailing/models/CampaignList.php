<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
* This is the model class for table "{{%campaign_list}}".
*
* @property integer $campaign_id
* @property integer $list_id
* @property string $created_at
* @property integer $created_by
* @property string $updated_at
* @property integer $updated_by
*/
class CampaignList extends ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%campaign_list}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['campaign_id', 'list_id'], 'required'],
      [['campaign_id', 'list_id'], 'integer'],
    ];
  }

  public static function primaryKey()
  {
  	return ['campaign_id','list_id'];
  }

  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'timestamp' => [
        'class' => TimestampBehavior::className(),
        'attributes' => [
          ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
          ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
        ],
        'value' => function($event) {
          return date("Y-m-d H:i:s");
        },
      ],
      'blameable' => [
        'class' => BlameableBehavior::className(),
        'createdByAttribute' => 'created_by',
        'updatedByAttribute' => 'updated_by',
      ],
    ];
  }
}
