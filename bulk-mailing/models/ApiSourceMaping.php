<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\components\models\ActiveRecordTsBa;

/**
* This is the model class for table "{{%api_source_maping}}".
*
* @property integer $id
* @property integer $api_source_id
* @property string $source_column
* @property string $target_column
*/
class ApiSourceMaping extends ActiveRecordTsBa
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%api_source_maping}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['api_source_id','source_column','target_column'], 'required'],
      [['source_column','target_column'], 'safe'],
      [['source_column','target_column'], 'trim'],
      [['api_source_id'],'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'api_source_id' => Yii::t('app', 'API Source'),
      'source_column' => Yii::t('app', 'Source Column'),
      'target_column' => Yii::t('app', 'Target Column'),
    ];
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getApiSource()
  {
    return $this->hasOne(ApiSource::className(), ['id' => 'api_source_id']);
  }
}
