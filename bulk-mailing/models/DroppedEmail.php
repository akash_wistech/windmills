<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\components\models\ActiveRecordFull;

/**
* This is the model class for table "{{%dropped_email}}".
*
* @property integer $id
* @property integer $email
*/
class DroppedEmail extends ActiveRecordFull
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%dropped_email}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['email'], 'required'],
      [['email','reason'], 'string'],
      [['email'], 'unique'],
      [['email'],'trim'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'email' => Yii::t('app', 'Email'),
    ];
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecTitle()
  {
    return $this->email;
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecType()
  {
    return 'Soft Bounced Email';
  }
}
