<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\components\models\ActiveRecordTsBa;

/**
* This is the model class for table "{{%api_source}}".
*
* @property integer $id
* @property integer $company_id
* @property string $title
* @property string $url
* @property string $sync_frequency
* @property integer $status
* @property string $last_sync
*/
class ApiSource extends ActiveRecordTsBa
{
  public $target_column;
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%api_source}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['company_id','title','url','sync_frequency'], 'required'],
      [['title','url','sync_frequency'], 'string'],
      [['title','url'], 'trim'],
      [['source_column','target_column'], 'safe'],
      [['company_id'],'integer'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'title' => Yii::t('app', 'Title'),
      'url' => Yii::t('app', 'Url'),
      'sync_frequency' => Yii::t('app', 'Frequency'),
    ];
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecTitle()
  {
    return $this->title;
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecType()
  {
    return 'Api Source';
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getApiMapping()
  {
    return $this->hasOne(ApiSourceMaping::className(), ['api_source_id' => 'id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getApiCronJobs()
  {
    return $this->hasMany(CronJobSyncApi::className(), ['api_source_id' => 'id']);
  }
}
