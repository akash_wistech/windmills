<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordTsBa;

/**
* This is the model class for table "{{%custom_field_data}}".
*
* @property int $id
* @property int $company_id
* @property string $module_type
* @property int $module_id
* @property int $input_field_id
* @property string $input_value
* @property string $input_org_value
* @property string $created_at
* @property int $created_by
* @property string $updated_at
* @property int $updated_by
*/
class CustomFieldData extends ActiveRecordTsBa
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%custom_field_data}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['company_id','module_type','module_id'],'required'],
      [['created_at','updated_at'],'safe'],
      [['company_id','module_id','input_field_id','created_by','updated_by'],'integer'],
      [['module_type','input_value','input_org_value'],'string'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'module_id' => Yii::t('app', 'Module'),
      'input_value' => Yii::t('app', 'Value'),
      'created_at' => Yii::t('app', 'Created'),
      'created_by' => Yii::t('app', 'Created By'),
      'updated_at' => Yii::t('app', 'Updated'),
      'updated_by' => Yii::t('app', 'Updated By'),
    ];
  }
}
