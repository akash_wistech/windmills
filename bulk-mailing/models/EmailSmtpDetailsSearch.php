<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EmailSmtpDetails;
use yii\db\Expression;

/**
* EmailSmtpDetailsSearch represents the model behind the search form about `app\models\EmailSmtpDetails`.
*/
class EmailSmtpDetailsSearch extends EmailSmtpDetails
{
  public $pageSize;
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id', 'company_id', 'is_default', 'pageSize'], 'integer'],
      [['service_type','smtp_host','smtp_username','smtp_password','smtp_port','smtp_encryption','mail_from'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);
    $query = EmailSmtpDetails::find()
    ->asArray();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
    ]);

    $query->andFilterWhere([
      'id' => $this->id,
      'company_id' => $this->company_id,
      'service_type' => $this->service_type,
    ]);
    $query->andWhere([
        'is','deleted_at',new Expression('null')
    ]);
    if($this->is_default!=null){
      $company_id = Yii::$app->user->identity->company_id;
      $defSmtpId = Yii::$app->appHelperFunctions->getCustomerSetting($company_id,'default_smtp');
      if($this->is_default==1){
        $query->andWhere(['id'=>$defSmtpId]);
      }else{
        $query->andWhere(['!=','id',$defSmtpId]);
      }

    }

    $query->andFilterWhere(['like', 'smtp_host', $this->smtp_host])
    ->andFilterWhere(['like', 'smtp_username', $this->smtp_username])
    ->andFilterWhere(['like', 'smtp_password', $this->smtp_password])
    ->andFilterWhere(['like', 'smtp_port', $this->smtp_port])
    ->andFilterWhere(['like', 'smtp_encryption', $this->smtp_encryption])
    ->andFilterWhere(['like', 'mail_from', $this->mail_from]);

    return $dataProvider;
  }
}
