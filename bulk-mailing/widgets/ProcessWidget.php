<?php
namespace app\widgets;

use Yii;
use app\models\Workflow;
use app\models\WorkflowModule;
use yii\helpers\ArrayHelper;

class ProcessWidget extends \yii\bootstrap\Widget
{
  public $form;
  public $type;
  public $model;
  public function init()
  {
    parent::init();
  }

  public function run()
  {
    $workflowLists=Yii::$app->appHelperFunctions->getWorkflowList($this->model->moduleTypeId);
    $workflowListsArr=ArrayHelper::map($workflowLists,"id","title");
    $workflowListStagesArr=[];
    if($workflowLists!=null){
      foreach($workflowLists as $workflowList){
        $workflowListStagesArr[]=$workflowList['id'];
      }
    }
    $workflowListStagesArr=Yii::$app->jsFunctions->getWorkFlowStagesList($workflowListStagesArr);
    return $this->render('process_selection',[
      'form'=>$this->form,
      'model'=>$this->model,
      'workflowLists'=>$workflowListsArr,
      'workflowListStagesArr'=>$workflowListStagesArr,
    ]);
  }
}
