<?php
namespace app\widgets;

use Yii;
use yii\bootstrap\Widget;

class PieChartWidget extends Widget
{
  public $heading,$data;
  public $type = 'pie';
  public $className = null;
  public function init()
  {
    parent::init();
  }

  public function run()
  {
    return $this->render('pie_chart',[
      'heading'=>$this->heading,
      'type'=>$this->type,
      'className'=>$this->className,
      'dataSet'=>$this->data,
      'wdgtid'=>$this->getId(),
    ]);
  }
}
