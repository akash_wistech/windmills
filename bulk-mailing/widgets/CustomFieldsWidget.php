<?php
namespace app\widgets;

use Yii;
use yii\helpers\ArrayHelper;

class CustomFieldsWidget extends \yii\bootstrap\Widget
{
  public $form;
  public $type;
  public $model;
  public function init()
  {
    parent::init();
  }

  public function run()
  {
    $inputFields=Yii::$app->inputHelperFunctions->getInputTypesByModule($this->type);

    return $this->render('custom_fields',[
      'form'=>$this->form,
      'model'=>$this->model,
      'inputFields'=>$inputFields,
    ]);
  }
}
