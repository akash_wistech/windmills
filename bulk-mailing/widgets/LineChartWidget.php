<?php
namespace app\widgets;

use Yii;
use yii\bootstrap\Widget;

class LineChartWidget extends Widget
{
  public $heading,$data;
  public $type = 'line';
  public $className = null;
  public function init()
  {
    parent::init();
  }

  public function run()
  {
    return $this->render('line_chart',[
      'heading'=>$this->heading,
      'type'=>$this->type,
      'className'=>$this->className,
      'dataSet'=>$this->data,
      'wdgtid'=>$this->getId(),
    ]);
  }
}
