<?php
namespace app\widgets;

use Yii;

class AdvanceSearchWidget extends \yii\bootstrap\Widget
{
  public $model;
  public function init()
  {
    parent::init();
  }

  public function run()
  {
    return $this->render('advance_search',[
      'model'=>$this->model,
    ]);
  }
}
