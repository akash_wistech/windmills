<?php
namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\models\NotificationSearch;

class NotificationsDropDown extends Widget
{
	public function run()
	{
		$searchModel = new NotificationSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->pagination->pageSize = 5;

		return $this->render('top_notifications_dropdown_menu',[
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}
}
?>
