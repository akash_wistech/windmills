<?php
namespace app\widgets;

use Yii;
use app\models\AttachmentForm;
use app\models\ActionLogAttachmentSearch;

class AttachmentsWidget extends \yii\bootstrap\Widget
{
  public $type;
  public $model;
  public function init()
  {
    parent::init();
  }

  public function run()
  {
    $searchModel = new ActionLogAttachmentSearch;
    $searchModel->module_type=$this->type;
    $searchModel->module_id=$this->model->id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('attachment_form',[
      'model'=>$this->model,
      'searchModel'=>$searchModel,
      'dataProvider'=>$dataProvider,
    ]);
  }
}
