<?php
use yii\helpers\Url;
use yii\widgets\ListView;
use app\assets\TopDropDownMenuAssets;
TopDropDownMenuAssets::register($this);

$count=$dataProvider->getTotalCOunt();
if($count>0){
$this->registerJs('
var iasTMNotifications = $("#tmNotificationSbarCnt").ias({
	container:  "#tm-notification",
	item:       ".tm-n-item",
	pagination: "#tm-notification .pagination",
	next:       "#tm-notification .pagination .next a",
	delay:      1200,
	negativeMargin:400
});

iasTMNotifications.extension(new IASSpinnerExtension({
	html: "<div class=\"ias-spinner\" style=\"text-align:center;\"><a href=\"javascript:;\" rel=\"canonical\"><img src=\"images/loading.gif\" height=\"25\"> '.Yii::t('app','Loading').'</a></div></div>",
}));

$("#tmNotificationSbarCnt").mCustomScrollbar({
	scrollbarPosition: "outside",
	autoHideScrollbar: true,
	callbacks:{
		onTotalScroll: function(){
			iasTMNotifications.next();
		}
	}
});
');
}
?>
<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
	<span class="dropdown-item dropdown-header"><?= Yii::t('app','Notifications')?></span>
	<div id="tmNotificationSbarCnt" class="scollerbarContentTopMenu" data-mcs-theme="dark">
		<?= ListView::widget([
			'dataProvider' => $dataProvider,
			'id' => 'tm-notification',
			'itemView' => '/notifications/_top_menu_item',
			'layout'=>'
				{items}
				{pager}
			',
			'emptyText'=>'
				<div class="empty-space">
					<img src="images/empty-spaces/no-notifications.svg" height="75" />
					<br />
					<br />
					'.Yii::t('app','No notifications found').'
				</div>
			',
		]);
		?>
	</div>
</div>
