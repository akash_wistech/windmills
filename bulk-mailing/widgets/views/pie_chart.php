<?php
use app\assets\ChartAsset;
ChartAsset::register($this);

$chartOptions = [];
$chartOptions['responsive']=true;
$chartOptions['maintainAspectRatio']=false;
$chartOptions['plugins']=[
  'datalabels'=>[
    'formatter'=>'showPercentage',
    'color'=>'#fff'
  ]
];

$this->registerJs('
var pieChartData'.$wdgtid.' = '.json_encode($dataSet).'
var pieChartCanvas'.$wdgtid.' = $("#pieChart'.$wdgtid.'").get(0).getContext("2d");

var pieChartOptions'.$wdgtid.' = {
  responsive: true,
  maintainAspectRatio: false,
  plugins: {
     datalabels: {
       formatter: (value, ctx) => {
         let datasets = ctx.chart.data.datasets;
         console.log();
         if (datasets.indexOf(ctx.dataset) === datasets.length - 1) {
           let sum = datasets[0].data.reduce((a, b) => parseFloat(a) + parseFloat(b), 0);
           let percentage = Math.round((value / sum) * 100);
           if(percentage<=10)percentage="";
           percentage!="" ? percentage+="%" : "";
           let labelsArr = ctx.chart.config.data.labels;
           if(percentage!=""){
             //percentage=labelsArr[ctx.dataIndex]+" ("+percentage+")";
           }
           return percentage;
         } else {
           return percentage;
         }
       },
       color: "#fff",
     }
   },
}

var pieChart'.$wdgtid.' = new Chart(pieChartCanvas'.$wdgtid.', {
  type: "'.$type.'",
  data: pieChartData'.$wdgtid.',
  options: pieChartOptions'.$wdgtid.'
});
');
?>
<div class="card<?= $className!=null ? ' card-'.$className : ''?>">
  <div class="card-header border-0">
    <div class="d-flex justify-content-between">
      <h3 class="card-title"><?= $heading?></h3>
    </div>
  </div>
  <div class="card-body">
    <div class="chartjs-size-monitor">
      <div class="chartjs-size-monitor-expand">
        <div class=""></div>
      </div>
      <div class="chartjs-size-monitor-shrink">
        <div class=""></div>
      </div>
    </div>
    <canvas id="pieChart<?= $wdgtid?>" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
  </div>
</div>
