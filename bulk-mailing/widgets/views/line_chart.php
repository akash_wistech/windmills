<?php
use app\assets\ChartAsset;
ChartAsset::register($this);

$chartOptions = [];
$chartOptions['responsive']=true;
$chartOptions['maintainAspectRatio']=false;
$chartOptions['plugins']=['datalabels'=>['display'=>false]];
if($type=='stacked'){
  $chartOptions['scales']=[
    'xAxes'=>[['stacked'=>true]],
    'yAxes'=>[['stacked'=>true]],
  ];
}else{
  $chartOptions['datasetFill']=false;
}

$this->registerJs('
var lineChartData'.$wdgtid.' = '.json_encode($dataSet).'
var lineChartCanvas'.$wdgtid.' = $("#lineChart'.$wdgtid.'").get(0).getContext("2d");

var lineChartOptions'.$wdgtid.' = '.json_encode($chartOptions).'

var lineChart'.$wdgtid.' = new Chart(lineChartCanvas'.$wdgtid.', {
  type: "'.($type=='stacked' ? 'bar' : $type).'",
  data: lineChartData'.$wdgtid.',
  options: lineChartOptions'.$wdgtid.'
})
');
?>
<div class="card<?= $className!=null ? ' card-'.$className : ''?>">
  <div class="card-header border-0">
    <div class="d-flex justify-content-between">
      <h3 class="card-title"><?= $heading?></h3>
    </div>
  </div>
  <div class="card-body">
    <div class="chartjs-size-monitor">
      <div class="chartjs-size-monitor-expand">
        <div class=""></div>
      </div>
      <div class="chartjs-size-monitor-shrink">
        <div class=""></div>
      </div>
    </div>
    <canvas id="lineChart<?= $wdgtid?>" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
  </div>
</div>
