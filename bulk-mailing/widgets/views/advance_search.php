<?php
use app\components\widgets\CustomModal;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$headerHtml = '<h5 class="modal-title">'.Yii::t('app','Advance Search').'</h5>';
$headerHtml.='<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
$headerHtml.='  <span aria-hidden="true">×</span>';
$headerHtml.='</button>';

CustomModal::begin([
  'headerOptions' => ['class' => 'modal-header'],
  'id' => 'advanceSearchModal',
  'size' => 'modal-lg',
  'header' => $headerHtml,
  'closeButton' => false,
]);
?>
<section class="prospect-form card">
  <?php $form = ActiveForm::begin(['action'=>Url::to(['index']),'method'=>'get']); ?>
  <div class="card-body">
  <div class="row">
    <div class="col-12 col-sm-6">
      <?= $form->field($model, 'cp_name')->textInput(['maxlength' => true])?>
    </div>
    <div class="col-12 col-sm-6">
      <?= $form->field($model, 'email')->textInput(['maxlength' => true])?>
    </div>
  </div>
  <?php
  $inputFields=Yii::$app->inputHelperFunctions->getInputTypesByModule($model->moduleTypeId);
  if($inputFields!=null){
    ?>
    <div class="row">
      <?php foreach($inputFields as $inputField){?>
        <div class="col-12 col-sm-6">
          <?php
          //Text, Qty, Number, Website, Email, Date, Date Range, Time
          if(
            $inputField['input_type']=='textarea' ||
            $inputField['input_type']=='tinymce' ||
            $inputField['input_type']=='text' ||
            $inputField['input_type']=='qtyField' ||
            $inputField['input_type']=='numberinput' ||
            $inputField['input_type']=='websiteinput' ||
            $inputField['input_type']=='emailinput' ||
            $inputField['input_type']=='autocomplete'
          ){
            $clsNames='form-control';
            $inputOpts['maxlength']=true;
            $inputOpts['autocomplete'] = 'off';
            if($inputField['input_type']=='qtyField' || $inputField['input_type']=='numberinput')$clsNames.=' numeric-field';
            $inputOpts['class']=$clsNames;

            $template=[];
            //echo $form->field($model, 'input_field['.$inputField['id'].']',$template)->textInput($inputOpts)->label($inputField['title']);
          }elseif(
            $inputField['input_type']=='select' ||
            $inputField['input_type']=='checkbox' ||
            $inputField['input_type']=='radio'
          ){
            $clsNames='form-control selectpicker';
            $selectOptions=[];
            $selectOptions['class']=$clsNames;
            $selectOptions['prompt']=Yii::t('app','Select');

            $selectArr=Yii::$app->inputHelperFunctions->getInputListArr($inputField);
            //echo $form->field($model, 'input_field['.$inputField['id'].']')->dropDownList($selectArr,$selectOptions)->label($inputField['title']);
          }
          ?>
        </div>
      <?php }?>
    </div>
  <?php }?>
    <div class="row">
      <div class="col-12 col-sm-6">
        <?= $form->field($model, 'status')->dropDownList(Yii::$app->helperFunctions->arrFilterStatus,['prompt'=>Yii::t('app','Select')])?>
      </div>
    </div>
  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), 'javascript:;', ['data-dismiss'=>'modal','class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</div>
<?php
CustomModal::end();
?>
