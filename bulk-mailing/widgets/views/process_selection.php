<?php
use yii\helpers\ArrayHelper;
use app\models\WorkflowDataItem;
$this->registerJs('
'.$workflowListStagesArr.'
$("#workflowsel").change(function() {
	$("#workflowstagesel").html("");
	array_list=workflowStages[$(this).val()];
	$(array_list).each(function (i) {
		$("#workflowstagesel").append("<option value=\""+array_list[i].value+"\""+(array_list[i].sel!="" ? array_list[i].sel : "")+">"+array_list[i].display+"</option>");
	});
});
');
$defStages=[];
$defStagesOpts=['prompt'=>Yii::t('app','Select'),'id'=>'workflowstagesel'];
$workflowRow=WorkflowDataItem::find()->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id])->asArray()->one();
if($workflowRow!=null){
  $model->workflow_id=$workflowRow['workflow_id'];
  $model->workflow_stage_id=$workflowRow['workflow_stage_id'];
  $defStages=Yii::$app->appHelperFunctions->getWorkflowStagesList($model->workflow_id);
  $defStages=ArrayHelper::map($defStages,"id","title");
  $defStagesOpts=['id'=>'workflowstagesel'];
}
?>
<section class="card card-warning">
  <header class="card-header">
    <h2 class="card-title"><?= Yii::t('app','Process')?></h2>
  </header>
  <div class="card-body">
    <div class="row">
      <div class="col-sm-6">
        <?= $form->field($model, 'workflow_id')->dropDownList($workflowLists,['prompt'=>Yii::t('app','Select'),'id'=>'workflowsel'])?>
      </div>
      <div class="col-sm-6">
        <?= $form->field($model, 'workflow_stage_id')->dropDownList($defStages,$defStagesOpts)?>
      </div>
    </div>
  </div>
</section>
