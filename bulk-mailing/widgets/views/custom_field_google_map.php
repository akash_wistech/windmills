<?php
use yii\bootstrap\Modal;
use yii\helpers\Url;
/*
$moGoogleMap=Yii::$app->inputHelperFunctions->getGoogleMapValue($model,$showGoogleMap);
if($moGoogleMap!=null){
  $model->map_lat=$moGoogleMap['map_lat_org'];
  $model->map_lng=$moGoogleMap['map_lng_org'];

  $model->map_location=$moGoogleMap['map_location'];
}

$model->map_lat=$model->map_lat!='' ? $model->map_lat : Yii::$app->params['defLatitude'];
$model->map_lng=$model->map_lng!='' ? $model->map_lng : Yii::$app->params['defLongitude'];
?>
<div class="col-md-12">
  <div class="row mt20 loc-map">
    <div class="col-md-4 loc-map-left"><img src="resources/img/map.svg" alt="Map"><h3 class="heading-3">Media Outlet Location<?= $hintHtml?></h3></div>
    <div class="col-md-4"></div>
    <div class="col-md-4">
      <div class="btn-map">
        <a href="javascript:;" data-toggle="modal" data-target="#map-by-latlng" class="btn btn-border-white"><?= Yii::t('app','Update map by Latitude / Longitude')?></a>
      </div>
    </div>
  </div>
  <div class="map">
    <div class="searchbox">
        <img class="burger" src="resources/img/burger-icon.svg" alt="Burger icon">
        <?= $form->field($model, 'input_field['.$showGoogleMap['id'].'][map_location]')->textInput(['id'=>'input_field_gmap_ml_'.$showGoogleMap['id'],'maxlength'=>true,'class'=>'searchbox__input','placeholder'=>'Search Google Maps'])->label(false) ?>
        <img src="resources/img/search.svg" alt="Search">
    </div>
    <div id="map_canvas"></div>
  </div>
</div>

<?php
Modal::begin([
  'headerOptions' => ['class' => 'modal-header clearfix'],
  'id' => 'map-by-latlng',
  'size' => 'modal-md',
 	'header' => '<h4 class="modal-title">'.Yii::t('app','Update Map Location By Latitude / Longitude').'</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>',
  'closeButton'=>false,
	'footer' => '
		<button type="button" class="btn btn-darft" data-dismiss="modal">Close</button>
		<button type="button" class="btn btn-blue" data-dismiss="modal">Done</button>',
]);
?>
<div class="modalContent">
	<div class="row">
		<div class="col-sm-6">
      <?= $form->field($model, 'map_lat',['labelOptions'=>['class'=>'form__label']])->textInput(['id'=>'input_field_gmap_mlat_'.$showGoogleMap['id'],'maxlength'=>true,'class'=>'form__input','placeholder'=>Yii::t('app','Latitude')])->label(Yii::t('app','Enter Latitude')) ?>
		</div>
		<div class="col-sm-6">
      <?= $form->field($model, 'map_lng',['labelOptions'=>['class'=>'form__label']])->textInput(['id'=>'input_field_gmap_mlng_'.$showGoogleMap['id'],'maxlength'=>true,'class'=>'form__input','placeholder'=>Yii::t('app','Longitude')])->label(Yii::t('app','Enter Longitude')) ?>
		</div>
	</div>
</div>
<?php
Modal::end();
?>
<?php
$markerIcon='';
//$markerIcon=Yii::$app->helperFunction->getImagePath("media-type",$model->mediaType->image,"mapicon");
	$this->registerJs('
  initLocationPicker('.$model->map_lat.','.$model->map_lng.','.($markerIcon!='' ? '"'.$markerIcon.'"' : 'undefined').');

	$("body").on("change", "#mediaoutlet-zone_id", function () {
		updateMap();
	});
	$("body").on("change", "#mediaoutlet-country_id", function () {
		updateMap();
	});
	$("body").on("blur", "#mediaoutlet-city", function () {
		updateMap();
	});
	');
?>
<script>

function updateMap()
{
	if($("#map_canvas").length>0){
		city=$("#mediaoutlet-city").val();
		zone=$("#mediaoutlet-zone_id option:selected").html();
		country=$("#mediaoutlet-country_id option:selected").html();
		address=city;
		if(zone!='' && zone!='Select State / Province'){
			if(address!=''){
				address += ", ";
			}
			address+=zone;
		}
		if(country!='' && address!=''){
			address += ", ";
		}
		address+=country;

		$.getJSON( "https://maps.googleapis.com/maps/api/geocode/json?address="+address+"&key=<?= Yii::$app->params['googleApiKey']?>", function( data ) {
		  var items = [];

			if(data.status=="OK"){
				console.log(data);
				locationObj=data.results[0].geometry.location
        $("#input_field_gmap_mlat_<?= $showGoogleMap['id']?>").val(locationObj.lat).trigger("blur");
        $("#input_field_gmap_mlng_<?= $showGoogleMap['id']?>").val(locationObj.lng).trigger("blur");
        $("#input_field_gmap_ml_<?= $showGoogleMap['id']?>").val(address);

				initLocationPicker(locationObj.lat,locationObj.lng,undefined);
			}
		});
	}
}

function initLocationPicker(lat,lng,icon)
{
	$("#map_canvas").locationpicker({
      location:{latitude: lat,longitude: lng},
      inputBinding: {
          latitudeInput: $("#input_field_gmap_mlat_<?= $showGoogleMap['id']?>"),
          longitudeInput: $("#input_field_gmap_mlng_<?= $showGoogleMap['id']?>"),
          locationNameInput: $("#input_field_gmap_ml_<?= $showGoogleMap['id']?>")
      },
      radius: 0,
      enableAutocomplete: true,
      markerIcon: icon,
  });
}
</script>
<?php */?>
