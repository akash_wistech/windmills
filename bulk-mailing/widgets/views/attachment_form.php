<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->registerJs('
initDropZone();


$("body").on("click", ".remove_file", function () {
	_this=$(this);
	id=_this.data("id");
	var _targetContainer=".panel-body";
	swal({
		title: "'.Yii::t('app','Confirmation').'",
		html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
		type: "warning",
		showCancelButton: true,
    confirmButtonColor: "#47a447",
		confirmButtonText: "'.Yii::t('app','Yes, delete it!').'",
		cancelButtonText: "'.Yii::t('app','Cancel').'",
	}).then((result) => {
    if (result.value) {
      App.blockUI({
  			message: "'.Yii::t('app','Please wait...').'",
  			target: _targetContainer,
  			overlayColor: "none",
  			cenrerY: true,
  			boxed: true
  		});
  		$.ajax({
  			url: "'.Url::to(['delete-file','tid'=>$model->id,'id'=>'']).'"+id,
  			dataType: "html",
  			type: "POST",
  			success: function(html) {
  				_this.parents("#attchment"+id).remove();
  				App.unblockUI($(_targetContainer));
  				swal({title: "'.Yii::t('app','Deleted!').'", html: "'.Yii::t('app','Image deleted.').'", type: "success", timer: 3000});
  			},
  			error: bbAlert
  		});
    }
  });
});
');
?>
<section class="card card-outline card-info mb-3 collapsed-card">
  <header class="card-header">
    <h2 class="card-title"><?= Yii::t('app','Add Attachments')?></h2>
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse">
        <i class="fas fa-plus"></i>
      </button>
    </div>
  </header>
  <div class="card-body" style="display:none;">
      <?php
    	/*if($model->attachmentInfo!=null){
    	?>
      <ul class="att-container">
        <?php foreach($model->attachmentInfo as $attachmentFile){?>
          <li id="attchment<?= $attachmentFile['id']?>" class="att-Item">
            <a href="javascript:;" class="btn btn-xs btn-danger remove_file" data-id="<?= $attachmentFile['id']?>"><i class="fa fa-times"></i></a>
            <a href="<?= $attachmentFile['link']?>" target="_blank">
              <img src="<?= $attachmentFile['iconPath']?>" class="img-responsive" alt="" />
            </a>
          </li>
        <?php }?>
      </ul>
    	<?php
    }*/
    	?>
		<?php $form = ActiveForm::begin(['id'=>'attfrm']); ?>
    <div id="hidden-files" class="hidden"></div>
		<div class="hidden"><?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?></div>
		<?php ActiveForm::end(); ?>
    <div id="dZU1pload" class="dropzone_my">
      <div class="hidden">
        <div id="preview-template">
          <div class="dz-preview dz-file-preview">
            <div class="dz-image"><img data-dz-thumbnail=""></div>
            <div class="dz-details">
              <div class="dz-size"><span data-dz-size=""></span></div>
              <div class="dz-filename"><span data-dz-name=""></span></div>
            </div>
            <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>
            <div class="dz-error-message"><span data-dz-errormessage=""></span></div>
            <div class="dz-success-mark">

              <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                <title>Check</title>
                <desc>Created with Sketch.</desc>
                <defs></defs>
                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                    <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
                </g>
              </svg>

            </div>
            <div class="dz-error-mark">

              <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                  <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                  <title>error</title>
                  <desc>Created with Sketch.</desc>
                  <defs></defs>
                  <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                      <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">
                          <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>
                      </g>
                  </g>
              </svg>

            </div>
          </div>
        </div>
      </div>
      <div class="dz-default dz-message">
        <?= Yii::t('app','Drop attachments here or click to upload.')?><br/>
        <small>(png,jpg,jpeg,gif,doc,docx,xls,xlsx,pdf)</small>
      </div>
    </div>
  </div>
</section>
<script>
function initDropZone()
{
		var previewNode = document.querySelector("#preview-template");
		previewNode.id = "";
		var previewTemplate = previewNode.parentNode.innerHTML;

		var mydropzone;
		Dropzone.autoDiscover = false;
		$("#dZU1pload").dropzone({
		  url: "<?= Url::to(['site/att-drop-upload','type'=>$model->moduleTypeId,'mid'=>$model->id])?>",
		  previewTemplate: previewTemplate,
		  addRemoveLinks: false,
			acceptedFiles: '.png,.jpg,.jpeg,.gif,.pdf,.doc,.docx,.xls,.xlsx',
		  init: function() {
		    mydropzone=this;
		  },
		  success: function (file, response) {
		    var fileName = response;
				$("#hidden-files").append("<input type=\"hidden\" name=\"AttachmentForm[attachment_file][]\" class=\"form-control\" value=\""+fileName+"\" />");
		    file.previewElement.classList.add("dz-success");
        file.previewElement.querySelector(".dz-progress").style.opacity = 0;
        file.previewElement.querySelector(".dz-success-mark").style.opacity = 1;
		  },
		  removedfile: function(file){
		    $("input[value=\""+file.name+"\"]").remove();
		    $(document).find(file.previewElement).remove();
		  },
		  error: function (file, response) {
		    file.previewElement.querySelector("[data-dz-errormessage]").textContent = response;
		    file.previewElement.classList.add("dz-error");
		  },
		  queuecomplete: function (file, response) {
				submitFiles();
		  }
		});
}
function submitFiles()
{
	var form = $("#attfrm");
	$.ajax({
   type: "POST",
   url: "<?= Url::to(['action-log/save-attachment','type'=>$model->moduleTypeId,'mid'=>$model->id])?>",
   data: form.serialize(), // serializes the form's elements.
	 dataType: 'json',
   success: function(response){
		 if(response["success"]){
			 $("#hidden-files").html("");
			 if($("#al-lv-container").length){
				 $.pjax.reload({container: "#al-lv-container", timeout: 2000});
			 }
			 reloadAlHistory();
			 toastr.success(response["success"]["msg"]);
		 }else{
			 Swal.fire({title: response["error"]["heading"], html: response["error"]["msg"], icon: "error"});
		 }
   }
 });
}
</script>
