<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\db\Expression;
use app\models\Company;
use app\models\User;
use app\models\PredefinedList;
use app\models\CustomFieldData;
use yii\data\Pagination;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Country;
use app\models\CountryList;
use app\models\Zone;
use app\models\ZoneList;

class SuggestionController extends DefController
{
  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['job-title','department'],
        'rules' => [
          [
            'actions' => ['job-title','department'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
    ];
  }

  /**
  * search all the job roles
  * @return mixed
  */
  public function actionJobRoles($query=null)
  {
    $this->checkLogin();
    $listId=Yii::$app->appHelperFunctions->getSetting('role_list_id');
    header('Content-type: application/json');
    $output_arrays=[];
    $results=PredefinedList::find()
    ->select(['id','title'])
    ->where([
      'and',
      ['like','title',$query],
      ['parent'=>$listId,'status'=>1],
      ['is','deleted_at',new Expression('null')]
    ])
    ->asArray()->all();
    if($results!=null){
      foreach($results as $result){
        $output_arrays[] = [
          'data'=>$result['id'],
          'value'=>trim($result['title']),
        ];
      }
    }
    // Send JSON to the client.
    echo json_encode(["suggestions"=>$output_arrays]);
    exit;
  }

  /**
  * search all the job title
  * @return mixed
  */
  public function actionJobTitles($query=null)
  {
    $this->checkLogin();
    $listId=Yii::$app->appHelperFunctions->getSetting('job_title_list_id');
    header('Content-type: application/json');
    $output_arrays=[];
    $results=PredefinedList::find()
    ->select(['id','title'])
    ->where([
      'and',
      ['like','title',$query],
      ['parent'=>$listId,'status'=>1],
      ['is','deleted_at',new Expression('null')]
    ])
    ->asArray()->all();
    if($results!=null){
      foreach($results as $result){
        $output_arrays[] = [
          'data'=>$result['id'],
          'value'=>trim($result['title']),
        ];
      }
    }
    // Send JSON to the client.
    echo json_encode(["suggestions"=>$output_arrays]);
    exit;
  }

  /**
  * search all the companies
  * @return mixed
  */
  public function actionCompany($query=null)
  {
    $this->checkLogin();
    header('Content-type: application/json');
    $output_arrays=[];
    $results=Company::find()
    ->select(['id','title'])
    ->where([
      'and',
      ['like','title',$query],
      ['is','deleted_at',new Expression('null')]
    ])
    ->asArray()->all();
    if($results!=null){
      foreach($results as $result){
        $output_arrays[] = [
          'data'=>$result['id'],
          'value'=>trim($result['title']),
        ];
      }
    }
    // Send JSON to the client.
    echo json_encode(["suggestions"=>$output_arrays]);
    exit;
  }

  /**
  * search all the departments
  * @return mixed
  */
  public function actionDepartments($query=null)
  {
    $this->checkLogin();
    $listId=Yii::$app->appHelperFunctions->getSetting('department_list_id');
    header('Content-type: application/json');
    $output_arrays=[];
    $results=PredefinedList::find()
    ->select(['id','title'])
    ->where([
      'and',
      ['like','title',$query],
      ['parent'=>$listId,'status'=>1],
      ['is','deleted_at',new Expression('null')]
    ])
    ->asArray()->all();
    if($results!=null){
      foreach($results as $result){
        $output_arrays[] = [
          'data'=>$result['id'],
          'value'=>trim($result['title']),
        ];
      }
    }
    // Send JSON to the client.
    echo json_encode(["suggestions"=>$output_arrays]);
    exit;
  }

  /**
  * search suggestions for auto complete column
  * @return mixed
  */
  public function actionInput($fid,$id=null,$query=null)
  {
    $this->checkLogin();
    header('Content-type: application/json');
    $output_arrays=[];
    if($query!=null){
      $query=CustomFieldData::find()
      ->select(['input_value'])
      ->where(['input_field_id'=>$fid])
      ->andFilterWhere(['like','input_value',$query])
      ->groupBy('input_value');

      $results=$query->asArray()->all();
      if($results!=null){
        foreach($results as $result){
          $output_arrays[] = [
            'data'=>'0',
            'value'=>trim($result['input_value']),
            'descp'=>"",
          ];
        }
      }
    }
    // Send JSON to the client.
    echo json_encode(["suggestions"=>$output_arrays]);
    exit;
  }

  /**
  * search from predefined lists
  * @return mixed
  */
  public function actionPredefinedList($lid,$id=null,$query=null)
  {
    $this->checkLogin();
    header('Content-type: application/json');
    $output_arrays=[];
    if($query!=null){
      $query=PredefinedList::find()
      ->select(['id','title'])
      ->where(['and',['parent'=>$lid,'status'=>1],['is','deleted_at',new Expression('null')]])
      ->andFilterWhere(['id'=>$id])
      ->andFilterWhere(['like','title',$query]);

      $results=$query->asArray()->all();
      if($results!=null){
        foreach($results as $result){
          $output_arrays[] = [
            'data'=>$result['id'],
            'value'=>trim($result['title']),
            'descp'=>"",
          ];
        }
      }
    }
    // Send JSON to the client.
    echo json_encode(["suggestions"=>$output_arrays]);
    exit;
  }

  /**
  * Loads States / Province for a country
  *
  * @return string
  */
  public function actionZoneOptions($country_id)
  {
    $html = '';
    $results=Yii::$app->appHelperFunctions->getCountryZoneListArr($country_id);
    if($results!=null){
      $html.='<option value="">'.Yii::t('app','Select').'</option>';
      foreach($results as $key=>$val){
        $html.='<option value="'.$key.'">'.$val.'</option>';
      }
    }
    echo $html;
    exit;
  }
  //
  // public function actionCountryImport($lastid=null)
  // {
  //   $countryParentId=Yii::$app->appHelperFunctions->getSetting('country_list_id');
  //   $zoneParentId=Yii::$app->appHelperFunctions->getSetting('zone_list_id');
  //   $results=CountryList::find()
  //   ->andFilterWhere(['>','id',$lastid])
  //   ->asArray()->orderBy(['id'=>SORT_ASC])->limit(1)->all();
  //   if($results!=null){
  //     foreach($results as $result){
  //       $lastid=$result['id'];
  //       //Add Into country list;
  //       $countryPDList=new PredefinedList;
  //       $countryPDList->company_id = 0;
  //       $countryPDList->parent = $countryParentId;
  //       $countryPDList->title = $result['title'];
  //       $countryPDList->status = 1;
  //       if($countryPDList->save()){
  //         //Save country Info
  //         $countryInfo = new Country;
  //         $countryInfo->id = $countryPDList->id;
  //         $countryInfo->title = $result['title'];
  //         $countryInfo->iso_code_2 = $result['iso_code_2'];
  //         $countryInfo->iso_code_3 = $result['iso_code_3'];
  //         $countryInfo->phone_code = $result['phone_code'];
  //         $countryInfo->phone_format = $result['phone_format'];
  //         $countryInfo->mobile_format = $result['mobile_format'];
  //         $countryInfo->address_format = $result['address_format'];
  //         $countryInfo->postcode_required = $result['postcode_required'];
  //         $countryInfo->save();
  //
  //         //Save Zones for this country
  //         $resultZones=ZoneList::find()->where(['country_id'=>$result['id']])->asArray()->all();
  //         if($resultZones!=null){
  //           foreach($resultZones as $resultZone){
  //             $zonePDList=new PredefinedList;
  //             $zonePDList->company_id = 0;
  //             $zonePDList->parent = $zoneParentId;
  //             $zonePDList->title = $resultZone['title'];
  //             $zonePDList->status = 1;
  //             if($zonePDList->save()){
  //               //Save country Info
  //               $zoneInfo = new Zone;
  //               $zoneInfo->id = $zonePDList->id;
  //               $zoneInfo->country_id = $countryPDList->id;
  //               $zoneInfo->title = $resultZone['title'];
  //               $zoneInfo->code = $resultZone['code'];
  //               $zoneInfo->save();
  //             }
  //           }
  //         }
  //       }
  //     }
  //     sleep(1);
  //     echo '<script>window.location.href="'.Url::to(['country-import','lastid'=>$lastid]).'"</script>';
  //     //echo '<hr />Updated Members <a href="'.Url::to(['country-import','lastid'=>$lastid]).'">Next Page</a>';
  //   }else{
  //     echo "<b>Finished</b>";
  //   }
  //   exit;
  // }
}
