<?php
namespace app\controllers;

use Yii;
use app\components\helpers\DefController;
use yii\db\Expression;
use app\models\BlackListedEmail;
use app\models\DroppedEmail;

use app\models\Campaign;
use app\models\CampaignEmail;
use app\models\User;

/**
 * AwsController implements the Aws Ses web hooks.
 */
class AwsController extends DefController
{
	public function beforeAction($action)
	{
      $this->enableCsrfValidation = false;
      return parent::beforeAction($action);
  }

	public function actionListener()
	{
    $postBody = file_get_contents('php://input');
    // JSON decode the body to an array of message data
    $message = json_decode($postBody, true);
    if ($message) {
		//Yii::info($message,'my_custom_log');
        // Do something with the data
				$message = $message['Message'];
        $messageArr = json_decode($message,true);
        $campaign_id=$messageArr['mail']['tags']['campaign_id'][0];
				$recipient=$messageArr['mail']['destination'][0];
				$event=$messageArr['eventType'];

				$reason='';
				if($event=='Bounce'){
					$reason = $messageArr['bounce']['bouncedRecipients'][0]['diagnosticCode'];
				}
				if($event=='Reject'){
					$reason='';
				}

				$data=[
					'campaign_id'=>$campaign_id,
					'event'=>$event,
					'recipient'=>$recipient,
					'reason'=>$reason,
				];
				// Yii::info($messageArr,'my_custom_log');
				$this->updateTracking($data);
    }
	}

	public function updateTracking($post)
	{
		if(isset($post['campaign_id']) && $post['campaign_id']!=null){
			$connection = \Yii::$app->db;
			$campaignId=$post['campaign_id'];
			//Updating click
			if($post['event'] == 'Click') {
				$connection->createCommand("update ".CampaignEmail::tableName()." set click=(click+1) where email=:email and campaign_id=:campaign_id",[':email'=>$post['recipient'], ':campaign_id' => $campaignId])->execute();
				$connection->createCommand("update ".Campaign::tableName()." set click=(click+1) where id=:id",[':id'=>$campaignId])->execute();
			}
			//Updating open
			if($post['event'] == 'Open') {
				$connection->createCommand("update ".CampaignEmail::tableName()." set open=(open+1) where email=:email and campaign_id=:campaign_id",[':email'=>$post['recipient'], ':campaign_id' => $campaignId])->execute();
				$connection->createCommand("update ".Campaign::tableName()." set open=(open+1) where id=:id",[':id'=>$campaignId])->execute();
			}
			//Updating delivery
			if($post['event'] == 'Delivery') {
				$connection->createCommand("update ".CampaignEmail::tableName()." set delivered=1 where email=:email and campaign_id=:campaign_id",[':email'=>$post['recipient'], ':campaign_id' => $campaignId])->execute();
				$connection->createCommand("update ".Campaign::tableName()." set delivered=(delivered+1) where id=:id",[':id'=>$campaignId])->execute();
				$responseMsg = 'Delivery Received.';
			}
			//Updating dropped
			if($post['event'] == 'Reject') {
				$connection->createCommand("update ".CampaignEmail::tableName()." set soft_bounced=1 where email=:email and campaign_id=:campaign_id",[':email'=>$post['recipient'], ':campaign_id' => $campaignId])->execute();
				$connection->createCommand("update ".Campaign::tableName()." set soft_bounced=(soft_bounced+1) where id=:id",[':id'=>$campaignId])->execute();

				$softBounced=DroppedEmail::find()->where(['email' => $post['recipient']])->one();
				if($softBounced==null){
					$softBounced=new DroppedEmail;
					$softBounced->email = $post['recipient'];
					$softBounced->reason = $post['reason'];
					$softBounced->save();
				}else{
					if($softBounced->deleted_at!=null){
						$connection->createCommand()->update(DroppedEmail::tableName(), ['deleted_at' => new Expression('null')], 'id=:id', [':id' => $softBounced->id])->execute();
					}
				}

				$responseMsg = 'Drop Received.';
			}
			//Updating bounced
			if($post['event'] == 'Bounce') {
				$connection->createCommand("update ".CampaignEmail::tableName()." set hard_bounced=1 where email=:email and campaign_id=:campaign_id",[':email'=>$post['recipient'], ':campaign_id' => $campaignId])->execute();
				$connection->createCommand("update ".Campaign::tableName()." set hard_bounced=(hard_bounced+1) where id=:id",[':id'=>$campaignId])->execute();

				$blackListed=BlackListedEmail::find()->where(['email' => $post['recipient']])->one();
				if($blackListed==null){
					$blackListed=new BlackListedEmail;
					$blackListed->email = $post['recipient'];
					$blackListed->reason = $post['reason'];
					$blackListed->save();
				}else{
					if($blackListed->deleted_at==null){
						$connection->createCommand()->update(BlackListedEmail::tableName(), ['deleted_at' => new Expression('null')], 'id=:id', [':id' => $blackListed->id])->execute();
					}
				}

				$responseMsg = 'Bounce Received.';
			}
			//Updating Spam
			if($post['event'] == 'Complaint') {
				$connection->createCommand("update ".CampaignEmail::tableName()." set spam_complain=1 where email=:email and campaign_id=:campaign_id",[':email'=>$post['recipient'], ':campaign_id' => $campaignId])->execute();
				$connection->createCommand("update ".Campaign::tableName()." set spam_complain=(spam_complain+1) where id=:id",[':id'=>$campaignId])->execute();
				$responseMsg = 'Complaint Received.';
			}
		}
	}
}
