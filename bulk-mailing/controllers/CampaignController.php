<?php

namespace app\controllers;

use Yii;
use app\models\Campaign;
use app\models\CampaignAttachment;
use app\models\CampaignSearch;
use app\models\CampaignEmailSearch;
use app\models\EmailSmtpDetails;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\db\Expression;
use Twilio\Rest\Client;

/**
* CampaignController implements the CRUD actions for Campaign model.
*/
class CampaignController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * List all Campaign models.
  * @return mixed
  */
  public function actionIndex()
  {
      // Find your Account SID and Auth Token at twilio.com/console
// and set the environment variables. See http://twil.io/secure
   /*   $sid = "AC7ccee166e90e63654b21b8d76aac7163";
      $token ="0b513bce9d29b77a91265cc8571d1746";
      $twilio = new Client($sid, $token);

      $message = $twilio->messages
          ->create("whatsapp:+971522924846", // to
              [
                  "from" => "whatsapp:+971529877843",
                  "body" => "bhai bs ho gai hy"
              ]
          );

      print($message->sid);
      die;*/
      $this->checkLogin();
    $searchModel = new CampaignSearch();
    $searchModel->company_id = Yii::$app->user->identity->company_id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new Campaign model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionView($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);
    $searchModel = new CampaignEmailSearch();
    $searchModel->campaign_id = $model->id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

      return $this->render('view', [
          'model' => $model,
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
      ]);
  }

  /**
  * Creates a new Campaign model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkLogin();
    $model = new Campaign();
    $model->company_id=Yii::$app->user->identity->company_id;
    $model->status=0;
    $model->subject='new campaign';
    $model->from_id=Yii::$app->user->identity->id;
    $model->reply_to=Yii::$app->user->identity->email;
    $smtpDetail = EmailSmtpDetails::find()
    ->select(['id'])
    ->where(['and',['company_id'=>Yii::$app->user->identity->company_id,'status'=>1],['is','deleted_at',new Expression('null')]])
    ->asArray()->one();
    if($smtpDetail!=null){
        $model->smtp_id=[$smtpDetail['id']];
    }
    if($model->save()){
        return $this->redirect(['update','id'=>$model->id]);
    }else{
        Yii::$app->getSession()->addFlash('error', Yii::t('app','Something went wrong, Please try again or consult with the developer!'));
        return $this->redirect(['index']);
    }
  }

  /**
  * Updates an existing Campaign model.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionUpdate($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);
    $model->list_id = ArrayHelper::map($model->listIdz,"list_id","list_id");
    $model->smtp_id = ArrayHelper::map($model->smtpIdz,"smtp_id","smtp_id");

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Campaign saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('update', [
      'model' => $model,
    ]);
  }

  /**
   * Deletes an existing Campaign model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id is used to delete model object
   * @return mixed
   */
  public function actionDeleteFile($cid,$fn)
  {
    $this->checkLogin();
      $this->findAttachmentModel($cid,$fn)->softdelete();
      echo 'success';
  }

  /**
  * Creates a new model.
  */
  protected function newModel()
  {
    $model = new Campaign;
    return $model;
  }

  /**
  * Finds the Campaign model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Campaign the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = Campaign::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }

  /**
   * Finds the CampaignAttachment model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id is used to find model object
   * @return CampaignAttachment the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findAttachmentModel($cid,$fn)
  {
      if (($model = CampaignAttachment::findOne(['campaign_id'=>$cid,'file_name'=>$fn])) !== null) {
          return $model;
      } else {
          throw new NotFoundHttpException('The requested page does not exist.');
      }
  }
}
