<?php
namespace app\controllers;

use Yii;
use app\components\helpers\DefController;
use Mailgun\Mailgun;
use yii\db\Expression;
use app\models\BlackListedEmail;
use app\models\DroppedEmail;

use app\models\Campaign;
use app\models\CampaignEmail;
use app\models\User;

/**
 * MailGunController implements the mailgun web hooks.
 */
class MailGunController extends DefController
{
	public function beforeAction($action)
	{
      $this->enableCsrfValidation = false;
      return parent::beforeAction($action);
  }

	public function actionStats()
	{
		$responseMsg='';
		Yii::info($_POST,'my_custom_log');
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			if(isset($_POST['timestamp']) && isset($_POST['token']) && isset($_POST['signature']))// && $this->verify(Yii::$app->params['mailGunApi'], $_POST['token'], $_POST['timestamp'], $_POST['signature'])
			{
				if($_POST['event'] == 'complained' || $_POST['event'] == 'bounced' || $_POST['event'] == 'dropped' || $_POST['event'] == 'unsubscribed'){

					switch ($_POST['event']) {
						case "complained":
							$reason = "[Mailgun][".$_POST['event']."] Spam Complaint";
							break;
						case "bounced":
							$reason = "[Mailgun][".$_POST['event']."] Bounced Email";
							break;
						case "dropped":
							$reason = "[Mailgun][".$_POST['event']."] Dropped Email";
							break;
						default:
							$reason = "[Mailgun][".$_POST['event']."] Unsubscribed";
					}
					$description = $_POST['description'];
					$reason.=' ('.$description.')';
					$connection = \Yii::$app->db;

					//Saving Bounce
					if($_POST['event'] == 'bounced'){

						$blackListed=BlackListedEmail::find()->where(['email' => $_POST['recipient']])->one();
						if($blackListed==null){
							$blackListed=new BlackListedEmail;
							$blackListed->email = $_POST['recipient'];
							$blackListed->reason = $reason;
							$blackListed->save();
						}else{
							if($blackListed->deleted_at==null){
								$connection->createCommand()->update(BlackListedEmail::tableName(), ['deleted_at' => new Expression('null')], 'id=:id', [':id' => $blackListed->id])->execute();
							}
						}
					}

					//Saving Dropped
					if($_POST['event'] == 'dropped'){

						$softBounced=DroppedEmail::find()->where(['email' => $_POST['recipient']])->one();
						if($softBounced==null){
							$softBounced=new DroppedEmail;
							$softBounced->email = $_POST['recipient'];
							$softBounced->reason = $reason;
							$softBounced->save();
						}else{
							if($softBounced->deleted_at!=null){
								$connection->createCommand()->update(DroppedEmail::tableName(), ['deleted_at' => new Expression('null')], 'id=:id', [':id' => $softBounced->id])->execute();
							}
						}
					}
					$responseMsg = 'Remove Received.';
				}
				$this->updateTracking($_POST,'');
			}
		}
		Yii::$app->response->statusCode = 200;
		Yii::$app->response->statusText = 'Success';
		Yii::$app->response->content = $responseMsg;
		Yii::$app->response->send();
	}

	private function verify($apiKey, $token, $timestamp, $signature)
	{
		//check if the timestamp is fresh
		if (abs(time() - $timestamp) > 15) {
			return false;
		}

		//returns true if signature is valid
		return hash_hmac('sha256', $timestamp.$token, $apiKey) === $signature;
	}

	public function updateTracking($post,$type)
	{
		if(isset($post['campaign_id']) && $post['campaign_id']!=null){
			$connection = \Yii::$app->db;
			$campaignId=$post['campaign_id'];
			//Updating click
			
		Yii::info($post['event'],'my_custom_log');
		Yii::info($post['recipient'],'my_custom_log');
		Yii::info($campaignId,'my_custom_log');
		
			if($post['event'] == 'clicked') {
				$connection->createCommand("update ".CampaignEmail::tableName()." set click=(click+1) where email=:email and campaign_id=:campaign_id",[':email'=>$post['recipient'], ':campaign_id' => $campaignId])->execute();
				$connection->createCommand("update ".Campaign::tableName()." set click=(click+1) where id=:id",[':id'=>$campaignId])->execute();
			}
			//Updating open
			if($post['event'] == 'opened') {
				$connection->createCommand("update ".CampaignEmail::tableName()." set open=(open+1) where email=:email and campaign_id=:campaign_id",[':email'=>$post['recipient'], ':campaign_id' => $campaignId])->execute();
				$connection->createCommand("update ".Campaign::tableName()." set open=(open+1) where id=:id",[':id'=>$campaignId])->execute();
			}
			//Updating delivery
			if($post['event'] == 'delivered') {
				$connection->createCommand("update ".CampaignEmail::tableName()." set delivered=1 where email=:email and campaign_id=:campaign_id",[':email'=>$post['recipient'], ':campaign_id' => $campaignId])->execute();
				$connection->createCommand("update ".Campaign::tableName()." set delivered=(delivered+1) where id=:id",[':id'=>$campaignId])->execute();
				$responseMsg = 'Delivery Received.';
			}
			//Updating dropped
			if($post['event'] == 'dropped') {
				$connection->createCommand("update ".CampaignEmail::tableName()." set soft_bounced=1 where email=:email and campaign_id=:campaign_id",[':email'=>$post['recipient'], ':campaign_id' => $campaignId])->execute();
				$connection->createCommand("update ".Campaign::tableName()." set soft_bounced=(soft_bounced+1) where id=:id",[':id'=>$campaignId])->execute();
				$responseMsg = 'Drop Received.';
			}
			//Updating bounced
			if($post['event'] == 'bounced') {
				$connection->createCommand("update ".CampaignEmail::tableName()." set hard_bounced=1 where email=:email and campaign_id=:campaign_id",[':email'=>$post['recipient'], ':campaign_id' => $campaignId])->execute();
				$connection->createCommand("update ".Campaign::tableName()." set hard_bounced=(hard_bounced+1) where id=:id",[':id'=>$campaignId])->execute();
				$responseMsg = 'Bounce Received.';
			}
			//Updating Spam
			if($post['event'] == 'complained') {
				$connection->createCommand("update ".CampaignEmail::tableName()." set spam_complain=1 where email=:email and campaign_id=:campaign_id",[':email'=>$post['recipient'], ':campaign_id' => $campaignId])->execute();
				$connection->createCommand("update ".Campaign::tableName()." set spam_complain=(spam_complain+1) where id=:id",[':id'=>$campaignId])->execute();
				$responseMsg = 'Complaint Received.';
			}
		}
	}
}
