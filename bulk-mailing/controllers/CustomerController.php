<?php

namespace app\controllers;

use Yii;
use app\models\Customer;
use app\models\CustomerSearch;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* CustomerController implements the CRUD actions for Customer model.
*/
class CustomerController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all Customer models.
  * @return mixed
  */
  public function actionIndex()
  {
    $this->checkAdmin();
    $searchModel = new CustomerSearch();
    $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new Customer model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkAdmin();
    $model = new Customer();
    $model->permission_group_id=Yii::$app->appHelperFunctions->getSetting('cust_perm_id');
    $model->status=1;

    $request = Yii::$app->request;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        if($request->isAjax){
          $msg['success']=['heading'=>Yii::t('app','Saved'),'msg'=>Yii::t('app','Customer saved successfully'),'id'=>$model->id,'title'=>$model->title];
          echo json_encode($msg);
          die();
          exit;
        }else{
          Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
          return $this->redirect(['index']);
        }
      }else{
        if($request->isAjax){
          if($model->hasErrors()){
            foreach($model->getErrors() as $error){
              if(count($error)>0){
                foreach($error as $key=>$val){
                  $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>$val];
                  echo json_encode($msg);
                  die();
                  exit;
                }
              }
            }
          }
        }else{
          if($model->hasErrors()){
            foreach($model->getErrors() as $error){
              if(count($error)>0){
                foreach($error as $key=>$val){
                  Yii::$app->getSession()->addFlash('error', $val);
                }
              }
            }
          }
        }
      }
    }
    if($request->isAjax){
      return $this->renderAjax('_form_short', [
        'model' => $model,
      ]);
    }else {
      return $this->render('create', [
        'model' => $model,
      ]);
    }
  }

  /**
  * Updates an existing Customer model.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionUpdate($id)
  {
    $this->checkAdmin();
    $model = $this->findModel($id);
    $model->manager_id = ArrayHelper::map($model->managerIdz,"staff_id","staff_id");

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('update', [
      'model' => $model,
    ]);
  }

  /**
  * Creates a new model.
  */
  protected function newModel()
  {
    $model = new Customer;
    return $model;
  }

  /**
  * Finds the Customer model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Customer the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = Customer::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
