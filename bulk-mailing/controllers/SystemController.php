<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Setting;
use app\models\CustomerSetting;
use app\models\SettingForm;
use app\models\CustomerSettingForm;

class SystemController extends DefController
{
  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['setting'],
        'rules' => [
          [
            'actions' => ['setting'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
    ];
  }

  /**
  * Displays Setting Page.
  *
  * @return string
  */
  public function actionSetting()
  {
    $this->checkLogin();
    if(in_array(Yii::$app->user->identity->user_type,[0,1])){
      //Customer Settings
      $model=new CustomerSettingForm;
      $model->company_id=Yii::$app->user->identity->company_id;
      if ($model->load(Yii::$app->request->post()) && $model->save()) {
        Yii::$app->getSession()->setFlash('success', 'Settings saved successfully');
        return $this->redirect(['setting']);
      }

      $sValues['CustomerSettingForm']=[];
      $settings=CustomerSetting::find()->where(['company_id'=>$model->company_id])->all();
      foreach($settings as $record){
        $sValues['CustomerSettingForm'][$record['config_name']]=$record['config_value'];
      }
      $model->load($sValues);


      return $this->render('customer_setting',['model'=>$model]);
    }else{
      //App Settings
      $model=new SettingForm;
      if ($model->load(Yii::$app->request->post()) && $model->save()) {
        Yii::$app->getSession()->setFlash('success', 'Settings saved successfully');
        return $this->redirect(['setting']);
      }

      $sValues['SettingForm']=[];
      $settings=Setting::find()->all();
      foreach($settings as $record){
        $sValues['SettingForm'][$record['config_name']]=$record['config_value'];
      }
      $model->load($sValues);


      return $this->render('setting',['model'=>$model]);
    }
  }
}
