<?php

namespace app\controllers;

use Yii;
use app\models\ApiSource;
use app\models\ApiSourceSearch;
use app\models\ApiMapForm;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* ApiSourceController implements the CRUD actions for ApiSource model.
*/
class ApiSourceController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all ApiSource models.
  * @return mixed
  */
  public function actionIndex()
  {
    $this->checkLogin();
    $searchModel = new ApiSourceSearch();
    $searchModel->company_id = Yii::$app->user->identity->company_id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new Company model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkLogin();
    $model = new ApiSource();
    $model->company_id = Yii::$app->user->identity->company_id;
    $request = Yii::$app->request;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Api source saved successfully'));
        return $this->redirect(['mapping','id'=>$model->id]);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->setFlash('error',$val);
              }
            }
          }
        }
      }
    }
    return $this->render('create', [
        'model' => $model,
    ]);
  }

  /**
  * Updates an existing ApiSource model.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionUpdate($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Api source saved successfully'));
        return $this->redirect(['mapping','id'=>$model->id]);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('update', [
      'model' => $model,
    ]);
  }

  /**
  * Mapping for an existing ApiSource model.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionMapping($id)
  {
    $this->checkLogin();
    $apiModel = $this->findModel($id);
    $model = new ApiMapForm;
    $model->api_source_id = $apiModel->id;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Api mapped successfully'));
        return $this->redirect(['mapping','id'=>$apiModel->id]);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    $curlResponse = Yii::$app->helperFunctions->curlReq($apiModel->url);
    
    return $this->render('mapping', [
      'apiModel' => $apiModel,
      'model' => $model,
      'curlResponse' => $curlResponse,
    ]);
  }

  /**
  * Creates a new model.
  */
  protected function newModel()
  {
    $model = new ApiSource;
    return $model;
  }

  /**
  * Finds the ApiSource model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return ApiSource the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = ApiSource::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
