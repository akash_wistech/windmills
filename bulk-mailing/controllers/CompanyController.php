<?php

namespace app\controllers;

use app\models\BilalContacts;
use app\models\IcapContacts;
use app\models\ListItem;
use app\models\Prospect;
use Yii;
use app\models\Company;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\IcaiContacts;


/**
* CompanyController implements the CRUD actions for Company model.
*/
class CompanyController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Creates a new Company model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
//icap
     /* $all_ica_contacts = IcapContacts::find()->all();

     foreach ($all_ica_contacts as $record){
         $data_to_be_save = new Prospect();
         $data_to_be_save->company_id=1;
         $data_to_be_save->firstname=$record->firstname;
         $data_to_be_save->lastname=$record->lastname;
         $data_to_be_save->email=$record->email;
         $data_to_be_save->list_type='icap';
       //  $data_to_be_save->emails=array($record->email);
         $data_to_be_save->phone_numbers=array($record->phone_code.''.$record->phone);

         if(!$data_to_be_save->save()){

         }else{
             $listItem=new ListItem();
             $listItem->list_id=1;
             $listItem->module_id=$data_to_be_save->id;
             $listItem->module_type='prospect';
             $listItem->save();
         }


     }*/

     //icai
      /*$all_ica_contacts = IcaiContacts::find()->all();

      foreach ($all_ica_contacts as $record){
          $data_to_be_save = new Prospect();
          $data_to_be_save->company_id=1;
          $data_to_be_save->firstname=$record->firstname;
          $data_to_be_save->lastname=$record->lastname;
          $data_to_be_save->email=$record->email;
          $data_to_be_save->list_type='icai';
          //  $data_to_be_save->emails=array($record->email);
          $data_to_be_save->phone_numbers=array($record->phone_code.''.$record->phone);

          if(!$data_to_be_save->save()){
          }else{
              $listItem=new ListItem();
              $listItem->list_id=3;
              $listItem->module_id=$data_to_be_save->id;
              $listItem->module_type='prospect';
              $listItem->save();
          }


      }*/



      //bilal contacts

      $all_ica_contacts = BilalContacts::find()->all();

      foreach ($all_ica_contacts as $record){
          $data_to_be_save = new Prospect();
          $data_to_be_save->company_id=1;
          $data_to_be_save->firstname=$record->firstname;
          $data_to_be_save->lastname=$record->lastname;
          $data_to_be_save->email=$record->email;
          $data_to_be_save->list_type='bilal';
          //  $data_to_be_save->emails=array($record->email);
          $data_to_be_save->phone_numbers=array($record->phone_code.''.$record->phone);

          if(!$data_to_be_save->save()){
          }else{
              $listItem=new ListItem();
              $listItem->list_id=4;
              $listItem->module_id=$data_to_be_save->id;
              $listItem->module_type='prospect';
              $listItem->save();
          }


      }


      echo "<pre>";
      print_r($all_ica_contacts);
      die;






    $this->checkLogin();
    $model = new Company();
    $model->company_id = Yii::$app->user->identity->company_id;
    $request = Yii::$app->request;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        if($request->isAjax){
          $msg['success']=['heading'=>Yii::t('app','Saved'),'msg'=>Yii::t('app','Company saved successfully')];
          echo json_encode($msg);
          die();
          exit;
        }else{
          Yii::$app->getSession()->setFlash('success', Yii::t('app','Company saved successfully'));
          return $this->redirect(['index']);
        }
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                if($request->isAjax){
                  $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>$val];
                  echo json_encode($msg);
                  die();
                  exit;
                }else{
                  Yii::$app->getSession()->setFlash('error',$val);
                }
              }
            }
          }
        }
      }
    }
    if($request->isAjax){
      return $this->renderAjax('_form', [
          'model' => $model,
      ]);
    }else{
      return $this->render('create', [
          'model' => $model,
      ]);
    }
  }
}
