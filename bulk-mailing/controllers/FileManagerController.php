<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use app\components\helpers\Premailer;
use app\models\Template;
use app\models\TemplateSearch;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* FileManagerController
*/
class FileManagerController extends DefController
{
  public $enableCsrfValidation = false;
  public $http_response_code = 200;
  public $base_dir = '/';
  public $uploads_dir = 'uploads/';
  public $uploads_url = 'uploads/';
  public $thumbnail_dir = '/uploads/thumbnails/';
  public $thumbnail_width = 90;
  public $thumbnail_height = 90;
  /**
  * handler for img requests
  */
  public function actionImage($method,$params)
  {
    // echo Yii::$app->params['uploads_abs_path'];
    // die();
    $params = explode(",",$params);
    $width = (int) $params[0];
    $height = (int) $params[1];
    if($method == "placeholder")
    {
      $image = new \Imagick();
      $image->newImage($width, $height, "#707070");
      $image->setImageFormat("png");
      $x = 0;
      $y = 0;
      $size = 40;
      $draw = new \ImagickDraw();
      while($y < $height){
        $draw->setFillColor("#808080");
        $points = [
          ["x" => $x, "y" => $y],
          ["x" => $x + $size, "y" => $y],
          ["x" => $x + $size * 2, "y" => $y + $size],
          ["x" => $x + $size * 2, "y" => $y + $size * 2]
      ];
        $draw->polygon($points);
        $points = [
          ["x" => $x, "y" => $y + $size],
          ["x" => $x + $size, "y" => $y + $size * 2],
          ["x" => $x, "y" => $y + $size * 2]
      ];
        $draw->polygon($points);
        $x += $size * 2;
        if($x > $width)
        {
          $x = 0;
          $y += $size * 2;
        }
      }
      $draw->setFillColor("#B0B0B0");
      $draw->setFontSize($width / 5);
      $draw->setFontWeight(800);
      $draw->setGravity(\Imagick::GRAVITY_CENTER);
      $draw->annotation(0, 0, $width . " x " . $height);
      $image->drawImage($draw);
      header("Content-type: image/png");
      echo $image;
      exit;
    }else{
      $file_name = $_GET["src"];
      $path_parts = pathinfo($file_name);
      switch ($path_parts["extension"]){
        case "png":
        $mime_type = "image/png";
        break;
        case "gif":
        $mime_type = "image/gif";
        break;
        default:
        $mime_type = "image/jpeg";
        break;
      }
      $file_name = $path_parts["basename"];
      $image = $this->ResizeImage($file_name, $method, $width, $height);
      header("Content-type: " . $mime_type);
      echo $image;
    }
    http_response_code($this->http_response_code);
  }

  /**
  * handler for dl requests
  */
  public function actionDownload()
  {
    $baseUrl = Url::base();
    $uploadsAbsPath=Yii::$app->params['uploads_abs_path'];
    $uploadsRelPath=Yii::$app->params['uploads_rel_path'];
    $staticAbsPath=Yii::$app->params['static_abs_path'];
    $staticRelPath=Yii::$app->params['static_rel_path'];
    global $http_return_code;
    /* run this puppy through premailer */
    // $premailer = Premailer::html($_POST["html"], true, "hpricot", $baseUrl);
    $html = $_POST["html"];

    /* create static versions of resized images */
    $matches = [];
    $num_full_pattern_matches = preg_match_all('#<img.*?src="([^"]*?\/[^/]*\.[^"]+)#i', $html, $matches);
    for($i = 0; $i < $num_full_pattern_matches; $i++)
    {
      if(stripos($matches[1][$i], "/campaign-image?src=") !== FALSE)
      {
        $src_matches = [];
        if(preg_match('#/campaign-image\?src=(.*)&amp;method=(.*)&amp;params=(.*)#i', $matches[1][$i], $src_matches) !== FALSE)
        {
          $file_name = urldecode($src_matches[1]);
//echo '1,'.$file_name.'<br />';
          $file_name = substr($file_name, strlen($baseUrl . $uploadsRelPath));
//echo '2,'.$file_name.'<br />';
//die();
          $method = urldecode($src_matches[2]);
          $params = urldecode($src_matches[3]);
          $params = explode(",", $params);
          $width = (int) $params[0];
          $height = (int) $params[1];
          $static_file_name = $method . "_" . $width . "x" . $height . "_" . $file_name;
          $html = str_ireplace($matches[1][$i], $baseUrl . $staticRelPath . urlencode($static_file_name), $html);
          $image = $this->ResizeImage($file_name, $method, $width, $height);
          $image->writeImage($staticAbsPath . $static_file_name);
        }
      }
    }
    /* perform the requested action */

    switch ($_POST["action"])
    {
      case "download":
      {
        header("Content-Type: application/force-download");
        header("Content-Disposition: attachment; filename=\"" . $_POST["filename"] . "\"");
        header("Content-Length: " . strlen($html));
        echo $html;
        break;
      }
      case "email":
      {
        $smtpDetail = Yii::$app->appHelperFunctions->defaultSmtpInfo;
        if($smtpDetail!=null){
          Yii::$app->mailer->setTransport(
            [
              'class' => 'Swift_SmtpTransport',
              'host' => $smtpDetail['smtp_host'],
              'username' => $smtpDetail['smtp_username'],
              'password' => $smtpDetail['smtp_password'],
              'port' => $smtpDetail['smtp_port'],
              'encryption' => $smtpDetail['smtp_encryption'],
            ]
          );

          $message = \Yii::$app->mailer
          ->compose(['html' => 'default-html','text' => 'default-text'],['htmlBody'=>$html,'textBody'=>strip_tags($html)])
          ->setFrom([Yii::$app->user->identity->email => Yii::$app->user->identity->company->title . ' '.Yii::$app->params['fromAppName']])
          ->setTo([$_POST["rcpt"]])
          ->setSubject($_POST["subject"]);

          if($message->send()){
            $http_return_code = 200;
            http_response_code($http_return_code);
            return;
          }else{
            $http_return_code = 500;
            http_response_code($http_return_code);
            return;
          }
        }else{
          $http_return_code = 500;
          http_response_code($http_return_code);
          return;
        }
        break;
      }
    }
    http_response_code($this->http_response_code);
  }

  /**
  * handler for upload requests
  */
  public function actionUpload()
  {
  	// global $config;
  	// global $http_return_code;

    $uploadsAbsPath = Yii::$app->params['uploads_abs_path'];
    $uploadsRelPath = Yii::$app->params['uploads_rel_path'];

    $thumbnailAbsPath = Yii::$app->params['thumbnail_abs_path'];
    $thumbnailRelPath = Yii::$app->params['thumbnail_rel_path'];

  	$files = array();

  	if($_SERVER["REQUEST_METHOD"] == "GET"){
  		$dir = scandir($uploadsAbsPath);

  		foreach ($dir as $file_name){
  			$file_path = $uploadsAbsPath . $file_name;

  			if(is_file($file_path)){
  				$size = filesize($file_path);

  				$file = [
  					"name" => $file_name,
  					"url" => Url::base() . $uploadsRelPath . $file_name,
  					"size" => $size
  				];

  				if(file_exists($uploadsAbsPath . $file_name)){
  					$file["thumbnailUrl"] = Url::base() . $uploadsRelPath . $file_name;
  				}

  				$files[] = $file;
  			}
  		}
  	}
  	else if(!empty($_FILES))
  	{
  		foreach ($_FILES["files"]["error"] as $key => $error){
  			if($error == UPLOAD_ERR_OK){
  				$tmp_name = $_FILES["files"]["tmp_name"][$key];
  				$file_name = $_FILES["files"]["name"][$key];
  				$file_path = $uploadsAbsPath . $file_name;
  				if(move_uploaded_file($tmp_name, $file_path) === TRUE)
  				{
  					$size = filesize($file_path);
  					$image = new \Imagick($file_path);
  					$image->resizeImage($this->thumbnail_width, $this->thumbnail_height, \Imagick::FILTER_LANCZOS, 1.0, TRUE);
  					$image->writeImage($thumbnailAbsPath . $file_name);
  					$image->destroy();
  					$file = array(
  						"name" => $file_name,
  						"url" => Url::base() . $uploadsRelPath . $file_name,
  						"size" => $size,
  						"thumbnailUrl" => Url::base() . $thumbnailRelPath . $file_name
  					);
  					$files[] = $file;
  				}else{
  					$http_return_code = 500;
  					return;
  				}
  			}else{
  				$http_return_code = 400;
  				return;
  			}
  		}
  	}
  	header("Content-Type: application/json; charset=utf-8");
  	header("Connection: close");

  	echo json_encode(array("files"=>$files));
    http_response_code($this->http_response_code);
  }

  /**
   * function to resize images using resize or cover methods
   */
  public function ResizeImage($file_name,$method,$width,$height)
  {
  	global $config;

  	$image = new \Imagick(Yii::$app->params['uploads_abs_path'] . $file_name);

  	if($method == "resize"){
  		$image->resizeImage($width, $height, \Imagick::FILTER_LANCZOS, 1.0);
  	}else{ // $method == "cover"
  		$image_geometry = $image->getImageGeometry();

  		$width_ratio = $image_geometry["width"] / $width;
  		$height_ratio = $image_geometry["height"] / $height;

  		$resize_width = $width;
  		$resize_height = $height;

  		if($width_ratio > $height_ratio){
  			$resize_width = 0;
  		}else{
  			$resize_height = 0;
  		}

  		$image->resizeImage($resize_width, $resize_height, \Imagick::FILTER_LANCZOS, 1.0);

  		$image_geometry = $image->getImageGeometry();

  		$x = ($image_geometry["width"] - $width) / 2;
  		$y = ($image_geometry["height"] - $height) / 2;

  		$image->cropImage($width, $height, $x, $y);
  	}

  	return $image;
  }
}
