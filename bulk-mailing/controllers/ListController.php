<?php

namespace app\controllers;

use Yii;
use app\models\Lists;
use app\models\ListSearch;
use app\models\ListItem;
use app\models\ListItemSearch;
use app\models\ListSelectionForm;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* ListsController implements the CRUD actions for Lists model.
*/
class ListController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * List all Lists models.
  * @return mixed
  */
  public function actionIndex()
  {
    $this->checkLogin();
    $searchModel = new ListSearch();
    $searchModel->company_id = Yii::$app->user->identity->company_id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new Lists model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkLogin();
    $model = new Lists();
    $model->status=1;
    $model->company_id = Yii::$app->user->identity->company_id;
    $request = Yii::$app->request;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        if($request->isAjax){
          $msg['success']=['heading'=>Yii::t('app','Saved'),'msg'=>Yii::t('app','List saved successfully')];
          echo json_encode($msg);
          die();
          exit;
        }else{
          Yii::$app->getSession()->setFlash('success', Yii::t('app','List saved successfully'));
          return $this->redirect(['index']);
        }
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                if($request->isAjax){
                  $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>$val];
                  echo json_encode($msg);
                  die();
                  exit;
                }else{
                  Yii::$app->getSession()->setFlash('error',$val);
                }
              }
            }
          }
        }
      }
    }
    if($request->isAjax){
      return $this->renderAjax('_form', [
          'model' => $model,
      ]);
    }else{
      return $this->render('create', [
          'model' => $model,
      ]);
    }
  }

  /**
  * Updates an existing Lists model.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionUpdate($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app','List saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('update', [
      'model' => $model,
    ]);
  }

  /**
   * Updates an existing List model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id is for update model object
   * @return mixed
   */
  public function actionChoose()
  {
    $this->checkLogin();
      $model = new ListSelectionForm();

      if ($model->load(Yii::$app->request->post())) {
        if($model->save()){
          $msg['success']=['heading'=>Yii::t('app','Saved'),'msg'=>Yii::t('app','List updated successfully')];
          echo json_encode($msg);
          die();
          exit;
        }else{
          if($model->hasErrors()){
            foreach($model->getErrors() as $error){
              if(count($error)>0){
                foreach($error as $key=>$val){
                  $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>$val];
                  echo json_encode($msg);
                  die();
                  exit;
                }
              }
            }
          }
        }
      }
      return $this->renderAjax('choose', [
          'model' => $model,
      ]);
  }

  /**
  * Creates a new model.
  */
  protected function newModel()
  {
    $model = new Lists;
    return $model;
  }

  /**
   * Lists all List models.
   * @return mixed
   */
  public function actionContacts($id)
  {
    $this->checkLogin();
      $model = $this->findModel($id);
      $searchModel = new ListItemSearch();
      $searchModel->list_id=$id;
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

      return $this->render('contacts', [
          'model' => $model,
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
      ]);
  }

  /**
   * Deletes an existing ListItem model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id is used to delete model object
   * @return mixed
   */
  public function actionDeleteContact($lid,$mt,$mid)
  {
    $this->checkLogin();
      $this->findListItemModel($lid,$mt,$mid)->delete();
      Yii::$app->getSession()->setFlash('success', Yii::t('app','Contact deleted successfully'));
      return $this->redirect(['contacts','id'=>$lid]);
  }

  /**
  * Finds the Lists model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Lists the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = Lists::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }

  /**
   * Finds the ListItem model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id is used to find model object
   * @return ListItem the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findListItemModel($lid,$mt,$mid)
  {
      if (($model = ListItem::findOne(['list_id'=>$lid,'module_type'=>$mt,'module_id'=>$mid])) !== null) {
          return $model;
      } else {
          throw new NotFoundHttpException('The requested page does not exist.');
      }
  }
}
