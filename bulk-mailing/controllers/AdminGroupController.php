<?php

namespace app\controllers;

use Yii;
use app\models\AdminGroup;
use app\models\AdminGroupSearch;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
* AdminGroupController implements the CRUD actions for AdminGroup model.
*/
class AdminGroupController extends DefController
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

  /**
  * Lists all AdminGroup models.
  * @return mixed
  */
  public function actionIndex()
  {
    $this->checkSuperAdmin();
    $searchModel = new AdminGroupSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new model.
  */
  protected function newModel()
  {
    $model = new AdminGroup;
    return $model;
  }

  /**
  * Finds the AdminGroup model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return AdminGroup the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = AdminGroup::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException(Yii::t('app','The requested page does not exist.'));
    }
  }
}
