<?php

namespace app\controllers;

use app\models\SaleDiscount;
use Yii;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ForgetPasswordForm;
use app\models\ResetPasswordForm;
use app\models\Tags;
use yii\db\Expression;
use yii\helpers\Url;

class SiteController extends DefController
{
  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['logout'],
        'rules' => [
          [
            'actions' => ['logout'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'logout' => ['post'],
        ],
      ],
    ];
  }

  /**
  * Displays homepage.
  *
  * @return string
  */
  public function actionIndex()
  {
    $this->checkLogin();
    return $this->render('index');
  }

  /**
  * Displays homepage.
  *
  * @return string
  */
  public function actionFixTags($lastid=null)
  {
    $this->checkLogin();
    exit;
    $results = Tags::find()
    ->where(['is','deleted_at',new Expression('null')])
    ->andFilterWhere(['>','id',$lastid])
    ->orderBy(['id'=>SORT_ASC])->limit(1)->all();
    if($results!=null){
      foreach($results as $result){
        $lastid=$result->id;
        Yii::$app->db->createCommand()
        ->update('tags', ['deleted_at'=>date("Y-m-d H:i:s")], [
          'and',
          ['!=','id',$result->id],
          ['title'=>$result->title],
        ])
        ->execute();
      }
      $url = Url::to(['fix-tags','lastid'=>$lastid]);
      //echo '<a href="'.$url.'">Next</a>';
      echo '<script>window.location.href="'.$url.'"</script>';
    }else{
      echo 'Finished';
    }
    die();
    exit;
  }

  /**
  * Displays unsubscribe page.
  *
  * @return string
  */
  public function actionUnSubscribe()
  {
    $this->layout='guest';
    //return $this->render('un_subscribe');
  }

  /**
  * Login action.
  *
  * @return Response|string
  */
  public function actionLogin()
  {
    $this->layout='guest';
    if (!Yii::$app->user->isGuest) {
      return $this->goHome();
    }

    $model = new LoginForm();
    if ($model->load(Yii::$app->request->post()) && $model->login()) {
      return $this->goBack();
    }

    $model->password = '';
    return $this->render('login', [
      'model' => $model,
    ]);
  }

  /**
  * Logout action.
  *
  * @return Response
  */
  public function actionLogout()
  {
    Yii::$app->user->logout();

    return $this->goHome();
  }

  /**
   * Forget Password action.
   *
   * @return Response
   */
  public function actionForgetPassword()
  {
    $this->layout='guest';
    $model = new ForgetPasswordForm();
    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
      if ($model->sendEmail()) {
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Check your email for further instructions.'));
        return $this->goHome();
      } else {
        Yii::$app->getSession()->addFlash('error', Yii::t('app','Sorry, we are unable to reset password for email provided.'));
      }
    }

    return $this->render('forget_password', [
      'model' => $model,
    ]);
  }

  /**
   * Reset Password action.
   *
   * @return Response
   */
  public function actionResetPassword($token)
  {
    $this->layout='guest';
    try {
      $model = new ResetPasswordForm($token);
    } catch (InvalidParamException $e) {
      throw new BadRequestHttpException($e->getMessage());
    }
    if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
      Yii::$app->getSession()->addFlash('success', Yii::t('app','New password was saved.'));
      return $this->goHome();
    }
    return $this->render('reset_password', [
      'model' => $model,
    ]);
  }

  /**
  * Displays contact page.
  *
  * @return Response|string
  */
  public function actionContact()
  {
    $model = new ContactForm();
    if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
      Yii::$app->session->setFlash('contactFormSubmitted');

      return $this->refresh();
    }
    return $this->render('contact', [
      'model' => $model,
    ]);
  }


    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionSalesDiscounts()
    {
        $model = SaleDiscount::findOne(1);
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
                return $this->refresh();
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }


        }
        return $this->render('sale_discount', [
            'model' => $model,
        ]);
    }



  /**
  * Displays about page.
  *
  * @return string
  */
  public function actionAbout()
  {
    return $this->render('about');
  }
}
