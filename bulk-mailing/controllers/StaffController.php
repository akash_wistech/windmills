<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\StaffSearch;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* StaffController implements the CRUD actions for User model.
*/
class StaffController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all Staff models.
  * @return mixed
  */
  public function actionIndex()
  {
    $this->checkLogin();
    $searchModel = new StaffSearch();
    $searchModel->company_id = Yii::$app->user->identity->company_id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new Staff model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkLogin();
    $model = new User();
    if(Yii::$app->user->identity->user_type==0){
      $model->permission_group_id=Yii::$app->appHelperFunctions->getSetting('cust_perm_id');
      $model->user_type=1;
    }else{
      $model->user_type=10;
    }
    $model->company_id=Yii::$app->user->identity->company_id;
    $model->status=1;
    $model->verified=1;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }
    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
  * Updates an existing Staff model.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionUpdate($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);
    if(Yii::$app->user->identity->user_type==0){
      $model->permission_group_id=Yii::$app->appHelperFunctions->getSetting('cust_perm_id');
      $model->user_type=1;
    }else{
      $model->user_type=10;
    }
    $model->company_id=Yii::$app->user->identity->company_id;
    $model->status=1;
    $model->verified=1;
    $model->oldfile=$model->image;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('update', [
      'model' => $model,
    ]);
  }

  /**
  * Creates a new model.
  */
  protected function newModel()
  {
    $model = new User;
    return $model;
  }

  /**
  * Finds the User model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Staff the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = User::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
