<?php

namespace app\controllers;

use Yii;
use app\models\Prospect;
use app\models\ProspectSearch;
use app\models\ImportRawDataForm;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\Pagination;
use yii\helpers\Html;
use yii\helpers\Url;

/**
* ProspectController implements the CRUD actions for Prospect model.
*/
class ProspectController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all Prospect models.
  * @return mixed
  */
  public function actionIndex()
  {
    $this->checkLogin();
    $searchModel = new ProspectSearch();
    $searchModel->company_id = Yii::$app->user->identity->company_id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Lists all Prospect models for datatable
  * @return mixed
  */
  public function actionDatatables()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $getParams = Yii::$app->request->get();
    $searchModel = new ProspectSearch();
    $searchModel->company_id = Yii::$app->user->identity->company_id;
    if(isset($getParams['search']) && isset($getParams['search']['value']) && $getParams['search']['value']!=''){
      $searchModel->keyword = $getParams['search']['value'];
    }
    $query = $searchModel->generateQuery(Yii::$app->request->queryParams);

    $countQuery = clone $query;

    $models = $query->offset($getParams['start'])
    ->limit($getParams['length'])
    ->asArray()
    ->all();
    $dataArr=[];
    if($models!=null){
      foreach($models as $model){
        $thisData['id']=$model['id'];
        // $thisData['company_id']=$model['company_id'];
        $thisData['cb_col']='<input type="checkbox" name="selection[]" value="'.$model['id'].'">';
        $thisData['cp_name']=$model['cp_name'];
        $thisData['email']=$model['email'];

        $gridViewColumns=Yii::$app->inputHelperFunctions->getGridViewColumns($searchModel->moduleTypeId);
        if($gridViewColumns!=null){
          foreach($gridViewColumns as $gridViewColumn){
            // $colLabel=($gridViewColumn['short_name']!=null && $gridViewColumn['short_name']!='' ? $gridViewColumn['short_name'] : $gridViewColumn['title']);
            $thisData['col_'.$gridViewColumn['id']]=Yii::$app->inputHelperFunctions->getGridValue('prospect',$model,$gridViewColumn);
          }
        }

        $tagsHtml = Yii::$app->appHelperFunctions->getModuleTagsHtml('prospect',$model['id']);
        // $thisData['tag']=$tagsHtml;
        $thisData['status']=Yii::$app->helperFunctions->arrStatusIcon[$model['status']];
        $thisData['created_at']=$model['created_at'];
        $actBtns = Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), ['update','id'=>$model['id']], [
          'title' => Yii::t('app', 'Edit'),
          'class'=>'dropdown-item text-1',
        ]);
        $thisData['action_col']=Yii::$app->helperFunctions->getDatatableActionTemplate($actBtns);
        $dataArr[]=$thisData;
      }
    }

    $response = [
      'draw' => (int)$getParams['draw'],
      'recordsTotal' => (int)$countQuery->count(),
      'recordsFiltered' => (int)$countQuery->count(),
      'data' => $dataArr,
    ];
    return $response;
  }

  /**
  * Creates a new Prospect model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkLogin();
    $model = new Prospect();
    $model->company_id = Yii::$app->user->identity->company_id;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Raw data saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }
    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
  * Import for ImportRawDataForm.
  * If import is successful, the browser will be redirected to the 'index' page.
  * @return mixed
  */
  public function actionImport()
  {
    $model = new ImportRawDataForm();

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Data imported successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('import', [
      'model' => $model,
    ]);
  }

  /**
  * Updates an existing Prospect model.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionUpdate($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Raw data saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('update', [
      'model' => $model,
    ]);
  }

  /**
  * Creates a new model.
  */
  protected function newModel()
  {
    $model = new Prospect;
    return $model;
  }

  /**
  * Finds the Prospect model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Prospect the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = Prospect::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
