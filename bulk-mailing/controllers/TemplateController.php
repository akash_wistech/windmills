<?php

namespace app\controllers;

use Yii;
use app\models\Template;
use app\models\TemplateSearch;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Response;

/**
* TemplateController implements the CRUD actions for Template model.
*/
class TemplateController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * List all Template models.
  * @return mixed
  */
  public function actionIndex()
  {
    $this->checkLogin();
    $searchModel = new TemplateSearch();
    $searchModel->company_id = Yii::$app->user->identity->company_id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new Template model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkLogin();
    $model = new Template();
    $request=Yii::$app->request;
    $model->status=1;
    $model->company_id = Yii::$app->user->identity->company_id;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        if($request->isAjax){
          $listHtml='';
          $templatesArr=Yii::$app->appHelperFunctions->getEmailTemplatesArr($model->company_id);
          if($templatesArr!=null){
            foreach($templatesArr as $key=>$val){
              $listHtml.='<option value="'.$key.'"'.($key==$model->id ? ' selected' : '').'>'.$val.'</option>';
            }
          }
          $msg['success']=[
            'heading'=>Yii::t('app','Saved'),
            'msg'=>Yii::t('app','Template saved successfully'),
            'listHtml' => $listHtml,
          ];
          echo json_encode($msg);
          die();
          exit;
        }else{
          Yii::$app->getSession()->addFlash('success', Yii::t('app','Template saved successfully'));
          return $this->redirect(['index']);
        }
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                if($request->isAjax){
                  $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>$val];
                  echo json_encode($msg);
                  die();
                  exit;
                }else{
                  Yii::$app->getSession()->setFlash('error',$val);
                }
              }
            }
          }
        }
      }
    }
    if($request->isAjax){
      return $this->renderAjax('_form', [
          'model' => $model,
      ]);
    }else{
      return $this->render('create', [
          'model' => $model,
      ]);
    }
  }

  /**
  * Updates an existing Template model.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionUpdate($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Template saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('update', [
      'model' => $model,
    ]);
  }

	public function actionGjsAssets()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		$jsArr = [];
		$folder = Yii::$app->fileHelperFunctions->gjsAssetPath['abs'].Yii::$app->user->identity->company_id;
		$files=FileHelper::findFiles($folder);
		if($files!=null){
			foreach($files as $file){
				$path_parts = pathinfo($file);
				$jsArr[]=['src'=>Yii::$app->fileHelperFunctions->gjsAssetPath['rel'].Yii::$app->user->identity->company_id.'/'.$path_parts['basename']];
			}
		}
		return $this->asJson($jsArr);
	}

  /**
  * Creates a new model.
  */
  protected function newModel()
  {
    $model = new Template;
    return $model;
  }

  /**
  * Finds the Template model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Template the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = Template::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
