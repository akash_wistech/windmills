<?php
namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;
use Yii;
use Mailgun\Mailgun;
use app\models\Lists;
use app\models\ListItem;
use app\models\Campaign;
use app\models\CampaignList;
use app\models\CampaignAttachment;
use app\models\CampaignEmail;
use app\models\User;
use app\models\Prospect;
use app\models\Template;
use app\models\BlackListedEmail;
use app\models\DroppedEmail;
use app\models\CampaignSmtp;
use app\models\EmailSmtpDetails;
use yii\db\Expression;
use Twilio\Rest\Client;

/**
* Console actions for campaign
*/
class CampaignController extends Controller
{
  public function actionDateTime()
  {
    $consoleDif = Yii::$app->appHelperFunctions->getSetting('cron_server_diff');
    $currentDateTime = date("Y-m-d H:i:s",strtotime($consoleDif." Minute",strtotime(date("Y-m-d H:i:s"))));
    //echo $currentDateTime."\n";
    Yii::$app->mailer->compose()
         ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->params['appName']])
         ->setTo(["akash@wistech.biz" => "Akash"])
         ->setSubject('Email from bulk mailer')
         ->setTextBody("This is email body".$currentDateTime)
         ->send();
  }
  /**
  * Send a campaign
  */
  public function actionProcess()
  {

    $consoleDif = Yii::$app->appHelperFunctions->getSetting('cron_server_diff');
    $currentDateTime = date("Y-m-d H:i:s",strtotime($consoleDif." Minute",strtotime(date("Y-m-d H:i:s"))));

    //echo $currentDateTime."\n";

    $connection = Yii::$app->db;
    $campaigns = Campaign::find()
    ->where([
      'and',
      ['status'=>1,'sent'=>[0,1]],
      ['is','deleted_at',new Expression('null')],
      ['<=','distribution_date',$currentDateTime],
    ])
    ->orderBy(['distribution_date'=>SORT_ASC])
    ->limit(1)->all();
    if($campaigns!=null){
      foreach($campaigns as $campaign){
        if($campaign['sent']==0){
          $connection->createCommand()
          ->update(Campaign::tableName(), ['sent'=>1], [
            'id'=>$campaign['id'],
          ])
          ->execute();
        }
        $attachmentsArr=[];
        $attachments=CampaignAttachment::find()->where(['campaign_id'=>$campaign['id']])->asArray()->all();
        if($attachments!=null){
          ini_set('allow_url_fopen',1);
          foreach($attachments as $attachment){
            if(
              !filter_var($attachment['file_name'], FILTER_VALIDATE_URL) &&
              $attachment['file_name']!=null &&
              file_exists(Yii::$app->fileHelperFunctions->campaignPath['abs'].$attachment['file_name'])
            ){
              $fpInfo=pathinfo(Yii::$app->fileHelperFunctions->campaignPath['abs'].$attachment['file_name']);
              $readableFileName=$attachment['file_title'];
              $copyToFile=Yii::$app->fileHelperFunctions->campaignPath['abs'].$readableFileName;
              if(!file_exists($copyToFile)){
                Yii::$app->fileHelperFunctions->copyToLocalByCurl($attachment['file_name'],$copyToFile);
                //copy(Yii::$app->fileHelperFunctions->campaignPath['abs'].$attachment['file_name'],$copyToFile);
              }
            }else{
              $fpInfo=pathinfo($attachment['file_name']);
              $readableFileName=$attachment['file_title'];
              $copyToFile=Yii::$app->fileHelperFunctions->campaignPath['abs'].$readableFileName;
              if(!file_exists($copyToFile)){
                Yii::$app->fileHelperFunctions->copyToLocalByCurl($attachment['file_name'],$copyToFile);
                //copy($attachment['file_name'],$copyToFile);
              }
            }
            if(!in_array($copyToFile,$attachmentsArr)){
              $attachmentsArr[]=$copyToFile;
            }
          }
        }

        $subQueryLists=CampaignList::find()->select(['list_id'])->where(['campaign_id'=>$campaign['id']]);
        $subQueryAlreadySent=CampaignEmail::find()->select(['email'])->where(['campaign_id'=>$campaign['id']]);

        //Query Employees
        $ec = $employees = ListItem::find()
        ->select([
          'list_id',
          'module_type',
          'module_id',
          'email',
          'fullname'=>'CONCAT(firstname," ",lastname)',
        ])
        ->where([
          'and',
          [
            'list_id'=>$subQueryLists,
            'module_type'=>'employee',
          ],
          ['not in','email',$subQueryAlreadySent],
        ])
        ->innerJoin(User::tableName(),User::tableName().".id=".ListItem::tableName().".module_id and module_type='employee'");

        //Query Prospects
        $prospects = ListItem::find()
        ->select([
          'list_id',
          'module_type',
          'module_id',
          'email',
          'fullname'=>'CONCAT(firstname," ",lastname)',
        ])
        ->where([
          'and',
          [
            'list_id'=>$subQueryLists,
            'module_type'=>'prospect',
          ],
          ['not in','email',$subQueryAlreadySent],
        ])
        ->innerJoin(Prospect::tableName(),Prospect::tableName().".id=".ListItem::tableName().".module_id and module_type='prospect'");


        $foundContacts=false;
        $contacts = (new yii\db\Query())
        ->select('*')
        ->from([
          'contacts'=>$employees
          ->union($prospects)
        ])->limit($campaign['per_min_email'])->all();

        //Count
        $totalCount=(new yii\db\Query())
        ->select('email')
        ->from([
          'contacts'=>$employees
          ->union($prospects)
        ])->groupBy('email')->count('email');

        $subQueryCampaignSmtp=CampaignSmtp::find()->select(['smtp_id'])->where(['campaign_id'=>$campaign['id']]);
        $smtps=EmailSmtpDetails::find()
        ->where([
          'and',
          ['id'=>$subQueryCampaignSmtp,'status'=>1],
          ['is','deleted_at',new Expression('null')]
        ])
        ->asArray()->all();

        if($contacts!=null){
          $foundContacts=true;
          foreach($contacts as $contact){

            $this->sendEmail($consoleDif,$smtps,$campaign,$totalCount,$attachmentsArr,$contact);
          }
        }

        if($foundContacts==false){
          if($attachmentsArr!=null){
            foreach($attachmentsArr as $key=>$val){
              if(file_exists($val)){
                unlink($val);
              }
            }
          }

          if($consoleDif!=''){
            $sentDateTime=date('Y-m-d H:i:s', strtotime("".$consoleDif." Minute", strtotime(date("Y-m-d H:i:s"))));
          }else{
            $sentDateTime=date("Y-m-d H:i:s");
          }

          $connection->createCommand()
          ->update(Campaign::tableName(),['sent' => 2,'sent_date_time'=>$sentDateTime], 'id=:id', [':id' => $campaign['id']])
          ->execute();
        }
      }
    }
    return ExitCode::OK;
  }

  public function sendEmail($consoleDif,$smtps,$campaign,$totalCount,$attachmentsArr,$contact)
  {
    $already = CampaignEmail::find()->where(['campaign_id'=>$campaign['id'],'email'=>$contact['email']]);
    if(!$already->exists()) {
        $smtpDetail = null;
        if ($smtps != null) {
            $shouldSend = round($totalCount / count($smtps));
            foreach ($smtps as $smtpDetail) {
                $usedThisSmtp = CampaignEmail::find()
                    ->where(['campaign_id' => $campaign['id'], 'smtp_id' => $smtpDetail['id']])
                    ->count('email');
                //If sent is less break and use
                if ($usedThisSmtp < $shouldSend) {
                    break;
                }
            }
        }
        //
        $connection = Yii::$app->db;
        if ($consoleDif != '') {
            $sentDateTime = date('Y-m-d H:i:s', strtotime("" . $consoleDif . " Minute", strtotime(date("Y-m-d H:i:s"))));
        } else {
            $sentDateTime = date("Y-m-d H:i:s");
        }

        $sent = new CampaignEmail;
        $sent->campaign_id = $campaign['id'];
        $sent->smtp_id = $smtpDetail['id'];
        $sent->list_id = $contact['list_id'];
        $sent->module_type = $contact['module_type'];
        $sent->module_id = $contact['module_id'];
        $sent->email = $contact['email'];
        $sent->status = 0;
        $sent->distributed_at = $sentDateTime;
        $sent->save();

        $flagged = false;
        $soft_bounced = 0;
        $hard_bounced = 0;
        $isBlackListed = BlackListedEmail::find()->where([
            'and',
            ['email' => $contact['email']],
            ['is', 'deleted_at', new Expression('null')],
        ]);
        if ($isBlackListed->exists()) {
            $flagged = true;
            $hard_bounced = 1;
        }
        $isSoftBounced = DroppedEmail::find()->where([
            'and',
            ['email' => $contact['email']],
            ['is', 'deleted_at', new Expression('null')],
        ]);
        if ($isSoftBounced->exists()) {
            $flagged = true;
            $soft_bounced = 1;
        }


        if ($flagged == false) {
            $unSubscribeLink = Yii::$app->urlManager->createAbsoluteUrl(['site/un-subscribe', 'email' => $contact['email'], 'cid' => $campaign['company_id'], 'ref' => 'mscnr' . $campaign['id']]);
            //\Yii::$app->params['unSubscribeLink'] = $unSubscribeLink;
            $subject = $campaign['subject'];
            $htmlBody = $campaign['descp'];
            $textBody = $campaign['descp'];
            $template = Template::findOne(['id' => $campaign['template_id']]);
            if ($template != null) {
                $htmlBody = str_replace("{content}", $htmlBody, $template->email_body);
                $textBody = str_replace("{content}", $textBody, $template->email_body);
            }
            //Replace email
            $htmlBody = str_replace("{userEmail}", $contact['email'], $htmlBody);
            $textBody = str_replace("{userEmail}", $contact['email'], $textBody);
            //Replace name
            $htmlBody = str_replace("{userName}", $contact['fullname'], $htmlBody);
            $textBody = str_replace("{userName}", $contact['fullname'], $textBody);
            $subject = str_replace("{userName}", $contact['fullname'], $subject);
            //Replace unsubscribe link
            $unSubscribeHtmlBtn = '<a href="' . $unSubscribeLink . '" target="_blank">Unsubscribe here</a>';
            $htmlBody = str_replace("{unsubscribeLink}", $unSubscribeHtmlBtn, $htmlBody);
            $textBody = str_replace("{unsubscribeLink}", $unSubscribeLink, $textBody);

            $textBody = strip_tags($textBody);

            Yii::$app->mailer->setTransport(
                [
                    'class' => 'Swift_SmtpTransport',
                    'host' => $smtpDetail['smtp_host'],
                    'username' => $smtpDetail['smtp_username'],
                    'password' => $smtpDetail['smtp_password'],
                    'port' => $smtpDetail['smtp_port'],
                    'encryption' => $smtpDetail['smtp_encryption'],
                ]
            );


            $sendByEmail = $smtpDetail['mail_from'];
            $message = \Yii::$app->mailer
                ->compose(['html' => 'campaign-html', 'text' => 'campaign-text'], ['htmlBody' => $htmlBody, 'textBody' => $textBody])
                ->setFrom([$sendByEmail => $campaign->sentBy->company->title . ''])
               // ->setTo(['akash@wistech.biz' => $contact['fullname']])
                ->setTo([$contact['email'] => $contact['fullname']])
                ->setSubject($subject);
            if ($campaign['reply_to'] != '') {
                $message->setReplyTo([$campaign['reply_to'] => $campaign->sentBy->company->title]);
            }

            if ($attachmentsArr != null) {
                foreach ($attachmentsArr as $key => $val) {
                    $message->attach($val);
                }
            }

            $headers = $message->getSwiftMessage()->getHeaders();
            $headers->addTextHeader('X-Mailgun-Variables', json_encode(['mail_type' => 'newsletter', 'campaign_id' => $campaign['id']]));
            $headers->addTextHeader('X-SES-CONFIGURATION-SET', 'WindMillsSNS');
            $headers->addTextHeader('X-SES-MESSAGE-TAGS', 'mail_type=newsletter,campaign_id=' . $campaign['id'] . '');
            $headers->addTextHeader('Precedence', 'bulk');
            $headers->addTextHeader('List-Unsubscribe', $unSubscribeLink);

            $message->send();

        } else {
            if ($soft_bounced == 1) {
                $connection->createCommand()
                    ->update(CampaignEmail::tableName(), ['soft_bounced' => 1], [
                        'campaign_id' => $campaign['id'],
                        'module_type' => $contact['module_type'],
                        'module_id' => $contact['module_id'],
                    ])
                    ->execute();
            }
            if ($hard_bounced == 1) {
                $connection->createCommand()
                    ->update(CampaignEmail::tableName(), ['hard_bounced' => 1], [
                        'campaign_id' => $campaign['id'],
                        'module_type' => $contact['module_type'],
                        'module_id' => $contact['module_id'],
                    ])
                    ->execute();
            }
        }

        $connection->createCommand()
            ->update(CampaignEmail::tableName(), ['status' => 1], [
                'campaign_id' => $campaign['id'],
                'module_type' => $contact['module_type'],
                'module_id' => $contact['module_id'],
            ])
            ->execute();

//  ->create("whatsapp:+971543031093",
       /* if (isset($campaign['sms_text']) && $campaign['sms_text'] <> null) {
            $sid = "AC7ccee166e90e63654b21b8d76aac7163";
            $token = "0b513bce9d29b77a91265cc8571d1746";
            $twilio = new Client($sid, $token);

            $message = $twilio->messages
                ->create("whatsapp:+971529877843", // to
                    [
                        "from" => "whatsapp:+14155238886",
                        "body" => $campaign['sms_text']
                    ]
                );


        }*/
    }
  }
}
