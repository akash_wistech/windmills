<?php

namespace app\components\widgets;

use Yii;
use yii\grid\GridView;
use yii\helpers\Html;

class CustomGridView extends GridView
{
	public $cardTitle='';
	public $cardHeader=true;
	public $showPerPage=false;
	public $createBtn=false;
	public $mailBtn=false;
	public $showSummary=true;
  public $createBtnLink = ['create'];
  public $createBtnHtml = '';
	public $tableOptions = ['class'=>'table'];

	public $export=false;
	public $import=false;

  /**
   * Initializes the grid view.
   * This method will initialize required property values and instantiate [[columns]] objects.
   */
  public function init()
  {
		$this->tableOptions=['class' => 'table table-responsive-lg table-sm table-striped table-hover mb-0'];
		$this->filterSelector = '#' . Html::getInputId($this->filterModel, 'pageSize');
		$this->filterSelector = '#filter-pageSize';
		$Btns='';

		if($this->createBtn==true){
			if($this->createBtnHtml!=''){
				$Btns.=$this->createBtnHtml;
			}else{
				$Btns.='&nbsp;';
				$Btns.=Html::a('<i class="fas fa-plus"></i>',$this->createBtnLink,['class'=>'btn btn-xs btn-primary', 'data-pjax'=>'0', 'data-toggle'=>'tooltip', 'data-title'=>Yii::t('app','Add New')]);
			}
		}
		if($this->mailBtn==true){
			$Btns.='&nbsp;';
			$Btns.=Html::a('<i class="fas fa-envelope"></i>','javascript:;',['id'=>'btn-mailing', 'class'=>'btn btn-xs btn-primary', 'data-pjax'=>'0', 'data-toggle'=>'tooltip', 'data-title'=>Yii::t('app','Send Email')]);
		}
		if($this->import==true && Yii::$app->menuHelperFunction->checkActionAllowed('import')){
			$Btns.='&nbsp;';
			$Btns.=Html::a('<i class="fas fa-file-import"></i>',['import'],['class'=>'btn btn-xs btn-success', 'data-pjax'=>'0', 'data-toggle'=>'tooltip', 'data-title'=>Yii::t('app','Import')]);
		}
		if($this->export==true && Yii::$app->menuHelperFunction->checkActionAllowed('export')){
			$Btns.='&nbsp;';
			$Btns.=Html::a('<i class="fas fa-download"></i>',['export'],['class'=>'btn btn-xs btn-info', 'data-pjax'=>'0', 'data-toggle'=>'tooltip', 'data-title'=>Yii::t('app','Export')]);
		}
		$carHeaderHtml='';
		if($this->cardHeader==true || $this->showPerPage==true){
			$carHeaderHtml='
			<header class="card-header">
				<div class="row">
					<div class="col-xs-12 col-sm-6">
						<h2 class="card-title">'.($this->cardTitle!='' ? $this->cardTitle : $this->view->title).'</h2>
					</div>
					<div class="col-xs-12 col-sm-6">
						<div class="clearfix">
							<div class="float-right">
								'.$Btns.'
							</div>
							<div class="float-right" style="margin-right:10px;">
								'.($this->showPerPage==true ? Html::activeDropDownList($this->filterModel, 'pageSize', Yii::$app->params['pageSizeArray'], ['id'=>'filter-pageSize', 'class'=>'form-control input-xs']) : '').'
							</div>
						</div>
					</div>
				</div>
			</header>';
		}
		$this->layout='
		<section class="card'.($this->cardHeader==true ? ' card card-outline card-primary' : '').'">
			'.$carHeaderHtml.'
			<div class="card-body tbl-container table-responsive">
				{items}
			</div>
			<div class="card-footer">
				<div class="row">
					'.($this->showSummary==true ? '<div class="col-sm-3">{summary}</div>' : '').'
					<div class="col-sm-'.($this->showSummary==true ? '9' : '12').' pager-container">{pager}</div>
				</div>
			</div>
		</section>
		';
		$this->pager=[
      'maxButtonCount' => 5,
			'linkContainerOptions' => ['class'=>'paginate_button page-item'],
			'linkOptions' => ['class'=>'page-link'],
			'prevPageCssClass' => 'previous',
			'disabledPageCssClass' => 'disabled',
			'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'page-link'],
    ];
    parent::init();
  }
}
