<?php

namespace app\components\widgets;

use Yii;
use app\assets\ToastrAsset;

/**
 * Alert widget renders a message from session flash. All flash messages are displayed
 * in the sequence they were assigned using setFlash. You can set message as following:
 *
 * ```php
 * \Yii::$app->getSession()->setFlash('error', 'This is the message');
 * \Yii::$app->getSession()->setFlash('success', 'This is the message');
 * \Yii::$app->getSession()->setFlash('info', 'This is the message');
 * ```
 *
 * Multiple messages could be set as follows:
 *
 * ```php
 * \Yii::$app->getSession()->setFlash('error', ['Error 1', 'Error 2']);
 * ```
 *
 * @author Akash Ahmed <akash@wistech.biz>
 */
class ToasterAlert extends \yii\bootstrap\Widget
{
    /**
     * @var array the alert types configuration for the flash messages.
     * This array is setup as $key => $value, where:
     * - $key is the name of the session flash variable
     * - $value is the bootstrap alert type (i.e. danger, success, info, warning)
     */
    public $alertTypes = [
        'error' => 'error',
        'danger' => 'error',
        'success' => 'success',
        'info' => 'info',
        'warning' => 'warning'
    ];

    /**
     * @var boolean close button option.
     */
    public $closeButton = true;

    /**
     * @var string position class.
     */
    public $positionClass = 'toast-top-right';

    public function init()
    {
        parent::init();


        $view = $this->getView();
        $session = \Yii::$app->getSession();
        $flashes = $session->getAllFlashes();
        $appendCss = isset($this->options['class']) ? ' ' . $this->options['class'] : '';

        //Bootstrap3 Asset by default
        ToastrAsset::register($view);

        foreach ($flashes as $type => $data) {
            if (isset($this->alertTypes[$type])) {
                $data = (array)$data;
                foreach ($data as $i => $message) {
                    $message = str_replace('"', "", $message);
                    $this->getView()->registerJs('
            toastr.' . $this->alertTypes[$type] . '("' . $message . '", "", {closeButton:' . $this->closeButton . ', positionClass:"' . $this->positionClass . '", timeOut: 5000});
          ');
                }
                $session->removeFlash($type);
            }
        }
    }
}
