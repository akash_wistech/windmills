<?php

namespace app\components\widgets;

use Yii;
use yii\grid\GridView;
use yii\helpers\Html;

class InfiniteGridView extends GridView
{
	public $tableOptions = ['class'=>'table'];
  /**
   * Initializes the grid view.
   * This method will initialize required property values and instantiate [[columns]] objects.
   */
  public function init()
  {
		$this->layout='
		{items}
		{pager}';
    parent::init();
  }
}
