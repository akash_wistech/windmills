<?php
namespace app\components\widgets;

use Yii;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/**
* Extended Modal Widget to ignore yii bootstrap
*
* @author Akash Ahmed <akash@wistech.biz>
* @since 1.0
*/
class CustomModal extends Modal
{
  /**
  * Renders the widget.
  */
  public function run()
  {
    echo "\n" . $this->renderBodyEnd();
    echo "\n" . $this->renderFooter();
    echo "\n" . Html::endTag('div'); // modal-content
    echo "\n" . Html::endTag('div'); // modal-dialog
    echo "\n" . Html::endTag('div');
  }
}
