<?php

namespace app\components\widgets;

use Yii;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use app\assets\DataTableGridAsset;
use yii\bootstrap\Widget;

class DataTableGridView extends Widget
{
	public $filterModel=null;
	public $columns=[];
	public $advSearch=false;
	public $cbSelection=false;
	public $cardTitle='';
	public $cardHeader=true;
	public $showPerPage=false;
	public $createBtn=false;
	public $mailBtn=false;
	public $showSummary=true;
  public $createBtnLink = ['create'];

	public $export=false;
	public $import=false;
	public $ajaxUrl=false;

  /**
  * @var array the HTML attributes for the container tag of the datatables view.
  */
  public $options = [];

  /**
  * @var array the HTML attributes for the datatables table element.
  */
  public $tableOptions = ["class"=>"table table-respons1ive-lg table-sm table-striped table-hover mb-0","cellspacing"=>"0", "width"=>"100%"];

  /**
  * @var array the HTML attributes for the datatables table element.
  */
  public $clientOptions = [
		"dom"=>"<'row'<'col-sm-6'Bf><'col-sm-6'l>><'table-responsive1't><'row'<'col-sm-6'i><'col-sm-6'p>>",
    "processing"=>true,
    "language"=>[
      "search"=>"",
      "lengthMenu"=>"_MENU_ Per Page",
			"processing"=>"<img src='/images/loading.gif' />"
    ]
  ];


  /**
   * Runs the widget.
   */
  public function run()
  {
      $clientOptions = $this->getClientOptions();
			$clientOptions['lengthMenu'] = [25, 50, 100, 200, 500];
			$clientOptions['pageLength'] = 100;
			$clientOptions['ordering'] = false;
			if($this->advSearch==true){
				$clientOptions["buttons"]=[
		      [
		        'extend'=>'colvis',
		        'columns'=>':not(.noVis)',
		        'collectionLayout'=>'two-column',
		        'className'=>'btn btn-sm btn-info',
		        'text'=>'Columns'
		      ],
		      [
		        'className'=>'btn btn-sm btn-warning advance-search-popup',
		        'text'=>'Advance Search'
		      ]
		    ];
			}else{
				$clientOptions["buttons"]=[
		      [
		        'extend'=>'colvis',
		        'columns'=>':not(.noVis)',
		        'collectionLayout'=>'two-column',
		        'className'=>'btn btn-sm btn-info',
		        'text'=>'Columns'
		      ]
		    ];
			}
			if($this->ajaxUrl!=false){
				$searchParams=[];
				$params=Yii::$app->request->queryParams;
				if(isset($params[$this->filterModel->recType.'Search'])){
				  foreach($params[$this->filterModel->recType.'Search'] as $field=>$checkedVals){
				    $searchParams[$this->filterModel->recType.'Search'][$field]=$checkedVals;
				  }
				}
				$clientOptions["processing"]=true;
				$clientOptions["serverSide"]=true;
				$clientOptions["ajax"]=[
					'url'=>$this->ajaxUrl,
					'data'=>$searchParams
				];
			}
			if($this->cbSelection==true){
				$clientOptions["select"]=['style'=>'multi','selector'=>'td:first-child'];
					$clientOptions["columnDefs"]=[
						[
							'orderable'=>false,
							'className'=>'noVis',
							'visible'=>false,
							'targets'=>0
						],
						[
							'orderable'=>false,
							'className'=>'noVis select-checkbox',
							'targets'=>1
						],
						[
							'orderable'=>false,
							'targets'=>-1
						],
					];
			}else{
				$clientOptions["columnDefs"]=[
					[
						'orderable'=>false,
						'className'=>'noVis',
						'visible'=>false,
						'targets'=>0
					],
				];
			}
			if($this->columns!=null){
				$columnsArr=[];
				$i=0;
				foreach($this->columns as $key=>$val){
					$columnsArr[]=['data'=>$key];
					$i++;
				}
				$clientOptions["columns"]=$columnsArr;
			}
      $view = $this->getView();
      $gridId=$this->options['id'];
      $id = $this->tableOptions['id'];

      //Bootstrap3 Asset by default
      DataTableGridAsset::register($view);

			$statusFilterArr = Yii::$app->helperFunctions->arrFilterStatus;
			$statusFilterOpts = '';
			if($statusFilterArr!=null){
				foreach($statusFilterArr as $key=>$val){
					$statusFilterOpts.= "select.append( '<option value=\"".$key."\">".$val."</option>' );";
				}
			}


      $options = Json::encode($clientOptions);
      $view->registerJs("
				var dtOpts = ".$options.";
				dtOpts[\"initComplete\"] = function () {
					  this.api().columns(6).every( function () {
								var column = this;
                var select = $('<select><option value=\"\"></option></select>')
                    .appendTo( $(column.header()) )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? val : '', true, false )
                            .draw();
                    } );
										".$statusFilterOpts."
            } );
        }
				$('#$id').dataTable().fnDestroy()
				jQuery('#$id').DataTable(dtOpts);
				$('body').on('click', '.advance-search-popup', function () {
					$('#advanceSearchModal').modal();
				});
			");
  }

  /**
   * Initializes the datatables widget disabling some GridView options like
   * search, sort and pagination and using DataTables JS functionalities
   * instead.
   */
  public function init()
  {
      parent::init();

			$searchModel = $this->filterModel;

      //the table id must be set
      if (!isset($this->tableOptions['id'])) {
          $this->tableOptions['id'] = 'datatables_'.$this->getId();
      }

      $Btns='';

      if($this->createBtn==true){
        $Btns.='&nbsp;'.Html::a('<i class="fas fa-plus"></i>',$this->createBtnLink,['class'=>'btn btn-xs btn-primary', 'data-pjax'=>'0', 'data-toggle'=>'tooltip', 'data-title'=>Yii::t('app','Add New')]);
      }
      if($this->mailBtn==true){
        $Btns.='&nbsp;'.Html::a('<i class="fas fa-envelope"></i>','javascript:;',['id'=>'btn-mailing', 'class'=>'btn btn-xs btn-primary', 'data-pjax'=>'0', 'data-toggle'=>'tooltip', 'data-title'=>Yii::t('app','Send Email')]);
      }
      if($this->import==true && Yii::$app->menuHelperFunction->checkActionAllowed('import')){
        $Btns.='&nbsp;'.Html::a('<i class="fas fa-file-import"></i>',['import'],['class'=>'btn btn-xs btn-success', 'data-pjax'=>'0', 'data-toggle'=>'tooltip', 'data-title'=>Yii::t('app','Import')]);
      }
      if($this->export==true && Yii::$app->menuHelperFunction->checkActionAllowed('export')){
        $Btns.='&nbsp;'.Html::a('<i class="fas fa-download"></i>',['export'],['class'=>'btn btn-xs btn-info', 'data-pjax'=>'0', 'data-toggle'=>'tooltip', 'data-title'=>Yii::t('app','Export')]);
      }
      $carHeaderHtml='
      <header class="card-header">
        <div class="row">
          <div class="col-xs-12 col-sm-6">
            <h2 class="card-title clearfix">
							<span class="pull-left">
								'.($this->cardTitle!='' ? $this->cardTitle : $this->view->title).'
							</span>
							<span class="pull-left">
				        <div class="btn-group" role="group">
				            <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				              Save to list <i class="mdi mdi-chevron-down"></i>
				            </button>
				            <ul class="dropdown-menu" style="">
				                <li class="dropdown-item">
				                  <a href="javascript:;" class="load-list-modal" data-url="'.Url::to(['list/create']).'" data-mtype="'.$searchModel->ModuleTypeId.'" data-heading="'.Yii::t('app','Create new list').'">
				                    '.Yii::t('app','Create New List').'
				                  </a>
				                </li>
				                <li class="dropdown-item">
				                  <a href="javascript:;" class="load-list-modal" data-url="'.Url::to(['list/choose']).'" data-mtype="'.$searchModel->ModuleTypeId.'" data-heading="'.Yii::t('app','Select List').'">
				                    '.Yii::t('app','Select a List').'
				                  </a>
				                </li>
				            </ul>
				        </div>
				      </span>
						</h2>
          </div>
          <div class="col-xs-12 col-sm-6">
            <div class="clearfix">
              <div class="float-right">
                '.$Btns.'
              </div>
              <div class="float-right" style="margin-right:10px;">
                '.($this->showPerPage==true ? Html::activeDropDownList($this->filterModel, 'pageSize', Yii::$app->params['pageSizeArray'], ['id'=>'filter-pageSize', 'class'=>'form-control input-xs']) : '').'
              </div>
            </div>
          </div>
        </div>
      </header>';

      //layout showing only items
			$tableHtml = '<table id="'.$this->tableOptions['id'].'" class="table table-bordered table-stripped">';
			$tableHtml.= '	<thead>';
			$tableHtml.= '		<tr>';
			if($this->columns!=null){
				foreach($this->columns as $key=>$val){
					if($this->cbSelection==true && $key=='cb_col'){
						$tableHtml.='<th><input type="checkbox" class="select-on-check-all" name="selection_all" value="1"></th>';
					}else{
						$tableHtml.='<th>'.$val.'</th>';
					}
				}
			}
			$tableHtml.= '		</tr>';
			$tableHtml.= '	</thead>';
			$tableHtml.= '</table>';

      $layout = '
      <section class="dtdv1 card'.($this->cardHeader==true ? ' card card-outline card-primary' : '').'">
        '.$carHeaderHtml.'
        <div class="card-body tbl-container">
          '.$tableHtml.'
        </div>
      </section>';
			echo $layout;
  }

  /**
   * Returns the options for the datatables view JS widget.
   * @return array the options
   */
  protected function getClientOptions()
  {
      return $this->clientOptions;
  }
}
