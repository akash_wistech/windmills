<?php

namespace app\components\widgets;

use Yii;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use app\assets\DataTableGridAsset;

class DataTableGridView extends GridView
{
	public $cardTitle='';
	public $cardHeader=true;
	public $showPerPage=false;
	public $createBtn=false;
	public $mailBtn=false;
	public $showSummary=true;
  public $createBtnLink = ['create'];

	public $export=false;
	public $import=false;
	public $ajaxUrl=false;

  /**
  * @var array the HTML attributes for the container tag of the datatables view.
  */
  public $options = [];

  /**
  * @var array the HTML attributes for the datatables table element.
  */
  public $tableOptions = ["class"=>"table table-respons1ive-lg table-sm table-striped table-hover mb-0","cellspacing"=>"0", "width"=>"100%"];

  /**
  * @var array the HTML attributes for the datatables table element.
  */
  public $clientOptions = [
    "dom"=>"<'row'<'col-sm-6'Bf><'col-sm-6'l>><'table-responsive't><'row'<'col-sm-6'i><'col-sm-6'p>>",
    "columnDefs"=>[
			[
				'orderable'=>false,
				'targets'=>0
			],
			[
				'targets'=>0,
				'className'=>'noVis'
			]
		],
    "buttons"=>[
      [
        'extend'=>'colvis',
        'columns'=>':not(.noVis)',
        'collectionLayout'=>'two-column',
        'className'=>'btn btn-sm btn-info',
        'text'=>'Columns'
      ]
    ],
    "language"=>[
      "search"=>"",
      "lengthMenu"=>"_MENU_ Per Page",
    ]
  ];


  /**
   * Runs the widget.
   */
  public function run()
  {
      $clientOptions = $this->getClientOptions();
      $view = $this->getView();
      $gridId=$this->options['id'];
      $id = $this->tableOptions['id'];

      //Bootstrap3 Asset by default
      DataTableGridAsset::register($view);


      $options = Json::encode($clientOptions);
      $view->registerJs("jQuery('#$id').DataTable($options);");

      //base list view run
      if ($this->showOnEmpty || $this->dataProvider->getCount() > 0) {
          $content = preg_replace_callback("/{\\w+}/", function ($matches) {
              $content = $this->renderSection($matches[0]);
              return $content === false ? $matches[0] : $content;
          }, $this->layout);
      } else {
          $content = $this->renderEmpty();
      }
      $tag = ArrayHelper::remove($this->options, 'tag', 'div');
      echo Html::tag($tag, $content, $this->options);
  }

  /**
   * Initializes the datatables widget disabling some GridView options like
   * search, sort and pagination and using DataTables JS functionalities
   * instead.
   */
  public function init()
  {
      parent::init();

			$searchModel = $this->filterModel;

      //disable filter model by grid view
      $this->filterModel = null;

      //disable sort by grid view
      $this->dataProvider->sort = false;

      //disable pagination by grid view
      $this->dataProvider->pagination = false;

      $Btns='';

      if($this->createBtn==true){
        $Btns.='&nbsp;'.Html::a('<i class="fas fa-plus"></i>',$this->createBtnLink,['class'=>'btn btn-xs btn-primary', 'data-pjax'=>'0', 'data-toggle'=>'tooltip', 'data-title'=>Yii::t('app','Add New')]);
      }
      if($this->mailBtn==true){
        $Btns.='&nbsp;'.Html::a('<i class="fas fa-envelope"></i>','javascript:;',['id'=>'btn-mailing', 'class'=>'btn btn-xs btn-primary', 'data-pjax'=>'0', 'data-toggle'=>'tooltip', 'data-title'=>Yii::t('app','Send Email')]);
      }
      if($this->import==true && Yii::$app->menuHelperFunction->checkActionAllowed('import')){
        $Btns.='&nbsp;'.Html::a('<i class="fas fa-file-import"></i>',['import'],['class'=>'btn btn-xs btn-success', 'data-pjax'=>'0', 'data-toggle'=>'tooltip', 'data-title'=>Yii::t('app','Import')]);
      }
      if($this->export==true && Yii::$app->menuHelperFunction->checkActionAllowed('export')){
        $Btns.='&nbsp;'.Html::a('<i class="fas fa-download"></i>',['export'],['class'=>'btn btn-xs btn-info', 'data-pjax'=>'0', 'data-toggle'=>'tooltip', 'data-title'=>Yii::t('app','Export')]);
      }
      $carHeaderHtml='
      <header class="card-header">
        <div class="row">
          <div class="col-xs-12 col-sm-6">
            <h2 class="card-title clearfix">
							<span class="pull-left">
								'.($this->cardTitle!='' ? $this->cardTitle : $this->view->title).'
							</span>
							<span class="pull-left">
				        <div class="btn-group" role="group">
				            <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				              Save to list <i class="mdi mdi-chevron-down"></i>
				            </button>
				            <ul class="dropdown-menu" style="">
				                <li class="dropdown-item">
				                  <a href="javascript:;" class="load-list-modal" data-url="'.Url::to(['list/create']).'" data-mtype="'.$searchModel->ModuleTypeId.'" data-heading="'.Yii::t('app','Create new list').'">
				                    '.Yii::t('app','Create New List').'
				                  </a>
				                </li>
				                <li class="dropdown-item">
				                  <a href="javascript:;" class="load-list-modal" data-url="'.Url::to(['list/choose']).'" data-mtype="'.$searchModel->ModuleTypeId.'" data-heading="'.Yii::t('app','Select List').'">
				                    '.Yii::t('app','Select a List').'
				                  </a>
				                </li>
				            </ul>
				        </div>
				      </span>
						</h2>
          </div>
          <div class="col-xs-12 col-sm-6">
            <div class="clearfix">
              <div class="float-right">
                '.$Btns.'
              </div>
              <div class="float-right" style="margin-right:10px;">
                '.($this->showPerPage==true ? Html::activeDropDownList($this->filterModel, 'pageSize', Yii::$app->params['pageSizeArray'], ['id'=>'filter-pageSize', 'class'=>'form-control input-xs']) : '').'
              </div>
            </div>
          </div>
        </div>
      </header>';

      //layout showing only items
      $this->layout = '
      <section class="dtdv1 card'.($this->cardHeader==true ? ' card card-outline card-primary' : '').'">
        '.$carHeaderHtml.'
        <div class="card-body tbl-container">
          {items}
        </div>
        {pager}
      </section>';

      //the table id must be set
      if (!isset($this->tableOptions['id'])) {
          $this->tableOptions['id'] = 'datatables_'.$this->getId();
      }
  }

  /**
   * Returns the options for the datatables view JS widget.
   * @return array the options
   */
  protected function getClientOptions()
  {
      return $this->clientOptions;
  }

  public function renderTableBody()
  {
      $models = array_values($this->dataProvider->getModels());
      if (count($models) === 0) {
          return "<tbody>\n</tbody>";
      } else {
          return parent::renderTableBody();
      }
  }
}
