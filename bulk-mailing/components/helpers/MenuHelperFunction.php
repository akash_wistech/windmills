<?php
namespace app\components\helpers;

use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\AdminMenu;
use app\models\AdminGroupPermissions;
use app\models\AdminGroupPermissionType;
use app\models\AdminGroup;

class MenuHelperFunction extends Component
{
	public function getAdminGroupList()
	{
		return AdminGroup::find()
		->select([
			'id',
			'title',
		])
		->orderBy(['id'=>SORT_ASC])
		->asArray()
		->all();
	}

	public function getAdminGroupListArr()
	{
		return ArrayHelper::map($this->adminGroupList,"id","title");
	}

	public function getAdminMenuList()
	{
		return AdminMenu::find()
		->select([
			'id',
			'title',
			'action_id',
			'ask_list_type',
		])
		->where(['parent'=>0])
		->orderBy(['rank'=>SORT_ASC])
		->asArray()
		->all();
	}

	public function getSubOptions($parent_id)
	{
		return AdminMenu::find()
		->select([
			'id',
			'title',
			'action_id',
			'ask_list_type',
		])
		->where(['parent'=>$parent_id])
		->orderBy(['rank'=>SORT_ASC])
		->asArray()
		->all();
	}

	public function getSubMenuOptions($groupId,$parent_id)
	{
		return AdminGroupPermissions::find()
		->select([
			'menu_id'=>AdminGroupPermissions::tableName().'.menu_id',
			'title'=>AdminMenu::tableName().'.title',
			'controller_id'=>AdminMenu::tableName().'.controller_id',
			'action_id'=>AdminMenu::tableName().'.action_id',
			'icon'=>AdminMenu::tableName().'.icon',
		])
		->innerJoin("admin_menu",AdminMenu::tableName().".id=".AdminGroupPermissions::tableName().".menu_id")
		->where(['parent'=>$parent_id,'show_in_menu'=>1,'group_id'=>$groupId])
		->orderBy([AdminMenu::tableName().'.rank'=>SORT_ASC])
		->asArray()->all();
	}

	public function getGenerateStaffMenu()
	{
		$loggedInUser=Yii::$app->user->identity;
		$groupId=$loggedInUser->permission_group_id;
		$html='';
		$results=AdminGroupPermissions::find()->select([
			'menu_id'=>AdminGroupPermissions::tableName().'.menu_id',
			'title'=>AdminMenu::tableName().'.title',
			'controller_id'=>AdminMenu::tableName().'.controller_id',
			'action_id'=>AdminMenu::tableName().'.action_id',
			'icon'=>AdminMenu::tableName().'.icon',
		])
		->innerJoin("admin_menu",AdminMenu::tableName().".id=".AdminGroupPermissions::tableName().".menu_id")
		->where(['group_id'=>$groupId,'show_in_menu'=>1,'parent'=>0])
		->orderBy([AdminMenu::tableName().'.rank'=>SORT_ASC])
		->asArray()->all();
		if($results!=null){
			foreach($results as $result){
				$html.=$this->generateSubMenu($result,$groupId);
			}
		}
		return $html;
	}

	public function generateSubMenu($result,$groupId)
	{
		$html='';
		$subOptions = $this->getSubMenuOptions($groupId,$result['menu_id']);
		$isActive=false;
		$activeIdz = $this->isActive;
		if(in_array($result['menu_id'],$activeIdz))$isActive=true;
		if($subOptions!=null){
			$html.='<li class="nav-item has-treeview'.($isActive==true ? ' menu-open' : '').'">';
			$html.='	<a class="nav-link'.($isActive==true ? ' active' : '').'" href="#">';
			$html.='		<i class="nav-icon '.$result['icon'].'"></i>';
			$html.='		<p>';
			$html.='			'.$result['title'].'';
			$html.='			<i class="right fas fa-angle-left"></i>';
			$html.='		</p>';
			$html.='	</a>';
			$html.='	<ul class="nav nav-treeview">';
			foreach($subOptions as $subOption){
				$html.=$this->generateSubMenu($subOption,$groupId);
			}
			$html.='	</ul>';
			$html.='</li>';
		}else{
			$html.='<li class="nav-item">';
			$html.='	<a class="nav-link'.($isActive==true ? ' active' : '').'" href="'.Url::to([$result['controller_id'].'/'.$result['action_id']]).'">';
			$html.='		<i class="nav-icon '.$result['icon'].'"></i> <p>'.$result['title'].'</p>';
			$html.='	</a>';
			$html.='</li>';
		}
		return $html;
	}

	//check if current menu is active
	public function getIsActive()
	{
		$controller_id=Yii::$app->controller->id;
		$result=AdminMenu::find()->select(['id','parent'])->where(['controller_id'=>$controller_id])->asArray()->one();
		if($result!=null){
			$idz[]=$result['id'];
			return $this->getMenuParentIdz($result['parent'],$idz);
		}
		return [0];
	}

	//Get parent ids for active menu
	public function getMenuParentIdz($parent_id,$idz)
	{
		$result=AdminMenu::find()->select(['id','parent'])->where(['id'=>$parent_id])->asArray()->one();
		if($result!=null){
			$idz[]=$result['id'];
			return $this->getMenuParentIdz($result['parent'],$idz);
		}else{
			return $idz;
		}
	}

	//Before Action enableCsrfValidation
	public function checkPagePermission()
	{
		$controller=Yii::$app->controller;
		$loggedInUser=Yii::$app->user->identity;
		$groupId=$loggedInUser->groupId;
		$menu=AdminMenu::find()->where(['controller_id'=>$controller->id,'action_id'=>$controller->action->id])->one();
		if($menu!=null){
			if($menu->param1!=null && $menu->value1!=null){
				if(Yii::$app->request->get($menu->param1) && Yii::$app->request->get($menu->param1)==$menu->value1){
					$result=AdminGroupPermissions::find()->where(['group_id'=>$groupId,'menu_id'=>$menu->id])->one();
					if($result==null){
						return $controller->redirect(['site/not-allowed']);
					}
				}else{
					return $controller->redirect(['site/not-allowed']);
				}
			}else{
				$result=AdminGroupPermissions::find()->where(['group_id'=>$groupId,'menu_id'=>$menu->id])->one();
				if($result==null){
					return $controller->redirect(['site/not-allowed']);
				}
			}
		}else{
			return $controller->redirect(['site/not-allowed']);
		}
	}

	//Check action is allowed?
	public function checkActionAllowed($action_id,$controller_id=null,$param1=null,$value1=null)
	{
		if($controller_id==null)$controller_id=Yii::$app->controller->id;
		$menu=AdminMenu::find()->where(['controller_id'=>$controller_id,'action_id'=>$action_id])->one();
		if($menu!=null){
			if($menu->param1!=null && $menu->value1!=null){
				if($param1!=null && $param1==$menu->param1 && $value1!=null && $value1==$menu->value1){
					$result=AdminGroupPermissions::find()->where(['group_id'=>Yii::$app->user->identity->permission_group_id,'menu_id'=>$menu->id])->one();
					if($result==null){
						return false;
						exit;
					}
				}else{
					return false;
					exit;
				}
			}else{
				$result=AdminGroupPermissions::find()->where(['group_id'=>Yii::$app->user->identity->permission_group_id,'menu_id'=>$menu->id])->one();
				if($result==null){
					return false;
					exit;
				}
			}
		}else{
			return false;
			exit;
		}
		return true;
	}

	//Permission Group
	public function isChecked($group_id,$menu_id)
	{
		if($menu_id!=null){
			$result=AdminGroupPermissions::find()->where(['group_id'=>$group_id,'menu_id'=>$menu_id]);
			if($result->exists()){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

  /**
  * returns listing type value for the group
  */
	public function getListingType($group_id,$menu_id)
	{
		if($menu_id!=null){
			$result=AdminGroupPermissionType::find()->select(['list_type'])->where(['group_id'=>$group_id,'menu_id'=>$menu_id])->asArray()->one();
			if($result!=null){
				return $result['list_type'];
			}else{
				return 1;
			}
		}else{
			return 1;
		}
	}

  /**
  * returns listing type value for the group
  */
	public function getListingTypeByController($controller_id)
	{
		if($controller_id!=null){
			$result=AdminGroupPermissionType::find()
			->select(['list_type'])
			->where(['group_id'=>Yii::$app->user->identity->permission_group_id,'controller_id'=>$controller_id])
			->asArray()->one();
			if($result!=null){
				return $result['list_type'];
			}else{
				return 1;
			}
		}else{
			return 1;
		}
	}
}
?>
