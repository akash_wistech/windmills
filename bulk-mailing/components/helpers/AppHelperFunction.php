<?php
namespace app\components\helpers;

use Yii;
use yii\helpers\Url;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\db\Expression;
use app\models\CustomerSetting;
use app\models\Setting;
use app\models\User;
use app\models\AdminGroup;
use app\models\Company;
use app\models\Country;
use app\models\Zone;
use app\models\CustomFieldModule;
use app\models\PredefinedList;
use app\models\ModuleEmail;
use app\models\ModuleNumber;
use app\models\ModuleCompany;
use app\models\ModuleAddress;
use app\models\ModuleTag;
use app\models\ModuleManager;
use app\models\Lists;
use app\models\Tags;
use app\models\Template;
use app\models\EmailSmtpDetails;
use app\models\CampaignEmail;
use app\models\ApiSource;
use app\models\CronJobSyncApi;


class AppHelperFunction extends Component
{
  /**
  * Get settings value for a keyword
  */
  public static function getSetting($key)
  {
    $value = "";
    $model = Setting::find()->where("config_name='$key'")->asArray()->one();
    if ($model != null) {
      $value = $model['config_value'];
    }
    return $value;
  }

  /**
  * Get settings value for a keyword
  */
  public static function getCustomerSetting($comp_id,$key)
  {
    $value = "";
    $model = CustomerSetting::find()->where(['company_id'=>$comp_id,'config_name'=>$key])->asArray()->one();
    if ($model != null) {
      $value = $model['config_value'];
    }
    return $value;
  }

  /**
  * Default Smtp
  *
  * @return array
  */
  public function getDefaultSmtpInfo($company_id = null)
  {
    if($company_id==null)$company_id=Yii::$app->user->identity->company_id;
    $defSmtpId = $this->getCustomerSetting($company_id,'default_smtp');
    return EmailSmtpDetails::find()
    ->where([
      'and',
      ['id'=>$defSmtpId,'company_id'=>$company_id,'status'=>1],
      ['is','deleted_at',new Expression('null')]
    ])
    ->asArray()->one();
  }

  /**
  * page sizes for grid pagination
  *
  * @return array
  */
  public function getPageSizeArray()
  {
    return [
      25, 50, 100
    ];
  }

  /**
  * All active Permission groups
  *
  * @return array
  */
  public function getPermissionGroupList()
  {
    return AdminGroup::find()
    ->select([
      'id',
      'title',
    ])
    ->where(['and',['status' => 1],['is','deleted_at',new Expression('null')]])
    ->orderBy(['title' => SORT_ASC])->asArray()->all();
  }

  /**
  * All active Permission groups for dropdown
  *
  * @return array
  */
  public function getPermissionGroupListArr()
  {
    return ArrayHelper::map($this->permissionGroupList, "id", "title");
  }

  /**
  * All predefined lists
  *
  * @return array
  */
  public function getPredefinedList()
  {
    return PredefinedList::find()
    ->select([
      'id',
      'title',
    ])
    ->where(['and',['parent' => 0, 'status' => 1],['is','deleted_at',new Expression('null')]])
    ->orderBy(['title' => SORT_ASC])->asArray()->all();
  }

  /**
  * All predefined for dropdown
  *
  * @return array
  */
  public function getPredefinedListArr()
  {
    return ArrayHelper::map($this->predefinedList, "id", "title");
  }

  /**
  * All predefined lists
  *
  * @return array
  */
  public function getPredefinedListOptions($id)
  {
    return PredefinedList::find()
    ->select([
      'id',
      'title',
    ])
    ->where(['and',['parent' => $id, 'status' => 1],['is','deleted_at',new Expression('null')]])
    ->orderBy(['title' => SORT_ASC])->asArray()->all();
  }

  /**
  * All predefined for dropdown
  *
  * @return array
  */
  public function getPredefinedListOptionsArr($id)
  {
    return ArrayHelper::map($this->getPredefinedListOptions($id), "id", "title");
  }

  /**
  * predefined list option by id
  *
  * @return array
  */
  public function getPredefinedListOption($id)
  {
    return PredefinedList::find()
    ->select([
      'id',
      'title',
    ])
    ->where(['and',['id' => $id, 'status' => 1],['is','deleted_at',new Expression('null')]])
    ->asArray()->one();
  }

  /**
  * All active Emirates
  *
  * @return array
  */
  public function getStaffMemberList()
  {
    return User::find()
    ->select([
      'id',
      'full_name' => 'CONCAT(firstname," ",lastname)',
    ])
    ->where(['and',['user_type' => 10, 'status' => 1],['is','deleted_at',new Expression('null')]])
    ->orderBy(['full_name' => SORT_ASC])->asArray()->all();
  }

  /**
  * All active Emirates for dropdown
  *
  * @return array
  */
  public function getStaffMemberListArr()
  {
    return ArrayHelper::map($this->staffMemberList, "id", "full_name");
  }

  /**
  * All email for a module
  *
  * @return \yii\db\ActiveQuery
  */
  public function getModuleEmails($model)
  {
    return ModuleEmail::find()
    ->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id])
    ->asArray()->all();
  }

  /**
  * All numbers for a module
  *
  * @return \yii\db\ActiveQuery
  */
  public function getModuleNumbers($model)
  {
    return ModuleNumber::find()
    ->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id])
    ->asArray()->all();
  }

  /**
  * All companies for a module
  *
  * @return \yii\db\ActiveQuery
  */
  public function getModuleCompany($model)
  {
    return ModuleCompany::find()
    ->select([
      ModuleCompany::tableName().'.id',
      ModuleCompany::tableName().'.company_id',
      'company_name'=>'comp.title',
      ModuleCompany::tableName().'.role_id',
      'role_name'=>'jr.title',
      ModuleCompany::tableName().'.job_title_id',
      'job_title'=>'jt.title',
    ])
    ->leftJoin(Company::tableName().' as comp','comp.id='.ModuleCompany::tableName().'.company_id')
    ->leftJoin(PredefinedList::tableName().' as jr','jr.id='.ModuleCompany::tableName().'.role_id')
    ->leftJoin(PredefinedList::tableName().' as jt','jt.id='.ModuleCompany::tableName().'.job_title_id')
    ->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id])
    ->asArray()->all();
  }

  /**
  * All companies for a module
  *
  * @return \yii\db\ActiveQuery
  */
  public function getModuleCompanyById($type,$id)
  {
    return ModuleCompany::find()
    ->select([
      ModuleCompany::tableName().'.id',
      ModuleCompany::tableName().'.company_id',
      'company_name'=>'comp.title',
      ModuleCompany::tableName().'.role_id',
      'role_name'=>'jr.title',
      ModuleCompany::tableName().'.job_title_id',
      'job_title'=>'jt.title',
    ])
    ->leftJoin(Company::tableName().' as comp','comp.id='.ModuleCompany::tableName().'.company_id')
    ->leftJoin(PredefinedList::tableName().' as jr','jr.id='.ModuleCompany::tableName().'.role_id')
    ->leftJoin(PredefinedList::tableName().' as jt','jt.id='.ModuleCompany::tableName().'.job_title_id')
    ->where(['module_type'=>$type,'module_id'=>$id])
    ->asArray()->all();
  }

  /**
  * All Addresses for a module
  *
  * @return \yii\db\ActiveQuery
  */
  public function getModuleAddress($model)
  {
    $countryQuery=Country::find()
    ->select([Country::tableName().'.id','pdl.title'])
    ->innerJoin(['pdl'=>PredefinedList::tableName()],"pdl.id=".Country::tableName().".id");

    $zoneQuery=Zone::find()
    ->select([Zone::tableName().'.id','pdl.title'])
    ->innerJoin(['pdl'=>PredefinedList::tableName()],"pdl.id=".Zone::tableName().".id");

    return ModuleAddress::find()
    ->select([
      ModuleAddress::tableName().'.id',
      ModuleAddress::tableName().'.is_primary',
      ModuleAddress::tableName().'.country_id',
      'country_name'=>'country.title',
      ModuleAddress::tableName().'.zone_id',
      'zone_name'=>'zone.title',
      ModuleAddress::tableName().'.city',
      ModuleAddress::tableName().'.phone',
      ModuleAddress::tableName().'.address',
    ])
    ->innerJoin(['country'=>$countryQuery],"country.id=".ModuleAddress::tableName().".country_id")
    ->innerJoin(['zone'=>$zoneQuery],"zone.id=".ModuleAddress::tableName().".zone_id")
    ->where([
      'and',
      [
        'module_type'=>$model->moduleTypeId,
        'module_id'=>$model->id
      ],
      ['is',ModuleAddress::tableName().'.deleted_at',new Expression('null')]
    ])
    ->asArray()->all();
  }

  /**
  * List of Lead Types
  *
  * @return array
  */
  public function getLeadTypeListArr()
  {
    return [
      '1' => Yii::t('app', 'None'),
      '2' => Yii::t('app', 'Web'),
      '3' => Yii::t('app', 'In Person'),
      '4' => Yii::t('app', 'Phone'),
      '5' => Yii::t('app', 'Email'),
    ];
  }

  /**
  * List of Lead Sources
  *
  * @return array
  */
  public function getLeadSourceListArr()
  {
    return [
      '1' => Yii::t('app', 'None'),
      '2' => Yii::t('app', 'Google'),
      '3' => Yii::t('app', 'Facebook'),
      '4' => Yii::t('app', 'Walk In'),
    ];
  }

  /**
  * List of Lead Status
  *
  * @return array
  */
  public function getLeadStatusListArr()
  {
    return [
      '1' => Yii::t('app', 'Unassigned'),
      '2' => Yii::t('app', 'Assigned'),
      '3' => Yii::t('app', 'Accepted'),
      '4' => Yii::t('app', 'Working'),
      '5' => Yii::t('app', 'Dead'),
      '6' => Yii::t('app', 'Rejected'),
    ];
  }

  /**
  * List of Deal Status
  *
  * @return array
  */
  public function getDealStatusListArr()
  {
    return [
      '1' => Yii::t('app', 'Working'),
      '2' => Yii::t('app', 'Won'),
      '3' => Yii::t('app', 'Lost'),
    ];
  }

  /**
  * List of Deal Status
  *
  * @return array
  */
  public function getLeadStagesListArr()
  {
    return [
      '1' => Yii::t('app', 'Working'),
      '2' => Yii::t('app', 'Won'),
      '3' => Yii::t('app', 'Lost'),
    ];
  }

  /**
  * List of Module Types
  *
  * @return array
  */
  public function getModuleTypeListArr()
  {

    return [
      'prospect' => Yii::t('app', 'All Data'),
      'customer' => Yii::t('app', 'Customer'),
      'client' => Yii::t('app', 'Clients'),
      'contact' => Yii::t('app', 'Contacts'),
      'opportunity' => Yii::t('app', 'Opportunities'),
    ];
  }

  /**
  * List of Module Type Names for a workflow
  *
  * @return array
  */
  public function getCustomFieldModuleNames($id)
  {
    $listArr=$this->moduleTypeListArr;
    $namesArr=[];
    $results=CustomFieldModule::find()
    ->select(['module_type'])
    ->where(['custom_field_id'=>$id])
    ->asArray()->all();
    if($results!=null){
      foreach($results as $result){
        $namesArr[]=$listArr[$result['module_type']];
      }
    }
    return $namesArr;
  }

  /**
  * get Templates list
  */
  public function getLists($company_id)
  {
    return Lists::find()
    ->select(['id','title'])
    ->where(['and',['company_id'=>$company_id],['is','deleted_at',new Expression('null')]])
    ->asArray()->all();
  }

  /**
  * get Templates list array
  */
  public function getListsArr($company_id)
  {
    return ArrayHelper::map($this->getLists($company_id),'id','title');
  }

  /**
  * get Templates list
  */
  public function getSmtpServersLists($company_id)
  {
    return EmailSmtpDetails::find()
    ->select(['id','host'=>'CONCAT(UCASE(service_type)," (",smtp_host,")")'])
    ->where(['and',['company_id'=>$company_id,'status'=>1],['is','deleted_at',new Expression('null')]])
    ->asArray()->all();
  }

  /**
  * get Templates list array
  */
  public function getSmtpServersListsArr($company_id)
  {
    return ArrayHelper::map($this->getSmtpServersLists($company_id),'id','host');
  }

  /**
  * get Templates list
  */
  public function getEmailTemplates($company_id)
  {
    return Template::find()
    ->select(['id','title'])
    ->where(['and',['company_id'=>$company_id],['is','deleted_at',new Expression('null')]])
    ->asArray()->all();
  }

  /**
  * get Templates list array
  */
  public function getEmailTemplatesArr($company_id)
  {
    return ArrayHelper::map($this->getEmailTemplates($company_id),'id','title');
  }

  /**
  * get Templates list
  */
  public function getEmployeeList($company_id)
  {
    return User::find()
    ->select(['id','fullname'=>'CONCAT(firstname," ",lastname)'])
    ->where(['and',['user_type'=>[0,1],'company_id'=>$company_id],['is','deleted_at',new Expression('null')]])
    ->asArray()->all();
  }

  /**
  * get Templates list array
  */
  public function getEmployeeListArr($company_id)
  {
    return ArrayHelper::map($this->getEmployeeList($company_id),'id','fullname');
  }

  /**
  * All active Countries
  *
  * @return array
  */
  public function getCountryList()
  {
    return Country::find()
    ->select([
      Country::tableName().'.id',
      'pdl.title',
    ])
    ->innerJoin(['pdl'=>PredefinedList::tableName()],"pdl.id=".Country::tableName().".id")
    ->where(['and',['pdl.status' => 1],['is','deleted_at',new Expression('null')]])
    ->orderBy(['pdl.title' => SORT_ASC])->asArray()->all();
  }

  /**
  * All active Countries for dropdown
  *
  * @return array
  */
  public function getCountryListArr()
  {
    return ArrayHelper::map($this->countryList, "id", "title");
  }

  /**
  * All active Zones
  *
  * @return array
  */
  public function getZoneList()
  {
    return Zone::find()
    ->select([
      Zone::tableName().'.id',
      'pdl.title',
    ])
    ->innerJoin(['pdl'=>PredefinedList::tableName()],"pdl.id=".Zone::tableName().".id")
    ->where(['and',['status' => 1],['is','deleted_at',new Expression('null')]])
    ->orderBy(['pdl.title' => SORT_ASC])->asArray()->all();
  }

  /**
  * All active Zones for dropdown
  *
  * @return array
  */
  public function getZoneListArr()
  {
    return ArrayHelper::map($this->zoneList, "id", "title");
  }

  /**
  * All active Zones
  *
  * @return array
  */
  public function getCountryZoneList($country_id)
  {
    return Zone::find()
    ->select([
      Zone::tableName().'.id',
      'pdl.title',
    ])
    ->innerJoin(['pdl'=>PredefinedList::tableName()],"pdl.id=".Zone::tableName().".id")
    ->where(['and',['country_id' => $country_id, 'status' => 1],['is','deleted_at',new Expression('null')]])
    ->orderBy(['pdl.title' => SORT_ASC])->asArray()->all();
  }

  /**
  * All active Zones for dropdown
  *
  * @return array
  */
  public function getCountryZoneListArr($country_id)
  {
    return ArrayHelper::map($this->getCountryZoneList($country_id), "id", "title");
  }

  /**
  * Get all module tags
  *
  * @return array
  */
  public function getModuleTags($moduleType,$moduleId)
  {
    $moduleTags=ModuleTag::find()->select(['tag_id'])->where(['module_type'=>$moduleType,'module_id'=>$moduleId]);
    return Tags::find()
    ->select(['title'])
    ->where([
      'and',
      ['id'=>$moduleTags],
      ['is','deleted_at',new Expression('null')]
    ])
    ->asArray()->all();
  }

  /**
  * Get all module tags as html
  *
  * @return array
  */
  public function getModuleTagsHtml($moduleType,$moduleId)
  {
    $html = '';
    $moduleTags=$this->getModuleTags($moduleType,$moduleId);
    if($moduleTags!=null){
      foreach($moduleTags as $moduleTag){
        $html.='<span class="badge badge-info">'.$moduleTag['title'].'</span> ';
      }
    }
    return $html;
  }

  /**
  * Get Campaign SMTP List
  *
  * @return array
  */
  public function getCampaignSmtpsListArr($campaign_id)
  {
    $arr=[];
    $subQueryCampaignSmtp=CampaignEmail::find()->select(['smtp_id'])->where(['campaign_id'=>$campaign_id]);
    $smtps=EmailSmtpDetails::find()->where(['id'=>$subQueryCampaignSmtp])->asArray()->all();
    if($smtps!=null){
      foreach($smtps as $smtp){
        $arr[$smtp['service_type']][]=$smtp['id'];
      }
    }
    return $arr;
  }

  /**
  * Get SMTP Service Types
  *
  * @return array
  */
  public function getSmtpServiceTypesArr()
  {
    return [
      'mailgun' => Yii::t('app','Mailgun'),
      'aws' => Yii::t('app','AWS SES'),
      'gmail' => Yii::t('app','Gmail'),
    ];
  }

  /**
  * Get SMTP Service Types Tracking Urls
  *
  * @return array
  */
  public function getSmtpServiceTypesTrackingOptsArr()
  {
    return [
      'mailgun' => Url::to(['mail-gun/stats']),
      'aws' => Url::to(['aws/listener']),
      'gmail' => Yii::t('app','Not applicable'),
    ];
  }

  /**
  * Get SMTP Encryption
  *
  * @return array
  */
  public function getSmtpServiceEncryptionTypesArr()
  {
    return [
      'tls' => Yii::t('app','TLS'),
      'ssl' => Yii::t('app','SSL'),
    ];
  }

  /**
  * Get Sync Progress
  *
  * @return array
  */
  public function getSyncProgress($model)
  {
    $str = '';
    $activeProcess = CronJobSyncApi::find()->where(['api_source_id'=>$model['id'],'status'=>2])->asArray()->one();
    if($activeProcess!=null){
      $percent=0;
      if($activeProcess['total_pages']>0){
        $percent = (($activeProcess['done_pages']/$activeProcess['total_pages'])*100);
      }
      $percent = round($percent);
      $str = '<span class="progress"><span class="progress-bar bg-success" role="progressbar" aria-valuenow="'.$percent.'" style="width: '.$percent.'%">'.$percent.'%</span></span>';
    }else{
      if($model['last_sync']!=null)$str = 'Last synced: '.Yii::$app->formatter->asDateTime($model['last_sync']);
    }
    return '&nbsp;<small>'.$str.'</small>';
  }
}

?>
