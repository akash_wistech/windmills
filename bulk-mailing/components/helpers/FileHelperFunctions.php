<?php
namespace app\components\helpers;

use Yii;
use yii\helpers\Url;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Response;

class FileHelperFunctions extends Component
{

    public $allowedFileTypes=[
      'jpg','JPG','jpeg','JPEG','png','PNG','gif','GIF','doc','DOC','docx','DOCX',
      'xls','XLS','xlsx','XLSX','pdf','PDF'
    ];

    public $allowedImageTypes=['jpg','JPG','jpeg','JPEG','png','PNG','gif','GIF'];

  /**
   * get cache image path
   */
	public function getCacheImagePath()
	{
		return [
			'rel'=> '/cache/',
			'abs'=> dirname(dirname(__DIR__)) . '/cache/',
		];
	}
  /**
   * get customer image path
   */
	public function getCustomerImagePath()
	{
		return [
			'rel'=> '',
			'abs'=> dirname(dirname(__DIR__)) . '/uploads/customer/',
		];
	}
  /**
   * get customer image path
   */
	public function getCampaignPath()
	{
		return [
			'rel'=> '',
			'abs'=> dirname(dirname(__DIR__)) . '/uploads/campaign/',
		];
	}
  /**
   * get GrapesJs image path
   */
	public function getGjsAssetPath()
	{
		return [
			'rel'=> Url::to(['/']).'uploads/gjs/',
			'abs'=> dirname(dirname(__DIR__)) . '/uploads/gjs/',
		];
	}
  /**
   * get temp uploads path
   */
	public function getTempImagePath()
	{
		return [
			'rel'=> '',
			'abs'=> dirname(dirname(__DIR__)) . '/uploads/temp_uploads/',
		];
	}
  /**
   * Move local temp image to AWS bucket
   */
	public function getDefaultPhoto($type)
	{
		if($type=='user'){
			return 'images/default/default_avatar.png';
		}else{
			return 'images/default/default.png';
		}
	}

	public function getImagePath($source,$image,$size)
	{
		$photo='';
		if($image!='' && $image!=null){
			if(filter_var($image, FILTER_VALIDATE_URL)){
				return $image;
			}
			$ext = pathinfo($image, PATHINFO_EXTENSION);
			if($source=='user'){
				$absPath=$this->customerImagePath['abs'];
				if(file_exists($absPath.$image)){
					if($size=='topbar'){
  					$photo=$this->resizeAbsolute($absPath, $image, 25, 25);
  				}elseif($size=='tiny'){
  					$photo=$this->resizeAbsolute($absPath, $image, 75, 75);
  				}elseif($size=='small'){
  					$photo=$this->resizeAbsolute($absPath, $image, 160, 160);
  				}elseif($size=='medium'){
  					$photo=$this->resizeAbsolute($absPath, $image, 160, 160);
  				}elseif($size=='full'){
  					$photo=$this->resizeAbsolute($absPath, $image, 500, 500);
  				}
				}
			}
			if($source=='attachment'){
				$absPath=Yii::$app->params['attachment_uploads_abs_path'];
				if(file_exists($absPath.$image)){
					if($size=='icon'){
  					$photo=$this->resizeAbsolute($absPath, $image, 75, 75);
  				}elseif($size=='small'){
  					$photo=$this->resizeAbsolute($absPath, $image, 160, 160);
  				}
				}
			}
		}
		if($photo==''){
			$photo=$this->getDefaultPhoto($source);
		}
		return $photo;
	}

	public function resizeAbsolute($folder, $filename, $width, $height)
	{
		if (!is_file($folder . $filename)) {
			return;
		}
		$extension = pathinfo($filename, PATHINFO_EXTENSION);
		$old_image = $filename;
	 	$new_image = substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;
		$cacheFolder = $this->cacheImagePath['abs'];

		if (!is_file($cacheFolder . $new_image) || (filectime($folder . $old_image) > filectime($cacheFolder . $new_image))) {
			list($width_orig, $height_orig) = getimagesize($folder . $old_image);
			if($width>1000 || $height>1000){
				copy($folder . $old_image, $cacheFolder . $new_image);
			}else{

				if (($width_orig > $width || $height_orig > $height) && ($width_orig != $width || $height_orig != $height)) {
					$image = new ImageResize($folder . $old_image);
					$image->crop($width, $height);
					$image->save($cacheFolder . $new_image);

					//Crop
					//$image = new ImageResize($cacheFolder . $new_image);
					//$image->crop($width, $height);
					//$image->save($cacheFolder . $new_image);
				} else {
					copy($folder . $old_image, $cacheFolder . $new_image);
				}
			}
		}
		return $this->cacheImagePath['rel'] . $new_image;
	}

	public function resizeByWidth($folder, $filename, $width, $height)
	{
		if (!is_file($folder . $filename)) {
			return;
		}
		$extension = pathinfo($filename, PATHINFO_EXTENSION);
		$old_image = $filename;
	 	$new_image = substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;
		$cacheFolder = $this->cacheImagePath['abs'];

		if (!is_file($cacheFolder . $new_image) || (filectime($folder . $old_image) > filectime($cacheFolder . $new_image))) {
			list($width_orig, $height_orig) = getimagesize($folder . $old_image);
			if($width>1000 || $height>1000){
				copy($folder . $old_image, $cacheFolder . $new_image);
			}else{
				if (($width_orig > $width || $height_orig > $height) && ($width_orig != $width || $height_orig != $height)) {
					$image = new ImageResize($folder . $old_image);
					$image->resizeToWidth($width);
					$image->save($cacheFolder . $new_image);
				} else {
					copy($folder . $old_image, $cacheFolder . $new_image);
				}
			}
		}
		return $this->cacheImagePath['rel'] . $new_image;
	}

	public function resizeByHeight($folder, $filename, $width, $height)
	{
		if (!is_file($folder . $filename)) {
			return;
		}
		$extension = pathinfo($filename, PATHINFO_EXTENSION);
		$old_image = $filename;
	 	$new_image = substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;
		$cacheFolder = $this->cacheImagePath['abs'];

		if (!is_file($cacheFolder . $new_image) || (filectime($folder . $old_image) > filectime($cacheFolder . $new_image))) {
			list($width_orig, $height_orig) = getimagesize($folder . $old_image);
			if($width>1000 || $height>1000){
				copy($folder . $old_image, $cacheFolder . $new_image);
			}else{
				if (($width_orig > $width || $height_orig > $height) && ($width_orig != $width || $height_orig != $height)) {
					$image = new ImageResize($folder . $old_image);
					$image->resizeToHeight($width);
					$image->save($cacheFolder . $new_image);
				} else {
					copy($folder . $old_image, $cacheFolder . $new_image);
				}
			}
		}
		return $this->cacheImagePath['rel'] . $new_image;
	}

	public function resizeBestFit($folder, $filename, $width, $height)
	{
		if (!is_file($folder . $filename)) {
			return;
		}
		$extension = pathinfo($filename, PATHINFO_EXTENSION);
		$old_image = $filename;
	 	$new_image = substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;
		$cacheFolder = $this->cacheImagePath['abs'];

		if (!is_file($cacheFolder . $new_image) || (filectime($folder . $old_image) > filectime($cacheFolder . $new_image))) {
			list($width_orig, $height_orig) = getimagesize($folder . $old_image);
			if (($width_orig > $width || $height_orig > $height) && ($width_orig != $width || $height_orig != $height)) {
				$image = new ImageResize($folder . $old_image);
				$image->resizeToBestFit($width, $height);
				$image->save($cacheFolder . $new_image);
			} else {
				copy($folder . $old_image, $cacheFolder . $new_image);
			}
		}
		return $this->cacheImagePath['rel'] . $new_image;
	}

	public function getGenerateName()
	{
		return str_replace(array(" ","."),"",microtime());
	}

  /**
   * Generates google map static image
   */
	public function copyGoogleImage($lat,$lng)
	{
		$imagePath = 'https://maps.googleapis.com/maps/api/staticmap?zoom=15&size=800x400&maptype=roadmap&markers=color:red%7C'.$lat.','.$lng.'&key='.Yii::$app->params['googleApiKey'].'';
		$copiedImage = Yii::$app->helperFunction->generateName().".png";

		$image = file_get_contents($imagePath);
		$fp  = fopen(Yii::$app->params['gmap_uploads_abs_path'].$copiedImage, 'w+');
		fputs($fp, $image);
		fclose($fp);
		unset($image);
		return $copiedImage;
		//copy($imagePath, Yii::$app->params['media_outlet_gallery_uploads_abs_path'].$copiedImage);
	}

  /**
   * Move local temp image to AWS bucket
   */
	public function moveLocalToAws($folder,$fileName,$path,$newFileName)
	{
		ini_set('memory_limit', '-1');

		if (getimagesize($folder . $fileName)) {
			list($width_orig, $height_orig) = getimagesize($folder . $fileName);

			$image = new ImageHelper($folder . $fileName);
			$image->resize($width_orig,$height_orig);
		}

		$dataFromFile = file_get_contents($folder . $fileName);
		$url = '';
		$s3 = Yii::$app->get('s3bucket');
    $uploadObject = $s3->upload($path . $newFileName, $folder.$fileName);

    if ($uploadObject) {
      // check if CDN host is available then upload and get cdn URL.
      $url = $s3->getUrl($path . $newFileName);
    }
		// $result = $s3->commands()->put($newFileName, $dataFromFile)->withContentType(mime_content_type($folder . $fileName))->execute();
		// $url = $s3->commands()->getUrl($newFileName)->execute();
		if($url!=''){
			unlink($folder . $fileName);
		}
		return $url;
	}

	public function copyToLocal($url,$newFileName)
	{
		ini_set('memory_limit', '-1');

		$content = file_get_contents($url);
		//Store in the filesystem.
		$fp = fopen($newFileName, "w");
		fwrite($fp, $content);
		fclose($fp);
		return $newFileName;
	}

	public function copyToLocalByCurl($url,$newFileName)
	{
		ini_set('memory_limit', '-1');

		$ch = curl_init($url);
		$fp = fopen($newFileName, 'wb');
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_exec($ch);
		curl_close($ch);
		fclose($fp);
		return $newFileName;
	}

	public function getGjsAssets()
	{
		$jsArr = [];
		$allowedExts = [];
		foreach($this->allowedImageTypes as $key=>$val){
			$allowedExts[]='*.'.$val;
		}
		$folder = $this->gjsAssetPath['abs'].Yii::$app->user->identity->company_id;
		$files=FileHelper::findFiles($folder,['only'=>$allowedExts]);
		if($files!=null){
			foreach($files as $file){
				$path_parts = pathinfo($file);
				$jsArr[]=['src'=>$this->gjsAssetPath['rel'].Yii::$app->user->identity->company_id.'/'.$path_parts['basename']];
			}
		}
		return json_encode($jsArr);
	}
}
?>
