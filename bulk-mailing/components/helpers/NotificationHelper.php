<?php
namespace app\components\helpers;

use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Notification;
use app\models\NotificationReceipient;
use yii\db\Expression;

class NotificationHelper extends Component
{
  public function getContentNotificationCategories()
  {
    return [
      'inquiry',
    ];
  }

  public static function log($recipients,$module,$type,$model,$sender_id)
  {
    $parent_id=0;
    if($type=='action'){

    }
    if($type=='calendar'){

    }

    $notification=Notification::find()->where([
      'and',
      [
        'category'=>$module,
        'parent_id'=>$model->module_id,
        'action_type'=>$type,
        'target_id'=>$model->id,
        'sender_id'=>$sender_id
      ],
      ['is','deleted_at',new Expression('null')]
    ])->one();
    if($notification==null){
      $notification = new Notification;
      $notification->category = $module;
      $notification->parent_id = $model->module_id;
      $notification->action_type = $type;
      $notification->target_id = $model->id;
      $notification->sender_id = $sender_id;
      $notification->receipient_id = $recipients;
      $notification->save();
    }
  }

  public function getDetail($model)
  {
    $isread=$model->meRecipient->is_read;

    $descp='';
    $limit="80";
		//if($model->showFullDetail==true){$limit=180;}

    $notificationOpts=$this->notificationOptions[$model->category][$model->action_type];

    if($model->category=='inquiry' && $model->action_type=='reply'){
      $row=GeneralInquiry::findOne($model->parent_id);
      if($row!=null){
        $descp = '
        <a href="'.Url::to(['inquiries/view','id'=>$row->rssid]).'" class="'.($isread==0 ? ' new' : '').'" data-pjax="0">
          New update on <strong>'.$row->subject.'</strong>
          <br /><small><i class="fa fa-clock-o"></i> '.Yii::$app->helperFunction->ConvertTime($model->created_at).'</small>
        </a>
			  ';
      }
    }

    return $descp;
  }

  public function deleteNotification($group,$type,$id)
  {
    $toDel=Notification::find()->where(['groups'=>$group,'type'=>$type,'target_id'=>$id,'created_by'=>Yii::$app->user->identity->id])->one();
    if($toDel!=null){
      $toDel->softDelete();
    }
  }

  public function getNotificationOptions()
  {
    return [
      'inquiry' => [
        'reply' => ['icon'=>'fa-envelope','color'=>''],
      ],
    ];
  }

	/**
	 * return new notifications count
   * @return integer
	 */
  public function getNewCount()
  {
		$subQueryNotificationReceipient=NotificationReceipient::find()
		->select(['notification_id'])
		->where(['receipient_id'=>Yii::$app->user->identity->id,'is_read'=>0]);
		return Notification::find()
		->where([
      'and',
			['id'=>$subQueryNotificationReceipient],
			['is','deleted_at',new Expression('null')]
		])
		->count('id');
  }
}
?>
