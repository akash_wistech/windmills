<?php
namespace app\components\helpers;

use Yii;
use yii\helpers\Url;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use DateTime;
use DateTimeZone;

class HelperFunctions extends Component
{
	/**
	* encrypts string with a key
	* @return string
	*/
	public function encryptData($data,$key)
	{
		return utf8_encode(Yii::$app->getSecurity()->encryptByKey($data,$key));
	}

	/**
	* dencrypts string with a key
	* @return string
	*/
	public function deryptData($data,$key)
	{
		return Yii::$app->getSecurity()->decryptByKey(utf8_decode($data),$key);
	}

	/**
	* returns number of days between two dates
	* @return integer
	*/
	public function getNumberOfDaysBetween($s,$e){
		$start = strtotime($s);
		$end = strtotime($e);

		$days_between = ceil(abs($end - $start) / 86400);
		return $days_between;
	}

	/**
	* converts a given date time to ago info string respect to current date time
	* @return string
	*/
	public function convertTime($time_ago2)
	{
		$time_ago = strtotime($time_ago2);
		$cur_time   = time();
		$time_elapsed   = $cur_time - $time_ago;
		$seconds    = $time_elapsed ;
		$minutes    = round($time_elapsed / 60 );
		$hours      = round($time_elapsed / 3600);
		$days       = round($time_elapsed / 86400 );
		$weeks      = round($time_elapsed / 604800);
		$months     = round($time_elapsed / 2600640 );
		$years      = round($time_elapsed / 31207680 );

		if($seconds <= 60){// Seconds
			return Yii::t('app', 'just now');
		}else if($minutes <=60){//Minutes
			if($minutes==1){
				return Yii::t('app', 'one minute ago');
			}else{
				return Yii::t('app', '{mn} minutes',['mn'=>$minutes]).' '.Yii::t('app', 'ago');
			}
		}else if($hours <=24){//Hours
			if($hours==1){
				return Yii::t('app', 'an hour ago');
			}else{
				return Yii::t('app', '{hr} hrs ago',['hr'=>$hours]);
			}
		}else{
			return Yii::$app->formatter->asdatetime($time_ago2);
		}
	}

	public function getDateTimeWithDiff($date)
	{
		$serverDif=Yii::$app->appHelperFunctions->getSetting('cron_server_diff');
		return date("Y-m-d H:i:s",strtotime($serverDif." Minute",strtotime($date)));
	}

	/**
	* returns a date before given date
	* @return datetime
	*/
	public function getDayBefore($date)
	{
		return date ("Y-m-d", strtotime("-1 day", strtotime($date)));
	}

	/**
	* returns a date after given date
	* @return datetime
	*/
	public function getNextDay($date)
	{
		return date ("Y-m-d", strtotime("+1 day", strtotime($date)));
	}

	/**
	* returns an array of start and end date for the difference
	* @return array
	*/
	public function getStartEndDateTime($date,$diff)
	{
		return ['start'=>date("Y-m-d H:i:s", strtotime($diff, strtotime($date))),'end'=>$date];
	}

	/**
	* returns an array of start and end date for the difference from now
	* @return array
	*/
	public function getStartEndDateTimeFromNow($diff)
	{
		$date=date("Y-m-d H:i:s");
		return ['start'=>date("Y-m-d H:i:s", strtotime($diff, strtotime($date))),'end'=>$date];
	}

	// Parses a string into a DateTime object, optionally forced into the given timeZone.
	function parseDateTime($string, $timeZone=null) {
		$date = new DateTime(
			$string,
			$timeZone ? $timeZone : new DateTimeZone('UTC')
			// Used only when the string is ambiguous.
			// Ignored if string has a timeZone offset in it.
		);
		if ($timeZone) {
			// If our timeZone was ignored above, force it.
			$date->setTimezone($timeZone);
		}
		return $date;
	}


	// Takes the year/month/date values of the given DateTime and converts them to a new DateTime,
	// but in UTC.
	function stripTime($datetime) {
		return $datetime->format('Y-m-d');
	}

	/**
	* returns GMT offset
	* @return string
	*/
	function getDateTimeZones(){
		$arr=[];
		$timeZones=timezone_identifiers_list();
		if($timeZones!=null){
			foreach($timeZones as $k=>$v){
				$arr[$v]=$v;
			}
		}
		return $arr;
	}

	/**
	* returns array of user types
	* @return array
	*/
	public function getUserTypes()
	{
		return [
			'0' => Yii::t('app','Contact / User'),
			'10' => Yii::t('app','Admin'),
			'20' => Yii::t('app','Super Admin'),
		];
	}

	/**
	* returns array of Yes/No
	* @return array
	*/
	public function getArrYesNo()
	{
		return [
			'1' => Yii::t('app','Yes'),
			'0' => Yii::t('app','No'),
		];
	}

	/**
	* returns array of Yes/No for badge icon html
	* @return array
	*/
	public function getArrYesNoIcon()
	{
		return [
			'1' => '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i></span>',
			'0' => '<span class="badge grid-badge badge-danger"><i class="fa fa-times"></i></span>',
		];
	}

	/**
	* returns array of Yes/No for badge html
	* @return array
	*/
	public function getArrYesNoSpan()
	{
		return [
			'1' => '<span class="badge grid-badge badge-success">'.Yii::t('app','Yes').'</span>',
			'0' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','No').'</span>',
		];
	}

	/**
	* returns array of Status types
	* @return array
	*/
	public function getArrPublishing()
	{
		return [
			'1' => Yii::t('app','Publish'),
			'2' => Yii::t('app','Disable'),
			'0' => Yii::t('app','Draft'),
		];
	}

	/**
	* returns array of status types html
	* @return array
	*/
	public function getArrPublishingIcon()
	{
		return [
			'1' => '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i> '.Yii::t('app','Published').'</span>',
			'2' => '<span class="badge grid-badge badge-warning"><i class="fa fa-hourglass"></i> '.Yii::t('app','Disabled').'</span>',
			'0' => '<span class="badge grid-badge badge-info"><i class="fa fa-edit"></i> '.Yii::t('app','Draft').'</span>',
		];
	}

	/**
	* returns array of status types for form Dropdowns
	* @return array
	*/
	public function getArrFormStatus()
	{
		return [
			'1' => Yii::t('app','Active'),
			'2' => Yii::t('app','Disable'),
			//'0' => Yii::t('app','Draft'),
		];
	}

	/**
	* returns array of status types for filter Dropdowns
	* @return array
	*/
	public function getArrFilterStatus()
	{
		return [
			'1' => Yii::t('app','Active'),
			'2' => Yii::t('app','Disabled'),
			//'0' => Yii::t('app','Draft'),
		];
	}

	/**
	* returns array of status types with badge html
	* @return array
	*/
	public function getArrStatusIcon()
	{
		return [
			'1' => '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i> '.Yii::t('app','Active').'</span>',
			'2' => '<span class="badge grid-badge badge-warning"><i class="fa fa-hourglass"></i> '.Yii::t('app','Disabled').'</span>',
			//'0' => '<span class="badge grid-badge badge-info"><i class="fa fa-edit"></i> '.Yii::t('app','Draft').'</span>',
		];
	}

	/**
	* returns array of email status types
	* @return array
	*/
	public function getEmailPublishing()
	{
		return [
			'1' => Yii::t('app','Sent'),
			'0' => Yii::t('app','In Queue'),
			'2' => Yii::t('app','Draft'),
			'-1' => Yii::t('app','Canceled'),
		];
	}

	/**
	* returns array of email status types with badge html
	* @return array
	*/
	public function getEmailPublishingIcon()
	{
		return [
			'1' => '<span class="badge grid-badge badge-success"><i class="fa fa-check"></i> '.Yii::t('app','Sent').'</span>',
			'0' => '<span class="badge grid-badge badge-primary"><i class="fa fa-hourglass"></i> '.Yii::t('app','In Queue').'</span>',
			'2' => '<span class="badge grid-badge badge-info"><i class="fa fa-edit"></i> '.Yii::t('app','Draft').'</span>',
			'-1' => '<span class="badge grid-badge badge-danger">'.Yii::t('app','Cancelled').'</span>',
		];
	}

	/**
	* returns listing types for permission group
	*/
	public function getPermissionListingType()
	{
		return [
			'1'=>Yii::t('app','All Listing'),
			'2'=>Yii::t('app','Own Listing'),
		];
	}

	/**
	* returns array of week days
	* @return array
	*/
	public function getWeekDayNames()
	{
		return [
			'0' => 'Sunday',
			'1' => 'Monday',
			'2' => 'Tuesday',
			'3' => 'Wednesday',
			'4' => 'Thursday',
			'5' => 'Friday',
			'6' => 'Saturday',
		];
	}

	/**
	* returns if given date is a weekend
	* @return boolean
	*/
	public function isWeekend($date)
	{
		$weekDay = date('w', strtotime($date));
		return ($weekDay == 5 || $weekDay == 6);
	}

	/**
	* returns day of week
	* @return integer
	*/
	public function WeekendDay($date)
	{
		$weekDay = date('w', strtotime($date));
		return $weekDay;
	}

	/**
	* returns string with currency sign prefixed
	* @return string
	*/
	public function withCurrencySign($amount)
	{
		return Yii::$app->appHelperFunctions->getSetting('currency_sign').' '.Yii::$app->formatter->asDecimal($amount);
	}

	/**
	* returns array of visibility list
	* @return array
	*/
	public function getVisibilityListArr()
	{
		return [
			'1'=>Yii::t('app','Public'),
			'2'=>Yii::t('app','Private'),
		];
	}

	/**
	* returns array of priority list
	* @return array
	*/
	public function getPriorityListArr()
	{
		return [
			'1'=>Yii::t('app','Low'),
			'2'=>Yii::t('app','Medium'),
			'3'=>Yii::t('app','High'),
		];
	}

	/**
	* returns array of priority list
	* @return array
	*/
	public function getActionLogActionRemindToListArr()
	{
		return [
			'me'=>Yii::t('app','Me'),
			'assigned'=>Yii::t('app','Assigned Users'),
			'both'=>Yii::t('app','Me and Assigned Users'),
		];
	}

	/**
	* returns array of priority list
	* @return array
	*/
	public function getActionLogActionRemindTimeListArr()
	{
		return [
			'1'=>Yii::t('app','1 minute'),
			'5'=>Yii::t('app','5 minutes'),
			'10'=>Yii::t('app','10 minutes'),
			'15'=>Yii::t('app','15 minutes'),
			'30'=>Yii::t('app','30 minutes'),
			'60'=>Yii::t('app','1 hour'),
			'1440'=>Yii::t('app','1 day'),
			'10080'=>Yii::t('app','1 week'),
		];
	}

	/**
	* returns array of Log Call Quick Note action list
	* @return array
	*/
	public function getActionLogCallQuickNoteAction()
	{
		return [
			'1'=>Yii::t('app','Contacted'),
			'2'=>Yii::t('app','Not Contacted'),
		];
	}

	/**
	* returns array of Log Call Quick Note action response list
	* @return array
	*/
	public function getActionLogCallQuickNoteResponse()
	{
		return [
			'1'=>[
				'1'=>Yii::t('app','Not interested.'),
				'2'=>Yii::t('app','Requested follow up call.'),
				'3'=>Yii::t('app','Contact made.'),
			],
			'2'=>[
				'1'=>Yii::t('app','No answer.'),
				'2'=>Yii::t('app','Wrong number.'),
				'3'=>Yii::t('app','Left voicemail.'),
			],
		];
	}

	/**
	* returns array of Calendar Colors
	* @return array
	*/
	public function getCalendarColorListArr()
	{
		return [
			'#6389de'=>Yii::t('app','Blue'),
			'#a9c1fd'=>Yii::t('app','Light Blue'),
			'#5de1e5'=>Yii::t('app','Turquoise'),
			'#82e7c2'=>Yii::t('app','Light Green'),
			'#6bc664'=>Yii::t('app','Green'),
			'#fddb68'=>Yii::t('app','Yellow'),
			'#ffbc80'=>Yii::t('app','Orange'),
			'#ff978c'=>Yii::t('app','Pink'),
			'#e74046'=>Yii::t('app','Red'),
			'#d9adfb'=>Yii::t('app','Purple'),
			'#dedddd'=>Yii::t('app','Gray'),
		];
	}

	/**
	* returns array of Event Sub Types
	* @return array
	*/
	public function getEventSubTypesListArr()
	{
		return [
			'1'=>Yii::t('app','Meeting'),
			'2'=>Yii::t('app','Appointment'),
			'3'=>Yii::t('app','Call'),
		];
	}

	/**
	* returns array of Event Status
	* @return array
	*/
	public function getEventStatusListArr()
	{
		return [
			'1'=>Yii::t('app','Confirmed'),
			'2'=>Yii::t('app','Cancelled'),
		];
	}

	/**
	* returns array of property type for fee structure
	* @return array
	*/
	public function getFeeStructureTypeListArr()
	{
		return [
			'1'=>Yii::t('app','Free Hold'),
			'2'=>Yii::t('app','Non Free Hold'),
		];
	}


	/**
	* returns general delete confirmation msg
	* @return string
	*/
	public function getDelConfirmationMsg()
	{
		return Yii::t('app','Are you sure you want to delete this?');
	}


	/**
	* get campaign status
	*/
	public function getCampaignStatusArr()
	{
		return [
			'0' => Yii::t('app','Draft'),
			'1' => Yii::t('app','Published'),
		];
	}

	/**
	* get campaign mail status
	*/
	public function getCampaignMailStatusArr()
	{
		return [
			'0' => Yii::t('app','Pending'),
			'1' => Yii::t('app','Queued'),
			'2' => Yii::t('app','Finished'),
		];
	}

	/**
	* get module types for list and campaign
	*/
	public function getModuleTypeArr()
	{
		return [
			//'employee' => Yii::t('app','Employees'),
			'prospect' => Yii::t('app','All Data'),
		];
	}

	/**
	* get last six months from today
	*/
	public function getLastSixMonths()
	{
		$arr=[];
		for($n=5;$n>=1;$n--){
			$decVal=strtotime(date('Y-m')." -".$n." MONTH");
			$arr[date('Y-m', $decVal)]=date('M', $decVal);
		}
		$arr[date("Y-m")]=date('M');
		return $arr;
	}

	/**
	* get colors array
	*/
	public function getColorsArr()
	{
		return [
			'#28a745',
			'#555E7B',
			'#B7D968',
			'#B576AD',
			'#E04644',
			'#FDE47F',
			'#7CCCE5',
			'#BF4D28',
			'#99B2B7',
			'#E8BF56',
			'#480048',
			'#60B89A',
			'#99173C',
			'#948C75',
		];
	}

	/*
	*Sync Frequency
	*/
	public function getSyncFrequencyArr()
	{
		return [
			'daily' => Yii::t('app','Daily'),
			'weekly' => Yii::t('app','Weekly'),
			'monthly' => Yii::t('app','Monthly'),
			'yearly' => Yii::t('app','Yearly'),
		];
	}

	/*
	*Sync Frequency
	*/
	public function getSyncDateDifArr()
	{
		return [
			'daily' => 'DAY',
			'weekly' => 'WEEK',
			'monthly' => 'MONTH',
			'yearly' => 'YEAR',
		];
	}

	/*
	*curl Request
	*/
	public function curlReq($url)
  {
    // $apiKey='d9388c48a7491b9fcaac43550d12a3a50ee59d84';
		// $headers = array ( 'Authorization: key=' . $encyApi, 'Content-Type: application/json' );
		$ch = curl_init();
		curl_setopt( $ch,CURLOPT_URL, $url);
    curl_setopt( $ch,CURLOPT_POST, false );
    // curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    $result = curl_exec($ch);
    curl_close ($ch);
		return json_decode($result);
  }

	public function getDatatableActionTemplate($actionBtns)
	{
		return '<div class="btn-group flex-wrap">
			<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
				<span class="caret"></span>
			</button>
			<div class="dropdown-menu" role="menu">
				'.$actionBtns.'
			</div>
		</div>';
	}
}
?>
