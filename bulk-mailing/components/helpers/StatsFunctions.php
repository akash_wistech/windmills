<?php
namespace app\components\helpers;

use Yii;
use yii\base\Component;
use yii\db\Expression;
use app\models\Prospect;
use app\models\Lists;
use app\models\Campaign;
use app\models\CampaignEmail;
use app\models\Template;
use app\models\BlackListedEmail;
use app\models\DroppedEmail;
use app\models\User;
use app\models\PredefinedList;

class StatsFunctions extends Component
{
  /**
  * Total number of raw data
  * @return integer
  **/
  public function getRawDataCount($company_id=null)
  {
    return Prospect::find()
    ->Where(['is','deleted_at',new Expression('null')])
    ->andFilterWhere(['company_id'=>$company_id])
    ->count('id');
  }

  /**
  * Total number of lists
  * @return integer
  **/
  public function getListsCount($company_id=null)
  {
    return Lists::find()
    ->Where(['is','deleted_at',new Expression('null')])
    ->andFilterWhere(['company_id'=>$company_id])
    ->count('id');
  }

  /**
  * Total number of campaigns
  * @return integer
  **/
  public function getCampaignsCount($company_id=null)
  {
    return Campaign::find()
    ->Where(['is','deleted_at',new Expression('null')])
    ->andFilterWhere(['company_id'=>$company_id])
    ->count('id');
  }

  /**
  * Total number of templates
  * @return integer
  **/
  public function getTemplatesCount($company_id=null)
  {
    return Template::find()
    ->Where(['is','deleted_at',new Expression('null')])
    ->andFilterWhere(['company_id'=>$company_id])
    ->count('id');
  }

  /**
  * Total number of hard bounced
  * @return integer
  **/
  public function getHardBouncedCount($company_id=null)
  {
    return BlackListedEmail::find()
    ->Where(['is','deleted_at',new Expression('null')])
    ->count('id');
  }

  /**
  * Total number of soft bounced
  * @return integer
  **/
  public function getSoftBouncedCount($company_id=null)
  {
    return DroppedEmail::find()
    ->Where(['is','deleted_at',new Expression('null')])
    ->count('id');
  }

  /**
  * Total number of customers
  * @return integer
  **/
  public function getCustomerCount($company_id=null)
  {
    return User::find()
    ->select(['company_id'])
    ->Where([
      'and',
      ['user_type'=>0],
      ['is','deleted_at',new Expression('null')]
    ])
    ->distinct('company_id')
    ->count('company_id');
  }

  /**
  * Total number of predefined lists
  * @return integer
  **/
  public function getPdListsCount($company_id=null)
  {
    return PredefinedList::find()
    ->Where([
      'and',
      ['parent'=>0],
      ['is','deleted_at',new Expression('null')]
    ])
    ->count('id');
  }

  /**
  * get last six months email send data
  * @return integer
  **/
  public function getLastSixMonthEmailsStats($lastSixMonths,$companyId=null)
  {
    $sentData=[];
    $deliveredData=[];
    $bouncedData=[];
    $droppedData=[];
    $spamComplaintData=[];
    foreach($lastSixMonths as $key=>$val){
      $month=date("m",strtotime($key));
      $year=date("Y",strtotime($key));
      //Sent
      $sentData[]=CampaignEmail::find()->where([
        'MONTH('.CampaignEmail::tableName().'.distributed_at)'=>$month,
        'YEAR('.CampaignEmail::tableName().'.distributed_at)'=>$year
      ])
      ->innerJoin(Campaign::tableName(),Campaign::tableName().".id=".CampaignEmail::tableName().".campaign_id")
      ->andFilterWhere(['company_id'=>$companyId])
      ->count('email');
      //Delivered
      $deliveredData[]=CampaignEmail::find()->where([
        CampaignEmail::tableName().'.delivered'=>1,
        'MONTH('.CampaignEmail::tableName().'.distributed_at)'=>$month,
        'YEAR('.CampaignEmail::tableName().'.distributed_at)'=>$year
      ])
      ->innerJoin(Campaign::tableName(),Campaign::tableName().".id=".CampaignEmail::tableName().".campaign_id")
      ->andFilterWhere(['company_id'=>$companyId])
      ->count('email');
      //Bounced
      $bouncedData[]=CampaignEmail::find()->where([
        CampaignEmail::tableName().'.hard_bounced'=>1,
        'MONTH('.CampaignEmail::tableName().'.distributed_at)'=>$month,
        'YEAR('.CampaignEmail::tableName().'.distributed_at)'=>$year
      ])
      ->innerJoin(Campaign::tableName(),Campaign::tableName().".id=".CampaignEmail::tableName().".campaign_id")
      ->andFilterWhere(['company_id'=>$companyId])
      ->count('email');
      //Dropped
      $droppedData[]=CampaignEmail::find()->where([
        CampaignEmail::tableName().'.soft_bounced'=>1,
        'MONTH('.CampaignEmail::tableName().'.distributed_at)'=>$month,
        'YEAR('.CampaignEmail::tableName().'.distributed_at)'=>$year
      ])
      ->innerJoin(Campaign::tableName(),Campaign::tableName().".id=".CampaignEmail::tableName().".campaign_id")
      ->andFilterWhere(['company_id'=>$companyId])
      ->count('email');
      //Spam Complaint
      $spamComplaintData[]=CampaignEmail::find()->where([
        CampaignEmail::tableName().'.spam_complain'=>1,
        'MONTH('.CampaignEmail::tableName().'.distributed_at)'=>$month,
        'YEAR('.CampaignEmail::tableName().'.distributed_at)'=>$year
      ])
      ->innerJoin(Campaign::tableName(),Campaign::tableName().".id=".CampaignEmail::tableName().".campaign_id")
      ->andFilterWhere(['company_id'=>$companyId])
      ->count('email');
    }
    return [
      'sent'=>$sentData,
      'delivered'=>$deliveredData,
      'bounced'=>$bouncedData,
      'dropped'=>$droppedData,
      'spamComplaint'=>$spamComplaintData,
    ];
  }

  /**
  * get total delivered
  * @return integer
  **/
  public function getTotalDelivered($companyId=null)
  {
    return CampaignEmail::find()
    ->innerJoin(Campaign::tableName(),Campaign::tableName().".id=".CampaignEmail::tableName().".campaign_id")
    ->andFilterWhere([
      CampaignEmail::tableName().'.delivered'=>1,
      Campaign::tableName().'.company_id'=>$companyId
    ])
    ->count('email');
  }

  /**
  * get total bounced
  * @return integer
  **/
  public function getTotalBounced($companyId=null)
  {
    return CampaignEmail::find()
    ->innerJoin(Campaign::tableName(),Campaign::tableName().".id=".CampaignEmail::tableName().".campaign_id")
    ->andFilterWhere([
      Campaign::tableName().'.company_id'=>$companyId,
      CampaignEmail::tableName().'.hard_bounced'=>1
    ])
    ->count('email');
  }

  /**
  * get total dropped
  * @return integer
  **/
  public function getTotalDropped($companyId=null)
  {
    return CampaignEmail::find()
    ->innerJoin(Campaign::tableName(),Campaign::tableName().".id=".CampaignEmail::tableName().".campaign_id")
    ->andFilterWhere([
      Campaign::tableName().'.company_id'=>$companyId,
      CampaignEmail::tableName().'.soft_bounced'=>1
    ])
    ->count('email');
  }

  /**
  * get predefined sub options count
  * @return integer
  **/
  public function getPredefinedListSubOptionCount($id)
  {
    return PredefinedList::find()->where(['parent'=>$id])->count('id');
  }

  /**
  * Get Campaign SMTP List with stats
  *
  * @return array
  */
  public function getCampaignSmtpCount($campaign_id,$smtpIdz)
  {
    return CampaignEmail::find()->where(['campaign_id'=>$campaign_id,'smtp_id'=>$smtpIdz])->count('campaign_id');
  }
}
