<?php
namespace app\components\helpers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\UploadedFile;

/**
 * Default controller
 */
class DefController extends Controller
{
  /**
   * @inheritdoc
   */
  public function actions()
  {
    return [
      'error' => [
        'class' => 'app\components\helpers\MyErrorAction',
        //'class' => 'yii\web\ErrorAction',
      ],
      'captcha' => [
        'class' => 'yii\captcha\CaptchaAction',
        'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
      ],
    ];
  }
  public function beforeAction($action)
  {
    if(
      $action->id == "att-drop-upload" || $action->id == "gjs-drop-upload"
    ){
      $this->enableCsrfValidation = false;
    }
    return parent::beforeAction($action);
  }

  /**
   * Displays homepage.
   *
   * @return string
   */
  public function actionIndex()
  {
    return $this->render('index');
  }

	public function goHome()
	{
		return $this->redirect(['index']);
	}

	public function checkLogin()
	{
		if (\Yii::$app->user->isGuest) {
      header("location:".Url::to(['site/login']));
      die();
      exit;
    }
	}

	public function checkAdmin()
	{
		if (\Yii::$app->user->isGuest) {
      header("location:".Url::to(['site/login'])."");
      exit;
    }
		if(!Yii::$app->user->isGuest && in_array(Yii::$app->user->identity->user_type,[0,1])){
      header("location:".Url::to(['site/index'])."");
      exit;
		}
	}

	public function checkSuperAdmin()
	{
		if (Yii::$app->user->isGuest) {
      header("location:".Url::to(['site/login'])."");
      exit;
    }
		if(!Yii::$app->user->isGuest && in_array(Yii::$app->user->identity->user_type,[0,1])){
      header("location:".Url::to(['site/index'])."");
      exit;
		}
	}

  /**
  * Creates a new model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkAdmin();
    $model = $this->newModel();
    $model->status=1;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
          Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
          return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
   * Displays a single model.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionView($id)
  {
    $this->checkLogin();
      return $this->render('view', [
          'model' => $this->findModel($id),
      ]);
  }

  /**
   * Updates an existing model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionUpdate($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
        return $this->redirect(['index']);
      }else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								Yii::$app->getSession()->addFlash('error', $val);
							}
						}
					}
				}
      }
    }

    return $this->render('update', [
        'model' => $model,
    ]);
  }

  /**
  * Enable/Disable an existing model.
  * If action is successful, the browser will be redirected to the 'index' page.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionStatus($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);
    $model->updateStatus();
    Yii::$app->getSession()->addFlash('success', Yii::t('app','Record updated successfully'));
    return $this->redirect(['index']);
  }

  /**
   * Deletes an existing model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionDelete($id)
  {
    $this->checkLogin();
    $this->findModel($id)->softDelete();
    Yii::$app->getSession()->addFlash('success', Yii::t('app','Record deleted successfully'));
    return $this->redirect(['index']);
  }

  /**
   * Displays Not allowed page.
   *
   * @return string
   */
  public function actionNotAllowed()
  {
    return $this->render('/site/not_allowed');
  }

  /**
   * Flushes the cache
   */
  public function actionFlushCache()
  {
    if(!Yii::$app->user->isGuest){
      Yii::$app->cache->flush();
      echo 'Done';
      exit;
    }
  }

  /**
   * Drop Upload Action
   */
  public function actionAttDropUpload()
  {
    $allowedFileTypes=[
      'jpg','JPG','jpeg','JPEG','png','PNG','gif','GIF','doc','DOC','docx','DOCX',
      'xls','XLS','xlsx','XLSX','pdf','PDF'
    ];

    $fileName = 'file';
    $uploadPath = Yii::$app->fileHelperFunctions->tempImagePath['abs'];

    if (isset($_FILES[$fileName])) {
      $file = UploadedFile::getInstanceByName($fileName);
      if (!empty($file)) {
        $pInfo=pathinfo($file->name);
        $ext = $pInfo['extension'];
        if (in_array($ext,$allowedFileTypes)) {
          // Check to see if any PHP files are trying to be uploaded
          $content = file_get_contents($file->tempName);
          if (preg_match('/\<\?php/i', $content)) {
          }else{
            if ($file->saveAs(Yii::$app->fileHelperFunctions->tempImagePath['abs'].$file->name)) {
              echo $file->name;
            }
          }
        }
      }
    }
    exit;
  }

  /**
  * Drop Upload Action
  */
  public function actionGjsDropUpload()
  {
    $allowedFileTypes=[
      'jpg','JPG','jpeg','JPEG','png','PNG','gif','GIF','doc','DOC','docx','DOCX',
      'xls','XLS','xlsx','XLSX','pdf','PDF'
    ];

    if($_FILES)
    {
      $resultArray = array();
      $uploadPath = Yii::$app->fileHelperFunctions->gjsAssetPath['abs'];
      $i=0;
      if(!file_exists(Yii::$app->fileHelperFunctions->gjsAssetPath['abs'].Yii::$app->user->identity->company_id)) {
        mkdir(Yii::$app->fileHelperFunctions->gjsAssetPath['abs'].Yii::$app->user->identity->company_id, 0777, true);
      }
      foreach ( $_FILES as $ufile){
        $fileName = 'file-'.$i;
        if (isset($_FILES[$fileName])) {
          $file = UploadedFile::getInstanceByName($fileName);
          if (!empty($file)) {
            $pInfo=pathinfo($file->name);
            $ext = $pInfo['extension'];
            if (in_array($ext,$allowedFileTypes)) {
              // Check to see if any PHP files are trying to be uploaded
              $content = file_get_contents($file->tempName);
              if (preg_match('/\<\?php/i', $content)) {
              }else{
                if ($file->saveAs(Yii::$app->fileHelperFunctions->gjsAssetPath['abs'].Yii::$app->user->identity->company_id.'/'.$file->name)) {
                  $result=[
                   'name'=>$file->name,
                   'type'=>'image',
                   'src'=>Yii::$app->fileHelperFunctions->gjsAssetPath['rel'].Yii::$app->user->identity->company_id.'/'.$file->name,
                   'height'=>350,
                   'width'=>250
                  ];
                  array_push($resultArray,$result);
                }
              }
            }
          }
        }
        $i++;
      }
      $response = array( 'data' => $resultArray );
      echo json_encode($response);
    }
    exit;
  }
}
