<?php
require_once( __DIR__ . '/../../../vendor/tcpdf/tcpdf.php');

class MyPDF extends TCPDF
{
  //Page header
  public function Header() {
    // get the current page break margin
    $bMargin = $this->getBreakMargin();
    // get current auto-page-break mode
    $auto_page_break = $this->AutoPageBreak;
    // disable auto-page-break
    $this->SetAutoPageBreak(false, 0);
    // set bacground image

    if($this->CurOrientation=='L'){
      $img_file = K_PATH_IMAGES.'pdf_background_l.jpg';
      $this->Image($img_file, 0, 0, 297, 210, '', '', '', false, 300, '', false, false, 0);
    }else{
      $img_file = K_PATH_IMAGES.'pdf_background_p.jpg';
      $this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
    }


    // restore auto-page-break status
    $this->SetAutoPageBreak($auto_page_break, $bMargin);
    // set the starting point for the page content
    $this->setPageMark();
  }

  // Page footer
  public function Footer() {
  }
}

?>
