<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $apiModel app\models\ApiSource */
/* @var $model app\models\ApiMapForm */

$this->title = Yii::t('app', 'Api Sources');
$cardTitle = Yii::t('app', 'Map Fields');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $apiModel->title, 'url' => ['update','id'=>$apiModel->id]];
$this->params['breadcrumbs'][] = $cardTitle;

$apiCols = [];
if($curlResponse!=null && $curlResponse->items!=null){
  $apiItem = $curlResponse->items[0];
  foreach($apiItem as $key=>$val){
    $apiCols[$key]=$key;
  }
}
$model->target_column['first_name'] = Yii::$app->inputHelperFunctions->getApiMapColumn($apiModel->id,'first_name');
$model->target_column['last_name'] = Yii::$app->inputHelperFunctions->getApiMapColumn($apiModel->id,'last_name');
$model->target_column['email'] = Yii::$app->inputHelperFunctions->getApiMapColumn($apiModel->id,'email');
$model->target_column['status'] = Yii::$app->inputHelperFunctions->getApiMapColumn($apiModel->id,'status');
?>
<style>
td .form-group{margin-bottom:0;}
</style>
<div class="api-source-mapping">
  <section class="api-source-mapping-form card card-outline card-primary">
    <?php $form = ActiveForm::begin(['id'=>'create-email-template-form']); ?>
    <header class="card-header">
      <h2 class="card-title"><?= Yii::t('app','{modelName}: Map Fields',['modelName'=>$apiModel->title])?></h2>
    </header>
    <div class="card-body">
      <table class="table table-striped">
        <tr>
          <th>System Column</th>
          <th>API Column</th>
        </tr>
        <tr>
          <td>First Name</td>
          <td><?= $form->field($model, 'target_column[first_name]')->dropDownList($apiCols,['prompt'=>Yii::t('app','Select')])->label(false) ?></td>
        </tr>
        <tr>
          <td>Last Name</td>
          <td><?= $form->field($model, 'target_column[last_name]')->dropDownList($apiCols,['prompt'=>Yii::t('app','Select')])->label(false) ?></td>
        </tr>
        <tr>
          <td>Email</td>
          <td><?= $form->field($model, 'target_column[email]')->dropDownList($apiCols,['prompt'=>Yii::t('app','Select')])->label(false) ?></td>
        </tr>
        <?php
        $inputFields=Yii::$app->inputHelperFunctions->getInputTypesByModule('prospect');
        if($inputFields!=null){
          foreach($inputFields as $inputField){
            $model->target_column[$inputField['id']] = Yii::$app->inputHelperFunctions->getApiMapColumn($apiModel->id,$inputField['id']);
        ?>
        <tr>
          <td><?= $inputField['title']?></td>
          <td><?= $form->field($model, 'target_column['.$inputField['id'].']')->dropDownList($apiCols,['prompt'=>Yii::t('app','Select')])->label(false) ?></td>
        </tr>
        <?php
          }
        }
        ?>
        <tr>
          <td>Status</td>
          <td><?= $form->field($model, 'target_column[status]')->dropDownList($apiCols,['prompt'=>Yii::t('app','Select')])->label(false) ?></td>
        </tr>
      </table>
    </div>
    <div class="card-footer">
      <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
      <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>
    <?php ActiveForm::end(); ?>
  </section>
</div>
