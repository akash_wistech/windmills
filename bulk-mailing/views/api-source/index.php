<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ApiSourceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$company_id = Yii::$app->user->identity->company_id;

$this->title = Yii::t('app', 'Api Sources');
$cardTitle = Yii::t('app', 'Api Sources');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns='';
$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
  $createBtn=true;
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
  $actionBtns.='{update}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){
  $actionBtns.='{status}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('mapping')){
  $actionBtns.='{mapping}';
}
$syncFrequencyArr = Yii::$app->helperFunctions->syncFrequencyArr;
?>
<div class="api-source-index">
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <?= CustomGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'cardTitle' => $cardTitle,
    'createBtn' => $createBtn,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']],
      'title',
      'url',
      ['format'=>'raw','attribute'=>'sync_frequency','value'=>function($model) use ($syncFrequencyArr){
        $strFreqValue = $syncFrequencyArr[$model['sync_frequency']];
        $strFreqValue.=Yii::$app->appHelperFunctions->getSyncProgress($model);
        return $strFreqValue;
      },'filter'=>Yii::$app->helperFunctions->syncFrequencyArr],
      ['format'=>'raw','attribute'=>'status','label'=>Yii::t('app','Status'),'value'=>function($model){
        return Yii::$app->helperFunctions->arrStatusIcon[$model['status']];
      },'headerOptions'=>['class'=>'noprint','style'=>'width:100px;'],'filter'=>Yii::$app->helperFunctions->arrFilterStatus],
      [
        'class' => 'yii\grid\ActionColumn',
        'header'=>'',
        'headerOptions'=>['class'=>'noprint','style'=>'width:50px;'],
        'contentOptions'=>['class'=>'noprint actions'],
        'template' => '
          <div class="btn-group flex-wrap">
  					<button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
              <span class="caret"></span>
            </button>
  					<div class="dropdown-menu" role="menu">
              '.$actionBtns.'
  					</div>
  				</div>',
        'buttons' => [
            'update' => function ($url, $model) {
              return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                'title' => Yii::t('app', 'Edit'),
                'class'=>'dropdown-item text-1',
                'data-pjax'=>"0",
              ]);
            },
            'status' => function ($url, $model) {
              if($model['status']==1){
                return Html::a('<i class="fas fa-eye-slash"></i> '.Yii::t('app', 'Disable'), $url, [
                  'title' => Yii::t('app', 'Disable'),
                  'class'=>'dropdown-item text-1',
                  'data-confirm'=>Yii::t('app','Are you sure you want to disable this item?'),
                  'data-method'=>"post",
                ]);
              }else{
                return Html::a('<i class="fas fa-eye"></i> '.Yii::t('app', 'Enable'), $url, [
                  'title' => Yii::t('app', 'Enable'),
                  'class'=>'dropdown-item text-1',
                  'data-confirm'=>Yii::t('app','Are you sure you want to enable this item?'),
                  'data-method'=>"post",
                ]);
              }
            },
            'mapping' => function ($url, $model) {
              return Html::a('<i class="fas fa-map-signs"></i> '.Yii::t('app', 'Map Fields'), $url, [
                'title' => Yii::t('app', 'Map Fields'),
                'class'=>'dropdown-item text-1',
                'data-pjax'=>"0",
              ]);
            },
            'delete' => function ($url, $model) {
              return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                'title' => Yii::t('app', 'Delete'),
                'class'=>'dropdown-item text-1',
                'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                'data-method'=>"post",
                'data-pjax'=>"0",
              ]);
            },
        ],
      ],
    ],
  ]);?>
  <?php CustomPjax::end(); ?>
</div>
