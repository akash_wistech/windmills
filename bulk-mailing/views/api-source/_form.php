<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApiSourceDetails */
/* @var $form yii\widgets\ActiveForm */

$company_id = Yii::$app->user->identity->company_id;
?>
<section class="api-source-form card card-outline card-primary">
  <?php $form = ActiveForm::begin(['id'=>'create-email-template-form']); ?>
  <header class="card-header">
    <h2 class="card-title"><?= $cardTitle?></h2>
  </header>
  <div class="card-body">
    <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
    <?= $form->field($model, 'url')->textInput(['maxlength' => true])?>
    <?= $form->field($model, 'sync_frequency')->dropDownList(Yii::$app->helperFunctions->syncFrequencyArr)?>
  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>
