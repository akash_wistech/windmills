<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmailSmtpDetails */

$this->title = Yii::t('app', 'Api Sources');
$cardTitle = Yii::t('app','Update Api Source:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="api-source-update">
    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
