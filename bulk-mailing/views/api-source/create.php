<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ApiSourceDetails */

$this->title = Yii::t('app', 'Api Sources');
$cardTitle = Yii::t('app','New Api Source');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="api-source-create">
    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
