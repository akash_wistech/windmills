<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\assets\StaffFormAsset;
StaffFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Staff */
/* @var $form yii\widgets\ActiveForm */

if($model->profileInfo!=null){
  $model->job_title_id=$model->profileInfo->job_title_id;
  $model->job_title=$model->profileInfo->jobTitle!=null ? $model->profileInfo->jobTitle->title : '';
  $model->department_id=$model->profileInfo->department_id;
  $model->department_name=$model->profileInfo->department!=null ? $model->profileInfo->department->title : '';
  $model->mobile=$model->profileInfo->mobile;
  $model->phone1=$model->profileInfo->phone1;
  $model->phone2=$model->profileInfo->phone2;
  $model->address=$model->profileInfo->address;
}
if($model->company!=null){
  $model->company_name=$model->company!=null ? $model->company->title : '';
}

$this->registerJs('
$("#customer-company_name").autocomplete({
  serviceUrl: "'.Url::to(['suggestion/company']).'",
  noCache: true,
  onSelect: function(suggestion) {
    if($("#customer-company_id").val()!=suggestion.data){
      $("#customer-company_id").val(suggestion.data);
    }
  },
  onInvalidateSelection: function() {
    $("#customer-company_id").val("0");
  }
});
$("#customer-job_title").autocomplete({
  serviceUrl: "'.Url::to(['suggestion/job-titles']).'",
  noCache: true,
  onSelect: function(suggestion) {
    if($("#customer-job_title_id").val()!=suggestion.data){
      $("#customer-job_title_id").val(suggestion.data);
    }
  },
  onInvalidateSelection: function() {
    $("#customer-job_title_id").val("0");
  }
});
$("#customer-department_name").autocomplete({
  serviceUrl: "'.Url::to(['suggestion/departments']).'",
  noCache: true,
  onSelect: function(suggestion) {
    if($("#customer-department_id").val()!=suggestion.data){
      $("#customer-department_id").val(suggestion.data);
    }
  },
  onInvalidateSelection: function() {
    $("#customer-department_id").val("0");
  }
});
');
?>
<section class="customer-form card card-outline card-primary">
  <?php $form = ActiveForm::begin(); ?>
  <div class="hidden">
    <?= $form->field($model, 'company_id')->textInput()?>
    <?= $form->field($model, 'job_title_id')->textInput()?>
    <?= $form->field($model, 'department_id')->textInput()?>
  </div>
  <header class="card-header">
    <h2 class="card-title"><?= $cardTitle?></h2>
  </header>
  <div class="card-body">
    <div class="row">
      <div class="col-sm-6">
        <?= $form->field($model, 'firstname')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-6">
        <?= $form->field($model, 'lastname')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <?= $form->field($model, 'email')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-6">
        <?= $form->field($model, 'new_password')->passwordInput(['maxlength' => true])?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <?= $form->field($model, 'company_name')->textInput(['maxlength' => true, 'autocomplete' => 'off'])?>
      </div>
      <div class="col-sm-6">
        <?= $form->field($model, 'job_title')->textInput(['maxlength' => true, 'autocomplete' => 'off'])?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <?= $form->field($model, 'department_name')->textInput(['maxlength' => true, 'autocomplete' => 'off'])?>
      </div>
      <div class="col-sm-6">
        <?= $form->field($model, 'mobile')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6">
        <?= $form->field($model, 'phone1')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-6">
        <?= $form->field($model, 'phone2')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <?= $form->field($model, 'address')->textArea(['rows' => 4])?>
  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>
