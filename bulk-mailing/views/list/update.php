<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Lists */

$this->title = Yii::t('app', 'Lists');
$cardTitle = Yii::t('app','Update List:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="customer-update">
    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
