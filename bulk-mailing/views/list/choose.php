<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

/* @var $this yii\web\View */
/* @var $model app\models\List */
/* @var $form yii\widgets\ActiveForm */

$addedLists=Yii::$app->appHelperFunctions->getListsArr(Yii::$app->user->identity->company_id);
?>

<div class="list-form">
    <?php $form = ActiveForm::begin(['id'=>'create-list-form']); ?>
    <div id="lhitms" style="display:none;">
      <?= $form->field($model, 'module_type')->textInput(['id'=>'sel-module-type','maxlength' => true]) ?>
      <?= $form->field($model, 'module_ids')->textInput(['id'=>'sel-module-ids','maxlength' => true]) ?>
    </div>
    <?= $form->field($model, 'list_id')->dropDownList($addedLists,['prompt'=>Yii::t('app','Select'),'maxlength' => true]) ?>
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
