<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Lists */
/* @var $form yii\widgets\ActiveForm */
?>
<section class="lists-form card card-outline card-primary">
  <?php $form = ActiveForm::begin(['id'=>'create-list-form']); ?>
  <div id="lhitms" style="display:none;">
    <?= $form->field($model, 'module_type')->textInput(['id'=>'sel-module-type','maxlength' => true]) ?>
    <?= $form->field($model, 'module_ids')->textInput(['id'=>'sel-module-ids','maxlength' => true]) ?>
  </div>
  <?php if(isset($cardTitle)){?>
  <header class="card-header">
    <h2 class="card-title"><?= $cardTitle?></h2>
  </header>
  <?php }?>
  <div class="card-body">
    <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>
