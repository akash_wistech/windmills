<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Lists */

$this->title = Yii::t('app', 'Lists');
$cardTitle = Yii::t('app','New List');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="customer-create">
    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
