<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error card card-danger">
  <div class="card-header">
    <h3 class="card-title"><?= nl2br(Html::encode($message)) ?></h3>
  </div>
  <div class="card-body">
    <p>
        <?= Yii::t('app','The above error occurred while the Web server was processing your request.')?>
    </p>
    <p>
        <?= Yii::t('app','Please contact us if you think this is a server error. Thank you.')?>
    </p>
  </div>
</div>
