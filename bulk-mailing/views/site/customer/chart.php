<?php
use app\widgets\LineChartWidget;
use app\widgets\PieChartWidget;
$companyId=Yii::$app->user->identity->company_id;
$lastSixMonths=Yii::$app->helperFunctions->lastSixMonths;
$lastSixMonthsEmailsStats=Yii::$app->statsFunctions->getLastSixMonthEmailsStats($lastSixMonths,$companyId);
$emailsSentData=[
  'labels' => array_values($lastSixMonths),
  'datasets' => [
    [
      'label'=>"Sent",
      'backgroundColor'=>"rgba(60,141,188,0.9)",
      'data'=>$lastSixMonthsEmailsStats['sent']
    ],
    [
      'label'=>"Delivered",
      'backgroundColor' =>"rgba(34,139,34)",
      'data'=>$lastSixMonthsEmailsStats['delivered']
    ],
    [
      'label'=>"Hard Bounced",
      'backgroundColor' =>"rgba(255,0,0,1)",
      'data'=>$lastSixMonthsEmailsStats['bounced']
    ],
    [
      'label'=>"Soft Bounced",
      'backgroundColor' =>"rgba(255,165,0,1)",
      'data'=>$lastSixMonthsEmailsStats['dropped']
    ],
  ],
];

$overallEmailStats=[
  'labels' => [
    Yii::t('app','Delivered'),
    Yii::t('app','Hard Bounced'),
    Yii::t('app','Soft Bounced'),
  ],
  'datasets' => [[
    'data'=>[
      Yii::$app->statsFunctions->getTotalDelivered($companyId),
      Yii::$app->statsFunctions->getTotalBounced($companyId),
      Yii::$app->statsFunctions->getTotalDropped($companyId)
    ],
    'backgroundColor'=>['#28a745', '#dc3545', '#ffc107'],
  ]]
];
?>
<div class="row">
  <div class="col-12 col-sm-6">
    <?= LineChartWidget::widget(['heading'=>Yii::t('app','Emails Sent'),'type'=>'bar','data'=>$emailsSentData,'className'=>'info'])?>
  </div>
  <div class="col-12 col-sm-6">
    <?= PieChartWidget::widget(['heading'=>Yii::t('app','Overall Stats'),'data'=>$overallEmailStats,'className'=>'info'])?>
  </div>
</div>
