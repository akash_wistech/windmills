<?php
use yii\helpers\Url;

$rawDataCount = Yii::$app->statsFunctions->getRawDataCount($loggedInUser->company_id);
$listsCount = Yii::$app->statsFunctions->getListsCount($loggedInUser->company_id);
$campaignsCount = Yii::$app->statsFunctions->getCampaignsCount($loggedInUser->company_id);
?>
<div class="row">
  <div class="col-lg-4 col-6">
    <div class="small-box bg-warning">
      <div class="inner">
        <h3><?= $rawDataCount?></h3>
        <p><?= Yii::t('app','All data')?></p>
      </div>
      <div class="icon">
        <i class="ion ion-person-add"></i>
      </div>
      <a href="<?= Url::to(['prospect/index'])?>" class="small-box-footer">
        <?= Yii::t('app','view all')?> <i class="fas fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <div class="col-lg-4 col-6">
    <div class="small-box bg-info">
      <div class="inner">
        <h3><?= $listsCount?></h3>
        <p><?= Yii::t('app','Lists')?></p>
      </div>
      <div class="icon">
        <i class="ion ion-ios-list-outline"></i>
      </div>
      <a href="<?= Url::to(['list/index'])?>" class="small-box-footer">
        <?= Yii::t('app','view all')?> <i class="fas fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <div class="col-lg-4 col-6">
    <div class="small-box bg-success">
      <div class="inner">
        <h3><?= $campaignsCount?></h3>
        <p><?= Yii::t('app','Campaigns')?></p>
      </div>
      <div class="icon">
        <i class="ion ion-email"></i>
      </div>
      <a href="<?= Url::to(['campaign/index'])?>" class="small-box-footer">
        <?= Yii::t('app','view all')?> <i class="fas fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
</div>
