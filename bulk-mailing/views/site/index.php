<?php
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = Yii::t('app','Dashboard');
$loggedInUser = Yii::$app->user->identity;
?>
<div class="container-fluid">
  <?php if($loggedInUser->user_type==0){?>
  <?= $this->render('customer/info_blocks',[
    'loggedInUser'=>$loggedInUser,
  ])?>
  <?= $this->render('customer/chart',[
    'loggedInUser'=>$loggedInUser,
  ])?>
  <?php }else{?>
  <?= $this->render('admin/info_blocks',[
    'loggedInUser'=>$loggedInUser,
  ])?>
  <?= $this->render('admin/chart',[
    'loggedInUser'=>$loggedInUser,
  ])?>
  <?php }?>
</div>
