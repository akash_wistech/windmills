<?php
use yii\helpers\Url;

$rawDataCount = Yii::$app->statsFunctions->rawDataCount;
$listsCount = Yii::$app->statsFunctions->listsCount;
$campaignsCount = Yii::$app->statsFunctions->campaignsCount;
$templatesCount = Yii::$app->statsFunctions->templatesCount;

$hardBouncedCount = Yii::$app->statsFunctions->hardBouncedCount;
$softBouncedCount = Yii::$app->statsFunctions->softBouncedCount;
$customerCount = Yii::$app->statsFunctions->customerCount;
$pdListsCount = Yii::$app->statsFunctions->pdListsCount;
?>
<div class="row">
  <div class="col-lg-3 col-6">
    <div class="info-box">
      <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cubes"></i></span>
      <div class="info-box-content">
        <span class="info-box-text"><?= Yii::t('app','All data')?></span>
        <span class="info-box-number">
          <?= $rawDataCount?>
        </span>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-6">
    <div class="info-box">
      <span class="info-box-icon bg-lightblue elevation-1"><i class="fas fa-list"></i></span>
      <div class="info-box-content">
        <span class="info-box-text"><?= Yii::t('app','Lists')?></span>
        <span class="info-box-number">
          <?= $listsCount?>
        </span>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-6">
    <div class="info-box">
      <span class="info-box-icon bg-teal elevation-1"><i class="fas fa-bullhorn"></i></span>
      <div class="info-box-content">
        <span class="info-box-text"><?= Yii::t('app','Campaigns')?></span>
        <span class="info-box-number">
          <?= $campaignsCount?>
        </span>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-6">
    <div class="info-box">
      <span class="info-box-icon bg-primary elevation-1"><i class="fas fa-file-image"></i></span>
      <div class="info-box-content">
        <span class="info-box-text"><?= Yii::t('app','Templates')?></span>
        <span class="info-box-number">
          <?= $templatesCount?>
        </span>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-3 col-6">
    <div class="info-box">
      <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-ban"></i></span>
      <div class="info-box-content">
        <span class="info-box-text"><?= Yii::t('app','Hard Bounced')?></span>
        <span class="info-box-number">
          <?= $hardBouncedCount?>
        </span>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-6">
    <div class="info-box">
      <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-envelope"></i></span>
      <div class="info-box-content">
        <span class="info-box-text"><?= Yii::t('app','Soft Bounced')?></span>
        <span class="info-box-number">
          <?= $softBouncedCount?>
        </span>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-6">
    <div class="info-box">
      <span class="info-box-icon bg-success elevation-1"><i class="fas fa-users"></i></span>
      <div class="info-box-content">
        <span class="info-box-text"><?= Yii::t('app','Customers')?></span>
        <span class="info-box-number">
          <?= $customerCount?>
        </span>
      </div>
    </div>
  </div>
  <div class="col-lg-3 col-6">
    <div class="info-box">
      <span class="info-box-icon bg-purple elevation-1"><i class="fas fa-window-restore"></i></span>
      <div class="info-box-content">
        <span class="info-box-text"><?= Yii::t('app','Predefined Lists')?></span>
        <span class="info-box-number">
          <?= $pdListsCount?>
        </span>
      </div>
    </div>
  </div>
</div>
