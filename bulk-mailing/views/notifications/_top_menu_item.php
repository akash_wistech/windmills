<?php
use yii\helpers\Html;
use yii\helpers\Url;
$createdAt=Yii::$app->helperFunctions->getDateTimeWithDiff($model->created_at);
?>
<div class="dropdown-divider"></div>
<a href="<?= Url::to([$model->category.'/view','id'=>$model->parent_id])?>" class="dropdown-item text-truncate">
  <img src="images/icons/<?= $model->action_type?>.png" width="20" height="20" /> <?= $model->infoTitle?>
  <!--span class="float-right text-muted text-sm"><?= Yii::$app->helperFunctions->convertTime($createdAt)?></span-->
</a>
<?= '';//Yii::$app->notificationHelper->getDetail($model)?>
