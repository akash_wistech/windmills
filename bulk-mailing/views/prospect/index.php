<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\DataTableGridView;
use app\components\widgets\CustomPjax;
use app\widgets\AdvanceSearchWidget;
use app\assets\ProspectListAsset;
ProspectListAsset::register($this);

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProspectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'All data');
$cardTitle = Yii::t('app', '');
$this->params['breadcrumbs'][] = $this->title;

$actionBtns='';
$createBtn=false;
if(Yii::$app->menuHelperFunction->checkActionAllowed('create')){
  $createBtn=true;
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
  //$actionBtns.='{view}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
  $actionBtns.='{update}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){
  $actionBtns.='{status}';
}
$this->registerJs('
initScripts();
$(document).on("pjax:success", function() {
  initScripts();
});
');
//$columns[]=['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']];
$dtColumns['id']='';
$dtColumns['cb_col']='';
$dtColumns['cp_name']=Yii::t('app','Name');
$dtColumns['email']=Yii::t('app','Email');
$columns[]=[
  'format'=>'raw',
  'header'=>'<input type="checkbox" class="select-on-check-all" name="selection_all" value="1">',
  'value'=>function($model){
    return '<input type="checkbox" name="selection[]" value="'.$model['id'].'">';
  }
];
$columns[]=['format'=>'raw','attribute'=>'cp_name','value'=>function($model){
  //if(Yii::$app->menuHelperFunction->checkActionAllowed('view')){
  //  return Html::a($model['cp_name'],['view','id'=>$model['id']],['data-pjax'=>'0']);
  //}else{
    return $model['cp_name'];
  //}
}];
$columns[]='email';

$gridViewColumns=Yii::$app->inputHelperFunctions->getGridViewColumns($searchModel->moduleTypeId);
if($gridViewColumns!=null){
  foreach($gridViewColumns as $gridViewColumn){
    $colLabel=($gridViewColumn['short_name']!=null && $gridViewColumn['short_name']!='' ? $gridViewColumn['short_name'] : $gridViewColumn['title']);
    $columns[]=['format'=>'html','label'=>$colLabel,'value'=>function($model) use ($gridViewColumn){
      return Yii::$app->inputHelperFunctions->getGridValue('prospect',$model,$gridViewColumn);
    },'filter'=>Yii::$app->inputHelperFunctions->getGridViewFilters($gridViewColumn)];
    $dtColumns['col_'.$gridViewColumn['id']]=$gridViewColumn['title'];
  }
}
// $dtColumns['tag']=Yii::t('app','Tags');
$dtColumns['status']=Yii::t('app','Status');
$dtColumns['created_at']=Yii::t('app','Created');
$dtColumns['action_col']=Yii::t('app','Actions');
$columns[]=[
  'format'=>'raw',
  'attribute'=>'status',
  'label'=>Yii::t('app', 'Status'),
  'value'=>function($model){
    return Yii::$app->helperFunctions->arrStatusIcon[$model->status];
  }
];
$columns[]=[
  'format'=>'raw',
  'attribute'=>'tag',
  'label'=>Yii::t('app','Tags'),
  'value'=>function($model){
    $tagsHtml = Yii::$app->appHelperFunctions->getModuleTagsHtml('prospect',$model['id']);
    return $tagsHtml;
  }
];
$columns[]=[
  'attribute'=>'created_at',
  'label'=>Yii::t('app', 'Created'),
  'format'=>'datetime',
  'filterInputOptions'=>['class'=>'form-control dtrpicker','autocomplete'=>'off']
];
$columns[]=[
  'class' => 'yii\grid\ActionColumn',
  'header'=>Yii::t('app','Action'),
  'headerOptions'=>['class'=>'noprint noVis','style'=>'width:50px;'],
  'contentOptions'=>['class'=>'noprint actions'],
  'template' => '
    <div class="btn-group flex-wrap">
      <button type="button" class="mb-1 mt-1 mr-1 btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
        <span class="caret"></span>
      </button>
      <div class="dropdown-menu" role="menu">
        '.$actionBtns.'
      </div>
    </div>',
  'buttons' => [
      'view' => function ($url, $model) {
        return Html::a('<i class="fas fa-table"></i> '.Yii::t('app', 'View'), $url, [
          'title' => Yii::t('app', 'View'),
          'class'=>'dropdown-item text-1',
          'data-pjax'=>"0",
        ]);
      },
      'update' => function ($url, $model) {
        return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
          'title' => Yii::t('app', 'Edit'),
          'class'=>'dropdown-item text-1',
          'data-pjax'=>"0",
        ]);
      },
      'delete' => function ($url, $model) {
        return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
          'title' => Yii::t('app', 'Delete'),
          'class'=>'dropdown-item text-1',
          'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
          'data-method'=>"post",
          'data-pjax'=>"0",
        ]);
      },
  ],
];
?>
<div class="lead-index">
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <?= DataTableGridView::widget([
    // 'dataProvider' => [],
    'filterModel' => $searchModel,
    'cardTitle' => $cardTitle,
    'advSearch'=>false,
    'cbSelection'=>true,
    'createBtn' => $createBtn,
    'import' => true,
    'columns' => $dtColumns,
    'ajaxUrl' => Url::to(['datatables'])
  ]);?>
  <?php CustomPjax::end(); ?>
</div>
<?= '';//AdvanceSearchWidget::widget(['model'=>$searchModel]);?>
<script>
function initScripts(){
  if($(".dtrpicker").length>0){
    $(".dtrpicker").daterangepicker({autoUpdateInput: false,locale: {format: 'YYYY-MM-DD',cancelLabel: '<?= Yii::t('app','Clear')?>'},});
    $(".dtrpicker").on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('YYYY-MM-DD') + " - " + picker.endDate.format('YYYY-MM-DD'));
      $(this).trigger("change");
    });
    $(".dtrpicker").on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
    });
  }
}
</script>
<?= $this->render('/list/js/scripts')?>
