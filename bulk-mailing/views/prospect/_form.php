<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\widgets\CustomFieldsWidget;
use app\components\widgets\CustomPjax;
use app\assets\ProspectFormAsset;
ProspectFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Prospect */
/* @var $form yii\widgets\ActiveForm */


$model->tags=implode(",",$model->tagsListArray);
$model->manager_id = ArrayHelper::map($model->managerIdz,"staff_id","staff_id");

$this->registerJs('
$(".numeral-input").each(function(index){
  $(this).val(numeral($(this).val()).format("0,0.00"));
});
$("body").on("blur", ".numeral-input", function () {
  $(this).val(numeral($(this).val()).format("0,0.00"));
});
$("#prospect-manager_id").select2({
	allowClear: true,
	width: "100%",
});
$("#prospect-tags").tagsInput({
	"width":"100%",
	"defaultText":"Add tags",
});
');
?>
<section class="prospect-form card card-outline card-primary">
  <?php $form = ActiveForm::begin(); ?>
  <header class="card-header">
    <h2 class="card-title"><?= $cardTitle?></h2>
  </header>
  <div class="card-body">
    <div class="row">
      <div class="col-sm-4">
        <?= $form->field($model, 'firstname')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'lastname')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-4">
        <?= $form->field($model, 'email')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <?= $this->render('/shared/_multi_email_phone_form',['form'=>$form,'model'=>$model])?>

    <div class="row">
      <div class="col-12 col-sm-6">
        <?= $form->field($model, 'status')->dropDownList(Yii::$app->helperFunctions->arrFilterStatus,['prompt'=>Yii::t('app','Select')])?>
      </div>
    </div>
  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>
<?= $this->render('js/form_scripts',['model'=>$model]);?>
