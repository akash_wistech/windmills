<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Prospect */

$this->title = Yii::t('app', 'Raw data');
$cardTitle = Yii::t('app','Update Raw data:  {nameAttribute}', [
    'nameAttribute' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="prospect-update">
    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
