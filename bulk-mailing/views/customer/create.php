<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Customer */

$this->title = Yii::t('app', 'Customers');
$cardTitle = Yii::t('app','New Customer');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="customer-create">
    <?= $this->render('/user/customer_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
