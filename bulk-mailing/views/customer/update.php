<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */

$this->title = Yii::t('app', 'Customers');
$cardTitle = Yii::t('app','Update Customer:  {nameAttribute}', [
    'nameAttribute' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="customer-update">
    <?= $this->render('/user/customer_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
