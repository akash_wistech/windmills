<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\components\widgets\CustomPjax;
use app\assets\CustomFieldFormAsset;
CustomFieldFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\CustomField */
/* @var $form yii\widgets\ActiveForm */
$columnList=[];
for($n=1;$n<=12;$n++){
  $columnList[$n]=$n;
}

$model->for_modules=ArrayHelper::map($model->selectedModules,"module_type","module_type");

$this->registerJs('
$("body").on("change", "#customfield-input_type", function () {
  inputType=$(this).val();
  col1="col-sm-8";
	col2="col-sm-2";
	col3="";
  $("#r1c3").hide();
  $("#r1c4").hide();
  $("#rac").hide();
  $("#rddops").hide();
  $("#subtype").hide();
	if(inputType=="text" || inputType=="select"){
		col1="col-sm-6";
		col2="col-sm-2";
		col3="col-sm-2";
	}
  if(inputType=="select"){
    $("#r1c3").show();
    $("#r1c4").hide();
    $("#rddops").show();
    $("#subtype").show();
  }
  if(inputType=="checkbox" || inputType=="radio"){
    $("#rddops").show();
    $("#subtype").show();
  }
  if(inputType=="text"){
    $("#r1c3").hide();
    $("#r1c4").show();
  }
  if(inputType=="autocomplete"){
    $("#rddops").show();
    $("#predefinedOpts").show();
    $("#customOpts").hide();
    $("#subtype").hide();
  }
  $("#r1").attr("class", "");
  $("#r1").removeClass("").addClass(col1);
});
$(document).delegate(".removeTmpSubOption", "click", function() {
	$(this).closest("tr.subOptRow").remove();
});

	$(document).delegate(".change-field-option-status", "click", function() {
		id=$(this).data("id");
		field_id=$(this).data("field_id");
		stype=$(this).data("stype");
		msgText="You want to enable this?";
		if(stype=="disable"){
			msgText="You want to disable this?";
		}
    swal({
      title: "'.Yii::t('app','Are you sure?').'",
      text: msgText,
      type: "info",
      showCancelButton: true,
      confirmButtonText: "'.Yii::t('app','Confirm').'",
      cancelButtonText: "'.Yii::t('app','Cancel').'"
    }).then((result) => {
      if (result.value) {
        _targetContainer=$("#tbl-"+field_id+"-container-"+id);
        App.blockUI({
          message: "'.Yii::t('app','Please wait...').'",
          target: _targetContainer,
          overlayColor: "none",
          cenrerY: true,
          boxed: true
        });
        $.ajax({
          url: "'.Url::to(['change-field-option-status','media_type'=>$model->id,'field_id'=>'']).'"+field_id+"&id="+id,
          dataType: "html",
          type: "post",
          success: function(data) {
            if(data=="success"){
              toastr.success("'.Yii::t('app','Updated!').'", "'.Yii::t('app','Field updated successfully').'");
              App.unblockUI($(_targetContainer));
              $.pjax.reload({container: "#tbl-"+field_id+"-container-"+id, timeout: 2000});
            }
          },
          error: bbAlert
        });
      }
    });
  });
	$(document).delegate(".remove-input-field-option", "click", function() {
		_this=$(this);
		id=$(this).data("option_id");
		field_id=$(this).data("field_id");
    swal({
      title: "'.Yii::t('app','Are you sure?').'",
      text: "'.Yii::t('app','You will not not be able to revert this.').'",
      type: "info",
      showCancelButton: true,
      confirmButtonText: "'.Yii::t('app','Confirm').'",
      cancelButtonText: "'.Yii::t('app','Cancel').'"
    }).then((result) => {
      if (result.value) {
        _targetContainer=$("#frmFld-"+field_id);
        App.blockUI({
          message: "'.Yii::t('app','Please wait...').'",
          target: _targetContainer,
          overlayColor: "none",
          cenrerY: true,
          boxed: true
        });
        $.ajax({
          url: "'.Url::to(['delete-input-field-option','media_type_id'=>$model->id,'field_id'=>'']).'"+field_id+"&id="+id,
          dataType: "json",
          type: "post",
          success: function(response) {
						if(response["success"]){
							_this.closest(".subOptRow").remove();
							swal({title: response["success"]["heading"], text: response["success"]["msg"], type: "success", timer: 5000});
						}else{
							swal({title: response["error"]["heading"], text: response["error"]["msg"], type: "error", timer: 5000});
						}
						App.unblockUI($(_targetContainer));
          },
          error: bbAlert
        });
      }
    });
  });
');
?>
<style>
#customfield-for_modules label{width: 20%;}
</style>
<section class="custom-field-form card card-outline card-primary">
  <?php $form = ActiveForm::begin(); ?>
  <header class="card-header">
    <h2 class="card-title"><?= $cardTitle?></h2>
  </header>
  <div class="card-body">
    <?= $form->field($model, 'for_modules')->checkboxList(Yii::$app->helperFunctions->moduleTypeArr)?>
    <div class="row">
      <div class="col-sm-10">
        <?= $form->field($model, 'input_type')->dropDownList(Yii::$app->inputHelperFunctions->dropDownList)?>
      </div>
      <div class="col-sm-2">
        <?= $form->field($model, 'sort_order')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <div class="row">
      <div id="r1" class="col-sm-<?= $model->input_type=='select' || $model->input_type=='text' || $model->input_type=='' ? '6' : '8'?>">
        <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-2">
        <?= $form->field($model, 'show_in_grid')->dropDownList(Yii::$app->helperFunctions->arrYesNo)?>
      </div>
      <div class="col-sm-2">
        <?= $form->field($model, 'is_required')->dropDownList(Yii::$app->helperFunctions->arrYesNo)?>
      </div>
      <div id="r1c3" class="col-sm-2"<?= $model->input_type=='select' ? '' : ' style="display:none;"'?>>
        <?= $form->field($model, 'selection_type')->dropDownList(Yii::$app->inputHelperFunctions->selectTypeArr)?>
      </div>
      <div id="r1c4" class="col-sm-2"<?= $model->input_type=='' || $model->input_type=='text' ? '' : ' style="display:none;"'?>>
        <?= $form->field($model, 'autocomplete_text')->dropDownList(Yii::$app->helperFunctions->arrYesNo)?>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-3">
        <?= $form->field($model, 'short_name')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-7">
        <?= $form->field($model, 'hint_text')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-sm-2">
        <?= $form->field($model, 'colum_width')->dropDownList($columnList)?>
      </div>
    </div>
    <?= $form->field($model, 'descp')->textArea(['rows' => 4])?>
    <div id="rddops"<?= $model->input_type=='autocomplete' || $model->input_type=='select' || $model->input_type=='radio' || $model->input_type=='checkbox' ? '' : ' style="display:none;"'?>>
      <div id="subtype" class="form-group"<?= $model->input_type=='autocomplete' ? 'style="display:none;"' : ''?>>
        <label class="control-label" onclick="showSubOptions('p')" for="input-field-sub-opt-selection-up"><input type="radio" id="input-field-sub-opt-selection-up" name="CustomField[suboptselection]" value="usepredefined"<?= $model->id==null || $model->predefined_list_id!=null ? ' checked="checked"' : ''?> /> Use Predefined list</label>&nbsp;&nbsp;&nbsp;
        <label class="control-label" onclick="showSubOptions('c')" for="input-field-sub-opt-selection-uc"><input type="radio" id="input-field-sub-opt-selection-uc" name="CustomField[suboptselection]" value="usecustom"<?= $model->id!=null && $model->predefined_list_id==null ? ' checked="checked"' : ''?> /> Add New Options</label>
      </div>
      <div id="predefinedOpts"<?= $model->id==null || $model->predefined_list_id!=null || $model->input_type=='autocomplete' ? '' : ' style="display:none;"'?>>
        <?= $form->field($model, 'predefined_list_id')->radioList(Yii::$app->appHelperFunctions->predefinedListArr)?>
      </div>
      <div id="customOpts"<?= $model->id!=null && $model['predefined_list_id']==null && $model->input_type!='autocomplete' ? '' : ' style="display:none;"'?>>
        <table id="customOptsTable" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th><?= Yii::t('app','Title')?></th>
            <th width="10" style="text-align: center">
              <a href="javascript:;" class="btn btn-xs btn-success" onclick="javascript:addSubOption();">
                <i class="fa fa-plus"></i>
              </a>
            </th>
          </tr>
          </thead>
          <tbody>
          <?php
          $subOptions=$model->fieldSubOptions;
          if($subOptions!=null){
            foreach($subOptions as $subOption){
              $delUrl=Url::to(['delete-sub-option','field_id'=>$model->id,'id'=>$subOption['id']]);
              $statusUrl=Url::to(['sub-option-status','field_id'=>$model->id,'id'=>$subOption['id']]);
              $statusMsg=$subOption['status']==1 ? Yii::t('app','Are you sure you want to disable this?') : Yii::t('app','Are you sure you want to enable this?');
          ?>
          <tr class="subOptRow">
            <td colspan="2">
              <?php CustomPjax::begin(['id'=>'tbl-container-'.$subOption['id']]); ?>
              <div class="row">
                <div class="col-xs-12 col-sm-11">
                  <input type="text" class="form-control" name="CustomField[suboption_title][]" value="<?= $subOption['title']?>"<?= $subOption['status']==1 ? '' : ' disabled="disabled"'?> />
                </div>
                <div class="col-xs-12 col-sm-1 text-right">
                  <a href="javascript:;" class="btn btn-xs btn-<?= $subOption['status']==1 ? 'success' : 'warning'?> aa-confirm-action" data-url="<?= $statusUrl?>" data-cmsg="<?= $statusMsg?>" data-container="<?= 'tbl-container-'.$subOption['id']?>">
                    <i class="fa fa-<?= $subOption['status']==1 ? 'eye' : 'eye-slash'?>"></i>
                  </a>
                  <a href="javascript:;" class="btn btn-xs btn-danger aa-confirm-action" data-url="<?= $delUrl?>" data-cmsg="<?= Yii::t('app','Are you sure you want to delete this?')?>" data-container="<?= 'tbl-container-'.$subOption['id']?>">
                    <i class="fa fa-times"></i>
                  </a>
                </div>
              </div>
              <?php CustomPjax::end(); ?>
            </td>
          </tr>
          <?php }}?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>
<script>

function showSubOptions(type)
{
	if(type=='p'){
		$("#rddops").find("#predefinedOpts").show();
		$("#rddops").find("#customOpts").hide();
	}
	if(type=='c'){
		$("#rddops").find("#predefinedOpts").hide();
		$("#rddops").find("#customOpts").show();

	}
}
function addSubOption()
{
	html = '';
	html+= '<tr class="subOptRow">';
	html+= '	<td>';
	html+= '		<input type="text" class="form-control" name="CustomField[suboption_title][]" />';
	html+= '	</td>';
	html+= '	<td><a href="javascript:;" class="btn btn-xs btn-danger removeTmpSubOption"><i class="fa fa-times"></a></td>';
	html+= '</tr>';
	$("#customOptsTable tbody").append(html);
}
</script>
