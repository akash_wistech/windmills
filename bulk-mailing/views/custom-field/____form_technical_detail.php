<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\MediaTypeLengthUnit;
use app\models\MediaOutletBrand;
use app\models\Brand;
?>
<?php
$this->registerJs('$("[data-toggle=\"popover\"]").popover();');
$targetAudience=$model->targetAudience;
$mediaTypeFields=$model->mediaType->formFields;
if($mediaTypeFields!=null){
  $txtRules='';
  $txtMessages='';

  $showGoogleMap=$model->mediaType->showGoogleMap;
  $multiFiledropZone=$model->mediaType->multiFiledropZone;
  $multiImagedropZone=$model->mediaType->multiImagedropZone;

  if($showGoogleMap!=null){
    $mCol1='8';
    $mCol2='4';
  }else{
    $mCol1='12';
    $mCol2='';
  }
?>
<div class="row">
<?php
$colsInrow=0;
foreach($mediaTypeFields as $mediaTypeField){
  $colsInrow+=$mediaTypeField->colum_width;
  $fromDb=true;
  if(isset($_REQUEST['MediaOutlet']['input_field'][$mediaTypeField['id']])){
    $fromDb=false;
  }else{
    $savedInfo=Yii::$app->inputHelperFunctions->getInputValue($model,$mediaTypeField);
    $model->input_field[$mediaTypeField['id']]=$savedInfo;
  }

  $hintText=$mediaTypeField['hint_text'];
  $hintHtml='';
  if($hintText!=''){
    $hintHtml=' <a href="javascript:;" class="blue-hint" data-toggle="popover" data-trigger="hover" data-placement="top" title="'.$mediaTypeField['title'].'" data-content="'.$hintText.'"><i class="fa fa-info-circle"></i></a>';
  }
  if(
    $mediaTypeField->input_type=='text' ||
    $mediaTypeField->input_type=='numberinput' ||
    $mediaTypeField->input_type=='websiteinput' ||
    $mediaTypeField->input_type=='emailinput' ||
    $mediaTypeField->input_type=='date' ||
    $mediaTypeField->input_type=='daterange' ||
    $mediaTypeField->input_type=='time'
  ){
    echo '<div class="col-md-'.$mediaTypeField->colum_width.'">';
    echo $form->field($model, 'input_field['.$mediaTypeField['id'].']',['labelOptions'=>['class'=>'form__label']])->textInput(['id'=>'input_field_'.$mediaTypeField['id'],'class'=>'form__input','maxlength' => true, 'autocomplete'=>'off'])->label($mediaTypeField['title'].$hintHtml);
    echo '</div>';

    if($mediaTypeField->input_type=='text'){
      if($mediaTypeField->autocomplete_text==1){
        $this->registerJs('
        $("#input_field_'.$mediaTypeField['id'].'").autocomplete({
          serviceUrl: "'.Url::to(['suggestions/input','mtid'=>$model->media_type_id,'fid'=>$mediaTypeField['id']]).'",
          noCache: true,
          formatResult: function(suggestion, currentValue) {
            return ""+suggestion.value+"";
          }
        });
        ');
      }
    }
    if($mediaTypeField->input_type=='numberinput'){
    }
    if($mediaTypeField->input_type=='websiteinput'){
    }
    if($mediaTypeField->input_type=='emailinput'){
    }
    if($mediaTypeField->input_type=='date'){
      $this->registerJs('$("#input_field_'.$mediaTypeField['id'].'").datepicker({format: "yyyy-mm-dd",}).on("changeDate", function(e) {$(this).datepicker("hide");});');
    }
    if($mediaTypeField->input_type=='daterange'){
      $this->registerJs('
      $("#input_field_'.$mediaTypeField['id'].'").daterangepicker({autoUpdateInput: false,locale: {format: "YYYY-MM-DD",cancelLabel: "'.Yii::t('app','Clear').'"},});
      $("#input_field_'.$mediaTypeField['id'].'").on("apply.daterangepicker", function(ev, picker) {
        $(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
        $(this).trigger("change");
      });
      $("#input_field_'.$mediaTypeField['id'].'").on("cancel.daterangepicker", function(ev, picker) {
        $(this).val("");
      });
      ');
    }
    if($mediaTypeField->input_type=='time'){
      $this->registerJs('$("#input_field_'.$mediaTypeField['id'].'").timepicker({"timeFormat":"h:i A"});');
    }
  }

  if($mediaTypeField->input_type=='timerange'){
    if($fromDb==true){
      $moTimeRange=Yii::$app->inputHelperFunctions->getInputTimeRangeValue($model,$mediaTypeField);
      $model->input_field[$mediaTypeField['id']]['start']=$moTimeRange['start'];
      $model->input_field[$mediaTypeField['id']]['end']=$moTimeRange['end'];
    }
    echo '<div class="col-md-'.$mediaTypeField->colum_width.'">';
    echo '  <div id="timeRange'.$mediaTypeField['id'].'" class="row">';
    echo '    <div class="col-md-6">';
    echo '      '.$form->field($model, 'input_field['.$mediaTypeField['id'].'][start]',['labelOptions'=>['class'=>'form__label']])->textInput(['id'=>'input_field_st_'.$mediaTypeField['id'],'class'=>'form__input time start','maxlength' => true, 'autocomplete'=>'off'])->label('Start Time'.$hintHtml);
    echo '    </div>';
    echo '    <div class="col-md-6">';
    echo '      '.$form->field($model, 'input_field['.$mediaTypeField['id'].'][end]',['labelOptions'=>['class'=>'form__label']])->textInput(['id'=>'input_field_et_'.$mediaTypeField['id'],'class'=>'form__input time end','maxlength' => true, 'autocomplete'=>'off'])->label('End Time');
    echo '    </div>';
    echo '  </div>';
    echo '</div>';

    $this->registerJs('
    $("#input_field_st_'.$mediaTypeField['id'].',#input_field_et_'.$mediaTypeField['id'].'").timepicker({"timeFormat":"h:i A","step":15,"showDuration": true});
    var pairTimeRangeEl'.$mediaTypeField['id'].' = document.getElementById("timeRange'.$mediaTypeField['id'].'");
    var datepair = new Datepair(pairTimeRangeEl'.$mediaTypeField['id'].');
    ');
  }

  if($mediaTypeField->input_type=='autocomplete'){
    if($fromDb==true){
      $moAutoComplete=Yii::$app->inputHelperFunctions->getAutoCompleteValue($model,$mediaTypeField);
      if($moAutoComplete!=null){
        $model->input_field[$mediaTypeField['id']]['id']=$moAutoComplete['id'];
        $model->input_field[$mediaTypeField['id']]['text']=$moAutoComplete['text'];
      }
    }
    echo '<div class="col-md-'.$mediaTypeField->colum_width.'">';
    echo '<div class="hidden" style="display:none;">'.$form->field($model, 'input_field['.$mediaTypeField['id'].'][id]')->textInput(['id'=>'input_field_id_'.$mediaTypeField['id'],'maxlength' => true])->label($mediaTypeField['title']).'</div>';
    echo $form->field($model, 'input_field['.$mediaTypeField['id'].'][text]',['labelOptions'=>['class'=>'form__label']])->textInput(['id'=>'input_field_text_'.$mediaTypeField['id'],'class'=>'form__input','maxlength' => true, 'autocomplete'=>'off'])->label($mediaTypeField['title'].$hintHtml);
    echo '</div>';
    $this->registerJs('
    $("#input_field_text_'.$mediaTypeField['id'].'").autocomplete({
      serviceUrl: "'.Url::to([Yii::$app->inputHelperFunctions->autoCompleteOptions[$mediaTypeField->autocomplete_link]['link']]).'",
      noCache: true,
      formatResult: function(suggestion, currentValue) {
        return ""+suggestion.value+"<br><small style=\"font-size:78%;\">"+suggestion.descp+"</small>";
      },
      onSelect: function(suggestion) {
        if($("#input_field_id_'.$mediaTypeField['id'].'").val()!=suggestion.data){
          $("#input_field_id_'.$mediaTypeField['id'].'").val(suggestion.data);
        }
      },
      onInvalidateSelection: function() {
        $("#input_field_id_'.$mediaTypeField['id'].'").val("");
      }
    });
    ');
  }

  if($mediaTypeField->input_type=='textarea'){
    echo '<div class="col-md-'.$mediaTypeField->colum_width.'">';
    echo $form->field($model, 'input_field['.$mediaTypeField['id'].']',['labelOptions'=>['class'=>'form__label']])->textArea(['id'=>'input_field_'.$mediaTypeField['id'],'class'=>'form-control form__textarea','rows' => 10])->label($mediaTypeField['title'].$hintHtml);
    echo '</div>';
  }

  //Reference Number
  if($mediaTypeField->input_type=='refNumberInput'){
    echo '<div class="col-md-'.$mediaTypeField->colum_width.'">';
    echo $form->field($model, 'reference_number',['labelOptions'=>['class'=>'form__label']])->textInput(['id'=>'input_field_'.$mediaTypeField['id'],'class'=>'form__input','maxlength' => true, 'autocomplete'=>'off'])->label($mediaTypeField['title'].$hintHtml);
    echo '</div>';
  }

  //2 Fields
  if($mediaTypeField->input_type=='dimension'){
    if($fromDb==true){
      $moDimensions=Yii::$app->inputHelperFunctions->getInputDimensionValue($model,$mediaTypeField);
      if($moDimensions!=null){
        $model->width=$moDimensions['width'];
        $model->height=$moDimensions['height'];
        $model->unit_id=$moDimensions['unit_id'];
      }
    }
    echo '<div class="col-md-'.$mediaTypeField->colum_width.'">';
    echo '  '.$form->field($model, 'width',['template'=>'
    {label}
    <div class="box-wrap">
      {input}
      '.Html::textInput('MediaOutlet[height]', $model->height, ['id'=>'input_field_h_'.$mediaTypeField['id'],'class'=>'form__input form__input--h','placeholder'=>'H','maxlength' => true, 'autocomplete'=>'off']).'
      <div class="select-wrap">
        '.Html::dropDownList('MediaOutlet[unit_id]', $model->unit_id, ArrayHelper::map(Yii::$app->helperFunction->getLengthMetricUnitListByMediaTypeAllowed($model->media_type_id),"id","title"),['id'=>'input_field_u_'.$mediaTypeField['id'],'class'=>'form__input form__select']).'
        <i class="fa fa-sort-down"></i>
      </div>
    </div>
    ','labelOptions'=>['class'=>'form__label']])->textInput(['id'=>'input_field_w_'.$mediaTypeField['id'],'class'=>'form__input form__input--w','placeholder'=>'W','maxlength' => true, 'autocomplete'=>'off'])->label($mediaTypeField['title'].$hintHtml.' <span>(Width x Height)</span>');
    echo '</div>';
  }

  //2 Fields
  if($mediaTypeField->input_type=='viewership'){
    if($fromDb==true){
      if($targetAudience!=null){
        if($targetAudience['viewership']>0){
          $model->viewership=Yii::$app->formatter->asInteger($targetAudience['viewership']);
        }else{
          $model->viewership=0;
        }
      }
    }else{
      $model->viewership=0;
    }
    echo '<div class="col-md-'.$mediaTypeField->colum_width.'">';
    echo '  '.$form->field($model, 'viewership',['labelOptions'=>['class'=>'form__label']])->textInput(['id'=>'viewership','class'=>'form__input numeric-field-noz','maxlength' => true, 'autocomplete'=>'off'])->label($mediaTypeField['title'].$hintHtml);
    echo '</div>';
  }

  //Brightness
  if($mediaTypeField->input_type=='brightness'){
    if($fromDb==true){
      $moBrightness=Yii::$app->inputHelperFunctions->getInputBrightnessValue($model,$mediaTypeField);
      if($moBrightness!=null){
        $model->brightness_sign=$moBrightness['brightness_sign'];
        $model->brightness=$moBrightness['brightness'];
        $model->brightness_unit=$moBrightness['brightness_unit'];
      }
    }
    echo '<div class="col-md-'.$mediaTypeField->colum_width.'">';
    echo '  '.$form->field($model, 'brightness',['template'=>'
    {label}
    <div class="box-wrap">
      <div class="select-wrap">
        '.Html::dropDownList('MediaOutlet[brightness_sign]', $model->brightness_sign, Yii::$app->helperFunction->brightnessSigns,['id'=>'input_field_bs_'.$mediaTypeField['id'],'class'=>'form__input form__select form__input-3sel-1']).'
        <i class="fa fa-sort-down"></i>
      </div>
      {input}
      <div class="select-wra1p">
        '.Html::dropDownList('MediaOutlet[brightness_unit]', $model->brightness_unit, Yii::$app->helperFunction->brightnessUnits,['id'=>'input_field_bu_'.$mediaTypeField['id'],'class'=>'form__input form__select form__input-3sel-2']).'
        <!--i class="fa fa-sort-down"></i-->
      </div>
    </div>
    ','labelOptions'=>['class'=>'form__label']])->textInput(['id'=>'input_field_b_'.$mediaTypeField['id'],'maxlength'=>true,'class'=>'form__input','autocomplete'=>'off'])->label($mediaTypeField['title'].$hintHtml);
    echo '</div>';
  }

  //Tinymce
  if($mediaTypeField->input_type=='tinymce'){
    echo '<div class="col-md-'.$mediaTypeField->colum_width.'">';
    echo $form->field($model, 'input_field['.$mediaTypeField['id'].']',['labelOptions'=>['class'=>'form__label']])->textArea(['id'=>'input_field_'.$mediaTypeField['id'],'rows' => 4])->label($mediaTypeField['title'].$hintHtml);
    echo '</div>';
    $this->registerJs('
      tinymce.init({
        selector:"#input_field_'.$mediaTypeField['id'].'",theme: "modern",
        menubar:false,
        paste_as_text: true,
        directionality : "ltr",
        relative_urls : false,
        remove_script_host : false,
        convert_urls : false,
        plugins: [
          "advlist autolink lists link image charmap print preview anchor",
          "searchreplace visualblocks code fullscreen",
          "insertdatetime media table paste"
        ],
        toolbar: "insertfile undo redo | paste | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
      });
    ');
  }


  if($mediaTypeField->input_type=='select'){
    $selectOptions=[];
    if($mediaTypeField->predefined_link!=null){
      $selectArr=Yii::$app->inputHelperFunctions->getPredefinedList($model->media_type_id,$mediaTypeField->predefined_link);
    }else{
      $selectArr=ArrayHelper::map(Yii::$app->inputHelperFunctions->getSubOptions($mediaTypeField->id),"id","title");
    }
    $selectOptions['prompt']=Yii::t('app','Select');
    if($mediaTypeField->selection_type==2){
      $selectOptions['multiple']='multiple';
      $selectOptions['class']='form__input form__select select2';

      $this->registerJs('
      $("#mediaoutlet-input_field-'.$mediaTypeField['id'].'").select2({
        placeholder: "'.$mediaTypeField['title'].'",
      });
      ');
    }else{
      $selectOptions['class']='form__input form__select';
    }
    $label=$mediaTypeField['title'].$hintHtml;

    echo '<div class="col-md-'.$mediaTypeField->colum_width.'">';
    echo $form->field($model, 'input_field['.$mediaTypeField['id'].']',['template'=>'
    {label}
    <div class="select-wrap">
    {input}
    '.($mediaTypeField->selection_type==2 ? '' : '<i class="fa fa-sort-down"></i>').'
    </div>
    {error}
    ','labelOptions'=>['class'=>'form__label']])->dropDownList($selectArr,$selectOptions,['id'=>'input_field_'.$mediaTypeField['id'],])->label($label);
    echo '</div>';


  }


  if($mediaTypeField->input_type=='brandOption'){
    Yii::$app->params['brandFldId']='mediaoutlet-brand_id';
    $selectOptions=[];
    if(isset($_REQUEST['MediaOutlet']['brand_id'])){
      $brandsList=Brand::find()->where(['id'=>$model->brand_id,'status'=>1,'trashed'=>0])->asArray()->all();
      //$model->brand_id=ArrayHelper::map($brandsList,"id","title");
      $txtBrandsSelection='';
      if($brandsList!=null){
        foreach($brandsList as $selectedBrand){
          $txtBrandsSelection.='
          var newOption = new Option("'.$selectedBrand['title'].'", "'.$selectedBrand['id'].'", true, true);
          $("#mediaoutlet-brand_id").append(newOption).trigger("change");
          ';
        }
      }
    }else{
      $subQueryMOSelBrands=MediaOutletBrand::find()->select(['brand_id'])->where(['media_outlet_id'=>$model->id]);

      $brandsList=Brand::find()->where(['id'=>$subQueryMOSelBrands,'status'=>1,'trashed'=>0])->asArray()->all();
      //$model->brand_id=ArrayHelper::map($brandsList,"id","title");
      $txtBrandsSelection='';
      if($brandsList!=null){
        foreach($brandsList as $selectedBrand){
          $txtBrandsSelection.='
          var newOption = new Option("'.$selectedBrand['title'].'", "'.$selectedBrand['id'].'", true, true);
          $("#mediaoutlet-brand_id").append(newOption).trigger("change");
          ';
        }
      }
    }

    if($mediaTypeField->selection_type==2){
      $selectOptions['multiple']='multiple';
      $selectOptions['class']='form__input form__select select2';

      $this->registerJs('
      $("#mediaoutlet-brand_id").select2({
        placeholder: "'.$mediaTypeField['title'].'",
        minimumInputLength: 1,
        ajax: {
          url: "'.Url::to(['suggestions/brands-list']).'",
          dataType: "json"

        }
      });

      '.$txtBrandsSelection.'
      ');
    }else{
      $selectOptions['class']='form__input form__select';
    }
    $label=$mediaTypeField['title'].$hintHtml;
    $label.=' <span><a href="javascript:;" data-toggle="modal" data-target="#modalBrandForm" class="btn btn-box-tool"><i class="fa fa-plus"></i></a>';

    echo '<div class="col-md-'.$mediaTypeField->colum_width.'">';
    echo $form->field($model, 'brand_id',['template'=>'
    {label}
    <div class="select-wrap">
    {input}
    '.($mediaTypeField->selection_type==2 ? '' : '<i class="fa fa-sort-down"></i>').'
    </div>
    {error}
    ','labelOptions'=>['class'=>'form__label']])->dropDownList([],$selectOptions,['id'=>'brand_id'])->label($label);
    echo '</div>';


  }
  if($mediaTypeField->input_type=='checkbox'){
    if($mediaTypeField->predefined_link!=null){
      $selectArr=Yii::$app->inputHelperFunctions->getPredefinedList($model->media_type_id,$mediaTypeField->predefined_link);
    }else{
      $selectArr=ArrayHelper::map(Yii::$app->inputHelperFunctions->getSubOptions($mediaTypeField->id),"id","title");
    }
    echo '<div class="col-md-'.$mediaTypeField->colum_width.'">';
    echo $form->field($model, 'input_field['.$mediaTypeField['id'].']',['labelOptions'=>['class'=>'form__label']])->checkboxList($selectArr)->label($mediaTypeField['title'].$hintHtml);
    echo '</div>';
  }
  if($mediaTypeField->input_type=='radio'){
    if($mediaTypeField->predefined_link!=null){
      $selectArr=Yii::$app->inputHelperFunctions->getPredefinedList($model->media_type_id,$mediaTypeField->predefined_link);
    }else{
      $selectArr=ArrayHelper::map(Yii::$app->inputHelperFunctions->getSubOptions($mediaTypeField->id),"id","title");
    }
    echo '<div class="col-md-'.$mediaTypeField->colum_width.'">';
    echo $form->field($model, 'input_field['.$mediaTypeField['id'].']',['labelOptions'=>['class'=>'form__label']])->radioList($selectArr)->label($mediaTypeField['title'].$hintHtml);
    echo '</div>';
  }

  //Rate card file input
  if($mediaTypeField->input_type=='ratecardFile'){
    $defRCImage='images/no_image.png';
    $defRCImage=Yii::$app->helperFunction->getImagePath('media-outlet-ratecard',$model->rate_card,'tiny');
    echo '<div class="col-md-'.$mediaTypeField->colum_width.'">';
    echo '  '.$form->field($model, 'rate_card',['labelOptions'=>['class'=>'form__label'],'template'=>'
    {label}
    <div class="input-group file-input">
    <div class="input-group-prepend">
    <span class="input-group-text"><img src="'.$defRCImage.'" width="25" height="25" /></span>
    </div>
    <div class="custom-file">
    {input}
    <label class="custom-file-label" for="inputFileRC">Choose file</label>
    </div>
    </div>
    '])->fileInput(['id'=>'inputFileRC','class'=>'form__input custom-file-input','accept'=>'application/pdf,image/*'])->label($mediaTypeField['title']);
    echo '</div>';
  }

  //Media Kit file input
  if($mediaTypeField->input_type=='mediaKitFile'){
    $defMKImage='images/no_image.png';
    $defMKImage=Yii::$app->helperFunction->getImagePath('media-outlet-mediakit',$model->media_kit,'tiny');
    echo '<div class="col-md-'.$mediaTypeField->colum_width.'">';
    echo '  '.$form->field($model, 'media_kit',['labelOptions'=>['class'=>'form__label'],'template'=>'
    {label}
    <div class="input-group file-input">
    <div class="input-group-prepend">
    <span class="input-group-text"><img src="'.$defMKImage.'" width="25" height="25" /></span>
    </div>
    <div class="custom-file">
    {input}
    <label class="custom-file-label" for="inputFileRC">Choose file</label>
    </div>
    </div>
    '])->fileInput(['id'=>'inputFileMK','class'=>'form__input custom-file-input','accept'=>'application/pdf'])->label($mediaTypeField['title']);
    echo '</div>';
  }

  //Qty field
  if($mediaTypeField->input_type=='qtyField'){
    echo '<div class="col-md-'.$mediaTypeField->colum_width.'">';
    echo '  '.$form->field($model, 'qty',['labelOptions'=>['class'=>'form__label']])->textInput(['id'=>'media-outlet-qty','class'=>'form__input numeric-field-noz','maxlength' => true, 'autocomplete'=>'off'])->label($mediaTypeField['title']);
    echo '</div>';
  }

  //Google Map
  if($mediaTypeField->input_type=='googlemap'){
    echo $this->render('google_map',['form'=>$form,'model'=>$model,'showGoogleMap'=>$showGoogleMap,'hintHtml'=>$hintHtml]);
  }
  if($colsInrow==12){
    echo '</div><div class="row">';
    $colsInrow=0;
  };
}
?>
</div>
<?php
/*$this->registerJs('
$( "#w1" ).validate( {
  rules:{
    '.$txtRules.'
  }
  messages:{
    '.$txtMessages.'
  },
  errorElement: "em",
	errorPlacement: function ( error, element ) {
		error.addClass( "help-block" );
		if ( element.prop( "type" ) === "checkbox" ) {
			error.insertAfter( element.parent( "label" ) );
		} else {
			error.insertAfter( element );
		}
	},
	highlight: function ( element, errorClass, validClass ) {
		$( element ).parents( ".form-group" ).addClass( "has-error" ).removeClass( "has-success" );
	},
	unhighlight: function (element, errorClass, validClass) {
		$( element ).parents( ".form-group" ).addClass( "has-success" ).removeClass( "has-error" );
	}
});
');*/
}
?>
