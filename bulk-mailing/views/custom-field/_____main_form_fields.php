<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
?>
<div id="mainformfields" class="row">
  <div class="col-xs-12 col-sm-9">
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('app','Form Fields')?> <small><a href="javascript:;" onclick="javascript:updateWidgets('col');">Collapse All</a> | <a href="javascript:;" onclick="javascript:updateWidgets('exp');">Expand All</a></small></h3>
      </div>
      <div class="box-body">
        <ul id="form-fields">
          <?php
          $rows=1;
          $fldRank=[];
          $formFields=$model->formFields;
          $formFieldTypes=Yii::$app->inputHelperFunctions->inputOptions;
          if($formFields!=null){
            foreach ($formFields as $formField) {
              $fldRank[]=$formField['id'];
              $col1='8';
              $col2='2';
              $col3='';
              if($formField['input_type']=="select" || $formField['input_type']=="brandOption" || $formField['input_type']=="text"){
                $col1='6';
                $col2='2';
                $col3='2';
              }
          ?>
          <li id="<?= $formField['id']?>">
          <?php Pjax::begin(['id'=>'box-container-'.$formField['id']]); ?>
          <div id="frmFld-<?= $rows?>" class="box box-default<?= $formField['status']==0 ? ' collapsed-box' : ''?> box-solid">
            <input type="hidden" name="MediaType[inputfield][<?= $rows?>][id]" value="<?= $formField['id']?>" />
            <input type="hidden" name="MediaType[inputfield][<?= $rows?>][type]" value="<?= $formField['input_type']?>" />
            <div class="box-header with-border">
              <h3 class="box-title"><?= '<i class="'.$formFieldTypes[$formField['input_type']]['icon'].'"></i> '.$formFieldTypes[$formField['input_type']]['title']?><span class="fldTitle"> - <?= $formField['title']?></span></h3>
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-box-tool box-action<?= $formField['status']==0 ? ' un-collapse-it' : ' collapse-it'?>"><i class="fa fa-<?= $formField['status']==0 ? 'plus' : 'minus'?>"></i></button>
                <button type="button" class="btn btn-box-tool change-field-status" data-id="<?= $formField['id']?>" data-stype="<?= $formField['status']==1 ? 'disable' : 'enable'?>"><i class="fa fa-<?= $formField['status']==1 ? 'eye' : 'eye-slash'?>"></i></button>
                <button type="button" class="btn btn-box-tool remove-input-field" data-field_id="<?= $formField['id']?>"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-xs-12 col-sm-<?= $col1?>">
                  <div class="form-group">
                    <label class="control-label" for="input-field-title-<?= $rows?>">Title</label>
                    <input type="text" id="input-field-title-<?= $rows?>" class="form-control field-title" name="MediaType[inputfield][<?= $rows?>][title]" value="<?= $formField['title']?>" placeholder="Title">
                  </div>
                </div>
                <div class="col-xs-12 col-sm-<?= $col2?>">
                  <div class="form-group">
                    <label class="control-label" for="input-field-show_in_grid-<?= $rows?>">Show In Grid</label>
                    <select type="text" id="input-field-show_in_grid-<?= $rows?>" class="form-control" name="MediaType[inputfield][<?= $rows?>][show_in_grid]">
                      <option value="1"<?= $formField['show_in_grid']==1 ? ' selected="selected"' : ''?>>Yes</option>
                      <option value="0"<?= $formField['show_in_grid']==0 ? ' selected="selected"' : ''?>>No</option>
                    </select>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-<?= $col2?>">
                  <div class="form-group">
                    <label class="control-label" for="input-field-is_required-<?= $rows?>">Required</label>
                    <select type="text" id="input-field-is_required-<?= $rows?>" class="form-control" name="MediaType[inputfield][<?= $rows?>][is_required]">
                      <option value="1"<?= $formField['is_required']==1 ? ' selected="selected"' : ''?>>Yes</option>
                      <option value="0"<?= $formField['is_required']==0 ? ' selected="selected"' : ''?>>No</option>
                    </select>
                  </div>
                </div>
                <?php if($formField['input_type']=="select" || $formField['input_type']=="brandOption"){?>
                <div class="col-xs-12 col-sm-<?= $col3?>">
                  <div class="form-group">
                    <label class="control-label" for="input-field-selection_type-<?= $rows?>">Selection</label>
                    <select type="text" id="input-field-selection_type-<?= $rows?>" class="form-control" name="MediaType[inputfield][<?= $rows?>][selection_type]">
                      <option value="1"<?= $formField['selection_type']==1 ? ' selected="selected"' : ''?>>Single</option>
                      <option value="2"<?= $formField['selection_type']==2 ? ' selected="selected"' : ''?>>Multiple</option>
                    </select>
                  </div>
                </div>
              <?php }?>
                <?php if($formField['input_type']=="text"){?>
                <div class="col-xs-12 col-sm-<?= $col3?>">
                  <div class="form-group">
                    <label class="control-label" for="input-field-autocomplete_text-<?= $rows?>">Autocomplete</label>
                    <select type="text" id="input-field-autocomplete_text-<?= $rows?>" class="form-control" name="MediaType[inputfield][<?= $rows?>][autocomplete_text]">
                      <option value="0"<?= $formField['autocomplete_text']==0 ? ' selected="selected"' : ''?>>No</option>
                      <option value="1"<?= $formField['autocomplete_text']==1 ? ' selected="selected"' : ''?>>Yes</option>
                    </select>
                  </div>
                </div>
              <?php }?>
              </div>
              <div class="row">
                <div class="col-xs-6">
                  <div class="form-group">
                    <label class="control-label" for="input-field-short_name-<?= $rows?>">Short Name</label>
                    <input type="text" id="input-field-short_name-<?= $rows?>" class="form-control" name="MediaType[inputfield][<?= $rows?>][short_name]" value="<?= $formField['short_name']?>" placeholder="Short Name">
                  </div>
                </div>
                <div class="col-xs-6">
                  <div class="form-group">
                    <label class="control-label" for="input-field-colum_width-<?= $rows?>">Columns</label>
                    <select type="text" id="input-field-colum_width-<?= $rows?>" class="form-control" name="MediaType[inputfield][<?= $rows?>][colum_width]">
                      <?php for($cn=1;$cn<=12;$cn++){?>
                      <option value="<?= $cn?>"<?= $formField['colum_width']==$cn ? ' selected="selected"' : ''?>><?= $cn?> Columns</option>
                      <?php }?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <div class="form-group">
                    <label class="control-label" for="input-field-hint_text-<?= $rows?>">Hint / Information Text (Frontend)</label>
                    <textarea rows="4" id="input-field-hint_text-<?= $rows?>" class="form-control" name="MediaType[inputfield][<?= $rows?>][hint_text]"><?= $formField['hint_text']?></textarea>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-xs-12">
                  <div class="form-group">
                    <label class="control-label" for="input-field-descp-<?= $rows?>">Description (Backend)</label>
                    <textarea rows="4" id="input-field-descp-<?= $rows?>" class="form-control" name="MediaType[inputfield][<?= $rows?>][descp]"><?= $formField['descp']?></textarea>
                  </div>
                </div>
              </div>
              <?php if($formField['input_type']=="autocomplete"){?>
              <div class="row">
                <div class="col-xs-12">
                  <h4>Link to </h4>
                  <?php foreach(Yii::$app->inputHelperFunctions->autoCompleteOptions as $key=>$val){?>
                    <label class="dd-pdtypes" for="input-field-autocomplete-link-<?= $rows?>-<?= $key?>"><input type="radio" id="input-field-autocomplete-link-<?= $rows?>-<?= $key?>" name="MediaType[inputfield][<?= $rows?>][autocomplete_link]" value="<?= $key?>"<?= $formField['autocomplete_link']==$key ? ' checked="checked"' : ''?>> <?= $val['title']?></label>
                  <?php }?>
                </div>
              </div>
              <?php }?>
              <?php if($formField['input_type']=="select" || $formField['input_type']=="checkbox" || $formField['input_type']=="radio"){?>
              <div class="row">
                <div class="col-xs-12">
                  <div class="form-group">
                    <label class="control-label" onclick="showSubOptions('p',<?= $rows?>)" for="input-field-sub-opt-selection-<?= $rows?>-up"><input type="radio" id="input-field-sub-opt-selection-<?= $rows?>-up" name="MediaType[inputfield][<?= $rows?>][suboptselection]" value="usepredefined"<?= $formField['predefined_link']!=null ? ' checked="checked"' : ''?> /> Use Predefined list</label>&nbsp;&nbsp;&nbsp;
                    <label class="control-label" onclick="showSubOptions('c',<?= $rows?>)" for="input-field-sub-opt-selection-<?= $rows?>-uc"><input type="radio" id="input-field-sub-opt-selection-<?= $rows?>-uc" name="MediaType[inputfield][<?= $rows?>][suboptselection]" value="usecustom"<?= $formField['predefined_link']==null ? ' checked="checked"' : ''?> /> Add New Options</label>
                  </div>
                  <div id="predefinedOpts<?= $rows?>"<?= $formField['predefined_link']!=null ? '' : ' style="display:none;"'?>>
                    <?php
                    foreach(Yii::$app->inputHelperFunctions->predefinedOptions as $predefinedOption){
                      $key='pdlist_'.$predefinedOption['id'];
                      $val=$predefinedOption['title'];
                    ?>
                    <label class="dd-pdtypes" for="input-field-predefined-selection-<?= $rows?>-<?= $key?>"><input type="radio" id="input-field-predefined-selection-<?= $rows?>-<?= $key?>" name="MediaType[inputfield][<?= $rows?>][predefined_link]" value="<?= $key?>"<?= $formField['predefined_link']==$key ? ' checked="checked"' : ''?>> <?= $val?></label>
                    <?php }?>
                  </div>
                  <div id="customOpts<?= $rows?>"<?= $formField['predefined_link']==null ? '' : ' style="display:none;"'?>>
                    <table id="customOptsTable<?= $rows?>" class="table table-bordered table-striped">
                      <thead>
                      <tr>
                        <th>Title</th>
                        <th width="10" style="text-align: center"><a href="javascript:;" class="btn btn-xs btn-success" onclick="javascript:addSubOption(<?= $rows?>);"><i class="fa fa-plus"></i></a></th>
                      </tr>
                      </thead>
                      <tbody>
                      <?php
                      $subOptions=$formField->fieldSubOptions;
                      if($subOptions!=null){
                        foreach($subOptions as $subOption){
                      ?>
                      <tr class="subOptRow">
                        <td colspan="2">
                          <?php Pjax::begin(['id'=>'tbl-'.$formField['id'].'-container-'.$subOption['id']]); ?>
                          <div class="row">
                            <div class="col-xs-12 col-sm-10">
                              <input type="text" class="form-control" name="MediaType[inputfield][<?= $rows?>][suboption_title][]" value="<?= $subOption['title']?>"<?= $subOption['status']==1 ? '' : ' disabled="disabled"'?> />
                            </div>
                            <div class="col-xs-12 col-sm-2 text-right">
                              <a href="javascript:;" class="btn btn-xs btn-<?= $subOption['status']==1 ? 'success' : 'warning'?> change-field-option-status" data-id="<?= $subOption['id']?>" data-field_id="<?= $formField['id']?>" data-stype="<?= $subOption['status']==1 ? 'disable' : 'enable'?>"><i class="fa fa-<?= $subOption['status']==1 ? 'eye' : 'eye-slash'?>"></i></a>
                              <a href="javascript:;" class="btn btn-xs btn-danger remove-input-field-option" data-field_id="<?= $formField['id']?>" data-option_id="<?= $subOption['id']?>"><i class="fa fa-times"></i></a>
                            </div>
                          </div>
                          <?php Pjax::end(); ?>
                        </td>
                      </tr>
                      <?php }}?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <?php }?>
            </div>
          </div>
          <?php Pjax::end(); ?>
          </li>
          <?php
              $rows++;
            }
          }
          ?>
        </ul>
      </div>
    </div>
  </div>
  <div class="col-xs-12 col-sm-3">
    <div data-spy="affix" data-offset-top="500">
      <div class="box box-info sidebar">
        <div class="box-header with-border">
          <h3 class="box-title"><?= Yii::t('app','Input Options')?></h3>
        </div>
        <div id="inputOpts" class="box-body">
          <ul class="nav nav-pills nav-stacked">
            <?php foreach(Yii::$app->inputHelperFunctions->inputOptions as $key=>$val){?>
              <li>
                <a class="inputype" href="javascript:;" onclick="javascript:addInput('<?= $key?>');">
                  <?= '<i class="'.$val['icon'].'"></i> '.$val['title']?>
                </a>
              </li>
            <?php }?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<input id="fldRank" name="MediaType['inputfieldrank']" type="hidden" value="<?= implode(",",$fldRank)?>" />
<script>
var row=<?= $rows?>;
<?php foreach(Yii::$app->inputHelperFunctions->inputOptions as $key=>$val){?>
var inputOptions_<?= $key?> = '<i class="<?= $val['icon']?>"></i> <?= $val['title']?>';
<?php }?>
function addInput(inputType)
{
	col1='8';
	col2='2';
	col3='';
	if(inputType=="text" || inputType=="select" || inputType=="brandOption"){
		col1='6';
		col2='2';
		col3='2';
	}
	html = '';
	html+= '<li>';
	html+= '<div id="tmpFld-'+row+'" class="box box-default box-solid">';
	html+= '	<input type="hidden" name="MediaType[inputfield]['+row+'][type]" value="'+inputType+'" />';
	html+= '	<div class="box-header with-border">';
	html+= '		<h3 class="box-title">'+eval("inputOptions_"+inputType)+'<span class="fldTitle"></span></h3>';
	html+= '		<div class="pull-right box-tools">';
	html+= '			<button type="button" class="btn btn-box-tool collapse-it"><i class="fa fa-minus"></i></button>';
	html+= '			<button type="button" class="btn btn-box-tool" onclick="removeField('+row+');"><i class="fa fa-times"></i></button>';
	html+= '		</div>';
	html+= '	</div>';
	html+= '	<div class="box-body">';
	html+= '		<div class="row">';
	html+= '			<div class="col-xs-12 col-sm-'+col1+'">';
	html+= '				<div class="form-group">';
	html+= '					<label class="control-label" for="input-field-title-'+row+'">Title</label>';
	html+= '					<input type="text" id="input-field-title-'+row+'" class="form-control field-title" name="MediaType[inputfield]['+row+'][title]" value="" placeholder="Title">';
	html+= '				</div>';
	html+= '			</div>';
	html+= '			<div class="col-xs-12 col-sm-'+col2+'">';
	html+= '				<div class="form-group">';
	html+= '					<label class="control-label" for="input-field-show_in_grid-'+row+'">Show in Grid</label>';
	html+= '					<select type="text" id="input-field-show_in_grid-'+row+'" class="form-control" name="MediaType[inputfield]['+row+'][show_in_grid]">';
	html+= '						<option value="1">Yes</option>';
	html+= '						<option value="0" selected="selected">No</option>';
	html+= '					</select>';
	html+= '				</div>';
	html+= '			</div>';
	html+= '			<div class="col-xs-12 col-sm-'+col2+'">';
	html+= '				<div class="form-group">';
	html+= '					<label class="control-label" for="input-field-is_required-'+row+'">Required</label>';
	html+= '					<select type="text" id="input-field-is_required-'+row+'" class="form-control" name="MediaType[inputfield]['+row+'][is_required]">';
	html+= '						<option value="1">Yes</option>';
	html+= '						<option value="0">No</option>';
	html+= '					</select>';
	html+= '				</div>';
	html+= '			</div>';
	if(inputType=="select" || inputType=="brandOption"){
	html+= '			<div class="col-xs-12 col-sm-'+col3+'">';
	html+= '				<div class="form-group">';
	html+= '					<label class="control-label" for="input-field-selection_type-'+row+'">Selection</label>';
	html+= '					<select type="text" id="input-field-selection_type-'+row+'" class="form-control" name="MediaType[inputfield]['+row+'][selection_type]">';
	html+= '						<option value="1">Single</option>';
	html+= '						<option value="2">Multiple</option>';
	html+= '					</select>';
	html+= '				</div>';
	html+= '			</div>';
	}
	if(inputType=="text"){
	html+= '			<div class="col-xs-12 col-sm-'+col3+'">';
	html+= '				<div class="form-group">';
	html+= '					<label class="control-label" for="input-field-autocomplete_text-'+row+'">Autocomplete</label>';
	html+= '					<select type="text" id="input-field-autocomplete_text-'+row+'" class="form-control" name="MediaType[inputfield]['+row+'][autocomplete_text]">';
	html+= '						<option value="0">No</option>';
	html+= '						<option value="1">Yes</option>';
	html+= '					</select>';
	html+= '				</div>';
	html+= '			</div>';
	}
	html+= '		</div>';
  html+= '		<div class="row">';
  html+= '		  <div class="col-xs-6">';
  html+= '		    <div class="form-group">';
  html+= '		      <label class="control-label" for="input-field-short_name-'+row+'">Short Name</label>';
  html+= '		      <input type="text" id="input-field-short_name-'+row+'" class="form-control" name="MediaType[inputfield]['+row+'][short_name]" value="" placeholder="Short Name">';
  html+= '		    </div>';
  html+= '		  </div>';
  html+= '		  <div class="col-xs-6">';
  html+= '		    <div class="form-group">';
  html+= '		      <label class="control-label" for="input-field-colum_width-'+row+'">Columns</label>';
  html+= '		      <select type="text" id="input-field-colum_width-'+row+'" class="form-control" name="MediaType[inputfield]['+row+'][colum_width]">';
          <?php for($cn=1;$cn<=12;$cn++){?>
  html+= '		        <option value="<?= $cn?>"><?= $cn?> Columns</option>';
          <?php }?>
  html+= '		      </select>';
  html+= '		    </div>';
  html+= '		  </div>';
  html+= '		</div>';
	html+= '		<div class="row">';
	html+= '			<div class="col-xs-12">';
	html+= '				<div class="form-group">';
	html+= '					<label class="control-label" for="input-field-hint_text-'+row+'">Hint / Information Text (Frontend)</label>';
	html+= '					<textarea rows="4" id="input-field-hint_text-'+row+'" class="form-control" name="MediaType[inputfield]['+row+'][hint_text]"></textarea>';
	html+= '				</div>';
	html+= '			</div>';
	html+= '		</div>';
	html+= '		<div class="row">';
	html+= '			<div class="col-xs-12">';
	html+= '				<div class="form-group">';
	html+= '					<label class="control-label" for="input-field-descp-'+row+'">Description (Backend)</label>';
	html+= '					<textarea rows="4" id="input-field-descp-'+row+'" class="form-control" name="MediaType[inputfield]['+row+'][descp]"></textarea>';
	html+= '				</div>';
	html+= '			</div>';
	html+= '		</div>';
	if(inputType=="autocomplete"){
	html+= '		<div class="row">';
	html+= '			<div class="col-xs-12">';
	html+= '				<h4>Link to </h4>';
	<?php foreach(Yii::$app->inputHelperFunctions->autoCompleteOptions as $key=>$val){?>
	html+= '					<label class="dd-pdtypes" for="input-field-autocomplete-link-'+row+'-<?= $key?>"><input type="radio" id="input-field-autocomplete-link-'+row+'-<?= $key?>" name="MediaType[inputfield]['+row+'][autocomplete_link]" value="<?= $key?>"> <?= $val['title']?></label>';
	<?php }?>
	html+= '			</div>';
	html+= '		</div>';
	}
	if(inputType=="select" || inputType=="checkbox" || inputType=="radio"){
	html+= '		<div class="row">';
	html+= '			<div class="col-xs-12">';
	html+= '				<div class="form-group">';
	html+= '					<label class="control-label" onclick="showTmpSubOptions(\'p\','+row+')" for="input-field-sub-opt-selection-'+row+'-up"><input type="radio" id="input-field-sub-opt-selection-'+row+'-up" name="MediaType[inputfield]['+row+'][suboptselection]" value="usepredefined" checked="checked" /> Use Predefined list</label>&nbsp;&nbsp;&nbsp;';
	html+= '					<label class="control-label" onclick="showTmpSubOptions(\'c\','+row+')" for="input-field-sub-opt-selection-'+row+'-uc"><input type="radio" id="input-field-sub-opt-selection-'+row+'-uc" name="MediaType[inputfield]['+row+'][suboptselection]" value="usecustom" /> Add New Options</label>';
	html+= '				</div>';
	html+= '				<div id="predefinedOpts'+row+'">';
	<?php
  foreach(Yii::$app->inputHelperFunctions->predefinedOptions as $predefinedOption){
    $key='pdlist_'.$predefinedOption['id'];
    $val=$predefinedOption['title'];
  ?>
	html+= '					<label class="dd-pdtypes" for="input-field-predefined-selection-'+row+'-<?= $key?>"><input type="radio" id="input-field-predefined-selection-'+row+'-<?= $key?>" name="MediaType[inputfield]['+row+'][predefined_link]" value="<?= $key?>"> <?= $val?></label>';
	<?php }?>
	html+= '				</div>';
	html+= '				<div id="customOpts'+row+'" style="display:none;">';
	html+= '					<table id="customOptsTable'+row+'" class="table table-bordered table-striped">';
	html+= '						<thead>';
	html+= '						<tr>';
	html+= '							<th>Title</th>';
	html+= '							<th width="10"><a href="javascript:;" class="btn btn-xs btn-success" onclick="javascript:addSubOption('+row+');"><i class="fa fa-plus"></i></a></th>';
	html+= '						</tr>';
	html+= '						</thead>';
	html+= '						<tbody>';
	for(g=1;g<=3;g++){
	html+= '						<tr class="subOptRow">';
	html+= '							<td>';
	html+= '								<input type="text" class="form-control" name="MediaType[inputfield]['+row+'][suboption_title][]" />';
	html+= '							</td>';
	html+= '							<td><a href="javascript:;" class="btn btn-xs btn-danger removeTmpSubOption"><i class="fa fa-times"></a></td>';
	html+= '						</tr>';
	}
	html+= '						</tbody>';
	html+= '					</table>';
	html+= '				</div>';
	html+= '			</div>';
	html+= '		</div>';
	}
	html+= '	</div>';
	html+= '</div>';
	html+= '</li>';
	$("#form-fields").append(html);
	row++;
}
function showSubOptions(type,id)
{
	if(type=='p'){
		$("#frmFld-"+id).find("#predefinedOpts"+id).show();
		$("#frmFld-"+id).find("#customOpts"+id).hide();
	}
	if(type=='c'){
		$("#frmFld-"+id).find("#predefinedOpts"+id).hide();
		$("#frmFld-"+id).find("#customOpts"+id).show();

	}
}
function showTmpSubOptions(type,id)
{
	if(type=='p'){
		$("#tmpFld-"+id).find("#predefinedOpts"+id).show();
		$("#tmpFld-"+id).find("#customOpts"+id).hide();
	}
	if(type=='c'){
		$("#tmpFld-"+id).find("#predefinedOpts"+id).hide();
		$("#tmpFld-"+id).find("#customOpts"+id).show();

	}
}
function addSubOption(id)
{
	html = '';
	html+= '<tr class="subOptRow">';
	html+= '	<td>';
	html+= '		<input type="text" class="form-control" name="MediaType[inputfield]['+id+'][suboption_title][]" />';
	html+= '	</td>';
	html+= '	<td><a href="javascript:;" class="btn btn-xs btn-danger removeTmpSubOption"><i class="fa fa-times"></a></td>';
	html+= '</tr>';
	$("#customOptsTable"+id).find("tbody").append(html);
}
function removeField(id)
{
	$("#tmpFld-"+id).remove();
}
function updateWidgets(act)
{
	$("#mainformfields .box-default").each(function( index ) {
		if(act=='col'){
			$(this).addClass("collapsed-box");
			$(this).find(".box-action").removeClass("collapse-it").addClass("un-collapse-it").html("<i class=\"fa fa-plus\"></i>");
		}else{
			$(this).removeClass("collapsed-box");
			$(this).find(".box-action").removeClass("un-collapse-it").addClass("collapse-it").html("<i class=\"fa fa-minus\"></i>");
		}
	});
}
</script>
