<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;
use app\widgets\PieChartWidget;

/* @var $this yii\web\View */
/* @var $model app\models\Campaign */

$this->title = Yii::t('app', 'Campaign');
$cardTitle = Yii::t('app','View Campaign:  {nameAttribute}', [
  'nameAttribute' => $model->subject,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->subject;
$this->params['breadcrumbs'][] = Yii::t('app', 'view');

$delivered=$model->delivered;
$opened = $model->uniqueOpened;
$pendingOpened = $delivered-$opened;
$clicked = $model->uniqueClicked;
$pendingClicked = $delivered-$clicked;

$overallEmailStats=[
  'labels' => [
    Yii::t('app','Delivered').' ('.$delivered.')',
    Yii::t('app','Hard Bounced').' ('.$model->hard_bounced.')',
    Yii::t('app','Soft Bounced').' ('.$model->soft_bounced.')',
  ],
  'datasets' => [[
    'data'=>[
      $delivered,
      $model->hard_bounced,
      $model->soft_bounced,
    ],
    'backgroundColor'=>['#28a745', '#dc3545', '#ffc107'],
  ]]
];

$openEmailStats=[
  'labels' => [
    Yii::t('app','Opened').' ('.$opened.')',
    Yii::t('app','Pending').' ('.$pendingOpened.')'
  ],
  'datasets' => [[
    'data'=>[
      $opened,
      $pendingOpened
    ],
    'backgroundColor'=>['#28a745', '#ccc'],
  ]]
];
$clickEmailStats=[
  'labels' => [
    Yii::t('app','Clicked').' ('.$clicked.')',
    Yii::t('app','Pending').' ('.$pendingClicked.')'
  ],
  'datasets' => [[
    'data'=>[
      $clicked,
      $pendingClicked
    ],
    'backgroundColor'=>['#28a745', '#ccc'],
  ]]
];

$smtpStats=null;
$smtps=Yii::$app->appHelperFunctions->getCampaignSmtpsListArr($model['id']);
if($smtps!=null){
  $smtpLabels=[];
  $smtpData=[];
  $smtpClrs=[];
  $n=0;
  foreach($smtps as $type=>$idz){
    $sentVia=Yii::$app->statsFunctions->getCampaignSmtpCount($model['id'],$idz);
    $smtpLabels[]=Yii::$app->appHelperFunctions->smtpServiceTypesArr[$type].' ('.$sentVia.')';
    $smtpData[]=$sentVia;
    $smtpClrs[]=Yii::$app->helperFunctions->colorsArr[$n];
    $n++;
  }

  $smtpStats=[
    'labels' => $smtpLabels,
    'datasets' => [[
      'data'=>$smtpData,
      'backgroundColor'=>$smtpClrs,
    ]]
  ];
}
?>
<style>
.filters{display: none;}
</style>
<div class="campaign-view">
  <div class="card card-primary card-outline card-outline-tabs mt-3">
    <div class="card-header p-0 border-bottom-0">
      <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true">Preview</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill" href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false">Emails Sent</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="custom-tabs-three-messages-tab" data-toggle="pill" href="#custom-tabs-three-messages" role="tab" aria-controls="custom-tabs-three-messages" aria-selected="false">Stats</a>
        </li>
      </ul>
    </div>
    <div class="card-body">
      <div class="tab-content" id="custom-tabs-three-tabContent">
        <div class="tab-pane fade show active" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
          <div class="col ml-4 mt-4">
            <span class="font-weight-bold">From: </span>
            <?= $model->sentBy->name.' < '.$model->sentBy->email.' >';?>
          </div>
          <div class="col ml-4 mt-3">
            <span class="font-weight-bold">Message:</span>
            <div>
              <?php
              $htmlBody=$model['descp'];
              $template = $model->template;
              if($template!=null){
                $htmlBody=str_replace("{content}",$htmlBody,$template->descp);
              }
              echo $htmlBody;
              ?>
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
          <?php CustomPjax::begin(['id'=>'grid-container']); ?>
          <?= CustomGridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'cardHeader' => false,
            'columns' => [
              ['attribute'=>'module_id','label'=>'UserName','value'=> function($model){
                return $model->fullname;
              }],
              'email',
              'delivered',
              'open',
              'click',
              'soft_bounced',
              'hard_bounced',
              'spam_complain',
            ],
          ])
          ?>
          <?php CustomPjax::end(); ?>
        </div>
        <div class="tab-pane fade" id="custom-tabs-three-messages" role="tabpanel" aria-labelledby="custom-tabs-three-messages-tab">
          <div class="row">
            <div class="col-12 col-sm-6">
              <?= PieChartWidget::widget(['heading'=>Yii::t('app','Delivery Stats'),'data'=>$overallEmailStats,'className'=>'info'])?>
            </div>
            <div class="col-12 col-sm-6">
              <?= PieChartWidget::widget(['heading'=>Yii::t('app','Open Stats'),'data'=>$openEmailStats,'className'=>'info'])?>
            </div>
            <div class="col-12 col-sm-6">
              <?= PieChartWidget::widget(['heading'=>Yii::t('app','Click Stats'),'data'=>$clickEmailStats,'className'=>'info'])?>
            </div>
            <div class="col-12 col-sm-6">
              <?= PieChartWidget::widget(['heading'=>Yii::t('app','Smtp Stats'),'data'=>$smtpStats,'className'=>'info'])?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
