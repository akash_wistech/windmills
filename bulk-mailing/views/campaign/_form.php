<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\assets\CampaignFormAsset;
CampaignFormAsset::register($this);

$employeesArr=Yii::$app->appHelperFunctions->getEmployeeListArr(Yii::$app->user->identity->company_id);
$templatesArr=Yii::$app->appHelperFunctions->getEmailTemplatesArr(Yii::$app->user->identity->company_id);
$listsArr=Yii::$app->appHelperFunctions->getListsArr(Yii::$app->user->identity->company_id);
$smtpServersListsArr=Yii::$app->appHelperFunctions->getSmtpServersListsArr(Yii::$app->user->identity->company_id);

/* @var $this yii\web\View */
/* @var $model app\models\Campaign */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs('
$("#campaign-list_id").select2({
	placeholder: "'.Yii::t('app','Select').'",
	allowClear: true,
	width: "100%",
});
$("#campaign-smtp_id").select2({
	placeholder: "'.Yii::t('app','Select').'",
	allowClear: true,
	width: "100%",
});
$("#campaign-distribution_date-input").datetimepicker({
  allowInputToggle: true,
  viewMode: "days",
  format: "YYYY-MM-DD",
  icons: {
    time: "fa fa-clock",
  },
});
initDropZone();


$("body").on("click", ".remove_file", function () {
	_this=$(this);
	id=_this.data("id");
	fn=_this.data("fn");
	var _targetContainer=".panel-body";
	Swal.fire({
		title: "'.Yii::t('app','Confirmation').'",
		html: "'.Yii::t('app','Are you sure you want to delete this? You will not be able to recover this!').'",
		type: "warning",
		showCancelButton: true,
    confirmButtonColor: "#47a447",
		confirmButtonText: "'.Yii::t('app','Yes, delete it!').'",
		cancelButtonText: "'.Yii::t('app','Cancel').'",
	}).then((result) => {
    if (result.value) {
			App.blockUI({
  			message: "'.Yii::t('app','Please wait...').'",
  			target: _targetContainer,
  			overlayColor: "none",
  			cenrerY: true,
  			boxed: true
  		});
  		$.ajax({
  			url: "'.Url::to(['delete-file','cid'=>$model->id,'fn'=>'']).'"+fn,
  			dataType: "html",
  			type: "POST",
  			success: function(html) {
  				_this.parents("#attchment"+id).remove();
  				App.unblockUI($(_targetContainer));
					toastr.success("'.Yii::t('app','Deleted!').'", "'.Yii::t('app','Attachment successfully').'");
  			},
  			error: bbAlert
  		});
    }
	});
});
tinymce.init({
	selector:".textareaEditor",theme: "modern",
	menubar:false,
	paste_as_text: true,
	directionality : "ltr",
	relative_urls : false,
	remove_script_host : false,
	convert_urls : false,
	plugins: [
		"advlist autolink lists link image charmap print preview anchor",
		"searchreplace visualblocks code fullscreen",
		"insertdatetime media table paste"
	],
	toolbar: "insertfile undo redo | paste | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image responsivefilemanager code",
	external_filemanager_path:"'.Yii::$app->params['baseDir'].'/filemanager/",
	filemanager_title:"Filemanager" ,
	external_plugins: { "filemanager" : "'.Yii::$app->params['baseDir'].'filemanager/plugin.min.js"}
});
');
$addTemplateHtml =' ';
$addTemplateHtml.='<a href="javascript:;" class="btn btn-xs btn-success load-modal" data-url="'.Url::to(['template/create']).'" data-heading="'.Yii::t('app','Create Template').'">';
$addTemplateHtml.='	<i class="fa fa-plus"></i>';
$addTemplateHtml.='</a>';


$msgHint =Yii::t('app','Following shortcodes are available.');
$msgHint.='<br />';
$msgHint.=Yii::t('app','{userEmail} {userName} {unsubscribeLink}');
?>
<section class="campaign-form card card-outline card-primary">
  <?php $form = ActiveForm::begin(); ?>
  <div style="display:none;">
    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>
  </div>
  <header class="card-header">
    <h2 class="card-title"><?= $cardTitle?></h2>
  </header>
  <div class="card-body">
    <div class="row">
      <div class="col-12 col-sm-6">
        <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>
      </div>
      <div class="col-12 col-sm-6">
        <?= $form->field($model, 'distribution_date',['template'=>'
        {label}
        <div class="input-group date" id="campaign-distribution_date-input" data-target-input="nearest">
          {input}
          <div class="input-group-append" data-target="#campaign-distribution_date-input" data-toggle="datetimepicker">
              <div class="input-group-text"><i class="fa fa-calendar"></i></div>
          </div>
        </div>
        {hint}{error}
        '])->textInput(['maxlength' => true, 'autocomplete'=>'off']) ?>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-6">
        <?= $form->field($model, 'from_id')->dropDownList($employeesArr,['prompt'=>Yii::t('app','Select')]) ?>
      </div>
      <div class="col-12 col-sm-6">
        <?= $form->field($model, 'reply_to')->textInput(['maxlength' => true])->label($model->getAttributeLabel('reply_to').' <small>('.Yii::t('app','Optional').')</small>') ?>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-6">
        <?= $form->field($model, 'template_id')->dropDownList($templatesArr,['prompt'=>Yii::t('app','Select'),'id'=>'dtmptid'])->label($model->getAttributeLabel('template_id').$addTemplateHtml) ?>
      </div>
      <div class="col-12 col-sm-6">
        <?= $form->field($model, 'list_id')->dropDownList($listsArr,['multiple'=>'multiple']) ?>
      </div>
    </div>

    <?= $form->field($model, 'descp')->textArea(['class'=>'form-control textareaEditor','rows' => 10])->hint($msgHint) ?>
		<?= $form->field($model, 'smtp_id')->dropDownList($smtpServersListsArr,['multiple'=>'multiple']) ?>
    <div id="hidden-files" class="hidden"></div>
		<?php
  	if($model->attachmentInfo!=null){
  	?>
		<ul class="mailbox-attachments d-flex align-items-stretch clearfix">
      <?php foreach($model->attachmentInfo as $attachmentFile){?>
				<li id="attchment<?= $attachmentFile['campaign_id']?>">
	        <span class="mailbox-attachment-icon clearfix" style="overflow:hidden;">
						<img src="<?= $attachmentFile['iconPath']?>" class="img-fluid img-thumbnail" alt="" />
					</span>
	        <div class="mailbox-attachment-info">
	          <a href="<?= $attachmentFile['link']?>" class="mailbox-attachment-name">
							<i class="fas fa-paperclip"></i> <?= $attachmentFile['file_title']?>
						</a>
            <span class="mailbox-attachment-size clearfix mt-1">
							<a href="javascript:;" class="btn btn-sm float-right btn-danger remove_file" data-id="<?= $attachmentFile['campaign_id']?>" data-fn="<?= $attachmentFile['file_name']?>">
								<i class="fa fa-times"></i>
							</a>
            </span>
	        </div>
	      </li>
      <?php }?>
    </ul>
  	<?php
    }
    ?>
<style>
.att-Item .img-box{max-height:55px;overflow:hidden;}
</style>
    <div id="dZU1pload" class="dropzone_my">
      <div class="hidden">
        <div id="preview-template">
          <div class="dz-preview dz-file-preview">
            <div class="dz-image"><img data-dz-thumbnail=""></div>
            <div class="dz-details">
              <div class="dz-size"><span data-dz-size=""></span></div>
              <div class="dz-filename"><span data-dz-name=""></span></div>
            </div>
            <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>
            <div class="dz-error-message"><span data-dz-errormessage=""></span></div>
            <div class="dz-success-mark">

              <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                <title>Check</title>
                <desc>Created with Sketch.</desc>
                <defs></defs>
                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                    <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
                </g>
              </svg>

            </div>
            <div class="dz-error-mark">

              <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                  <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                  <title>error</title>
                  <desc>Created with Sketch.</desc>
                  <defs></defs>
                  <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                      <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">
                          <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>
                      </g>
                  </g>
              </svg>

            </div>
          </div>
        </div>
      </div>
      <div class="dz-default dz-message">
        <?= Yii::t('app','Drop attachments here or click to upload.')?><br/>
        <small>(png,jpg,jpeg,gif,doc,docx,xls,xlsx,pdf)</small>
      </div>
    </div>

      <div class="col-12 col-sm-6">
          <?= $form->field($model, 'sms_text')->textarea(['maxlength' => true])->label('WhatsApp <small>('.Yii::t('app','Max 200 characters').')</small>') ?>
      </div>


  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save as draft'), ['onclick'=>"$('#campaign-status').val(0)",'class' => 'btn btn-info']) ?>
    <?= Html::submitButton(Yii::t('app', 'Save and send'), ['onclick'=>"$('#campaign-status').val(1)",'class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>

<script>
function initDropZone()
{
		var previewNode = document.querySelector("#preview-template");
		previewNode.id = "";
		var previewTemplate = previewNode.parentNode.innerHTML;

		var mydropzone;
		Dropzone.autoDiscover = false;
		$("#dZU1pload").dropzone({
		  url: "<?= Url::to(['site/att-drop-upload','cid'=>$model->id])?>",
		  previewTemplate: previewTemplate,
		  addRemoveLinks: false,
			acceptedFiles: '.png,.jpg,.jpeg,.gif,.pdf,.doc,.docx,.xls,.xlsx',
		  init: function() {
		    mydropzone=this;
		  },
		  success: function (file, response) {
		    var fileName = response;
				$("#hidden-files").append("<input type=\"hidden\" name=\"Campaign[attachment_file][]\" class=\"form-control\" value=\""+fileName+"\" />");
		    file.previewElement.classList.add("dz-success");
        file.previewElement.querySelector(".dz-progress").style.opacity = 0;
        file.previewElement.querySelector(".dz-success-mark").style.opacity = 1;
		  },
		  removedfile: function(file){
		    $("input[value=\""+file.name+"\"]").remove();
		    $(document).find(file.previewElement).remove();
		  },
		  error: function (file, response) {
		    file.previewElement.querySelector("[data-dz-errormessage]").textContent = response;
		    file.previewElement.classList.add("dz-error");
		  },
		  queuecomplete: function (file, response) {
				submitFiles();
		  }
		});
}
</script>
<?= $this->render('/template/js/scripts')?>
