<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\SettingsAssets;
SettingsAssets::register($this);

/* @var $this yii\web\View */

$permissionGroupListArr=Yii::$app->appHelperFunctions->permissionGroupListArr;
$predefinedListArr=Yii::$app->appHelperFunctions->predefinedListArr;

$this->title = 'Settings';
?>
<div class="system-setting">
	<section class="update-profile-form card card-outline card-primary">
		<header class="card-header">
			<h2 class="card-title"><?= $this->title?></h2>
		</header>
		<?php $form = ActiveForm::begin(); ?>
		<div class="card-body">
			<div class="row">
				<div class="col-sm-4">
					<?= $form->field($model, 'cust_perm_id')->dropDownList($permissionGroupListArr,['prompt'=>'Select']) ?>
				</div>
				<div class="col-sm-4">
					<?= $form->field($model, 'country_list_id')->dropDownList($predefinedListArr,['prompt'=>'Select']) ?>
				</div>
				<div class="col-sm-4">
					<?= $form->field($model, 'zone_list_id')->dropDownList($predefinedListArr,['prompt'=>'Select']) ?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<?= $form->field($model, 'role_list_id')->dropDownList($predefinedListArr,['prompt'=>'Select']) ?>
				</div>
				<div class="col-sm-4">
					<?= $form->field($model, 'job_title_list_id')->dropDownList($predefinedListArr,['prompt'=>'Select']) ?>
				</div>
				<div class="col-sm-4">
					<?= $form->field($model, 'department_list_id')->dropDownList($predefinedListArr,['prompt'=>'Select']) ?>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<?= $form->field($model, 'currency_sign')->textInput(['maxlength' => true]) ?>
				</div>
				<div class="col-sm-4">
					<?= $form->field($model, 'cron_server_diff',['template'=>'
					{label}
					<div class="input-group input-group-md">
			      {input}
			      <div class="input-group-append">
			        <span class="input-group-text">'.Yii::t('app','Minutes').'</span>
			      </div>
			    </div>
			    {error}{hint}'])->textInput(['maxlength' => true]) ?>
				</div>
			</div>
		</div>
	  <div class="card-footer">
			<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
			<?= Html::a('Cancel', ['site/index'], ['class' => 'btn btn-default']) ?>
		</div>
		<?php ActiveForm::end(); ?>
	</section>
</div>
