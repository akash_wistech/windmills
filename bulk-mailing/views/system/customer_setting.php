<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\assets\SettingsAssets;
SettingsAssets::register($this);

/* @var $this yii\web\View */
$predefinedListArr=Yii::$app->appHelperFunctions->predefinedListArr;
$dateTimeZones = Yii::$app->helperFunctions->dateTimeZones;

$this->title = 'Settings';
?>
<div class="system-setting">
	<section class="update-profile-form card card-outline card-primary">
		<header class="card-header">
			<h2 class="card-title"><?= $this->title?></h2>
		</header>
		<?php $form = ActiveForm::begin(); ?>
		<div class="card-body">
			<div class="row">
				<div class="col-sm-4">
					<?= $form->field($model, 'time_zone')->dropDownList($dateTimeZones,['prompt'=>'Select']) ?>
				</div>
			</div>
		</div>
	  <div class="card-footer">
			<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
			<?= Html::a('Cancel', ['site/index'], ['class' => 'btn btn-default']) ?>
		</div>
		<?php ActiveForm::end(); ?>
	</section>
</div>
