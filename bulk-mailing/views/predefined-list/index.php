<?php
use yii\helpers\Html;
use app\components\widgets\CustomGridView;
use app\components\widgets\CustomPjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PredefinedListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Predefined Lists');
if($searchModel->parent>0){

}
$this->params['breadcrumbs'][] = $this->title;

$actionBtns='';
if(Yii::$app->menuHelperFunction->checkActionAllowed('update')){
  $actionBtns.='{update}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('status')){
  $actionBtns.='{status}';
}
if(Yii::$app->menuHelperFunction->checkActionAllowed('delete')){
  $actionBtns.='{delete}';
}
?>
<div class="predefined-list-index">
  <?php CustomPjax::begin(['id'=>'grid-container']); ?>
  <?= CustomGridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'createBtn' => true,
    'columns' => [
      ['class' => 'yii\grid\SerialColumn','headerOptions'=>['class'=>'noprint','style'=>'width:15px;']],
      'title',
      ['label'=>'Sub Options','value'=>function($model){
        return Yii::$app->statsFunctions->getPredefinedListSubOptionCount($model['id']);
      },'headerOptions'=>['class'=>'noprint','style'=>'width:100px;']],
      ['format'=>'raw','attribute'=>'status','label'=>Yii::t('app','Status'),'value'=>function($model){
        return Yii::$app->helperFunctions->arrYesNo[$model['status']];
      },'headerOptions'=>['class'=>'noprint','style'=>'width:15px;']],
      [
        'class' => 'yii\grid\ActionColumn',
        'header'=>'',
        'headerOptions'=>['class'=>'noprint','style'=>'width:15px;'],
        'contentOptions'=>['class'=>'noprint'],
        'template' => '
        <div class="btn-group">
          <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu">
            '.$actionBtns.'
          </ul>
        </div>
        ','buttons' => [
            'update' => function ($url, $model) {
              return Html::a('<i class="fas fa-edit"></i> '.Yii::t('app', 'Edit'), $url, [
                'title' => Yii::t('app', 'Edit'),
                'class'=>'dropdown-item text-1',
                'data-pjax'=>"0",
              ]);
            },
            'status' => function ($url, $model) {
              if($model['status']==1){
                return Html::a('<i class="fas fa-eye-slash"></i> '.Yii::t('app', 'Disable'), $url, [
                  'title' => Yii::t('app', 'Disable'),
                  'class'=>'dropdown-item text-1',
                  'data-confirm'=>Yii::t('app','Are you sure you want to disable this item?'),
                  'data-method'=>"post",
                ]);
              }else{
                return Html::a('<i class="fas fa-eye"></i> '.Yii::t('app', 'Enable'), $url, [
                  'title' => Yii::t('app', 'Enable'),
                  'class'=>'dropdown-item text-1',
                  'data-confirm'=>Yii::t('app','Are you sure you want to enable this item?'),
                  'data-method'=>"post",
                ]);
              }
            },
            'delete' => function ($url, $model) {
              return Html::a('<i class="fas fa-trash"></i> '.Yii::t('app', 'Delete'), $url, [
                'title' => Yii::t('app', 'Delete'),
                'class'=>'dropdown-item text-1',
                'data-confirm'=>Yii::t('app','Are you sure you want to delete this item?'),
                'data-method'=>"post",
                'data-pjax'=>"0",
              ]);
            },
        ],
      ],
    ],
  ]);?>
  <?php CustomPjax::end(); ?>
</div>
