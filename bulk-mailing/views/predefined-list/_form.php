<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PredefinedList */
/* @var $form yii\widgets\ActiveForm */
?>
<section class="predefined-form card card-outline card-primary">
  <header class="card-header">
    <h2 class="card-title"><?= $cardTitle?></h2>
  </header>
  <?php $form = ActiveForm::begin(); ?>
  <div class="card-body">
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-xs-12 col-sm-6">
        <?= $form->field($model, 'status')->dropDownList(Yii::$app->helperFunctions->arrYesNo) ?>
      </div>
    </div>
    <?= $form->field($model, 'descp')->textArea(['rows'=>3])?>
  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>
