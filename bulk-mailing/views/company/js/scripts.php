<?php

$this->registerJs('
$("body").on("beforeSubmit", "form#create-company-form", function () {
	var _targetContainer="#general-modal";
	var form = $(this);
	// return false if form still have some validation errors
	if (form.find(".has-error").length) {
		return false;
	}
	$.blockUI({
		message: "'.Yii::t('app','Please wait...').'",
		target: _targetContainer,
		overlayColor: "none",
		cenrerY: true,
		boxed: true
	});
	// submit form
	$.ajax({
		url: form.attr("action"),
		type: "post",
		data: form.serialize(),
		dataType: "json",
		success: function (response) {
			if(response["success"]){
				//toastr.success(response["success"]["msg"], response["success"]["heading"], {timeOut: 5000});
        closeModal();
			}else{
				swal({title: response["error"]["heading"], html: response["error"]["msg"], icon: "error", timer: 5000});
			}
			$.unblockUI($(_targetContainer));
		}
	});
	return false;
});
');
