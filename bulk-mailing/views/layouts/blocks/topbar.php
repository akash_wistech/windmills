<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\NotificationsDropDown;

$notificationCount=Yii::$app->notificationHelper->newCount;
?>
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
    </li>
  </ul>

  <ul class="navbar-nav ml-auto">
    <!--li class="nav-item dropdown">
      <a class="nav-link" data-toggle="dropdown" href="#">
        <i class="far fa-comments"></i>
        <span class="badge badge-danger navbar-badge">3</span>
      </a>
      <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
        <a href="#" class="dropdown-item">
          <div class="media">
            <img src="img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
            <div class="media-body">
              <h3 class="dropdown-item-title">
                Brad Diesel
                <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
              </h3>
              <p class="text-sm">Call me whenever you can...</p>
              <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
            </div>
          </div>
        </a>
        <div class="dropdown-divider"></div>
        <a href="#" class="dropdown-item">
          <div class="media">
            <img src="img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
            <div class="media-body">
              <h3 class="dropdown-item-title">
                John Pierce
                <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
              </h3>
              <p class="text-sm">I got your message bro</p>
              <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
            </div>
          </div>
        </a>
        <div class="dropdown-divider"></div>
        <a href="#" class="dropdown-item">
          <div class="media">
            <img src="img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
            <div class="media-body">
              <h3 class="dropdown-item-title">
                Nora Silvester
                <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
              </h3>
              <p class="text-sm">The subject goes here</p>
              <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
            </div>
          </div>
        </a>
        <div class="dropdown-divider"></div>
        <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
      </div>
    </li-->
    <!--li class="nav-item dropdown">
      <a class="nav-link" data-toggle="dropdown" href="#">
        <i class="far fa-bell"></i>
        <?php if($notificationCount>0){?>
        <span class="badge badge-warning navbar-badge"><?= $notificationCount?></span>
        <?php }?>
      </a>
      <?= '';//NotificationsDropDown::widget()?>
    </li-->
    <li class="nav-item dropdown user-menu">
      <a href="javascript:;" class="nav-link dropdown-toggle" data-toggle="dropdown">
        <img src="<?= Yii::$app->fileHelperFunctions->getImagePath('user',Yii::$app->user->identity->image,'small')?>" class="user-image img-circle elevation-2" alt="<?= Yii::t('app','Profile Image')?>">
        <span class="d-none d-md-inline"><?= Yii::$app->user->identity->name?></span>
      </a>
      <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
        <li class="user-header bg-primary">
          <img src="<?= Yii::$app->fileHelperFunctions->getImagePath('user',Yii::$app->user->identity->image,'small')?>" class="img-circle elevation-2" alt="<?= Yii::t('app','Profile Image')?>">
          <p>
            <?= Yii::$app->user->identity->name;//.(Yii::$app->user->identity->profileInfo!=null && Yii::$app->user->identity->profileInfo->jobTitle!=null ? '<small>'.Yii::$app->user->identity->profileInfo->jobTitle->title.'</small>' : '')?>
          </p>
        </li>
        <li class="user-footer">
          <a href="<?= Url::to(['account/update-profile'])?>" class="btn btn-default btn-flat"><?= Yii::T('app','Update Profile')?></a>
          <a href="<?= Url::to(['account/change-password'])?>" class="btn btn-default btn-flat float-right"><?= Yii::t('app','Change Password')?></a>
        </li>
      </ul>
    </li>
</ul>
</nav>
