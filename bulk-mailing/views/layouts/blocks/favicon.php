<?php
use yii\helpers\Url;
?>
<link rel="shortcut icon" href="<?= Url::base();?>/images/favicon/favicon.ico" type="image/x-icon">
<link rel="icon" href="<?= Url::base();?>/images/favicon/favicon.ico" type="image/x-icon">
<base href="<?= Url::base();?>/">
