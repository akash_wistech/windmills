<?php
use yii\helpers\Html;
use yii\helpers\Url;
$companyName = Yii::$app->params['companyName'];
if(Yii::$app->user->identity->company!=null){
  $companyName=Yii::$app->user->identity->company->title;
}
?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="<?= Url::to(['site/index'])?>" class="brand-link">
      <img src="img/AdminLTELogo.png"
      alt="<?= $companyName?>"
      class="brand-image img-circle elevation-3"
      style="opacity: .8">
      <span class="brand-text font-weight-light"><?= $companyName?></span>
    </a>

    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?= Yii::$app->fileHelperFunctions->getImagePath('user',Yii::$app->user->identity->image,'small')?>" class="img-circle elevation-2" alt="<?= Yii::t('app','Profile Image')?>">
        </div>
        <div class="info">
          <a href="<?= Url::to(['site/index'])?>" class="d-block"><?= Yii::$app->user->identity->name?></a>
        </div>
      </div>

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

          <li class="nav-item">
            <a href="<?= Url::to(['site/index'])?>" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                <?= Yii::t('app','Dashboard')?>
              </p>
            </a>
          </li>
          <?= Yii::$app->menuHelperFunction->generateStaffMenu?>
          <li class="nav-item">
            <a href="<?= Url::to(['site/logout'])?>" data-method="post" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                <?= Yii::t('app','Logout')?>
              </p>
            </a>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
