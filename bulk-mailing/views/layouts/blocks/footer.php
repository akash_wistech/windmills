<?php
use app\components\widgets\CustomModal;

?>
<footer class="main-footer">
  <strong>Copyright &copy; <?= date("Y")?> <?= Yii::$app->params['companyName']?>.</strong> All rights reserved.
</footer>
<?php
$headerHtml ='<h5 class="modal-title"></h5>';
$headerHtml.='<button type="button" class="close" data-dismiss="modal" aria-label="Close">';
$headerHtml.='  <span aria-hidden="true">×</span>';
$headerHtml.='</button>';
CustomModal::begin([
  'headerOptions' => ['class' => 'modal-header'],
  'id' => 'general-modal',
  'size' => 'modal-lg',
 	'header' => $headerHtml,
  'closeButton' => false,
]);
echo "<div class='modalContent'></div>";
CustomModal::end();
CustomModal::begin([
  'headerOptions' => ['class' => 'modal-header'],
  'id' => 'general-modal-secondary',
  'size' => 'modal-lg',
 	'header' => $headerHtml,
  'closeButton' => false,
]);
echo "<div class='modalContent'></div>";
CustomModal::end();
?>
