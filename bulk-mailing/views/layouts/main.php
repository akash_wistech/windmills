<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\components\widgets\ToasterAlert;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\AdminLteBreadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

$companyName = Yii::$app->params['appName'];
if(Yii::$app->user->identity->company!=null){
  $companyName = Yii::$app->user->identity->company->title;
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
  <meta charset="<?= Yii::$app->charset ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php $this->registerCsrfMetaTags() ?>
  <title><?= Html::encode($this->title.' - '.$companyName) ?></title>
  <?= $this->render('blocks/favicon')?>
  <?php $this->head() ?>
  <base href="/">
</head>
<body class="hold-transition sidebar-mini">
  <?php $this->beginBody() ?>

  <div class="wrapper">
    <?= $this->render('blocks/topbar')?>
    <?= $this->render('blocks/left_menu')?>

    <div class="content-wrapper">
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>
                <?= $this->title?>
                <?php
                if(isset($this->params['titleBtns'])){
                  foreach ($this->params['titleBtns'] as $btn) {
                    echo $btn;
                  }
                }
                if(isset($this->params['availableWorkFlows'])){
                  $processSelectionHtml='';
                  $wfActiveTitle='';
                  foreach ($this->params['availableWorkFlows'] as $workFlow) {
                    if($workFlow['active']==true)$wfActiveTitle=$workFlow['title'];
                    $processSelectionHtml.='<a class="dropdown-item" href="'.Url::to($workFlow['r']).'">'.$workFlow['title'].'</a>';
                  }
                  echo '<div class="btn-group">
                    <button type="button" class="btn btn-success">'.$wfActiveTitle.'</button>
                    <button type="button" class="btn btn-success dropdown-toggle dropdown-hover dropdown-icon" data-toggle="dropdown">
                      <span class="sr-only">Toggle Dropdown</span>
                      <div class="dropdown-menu">
                        '.$processSelectionHtml.'
                      </div>
                    </button>
                  </div>';
                }
                ?>


              </h1>
            </div>
            <div class="col-sm-6">
              <?= AdminLteBreadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]) ?>
            </div>
          </div>
        </div>
      </section>

      <section class="content">
        <?= ToasterAlert::widget() ?>
        <?= $content ?>
      </section>
    </div>

    <?= $this->render('blocks/footer')?>
    <?= $this->render('/shared/js/scripts')?>
  </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
