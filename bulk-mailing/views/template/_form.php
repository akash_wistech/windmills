<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\assets\TemplateFormAsset;
TemplateFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Template */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs('
  tinymce.init({
    selector:".textareaEditor",theme: "modern",
    menubar:false,
    paste_as_text: true,
    directionality : "ltr",
    relative_urls : false,
    remove_script_host : false,
    convert_urls : false,
    plugins: [
      "advlist autolink lists link image charmap print preview anchor",
      "searchreplace visualblocks code fullscreen",
      "insertdatetime media table paste"
    ],
    toolbar: "insertfile undo redo | paste | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image responsivefilemanager code",
    external_filemanager_path:"'.Yii::$app->params['baseDir'].'filemanager/",
    filemanager_title:"Filemanager" ,
    external_plugins: { "filemanager" : "'.Yii::$app->params['baseDir'].'filemanager/plugin.min.js"}
  });
');
$hint =Yii::t('app','Following shortcodes are available.');
$hint.='<br />';
$hint.=Yii::t('app','{content} {userEmail} {userName} {unsubscribeLink}');
?>
<section class="template-form card card-outline card-primary">
  <?php $form = ActiveForm::begin(['id'=>'create-email-template-form']); ?>
  <?php if(isset($cardTitle)){?>
  <header class="card-header">
    <h2 class="card-title"><?= $cardTitle?></h2>
  </header>
  <?php }?>
  <div class="card-body">
    <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
    <?= $form->field($model, 'descp')->textArea(['class'=>'form-control textareaEditor','rows' => 15])->hint($hint)?>
  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>
