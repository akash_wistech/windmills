<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Template */

$this->title = Yii::t('app', 'Templates');
$cardTitle = Yii::t('app','New Template');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="template-create">
    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
