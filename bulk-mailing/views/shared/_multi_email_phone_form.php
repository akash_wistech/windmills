<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomPjax;
if(!isset($sk))$sk='';
$this->registerJs('
$("body").on("click", ".remove-'.$sk.'row", function () {
  $(this).parents(".new-'.$sk.'row").remove();
});
');
?>
<div class="row">
  <?php CustomPjax::begin(['id'=>'rd'.$sk.'emails','options'=>['class'=>'col-sm-6']]); ?>
    <?php
    $en=1;
    $emails=Yii::$app->appHelperFunctions->getModuleEmails($model);
    if($emails!=null){
      foreach($emails as $email){
        $model->emails[$en]=$email['email'];
        $emlABtn = '<button type="button" class="btn btn-success btn-flat" onclick="add'.$sk.'Email()"><i class="fa fa-plus"></i></button>';
        if($en>1){
          $emlABtn = '<button type="button" class="btn btn-danger btn-flat aa-confirm-action" data-url="'.Url::to(['module-action/delete-email','mt'=>$model->moduleTypeId,'mid'=>$model->id,'email'=>$email['email']]).'" data-cmsg="'.Yii::$app->helperFunctions->delConfirmationMsg.'" data-container="rd'.$sk.'emails"><i class="fa fa-trash"></i></button>';
        }
    ?>
    <?= $form->field($model, 'emails['.$en.']',['template'=>'
    {label}
    <div class="input-group input-group-md">
      <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-envelope"></i></span>
      </div>
      {input}
      <div class="input-group-append">
        '.$emlABtn.'
      </div>
    </div>
    {error}{hint}
    '])->textInput(['maxlength' => true])->label(($en>1 ? false : $model->getAttributeLabel('emails')))?>
    <?php
        $en++;
      }
    }
    if($en==1){
    ?>
    <?= $form->field($model, 'emails['.$en.']',['template'=>'
    {label}
    <div class="input-group input-group-md">
      <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-envelope"></i></span>
      </div>
      {input}
      <div class="input-group-append">
        <button type="button" class="btn btn-success btn-flat" onclick="add'.$sk.'Email()"><i class="fa fa-plus"></i></button>
      </div>
    </div>
    {error}{hint}
    '])->textInput(['maxlength' => true])?>
    <?php }?>
  <?php CustomPjax::end(); ?>
  <?php CustomPjax::begin(['id'=>'rd'.$sk.'numbers','options'=>['class'=>'col-sm-6']]); ?>
    <?php
    $nn=1;
    $numbers=Yii::$app->appHelperFunctions->getModuleNumbers($model);
    if($numbers!=null){
      foreach($numbers as $number){
        $model->phone_numbers[$nn]=$number['phone'];
        $nbrABtn = '<button type="button" class="btn btn-success btn-flat" onclick="add'.$sk.'Phone()"><i class="fa fa-plus"></i></button>';
        if($nn>1){
          $nbrABtn = '<button type="button" class="btn btn-danger btn-flat aa-confirm-action" data-url="'.Url::to(['module-action/delete-number','mt'=>$model->moduleTypeId,'mid'=>$model->id,'number'=>$number['phone']]).'" data-cmsg="'.Yii::$app->helperFunctions->delConfirmationMsg.'" data-container="rd'.$sk.'numbers"><i class="fa fa-trash"></i></button>';
        }
    ?>
    <?= $form->field($model, 'phone_numbers['.$nn.']',['template'=>'
    {label}
    <div class="input-group input-group-md">
      <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-phone fa-flip-horizontal"></i></span>
      </div>
      {input}
      <div class="input-group-append">
        '.$nbrABtn.'
      </div>
    </div>
    {error}{hint}
    '])->textInput(['maxlength' => true])->label(($nn>1 ? false : $model->getAttributeLabel('phone_numbers')))?>
    <?php
        $nn++;
      }
    }
    if($nn==1){
    ?>
    <?= $form->field($model, 'phone_numbers['.$nn.']',['template'=>'
    {label}
    <div class="input-group input-group-md">
      <div class="input-group-prepend">
        <span class="input-group-text"><i class="fas fa-phone fa-flip-horizontal"></i></span>
      </div>
      {input}
      <div class="input-group-append">
        <button type="button" class="btn btn-success btn-flat" onclick="add'.$sk.'Phone()"><i class="fa fa-plus"></i></button>
      </div>
    </div>
    {error}{hint}
    '])->textInput(['maxlength' => true])?>
    <?php }?>
  <?php CustomPjax::end(); ?>
</div>
<script>
var eml<?= $sk?>Rows = <?= $en?>;
function add<?= $sk?>Email(){
  eml<?= $sk?>Rows++;
  html ='';
  html+='<div class="new-<?= $sk?>row form-group field-emails-'+eml<?= $sk?>Rows+'">';
  html+='  <div class="input-group input-group-md">';
  html+='    <div class="input-group-prepend">';
  html+='      <span class="input-group-text"><i class="fas fa-envelope"></i></span>';
  html+='    </div>';
  html+='    <input type="text" id="emails-'+eml<?= $sk?>Rows+'" class="form-control" name="<?= $model->baseClassName?>[emails]['+eml<?= $sk?>Rows+']">';
  html+='    <div class="input-group-append">';
  html+='      <button type="button" class="btn btn-danger btn-flat remove-<?= $sk?>row"><i class="fa fa-trash"></i></button>';
  html+='    </div>';
  html+='  </div>';
  html+='</div>';
  $("#rd<?= $sk?>emails").append(html);
}
var nmbr<?= $sk?>Rows = <?= $nn?>;
function add<?= $sk?>Phone(){
  nmbr<?= $sk?>Rows++;
  html ='';
  html+='<div class="new-<?= $sk?>row form-group field-phone_numbers-'+nmbr<?= $sk?>Rows+'">';
  html+='  <div class="input-group input-group-md">';
  html+='    <div class="input-group-prepend">';
  html+='      <span class="input-group-text"><i class="fas fa-phone fa-flip-horizontal"></i></span>';
  html+='    </div>';
  html+='    <input type="text" id="phone_numbers-'+nmbr<?= $sk?>Rows+'" class="form-control" name="<?= $model->baseClassName?>[phone_numbers]['+nmbr<?= $sk?>Rows+']">';
  html+='    <div class="input-group-append">';
  html+='      <button type="button" class="btn btn-danger btn-flat remove-<?= $sk?>row"><i class="fa fa-trash"></i></button>';
  html+='    </div>';
  html+='  </div>';
  html+='</div>';
  $("#rd<?= $sk?>numbers").append(html);
}
</script>
