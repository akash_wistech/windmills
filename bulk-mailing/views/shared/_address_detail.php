<?php
use yii\helpers\Html;
use yii\helpers\Url;

$addresses=Yii::$app->appHelperFunctions->getModuleAddress($model);
?>
<section class="card card-outline card-info">
  <header class="card-header">
    <h2 class="card-title"><?= Yii::t('app','Address')?></h2>
  </header>
  <div class="card-body">
    <?php
    $addRows=1;
    if($addresses!=null){
      foreach($addresses as $address){
    ?>
    <section class="card<?= $address['is_primary']==1 ? ' card-outline card-primary' : ''?>">
      <header class="card-header">
        <h4 class="card-title">
          <?= Yii::t('app','Addres {number}',['number'=>$addRows])?>
        </h4>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse">
            <i class="fas fa-minus"></i>
          </button>
        </div>
      </header>
      <div class="card-body">
        <div class="row">
          <div class="col-sm-6">
            <strong><?= $model->getAttributeLabel('add_country_id')?>:</strong> <?= $address['country_name']?>
          </div>
          <div class="col-sm-6">
            <strong><?= $model->getAttributeLabel('add_zone_id')?>:</strong> <?= $address['zone_name']?>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <strong><?= $model->getAttributeLabel('add_city')?>:</strong> <?= $address['city']?>
          </div>
          <div class="col-sm-6">
            <strong><?= $model->getAttributeLabel('add_phone')?>:</strong> <?= $address['phone']?>
          </div>
        </div>
        <strong><?= $model->getAttributeLabel('add_address')?>:</strong><br /><?= nl2br($address['address'])?>
      </div>
    </section>
    <?php
        $addRows++;
      }
    }
    ?>
  </div>
</section>
