<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="row">
  <div class="col-sm-6">
    <strong><?= Yii::t('app','Emails:')?></strong>
    <?php
    $n=1;
    $emails=Yii::$app->appHelperFunctions->getModuleEmails($model);
    if($emails!=null){
      foreach($emails as $email){
        echo ($n>1 ? ', ' : '').$email['email'];
        $n++;
      }
    }
    ?>
  </div>
  <div class="col-sm-6">
    <strong><?= Yii::t('app','Numbers:')?></strong>
    <?php
    $n=1;
    $numbers=Yii::$app->appHelperFunctions->getModuleNumbers($model);
    if($numbers!=null){
      foreach($numbers as $number){
        echo ($n>1 ? ', ' : '').$number['phone'];
        $n++;
      }
    }
    ?>
  </div>
</div>
