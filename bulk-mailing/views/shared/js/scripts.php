<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerJs('
$("body").on("click", ".aa-confirm-action", function () {
  confirmHeading="'.Yii::t('app','Confirmation').'";
  if($(this).filter(\'[data-cheading]\').length!==0) {
    confirmHeading=$(this).data("cheading");
  }
  containerId=$(this).data("container");

  confirmMsg=$(this).data("cmsg");
  url=$(this).data("url");
  Swal.fire({
    title: confirmHeading,
    html: confirmMsg,
    icon: "info",
    focusConfirm: false,
    showCancelButton: true,
    confirmButtonColor: "#47a447",
    confirmButtonText: "'.Yii::t('app','Confirm').'",
    cancelButtonText: "'.Yii::t('app','Cancel').'",
  }).then((result) => {
    if (result.isConfirmed) {
      App.blockUI({
        message: "'.Yii::t('app','Please wait...').'",
        target: "#"+containerId,
        overlayColor: "none",
        cenrerY: true,
        boxed: true
      });
      $.ajax({
        url: url,
        dataType: "json",
        success: function(response) {
          if(response["success"]){
            toastr.success(response["success"]["msg"]);
            window.closeModal();
          }else{
            Swal.fire({title: response["error"]["heading"], html: response["error"]["msg"], icon: "error", timer: 5000});
          }
          App.unblockUI($("#containerId"));
          if(containerId!=null && containerId!=""){
            $.pjax.reload({container: "#"+containerId, timeout: 2000});
          }
        },
        error: bbAlert
      });
    }
  });
});
');
?>
