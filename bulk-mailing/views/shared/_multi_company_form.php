<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\CustomPjax;
$this->registerJs('
$("body").on("click", ".remove-company", function () {
  $(this).parents(".new-comp-row").remove();
});
$("body").on("keypress", ".autocomplete", function () {
  _t=$(this);
  $(this).autocomplete({
    serviceUrl: _t.data("ds"),
    noCache: true,
    onSelect: function(suggestion) {
      if($("#"+_t.data("fld")).val()!=suggestion.data){
        $("#"+_t.data("fld")).val(suggestion.data);
      }
    },
    onInvalidateSelection: function() {
      $("#"+_t.data("fld")).val("0");
    }
  });
});
');

$companies=Yii::$app->appHelperFunctions->getModuleCompany($model);
?>
<section class="card card-info<?= $companies!=null ? '' : ' collapsed-card'?>">
  <header class="card-header">
    <h2 class="card-title"><?= Yii::t('app','Company Information')?></h2>
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="collapse">
        <i class="fas fa-<?= $companies!=null ? 'minus' : 'plus'?>"></i>
      </button>
    </div>
  </header>
  <div class="card-body"<?= $companies!=null ? '' : ' style="display: none;"'?>>
    <a href="javascript:;" class="btn btn-md btn-primary load-modal" data-url="<?= Url::to(['company/create'])?>" data-heading="<?= Yii::t('app','New Company')?>">
      <?= Yii::t('app','Create New Company')?>
    </a>
    <?php CustomPjax::begin(['id'=>'rdcompanies']); ?>
    <?php
    $n=1;
    if($companies!=null){
      foreach($companies as $company){
        $model->comp_row_id[$n]=$company['id'];
        $model->comp_id[$n]=$company['company_id'];
        $model->comp_name[$n]=$company['company_name'];
        $model->comp_role_id[$n]=$company['role_id'];
        $model->comp_role[$n]=$company['role_name'];
        $model->comp_job_title_id[$n]=$company['job_title_id'];
        $model->comp_job_title[$n]=$company['job_title'];
    ?>
    <div class="row">
      <div class="hidden">
        <?= $form->field($model, 'comp_row_id['.$n.']')->textInput(['id'=>'comp_row_id-'.$n, 'maxlength' => true])?>
        <?= $form->field($model, 'comp_id['.$n.']')->textInput(['id'=>'comp_id-'.$n, 'maxlength' => true])?>
        <?= $form->field($model, 'comp_role_id['.$n.']')->textInput(['id'=>'comp_role_id'.$n, 'maxlength' => true])?>
        <?= $form->field($model, 'comp_job_title_id['.$n.']')->textInput(['id'=>'comp_job_title_id'.$n, 'maxlength' => true, 'autocomplete' => 'off'])?>
      </div>
      <div class="col-sm-5">
        <?= $form->field($model, 'comp_name['.$n.']')->textInput(['data-fld'=>'comp_id-'.$n,'data-ds'=>Url::to(['suggestion/company']),'class'=>'form-control autocomplete','maxlength' => true, 'autocomplete' => 'off'])?>
      </div>
      <div class="col-sm-3">
        <?= $form->field($model, 'comp_role['.$n.']')->textInput(['data-fld'=>'comp_role_id'.$n,'data-ds'=>Url::to(['suggestion/job-roles']),'class'=>'form-control autocomplete','maxlength' => true, 'autocomplete' => 'off'])?>
      </div>
      <div class="col-sm-3">
        <?= $form->field($model, 'comp_job_title['.$n.']')->textInput(['data-fld'=>'comp_job_title_id'.$n,'data-ds'=>Url::to(['suggestion/job-titles']),'class'=>'form-control autocomplete','maxlength' => true])?>
      </div>
      <div class="col-sm-1 text-center">
        <div class="form-group">
          <label class="control-label" style="display:block;">&nbsp;</label>
          <?php if($n>1){?>
          <a href="javascript:;" class="btn btn-danger aa-confirm-action" data-url="<?= Url::to(['module-action/delete-company','mt'=>$model->moduleTypeId,'mid'=>$model->id,'id'=>$company['id']])?>" data-cmsg="<?= Yii::$app->helperFunctions->delConfirmationMsg?>" data-container="rdcompanies">
            <i class="fas fa-trash"></i>
          </a>
        <?php }else{?>
          <a href="javascript:;" class="btn btn-success" onclick="addCompany()">
            <i class="fas fa-plus"></i>
          </a>
        <?php }?>
        </div>
      </div>
    </div>
    <?php
        $n++;
      }
    }
    ?>
    <?php if($n==1){?>
    <div class="row">
      <div class="hidden">
        <?= $form->field($model, 'comp_id['.$n.']')->textInput(['id'=>'comp_id-'.$n, 'maxlength' => true])?>
        <?= $form->field($model, 'comp_role_id['.$n.']')->textInput(['id'=>'comp_role_id'.$n, 'maxlength' => true])?>
        <?= $form->field($model, 'comp_job_title_id['.$n.']')->textInput(['id'=>'comp_job_title_id'.$n, 'maxlength' => true, 'autocomplete' => 'off'])?>
      </div>
      <div class="col-sm-5">
        <?= $form->field($model, 'comp_name['.$n.']')->textInput(['data-fld'=>'comp_id-'.$n,'data-ds'=>Url::to(['suggestion/company']),'class'=>'form-control autocomplete','maxlength' => true, 'autocomplete' => 'off'])?>
      </div>
      <div class="col-sm-3">
        <?= $form->field($model, 'comp_role['.$n.']')->textInput(['data-fld'=>'comp_role_id'.$n,'data-ds'=>Url::to(['suggestion/job-roles']),'class'=>'form-control autocomplete','maxlength' => true, 'autocomplete' => 'off'])?>
      </div>
      <div class="col-sm-3">
        <?= $form->field($model, 'comp_job_title['.$n.']')->textInput(['data-fld'=>'comp_job_title_id'.$n,'data-ds'=>Url::to(['suggestion/job-titles']),'class'=>'form-control autocomplete','maxlength' => true])?>
      </div>
      <div class="col-sm-1 text-center">
        <div class="form-group">
          <label class="control-label" style="display:block;">&nbsp;</label>
          <a href="javascript:;" class="btn btn-success" onclick="addCompany()">
            <i class="fas fa-plus"></i>
          </a>
        </div>
      </div>
    </div>
    <?php }?>
    <?php CustomPjax::end(); ?>
  </div>
</section>
<script>
var compRows = <?= $n?>;
function addCompany(){
  compRows++;
  html = '';
  html+= '<div class="row new-comp-row">';
  html+= '  <div class="hidden">';
  html+= '    <input type="text" id="comp_id-'+compRows+'" class="form-control" name="<?= $model->baseClassName?>[comp_id]['+compRows+']">';
  html+= '    <input type="text" id="comp_role_id-'+compRows+'" class="form-control" name="<?= $model->baseClassName?>[comp_role_id]['+compRows+']">';
  html+= '    <input type="text" id="comp_job_title_id-'+compRows+'" class="form-control" name="<?= $model->baseClassName?>[comp_job_title_id]['+compRows+']">';
  html+= '  </div>';
  html+= '  <div class="col-sm-5">';
  html+= '    <div class="form-group field-comp_name-'+compRows+'">';
  html+= '      <label class="control-label" for="comp_name-'+compRows+'"><?= $model->getAttributeLabel('comp_name')?></label>';
  html+= '      <input type="text" id="comp_name-'+compRows+'" data-fld="comp_id-'+compRows+'" data-ds="<?= Url::to(['suggestion/company'])?>" class="form-control autocomplete" name="<?= $model->baseClassName?>[comp_name]['+compRows+']" autocomplete="off">';
  html+= '    </div>';
  html+= '  </div>';
  html+= '  <div class="col-sm-3">';
  html+= '    <div class="form-group field-comp_role-'+compRows+'">';
  html+= '      <label class="control-label" for="comp_role-'+compRows+'"><?= $model->getAttributeLabel('comp_role')?></label>';
  html+= '      <input type="text" id="comp_role-'+compRows+'" data-fld="comp_role_id-'+compRows+'" data-ds="<?= Url::to(['suggestion/job-roles'])?>" class="form-control autocomplete" name="<?= $model->baseClassName?>[comp_role]['+compRows+']" autocomplete="off">';
  html+= '    </div>';
  html+= '  </div>';
  html+= '  <div class="col-sm-3">';
  html+= '    <div class="form-group field-comp_job_title-'+compRows+'">';
  html+= '      <label class="control-label" for="comp_job_title-'+compRows+'"><?= $model->getAttributeLabel('comp_job_title')?></label>';
  html+= '      <input type="text" id="comp_job_title-'+compRows+'" data-fld="comp_job_title_id-'+compRows+'" data-ds="<?= Url::to(['suggestion/job-titles'])?>" class="form-control autocomplete" name="<?= $model->baseClassName?>[comp_job_title]['+compRows+']" autocomplete="off">';
  html+= '    </div>';
  html+= '  </div>';
  html+= '  <div class="col-sm-1 text-center">';
  html+= '    <div class="form-group">';
  html+= '      <label class="control-label" style="display:block;">&nbsp;</label>';
  html+= '      <a href="javascript:;" class="btn btn-danger remove-company">';
  html+= '        <i class="fas fa-trash"></i>';
  html+= '      </a>';
  html+= '    </div>';
  html+= '  </div>';
  html+= '</div>';
  $("#rdcompanies").append(html);
}
</script>
