<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = Yii::t('app', 'Staff Members');
$cardTitle = Yii::t('app','New Staff Member');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;

if(Yii::$app->user->identity->user_type==0){
  $viewForm='/user/customer_staff_form';
}else{
  $viewForm='/user/staff_form';
}
?>
<div class="user-create">
    <?= $this->render($viewForm, [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
