<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EmailSmtpDetails */

$this->title = Yii::t('app', 'SMTP Servers');
$cardTitle = Yii::t('app','New SMTP Server');
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = $cardTitle;
?>
<div class="smtp-info-create">
    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
