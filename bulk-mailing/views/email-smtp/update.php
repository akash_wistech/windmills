<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmailSmtpDetails */

$this->title = Yii::t('app', 'SMTP Servers');
$cardTitle = Yii::t('app','Update SMTP Server:  {nameAttribute}', [
    'nameAttribute' => $model->smtp_host,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="smtp-info-update">
    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
