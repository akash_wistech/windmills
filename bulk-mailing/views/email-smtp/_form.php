<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmailSmtpDetails */
/* @var $form yii\widgets\ActiveForm */

$company_id = Yii::$app->user->identity->company_id;
$defaultSmtpId = Yii::$app->appHelperFunctions->getCustomerSetting($company_id,'default_smtp');

if($model->id==$defaultSmtpId){
  $model->is_default = 1;
}else{
  $model->is_default = 0;
}

$txtJs='smtpTrackingOpts='.json_encode(Yii::$app->appHelperFunctions->smtpServiceTypesTrackingOptsArr).';';
$this->registerJs($txtJs.'
$("body").on("change", "#emailsmtpdetails-service_type", function () {
  $("#trackingInfo").html(smtpTrackingOpts[$(this).val()]);
});
');
?>
<section class="smtp-info-form card card-outline card-primary">
  <?php $form = ActiveForm::begin(['id'=>'create-email-template-form']); ?>
  <header class="card-header">
    <h2 class="card-title"><?= $cardTitle?></h2>
  </header>
  <div class="card-body">
    <div class="row">
      <div class="col-12 col-sm-6">
        <?= $form->field($model, 'service_type')->dropDownList(Yii::$app->appHelperFunctions->smtpServiceTypesArr,['prompt'=>Yii::t('app','Select')])?>
      </div>
      <div class="col-12 col-sm-6">
        <?= $form->field($model, 'smtp_host')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-6">
        <?= $form->field($model, 'smtp_username')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-12 col-sm-6">
        <?= $form->field($model, 'smtp_password')->textInput(['maxlength' => true])?>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-6">
        <?= $form->field($model, 'smtp_port')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-12 col-sm-6">
        <?= $form->field($model, 'smtp_encryption')->dropDownList(Yii::$app->appHelperFunctions->smtpServiceEncryptionTypesArr)?>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-6">
        <?= $form->field($model, 'mail_from')->textInput(['maxlength' => true])?>
      </div>
      <div class="col-12 col-sm-6">
        <?= $form->field($model, 'is_default')->dropDownList(Yii::$app->helperFunctions->arrYesNo)?>
      </div>
    </div>
    <div class="alert alert-info">
      <strong><?= Yii::t('app','Tracking Url:')?></strong>
      <br />
      <span id="trackingInfo">
        <?= $model->id!=null ? Yii::$app->appHelperFunctions->smtpServiceTypesTrackingOptsArr[$model->service_type] : 'Select type'?>
      </span>
    </div>
  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>
