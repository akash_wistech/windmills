<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AdminGroup */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs('
$("body").on("click", ".cbl1", function () {
  thisid=$(this).attr("value");
  $(".cbl1_"+thisid).prop("checked",$(this).prop("checked"));
});
$("body").on("click", ".cbl2", function () {
  thisid=$(this).attr("value");
  $(".cbl2_"+thisid).prop("checked",$(this).prop("checked"));
});
$("body").on("click", ".cbl3", function () {
  thisid=$(this).attr("value");
  $(".cbl3_"+thisid).prop("checked",$(this).prop("checked"));
});
$("body").on("click", ".selAll", function () {
  thisid=$(this).data("id");
  $(".cbl1_"+thisid).prop("checked",true);
});
$("body").on("click", ".unselAll", function () {
  thisid=$(this).data("id");
  $(".cbl1_"+thisid).prop("checked",false);
});
');

$menuLinks=Yii::$app->menuHelperFunction->adminMenuList;
$arrListType=Yii::$app->helperFunctions->permissionListingType;
?>
<style>
.admin-group-form .card-header label{margin-bottom: 0px;}
.admin-group-form .card .card-header .card-title{padding-top: 7px;}
.admin-group-form .card-header input{margin-right: 5px;}
.admin-group-form .card .card-header .card-tools .form-group{margin-bottom: 0px;}
.admin-group-form .card-body{padding: .75rem .75rem .35rem;}
.admin-group-form .card .card-body label{margin-right: 20px;}
</style>
<section class="admin-group-form card card-outline card-primary">
  <?php $form = ActiveForm::begin(); ?>
  <header class="card-header">
    <h2 class="card-title"><?= $cardTitle?></h2>
  </header>
  <div class="card-body">
    <?= $form->field($model, 'title')->textInput(['maxlength' => true])?>
    <div class="row">
      <?php
      $n=1;
      foreach($menuLinks as $menuLink){
        $boxSelected=Yii::$app->menuHelperFunction->isChecked($model->id,$menuLink['id']);
        $model->list_type[$menuLink['id']]=Yii::$app->menuHelperFunction->getListingType($model->id,$menuLink['id']);
        ?>
        <div class="col-sm-6">
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">
                <label><input type="checkbox"<?= ($boxSelected==true ? ' checked="checked"' : '')?> class="cbl1" name="AdminGroup[actions][]" value="<?= $menuLink['id']?>" /> <?= $menuLink['title']?></label>
              </h3>
              <?php if($menuLink['ask_list_type']==1 && $menuLink['action_id']=='index'){?>
              <div class="card-tools">
                <?= $form->field($model, 'list_type['.$menuLink['id'].']')->dropDownList($arrListType,['class'=>'form-control form-control-sm'])->label(false) ?>
              </div>
              <?php }?>
            </div>
            <div class="card-body">
              <?php
              $secondLevelOptions=Yii::$app->menuHelperFunction->getSubOptions($menuLink['id']);
              if($secondLevelOptions!=null){
                foreach($secondLevelOptions as $secondLevelOption){
                  $boxSelected=Yii::$app->menuHelperFunction->isChecked($model->id,$secondLevelOption['id']);
                  $thirdLevelOptions=Yii::$app->menuHelperFunction->getSubOptions($secondLevelOption['id']);
                  if($thirdLevelOptions!=null){
                    $model->list_type[$secondLevelOption['id']]=Yii::$app->menuHelperFunction->getListingType($model->id,$secondLevelOption['id']);
                  ?>
                  <div class="card card-success">
                    <div class="card-header">
                      <h3 class="card-title">
                        <label><input type="checkbox"<?= ($boxSelected==true ? ' checked="checked"' : '')?> class="cbl2 cbl1_<?= $menuLink['id']?>" name="AdminGroup[actions][]" value="<?= $secondLevelOption['id']?>" /> <?= $secondLevelOption['title']?></label>
                      </h3>
                      <?php if($secondLevelOption['ask_list_type']==1 && $secondLevelOption['action_id']=='index'){?>
                      <div class="card-tools">
                        <?= $form->field($model, 'list_type['.$secondLevelOption['id'].']')->dropDownList($arrListType,['class'=>'form-control form-control-sm'])->label(false) ?>
                      </div>
                      <?php }?>
                    </div>
                    <div class="card-body">
                      <?php
                        foreach($thirdLevelOptions as $thirdLevelOption){
                          $boxSelected=Yii::$app->menuHelperFunction->isChecked($model->id,$thirdLevelOption['id']);
                          $fourthLevelOptions=Yii::$app->menuHelperFunction->getSubOptions($thirdLevelOption['id']);
                          if($fourthLevelOptions!=null){
                            $model->list_type[$thirdLevelOption['id']]=Yii::$app->menuHelperFunction->getListingType($model->id,$thirdLevelOption['id']);
                          ?>
                          <div class="card card-warning">
                            <div class="card-header">
                              <h3 class="card-title">
                                <label>
                                  <input type="checkbox"<?= ($boxSelected==true ? ' checked="checked"' : '')?> class="cbl3 cbl1_<?= $menuLink['id']?> cbl2_<?= $secondLevelOption['id']?>" name="AdminGroup[actions][]" value="<?= $thirdLevelOption['id']?>" />
                                  <?= $thirdLevelOption['title']?>
                                </label>
                              </h3>
                              <?php if($thirdLevelOption['ask_list_type']==1 && $thirdLevelOption['action_id']=='index'){?>
                              <div class="card-tools">
                                <?= $form->field($model, 'list_type['.$thirdLevelOption['id'].']')->dropDownList($arrListType,['class'=>'form-control form-control-sm'])->label(false) ?>
                              </div>
                              <?php }?>
                            </div>
                            <div class="card-body">
                              <?php
                                foreach($fourthLevelOptions as $fourthLevelOption){
                                  $boxSelected=Yii::$app->menuHelperFunction->isChecked($model->id,$fourthLevelOption['id']);
                                  ?>
                                  <label>
                                    <input type="checkbox"<?= ($boxSelected==true ? ' checked="checked"' : '')?> class="cbl1_<?= $menuLink['id']?> cbl2_<?= $secondLevelOption['id']?> cbl3_<?= $thirdLevelOption['id']?>" data-parent="" name="AdminGroup[actions][]" value="<?= $fourthLevelOption['id']?>" />
                                    <?= $fourthLevelOption['title']?>
                                  </label>
                                  <?php
                                }
                            ?>
                            </div>
                          </div>
                          <?php
                          }else{
                            ?>
                            <label>
                              <input type="checkbox"<?= ($boxSelected==true ? ' checked="checked"' : '')?> class="cbl1_<?= $menuLink['id']?> cbl2_<?= $secondLevelOption['id']?>" data-parent="" name="AdminGroup[actions][]" value="<?= $thirdLevelOption['id']?>" />
                              <?= $thirdLevelOption['title']?>
                            </label>
                            <?php
                          }
                        }
                      ?>
                    </div>
                  </div>
                  <?php
                  }else{
                    ?>
                    <label><input type="checkbox"<?= ($boxSelected==true ? ' checked="checked"' : '')?> class="cbl2 cbl1_<?= $menuLink['id']?>" name="AdminGroup[actions][]" value="<?= $secondLevelOption['id']?>" /> <?= $secondLevelOption['title']?></label>
                    <?php
                  }
                }
              }
              ?>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>
