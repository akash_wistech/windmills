<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AdminGroup */

$this->title = Yii::t('app', 'Permission Groups');
$cardTitle = Yii::t('app','Update Group:  {nameAttribute}', [
    'nameAttribute' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="admin-group-update">
    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
