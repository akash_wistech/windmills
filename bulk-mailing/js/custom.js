jQuery(document).ready(function() {
	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover();
	$("body").on("click", ".load-modal", function () {
		modalId="general-modal";
		if($(this).filter('[data-mid]').length!==0) {
			modalId=$(this).data("mid");
		}

		url=$(this).data("url");
		heading=$(this).data("heading");
		$.ajax({
			url: url,
			dataType: "html",
			success: function(data) {
				$("#"+modalId).find("h5.modal-title").html(heading);
				$("#"+modalId).find(".modalContent").html(data);
				$("#"+modalId).modal();
			},
			error: bbAlert
		});
	});
	$("body").on("click", ".load-list-modal", function () {
    _t=$(this);

    modalId="general-modal";
    if(_t.filter('[data-mid]').length!==0) {
      modalId=_t.data("mid");
    }
    medule_type=_t.data("mtype");
    module_ids=[];
    $.each($("input[name='selection[]']:checked"), function(){
      module_ids.push($(this).val());
    });
		if (module_ids.length === 0) {
			//swal.fire("Please select rows");
			//return false;
		}

    url=_t.data("url");
    heading=_t.data("heading");
    $.ajax({
      url: url,
      dataType: "html",
      success: function(response) {
        $("#"+modalId).find("h5.modal-title").html(heading);
        $("#"+modalId).find(".modalContent").html(response);
        $("#"+modalId).modal();
        $("#sel-module-type").val(medule_type);
        $("#sel-module-ids").val(module_ids.join(","));
      },
      error: bbAlert
    });
  });
	$("body").on("change", ".custom-file-input", function () {
		readImageURL(this,$(this));
	});
	$(".select-on-check-all").click(function(){
		if(this.checked===true){
			$('input[name="selection[]"]:checkbox').prop('checked', true);
		}else{
			$('input[name="selection[]"]:checkbox').prop('checked', false);
		}
	});
});
function bbAlert(xhr, ajaxOptions, thrownError){
	Swal.fire(thrownError,xhr.statusText + "\n" + xhr.responseText);
	$(".blockUI").remove();
}
window.closeModal = function()
{
    $('.modal').modal('hide');
};
function nl2br (str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}
function readImageURL(input,el) {
	console.log(el);
	console.log(input);
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function(e) {
			if($(el).parents(".form-group").find(".input-group-text img").length>0){
				$(el).parents(".form-group").find(".input-group-text img").attr("src", e.target.result);
			}
		}
		reader.readAsDataURL(input.files[0]);
		$(el).parents(".form-group").find(".input-group label").html(input.files[0].name);
	}
}
function startBackgrounTask(url,_targetContainer)
{
	$.ajax({
	  url: url,
	  type: "POST",
	  success: function(response) {
	    response = jQuery.parseJSON(response);
	    if(response["error"]!=null && response["error"]!=''){
	      App.unblockUI($(_targetContainer));
				if(response["error"]["closeWindow"]!=null && response["error"]["closeWindow"]!=''){
					window.closeModal();
				}
	      Swal.fire({title: response["error"]["heading"], html: response["error"]["msg"], icon: "error", timer: 5000});
	    }
	    if(response["success"]!=null && response["success"]!=''){
	      App.unblockUI($(_targetContainer));
	      if(response["success"]["reloadContainer"]!=null && response["success"]["reloadContainer"]!=''){
					$.pjax.reload({container: response["success"]["reloadContainer"], timeout: 2000});
				}
				if(response["success"]["msg"]!=null && response["success"]["msg"]!=''){
					Swal.fire({title: response["success"]["heading"], html: response["success"]["msg"], icon: "success", timer: 5000});
				}
				if(response["success"]["closeWindow"]!=null && response["success"]["closeWindow"]!=''){
					window.closeModal();
				}
	    }
	    if(response["progress"]!=null && response["progress"]!=""){
	      move(response["progress"]["percentage"]);
	      startBackgrounTask(response["progress"]["url"],_targetContainer);
	    }
	  },
	  error: bbAlert
	});
}
function move(width)
{
	if($("#myBar").length>0){
		var elem = document.getElementById("myBar");
		elem.style.width = width + '%';
		if(width>10){
			//elem.innerHTML = width * 1 + '%';
		}
	}
}
var App = function() {
	var isRTL = false;
    return {
        // wrMetronicer function to  block element(indicate loading)
        blockUI: function(options) {
            options = $.extend(true, {}, options);
            var html = '';
            if (options.animate) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '">' + '<div class="block-spinner-bar"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>' + '</div>';
            } else if (options.iconOnly) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><img src="images/loading-spinner-grey.gif" align=""></div>';
            } else if (options.textOnly) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><span>&nbsp;&nbsp;' + (options.message ? options.message : '...') + '</span></div>';
            } else {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><img src="images/loading-spinner-grey.gif" align=""><span>&nbsp;&nbsp;' + (options.message ? options.message : '...') + '</span></div>';
            }

            if (options.target) { // element blocking
                var el = $(options.target);
                if (el.height() <= ($(window).height())) {
                    options.cenrerY = true;
                }
                el.block({
                    message: html,
                    baseZ: options.zIndex ? options.zIndex : 1000,
                    centerY: options.cenrerY !== undefined ? options.cenrerY : false,
                    css: {
                        top: '10%',
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none'
                    },
                    overlayCSS: {
                        backgroundColor: options.overlayColor ? options.overlayColor : '#555',
                        opacity: options.boxed ? 0.05 : 0.1,
                        cursor: 'wait'
                    }
                });
            } else { // page blocking
                $.blockUI({
                    message: html,
                    baseZ: options.zIndex ? options.zIndex : 1000,
                    css: {
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none'
                    },
                    overlayCSS: {
                        backgroundColor: options.overlayColor ? options.overlayColor : '#555',
                        opacity: options.boxed ? 0.05 : 0.1,
                        cursor: 'wait'
                    }
                });
            }
        },
				blockUIProgress: function(options) {
            options = $.extend(true, {}, options);
						direction=options.dir ? options.dir : '';
            var html = '';
						html+='<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '" style="direction:'+direction+'">';
						html+='	<span><img src="images/loading-spinner-grey.gif" align=""><span>&nbsp;&nbsp;' + (options.message ? options.message : $('#loadingMsg').text()) + '</span>';
						html+='	<div id="myProgress">';
						html+='		<div id="myBar"></div>';
						html+='	</div>';
						html+='</div>';

            if (options.target) { // element blocking
                var el = $(options.target);
                if (el.height() <= ($(window).height())) {
                    options.cenrerY = true;
                }
                el.block({
                    message: html,
                    baseZ: options.zIndex ? options.zIndex : 1000,
                    centerY: options.cenrerY !== undefined ? options.cenrerY : false,
                    css: {
                        top: '10%',
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none'
                    },
                    overlayCSS: {
                        backgroundColor: options.overlayColor ? options.overlayColor : '#555',
                        opacity: options.boxed ? 0.05 : 0.1,
                        cursor: 'wait'
                    }
                });
            } else { // page blocking
                $.blockUI({
                    message: html,
                    baseZ: options.zIndex ? options.zIndex : 1000,
                    css: {
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none'
                    },
                    overlayCSS: {
                        backgroundColor: options.overlayColor ? options.overlayColor : '#555',
                        opacity: options.boxed ? 0.05 : 0.1,
                        cursor: 'wait'
                    }
                });
            }
        },
        // wrMetronicer function to  un-block element(finish loading)
        unblockUI: function(target) {
            if (target) {
                $(target).unblock({
                    onUnblock: function() {
                        $(target).css('position', '');
                        $(target).css('zoom', '');
                    }
                });
            } else {
                $.unblockUI();
            }
        },
    };
}();

function moveProgressBar(width)
{
    var elem = document.getElementById("myBar");
    elem.style.width = width + '%';
}
