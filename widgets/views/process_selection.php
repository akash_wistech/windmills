<?php
use yii\helpers\ArrayHelper;
use app\models\WorkflowDataItem;
$this->registerJs('
'.$workflowListStagesArr.'
$("#workflowsel").change(function() {
	$("#workflowstagesel").html("");
	if($(this).val()!=""){
		serviceType = $("#moduleServiceType").val();
		if(serviceType!=""){
			array_list=workflowStages[$(this).val()]["services"][serviceType]["stages"];
			$.each( array_list, function( key, value ) {
				$("#workflowstagesel").append("<option value=\""+value["id"]+"\">"+value["title"]+"</option>");
			});
		}else{
			toastr.error("'.Yii::t('app','Please select service type').'","'.Yii::t('app','Error').'",5000)
		}
	}
});
if($("#moduleServiceType").length>0){
	$("#moduleServiceType").change(function() {
		$("#workflowstagesel").html("");
		workflowId = $("#workflowsel").val();
		if(workflowId!=""){
			array_list=workflowStages[workflowId]["services"][$(this).val()]["stages"];
			$.each( array_list, function( key, value ) {
		    $("#workflowstagesel").append("<option value=\""+value["id"]+"\">"+value["title"]+"</option>");
		  });
		}
	});
}
');
$defStages=[];
$defStagesOpts=['prompt'=>Yii::t('app','Select'),'id'=>'workflowstagesel'];
$workflowRow=WorkflowDataItem::find()->where(['module_type'=>$model->moduleTypeId,'module_id'=>$model->id])->asArray()->one();
if($workflowRow!=null){
  $model->workflow_id=$workflowRow['workflow_id'];
  $model->workflow_stage_id=$workflowRow['workflow_stage_id'];
  $defStages=Yii::$app->appHelperFunctions->getWorkflowStagesList($model->workflow_id);
  $defStages=ArrayHelper::map($defStages,"id","title");
  $defStagesOpts=['id'=>'workflowstagesel'];
}
?>
<section class="card card-warning">
  <header class="card-header">
    <h2 class="card-title"><?= Yii::t('app','Process')?></h2>
  </header>
  <div class="card-body">
    <div class="row">
      <div class="col-sm-6">
        <?= $form->field($model, 'workflow_id')->dropDownList($workflowLists,['prompt'=>Yii::t('app','Select'),'id'=>'workflowsel'])?>
      </div>
      <div class="col-sm-6">
        <?= $form->field($model, 'workflow_stage_id')->dropDownList($defStages,$defStagesOpts)?>
      </div>
    </div>
  </div>
</section>
