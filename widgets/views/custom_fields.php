<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\CustomFieldsAsset;
CustomFieldsAsset::register($this);
$this->registerJs('
initCustomFields();
');
?>
<?php if($mixed==false){?>
<section class="card card-info">
	<header class="card-header">
		<h2 class="card-title"><?= Yii::t('app','Custom Fields')?></h2>
	</header>
	<div class="card-body">
		<div class="row">
<?php }?>
		<?php
		if($inputFields!=null){
			foreach($inputFields as $inputField){
				//$savedInfo=
				Yii::$app->inputHelperFunctions->getInputFielSavedValue($model,$inputField);
		    //$model->input_field[$inputField['id']]=$savedInfo;

				$thisFieldId='input_field_'.$inputField['id'];
				$inputOpts['id']=$thisFieldId;
			  $hintHtml='';
			  if($inputField['hint_text']!=''){
			    $hintHtml =' ';
			    $hintHtml.='<a href="javascript:;" class="blue-hint" data-toggle="popover" data-trigger="hover" data-placement="top" title="'.$inputField['title'].'" data-content="'.$inputField['hint_text'].'">';
			    $hintHtml.='	<i class="fa fa-info-circle"></i>';
			    $hintHtml.='</a>';
			  }
		?>
		<div class="col-sm-<?= $inputField['colum_width']?>">
			<?php
				//Text, Qty, Number, Website, Email, Date, Date Range, Time
				if(
					$inputField['input_type']=='text' ||
			    $inputField['input_type']=='qtyField' ||
			    $inputField['input_type']=='numberinput' ||
			    $inputField['input_type']=='websiteinput' ||
			    $inputField['input_type']=='emailinput' ||
			    $inputField['input_type']=='date' ||
			    $inputField['input_type']=='daterange' ||
			    $inputField['input_type']=='time'
				){
					$clsNames='form-control';
					$inputOpts['maxlength']=true;
					if($inputField['input_type']=='date' || $inputField['input_type']=='daterange' || $inputField['input_type']=='time'){
						$clsNames.=' input'.$inputField['input_type'].'picker';
					}
					if($inputField['autocomplete_text']==1){
						$this->registerJs('
						$("#'.$thisFieldId.'").autocomplete({
							serviceUrl: "'.Url::to(['suggestion/input','fid'=>$inputField['id']]).'",
							noCache: true,
							formatResult: function(suggestion, currentValue) {
								return ""+suggestion.value+"";
							}
						});
						');
					}
					$inputOpts['autocomplete'] = 'off';
					if($inputField['input_type']=='qtyField' || $inputField['input_type']=='numberinput')$clsNames.=' numeric-field';
					$inputOpts['class']=$clsNames;

					$template=[];
					if($inputField['input_type']=='date'){
						$template=['template'=>'
						{label}
						<div class="input-group date" id="input_field-dtinput'.$inputField['id'].'" data-target-input="nearest">
							{input}
							<div class="input-group-append" data-target="#input_field-dtinput'.$inputField['id'].'" data-toggle="datetimepicker">
									<div class="input-group-text"><i class="fa fa-calendar"></i></div>
							</div>
						</div>
						{hint}{error}
						'];
						$this->registerJs('
						$("#input_field-dtinput'.$inputField['id'].'").datetimepicker({
						  allowInputToggle: true,
						  viewMode: "days",
						  format: "YYYY-MM-DD",
						});
						');
					}
				?>
				<?= $form->field($model, $inputFieldName.'['.$inputField['id'].']',$template)->textInput($inputOpts)->label($inputField['title'].$hintHtml)?>
			<?php
				}
				//Time Range
				elseif($inputField['input_type']=='timerange'){
					$this->registerJs('
				    $("#input_field_tr_st_'.$inputField['id'].',#input_field_tr_et_'.$inputField['id'].'").timepicker({"timeFormat":"h:i A","step":15,"showDuration": true});
				    var pairTimeRangeEl'.$inputField['id'].' = document.getElementById("timeRange'.$inputField['id'].'");
				    var datepair = new Datepair(pairTimeRangeEl'.$inputField['id'].');
			    ');
			?>
				<div id="timeRange<?= $inputField['id']?>" class="row">
					<div class="col-md-6">
						<?= $form->field($model, $inputFieldName.'['.$inputField['id'].'][start]')->textInput(['id'=>'input_field_tr_st_'.$inputField['id'],'class'=>'form-control time start','maxlength'=>true,'autocomplete'=>'off'])->label(Yii::t('app','Start Time').$hintHtml)?>
					</div>
					<div class="col-md-6">
						<?= $form->field($model, $inputFieldName.'['.$inputField['id'].'][end]')->textInput(['id'=>'input_field_tr_et_'.$inputField['id'],'class'=>'form-control time start','maxlength'=>true,'autocomplete'=>'off'])->label(Yii::t('app','End Time').$hintHtml)?>
					</div>
				</div>
			<?php
				}
				//Auto Complete
				elseif($inputField['input_type']=='autocomplete'){
					$inputOpts['maxlength']=true;
					$inputOpts['autocomplete'] = 'off';
					$inputOpts['class'] = 'form-control autocomplete';
					$this->registerJs('
			    $("#'.$thisFieldId.'").autocomplete({
			      serviceUrl: "'.Url::to(['suggestion/predefined-list','lid'=>$inputField['autocomplete_link']]).'",
			      noCache: true,
			      formatResult: function(suggestion, currentValue) {
			        return ""+suggestion.value+"<br><small style=\"font-size:78%;\">"+suggestion.descp+"</small>";
			      },
			      onSelect: function(suggestion) {
			        if($("#input_field_id_'.$inputField['id'].'").val()!=suggestion.data){
			          $("#input_field_id_'.$inputField['id'].'").val(suggestion.data);
			        }
			      },
			      onInvalidateSelection: function() {
			        $("#input_field_id_'.$inputField['id'].'").val("");
			      }
			    });
			    ');
			?>
				<div class="hidden">
					<?= $form->field($model, $inputFieldName.'['.$inputField['id'].'][id]')->textInput(['id'=>'input_field_id_'.$inputField['id'],'maxlength' => true])->label($inputField['title'])?>
				</div>
				<?= $form->field($model, $inputFieldName.'['.$inputField['id'].'][title]')->textInput($inputOpts)->label($inputField['title'].$hintHtml)?>
			<?php
				}
				//Checkbox
				elseif($inputField['input_type']=='checkbox'){
					$selectArr=Yii::$app->inputHelperFunctions->getInputListArr($inputField);
			?>
				<?= $form->field($model, $inputFieldName.'['.$inputField['id'].']')->checkboxList($selectArr)->label($inputField['title'].$hintHtml)?>
			<?php
				}
				//Radio
				elseif($inputField['input_type']=='radio'){
					$selectArr=Yii::$app->inputHelperFunctions->getInputListArr($inputField);
			?>
				<?= $form->field($model, $inputFieldName.'['.$inputField['id'].']')->radioList($selectArr)->label($inputField['title'].$hintHtml)?>
			<?php
				}
				//Select
				elseif($inputField['input_type']=='select'){
					$selectArr=Yii::$app->inputHelperFunctions->getInputListArr($inputField);
					$selectOptions=[];
					if($inputField['selection_type']==2){
						$selectOptions['multiple']='multiple';
			      $selectOptions['class']='form-control selectdd2';
					}else{
						$selectOptions['prompt']=Yii::t('app','Select');
					}
			?>
				<?= $form->field($model, $inputFieldName.'['.$inputField['id'].']')->dropDownList($selectArr,$selectOptions)->label($inputField['title'].$hintHtml)?>
			<?php
				}
				//Textarea / Rich Text Editor
				elseif($inputField['input_type']=='textarea' || $inputField['input_type']=='tinymce'){
					$inputOpts['rows']=4;
					if($inputField['input_type']=='tinymce'){
						$inputOpts['class'] = 'form-control tinymce_editor';
					}
			?>
				<?= $form->field($model, $inputFieldName.'['.$inputField['id'].']')->textArea($inputOpts)->label($inputField['title'].$hintHtml)?>
			<?php
				}
				//Google Map
				elseif($inputField['input_type']=='googlemap'){
			?>
				<?= $this->render('custome_field_google_map',['form'=>$form,'model'=>$model]);?>
			<?php }?>
		</div>
		<?php
			}
		}
		?>
<?php if($mixed==false){?>
		</div>
	</div>
</section>
<?php }?>
<script>
function initCustomFields(){
	//Datepicker
	//Daterange Picker
	$(".inputdaterangepicker").daterangepicker({autoUpdateInput: false,locale: {format: "YYYY-MM-DD",cancelLabel: "<?= Yii::t('app','Clear')?>"},});
	$(".inputdaterangepicker").on("apply.daterangepicker", function(ev, picker) {
		$(this).val(picker.startDate.format("YYYY-MM-DD") + " - " + picker.endDate.format("YYYY-MM-DD"));
		$(this).trigger("change");
	});
	$(".inputdaterangepicker").on("cancel.daterangepicker", function(ev, picker) {
		$(this).val("");
	});
	//Timepicker
	$(".inputtimepicker").timepicker({"timeFormat":"h:i A"});
	//Select2
	if($(".selectdd2").length>0){
		$(".selectdd2").select2({
			placeholder: "<?= Yii::t('app','Select')?>",
		});
	}
	//Rich text Editor
	tinymce.init({
		selector:".tinymce_editor",theme: "modern",
		menubar:false,
		paste_as_text: true,
		directionality : "ltr",
		relative_urls : false,
		remove_script_host : false,
		convert_urls : false,
		plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table paste"
		],
		toolbar: "insertfile undo redo | paste | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
	});
}
</script>
