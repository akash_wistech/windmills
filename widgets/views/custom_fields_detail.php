<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<section class="card card-outline card-info">
	<header class="card-header">
		<h2 class="card-title"><?= Yii::t('app','Custom Fields')?></h2>
	</header>
	<div class="card-body">
		<div class="row">
		<?php
		if($inputFields!=null){
			foreach($inputFields as $inputField){

				$thisFieldId='input_field_'.$inputField['id'];
				$inputOpts['id']=$thisFieldId;
			  $hintHtml='';
			  if($inputField['hint_text']!=''){
			    $hintHtml =' ';
			    $hintHtml.='<a href="javascript:;" class="blue-hint" data-toggle="popover" data-trigger="hover" data-placement="top" title="'.$inputField['title'].'" data-content="'.$inputField['hint_text'].'">';
			    $hintHtml.='	<i class="fa fa-info-circle"></i>';
			    $hintHtml.='</a>';
			  }
				$value = Yii::$app->inputHelperFunctions->getInputFielSavedValueForDetail($model,$inputField);
		?>
		<div class="col-sm-<?= $inputField['colum_width']==12 ? $inputField['colum_width'] : '6'?>">
			<strong><?= $inputField['title'].' '.$hintHtml?>:</strong> <?= $value?>
		</div>
		<?php
			}
		}
		?>
		</div>
	</div>
</section>
