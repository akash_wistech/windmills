<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use app\components\widgets\CustomPjax;

$inputClass = Yii::$app->activityHelper->actionLogSmInputClass;
$dtInputClass = Yii::$app->activityHelper->actionLogDtSmInputClass;
$inputGroupTemplate = Yii::$app->activityHelper->actionLogSmInputGroupTemplate;
$inputGroupClass = ' input-group-sm';
$btnClass = ' btn-xs';
?>
<div class="activity-logger-card card card-success card-outline card-outline-tabs">
  <div class="card-header p-0 border-bottom-0">
    <ul class="nav nav-tabs" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="tabComment" data-toggle="pill" href="#tab-comment" role="tab" aria-selected="true">
          <?= Yii::t('app','Comment')?>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="tabAction" data-toggle="pill" href="#tab-action" role="tab" aria-selected="true">
          <?= Yii::t('app','Action')?>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="tabLogCall" data-toggle="pill" href="#tab-log-call" role="tab" aria-selected="false">
          <?= Yii::t('app','Log Call')?>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="tabLogTime" data-toggle="pill" href="#tab-log-time" role="tab" aria-selected="false">
          <?= Yii::t('app','Log Time')?>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="tabCalendarEvent" data-toggle="pill" href="#tab-calendar-event" role="tab" aria-selected="false">
          <?= Yii::t('app','Calendar')?>
        </a>
      </li>
    </ul>
  </div>
  <div class="card-body">
    <div class="tab-content">
      <div class="tab-pane fade active show" id="tab-comment" role="tabpanel">
        <?= $this->render('/action-log/_comment_form',[
          'model'=>$commentForm,
          'inputClass'=>$inputClass,
          'dtInputClass'=>$dtInputClass,
          'inputGroupTemplate'=>$inputGroupTemplate,
          'inputGroupClass'=>$inputGroupClass,
          'btnClass'=>$btnClass,
          ])?>
      </div>
      <div class="tab-pane fade" id="tab-action" role="tabpanel">
        <?= $this->render('/action-log/_action_form',[
          'model'=>$actionForm,
          'inputClass'=>$inputClass,
          'dtInputClass'=>$dtInputClass,
          'inputGroupTemplate'=>$inputGroupTemplate,
          'inputGroupClass'=>$inputGroupClass,
          'btnClass'=>$btnClass,
          ])?>
      </div>
      <div class="tab-pane fade" id="tab-log-call" role="tabpanel">
        <?= $this->render('/action-log/_call_form',[
          'model'=>$callForm,
          'inputClass'=>$inputClass,
          'dtInputClass'=>$dtInputClass,
          'inputGroupTemplate'=>$inputGroupTemplate,
          'inputGroupClass'=>$inputGroupClass,
          'btnClass'=>$btnClass,
          ])?>
      </div>
      <div class="tab-pane fade" id="tab-log-time" role="tabpanel">
        <?= $this->render('/action-log/_time_form',[
          'model'=>$timerForm,
          'inputClass'=>$inputClass,
          'dtInputClass'=>$dtInputClass,
          'inputGroupTemplate'=>$inputGroupTemplate,
          'inputGroupClass'=>$inputGroupClass,
          'btnClass'=>$btnClass,
          ])?>
      </div>
      <div class="tab-pane fade" id="tab-calendar-event" role="tabpanel">
        <?= $this->render('/action-log/_calendar_form',[
          'model'=>$calendarForm,
          'inputClass'=>$inputClass,
          'dtInputClass'=>$dtInputClass,
          'inputGroupTemplate'=>$inputGroupTemplate,
          'inputGroupClass'=>$inputGroupClass,
          'btnClass'=>$btnClass,
          ])?>
      </div>
    </div>
  </div>
</div>
<div class="card card-outline card-warning">
  <div class="card-header">
    <h3 class="card-title"><?= Yii::t('app','History')?></h3>
  </div>
  <div class="card-body p-0">
    <?php CustomPjax::begin(['id'=>'al-lv-container']); ?>
    <?= ListView::widget( [
      'dataProvider' => $dataProvider,
      'id' => 'alh-list-view',
      'itemOptions' => function ($model, $index, $widget, $grid){
        return ['class' => 'alh-item card-comment'];
      },
      'itemView' => '/action-log/_log_item',
      'layout'=>'
    		<div class="card-comments">{items}</div>
    		{pager}
    	',
      'emptyText' => '<center>No results found.</center>',
      ]);
    ?>
    <?php CustomPjax::end(); ?>
  </div>
</div>
<?= $this->render('/action-log/js/scripts')?>
