<?php
namespace app\widgets;

use Yii;
use app\models\ALCommentForm;
use app\models\ALActionForm;
use app\models\ALCallForm;
use app\models\ALTimerForm;
use app\models\ALCalendarEventForm;
use app\models\ActionLogSearch;

class ActivityLogWidget extends \yii\bootstrap\Widget
{
  public $type;
  public $model;
  public function init()
  {
    parent::init();
  }

  public function run()
  {
    //Comments Form
    $commentForm=new ALCommentForm;
    $commentForm->rec_type='comment';
    $commentForm->module_type=$this->type;
    $commentForm->module_id=$this->model->id;
    //Action Form
    $actionForm=new ALActionForm;
    $actionForm->rec_type='action';
    $actionForm->module_type=$this->type;
    $actionForm->module_id=$this->model->id;
    //Log Call Form
    $callForm=new ALCallForm;
    $callForm->rec_type='call';
    $callForm->module_type=$this->type;
    $callForm->module_id=$this->model->id;
    //Log Time Form
    $timerForm=new ALTimerForm;
    $timerForm->rec_type='timer';
    $timerForm->module_type=$this->type;
    $timerForm->module_id=$this->model->id;
    //Calendar Event Form
    $calendarForm=new ALCalendarEventForm;
    $calendarForm->rec_type='calendar';
    $calendarForm->module_type=$this->type;
    $calendarForm->module_id=$this->model->id;

    $searchModel = new ActionLogSearch;
    $searchModel->module_type=$this->type;
    $searchModel->module_id=$this->model->id;
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('activity_log',[
      'model'=>$this->model,
      'commentForm'=>$commentForm,
      'actionForm'=>$actionForm,
      'callForm'=>$callForm,
      'timerForm'=>$timerForm,
      'calendarForm'=>$calendarForm,
      'searchModel'=>$searchModel,
      'dataProvider'=>$dataProvider,
    ]);
  }
}
