<?php
namespace app\widgets;

use Yii;
use yii\helpers\ArrayHelper;

class CustomFieldsDetailWidget extends \yii\bootstrap\Widget
{
  public $type;
  public $model;
  public function init()
  {
    parent::init();
  }

  public function run()
  {
    $inputFields=Yii::$app->inputHelperFunctions->getInputTypesByModule($this->type);

    return $this->render('custom_fields_detail',[
      'model'=>$this->model,
      'inputFields'=>$inputFields,
    ]);
  }
}
