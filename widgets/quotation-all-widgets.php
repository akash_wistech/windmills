<style>
h4 {
    font-size: 18px !important;
    font-weight: bold;
}

.card-header {
    padding: 10px !important;
    padding-bottom: 20px !important;
}

.card-footer {
    padding: 0px !important;
}

.card-footer span {
    font-size: 13.5px !important;
}


@media only screen and (max-width: 600px) {
    h4 {
        font-size: 20px !important;
        font-weight: bold !important;
    }
}

</style>

<?php 
    $amountAllowedId = [1,14,33];
?>

<section class="card card-outline card-primar">
    <header class="card-header bg-gray" style="background-colo:#E0E0E0">
        <h2 class="card-title text-bold">Quotation All Time Widgets</h2>
    </header>
    <div class="card-body" style="background-color:#E5E4E2">

        <div class="row px-4">
            
            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 bg-gray" style="width:60px; height:60px;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">description</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase increase-font text-right" style="color:#757575">
                                Inquiry Received<br>
                                <?= number_format($data['inquiry_recieved']['total_records']) ?></h4>
                            <h4 class=" float-right" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,$amountAllowedId)) { ?>
                                <?= number_format($data['inquiry_recieved']['total_amount']) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="fas fa-arrow-right text-gray pt-1 pl-3" style="font-size:30px;"></i>
                            <a class="position-absolute footer-increase text-gray" style="left:53px;top:5px;"
                                target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index','CrmQuotationsSearch[widget_type]'=>'all', 'CrmQuotationsSearch[status_type]'=>'inquiry_received']) ?>">
                                <span style="font-size:20px;">Go to Inquiry Received</span> </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 bg-gray" style="width:60px; height:60px;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">description</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase increase-font text-right" style="color:#757575">
                                Quotation Approved<br>
                                <?= number_format($data['quotation_approved']['total_records']) ?></h4>
                            <h4 class=" float-right" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,$amountAllowedId)) { ?>
                                <?= number_format($data['quotation_approved']['total_amount']) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="fas fa-arrow-right text-gray pt-1 pl-3" style="font-size:30px;"></i>
                            <a class="position-absolute footer-increase text-gray" style="left:53px;top:5px;"
                                target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index','CrmQuotationsSearch[widget_type]'=>'all', 'CrmQuotationsSearch[status_type]'=>'quotation_approved']) ?>">
                                <span style="font-size:20px;">Go to Quotation Approved</span> </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 bg-gray" style="width:60px; height:60px;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">description</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase increase-font text-right" style="color:#757575">
                                Document Requested<br>
                                <?= number_format($data['document_requested']['total_records']) ?></h4>
                            <h4 class=" float-right" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,$amountAllowedId)) { ?>
                                <?= number_format($data['document_requested']['total_amount']) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="fas fa-arrow-right text-gray pt-1 pl-3" style="font-size:30px;"></i>
                            <a class="position-absolute footer-increase text-gray" style="left:53px;top:5px;"
                                target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index','CrmQuotationsSearch[widget_type]'=>'all', 'CrmQuotationsSearch[status_type]'=>'document_requested']) ?>">
                                <span style="font-size:20px;">Go to Document Requested</span> </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 bg-gray" style="width:60px; height:60px;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">description</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase increase-font text-right" style="color:#757575">
                                Quotation Sent<br>
                                <?= number_format($data['quotation_sent']['total_records']) ?></h4>
                            <h4 class=" float-right" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,$amountAllowedId)) { ?>
                                <?= number_format($data['quotation_sent']['total_amount']) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="fas fa-arrow-right text-gray pt-1 pl-3" style="font-size:30px;"></i>
                            <a class="position-absolute footer-increase text-gray" style="left:53px;top:5px;"
                                target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index','CrmQuotationsSearch[widget_type]'=>'all', 'CrmQuotationsSearch[status_type]'=>'quotation_sent']) ?>">
                                <span style="font-size:20px;">Go to Quotation Sent</span> </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 bg-gray" style="width:60px; height:60px;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">description</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase increase-font text-right" style="color:#757575">
                                Quotation Accepted<br>
                                <?= number_format($data['quotation_accepted']['total_records']) ?></h4>
                            <h4 class=" float-right" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,$amountAllowedId)) { ?>
                                <?= number_format($data['quotation_accepted']['total_amount']) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="fas fa-arrow-right text-gray pt-1 pl-3" style="font-size:30px;"></i>
                            <a class="position-absolute footer-increase text-gray" style="left:53px;top:5px;"
                                target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index','CrmQuotationsSearch[widget_type]'=>'all', 'CrmQuotationsSearch[status_type]'=>'quotation_accepted']) ?>">
                                <span style="font-size:20px;">Go to Quottion Accepted</span> </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 bg-gray" style="width:60px; height:60px;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">description</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase increase-font text-right" style="color:#757575">
                                TOE Sent<br>
                                <?= number_format($data['toe_sent']['total_records']) ?></h4>
                            <h4 class=" float-right" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,$amountAllowedId)) { ?>
                                <?= number_format($data['toe_sent']['total_amount']) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="fas fa-arrow-right text-gray pt-1 pl-3" style="font-size:30px;"></i>
                            <a class="position-absolute footer-increase text-gray" style="left:53px;top:5px;"
                                target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index','CrmQuotationsSearch[widget_type]'=>'all', 'CrmQuotationsSearch[status_type]'=>'toe_sent']) ?>">
                                <span style="font-size:20px;">Go to TOE Sent</span> </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 bg-gray" style="width:60px; height:60px;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">description</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase increase-font text-right" style="color:#757575">
                                TOE Signed And Received<br>
                                <?= number_format($data['toe_signed_and_received']['total_records']) ?></h4>
                            <h4 class=" float-right" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,$amountAllowedId)) { ?>
                                <?= number_format($data['toe_signed_and_received']['total_amount']) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="fas fa-arrow-right text-gray pt-1 pl-3" style="font-size:30px;"></i>
                            <a class="position-absolute footer-increase text-gray" style="left:53px;top:5px;"
                                target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index','CrmQuotationsSearch[widget_type]'=>'all', 'CrmQuotationsSearch[status_type]'=>'toe_signed_and_received']) ?>">
                                <span style="font-size:20px;">Go to TOE Signed And Received</span> </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 bg-gray" style="width:60px; height:60px;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">description</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase increase-font text-right" style="color:#757575">
                                Payment Received<br>
                                <?= number_format($data['payment_received']['total_records']) ?></h4>
                            <h4 class=" float-right" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,$amountAllowedId)) { ?>
                                <?= number_format($data['payment_received']['total_amount']) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="fas fa-arrow-right text-gray pt-1 pl-3" style="font-size:30px;"></i>
                            <a class="position-absolute footer-increase text-gray" style="left:53px;top:5px;"
                                target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index','CrmQuotationsSearch[widget_type]'=>'all', 'CrmQuotationsSearch[status_type]'=>'payment_received']) ?>">
                                <span style="font-size:20px;">Go to Payment Received</span> </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 bg-gray" style="width:60px; height:60px;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">description</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase increase-font text-right" style="color:#757575">
                                Quotation Rejected<br>
                                <?= number_format($data['quotation_rejected']['total_records']) ?></h4>
                            <h4 class=" float-right" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,$amountAllowedId)) { ?>
                                <?= number_format($data['quotation_rejected']['total_amount']) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="fas fa-arrow-right text-gray pt-1 pl-3" style="font-size:30px;"></i>
                            <a class="position-absolute footer-increase text-gray" style="left:53px;top:5px;"
                                target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index','CrmQuotationsSearch[widget_type]'=>'all', 'CrmQuotationsSearch[status_type]'=>'quotation_rejected']) ?>">
                                <span style="font-size:20px;">Go to Quotation Rejected</span> </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 bg-gray" style="width:60px; height:60px;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">description</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase increase-font text-right" style="color:#757575">
                                TOE Rejected<br>
                                <?= number_format($data['toe_rejected']['total_records']) ?></h4>
                            <h4 class=" float-right" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,$amountAllowedId)) { ?>
                                <?= number_format($data['toe_rejected']['total_amount']) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="fas fa-arrow-right text-gray pt-1 pl-3" style="font-size:30px;"></i>
                            <a class="position-absolute footer-increase text-gray" style="left:53px;top:5px;"
                                target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index','CrmQuotationsSearch[widget_type]'=>'all', 'CrmQuotationsSearch[status_type]'=>'toe_rejected']) ?>">
                                <span style="font-size:20px;">Go to TOE Rejected</span> </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 bg-gray" style="width:60px; height:60px;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">description</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase increase-font text-right" style="color:#757575">
                                Regretted<br>
                                <?= number_format($data['regretted']['total_records']) ?></h4>
                            <h4 class=" float-right" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,$amountAllowedId)) { ?>
                                <?= number_format($data['regretted']['total_amount']) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="fas fa-arrow-right text-gray pt-1 pl-3" style="font-size:30px;"></i>
                            <a class="position-absolute footer-increase text-gray" style="left:53px;top:5px;"
                                target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index','CrmQuotationsSearch[widget_type]'=>'all', 'CrmQuotationsSearch[status_type]'=>'regretted']) ?>">
                                <span style="font-size:20px;">Go to Regretted</span> </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row px-4">
            
            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 bg-gray" style="width:60px; height:60px;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">description</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase increase-font text-right" style="color:#757575">
                                Ahead of Time<br>
                                <?= number_format($data['ahead_of_time']['total_records']) ?></h4>
                            <h4 class=" float-right" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,$amountAllowedId)) { ?>
                                <?= number_format($data['ahead_of_time']['total_amount']) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="fas fa-arrow-right text-gray pt-1 pl-3" style="font-size:30px;"></i>
                            <a class="position-absolute footer-increase text-gray" style="left:53px;top:5px;"
                                target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-ahead-ontime-delay','CrmQuotationsSearch[widget_type]'=>'all', 'CrmQuotationsSearch[status_type]'=>'ahead_of_time']) ?>">
                                <span style="font-size:20px;">Go to Ahead of Time</span> </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 bg-gray" style="width:60px; height:60px;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">description</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase increase-font text-right" style="color:#757575">
                                On Time<br>
                                <?= number_format($data['on_time']['total_records']) ?></h4>
                            <h4 class=" float-right" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,$amountAllowedId)) { ?>
                                <?= number_format($data['on_time']['total_amount']) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="fas fa-arrow-right text-gray pt-1 pl-3" style="font-size:30px;"></i>
                            <a class="position-absolute footer-increase text-gray" style="left:53px;top:5px;"
                                target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-ahead-ontime-delay','CrmQuotationsSearch[widget_type]'=>'all', 'CrmQuotationsSearch[status_type]'=>'on_time']) ?>">
                                <span style="font-size:20px;">Go to On Time</span> </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 bg-gray" style="width:60px; height:60px;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">description</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase increase-font text-right" style="color:#757575">
                                Delay<br>
                                <?= number_format($data['delay']['total_records']) ?></h4>
                            <h4 class=" float-right" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,$amountAllowedId)) { ?>
                                <?= number_format($data['delay']['total_amount']) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="fas fa-arrow-right text-gray pt-1 pl-3" style="font-size:30px;"></i>
                            <a class="position-absolute footer-increase text-gray" style="left:53px;top:5px;"
                                target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-ahead-ontime-delay','CrmQuotationsSearch[widget_type]'=>'all', 'CrmQuotationsSearch[status_type]'=>'delay']) ?>">
                                <span style="font-size:20px;">Go to Delay</span> </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>