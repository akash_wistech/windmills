<style>
    h4 {
        font-size: 16px !important;
        font-weight: bold;
        font-family: sans-serif !important;
        width: fit-content;
    }

    .card-header {
        padding: 10px !important;
        padding-bottom: 20px !important;
    }

    .card-footer {
        padding: 0px !important;
    }

    .card-footer span {
        font-size: 13.5px !important;
    }

    .col-7 {
        max-width: fit-content !important;
    }

    .pending-text-info {
        color: #08A86D !important;
    }
</style>

<?php
$amountAllowedId = [1, 14, 33];

$today_start_date = date('Y-m-d') . ' 00:00:00';
$today_end_date = date('Y-m-d') . ' 23:59:59';

// pending proposals overview
$pending_p_overview = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['in', 'quotation_status', [17, 2, 14, 16, 10]])
    ->andWhere(['trashed' => null])
    ->andWhere(['converted' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

// pending quotations recommended
$pending_q_recommended = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 17])
    ->andWhere(['trashed' => null])
    ->andWhere(['converted' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

// pending quotations approved (label changed to verified)
$pending_q_approved = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 2])
    ->andWhere(['trashed' => null])
    ->andWhere(['converted' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();
    
// pending documents requested
$pending_document_requested = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 14])
    ->andWhere(['trashed' => null])
    ->andWhere(['converted' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

// pending quotations accepted(label changed to approved)
$pending_q_accepted = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 16])
    ->andWhere(['trashed' => null])
    ->andWhere(['converted' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

// pending toe sent
$pending_toe_sent = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 16])
    ->andWhere(['trashed' => null])
    ->andWhere(['converted' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

// pending toe signed and received
$pending_toe_signed_received = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 5])
    ->andWhere(['toe_signed_and_received' => 5])
    ->andWhere(['trashed' => null])
    ->andWhere(['payment_received_date' => null])
    ->andWhere(['converted' => 1])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

// pending quotations on hold
$pending_q_onhold = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 10])
    ->andWhere(['trashed' => null])
    ->andWhere(['converted' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

// pending quotations cancelled
$pending_q_cancelled = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['quotation_status' => 11])
    ->andWhere(['trashed' => null])
    ->andWhere(['converted' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

// pending proposals tat performance
$pending_p_tat_performance = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['in', 'quotation_status', [0, 9, 2, 14, 16, 10, 11]])
    ->andWhere(['trashed' => null])
    ->andWhere(['converted' => null])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

$pending_toe_recommended = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['trashed' => null])
    ->andWhere(['toe_recommended' => 1])
    ->andWhere(['status_approve' => "toe_recommended"])
    ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->asArray()
    ->all();

// reminders ahead of time
$pending_ahead_of_time = \app\models\CrmQuotations::find()
    ->select([
        \app\models\CrmQuotations::tableName() . '.id',
        \app\models\CrmQuotations::tableName() . '.reference_number',
        \app\models\CrmQuotations::tableName() . '.instruction_date',
        \app\models\CrmQuotations::tableName() . '.target_date',
        \app\models\CrmQuotations::tableName() . '.created_at',
    ])
    ->where([\app\models\CrmQuotations::tableName() . '.quotation_status' => 9])
    ->andWhere([\app\models\CrmQuotations::tableName() . '.converted' => null])
    ->andWhere([\app\models\CrmQuotations::tableName() . '.parent_id' => null])
    ->andwhere('DATE(target_date) > DATE(instruction_date)');

$pending_totalAhead_of_time_count = count($pending_ahead_of_time->all());
$pending_ahead_of_time->andFilterWhere([
    'between', "DATE(target_date)", 'DATE(instruction_date)', date('Y-m-d H:i:s')
]);
$pending_ahead_of_time_count = count($pending_ahead_of_time->all());

// reminders on time
$pending_on_time = \app\models\CrmQuotations::find()
    ->select([
        \app\models\CrmQuotations::tableName() . '.id',
        \app\models\CrmQuotations::tableName() . '.reference_number',
        \app\models\CrmQuotations::tableName() . '.instruction_date',
        \app\models\CrmQuotations::tableName() . '.target_date',
        \app\models\CrmQuotations::tableName() . '.created_at',
    ])
    ->where([\app\models\CrmQuotations::tableName() . '.quotation_status' => 9])
    ->andWhere([\app\models\CrmQuotations::tableName() . '.converted' => null])
    ->andWhere([\app\models\CrmQuotations::tableName() . '.parent_id' => null])
    ->andwhere('DATE(target_date) = DATE(instruction_date)');

$pending_totalOn_time_count = count($pending_on_time->all());
$pending_on_time->andFilterWhere([
    'between', 'DATE(target_date)', 'DATE(instruction_date)', date('Y-m-d')
]);
$pending_on_time_count = count($pending_on_time->all());

// delayed off time
$pending_delayed = \app\models\CrmQuotations::find()
    ->select([
        \app\models\CrmQuotations::tableName() . '.id',
        \app\models\CrmQuotations::tableName() . '.reference_number',
        \app\models\CrmQuotations::tableName() . '.instruction_date',
        \app\models\CrmQuotations::tableName() . '.target_date',
        \app\models\CrmQuotations::tableName() . '.created_at',
    ])
    ->where([\app\models\CrmQuotations::tableName() . '.quotation_status' => 9])
    ->andWhere([\app\models\CrmQuotations::tableName() . '.converted' => null])
    ->andWhere([\app\models\CrmQuotations::tableName() . '.parent_id' => null])
    ->andwhere('DATE(target_date) < DATE(instruction_date)');

$pending_totalDelayed_count = count($pending_delayed->all());
$pending_delayed->andFilterWhere([
    'between', 'DATE(instruction_date)', date('Y-m-d'), date('Y-m-d')
]);
$pending_delayed_count = count($pending_delayed->all());

// $pending_inquiries = \app\models\CrmQuotations::find()
// ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
// ->where(['quotation_status' => 0])
// ->andWhere(['status_approve' => null])
// ->andWhere(['trashed' => null])
// ->andWhere(['converted' => null])
// ->asArray()
// ->all();

// $pending_quotation_sent = \app\models\CrmQuotations::find()
// ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
// ->where(['quotation_status' => 1])
// // ->andWhere(['status_approve' => 'Approve'])
// ->andWhere(['trashed' => null])
// ->andWhere(['converted' => null])
// ->andWhere(['not', ['quotation_sent_date' => null]])
// ->asArray()
// ->all();




// $pending_payment_received = \app\models\CrmQuotations::find()
// ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
// ->where(['quotation_status' => 6])
// // ->andWhere(['status_approve' => 'toe_Accept'])
// ->andWhere(['trashed' => null])
// ->andWhere(['converted' => null])
// ->andWhere(['not', ['payment_received_date' => null]])
// ->asArray()
// ->all();

// $pending_regretted = \app\models\CrmQuotations::find()
// ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
// ->where(['quotation_status' => 12])
// // ->andWhere(['status_approve' => 'regretted'])
// ->andWhere(['trashed' => null])
// ->andWhere(['converted' => null])
// ->andWhere(['not', ['regretted_date' => null]])
// ->asArray()
// ->all();
?>

<!-- <div class="container">
    <div class="row mt-4 mb-4"> -->
<div class="col-lg-4 text-center ">
    <button type="button" class="button success" data-toggle="modal" data-target=".bd-example-modal-xl-5">
        <center>
            <div class=" col-12">
                <h4 class="card-category">
                    <span class="t-color-2"> Pending Proposals Performance </span>
                    <br>
                    <span style="float:left">
                        <?= number_format($pending_p_overview[0]['totalRecords']) ?>
                    </span>
                    <span style="float:right">
                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                            <?= number_format($pending_p_overview[0]['totalFee']) ?>
                        <?php } ?>
                    </span>
                </h4>
            </div>
        </center>
    </button>
</div>

<div class="modal fade bd-example-modal-xl-5" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"> Pending Proposals Performance </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="card-body" style="background-color:#E5E4E2">
                <div class="row px-4">

                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('pending_proposals_overview')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-7 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right"
                                        style="color:#757575">
                                        <span class="pending-text-info">Pending Proposals Overview </span><br>
                                        <?= number_format($pending_p_overview[0]['totalRecords']) ?>
                                    </h4>
                                    <h4 class=" float-right" style="color:#757575">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($pending_p_overview[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase"
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'pending', 'CrmQuotationsSearch[status_type]' => 'performance_overview']) ?>">
                                        <span style="font-size:20px;">Go to Proposals Overview</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('pending_proposals_recommended')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-7 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right"
                                        style="color:#757575">
                                        <span class="pending-text-info">Pending Recommended</span><br>
                                        <?= number_format($pending_q_recommended[0]['totalRecords']) ?>
                                    </h4>
                                    <h4 class=" float-right" style="color:#757575">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($pending_q_recommended[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase"
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'pending', 'CrmQuotationsSearch[status_type]' => 'quotation_recommended']) ?>">
                                        <span style="font-size:20px;">Go to Quotation Recommended</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('pending_quotation_documents')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-7 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right"
                                        style="color:#757575">
                                        <span class="pending-text-info">Pending Document Requested</span><br>
                                        <?= number_format($pending_document_requested[0]['totalRecords']) ?>
                                    </h4>
                                    <h4 class=" float-right" style="color:#757575">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($pending_document_requested[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase"
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'pending', 'CrmQuotationsSearch[status_type]' => 'document_requested']) ?>">
                                        <span style="font-size:20px;">Go to Document Requested</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('pending_quotation_verified')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-7 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right"
                                        style="color:#757575">
                                        <span class="pending-text-info">Pending Quotation Verified</span><br>
                                        <?= number_format($pending_q_approved[0]['totalRecords']) ?>
                                    </h4>
                                    <h4 class=" float-right" style="color:#757575">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($pending_q_approved[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase"
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'pending', 'CrmQuotationsSearch[status_type]' => 'quotation_approved']) ?>">
                                        <span style="font-size:20px;">Go to Quotation Verified</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    
                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('pending_quotation_hold')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-7 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right"
                                        style="color:#757575">
                                        <span class="pending-text-info">Pending Quotation On Hold</span><br>
                                        <?= number_format($pending_q_onhold[0]['totalRecords']) ?>
                                    </h4>
                                    <h4 class=" float-right" style="color:#757575">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($pending_q_onhold[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase"
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'pending', 'CrmQuotationsSearch[status_type]' => 'quotation_onhold']) ?>">
                                        <span style="font-size:20px;">Go to Quotation On Hold</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('pending_quotation_approved')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-7 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right"
                                        style="color:#757575">
                                        <span class="pending-text-info">Pending Quotation Approved</span><br>
                                        <?= number_format($pending_q_accepted[0]['totalRecords']) ?>
                                    </h4>
                                    <h4 class=" float-right" style="color:#757575">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($pending_q_accepted[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase"
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'pending', 'CrmQuotationsSearch[status_type]' => 'quotation_accepted']) ?>">
                                        <span style="font-size:20px;">Go to Quottion Approved</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('pending_toe_sent')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-7 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right"
                                        style="color:#757575">
                                        <span class="pending-text-info">Pending TOE Sent</span><br>
                                        <?= number_format($pending_toe_sent[0]['totalRecords']) ?>
                                    </h4>
                                    <h4 class=" float-right" style="color:#757575">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($pending_toe_sent[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase"
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'pending', 'CrmQuotationsSearch[status_type]' => 'toe_sent']) ?>">
                                        <span style="font-size:20px;">Go to TOE Sent</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('pending_toe_signed')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-7 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right"
                                        style="color:#757575">
                                        <span class="pending-text-info">Pending TOE Signed</span><br>
                                        <?= number_format($pending_toe_signed_received[0]['totalRecords']) ?>
                                    </h4>
                                    <h4 class=" float-right" style="color:#757575">
                                        <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                            <?= number_format($pending_toe_signed_received[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3"
                                        style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase"
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'pending', 'CrmQuotationsSearch[status_type]' => 'toe_signed_and_received']) ?>">
                                        <span style="font-size:20px;">Go to Pending TOE Signed</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('pending_toe_verified')) { ?>
                    <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                        <div id="w0" class="card-stats">
                            <div
                                class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                <div class="py-2 shadow rounded ml-2 bg-info"
                                    style="width:60px; height:60px; background-color: #08A86D !important;">
                                    <i class="fas fa-hourglass-half pt-1 pl-3" style="font-size: 38px;"></i>
                                </div>
                                <div class=" col-10 position-absolute" style="right:0px;">
                                    <h4 class="card-category font-increase increase-font text-right" style="color:#616161">
                                        <span class="pending-text-info">Pending TOE Recommended</span><br>
                                        <?= number_format($pending_toe_recommended[0]['totalRecords']) ?></h4>
                                    <h4 class=" float-right" style="color:#616161">
                                        <?php if(in_array( Yii::$app->user->identity->id,$amountAllowedId)) { ?>
                                            <?= number_format($pending_toe_recommended[0]['totalFee']) ?>
                                        <?php } ?>
                                    </h4>
                                </div>
                            </div>
                            <div class="card-footer bg-white">
                                <div class="stats position-relative" style="box-sizing: border-box;">
                                    <i class="fas fa-arrow-right pending-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                    <a class="position-absolute pending-text-info footer-increase"
                                        style="left:53px;top:5px;" target="_blank"
                                        href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-index', 'CrmQuotationsSearch[widget_type]' => 'pending', 'CrmQuotationsSearch[status_type]' => 'toe_recommended']) ?>">
                                        <span style="font-size:20px;">Go to TOE Recommended</span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- </div>
</div> -->