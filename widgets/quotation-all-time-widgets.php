<style>
h4 {
    font-size: 15px;
    font-weight: bold;
}

.card-header {
    padding: 10px !important;
    padding-bottom: 20px !important;
}

.card-footer {
    padding: 0px !important;
}

.card-footer span {
    font-size: 13.5px !important;
}

@media only screen and (max-width: 600px) {
    h4 {
        font-size: 20px !important;
        font-weight: bold !important;
    }
}
</style>


<section class="valuation-form card card-outline card-primary">
    <header class="card-header">
        <h2 class="card-title">Quotation All Time Widgets</h2>
    </header>
    <div class="card-body" style="background-color:#E5E4E2">


        <div class="row px-4">

            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">description</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase text-right" style="color:#757575">Total Inquiry Received<br>
                                <?= key($data['inquiry_recieved_all']) ?></h4>
                            <h4 class=" float-right font-increase" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,[1,14,33])) { ?>
                                <?= (reset($data['inquiry_recieved_all']) > 0) ? number_format(reset($data['inquiry_recieved_all'])) : 0 ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white footer-increase">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                            <a class="position-absolute text-info" style="left:53px;top:5px;" target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[quotation_status]'=> 15,'CrmQuotationsSearch[dashboard_filter]'=> 'yes','CrmQuotationsSearch[date_period]'=> 10]) ?>">
                                <span style="font-size:20px;">Go to Last 3 Month Inquiries</span> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">description</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase text-right" style="color:#757575">Inquiry Received<br>
                                <?= key($data['inquiry_recieved']) ?></h4>
                            <h4 class=" float-right font-increase" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,[1,14,33])) { ?>
                                <?= (reset($data['inquiry_recieved']) > 0) ? number_format(reset($data['inquiry_recieved'])) : 0 ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white footer-increase">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons text-info pt-1 pl-3" style="font-size:30px">description</i>
                            <a class="position-absolute text-info" style="left:53px;top:5px;" target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[quotation_status]'=> 0,'CrmQuotationsSearch[dashboard_filter]'=> 'yes','CrmQuotationsSearch[date_period]'=> 10]) ?>">
                                <span style="font-size:20px;">Go to Inquiry Received</span> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 "
                            style="width:60px; height:60px; background-color:#FFA000;">
                            <div class="card-icon pt-2 pl-2">
                                <i class="material-icons text-white pl-1" style="font-size:40px">business</i>
                            </div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase text-right" style="color:#757575">Quotation Sent<br>
                                <?= key($data['quotation_sent']) ?></h4>
                            <h4 class=" float-right font-increase" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,[1,14,33])) { ?>
                                <?= number_format(reset($data['quotation_sent'])) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white footer-increase">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#FFA000;">business</i>
                            <a class="position-absolute" style="left:53px;top:5px;color:#FFA000;" target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[quotation_status]'=> 1,'CrmQuotationsSearch[dashboard_filter]'=> 'yes','CrmQuotationsSearch[date_period]'=> 10]) ?>">
                                <span style="font-size:20px;">Go to Quotation Sent</span> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2" style="width:60px; height:60px; background-color:#00695C">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">money</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase text-right" style="color:#757575">Toe Sent<br>
                                <?= key($data['toe_sent']) ?></h4>
                            <h4 class=" float-right font-increase" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,[1,14,33])) { ?>
                                <?= (reset($data['toe_sent'])>0)?number_format(reset($data['toe_sent'])):0 ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white footer-increase">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#00695C;">money</i>
                            <a class="position-absolute" style="left:53px;top:5px; " target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[quotation_status]'=> 3,'CrmQuotationsSearch[dashboard_filter]'=> 'yes','CrmQuotationsSearch[date_period]'=> 10]) ?>">
                                <span style="font-size:20px; color:#00695C;">Go To Toe Sent</span> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 "
                            style="width:60px; height:60px; background-color:#C0CA33;">
                            <div class="card-icon pt-2 pl-2">
                                <i class="material-icons text-white pl-1" style="font-size:40px">business</i>
                            </div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase text-right" style="color:#757575">Payment Received<br>
                                <?= key($data['payment_recieved']) ?></h4>
                            <h4 class=" float-right font-increase" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,[1,14,33])) { ?>
                                <?= number_format(reset($data['payment_recieved'])) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white footer-increase">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#C0CA33;">business</i>
                            <a class="position-absolute" style="left:53px;top:5px;color:#C0CA33;" target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[quotation_status]'=> 6,'CrmQuotationsSearch[dashboard_filter]'=> 'yes','CrmQuotationsSearch[date_period]'=> 10]) ?>">
                                <span style="font-size:20px;">Go to Payment Received</span> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 "
                            style="width:60px; height:60px; background-color:#1565C0;">
                            <div class="card-icon pt-2 pl-2">
                                <i class="material-icons text-white pl-1" style="font-size:40px">business</i>
                            </div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase text-right" style="color:#757575">Toe Signed And Received<br>
                                <?= key($data['toe_Sign_and_Rec']) ?></h4>
                            <h4 class=" float-right font-increase" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,[1,14,33])) { ?>
                                <?= number_format(reset($data['toe_Sign_and_Rec'])) ?>
                                <?php } ?>

                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white footer-increase">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color:#1565C0;">business</i>
                            <a class="position-absolute" style="left:53px;top:5px;color:#1565C0;"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[toe_signed_and_received]'=> 1,'CrmQuotationsSearch[dashboard_filter]'=> 'yes','CrmQuotationsSearch[date_period]'=> 10]) ?>">
                                <span style="font-size:20px;">Go to Toe Signed</span> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 "
                            style="width:60px; height:60px; background-color:#90A4AE;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">assignment</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase text-right" style="color:#757575">On Hold<br>
                                <?= key($data['on_hold']) ?></h4>
                            <h4 class=" float-right font-increase" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,[1,14,33])) { ?>
                                <?= number_format(reset($data['on_hold'])) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white footer-increase">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color: #90A4AE;">assignment</i>
                            <a class="position-absolute" style="left:53px;top:5px; color: #90A4AE;" target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[quotation_status]'=> 10,'CrmQuotationsSearch[dashboard_filter]'=> 'yes','CrmQuotationsSearch[date_period]'=> 10]) ?>">
                                <span style="font-size:20px;">Go to On Hold</span> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 "
                            style="width:60px; height:60px; background-color:#1565C0;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">assignment</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase text-right" style="color:#757575">Approved<br>
                                <?= key($data['quotation_approved']) ?></h4>
                            <h4 class=" float-right font-increase" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,[1,14,33])) { ?>
                                <?= number_format(reset($data['quotation_approved'])) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white footer-increase">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color: #1565C0;">assignment</i>
                            <a class="position-absolute" style="left:53px;top:5px; color: #1565C0;" target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[quotation_status]'=> 2,'CrmQuotationsSearch[dashboard_filter]'=> 'yes','CrmQuotationsSearch[date_period]'=> 10]) ?>">
                                <span style="font-size:20px;">Go to Approved</span> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 "
                            style="width:60px; height:60px; background-color:#90A4AE;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">assignment</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase text-right" style="color:#757575">Quotation Rejected<br>
                                <?= key($data['quotation_rejected']) ?></h4>
                            <h4 class=" float-right font-increase" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,[1,14,33])) { ?>
                                <?= number_format(reset($data['quotation_rejected'])) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white footer-increase">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color: #90A4AE;">assignment</i>
                            <a class="position-absolute" style="left:53px;top:5px; color: #90A4AE;"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[quotation_status]'=> 7 ,'CrmQuotationsSearch[dashboard_filter]'=> 'yes','CrmQuotationsSearch[date_period]'=> 10]) ?>">
                                <span style="font-size:20px;">Go to Quotation Rejected</span> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 "
                            style="width:60px; height:60px; background-color:#90A4AE;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">assignment</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase text-right" style="color:#757575">TOE Rejected<br>
                                <?= key($data['toe_rejected']) ?></h4>
                            <h4 class=" float-right font-increase" style="color:#757575">
                                <?php if(in_array( Yii::$app->user->identity->id,[1,14,33])) { ?>
                                <?= number_format(reset($data['toe_rejected'])) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white footer-increase">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color: #90A4AE;">assignment</i>
                            <a class="position-absolute" style="left:53px;top:5px; color: #90A4AE;" target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[quotation_status]'=> 8  ,'CrmQuotationsSearch[dashboard_filter]'=> 'yes','CrmQuotationsSearch[date_period]'=> 10]) ?>">
                                <span style="font-size:20px;">Go to TOE Rejected</span> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 "
                            style="width:60px; height:60px; background-color:#90A4AE;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">assignment</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase text-right" style="color:#757575">Cancelled<br>
                                <?= key($data['cancelled']) ?></h4>
                            <h4 class=" float-right font-increase" style="color: #757575">
                                <?php if(in_array( Yii::$app->user->identity->id,[1,14,33])) { ?>
                                <?= number_format(reset($data['cancelled'])) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white footer-increase">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3" style="font-size:30px; color: #90A4AE;">assignment</i>
                            <a class="position-absolute" style="left:53px;top:5px; color: #90A4AE;" target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[quotation_status]'=> 11,'CrmQuotationsSearch[dashboard_filter]'=> 'yes','CrmQuotationsSearch[date_period]'=> 10 ]) ?>">
                                <span style="font-size:20px;">Go to Cancelled</span> </a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                <div id="w0" class="card-stats">
                    <div
                        class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                        <div class="py-2 shadow rounded ml-2 bg-danger"
                            style="width:60px; height:60px; background-color:#757575;">
                            <div class="card-icon pt-2 pl-2"><i class="material-icons text-white pl-1"
                                    style="font-size:40px">assignment</i></div>
                        </div>
                        <div class=" col-7 position-absolute" style="right:0px;">
                            <h4 class="card-category font-increase text-right" style="color:#757575">Regretted<br>
                                <?= key($data['regretted']) ?></h4>
                            <h4 class=" float-right font-increase" style="color: #757575">
                                <?php if(in_array( Yii::$app->user->identity->id,[1,14,33])) { ?>
                                <?= number_format(reset($data['regretted'])) ?>
                                <?php } ?>
                            </h4>
                        </div>
                    </div>
                    <div class="card-footer bg-white footer-increase">
                        <div class="stats position-relative" style="box-sizing: border-box;">
                            <i class="material-icons pt-1 pl-3 " style="font-size:30px; color: red;">assignment</i>
                            <a class="position-absolute" style="left:53px;top:5px; color: #90A4AE;" target="_blank"
                                href="<?= yii\helpers\Url::to(['crm-quotations/index','CrmQuotationsSearch[quotation_status]'=> 12 ,'CrmQuotationsSearch[dashboard_filter]'=> 'yes','CrmQuotationsSearch[date_period]'=> 10]) ?>">
                                <span style="font-size:20px;">Go to Regretted</span> </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</section>