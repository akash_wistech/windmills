<style>
    h4 {
        font-size: 16px !important;
        font-weight: bold;
        font-family: sans-serif !important;
        width: fit-content;
    }

    .card-header {
        padding: 10px !important;
        padding-bottom: 20px !important;
    }

    .card-footer {
        padding: 0px !important;
    }

    .card-footer span {
        font-size: 13.5px !important;
    }

    .text-primary-dynamic,
    .bg-primary-dynamic {
        color: #64B5F6 !important;
    }

    .pending-text-info {
        color: #08A86D !important;
    }
</style>

<?php
$today_start_date = date('Y-m-d') . ' 00:00:00';
$today_end_date = date('Y-m-d') . ' 23:59:59';
// $today_start_date = '2024-02-23  00:00:00';
// $today_end_date = '2024-02-23  23:59:59';

// today proposals overview
$proposals_overview = \app\models\CrmQuotations::find()
    ->select(['COUNT(*) AS totalRecords', 'SUM(final_quoted_fee) AS totalFee'])
    ->where(['trashed' => null])
    // ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->andFilterWhere(['between', 'created_at', $today_start_date, $today_end_date])
    ->asArray()
    ->all();

// today proposals recommended
$proposals_recommended = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['trashed' => null])
    // ->orWhere(['quotation_status' => 17])
    // ->orWhere(['status_approve' => 'Recommended'])
    // ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->andFilterWhere(['between', 'quotation_recommended_date', $today_start_date, $today_end_date])
    ->asArray()
    ->all();

// today proposals approved(label change to verified)
$proposals_approved = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['trashed' => null])
    // ->orWhere(['quotation_status' => 2])
    // ->orWhere(['status_approve' => 'Approve'])
    // ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->andFilterWhere(['between', 'approved_date', $today_start_date, $today_end_date])
    ->asArray()
    ->all();

// today proposals document requested
$today_documents_requested = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['trashed' => null])
    // ->orWhere(['quotation_status' => 14])
    // ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->andFilterWhere(['between', 'document_requested_date', $today_start_date, $today_end_date])
    ->asArray()
    ->all();

// today proposals on hold
$today_proposals_hold = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['trashed' => null])
    // ->orWhere(['quotation_status' => 10])
    // ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->andFilterWhere(['between', 'on_hold_date', $today_start_date, $today_end_date])
    ->asArray()
    ->all();

// today proposals accepted(label changed to approved)
$today_proposals_accepted = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['trashed' => null])
    // ->orWhere(['quotation_status' => 16])
    // ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->andFilterWhere(['between', 'quotation_accepted_date', $today_start_date, $today_end_date])
    ->asArray()
    ->all();


// today proposals rejected
$today_proposals_rejected = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['trashed' => null])
    // ->orWhere(['quotation_status' => 7])
    // ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->andFilterWhere(['between', 'quotation_rejected_date', $today_start_date, $today_end_date])
    ->asArray()
    ->all();

// today proposals toe sent
$today_toe_sent = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['trashed' => null])
    // ->orWhere(['quotation_status' => 3])
    // ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->andFilterWhere(['between', 'toe_sent_date', $today_start_date, $today_end_date])
    ->asArray()
    ->all();

// today proposals toe signed
$today_toe_signed = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['trashed' => null])
    // ->orWhere(['quotation_status' => 5])
    // ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->andFilterWhere(['between', 'toe_signed_and_received_date', $today_start_date, $today_end_date])
    ->asArray()
    ->all();


// today proposals toe rejected
$today_toe_rejected = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['trashed' => null])
    // ->orWhere(['quotation_status' => 8])
    // ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->andFilterWhere(['between', 'toe_rejected_date', $today_start_date, $today_end_date])
    ->asArray()
    ->all();

// today proposals payment received
$today_payment_received = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['trashed' => null])
    // ->orWhere(['quotation_status' => 6])
    // ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->andFilterWhere(['between', 'payment_received_date', $today_start_date, $today_end_date])
    ->asArray()
    ->all();

// today proposals regretted
$today_proposals_regretted = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['trashed' => null])
    // ->orWhere(['quotation_status' => 12])
    // ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->andFilterWhere(['between', 'regretted_date', $today_start_date, $today_end_date])
    ->asArray()
    ->all();

// today proposals cancelled
$today_quotation_cancelled = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['trashed' => null])
    // ->orWhere(['quotation_status' => 11])
    // ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->andFilterWhere(['between', 'cancelled_date', $today_start_date, $today_end_date])
    ->asArray()
    ->all();


// today toe recommended
$today_toe_recommended = \app\models\CrmQuotations::find()
    ->select([new \yii\db\Expression('SUM(final_quoted_fee) as totalFee , count(id) as totalRecords')])
    ->where(['trashed' => null])
    // ->andWhere(['not', ['id' => \app\models\CrmQuotations::find()->select('parent_id')->where(['not', ['parent_id' => null]])]])
    ->andWhere(['toe_recommended' => 1])
    ->andFilterWhere(['between', 'status_change_date', $today_start_date, $today_end_date])
    ->asArray()
    ->all();

$amountAllowedId = [1, 14, 33];
?>

<div class="container">
    <div class="row mt-4 mb-4">
        <div class="col-lg-4 text-center">
            <button type="button" class="button success" data-toggle="modal" data-target=".bd-example-modal-xl-4">
                <center>
                    <div class=" col-12">
                        <h4 class="card-category">
                            <span class="t-color-2"> Today Proposals Performance </span>
                            <br>
                            <span style="float:left">
                                <!-- <?php //echo number_format($data['inquiry_overview']['total_records']) 
                                        ?> -->
                                <?= number_format($proposals_overview[0]['totalRecords']) ?>
                            </span>
                            <span style="float:right">
                                <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                    <!-- <?php //echo number_format($data['inquiry_overview']['total_amount']) 
                                            ?> -->
                                    <?= number_format($proposals_overview[0]['totalFee']) ?>
                                <?php } ?>
                            </span>
                        </h4>
                    </div>
                </center>
            </button>
        </div>

        <div class="modal fade bd-example-modal-xl-4" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"> Today Proposals Performance </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="card-body" style="background-color:#E5E4E2">
                        <div class="row px-4">


                            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_proposals_overview')) { ?>
                                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                    <div id="w0" class="card-stats">
                                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                            <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #08A86D !important;">
                                                <div class="card-icon" style="font-size:15px; text-align:center">
                                                    <?= date('d'); ?> <br> <?= date('M'); ?>
                                                </div>
                                            </div>
                                            <div class=" col-10 position-absolute" style="right:0px;">
                                                <h4 class="card-category font-increase increase-font text-right " style="color:#616161">
                                                    <span class="pending-text-info"> Today Proposals Overview </span><br>
                                                    <!-- <?php //echo number_format($data['inquiry_overview']['total_records']) 
                                                            ?> -->
                                                    <?= number_format($proposals_overview[0]['totalRecords']) ?>
                                                </h4>
                                                <h4 class=" float-right " style="color:#616161">
                                                    <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                                        <!-- <?php //echo number_format($data['inquiry_overview']['total_amount']) 
                                                                ?> -->
                                                        <?= number_format($proposals_overview[0]['totalFee']) ?>
                                                    <?php } ?>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="card-footer bg-white">
                                            <div class="stats position-relative" style="box-sizing: border-box;">
                                                <i class="fas fa-arrow-right pending-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                <a class="position-absolute pending-text-info footer-increase text-primary-dynamic" style="left:53px;top:5px;" target="_blank" href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-dynamic-index', 'CrmQuotationsSearch[widget_type]' => 'today_dynamic', 'CrmQuotationsSearch[status_type]' => 'inquiry_overview']) ?>">
                                                    <span style="font-size:20px;">Go to Today Proposals Overview</span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_proposals_recommended')) { ?>
                                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                    <div id="w0" class="card-stats">
                                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                            <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #08A86D !important;">
                                                <div class="card-icon" style="font-size:15px; text-align:center">
                                                    <?= date('d'); ?> <br> <?= date('M'); ?>
                                                </div>
                                            </div>
                                            <div class=" col-10 position-absolute" style="right:0px;">
                                                <h4 class="card-category font-increase increase-font text-right" style="color:#616161">
                                                    <span class="pending-text-info">Today Proposals Recommended </span><br>
                                                    <?= number_format($proposals_recommended[0]['totalRecords']) ?>
                                                </h4>
                                                <h4 class=" float-right" style="color:#616161">
                                                    <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                                        <?= number_format($proposals_recommended[0]['totalFee']) ?>
                                                    <?php } ?>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="card-footer bg-white">
                                            <div class="stats position-relative" style="box-sizing: border-box;">
                                                <i class="fas fa-arrow-right pending-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                <a class="position-absolute pending-text-info footer-increase text-primary-dynamic" style="left:53px;top:5px;" target="_blank" href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-dynamic-index', 'CrmQuotationsSearch[widget_type]' => 'today_dynamic', 'CrmQuotationsSearch[status_type]' => 'today_recommended']) ?>">
                                                    <span style="font-size:20px;">Go to Today Proposals Recommended </span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_proposals_documents_requested')) { ?>
                                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                    <div id="w0" class="card-stats">
                                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                            <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #08A86D !important;">
                                                <div class="card-icon" style="font-size:15px; text-align:center">
                                                    <?= date('d'); ?> <br> <?= date('M'); ?>
                                                </div>
                                            </div>
                                            <div class=" col-10 position-absolute" style="right:0px;">
                                                <h4 class="card-category font-increase increase-font text-right" style="color:#616161">
                                                    <span class="pending-text-info"> Today Documents Requested </span><br>
                                                    <?= number_format($today_documents_requested[0]['totalRecords']) ?>
                                                </h4>
                                                <h4 class=" float-right" style="color:#616161">
                                                    <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                                        <?= number_format($today_documents_requested[0]['totalFee']) ?>
                                                    <?php } ?>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="card-footer bg-white">
                                            <div class="stats position-relative" style="box-sizing: border-box;">
                                                <i class="fas fa-arrow-right pending-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                <a class="position-absolute pending-text-info footer-increase text-primary-dynamic" style="left:53px;top:5px;" target="_blank" href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-dynamic-index', 'CrmQuotationsSearch[widget_type]' => 'today_dynamic', 'CrmQuotationsSearch[status_type]' => 'documents_requested']) ?>">
                                                    <span style="font-size:20px;">Go to Today Documents Requested </span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_proposals_verified')) { ?>
                                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                    <div id="w0" class="card-stats">
                                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                            <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #08A86D !important;">
                                                <div class="card-icon" style="font-size:15px; text-align:center">
                                                    <?= date('d'); ?> <br> <?= date('M'); ?>
                                                </div>
                                            </div>
                                            <div class=" col-10 position-absolute" style="right:0px;">
                                                <h4 class="card-category font-increase increase-font text-right" style="color:#616161">
                                                    <span class="pending-text-info"> Today Proposals Verified </span><br>
                                                    <?= number_format($proposals_approved[0]['totalRecords']) ?>
                                                </h4>
                                                <h4 class=" float-right" style="color:#616161">
                                                    <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                                        <?= number_format($proposals_approved[0]['totalFee']) ?>
                                                    <?php } ?>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="card-footer bg-white">
                                            <div class="stats position-relative" style="box-sizing: border-box;">
                                                <i class="fas fa-arrow-right pending-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                <a class="position-absolute pending-text-info footer-increase text-primary-dynamic" style="left:53px;top:5px;" target="_blank" href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-dynamic-index', 'CrmQuotationsSearch[widget_type]' => 'today_dynamic', 'CrmQuotationsSearch[status_type]' => 'quotation_approved']) ?>">
                                                    <span style="font-size:20px;">Go to Today Proposals Verified</span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_quotation_hold')) { ?>
                                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                    <div id="w0" class="card-stats">
                                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                            <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #08A86D !important;">
                                                <div class="card-icon" style="font-size:15px; text-align:center">
                                                    <?= date('d'); ?> <br> <?= date('M'); ?>
                                                </div>
                                            </div>
                                            <div class=" col-10 position-absolute" style="right:0px;">
                                                <h4 class="card-category font-increase increase-font text-right" style="color:#616161">
                                                    <span class="pending-text-info"> Today Proposals On Hold </span><br>
                                                    <?= number_format($today_proposals_hold[0]['totalRecords']) ?>
                                                </h4>
                                                <h4 class=" float-right" style="color:#616161">
                                                    <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                                        <?= number_format($today_proposals_hold[0]['totalFee']) ?>
                                                    <?php } ?>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="card-footer bg-white">
                                            <div class="stats position-relative" style="box-sizing: border-box;">
                                                <i class="fas fa-arrow-right pending-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                <a class="position-absolute pending-text-info footer-increase text-primary-dynamic" style="left:53px;top:5px;" target="_blank" href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-dynamic-index', 'CrmQuotationsSearch[widget_type]' => 'today_dynamic', 'CrmQuotationsSearch[status_type]' => 'quotation_hold']) ?>">
                                                    <span style="font-size:20px;">Go to Today Proposals On Hold</span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_quotation_approved')) { ?>
                                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                    <div id="w0" class="card-stats">
                                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                            <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #08A86D !important;">
                                                <div class="card-icon" style="font-size:15px; text-align:center">
                                                    <?= date('d'); ?> <br> <?= date('M'); ?>
                                                </div>
                                            </div>
                                            <div class=" col-10 position-absolute" style="right:0px;">
                                                <h4 class="card-category font-increase increase-font text-right" style="color:#616161">
                                                    <span class="pending-text-info">Today Proposals Approved</span><br>
                                                    <?= number_format($today_proposals_accepted[0]['totalRecords']) ?>
                                                </h4>
                                                <h4 class=" float-right" style="color:#616161">
                                                    <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                                        <?= number_format($today_proposals_accepted[0]['totalFee']) ?>
                                                    <?php } ?>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="card-footer bg-white">
                                            <div class="stats position-relative" style="box-sizing: border-box;">
                                                <i class="fas fa-arrow-right pending-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                <a class="position-absolute pending-text-info footer-increase text-primary-dynamic" style="left:53px;top:5px;" target="_blank" href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-dynamic-index', 'CrmQuotationsSearch[widget_type]' => 'today_dynamic', 'CrmQuotationsSearch[status_type]' => 'quotation_accepted']) ?>">
                                                    <span style="font-size:20px;">Go to Today Proposals Approved</span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_quotation_rejected')) { ?>
                                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                    <div id="w0" class="card-stats">
                                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                            <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #08A86D !important;">
                                                <div class="card-icon" style="font-size:15px; text-align:center">
                                                    <?= date('d'); ?> <br> <?= date('M'); ?>
                                                </div>
                                            </div>
                                            <div class=" col-10 position-absolute" style="right:0px;">
                                                <h4 class="card-category font-increase increase-font text-right" style="color:#616161">
                                                    <span class="pending-text-info">Today Proposals Rejected</span><br>
                                                    <?= number_format($today_proposals_rejected[0]['totalRecords']) ?>
                                                </h4>
                                                <h4 class=" float-right" style="color:#616161">
                                                    <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                                        <?= number_format($today_proposals_rejected[0]['totalFee']) ?>
                                                    <?php } ?>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="card-footer bg-white">
                                            <div class="stats position-relative" style="box-sizing: border-box;">
                                                <i class="fas fa-arrow-right pending-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                <a class="position-absolute pending-text-info footer-increase text-primary-dynamic" style="left:53px;top:5px;" target="_blank" href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-dynamic-index', 'CrmQuotationsSearch[widget_type]' => 'today_dynamic', 'CrmQuotationsSearch[status_type]' => 'quotation_rejected']) ?>">
                                                    <span style="font-size:20px;">Go to Today Proposals Rejected</span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_quotation_cancelled')) { ?>
                                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                    <div id="w0" class="card-stats">
                                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                            <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #08A86D !important;">
                                                <div class="card-icon" style="font-size:15px; text-align:center">
                                                    <?= date('d'); ?> <br> <?= date('M'); ?>
                                                </div>
                                            </div>
                                            <div class=" col-10 position-absolute" style="right:0px;">
                                                <h4 class="card-category font-increase increase-font text-right" style="color:#616161">
                                                    <span class="pending-text-info">Today Proposals Cancelled</span><br>
                                                    <?= number_format($today_quotation_cancelled[0]['totalRecords']) ?>
                                                </h4>
                                                <h4 class=" float-right" style="color:#616161">
                                                    <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                                        <?= number_format($today_quotation_cancelled[0]['totalFee']) ?>
                                                    <?php } ?>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="card-footer bg-white">
                                            <div class="stats position-relative" style="box-sizing: border-box;">
                                                <i class="fas fa-arrow-right pending-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                <a class="position-absolute pending-text-info footer-increase text-primary-dynamic" style="left:53px;top:5px;" target="_blank" href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-dynamic-index', 'CrmQuotationsSearch[widget_type]' => 'today_dynamic', 'CrmQuotationsSearch[status_type]' => 'today_quotation_cancelled']) ?>">
                                                    <span style="font-size:20px;">Go to Today Proposals Cancelled</span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_toe_verified')) { ?>
                                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                    <div id="w0" class="card-stats">
                                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                            <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #08A86D !important;">
                                                <div class="card-icon" style="font-size:15px; text-align:center">
                                                    <?= date('d'); ?> <br> <?= date('M'); ?>
                                                </div>
                                            </div>
                                            <div class=" col-10 position-absolute" style="right:0px;">
                                                <h4 class="card-category font-increase increase-font text-right" style="color:#616161">
                                                    <span class="pending-text-info">Today TOE Recommended</span><br>
                                                    <?= number_format($today_toe_recommended[0]['totalRecords']) ?>
                                                </h4>
                                                <h4 class=" float-right" style="color:#616161">
                                                    <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                                        <?= number_format($today_toe_recommended[0]['totalFee']) ?>
                                                    <?php } ?>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="card-footer bg-white">
                                            <div class="stats position-relative" style="box-sizing: border-box;">
                                                <i class="fas fa-arrow-right pending-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                <a class="position-absolute pending-text-info footer-increase text-primary-dynamic" style="left:53px;top:5px;" target="_blank" href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-dynamic-index', 'CrmQuotationsSearch[widget_type]' => 'today_dynamic', 'CrmQuotationsSearch[status_type]' => 'toe_recommended']) ?>">
                                                    <span style="font-size:20px;">Go to Today TOE Recommended</span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_toe_signed')) { ?>
                                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                    <div id="w0" class="card-stats">
                                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                            <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #08A86D !important;">
                                                <div class="card-icon" style="font-size:15px; text-align:center">
                                                    <?= date('d'); ?> <br> <?= date('M'); ?>
                                                </div>
                                            </div>
                                            <div class=" col-10 position-absolute" style="right:0px;">
                                                <h4 class="card-category font-increase increase-font text-right" style="color:#616161">
                                                    <span class="pending-text-info">Today TOE Signed</span><br>
                                                    <?= number_format($today_toe_signed[0]['totalRecords']) ?>
                                                </h4>
                                                <h4 class=" float-right" style="color:#616161">
                                                    <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                                        <?= number_format($today_toe_signed[0]['totalFee']) ?>
                                                    <?php } ?>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="card-footer bg-white">
                                            <div class="stats position-relative" style="box-sizing: border-box;">
                                                <i class="fas fa-arrow-right pending-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                <a class="position-absolute pending-text-info footer-increase text-primary-dynamic" style="left:53px;top:5px;" target="_blank" href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-dynamic-index', 'CrmQuotationsSearch[widget_type]' => 'today_dynamic', 'CrmQuotationsSearch[status_type]' => 'toe_signed']) ?>">
                                                    <span style="font-size:20px;">Go to Today TOE Signed</span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_toe_rejected')) { ?>
                                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                    <div id="w0" class="card-stats">
                                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                            <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #08A86D !important;">
                                                <div class="card-icon" style="font-size:15px; text-align:center">
                                                    <?= date('d'); ?> <br> <?= date('M'); ?>
                                                </div>
                                            </div>
                                            <div class=" col-10 position-absolute" style="right:0px;">
                                                <h4 class="card-category font-increase increase-font text-right" style="color:#616161">
                                                    <span class="pending-text-info">Today TOE Rejected</span><br>
                                                    <?= number_format($today_toe_rejected[0]['totalRecords']) ?>
                                                </h4>
                                                <h4 class=" float-right" style="color:#616161">
                                                    <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                                        <?= number_format($today_toe_rejected[0]['totalFee']) ?>
                                                    <?php } ?>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="card-footer bg-white">
                                            <div class="stats position-relative" style="box-sizing: border-box;">
                                                <i class="fas fa-arrow-right pending-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                <a class="position-absolute pending-text-info footer-increase text-primary-dynamic" style="left:53px;top:5px;" target="_blank" href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-dynamic-index', 'CrmQuotationsSearch[widget_type]' => 'today_dynamic', 'CrmQuotationsSearch[status_type]' => 'toe_rejected']) ?>">
                                                    <span style="font-size:20px;">Go to Today TOE Rejected</span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('today_payment_received')) { ?>
                                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                    <div id="w0" class="card-stats">
                                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                            <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #08A86D !important;">
                                                <div class="card-icon" style="font-size:15px; text-align:center">
                                                    <?= date('d'); ?> <br> <?= date('M'); ?>
                                                </div>
                                            </div>
                                            <div class=" col-10 position-absolute" style="right:0px;">
                                                <h4 class="card-category font-increase increase-font text-right" style="color:#616161">
                                                    <span class="pending-text-info">Today Payment Received</span><br>
                                                    <?= number_format($today_payment_received[0]['totalRecords']) ?>
                                                </h4>
                                                <h4 class=" float-right" style="color:#616161">
                                                    <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                                        <?= number_format($today_payment_received[0]['totalFee']) ?>
                                                    <?php } ?>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="card-footer bg-white">
                                            <div class="stats position-relative" style="box-sizing: border-box;">
                                                <i class="fas fa-arrow-right pending-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                <a class="position-absolute pending-text-info footer-increase text-primary-dynamic" style="left:53px;top:5px;" target="_blank" href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-dynamic-index', 'CrmQuotationsSearch[widget_type]' => 'today_dynamic', 'CrmQuotationsSearch[status_type]' => 'payment_received']) ?>">
                                                    <span style="font-size:20px;">Go to Today Payment Received</span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (Yii::$app->menuHelperFunction->checkActionAllowed('windmills_regretted')) { ?>
                                <div class="col-lg-4 col-md-6 col-sm-6 py-3">
                                    <div id="w0" class="card-stats">
                                        <div class=" mx-auto  card-header position-realtive col-md card-header-primary p-4 card-header-icon row bg-white">
                                            <div class="py-2 shadow rounded ml-2 bg-info" style="width:60px; height:60px; background-color: #08A86D !important;">
                                                <div class="card-icon" style="font-size:15px; text-align:center">
                                                    <?= date('d'); ?> <br> <?= date('M'); ?>
                                                </div>
                                            </div>
                                            <div class=" col-10 position-absolute" style="right:0px;">
                                                <h4 class="card-category font-increase increase-font text-right" style="color:#616161">
                                                    <span class="pending-text-info">Windmills Regretted</span><br>
                                                    <?= number_format($today_proposals_regretted[0]['totalRecords']) ?>
                                                </h4>
                                                <h4 class=" float-right" style="color:#616161">
                                                    <?php if (in_array(Yii::$app->user->identity->id, $amountAllowedId)) { ?>
                                                        <?= number_format($today_proposals_regretted[0]['totalFee']) ?>
                                                    <?php } ?>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="card-footer bg-white">
                                            <div class="stats position-relative" style="box-sizing: border-box;">
                                                <i class="fas fa-arrow-right pending-text-info pt-1 pl-3" style="font-size: 24px;"></i>
                                                <a class="position-absolute pending-text-info footer-increase text-primary-dynamic" style="left:53px;top:5px;" target="_blank" href="<?= yii\helpers\Url::to(['crm-quotations/dashboard-dynamic-index', 'CrmQuotationsSearch[widget_type]' => 'today_dynamic', 'CrmQuotationsSearch[status_type]' => 'regretted']) ?>">
                                                    <span style="font-size:20px;">Go to Windmills Regretted</span> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>


                        </div>
                    </div>
                </div>
            </div>
        </div>