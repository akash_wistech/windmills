<?php 
use kartik\select2\Select2;
?>

<section class="card card-outline card-info">
    <header class="card-header">
        <h2 class="card-title"><?= Yii::t('app', 'Verification') ?></h2>
    </header>
    <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-4">
                    <?php
                        echo $form->field($model, 'status_verified')->widget(Select2::classname(), [
                        'data' => array( '2' => 'Unverified','1' => 'Verified'),
                        ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>