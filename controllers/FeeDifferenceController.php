<?php

namespace app\controllers;

use Yii;
use app\models\FeeDifference;
use app\models\FeeDifferenceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\helpers\DefController;

/**
 * FeeDifferenceController implements the CRUD actions for FeeDifference model.
 */
class FeeDifferenceController extends DefController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

 
    public function actionIndex()
    {
        $this->checkLogin();
        $model = FeeDifference::find()->all();
        $searchModel = new FeeDifferenceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->isPjax) {
            return $this->renderPartial('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
        } else {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
        }
    }

    public function actionProposals()
    {
        $this->checkLogin();
        $type = 1;
        $model = FeeDifference::find()->all();
        $searchModel = new FeeDifferenceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $type);

        if (Yii::$app->request->isPjax) {
            return $this->renderPartial('proposals', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
        } else {
            return $this->render('proposals', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
        }
    }

    public function actionValuations()
    {
        $this->checkLogin();
        $type = 2;
        $model = FeeDifference::find()->all();
        $searchModel = new FeeDifferenceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $type);

        if (Yii::$app->request->isPjax) {
            return $this->renderPartial('valuation', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
        } else {
            return $this->render('valuation', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
        }
    }
    
 
    
  
    
    public function actionView($id)
    {
        // return $this->render('view', [
        //     'model' => $this->findModel($id),
        // ]);
    }



    
    public function actionCreate()
    {
        
    }


    
    public function actionUpdate($id)
    {
        
    }

 
    
    public function actionDelete($id)
    {
        // $this->findModel($id)->delete();

        // return $this->redirect(['index']);
    }


    
    protected function findModel($id)
    {
        if (($model = FeeDifference::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    

    
}
