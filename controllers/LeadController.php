<?php

namespace app\controllers;

use Yii;
use app\models\Opportunity;
use app\models\OpportunitySearch;
use app\models\Prospect;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* LeadController implements the CRUD actions for Opportunity model.
*/
class LeadController extends OpportunityController
{
  public $rec_type='l';
  public $savedMsg='Lead saved successfully';
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Creates a new Opportunity model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkSuperAdmin();
    $model = new Opportunity();
    // $model->scenario='leadcreate';
    $model->rec_type=$this->rec_type;

    if ($model->load(Yii::$app->request->post())) {
      $prospect = new Prospect;
      $prospect->load(Yii::$app->request->post());
      $prospect->firstname=$model->firstname;
      $prospect->lastname=$model->lastname;
      $prospect->email=$model->email;
      $prospect->mobile=$model->mobile;
      $prospect->company_name=$model->company_name;
      $prospect->manager_id = $model->manager_id;
      $prospect->tags = $model->tags;
      $prospect->save();
      $model->module_type=$prospect->moduleTypeId;
      $model->module_keyword=$prospect->name;
      $model->module_id=$prospect->id;
      if($model->save()){
        Yii::$app->getSession()->setFlash('success', Yii::t('app',$this->savedMsg));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>$val];
                echo json_encode($msg);
                die();
                exit;
              }
            }
          }
        }
      }
    }
    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
  * Updates an existing Opportunity model.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionUpdate($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);
    $model->manager_id = ArrayHelper::map($model->managerIdz,"staff_id","staff_id");
    $prospect=$model->prospect;
    if($prospect!=null){
      $model->firstname=$prospect->firstname;
      $model->lastname=$prospect->lastname;
      $model->email=$prospect->email;
      $model->mobile=$prospect->mobile;
      $model->company_name=$prospect->company_name;
    }

    if ($model->load(Yii::$app->request->post())) {
      $prospect = Prospect::findOne($model->module_id);
      $prospect->load(Yii::$app->request->post());
      $prospect->firstname=$model->firstname;
      $prospect->lastname=$model->lastname;
      $prospect->email=$model->email;
      $prospect->mobile=$model->mobile;
      $prospect->company_name=$model->company_name;
      $prospect->manager_id = $model->manager_id;
      $prospect->tags = $model->tags;
      $prospect->save();
      $model->module_keyword=$prospect->name;
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app',$this->savedMsg));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('update', [
      'model' => $model,
    ]);
  }
}
