<?php

namespace app\controllers;

use app\models\BilalContacts;
use app\models\Company;
use app\models\ScheduleInspectionSearch;
use app\models\UserProfileInfo;
use Yii;
use app\models\User;
use app\models\ContactSearch;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
* ContactController implements the CRUD actions for User model.
*/
class ContactController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        // Check if the user is logged in
        if (Yii::$app->user->isGuest) {
            // Redirect to the login page
            return Yii::$app->response->redirect(['site/login'])->send();
        }
    }
  /**
  * Lists all Staff models.
  * @return mixed
  */
  public function actionIndex()
  {
    // $this->checkAdmin();
    $searchModel = new ContactSearch();
    if (Yii::$app->user->identity->permission_group_id==15) {
      $searchModel->cname=Yii::$app->user->identity->company->title;
      $user=Yii::$app->user->identity;
    }

    $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
        'grid_title'=> 'All Contacts'
      // 'user' => $user,
    ]);
  }


    /**
     * Lists all Staff models.
     * @return mixed
     */
    public function actionAllContacts()
    {
        // $this->checkAdmin();
        $searchModel = new ContactSearch();
        if (Yii::$app->user->identity->permission_group_id==15) {
            $searchModel->cname=Yii::$app->user->identity->company->title;
            $user=Yii::$app->user->identity;
        }
        // $searchModel->bilal_contact=1;
        $dataProvider = $searchModel->searchMyall(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            // 'user' => $user,
            'create_url'=> '',
            'grid_title'=> 'All Contacts'

        ]);
    }
    public function actionExitingValuationContacts()
    {
        // $this->checkAdmin();
        $searchModel = new ContactSearch();
        if (Yii::$app->user->identity->permission_group_id==15) {
            $searchModel->cname=Yii::$app->user->identity->company->title;
            $user=Yii::$app->user->identity;
        }
        $searchModel->existing_valuation_contact=1;
        $dataProvider = $searchModel->searchMyexisting(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url'=> '',
            'grid_title'=> 'Exiting Valuation Contacts'
            // 'user' => $user,
        ]);
    }

    public function actionBilalContacts()
    {
        // $this->checkAdmin();
        $searchModel = new ContactSearch();
        if (Yii::$app->user->identity->permission_group_id==15) {
            $searchModel->cname=Yii::$app->user->identity->company->title;
            $user=Yii::$app->user->identity;
        }
       // $searchModel->bilal_contact=1;
        $dataProvider = $searchModel->searchMyBilal(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url'=> 'client/create-boss-contact',
            'grid_title'=> "Bilal's Contacts"
            // 'user' => $user,
        ]);
    }
    public function actionBankContacts()
    {
        // $this->checkAdmin();
        $searchModel = new ContactSearch();
        if (Yii::$app->user->identity->permission_group_id==15) {
            $searchModel->cname=Yii::$app->user->identity->company->title;
            $user=Yii::$app->user->identity;
        }
        // $searchModel->bilal_contact=1;
        $dataProvider = $searchModel->searchMyBank(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url'=> 'client/create-valuation-inquiry-contact',
            'grid_title'=> "Contacts List of Banks"
            // 'user' => $user,
        ]);
    }

    public function actionValuationContacts()
    {
        // $this->checkAdmin();
        $searchModel = new ContactSearch();
        if (Yii::$app->user->identity->permission_group_id==15) {
            $searchModel->cname=Yii::$app->user->identity->company->title;
            $user=Yii::$app->user->identity;
        }
        $searchModel->valuation_contact=1;
        $dataProvider = $searchModel->searchMyValuationContacts(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url'=> '',
            'grid_title'=> "Valuation Contacts"
            // 'user' => $user,
        ]);
    }

    public function actionValuationInspectionContacts()
    {
        // $this->checkAdmin();
        $searchModel = new ScheduleInspectionSearch();
       // $searchModel->valuation_contact=1;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index_valuation', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url'=> '',
            'grid_title'=> "Valuation Inspections Contacts"
        ]);
    }

    public function actionProspectiveContacts()
    {
        // $this->checkAdmin();
        $searchModel = new ContactSearch();
        if (Yii::$app->user->identity->permission_group_id==15) {
            $searchModel->cname=Yii::$app->user->identity->company->title;
            $user=Yii::$app->user->identity;
        }
        $searchModel->prospect_contact=1;
        $dataProvider = $searchModel->searchMyProspectiveContacts(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url'=> 'client/create-prospect-contact',
            'grid_title'=> "Prospect Contacts"
            // 'user' => $user,
        ]);
    }

    public function actionPropertyOwnersContacts()
    {



       /* $all_contacts = (new \yii\db\Query())
         //   ->select('user.id')
            ->select('user.id as userid,company.id as companyid,user_profile_info.id as pid,company.title as company_name,user.email, user.firstname, user.lastname,user_profile_info.job_title_id,user_profile_info.mobile,user_profile_info.phone1 as landline,user_profile_info.phone2 as mobile_2,user_profile_info.linked_in_page')
            ->from('user')
            ->leftJoin('user_profile_info',"user.id=user_profile_info.user_id")
            ->leftJoin('company',"user.company_id=company.id")
            ->andWhere(['user.bilal_contact' => 1])
           // ->limit('10')
            ->andWhere(['not', ['user.existing_valuation_contact' => 1]])
            ->andWhere(['not', ['user.company_id' => 0]])
            ->all();

        $phone_number_array = array('50','52','54','55','56','58');
        $land_number_array = array('2','3','4','6','7','9');
        foreach ($all_contacts as $all_contact){
         /*   User::findOne($all_contact['userid'])->delete();
            UserProfileInfo::findOne($all_contact['pid'])->delete();
            Company::findOne($all_contact['companyid'])->delete();*/

          /*  if($all_contact['job_title_id'] <> null) {
                $job_title = \app\models\JobTitle::find()->select(['title'])->where(['id' => $all_contact['job_title_id']])->one();
            }

            $model_new = new BilalContacts();
            $model_new->company = $all_contact['company_name'];
            $model_new->firstname = $all_contact['firstname'];
            $model_new->lastname = $all_contact['lastname'];
            $model_new->job_title_id = $all_contact['job_title_id'];
            if(isset($job_title->title) && $job_title->title <> null){
                $model_new->jobtitle = $job_title->title;
            }

            $model_new->email = $all_contact['email'];

            if($all_contact['mobile'] <> null && $all_contact['mobile'] != '') {
                $str_m = $all_contact['mobile'];
                $withoutsevendigits_m = substr($str_m, 0, -7);
                $exact_seven_digits_m = substr($str_m, -7);
                $withoutsevendigits_digits_1_m = substr($withoutsevendigits_m, -1);
                $withoutsevendigits_digits_2_m = substr($withoutsevendigits_m, -2);

                if (in_array($withoutsevendigits_digits_2_m, $phone_number_array)) {
                    $model_new->phone_code = $withoutsevendigits_digits_2_m;
                    $model_new->phone = $exact_seven_digits_m;
                } else if (in_array($withoutsevendigits_digits_1_m, $land_number_array)) {

                    $model_new->land_line_code = $withoutsevendigits_digits_1_m;
                    $model_new->landline = $exact_seven_digits_m;
                }
            }

            if($all_contact['landline'] <> null && $all_contact['landline'] != '') {
                $str_land = $all_contact['landline'];
                $withoutsevendigits_land = substr($str_land, 0, -7);
                $exact_seven_digits_land = substr($str_land, -7);
                $withoutsevendigits_digits_1_land = substr($withoutsevendigits_land, -1);
                $withoutsevendigits_digits_2_land = substr($withoutsevendigits_land, -2);

                if (in_array($withoutsevendigits_digits_2_land, $phone_number_array)) {
                    $model_new->phone_code = $withoutsevendigits_digits_2_land;
                    $model_new->phone = $exact_seven_digits_land;
                } else if (in_array($withoutsevendigits_digits_1_land, $land_number_array)) {

                    $model_new->land_line_code = $withoutsevendigits_digits_1_land;
                    $model_new->landline = $exact_seven_digits_land;
                }
            }

            $model_new->linkedin = $all_contact['linked_in_page'];*/

        //    if(!$model_new->save()){
             /*   echo "<pre>";
                print_r($model_new->errors);
                die;*/
         //   }
          //  die('here');

      //  }
//die('as');




  //      echo "<pre>";
  //      print_r(count($all_contacts));
  //      die;




        // $this->checkAdmin();
        $searchModel = new ContactSearch();
        if (Yii::$app->user->identity->permission_group_id==15) {
            $searchModel->cname=Yii::$app->user->identity->company->title;
            $user=Yii::$app->user->identity;
        }
        $searchModel->property_owner_contact=1;
        $dataProvider = $searchModel->searchMyPropertyOwnersContacts(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url'=> 'client/create-property-owners-contact',
            'grid_title'=> "Property Owners Contacts"
            // 'user' => $user,
        ]);
    }

    public function actionValuationInquiryContacts()
    {
        // $this->checkAdmin();
        $searchModel = new ContactSearch();
        if (Yii::$app->user->identity->permission_group_id==15) {
            $searchModel->cname=Yii::$app->user->identity->company->title;
            $user=Yii::$app->user->identity;
        }
        $searchModel->inquiry_valuations_contact=1;
        $dataProvider = $searchModel->searchMyValuationInquiryContacts(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url'=> 'client/create-valuation-inquiry-contact',
            'grid_title'=> "Valuation Inquiry Contacts"
            // 'user' => $user,
        ]);
    }

    public function actionIciaContacts()
    {
        // $this->checkAdmin();
        $searchModel = new ContactSearch();
        if (Yii::$app->user->identity->permission_group_id==15) {
            $searchModel->cname=Yii::$app->user->identity->company->title;
            $user=Yii::$app->user->identity;
        }
        $searchModel->icai_contact=1;
        $dataProvider = $searchModel->searchMyIciaContacts(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url'=> 'client/create-icia-contact',
            'grid_title'=> "Icia Contacts"
            // 'user' => $user,
        ]);
    }
    public function actionBrokerContacts()
    {
        // $this->checkAdmin();
        $searchModel = new ContactSearch();
        if (Yii::$app->user->identity->permission_group_id==15) {
            $searchModel->cname=Yii::$app->user->identity->company->title;
            $user=Yii::$app->user->identity;
        }
        $searchModel->broker_contact=1;
        $dataProvider = $searchModel->searchMyBrokerContacts(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url'=> 'client/create-broker-contact',
            'grid_title'=> "Broker Contacts"
            // 'user' => $user,
        ]);
    }

    public function actionDevelopersContacts()
    {
        // $this->checkAdmin();
        $searchModel = new ContactSearch();
        if (Yii::$app->user->identity->permission_group_id==15) {
            $searchModel->cname=Yii::$app->user->identity->company->title;
            $user=Yii::$app->user->identity;
        }
        $searchModel->developer_contact=1;
        $dataProvider = $searchModel->searchMyDevelopersContacts(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url'=> 'client/create-developers-contact',
            'grid_title'=> "Developers Contacts"
            // 'user' => $user,
        ]);
    }



  /**
  * Creates a new Staff model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkSuperAdmin();
    $model = new User();
    $model->user_type=0;
    $model->status=1;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }
    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
  * Updates an existing Staff model.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionUpdate($id,$length = 8)
  {
    $this->checkLogin();
    $model = $this->findModel($id);
    $model->oldfile=$model->image;
    $model->manager_id = ArrayHelper::map($model->managerIdz,"staff_id","staff_id");

    if ($model->load(Yii::$app->request->post())) {
      if ($model->password!=null) {
        $model->password_hash = Yii::$app->security->generatePasswordHash($model->password);
        $model->permission_group_id = 15;
        $model->verified = 1;
      }

      if($model->save()){
        if ($model->password!=null) {
        $notifyData = [
           'client' => $model->company,
           'attachments' => [],
           'replacements'=>[
            '{clientName}'=> $model->firstname.' '.$model->lastname,
            '{link}'=> Url::toRoute(['site/login']),
            '{Username}'=> $model->email,
            '{password}'=> $model->password,
          ],
        ];
      //  \app\modules\wisnotify\listners\NotifyEvent::fire('client.login', $notifyData);
          Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully, Login Details have been sent to Client.'));  
        }
        else{      
          Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully.'));
          return $this->redirect(['index']);
        }
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('update', [
      'model' => $model,
    ]);
  }

  /**
  * Creates a new model.
  */
  protected function newModel()
  {
    $model = new User;
    return $model;
  }

  /**
  * Finds the User model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Staff the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = User::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }


  function actionGenerateRandomString($id,$length = 8) 
  {

    $user= User::findOne($id);


    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    Yii::$app->db->createCommand()->update('user', ['password_hash' =>Yii::$app->security->generatePasswordHash($randomString),'permission_group_id'=>15,'verified'=>1], 'id='.$id)->execute();

    $notifyData = [
      'client' => $user->company,
      // 'attachments' => $attachments,
      'replacements'=>[
        '{clientName}'=> $user->firstname.' '.$user->lastname,
        '{link}'=> Url::toRoute(['site/login']),
        '{Username}'=> $user->email,
        '{password}'=> $randomString,
      ],
    ];

   // \app\modules\wisnotify\listners\NotifyEvent::fire('client.login', $notifyData);
    Yii::$app->getSession()->setFlash('success', Yii::t('app','Login Details have been sent to Client.'));
    return $this->redirect(Yii::$app->request->referrer);


  }
  



}
