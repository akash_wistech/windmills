<?php

namespace app\controllers;

use Yii;
use app\models\Workflow;
use app\models\WorkflowSearch;
use app\models\WorkflowStage;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* WorkflowController implements the CRUD actions for Workflow model.
*/
class WorkflowController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all Workflow models.
  * @return mixed
  */
  public function actionIndex()
  {
    $this->checkAdmin();
    $searchModel = new WorkflowSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new Workflow model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkSuperAdmin();
    $model = new Workflow();

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Process saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }
    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
  * Updates an existing Workflow model.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionUpdate($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);
    $model->assigned_modules=ArrayHelper::map($model->workflowSelectedModules,"module_type","module_type");
    $model->assigned_services=ArrayHelper::map($model->workflowSelectedServices,"service_type","service_type");

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Process saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('update', [
      'model' => $model,
    ]);
  }

  /**
  * Creates a new model.
  */
  protected function newModel()
  {
    $model = new Workflow;
    return $model;
  }

  /**
   * Deletes an existing WorkflowStage model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionDeleteStage($id)
  {
    $this->checkLogin();
    $this->findStageModel($id)->softDelete();
    $msg['success']=['heading'=>Yii::t('app','Success'),'msg'=>Yii::t('app','Stage deleted successfully')];
    echo json_encode($msg);
    die();
    exit;
  }

  /**
  * Finds the Workflow model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Workflow the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = Workflow::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }

  /**
  * Finds the WorkflowStage model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Workflow the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findStageModel($id)
  {
    if (($model = WorkflowStage::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
