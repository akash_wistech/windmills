<?php

namespace app\controllers;

use Yii;

use yii\filters\VerbFilter;
use app\components\helpers\DefController;
use app\models\KpiManager;
use app\models\KpiManagerSearch;
use yii\base\DynamicModel;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
USE app\models\UserProfileInfo;
use app\models\Department;
use app\models\Units;
use yii\helpers\Url;
use app\models\User;
use yii\helpers\Json;

/**
 * KpiManagerController implements the CRUD actions for Tasks model.
 */
class KpiManagerController extends DefController
{

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);        
        if (Yii::$app->user->isGuest) {
            return Yii::$app->response->redirect(['site/login'])->send();
        }

    }


    public function actionIndex()
    {
        $searchModel = new KpiManagerSearch();
        echo "<pre>";
        print_r(Yii::$app->request->queryParams);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);        
        $departments = Department::find()
            ->select(['department.id', 'department.title'])
            ->innerJoin('kpi_manager', 'department.id = kpi_manager.department')
            ->asArray()
            ->all();
        
        if (empty($departments)) {
            $departments = [['id' => 0, 'title' => 'No Departments Available']];
        }
        
        $units = Units::find()
            ->select(['units.id', 'units.title'])
            ->innerJoin('kpi_manager', 'units.id = kpi_manager.unit')
            ->asArray()
            ->all();
        
        if (empty($units)) {
            $units = [['id' => 0, 'title' => 'No Departments Available']];
        }
        
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'departments' => $departments,
            'units' => $units,
        ]);
        
    }

    public function actionGetdata($employe_id)
    {

        $currentUserId = $employe_id;

        $currentDepartmentId = UserProfileInfo::find()
            ->where(['user_id' => $currentUserId])
            ->select('department_id')
            ->scalar();

        $departmentDetails = null;
        if ($currentDepartmentId) {
            $departmentDetails = Department::find()
                ->where(['id' => $currentDepartmentId])
                ->one();
        }

        if (empty($departmentDetails)) {
            $departmentDetails = new Department();
            $departmentDetails->title = "Not Assigned";
        }

        // gettin the unit id by doing a join with kpi manager table
        $currentUnitId = UserProfileInfo::find()
            ->where(['user_id' => $currentUserId])
            ->select('unit_id')
            ->scalar();

        $unittDetails = null;
        if ($currentUnitId) {
            $unittDetails = Units::find()
                ->where(['id' => $currentUnitId])
                ->one();
        }

        if (empty($unittDetails)) {
            $unittDetails = new Units();
            $unittDetails->title = "Not Assigned";
        }


        // Prepare data to send back
        $responseData = [
            'departmentDetails' => $departmentDetails,
            'unittDetails' => $unittDetails
        ];

        // Encode data as JSON and return
        return Json::encode($responseData);


    }

    public function actionCreate()
    {
        $model = new KpiManager();
        $currentUserId = Yii::$app->user->identity->id;
        
        $currentDepartmentId = UserProfileInfo::find()
            ->where(['user_id' => $currentUserId])
            ->select('department_id')
            ->scalar();
        
        $departmentDetails = null;
        if ($currentDepartmentId) {
            $departmentDetails = Department::find()
                ->where(['id' => $currentDepartmentId])
                ->one();
        }
        
        if (empty($departmentDetails)) {
            $departmentDetails = new Department(); 
            $departmentDetails->title = "Not Assigned";
        }
        
        // gettin the unit id by doing a join with kpi manager table
        $currentUnitId = UserProfileInfo::find()
            ->where(['user_id' => $currentUserId])
            ->select('unit_id')
            ->scalar();
        
        $unittDetails = null;
        if ($currentUnitId) {
            $unittDetails = Units::find()
                ->where(['id' => $currentUnitId])
                ->one();
        }
        
        if (empty($unittDetails)) {
            $unittDetails = new Units(); 
            $unittDetails->title = "Not Assigned";
        }


        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){

            }else{
               /* echo "<pre>";
                print_r($model->errors);
                die;*/
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }
        
        return $this->render('create', [
            'model' => $model,
            'departmentDetails' => $departmentDetails,
            'unittDetails' => $unittDetails
        ]);
        
    }


    // 
    // Store ecords
    // 
    // 

    public function actionStore()
    {   
        $user_id = Yii::$app->user->identity->id; 
        $validation = DynamicModel::validateData(Yii::$app->request->post(),[
                ['department', 'required'],
                ['unit', 'required'],
                ['description','required'],
                ['description', 'string', 'max' => 300, 'tooLong' => 'Description cannot be greater than 300 characters.'],
                ['reference_number','required'],
                ['start_date','required'],
                ['end_date','required'],
                ['employe','required'],
        ]);

        if ($validation->hasErrors()) {
            Yii::$app->session->setFlash('old', Yii::$app->request->post());
            Yii::$app->session->setFlash('errors', $validation->errors);
            Yii::$app->session->setFlash('error', 'Validation Failed');
            return $this->redirect(Url::to(['/kpi-manager/create']));
        } 

        $StartDate = new \DateTime(Yii::$app->request->post()['start_date']);
        $formatStartDate = $StartDate->format('j M Y');
        $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
        $formatEndDate = $EndDate->format('j M Y');

        if($formatStartDate > $formatEndDate)
        {
            Yii::$app->session->setFlash('old', Yii::$app->request->post());
            Yii::$app->getSession()->addFlash('error', "Validation Failed");
            Yii::$app->session->setFlash('errors', [
                'end_date' => ["End Date Must be greater than Start Date!"]
            ]);

            return $this->redirect(Url::to(['/kpi-manager/create']));
        }

        if (isset(Yii::$app->request->post()['remaining_weightage'])) {
            $remainingWeightage = str_replace('%', '',Yii::$app->request->post()['remaining_weightage']);
            $remainingWeightage = (float)$remainingWeightage;
            if ($remainingWeightage < 0) {

                Yii::$app->session->setFlash('old', Yii::$app->request->post());
                Yii::$app->session->setFlash('errors', [
                 'remaining_weightage' => [
                    "Remaining weightage cannot be lower than 0"
                  ]
                ]);
                Yii::$app->getSession()->addFlash('error', "Validation Failed");
                return $this->redirect(Url::to(['/kpi-manager/create']));
            }
        }

        // Validation Complete
        $department  = Yii::$app->request->post()['department'];
        $response = (new \yii\db\Query())
        ->select('kpi_manager.sequence')
        ->from('kpi_manager')
        ->where(['kpi_manager.department' => $department])
        ->orderBy(['kpi_manager.sequence' => SORT_DESC])
        ->limit(1)
        ->one();

        if($response){
            $sequence = $response['sequence'] + 1;      
        }else{
            $sequence = 1;
        }
        
        $model = new KpiManager();
        $model->department = Yii::$app->request->post()['department'];
        $model->unit = Yii::$app->request->post()['unit'];
        $model->type = 'kpi';
        $model->achievement_score = Yii::$app->request->post()['achievement_score'];
        $model->sequence = $sequence;
        $model->reference_number =  Yii::$app->request->post()['reference_number'];
        $model->weightage = Yii::$app->request->post()['weightage'];
        $model->description = Yii::$app->request->post()['description'];
        $model->start_date = Yii::$app->request->post()['start_date'];
        $model->end_date = Yii::$app->request->post()['end_date'];
        $model->employe = Yii::$app->request->post()['employe'];
       
        $model->approved_by = $user_id;

        $model->created_by = $user_id;
        $model->created_at = date("Y/m/d H:i:s");
        // dd($model->department);
        $model->save();
        if($model->save()){

            $model = KpiManager::findOne($model->id);
            $reportedTo = UserProfileInfo::find()
                ->where(['user_id' => $model->employe]) 
                ->select('reports_to')
                ->scalar();

            $reportedToEmail = User::find()
            ->where(['id' => $reportedTo]) 
            ->select('email') 
            ->scalar();

            $subject = "New KPI Created". $model->reference_number;
            $body = "
                Hello,<br><br>
                A new KPI has been created with the following details:<br>
                <strong>Department:</strong> {$model->department}<br>
                <strong>Unit:</strong> {$model->unit}<br>
                <strong>Reference Number:</strong> {$model->reference_number}<br>
                <strong>Description:</strong> {$model->description}<br>
                <strong>Start Date:</strong> {$model->start_date}<br>
                <strong>End Date:</strong> {$model->end_date}<br>
                <strong>Weightage:</strong> {$model->weightage}%<br><br>
                Please review and take the necessary actions.
            ";

            if (!empty($reportedToEmail)) {
                Yii::$app->mailer->compose()
                    ->setTo($reportedToEmail)
                    ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['companyName']]) 
                    ->setSubject($subject)
                    ->setHtmlBody($body) 
                    ->send();
            } else {
                Yii::$app->session->setFlash('error', 'Unable to send email: Reports-to user email not found.');
            }
            Yii::$app->session->setFlash('success', 'Record Updated Successfully');
            return $this->redirect(['/kpi-manager/']);
        }
        else{
            Yii::$app->session->setFlash('error', 'Please review the fields');
            return $this->redirect(['/kpi-manager/create/']);
        }
            
    }



    // 
    // Edit Records
    // 

    public function actionUpdate($id)
    {
        $model = KpiManager::findOne($id);
        $reportedTo = UserProfileInfo::find()
            ->where(['user_id' => $model->employe])
            ->select('reports_to')
            ->scalar();
        
        $currentUserId = Yii::$app->user->identity->id;
        
        $currentDepartmentId = UserProfileInfo::find()
            ->where(['user_id' => $currentUserId])
            ->select('department_id')
            ->scalar();
        
        $departmentDetails = null;
        if ($currentDepartmentId) {
            $departmentDetails = Department::find()
                ->where(['id' => $currentDepartmentId])
                ->one(); 
        }
        
        $departmentTitle = Department::find()
            ->where(['id' => $model->department])
            ->select('title')
            ->scalar();
        
        
        if (empty($departmentTitle)) {
            $departmentTitle = "Not Specified"; 
        }

        // gettin the unit id by doing a join with kpi manager table
        $currentUnitId = UserProfileInfo::find()
        ->where(['user_id' => $currentUserId])
        ->select('unit_id')
        ->scalar();
        
        $unittDetails = null;
        if ($currentUnitId) {
            $unittDetails = Units::find()
                ->where(['id' => $currentUnitId])
                ->one();
        }

        $unitTitle = Units::find()
        ->where(['id' => $model->unit])
        ->select('title')
        ->scalar();
    
        
        if (empty($unittDetails)) {
            $unittDetails = new Units(); 
            $unittDetails->title = "Not Assigned";
        }

        
        if (empty($reportedTo)) {
            $reportedTo = "1"; 
        }
        
        if (empty($departmentDetails)) {
            $departmentDetails = new Department(); 
            $departmentDetails->title = "Not Specified";
        }
        
        return $this->render('update', [
            'model' => $model,
            'reportedTo' => $reportedTo,
            'departmentDetails' => $departmentDetails,
            'departmentTitle' => $departmentTitle,
            'unitTitle' => $unitTitle
        ]);
        
    }



    // 
    // Update Record
    // 
    // 

    public function actionUpdatedata($id)
    {

        $model = KpiManager::findOne(['id' => $id]);
        if(!$model){
            Yii::$app->getSession()->addFlash('error', "Record Not Found");
            return $this->redirect(['index']);
        }

        // dd(Yii::$app->request->post()['start_date']);

        $validation = DynamicModel::validateData(Yii::$app->request->post(),[
                // ['weightage','required'],    
                ['description','required'],
                ['description', 'string', 'max' => 300, 'tooLong' => 'Description cannot be greater than 300 characters.'],
                ['start_date','required'],
                ['end_date','required'],
        ]);

        if ($validation->hasErrors()) {
            Yii::$app->session->setFlash('old', Yii::$app->request->post());
            Yii::$app->session->setFlash('errors', $validation->errors);
            Yii::$app->session->setFlash('error', 'Validation Failed');
            return $this->redirect(Url::to(['/kpi-manager/update/']).'?id='.$model->id);
        } 


        //Date Validation 
        $StartDate = new \DateTime(Yii::$app->request->post()['start_date']);
        $formatStartDate = $StartDate->format('d-m-Y');
        $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
        $formatEndDate = $EndDate->format('d-m-Y');

        if($formatStartDate > $formatEndDate)
        {
            Yii::$app->session->setFlash('old', Yii::$app->request->post());
            Yii::$app->getSession()->addFlash('error', "Validation Failed");
            Yii::$app->session->setFlash('errors', [
                'end_date' => ["End Date Must be greater than Start Date!"]
            ]);

            return $this->redirect(Url::to(['/kpi-manager/update/']).'?id='.$model->id);
        }


        //Weightage Validation
        if (isset(Yii::$app->request->post()['remaining_weightage'])) {
            $remainingWeightage = str_replace('%', '',Yii::$app->request->post()['remaining_weightage']);
            $remainingWeightage = (float)$remainingWeightage;
            if ($remainingWeightage < 0) {

                Yii::$app->session->setFlash('old', Yii::$app->request->post());
                Yii::$app->session->setFlash('errors', [
                 'remaining_weightage' => [
                    "Remaining weightage cannot be lower than 0"
                  ]
                ]);
                Yii::$app->getSession()->addFlash('error', "Validation Failed");
                return $this->redirect(Url::to(['/kpi-manager/update/']).'?id='.$model->id);
            }
        }

        $model->description = Yii::$app->request->post()['description'];
        $model->start_date = Yii::$app->request->post()['start_date'];
        $model->end_date = Yii::$app->request->post()['end_date'];
        $model->weightage = Yii::$app->request->post()['weightage'];
        $model->achievement_score = Yii::$app->request->post()['achievement_score']; 
        $model->updated_at = date("Y/m/d H:i:s");
        $model->save();

        Yii::$app->getSession()->addFlash('success', "Record Updated Successfully");
        return $this->redirect(Url::to(['/kpi-manager/']));

    }


    public function actionDelete($id)
    {
         $model = KpiManager::findOne($id);
    
         if (!$model) {
             Yii::$app->session->setFlash('error', 'The requested record does not exist.');
         } else {
            
             if ($model->delete()) {
                 Yii::$app->session->setFlash('success', 'Record deleted successfully.');
             } else {
                 Yii::$app->session->setFlash('error', 'Failed to delete the record.');
             }
         }
     
         return $this->redirect(['index']);
        
    }

    public function actionWeightage(){

        $employe  = Yii::$app->request->get('emp_id');
        $model_id  = Yii::$app->request->get('model_id');

        $user_info = (new \yii\db\Query())
        ->select('kpi_manager.weightage')
        ->from('kpi_manager')
        ->where(['kpi_manager.employe' => $employe])
        ->all();

        $weightages = array_sum(array_column($user_info, 'weightage'));

        if($model_id){

            $current = (new \yii\db\Query())
            ->select('kpi_manager.weightage')
            ->from('kpi_manager')
            ->where(['kpi_manager.employe' => $employe])
            ->where(['kpi_manager.id' => $model_id])
            ->limit(1)
            ->one();

            $weightages = $weightages - intval($current['weightage']);

        }

        header('Content-Type: application/json');
        $response = [
            'status' => 'success',
            'message' => 'Data retrieved successfully',
            'data' => [
                'total_weightage' => 100 - $weightages,
            ]
        ];

        echo json_encode($response);

    }



    public function actionDeptprefix()
    {
        $department = Yii::$app->request->get('dept_id');
    
        // Count total records
        $totalRecords = (new \yii\db\Query())
            ->from('kpi_manager')
            ->count();
    
        // Generate the next sequence
        $nextSequence = $totalRecords + 1;
    
        // Generate prefix based on department
        $prefix = $this->prefix($department) . '-' . str_pad($nextSequence, 3, '0', STR_PAD_LEFT);
    
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return [
            'status' => 'success',
            'message' => 'Data retrieved successfully',
            'data' => [
                'prefix_number' => $prefix,
            ],
        ];
    }

    public function prefix($dept){

        if($dept == "operations" || ($dept =="Operations")){
            return "OPT";
        }elseif($dept == "Finance"){
            return "FIN";
        }elseif($dept == "Human Resource"){
            return "HRM";
        }elseif($dept == "Maxima"){
            return "MAX";
        }elseif($dept == "Administration"){
            return "ADM";
        }elseif($dept == "ITD"){
            return "ITD";
        }elseif($dept == "Services"){
            return "SR";
        }elseif($dept == "Valuation"){
            return "VAL";
        }
        elseif($dept == "Valuation Department"){
            return "VAD";
        }
        elseif($dept == "Support"){
            return "SPT";
        }elseif($dept == "Data Management"){
            return "DTM";
        }elseif($dept == "Sales"){
            return "SAL";
        }elseif($dept == "Marketing"){
            return "MAR";
        }elseif($dept == "Empanelment"){
            return "EMP";
        }elseif($dept == "Business"){
            return "BUS";
        }elseif($dept == "Bcs"){
            return "BCS";
        }elseif($dept == "pak"){
            return "PAK";
        }elseif($dept == "sa"){
            return "SA";
        }elseif($dept == "Management"){
            return "MG";
        }elseif($dept == "Compliance"){
            return "COM";
        }
        elseif($dept == "Chief Business Officer"){
            return "CBO";
        }
        elseif($dept == "Chief Executive Officer"){
            return "CEO";
        }
        elseif($dept == "Chief Services Officer"){
            return "CSO";
        }elseif($dept == "Valuation Support"){
            return "VLS";
        }elseif($dept == "Inspection"){
            return "INS";
        }elseif($dept == "Data"){
            return "DAT";
        }
        elseif($dept == "Chief Pakistan Officer"){
            return "CPO";
        }
        elseif($dept == "Software Development"){
            return "SOD";
        }  /*elseif($dept == "IT"){
            return "ITD";
        }*/
        else{
            if($dept <> null){
                return strtoupper(substr($dept, 0, 2));
            }
            return "none";
        }
    }
    
    
    public function prefix_old($dept){

        if($dept == "operations" || ($dept =="Operations")){
            return "OPT";
        }elseif($dept == "Finance"){
            return "FIN";
        }elseif($dept == "Human Resource"){
            return "HRM";
        }elseif($dept == "Maxima"){
            return "MAX";
        }elseif($dept == "Administration"){
            return "ADM";
        }elseif($dept == "ITD"){
        return "ITD";
        }elseif($dept == "Service"){
            return "SR";
        }elseif($dept == "Valuations"){
            return "VAL";
        }
        elseif($dept == "Valuation Department"){
            return "VAD";
        }    
        elseif($dept == "Support"){
            return "SPT";
        }elseif($dept == "Data Management"){
            return "DTM";
        }elseif($dept == "Sales"){
            return "SAL";
        }elseif($dept == "Marketing"){
          return "MAR";
        }elseif($dept == "Empanelment"){
            return "EMP";
        }elseif($dept == "Business Development"){
            return "BDM";
        }elseif($dept == "Bcs"){
            return "BCS";
        }elseif($dept == "pak"){
            return "PAK";
        }elseif($dept == "sa"){
            return "SA";
        }elseif($dept == "Management"){
            return "MG";   
        }elseif($dept == "Compliance"){
            return "COM";
        }
        elseif($dept == "Chief Business Officer"){
            return "CBO";
        }
        elseif($dept == "Chief Executive Officer"){
            return "CEO";
        }
        elseif($dept == "Chief Services Officer"){
            return "CSO";
        }elseif($dept == "Valuation Operations Support"){
            return "VOP";
        }elseif($dept == "Inspectors"){
            return "INS";
        }elseif($dept == "Data Entry"){
            return "DAT";
        }
        elseif($dept == "Chief Pakistan Officer"){
            return "CPO";
        }
        elseif($dept == "IT"){
            return "ITD";
        }
        else{
            return "none";
        }
    }
    

    public function actionApproved($id)
    {
        // Find the KPI record by ID
        $model = KpiManager::findOne($id);

        if (!$model) {
            Yii::$app->session->setFlash('error', 'KPI not found.');
            return $this->redirect(['index']); 
        }

        // Mark the KPI as approved
        $model->is_approved = 1; 
        $model->approved_by = Yii::$app->user->id; 

        $userEmail = User::find()
        ->where(['id' => $model->employe]) 
        ->select('email') 
        ->scalar(); 
        

        if ($model->save()) {
           
            Yii::$app->session->setFlash('success', 'KPI is approved.');
            Yii::$app->mailer->compose()
                ->setTo($userEmail) 
                ->setSubject('KPI Approved')
                ->setTextBody('Your KPI with Reference Number ' . $model->reference_number . ' has been approved.')
                ->send();
        } else {
            Yii::$app->session->setFlash('error', 'Failed to approve KPI.');
        }

        return $this->redirect(['index']); 
    }

    /*
        onlick verify by super admin 
        todo email will going to be sent
    */
    public function actionVerified($id)
    {
        // Find the KPI record by ID
        $model = KpiManager::findOne($id);

        if (!$model) {
            Yii::$app->session->setFlash('error', 'KPI not found.');
            return $this->redirect(['index']); 
        }

        // Mark the KPI as approved
        $model->is_verified = 1; 
        $model->verified_by = Yii::$app->user->id; 
        

        if ($model->save()) {
            // Send notification (custom logic or use Yii mailer)
            Yii::$app->session->setFlash('success', 'KPI is Verify.');

            // Example: Notification using Yii Mailer (if enabled)
            // Yii::$app->mailer->compose()
            //     ->setTo($model->employee->email) 
            //     ->setSubject('KPI Approved')
            //     ->setTextBody('Your KPI with Reference Number ' . $model->reference_number . ' has been approved.')
            //     ->send();
        } else {
            Yii::$app->session->setFlash('error', 'Failed to approve KPI.');
        }

        return $this->redirect(['index']); 
    }

    public function actionView($id)
    {
        $model = $this->findModel($id); 

        $departmentDetails = null;
        if ($model->department) {
            $departmentDetails = Department::find()
                ->where(['id' => $model->department])
                ->one(); 
        }

        $unitDetails = null;
        if ($model->unit) {
            $unitDetails = Units::find()
                ->where(['id' => $model->unit])
                ->one(); 
        }

        $selecteduser = null;
        if ($model->employe) {
            $selecteduser = User::find()
                ->where(['id' => $model->employe])
                ->one(); 
        }

        $uname = $selecteduser->firstname . " " . $selecteduser->lastname;

        
        return $this->render('view', [
            'model' => $model,
            'department' => $departmentDetails->title,
            'selecteduser' => $uname,
            'unit' => $unitDetails->title,
            
        ]);
    }

    protected function findModel($id)
    {
        if (($model = KpiManager::findOne($id)) !== null) {
            return $model;
        }

        throw new \yii\web\NotFoundHttpException('The requested page does not exist.');
    }
    
    

}