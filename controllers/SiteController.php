<?php

namespace app\controllers;

use app\models\AttributesData;
use app\models\Company;
use app\models\CrmQuotations;
use app\models\RatingHrData;
use app\models\SaleDiscount;
use app\models\ScheduleInspection;
use app\models\StandardReportIncome;
use app\models\Valuation;
use app\models\ValuationApproversData;
use app\models\ValuationDetail;
use app\models\ValuerLocations;
use Yii;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ForgetPasswordForm;
use app\models\ResetPasswordForm;
use app\models\StandardReport;
use app\models\SpecialAssumptionreport;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use yii\web\Request;
use app\models\ExpenseManager;
use yii\db\Expression;

class SiteController extends DefController
{
  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['logout'],
        'rules' => [
          [
            'actions' => ['logout'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'logout' => ['post'],
        ],
      ],
    ];
  }

  /**
  * Displays homepage.
  *
  * @return string
  */




public function actionDistance($lat1, $lon1, $lat2, $lon2) {
        $earthRadius = 6371000; // Radius of the Earth in meters

        $lat1 = deg2rad($lat1);
        $lon1 = deg2rad($lon1);
        $lat2 = deg2rad($lat2);
        $lon2 = deg2rad($lon2);

        $dlat = $lat2 - $lat1;
        $dlon = $lon2 - $lon1;

        $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlon / 2) * sin($dlon / 2);
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

        $distance = $earthRadius * $c; // Distance in meters

        return round($distance);
    }



  public function actionHaversineDistance() {
        // Radius of the Earth in kilometers
   //     $R = 6371;

   //   25.113836468773833, 55.193991928836276
   //   25.115464563614434, 55.19674248465493
      echo $this->actionDistance('25.1134916','55.1939812','25.1171103','55.1864958');
      DIE;

      // Example coordinates for two locations
$lat1 = 25.113836468773833; // Latitude of location 1
$lon1 = 5.193991928836276; // Longitude of location 1

$lat2 = 25.115464563614434; // Latitude of location 2
$lon2 = 55.19674248465493; // Longitude of location 2

$distance = $this->getDistance($lat1, $lon1, $lat2, $lon2);

echo "Distance between the two locations: " . round($distance, 2) . " meters";

die('ddd');


      $lat1 = 25.1137101;
      $lon1 = 55.1939811;
      $lat2 = 25.115347;
      $lon3 = 55.196699;

        // Convert latitude and longitude from degrees to radians
        $lat1 = deg2rad($lat1);
        $lon1 = deg2rad($lon1);
        $lat2 = deg2rad($lat2);
        $lon2 = deg2rad($lon2);

        // Calculate the change in coordinates
        $dlat = $lat2 - $lat1;
        $dlon = $lon2 - $lon1;

        // Haversine formula
        $a = sin($dlat / 2) ** 2 + cos($lat1) * cos($lat2) * sin($dlon / 2) ** 2;
        $c = 2 * atan2(sqrt($a), sqrt(1 - $a));

        // Calculate the distance
        $distance = $R * $c;

        echo $distance;
        die;

        return $distance;
    }
  public function actionIndex()
  {


      $this->checkLogin();

      $query_ahead_of_time = Valuation::find()
          ->select([
              Valuation::tableName().'.id',
              Valuation::tableName().'.reference_number',
              Valuation::tableName().'.instruction_date',
              Valuation::tableName().'.target_date',
              Valuation::tableName().'.service_officer_name',
              'submission_date' => ValuationApproversData::tableName().'.created_at',
              // 'submission_date' => ScheduleInspection::tableName().'.valuation_report_date',
              //'inspection_date' => ScheduleInspection::tableName().'.inspection_date',
              'inspection_officer_name' => ScheduleInspection::tableName().'.inspection_officer',
              Valuation::tableName().'.property_id',
              'approver'=>ValuationApproversData::tableName().'.created_by'
          ])
          ->innerJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
          ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
          ->where([Valuation::tableName().'.valuation_status' => 5])
          ->andWhere([Valuation::tableName().'.parent_id' => null])
          ->andwhere('DATE(target_date) > DATE(valuation_approvers_data.created_at)')
          ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver']);
          $query_totalAhead_of_time_count = count($query_ahead_of_time->all());
      $query_ahead_of_time->andFilterWhere([
          'between', 'DATE(valuation_approvers_data.created_at)', date('Y-m-d'), date('Y-m-d')
      ]);
      $query_ahead_of_time_count = count($query_ahead_of_time->all());

      $query_on_time = Valuation::find()
          ->select([
              Valuation::tableName().'.id',
              Valuation::tableName().'.reference_number',
              Valuation::tableName().'.instruction_date',
              Valuation::tableName().'.target_date',
              Valuation::tableName().'.service_officer_name',
              'submission_date' => ValuationApproversData::tableName().'.created_at',
              // 'submission_date' => ScheduleInspection::tableName().'.valuation_report_date',
              //'inspection_date' => ScheduleInspection::tableName().'.inspection_date',
              'inspection_officer_name' => ScheduleInspection::tableName().'.inspection_officer',
              Valuation::tableName().'.property_id',
              'approver'=>ValuationApproversData::tableName().'.created_by'
          ])
          ->innerJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
          ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
          ->where([Valuation::tableName().'.valuation_status' => 5])
          ->andWhere([Valuation::tableName().'.parent_id' => null])
          ->andwhere('DATE(target_date) = DATE(valuation_approvers_data.created_at)')
          ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver']);
          
      $query_totalOn_time_count = count($query_on_time->all());
      $query_on_time->andFilterWhere([
          'between', 'DATE(valuation_approvers_data.created_at)', date('Y-m-d'), date('Y-m-d')
      ]);
      $query_on_time_count = count($query_on_time->all());

      $query_delay = Valuation::find()
          ->select([
              Valuation::tableName().'.id',
              Valuation::tableName().'.reference_number',
              Valuation::tableName().'.instruction_date',
              Valuation::tableName().'.target_date',
              Valuation::tableName().'.service_officer_name',
              'submission_date' => ValuationApproversData::tableName().'.created_at',
              // 'submission_date' => ScheduleInspection::tableName().'.valuation_report_date',
              //'inspection_date' => ScheduleInspection::tableName().'.inspection_date',
              'inspection_officer_name' => ScheduleInspection::tableName().'.inspection_officer',
              Valuation::tableName().'.property_id',
              'approver'=>ValuationApproversData::tableName().'.created_by'
          ])
          ->innerJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
          ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
          ->where([Valuation::tableName().'.valuation_status' => 5])
          ->andWhere([Valuation::tableName().'.parent_id' => null])
          ->andwhere('DATE(target_date) < DATE(valuation_approvers_data.created_at)')
          ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver']);
          $query_totalDelay_count = count($query_delay->all());
      $query_delay->andFilterWhere([
          'between', 'DATE(valuation_approvers_data.created_at)', date('Y-m-d'), date('Y-m-d')
      ]);
      $query_delay_count = count($query_delay->all());


    return $this->render('index',[
        'query_ahead_of_time_count' => $query_ahead_of_time_count,
        'query_on_time_count' => $query_on_time_count,
        'query_delay_count' => $query_delay_count,
        'query_totalAhead_of_time_count' => $query_totalAhead_of_time_count,
        'query_totalOn_time_count' => $query_totalOn_time_count,
        'query_totalDelay_count' => $query_totalDelay_count,
    ]);
  }


    public function actionZohoInvoices()
    {
        $startDate = "2023-11-11";
        $toDate = date('Y-m-d');
        $get_valuation = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                Valuation::tableName().'.zoho_invoice_id',
                Valuation::tableName().'.reference_number',
                Valuation::tableName().'.client_id',
                Valuation::tableName().'.submission_approver_date',
                Valuation::tableName().'.total_fee',
            ])
            ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->where([Valuation::tableName().'.valuation_status' => 5])
            ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
            ->andWhere([Valuation::tableName().'.zoho_invoice_id'  => null])
            ->andFilterWhere([
                'between', 'DATE('.Valuation::tableName().'.submission_approver_date)', $startDate, $toDate
            ])->all();

        foreach($get_valuation as $key => $valuations)
        {
            $client =  Company::find()->where(['id' => $valuations->client_id])->one();

            if($valuations->quotation_id <> null)
            {
                $quotation_data = CrmQuotations::findOne($valuations->quotation_id);
                $item_name = $valuations->reference_number.'-('.$quotation_data->reference_number.')';
            }else{
                $item_name = $valuations->reference_number;
            }

            //ZohoBooks Integration Start
            // Zoho ID in Client
            if($client->zohobooks_id <> null)
            {
                $client->zohobooks_id = $client->zohobooks_id;

            }else{

                $client->zohobooks_id = Yii::$app->appHelperFunctions->postClientInZohoBooks($client->id, $client->title, $client->title);
            }

            /* echo "<pre>";
             print_r($client->zohobooks_id);
             die('dddd');*/

            $city = Yii::$app->appHelperFunctions->emiratedListArr[$valuations->building->city];
            if($city == "Ajman")
            {
                $branch_id = Yii::$app->appHelperFunctions->getSetting('zohobooks_ajman_id');
            }else if($city == "Abu Dhabi"){
                $branch_id = Yii::$app->appHelperFunctions->getSetting('zohobooks_abu_dhabi_id');
            }else{
                $branch_id = Yii::$app->appHelperFunctions->getSetting('zohobooks_dubai_id');
            }

            // Create Invoice & Item
           // Yii::$app->appHelperFunctions->ZohoRefereshToken();
            $AccessToken = Yii::$app->appHelperFunctions->getSetting('zohobooks_access_token');
            $OrganizationID = Yii::$app->appHelperFunctions->getSetting('zohobooks_organization_id');
            $TaxID = Yii::$app->appHelperFunctions->getSetting('zohobooks_tax_id');
            // $ItemID = Yii::$app->appHelperFunctions->getSetting('zohobooks_item_id');

            //Create an Item
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://books.zoho.com/api/v3/items?organization_id='.$OrganizationID,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('JSONString' => '{
                    "name": '.$item_name.',
                    "rate": '.$valuations->total_fee.',
                    "tax_id": '.$TaxID.',
                    "tax_percentage": "5%",
                    "product_type": "service",
                    "is_taxable": true,
                    "item_tax_preferences": [
                        {
                            "tax_id": '.$TaxID.',
                            "tax_specification": "intra"
                        }
                    ],
                }'),
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Zoho-oauthtoken'.' '.$AccessToken,
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            $response = json_decode($response);
            $ItemID = $response->item->item_id;
            $invoice_date = date("Y-m-d",strtotime($valuations->submission_approver_date));
            //end Create Item

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://books.zoho.com/api/v3/invoices?organization_id='.$OrganizationID,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('JSONString' => '{
                    "customer_id": '.$client->zohobooks_id.',
                    "branch_id": '.$branch_id.',
                    "line_items": [
                        {
                            "item_id": '.$ItemID.',
                            "tax_id": '.$TaxID.',
                        } 
                    ],
                    "tax_treatment": "vat_registered",
                    "place_of_supply": "AE",
                    "invoice_number": "'.$valuations->reference_number.'",
                    "date": '.$invoice_date.',
                }'),
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Zoho-oauthtoken'.' '.$AccessToken,
                ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            $response = json_decode($response);
            $query =  Yii::$app->db->createCommand()->update('valuation', ['zoho_invoice_id' => $response->invoice->invoice_id], 'id = '.$valuations->id.'')->execute();
            if(!empty($response->invoice->invoice_id)){
                Yii::$app->getSession()->addFlash('success', Yii::t('app', $response->message));
            }else{
                echo $response->message;
                Yii::$app->getSession()->addFlash('error', Yii::t('app', $response->message));
                die;
            }
            //zoho end
        }
    }

  /**
  * Login action.
  *
  * @return Response|string
  */
  public function actionLogin()
  {
    $this->layout='guest';
    if (!Yii::$app->user->isGuest) {
      return $this->goHome();
    }

    $model = new LoginForm();
    if ($model->load(Yii::$app->request->post()) && $model->login()) {
      return $this->goBack();
    }

    $model->password = '';
    return $this->render('login', [
      'model' => $model,
    ]);
  }
    public function actionCeoLogin()
    {
        Yii::$app->user->logout();
        $this->layout='guest';
        if (!Yii::$app->user->isGuest) {
            // echo "SiteController line 69"; die();
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

  /**
  * Logout action.
  *
  * @return Response
  */
  public function actionLogout()
  {
    Yii::$app->user->logout();

    return $this->goHome();
  }

  /**
   * Forget Password action.
   *
   * @return Response
   */
  public function actionForgetPassword()
  {
    $this->layout='guest';
    $model = new ForgetPasswordForm();
    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
      if ($model->sendEmail()) {
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Check your email for further instructions.'));
        return $this->goHome();
      } else {
        Yii::$app->getSession()->addFlash('error', Yii::t('app','Sorry, we are unable to reset password for email provided.'));
      }
    }

    return $this->render('forget_password', [
      'model' => $model,
    ]);
  }

  /**
   * Reset Password action.
   *
   * @return Response
   */
  public function actionResetPassword($token)
  {
    $this->layout='guest';
    try {
      $model = new ResetPasswordForm($token);
    } catch (InvalidParamException $e) {
      throw new BadRequestHttpException($e->getMessage());
    }
    if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
      Yii::$app->getSession()->addFlash('success', Yii::t('app','New password was saved.'));
      return $this->goHome();
    }
    return $this->render('reset_password', [
      'model' => $model,
    ]);
  }

  /**
  * Displays contact page.
  *
  * @return Response|string
  */
  public function actionContact()
  {
    $model = new ContactForm();
    if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
      Yii::$app->session->setFlash('contactFormSubmitted');

      return $this->refresh();
    }
    return $this->render('contact', [
      'model' => $model,
    ]);
  }


  public function assignVerification($model,$status){
    if($status==1){
      $model->status_verified    = $status;
      $model->status_verified_at = date('Y-m-d H:i:s');
      $model->status_verified_by = Yii::$app->user->identity->id;
    }
   }
   
    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionAttributesData()
    {



        $model = new AttributesData();
        $old_verify_status = $model->status_verified;

        
        $old_verify_status = AttributesData::find()->select('status_verified')->scalar();
        
        $model->status_verified = $old_verify_status;


        if ($model->load(Yii::$app->request->post())) {

          if(AttributesData::find()->count()>0){
            $action = "data_updated";
          }else{
            $action = "data_created";
          }

           /* echo "<pre>";
            print_r($model->upgrades);
            die;*/


            AttributesData::deleteAll();
            $attributes_transaction_limit= new AttributesData();
            $attributes_transaction_limit->attribute_name = 'transaction_limit';
            $attributes_transaction_limit->value =$model->transaction_limit;
            $this->assignVerification($attributes_transaction_limit, $model->status_verified);
            $attributes_transaction_limit->save();

            $attributes_transaction_limit= new AttributesData();
            $attributes_transaction_limit->attribute_name = 'proposal_limit';
            $attributes_transaction_limit->value =$model->proposal_limit;
            $this->assignVerification($attributes_transaction_limit, $model->status_verified);
            $attributes_transaction_limit->save();
            foreach ($model->location as $key => $location_data) {
                    foreach ($location_data as $key2 => $location_item) {
                        $attributes_data_location = new AttributesData();
                        $attributes_data_location->attribute_name = 'location';
                        $attributes_data_location->type = $key;
                        $attributes_data_location->key_name =$key2;
                        $attributes_data_location->value = $location_item;
                        $this->assignVerification($attributes_data_location, $model->status_verified);
                        $attributes_data_location->save();
                    }
                }

                foreach ($model->view as $key3 => $view_data) {
                    foreach ($view_data as $key4 => $view_item) {
                        $attributes_data_upgrade = new AttributesData();
                        $attributes_data_upgrade->attribute_name = 'view';
                        $attributes_data_upgrade->type = $key3;
                        $attributes_data_upgrade->key_name =$key4;
                        $attributes_data_upgrade->value = $view_item;
                        $this->assignVerification($attributes_data_upgrade, $model->status_verified);
                        $attributes_data_upgrade->save();
                    }
                }

            foreach ($model->upgrades as $key5 => $upgrade_data) {
                foreach ($upgrade_data as $key6 => $upgrade_item) {
                    $attributes_data_upgrade = new AttributesData();
                    $attributes_data_upgrade->attribute_name = 'upgrades';
                    $attributes_data_upgrade->type = $key5;
                    $attributes_data_upgrade->key_name =$key6;
                    $attributes_data_upgrade->value = $upgrade_item;
                    $this->assignVerification($attributes_data_upgrade, $model->status_verified);
                    $attributes_data_upgrade->save();
                }
            }

            $history = [
              'model' => $model,
              'model_name' => Yii::$app->appHelperFunctions->getModelName(),
              'action' => $action,
              'rec_type' => 'masterfile',
              'file_type' => 'attributes-data',
              'old_verify_status' => $old_verify_status,
             ];
             $this->makeMasterFilesHistory($history);

                Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
                return $this->refresh();
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }

        return $this->render('attributes', [
            'model' => $model,
        ]);
    }

    public function actionRatingHrData()
    {



        $model = new RatingHrData();
        $old_verify_status = $model->status_verified;


        $old_verify_status = RatingHrData::find()->select('status_verified')->scalar();

        $model->status_verified = $old_verify_status;

       /* echo "<pre>";
        print_r(Yii::$app->request->post());
        die;*/


        if ($model->load(Yii::$app->request->post())) {

            if(RatingHrData::find()->count()>0){
                $action = "data_updated";
            }else{
                $action = "data_created";
            }


            RatingHrData::deleteAll();

            foreach ($model->academic_degrees as $key => $d_data) {

                    $academic_degrees = new RatingHrData();
                    $academic_degrees->attribute_name = 'academic_degrees';
                    $academic_degrees->type = (string)$key;
                    $academic_degrees->value = $d_data;
                    $this->assignVerification($academic_degrees, $model->status_verified);
                    $academic_degrees->save();

            }

            foreach ($model->professional_degrees as $key3 => $pd_data) {

                    $professional_degrees = new RatingHrData();
                    $professional_degrees->attribute_name = 'professional_degrees';
                    $professional_degrees->type = (string)$key3;
                    $professional_degrees->value = $pd_data;
                    $this->assignVerification($professional_degrees, $model->status_verified);
                    $professional_degrees->save();

            }

            foreach ($model->experience_total as $key5 => $et_data) {

                    $experience_total = new RatingHrData();
                    $experience_total->attribute_name = 'experience_total';
                    $experience_total->type = (string)$key5;
                    $experience_total->value = $et_data;
                    $this->assignVerification($experience_total, $model->status_verified);
                    $experience_total->save();

            }

            foreach ($model->experience_relevent as $key7 => $er_data) {

                    $experience_relevent = new RatingHrData();
                    $experience_relevent->attribute_name = 'experience_relevent';
                    $experience_relevent->type = (string)$key7;
                    $experience_relevent->value = $er_data;
                    $this->assignVerification($experience_relevent, $model->status_verified);
                    $experience_relevent->save();

            }

            foreach ($model->experience_organization as $key9 => $eo_data) {

                    $experience_organization = new RatingHrData();
                    $experience_organization->attribute_name = 'experience_organization';
                    $experience_organization->type = (string)$key9;
                    $experience_organization->value = $eo_data;
                    $this->assignVerification($experience_organization, $model->status_verified);
                    $experience_organization->save();

            }

            foreach ($model->performance_kpi as $key11 => $pk_data) {

                    $performance_kpi = new RatingHrData();
                    $performance_kpi->attribute_name = 'performance_kpi';
                    $performance_kpi->type = (string)$key11;
                    $performance_kpi->value = $pk_data;
                    $this->assignVerification($performance_kpi, $model->status_verified);
                    $performance_kpi->save();

            }

            foreach ($model->performance_value_addition as $key13 => $va_data) {

                    $performance_value_addition = new RatingHrData();
                    $performance_value_addition->attribute_name = 'performance_value_addition';
                    $performance_value_addition->type = (string)$key13;
                    $performance_value_addition->value = $va_data;
                    $this->assignVerification($performance_value_addition, $model->status_verified);
                    $performance_value_addition->save();

            }

            foreach ($model->getting_things_done as $key15 => $gt_data) {

                    $getting_things_done = new RatingHrData();
                    $getting_things_done->attribute_name = 'getting_things_done';
                    $getting_things_done->type = (string)$key15;
                    $getting_things_done->value = $gt_data;
                    $this->assignVerification($getting_things_done, $model->status_verified);
                    $getting_things_done->save();

            }

            foreach ($model->communication_skills as $key17 => $cs_data) {
                    $communication_skills = new RatingHrData();
                    $communication_skills->attribute_name = 'communication_skills';
                    $communication_skills->type = (string)$key17;
                    $communication_skills->value = $cs_data;
                    $this->assignVerification($communication_skills, $model->status_verified);
                    $communication_skills->save();

            }

            foreach ($model->technological_skills as $key19 => $ts_data) {

                    $technological_skills = new RatingHrData();
                    $technological_skills->attribute_name = 'technological_skills';
                    $technological_skills->type = (string)$key19;
                    $technological_skills->value = $ts_data;
                    $this->assignVerification($technological_skills, $model->status_verified);
                    $technological_skills->save();

            }

            foreach ($model->ethics_and_attitude as $key21 => $ea_data) {

                    $ethics_and_attitude = new RatingHrData();
                    $ethics_and_attitude->attribute_name = 'ethics_and_attitude';
                    $ethics_and_attitude->type = (string)$key21;
                    $ethics_and_attitude->value = $ea_data;
                    $this->assignVerification($ethics_and_attitude, $model->status_verified);
                    $ethics_and_attitude->save();

            }

            foreach ($model->driving_license as $key23 => $dl_data) {

                    $ethics_and_attitude = new RatingHrData();
                    $ethics_and_attitude->attribute_name = 'driving_license';
                    $ethics_and_attitude->type = (string)$key23;
                    $ethics_and_attitude->value = $dl_data;
                    $this->assignVerification($ethics_and_attitude, $model->status_verified);
                    $ethics_and_attitude->save();

            }
            foreach ($model->cover_letter as $key23 => $dl_data) {

                $ethics_and_attitude = new RatingHrData();
                $ethics_and_attitude->attribute_name = 'cover_letter';
                $ethics_and_attitude->type = (string)$key23;
                $ethics_and_attitude->value = $dl_data;
                $this->assignVerification($ethics_and_attitude, $model->status_verified);
                $ethics_and_attitude->save();

            }





            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => 'rating-hr-data',
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);

            Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
            return $this->refresh();
        }else{
            if($model->hasErrors()){
                foreach($model->getErrors() as $error){
                    if(count($error)>0){
                        foreach($error as $key=>$val){
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        return $this->render('rating_hr_data', [
            'model' => $model,
        ]);
    }


    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionSalesDiscounts()
    {
        $model = SaleDiscount::findOne(1);
        $old_verify_status = $model->status_verified;

        if ($model->load(Yii::$app->request->post())) {
          $this->StatusVerify($model);
            if($model->save()){

              $this->makeHistory([
                'model' => $model, 
                'model_name' => 'app\models\SaleDiscount',
                'action' => 'data_updated',
                'verify_field' => 'status_verified',
                'old_verify_status' => $old_verify_status,
            ]);


                Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
                return $this->refresh();
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }


        }
        return $this->render('sale_discount', [
            'model' => $model,
        ]);
    }



  /**
  * Displays about page.
  *
  * @return string
  */
  public function actionAbout()
  {
    return $this->render('about');
  }

    public function actionStandardReport()
    {
        $model = StandardReport::findOne(1);
        $old_verify_status = $model->status_verified;

        if ($model->load(Yii::$app->request->post())) {
          $this->StatusVerify($model);
 
          if($model->save()){
              $this->makeHistory([
                  'model' => $model, 
                  'model_name' => get_class($model),
                  'action' => 'data_updated',
                  'verify_field' => 'status_verified',
                  'old_verify_status' => $old_verify_status,
              ]);
 
              Yii::$app->getSession()->addFlash('success', Yii::t('app','Information updated successfully'));
              return $this->redirect(['standard-report']);
          }
      }
        return $this->render('standard_report', [
            'model' => $model,
        ]);
    }
    public function actionStandardReportIncome()
    {
        $model = StandardReportIncome::findOne(1);
        $old_verify_status = $model->status_verified;

        if ($model->load(Yii::$app->request->post())) {
            $this->StatusVerify($model);

            if($model->save()){
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => get_class($model),
                    'action' => 'data_updated',
                    'verify_field' => 'status_verified',
                    'old_verify_status' => $old_verify_status,
                ]);

                Yii::$app->getSession()->addFlash('success', Yii::t('app','Information updated successfully'));
                return $this->redirect(['standard-report-income']);
            }
        }
        return $this->render('standard_report_income', [
            'model' => $model,
        ]);
    }


    public function actionLogodelete()
    {
        $model = $this->findModel($id);


        if ($id !=null) {

//delete single image when user select delete
//  Yii::$app->get('s3bucket')->delete('property/images/'.$model->front_image);


            $model->signature_img_name = 'unnamed.png';
            $model->save();
        }
    }


    public function actionOccupancystatus()
    {
        $model = SpecialAssumptionreport::findOne(1);
        $old_verify_status = $model->os_status_verified;
        $model->status_verified = $model->os_status_verified;
        if ($model->load(Yii::$app->request->post())) {
          $model->os_status_verified = $model->status_verified;
          if($model->status_verified==1){
              $model->os_status_verified = 1;
              $model->os_status_verified_at = date('Y-m-d H:i:s');
              $model->os_status_verified_by =  Yii::$app->user->identity->id;
          }
          if($model->save()){  
            // dd($model);        
            $this->makeHistory([
              'model' => $model, 
              'model_name' => 'app\models\SpecialAssumptionreport',
              'action' => 'data_updated',
              'verify_field' => 'os_status_verified',
              'old_verify_status' => $old_verify_status,
              'file_type' => 'occupancystatus',
          ]);
        }
            return $this->redirect(['occupancystatus']);
        }
        return $this->render('occupancy-status', [
            'model' => $model,
        ]);
    }


    public function actionAcquisitionmethod()
    {
        $model = SpecialAssumptionreport::findOne(1);
        $old_verify_status = $model->am_status_verified;
        $model->status_verified = $model->am_status_verified;
        if ($model->load(Yii::$app->request->post())) {
          $model->am_status_verified = $model->status_verified;
          if($model->status_verified==1){
              $model->am_status_verified = 1;
              $model->am_status_verified_at = date('Y-m-d H:i:s');
              $model->am_status_verified_by =  Yii::$app->user->identity->id;
          }

          if($model->save()){
              $this->makeHistory([
                'model' => $model, 
                'model_name' => 'app\models\SpecialAssumptionreport',
                'action' => 'data_updated',
                'verify_field' => 'am_status_verified',
                'old_verify_status' => $old_verify_status,
                'file_type' => 'acquisitionmethod',
              ]);
            return $this->redirect(['acquisitionmethod']);
          }
      }
        return $this->render('acquisition-method', [
            'model' => $model,
        ]);
    }


    public function actionPhysicalinspection()
    {
        $model = SpecialAssumptionreport::findOne(1);
        $old_verify_status = $model->pi_status_verified;
        $model->status_verified = $model->pi_status_verified;
        if ($model->load(Yii::$app->request->post())) {
          $model->pi_status_verified = $model->status_verified;
          
          if($model->status_verified==1){
              $model->pi_status_verified = 1;
              $model->pi_status_verified_at = date('Y-m-d H:i:s');
              $model->pi_status_verified_by =  Yii::$app->user->identity->id;
          }
          if($model->save()){
            $this->makeHistory([
              'model' => $model, 
              'model_name' => 'app\models\SpecialAssumptionreport',
              'action' => 'data_updated',
              'verify_field' => 'pi_status_verified',
              'old_verify_status' => $old_verify_status,
              'file_type' => 'physicalinspection',
          ]);

            return $this->redirect(['physicalinspection']);
          }
        }
        return $this->render('physical-inspection', [
            'model' => $model,
        ]);
    }
    
    public function actionStatusreport()
    {
        $model = SpecialAssumptionreport::findOne(1);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['statusreport']);
        }
        return $this->render('status-report', [
            'model' => $model,
        ]);
    }
    
     public function actionPrivacyPolicy()
    {
        $this->layout='public';
        return $this->render('privacy-policy');
    }

    public function actionLicenseAgreement()
    {
        $this->layout='public';
        return $this->render('license-agreement');
    }

    public function actionWindmillsGeneralDocs()
    {
        $old_about_windmills =\app\models\WindmillsDocs::find()->where(['file' => 'about_windmills'])->orderBy(['created_at' => SORT_DESC])->one();
        $old_corporate_profile =\app\models\WindmillsDocs::find()->where(['file' => 'corporate_profile'])->orderBy(['created_at' => SORT_DESC])->one();


        if (Yii::$app->request->post()) {

            //handle the about_windmills
            $file = UploadedFile::getInstanceByName('about_windmills');

            if ($file !== null) {
                $filename = 'about_windmills-'.$file->baseName .'-'. date('Y-m-d-H-i-s') . '.' . $file->extension;
                $file->saveAs('uploads/windmills_docs/' . $filename);
                $url = (new Request())->getHostInfo().Url::to('@web/uploads/windmills_docs/'.$filename);
                $model = $this->actionSaveWindmillsDocsDb([
                    'file' => 'about_windmills',
                    'url' => $url,
                    'base_name' => $filename,
                ]);
                $old_about_windmills = $model;
            }

            //handle the corporate_profile
            $file = UploadedFile::getInstanceByName('corporate_profile');
            if ($file !== null) {
                $filename = 'corporate_profile-'.$file->baseName .'-'. date('Y-m-d-H-i-s') . '.' . $file->extension;
                $file->saveAs('uploads/windmills_docs/' . $filename);
                $url = (new Request())->getHostInfo().Url::to('@web/uploads/windmills_docs/'.$filename);
                $model = $this->actionSaveWindmillsDocsDb([
                    'file' => 'corporate_profile',
                    'url' => $url,
                    'base_name' => $filename,
                ]);
                $old_corporate_profile = $model;
            }

        }


        return $this->render('windmills-general-docs',[
            'old_about_windmills'=>$old_about_windmills,
            'old_corporate_profile'=>$old_corporate_profile,
        ]);
    }
    public function actionWindmillReCostDocs()
    {
      $old_market_construction_cost =\app\models\WindmillsDocs::find()->where(['file' => 'market_construction_cost'])->orderBy(['created_at' => SORT_DESC])->one();
      $old_construction_cost_index =\app\models\WindmillsDocs::find()->where(['file' => 'construction_cost_index'])->orderBy(['created_at' => SORT_DESC])->one();
      $old_construction_cost_index_dubai =\app\models\WindmillsDocs::find()->where(['file' => 'construction_cost_index_dubai'])->orderBy(['created_at' => SORT_DESC])->one();
      $old_main_construction_cost_basis =\app\models\WindmillsDocs::find()->where(['file' => 'main_construction_cost_basis'])->orderBy(['created_at' => SORT_DESC])->one();
      $old_main_construction_cost_analysis =\app\models\WindmillsDocs::find()->where(['file' => 'main_construction_cost_analysis'])->orderBy(['created_at' => SORT_DESC])->one();
      $old_construction_cost_index_statistics =\app\models\WindmillsDocs::find()->where(['file' => 'construction_cost_index_statistics'])->orderBy(['created_at' => SORT_DESC])->one();
      $old_uae_central_bank_eibor_rates =\app\models\WindmillsDocs::find()->where(['file' => 'uae_central_bank_eibor_rates'])->orderBy(['created_at' => SORT_DESC])->one();
      $old_legal_fee_resources =\app\models\WindmillsDocs::find()->where(['file' => 'legal_fee_resources'])->orderBy(['created_at' => SORT_DESC])->one();
      $old_interest_rate_and_financing_schedule =\app\models\WindmillsDocs::find()->where(['file' => 'interest_rate_and_financing_schedule'])->orderBy(['created_at' => SORT_DESC])->one();
        $old_signature_image =\app\models\WindmillsDocs::find()->where(['file' => 'signature_image'])->orderBy(['created_at' => SORT_DESC])->one();

      if (Yii::$app->request->post()) {




        //handle the market_construction_cost
        $file = UploadedFile::getInstanceByName('market_construction_cost');

        if ($file !== null) {
            $filename = 'market_construction_cost-'.$file->baseName .'-'. date('Y-m-d-H-i-s') . '.' . $file->extension;
            $file->saveAs('uploads/windmills_docs/' . $filename);
            $url = (new Request())->getHostInfo().Url::to('@web/uploads/windmills_docs/'.$filename);
            $model = $this->actionSaveWindmillsDocsDb([
                      'file' => 'market_construction_cost',
                      'url' => $url,
                      'base_name' => $filename,
                    ]);
            $old_market_construction_cost = $model;
        }

        //handle the construction_cost_index
        $file = UploadedFile::getInstanceByName('construction_cost_index');
        if ($file !== null) {
            $filename = 'construction_cost_index-'.$file->baseName .'-'. date('Y-m-d-H-i-s') . '.' . $file->extension;
            $file->saveAs('uploads/windmills_docs/' . $filename);
            $url = (new Request())->getHostInfo().Url::to('@web/uploads/windmills_docs/'.$filename);
            $model = $this->actionSaveWindmillsDocsDb([
                      'file' => 'construction_cost_index',
                      'url' => $url,
                      'base_name' => $filename,
                    ]);
            $old_construction_cost_index = $model;
        }

          //handle the construction_cost_index dubai
          $file = UploadedFile::getInstanceByName('construction_cost_index_dubai');
          if ($file !== null) {
              $filename = 'construction_cost_index_dubai-'.$file->baseName .'-'. date('Y-m-d-H-i-s') . '.' . $file->extension;
              $file->saveAs('uploads/windmills_docs/' . $filename);
              $url = (new Request())->getHostInfo().Url::to('@web/uploads/windmills_docs/'.$filename);
              $model = $this->actionSaveWindmillsDocsDb([
                  'file' => 'construction_cost_index_dubai',
                  'url' => $url,
                  'base_name' => $filename,
              ]);
              $old_construction_cost_index_dubai = $model;
          }

          //handle the main_construction_cost_basis
          $file = UploadedFile::getInstanceByName('main_construction_cost_basis');

          if ($file !== null) {
              $filename = 'main_construction_cost_basis-'.$file->baseName .'-'. date('Y-m-d-H-i-s') . '.' . $file->extension;
              $file->saveAs('uploads/windmills_docs/' . $filename);
              $url = (new Request())->getHostInfo().Url::to('@web/uploads/windmills_docs/'.$filename);
              $model = $this->actionSaveWindmillsDocsDb([
                  'file' => 'main_construction_cost_basis',
                  'url' => $url,
                  'base_name' => $filename,
              ]);
              $old_main_construction_cost_basis = $model;
          }

          //handle the main_construction_cost_analysis
          $file = UploadedFile::getInstanceByName('main_construction_cost_analysis');

          if ($file !== null) {
              $filename = 'main_construction_cost_analysis-'.$file->baseName .'-'. date('Y-m-d-H-i-s') . '.' . $file->extension;
              $file->saveAs('uploads/windmills_docs/' . $filename);
              $url = (new Request())->getHostInfo().Url::to('@web/uploads/windmills_docs/'.$filename);
              $model = $this->actionSaveWindmillsDocsDb([
                  'file' => 'main_construction_cost_analysis',
                  'url' => $url,
                  'base_name' => $filename,
              ]);
              $old_main_construction_cost_analysis = $model;
          }

          //handle the signature_image
          $file = UploadedFile::getInstanceByName('signature_image');

          if ($file !== null) {
              $filename = 'signature_image-'.$file->baseName .'-'. date('Y-m-d-H-i-s') . '.' . $file->extension;
              $file->saveAs('uploads/windmills_docs/' . $filename);
              $url = (new Request())->getHostInfo().Url::to('@web/uploads/windmills_docs/'.$filename);
              $model = $this->actionSaveWindmillsDocsDb([
                  'file' => 'signature_image',
                  'url' => $url,
                  'base_name' => $filename,
              ]);
              $old_signature_image = $model;
          }

          //handle the construction_cost_index_statistics
          $file = UploadedFile::getInstanceByName('construction_cost_index_statistics');

          if ($file !== null) {
              $filename = 'construction_cost_index_statistics-'.$file->baseName .'-'. date('Y-m-d-H-i-s') . '.' . $file->extension;
              $file->saveAs('uploads/windmills_docs/' . $filename);
              $url = (new Request())->getHostInfo().Url::to('@web/uploads/windmills_docs/'.$filename);
              $model = $this->actionSaveWindmillsDocsDb([
                  'file' => 'construction_cost_index_statistics',
                  'url' => $url,
                  'base_name' => $filename,
              ]);
              $old_construction_cost_index_statistics = $model;
          }

          //handle the uae_central_bank_eibor_rates
          $file = UploadedFile::getInstanceByName('uae_central_bank_eibor_rates');

          if ($file !== null) {
              $filename = 'uae_central_bank_eibor_rates-'.$file->baseName .'-'. date('Y-m-d-H-i-s') . '.' . $file->extension;
              $file->saveAs('uploads/windmills_docs/' . $filename);
              $url = (new Request())->getHostInfo().Url::to('@web/uploads/windmills_docs/'.$filename);
              $model = $this->actionSaveWindmillsDocsDb([
                  'file' => 'uae_central_bank_eibor_rates',
                  'url' => $url,
                  'base_name' => $filename,
              ]);
              $old_uae_central_bank_eibor_rates = $model;
          }

          //handle the legal_fee_resources
          $file = UploadedFile::getInstanceByName('legal_fee_resources');

          if ($file !== null) {
              $filename = 'legal_fee_resources-'.$file->baseName .'-'. date('Y-m-d-H-i-s') . '.' . $file->extension;
              $file->saveAs('uploads/windmills_docs/' . $filename);
              $url = (new Request())->getHostInfo().Url::to('@web/uploads/windmills_docs/'.$filename);
              $model = $this->actionSaveWindmillsDocsDb([
                  'file' => 'legal_fee_resources',
                  'url' => $url,
                  'base_name' => $filename,
              ]);
              $old_legal_fee_resources = $model;
          }

          //handle the interest_rate_and_financing_schedule
          $file = UploadedFile::getInstanceByName('interest_rate_and_financing_schedule');

          if ($file !== null) {
              $filename = 'interest_rate_and_financing_schedule-'.$file->baseName .'-'. date('Y-m-d-H-i-s') . '.' . $file->extension;
              $file->saveAs('uploads/windmills_docs/' . $filename);
              $url = (new Request())->getHostInfo().Url::to('@web/uploads/windmills_docs/'.$filename);
              $model = $this->actionSaveWindmillsDocsDb([
                  'file' => 'interest_rate_and_financing_schedule',
                  'url' => $url,
                  'base_name' => $filename,
              ]);
              $old_interest_rate_and_financing_schedule = $model;
          }



      }


      return $this->render('windmills-general-docs-rec',[
        'old_market_construction_cost'=> $old_market_construction_cost,
        'old_construction_cost_index'=> $old_construction_cost_index,
        'old_main_construction_cost_basis'=> $old_main_construction_cost_basis,
        'old_main_construction_cost_analysis'=> $old_main_construction_cost_analysis,
        'old_construction_cost_index_statistics'=> $old_construction_cost_index_statistics,
        'old_uae_central_bank_eibor_rates'=> $old_uae_central_bank_eibor_rates,
        'old_legal_fee_resources'=> $old_legal_fee_resources,
        'old_interest_rate_and_financing_schedule'=> $old_interest_rate_and_financing_schedule,
        'old_construction_cost_index_dubai'=> $old_construction_cost_index_dubai,
        'old_signature_image'=> $old_signature_image,
      ]);
    }

    public function actionSaveWindmillsDocsDb($data){
      $model = new \app\models\WindmillsDocs;
      $model->file = $data['file'];
      $model->base_name = $data['base_name'];
      $model->url  = $data['url'];
      $model->created_at = date('Y-m-d H:i:s');
      $model->created_by = Yii::$app->user->id;
      if($model->save()){
        return $model;
      }
    }


    public function actionConvertValContacts(){
      $valuations = \app\models\Valuation::find()->select(['client_id'])->where(['valuation_status' => 5])->groupBy('client_id')->asArray()->all(); 
    //   dd(count($valuations));
      if(count($valuations)>0){
        foreach($valuations as $key=>$valuation){
          Yii::$app->db->createCommand()
             ->update('user', ['existing_valuation_contact' => 1], ['company_id' => $valuation['client_id']])
             ->execute();
        }
      }
    }
    
    

    
    public function actionImportBilalContacts()
    {
        $model = new \app\models\Company;

        if ($model->validate()) {
          $file = UploadedFile::getInstanceByName('Company[importfile]');
          if ($file !== null) {
              $filename = 'bilal_contacts-'.$file->baseName .'-'. date('Y-m-d-H-i-s') . '.' . $file->extension;
              $file->saveAs('uploads/bilal_contacts/' . $filename);
              $url = (new Request())->getHostInfo().Url::to('@web/uploads/bilal_contacts/'.$filename);
              return $this->redirect(['upload', 'file_path' => $url]);
                         
          }
          
        }

        return $this->render('../client/import', [
            'model' => $model,
        ]);
    }



    
    public function actionUpload($file_path, $n = 0, $s = 0)
    {
      // dd($file_path);
        $csvFile = new \SplFileObject($file_path, 'r');
        // $csvFile->seek($s);

        //skip rows
        if($s>0){
          for ($j=0; $j < $s; $j++) {
              $line = $csvFile->fgetcsv();
          }
        }


        
        while (!$csvFile->eof()) {
            $line = $csvFile->fgetcsv();
            $company        = (isset($line[0]) ? trim($line[0]) : '');
            $country        = (isset($line[1]) ? trim($line[1]) : '');
            $address        = (isset($line[2]) ? trim($line[2]) : '');
            $firstname      = (isset($line[3]) ? trim($line[3]) : '');
            $lastname       = (isset($line[4]) ? trim($line[4]) : '');
            $mobile_number  = (isset($line[5]) ? trim($line[5]) : '');
            $phone_number   = (isset($line[6]) ? trim($line[6]) : '');
            $job_title      = (isset($line[7]) ? trim($line[7]) : '');
            $email          = (isset($line[8]) ? trim($line[8]) : '');
            $statusWord     = (isset($line[9]) ? trim($line[9]) : '');

            if ($n > 0) {
                // dd($line);
              
                if ($line[0]<>null) {
                  
                    if($email<>null){
                      
                      $user = \app\models\User::find()->where(['email'=>trim($email)])->one();
                      // dd($user);
                      if($user<>null){
                        Yii::$app->db->createCommand()
                          ->update('user', [ 'bilal_contact' => 1 ], [ 'id' => $user->id ])
                          ->execute();

                        $valuation = \app\models\Valuation::find()->where(['client_id'=>$user->company_id,'valuation_status' => 5])->one();
                        if($valuation<>null){
                          Yii::$app->db->createCommand()
                          ->update('user', ['existing_valuation_contact' => 1], [ 'company_id' => $valuation->client_id ])
                          ->execute();
                        }
                          
                      }
                      else{
                        // dd($line);



                    $company = utf8_encode($company);

                    if ($firstname==null AND $firstname=='') {
                        $firstnameExplode = explode('@', $email);
                        if($firstnameExplode<>null)$firstname=$firstnameExplode[0];
                    }

                    $companyTosSave = $company;
                    if($companyTosSave == null){
                      $companyTosSave = $firstname;
                    }

                    if($companyTosSave<>null){
                      $companyModel = \app\models\Company::find()->where(['title'=>trim($companyTosSave)])->one();
                      
                      if($companyModel == null){
                        $companyModel = new \app\models\Company;
                      }

                      $companyModel->title = $companyTosSave;
                      if($companyModel->save()){
                        // dd($companyModel);

                        //save user and userprofileinfo
                          $job_title = utf8_encode($job_title);
                          // dd($job_title);

                          $address = utf8_encode($address);
                          
                          if ($mobile_number<>null) {
                              if (strpos($mobile_number, '/') !== false) {
                                  $explode_mobile = explode('/', $mobile_number);
                                  $mobile_number = $explode_mobile[0];
                              }
                              $mobile_number = str_replace(" ", "", $mobile_number);
                              $mobile_number = str_replace(".", "", $mobile_number);
                              $mobile_number = preg_replace('/[^0-9,.+]/', '', $mobile_number);
                          }

                          
                          $phone_1 = '';
                          $phone_2 = '';

                          if ($phone_number<>null) {
                              if (strpos($phone_number, '/') !== false) {
                                  $explode_phone = explode('/', $phone_number);
                                  $phone_1 = $explode_phone[0];
                                  $phone_2 = $explode_phone[1];
                              }else{
                                  $phone_1 = $phone_number;
                              }
                              $phone_1 = str_replace(" ", "", $phone_1);
                              $phone_1 = str_replace(".", "", $phone_1);
                              $phone_1 = preg_replace('/[^0-9,.+]/', '', $phone_1);

                              $phone_2 = str_replace(" ", "", $phone_2);
                              $phone_2 = str_replace(".", "", $phone_2);
                              $phone_2 = preg_replace('/[^0-9,.+]/', '', $phone_2);
                          }

                          // dd($phone_1);

                          if ($mobile_number=='' AND $mobile_number==null) {
                              if ($phone_1<>null) {
                                  $mobile_number = $phone_1;
                              }
                              elseif ($phone_2<>null) {
                                  $mobile_number = $phone_2;
                              }
                          }

                          // if($companyModel->title == "Amlak Finance PJSC"){
                          //   dd($mobile_number);
                          // }


                          $status = 2;
                          if ($statusWord=='Active') {
                              $status = 1;
                          }
                          
                          if($job_title<>null){
                              $job_title_id = \app\models\JobTitle::find()->select(['id','title'])->where(['title'=>trim($job_title)])->one();
                              // dd($job_title_id);
                              if ($job_title_id==null) {
                                  $job_title_id = new \app\models\JobTitle;
                                  $job_title_id->title  = $job_title;
                                  $job_title_id->status = 1;
                                  if($job_title_id->save()){
                                    // dd($job_title_id);
                                  }else{
                                    
                                    if ($job_title_id->hasErrors()) {
                                      dd($job_title_id->getErrors());
                                    }
                                  }
                              }
                          }


                          
                          $user_model = new \app\models\User;
                          $user_model->user_type       = 0;
                          $user_model->status          = 1;
                          $user_model->primary_contact = 0;
                          $user_model->company_id      = $companyModel->id;
                          $user_model->firstname       = $firstname;
                          $user_model->lastname        = ($lastname!=null AND $lastname!='') ? $lastname  : '-';
                          $user_model->email           = $email;                      
                          $user_model->job_title       = $job_title_id->title;
                          $user_model->job_title_id    = $job_title_id->id;
                          $user_model->mobile          = $mobile_number;
                          $user_model->phone1          = $phone_1;
                          $user_model->phone2          = $phone_2;
                          $user_model->bilal_contact   = 1;
                          // $user_model->existing_valuation_contact = 0;
                          if($user_model->save()){
                            // dd($user_model);
                          }
                          else{
                              if ($user_model->hasErrors()) {
                                
                                // save into database table 'OrphanContacts'
                                $orp_contact = \app\models\OrphanContacts::find()->where(['name'=>$firstname])->one();
                                if($orp_contact==null){
                                    $orp_contact = new \app\models\OrphanContacts;
                                    $orp_contact->type    = 'bilal_contact';
                                    $orp_contact->name    = $firstname;
                                    $orp_contact->email   = $email;
                                    $orp_contact->mobile  = $mobile_number;
                                    $orp_contact->phone_1 = $phone_1;
                                    $orp_contact->phone_2 = $phone_2;
                                    $orp_contact->error   = json_encode($user_model->getErrors());
                                    if($orp_contact->save()){
                                        // dd($orp_contact);
                                    }else{
                                        if ($orp_contact->hasErrors()) {
                                            echo "orp_contact err else"; dd($orp_contact->getErrors());
                                        }
                                    }
                                }
                              }
                          }

                      }else{
                          if ($companyModel->hasErrors()) {
                            dd($companyModel->getErrors());
                          }
                      }
                    }

                      }
                    }
                    else{
                        // save into database table 'OrphanContacts'
                        $orp_contact = \app\models\OrphanContacts::find()->where(['name'=>$firstname])->one();
                        if($orp_contact==null){
                            $orp_contact = new \app\models\OrphanContacts;
                            $orp_contact->type    = 'bilal_contact';
                            $orp_contact->name    = $firstname;
                            $orp_contact->email   = $email;
                            $orp_contact->mobile  = $mobile_number;
                            $orp_contact->phone_1 = $phone_1;
                            $orp_contact->phone_2 = $phone_2;
                            if($orp_contact->save()){
                                // dd($orp_contact);
                            }else{
                                if ($orp_contact->hasErrors()) {
                                    echo "orp_contact err else"; dd($orp_contact->getErrors());
                                }
                            }
                        }
                    }
                
                  }
            }

            if ($n >= 1000) {
                dd("imported 1000");
                echo 'Still Importing...<script>window.location.href="' . Url::to(['upload', 'file_path' => $file_path, 'n'=>1, 's' => $s]) . '"</script>';
                die();
                exit();
                break;
            }
            $s++;
            $n++;
        }
        die;
        $csvFile = null;
        if ($csvFile == null) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Contacts Updated successfully'));
            return $this->redirect(['import']);
        }
    }

    public function actionZohoInvoicesStatusChange()
    {
        $startDate = "2023-11-13";
        $toDate = date('q7Y-m-d');
        $get_valuation = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                Valuation::tableName().'.zoho_invoice_id',
                Valuation::tableName().'.reference_number',
                Valuation::tableName().'.client_id',
                Valuation::tableName().'.submission_approver_date',
                Valuation::tableName().'.total_fee',
            ])
            ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->where([Valuation::tableName().'.valuation_status' => 5])
            ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
            ->andWhere(['not', [Valuation::tableName().'.zoho_invoice_id' => null]])
            ->andWhere([Valuation::tableName().'.zoho_date_status' => null])
            ->andFilterWhere([
                'between', 'DATE('.Valuation::tableName().'.submission_approver_date)', $startDate, $toDate
            ])->all();

        foreach($get_valuation as $key => $valuations)
        {
            $OrganizationID = Yii::$app->appHelperFunctions->getSetting('zohobooks_organization_id');
            $AccessToken = Yii::$app->appHelperFunctions->getSetting('zohobooks_access_token');
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://www.zohoapis.com/books/v3/invoices/'.$valuations->zoho_invoice_id.'/status/sent?organization_id='.$OrganizationID,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Zoho-oauthtoken'.' '.$AccessToken,
                ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);

            $response = json_decode($response);
            if($response->code == 0)
            {
                $query =  Yii::$app->db->createCommand()->update('valuation', ['zoho_date_status' => 1], 'id = '.$valuations->id.'')->execute();
            }
        }
    }

    public function actionZohoInvoicesDateChange($startDate,$toDate)
    {
        $get_valuation = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                Valuation::tableName().'.zoho_invoice_id',
                Valuation::tableName().'.reference_number',
                Valuation::tableName().'.client_id',
                Valuation::tableName().'.submission_approver_date',
                Valuation::tableName().'.total_fee',
                Valuation::tableName().'.property_id',
                Valuation::tableName().'.building_info',
                Valuation::tableName().'.community',
            ])
            ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
            ->where([Valuation::tableName().'.valuation_status' => 5])
            ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
            ->andWhere(['not', [Valuation::tableName().'.zoho_invoice_id' => null]])
            ->andWhere([Valuation::tableName().'.zoho_date_status' => null])
            ->andFilterWhere([
                'between', 'DATE('.Valuation::tableName().'.submission_approver_date)', $startDate, $toDate
            ])
          //  ->limit(5)
            ->all();
        echo "<pre>";
        echo print_r(count($get_valuation));

       // dd($get_valuation);

        foreach($get_valuation as $key => $valuations)
        {
            $clientType = $valuations->client->client_type;
            if ($clientType == "bank") {
                $addTwoMonth = date('d/m/Y', strtotime('+2 months'));
                list($d, $m, $y) = explode("/", $addTwoMonth);
                $dueDate = '01/' . $m . '/' . $y;
                $dueDate = str_replace('/', '-', $dueDate);
                $dueDate = date('Y-m-d', strtotime($dueDate));
            } else {
                $dueDate = $valuations->instruction_date;
            }

            $inspection_type = $valuations->inspection_type;
            if($inspection_type != 3)
            {
                $inspection_type = "Inspection Based";
            }else{
                $inspection_type = "Desktop Based";
            }

            $OrganizationID = Yii::$app->appHelperFunctions->getSetting('zohobooks_organization_id');
            $AccessToken = Yii::$app->appHelperFunctions->getSetting('zohobooks_access_token');

            $curl_b = curl_init();
            curl_setopt_array($curl_b, array(
                CURLOPT_URL => 'https://www.zohoapis.com/books/v3/invoices/'.$valuations->zoho_invoice_id.'?organization_id='.$OrganizationID,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Zoho-oauthtoken'.' '.$AccessToken,
                ),
            ));

            $response_1 = curl_exec($curl_b);
            curl_close($curl_b);
            $response_1 = json_decode($response_1);
            echo 'e1<br>';
            print_r($response_1);

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://www.zohoapis.com/books/v3/invoices/'.$response_1->invoice->invoice_id.'?organization_id='.$OrganizationID,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'PUT',
                CURLOPT_POSTFIELDS => array('JSONString' => '{
                "customer_id": '.$response_1->invoice->customer_id.',
                "branch_id": '.$response_1->invoice->branch_id.',
                "due_date": "'.$dueDate.'",
                "line_items": [
                  {
                    "item_id": '.$response_1->invoice->line_items[0]->item_id.',
                    "description": "'.$inspection_type.' Valuation of '.$valuations->property->title.' - '. $valuations->building->title.' - '.$valuations->community.' - UAE",
                  }
                ],
                  "reason" : "Updating Due Date",
                }'),
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Zoho-oauthtoken'.' '.$AccessToken,
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            $response = json_decode($response);
            echo 'e2<br>';
            print_r($response_1);

            if($response->code == 0)
            {
                $query =  Yii::$app->db->createCommand()->update('valuation', ['zoho_date_status' => 1], 'id = '.$valuations->id.'')->execute();
            }else{
                echo 'here<br>';
            }
            echo print_r($response->message);
        }
    }

    public function actionValuerLatLong()
    {
        //die('new here');

        return $this->render('iploc');

    }
    public function actionCurrentLocation($lat1,$lon1)
    {

      /*  echo Yii::$app->user->identity->id;
        die;*/
      //  return Yii::$app->user->identity->id;
        $total_inspections_scheduled = (new \yii\db\Query())
            ->select(' schedule_inspection.*')
            ->from('schedule_inspection')
            ->innerJoin('valuation', "schedule_inspection.valuation_id=valuation.id")
            ->where(['valuation.valuation_status' => [2]])
            ->andWhere(['schedule_inspection.inspection_date' => date('Y-m-d')])
            ->andWhere(['schedule_inspection.inspection_officer' => Yii::$app->user->identity->id])
            ->andWhere(['valuation.parent_id' => null])
            ->all();

        $check = 0;
        $distance = 0;
        foreach ($total_inspections_scheduled as $key => $data_scheduled) {
           // return Yii::$app->user->identity->id;
            $valuation = Valuation::findOne($data_scheduled['valuation_id']);
            $model_detail = ValuationDetail::find()->where(['valuation_id' => $data_scheduled['valuation_id']])->one();


            if ($lat1 <> null && $lon1 <> null) {
               // $lat1 = 25.113719893522525; // Latitude of location 1
              //  $lon1 = 55.193949013491206; // Longitude of location 1


                $lat2 = $model_detail->latitude; // Latitude of location 2
                $lon2 = $model_detail->longitude; // Longitude of location 2


                if ($lat1 <> null && $lon1 <> null && $lat2 <> null && $lon2 <> null) {
                    $distance = $this->actionDistance($lat1, $lon1, $lat2, $lon2);
                } else {
                   // $distance = 50000;
                }
                //return $distance;

                if ($distance <> null && $distance <= 300 ) {
                    $check= 1;
                    $valuers_location_log = new ValuerLocations();
                    $valuers_location_log->valuer_id = Yii::$app->user->identity->id;
                    $valuers_location_log->valuation_id = $data_scheduled['valuation_id'];
                    $valuers_location_log->current_latitude = $lat1;
                    $valuers_location_log->current_longitude = $lon1;
                    $valuers_location_log->difference_in_meter = $distance;
                    $valuers_location_log->client_latitude = $lat2;
                    $valuers_location_log->client_longitude = $lon2;
                    $valuers_location_log->inpection_id = $valuation->inspectProperty->id;
                    $valuers_location_log->reached_date = date('Y-m-d H:i:s');;
                    //$valuers_location_log->left_date = date('Y-m-d H:i:s');;
                    $valuers_location_log->status_on_location = 1;
                    if (!$valuers_location_log->save()) {
                    } else {

                            if($model_detail->arrived_date_time <> null){

                            }else{
                                $query = Yii::$app->db->createCommand()->update('valuation_detail', ['arrived_date_time' => date('Y-m-d H:i:s')], 'id =' . $model_detail->id)->execute();
                            }


                            $query = Yii::$app->db->createCommand()->update('user', ['location_status' => 1], 'id =' . Yii::$app->user->identity->id)->execute();
                            return 1;


                    }
                }
            }
        }

        if($check == 0){

           // if($distance > 300) {
                $query = Yii::$app->db->createCommand()->update('valuer_locations', ['status_on_location' => 0, 'left_date' => date('Y-m-d H:i:s')], 'status_on_location = 1 AND valuer_id=' . Yii::$app->user->identity->id)->execute();
                $query = Yii::$app->db->createCommand()->update('user', ['location_status' => 0], 'id =' . Yii::$app->user->identity->id)->execute();
                return 2;
              //  return $lat1.'d'.$lon1;
           // }
        }





    }
    public function actionZohoDepreciation()
    {
        $get_items = ExpenseManager::find()
            ->where(['transaction_type' => 'asset'])
            ->andWhere(['status' => 'approved'])
            ->all();

        $id = [];
        foreach($get_items as $key => $items)
        {

            $delivery_date = $items->collection_date;
            $life = $items->asset_life;

            $startDate = new \DateTime($delivery_date);
            $endDate = new \DateTime();

            $interval = $startDate->diff($endDate);

            $years = $interval->format('%y');
            $years = $years*12;

            $months = $interval->format('%m');
            $monthsPassed = $years + $months;

            if( $life <= $monthsPassed )
            {

            }else{
                $id[] = $items->id;
            }

        }

        // Assuming $id is an array of IDs you want to include
        if (!empty($id)) {
            $expenses = ExpenseManager::find()
                ->select([
                    'category_id',
                    new Expression('SUM(amount / asset_life) AS amount'),
                ])
                ->where(['transaction_type' => 'asset'])
                ->andWhere(['status' => 'approved'])
                ->andWhere(['IN', 'id', $id])
                ->groupBy('category_id')
                ->all();
        } else {
            Yii::$app->getSession()->addFlash('error', "No Record Found!");
            return $this->redirect(['site/index']);
        }

        foreach($expenses as $single_expense)
        {
            //for laptops & computer equipments
            if($single_expense->category_id == "4631214000000100466")
            {
                // $debit_account = "4663764000000000451"; // local
                $debit_account = "4631214000000000451"; // live

                // $credit_account = "4663764000000156259"; //local
                $credit_account = "4631214000000100460"; //live

            }
            //for furniture & equipments
            if($single_expense->category_id == "4631214000000100966")
            {
                // $debit_account = "4663764000000000451"; // local
                $debit_account = "4631214000000000451"; // live

                // $credit_account = "4663764000000163035"; //local
                $credit_account = "4631214000000100960"; //live

            }
            //for tools & equipments
            if($single_expense->category_id == "4631214000000103658")
            {
                // $debit_account = "4663764000000000451"; // local
                $debit_account = "4631214000000000451"; // live

                // $credit_account = "4663764000000163041"; //local
                $credit_account = "4631214000000103652"; //live

            }
            //for vehicles
            if($single_expense->category_id == "4631214000000103924")
            {
                // $debit_account = "4663764000000000451"; // local
                $debit_account = "4631214000000000451"; // live

                // $credit_account = "4663764000000163047"; //local
                $credit_account = "4631214000000103900"; //live

            }
            $description = "Depreciation for the Month";

            //createing a Journal
            Yii::$app->appHelperFunctions->ZohoRefereshToken();
            $AccessToken = Yii::$app->appHelperFunctions->getSetting('zohobooks_access_token');
            $OrganizationID = Yii::$app->appHelperFunctions->getSetting('zohobooks_organization_id');
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://books.zoho.com/api/v3/journals?organization_id='.$OrganizationID,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => array('JSONString' => '{
                  "journal_date": "'.date("Y-m-d").'",
                  "reference_number": "DEP-'.date("m", strtotime(date("Y-m-d"))).'/'.date("Y", strtotime(date("Y-m-d"))).'",
                  "notes": "'.$description.'",
                  "line_items": [
                      {
                          "account_id": "'.$debit_account.'",
                          "description": "'.$description.'",
                          "amount": '.$single_expense->amount.',
                          "debit_or_credit": "debit",
                      },
                      {
                          "account_id": "'.$credit_account.'",
                          "description": "'.$description.'",
                          "amount": '.$single_expense->amount.',
                          "debit_or_credit": "credit",
                      }
                  ],

                  "status": "published",
              }'),
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Zoho-oauthtoken'.' '.$AccessToken,
                ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            $response = json_decode($response);
            echo "<pre>";
            print_r($response->message);
            echo "<br>";
            // if($response->code == 0)
            // {
            //     Yii::$app->getSession()->addFlash('success', Yii::t('app', $response->message));
            // }else{
            //     Yii::$app->getSession()->addFlash('error', Yii::t('app', $response->message));
            // }

        }
    }


}
