<?php

namespace app\controllers;

use Yii;
use app\models\MarketCommentary;
use app\models\MarketCommentarySearch;
use app\components\helpers\DefController;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MarketCommentaryController implements the CRUD actions for MarketCommentary model.
 */
class MarketCommentaryController extends DefController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MarketCommentary models.
     * @return mixed
     */
    public function actionIndex($type=0)
    {
        $searchModel = new MarketCommentarySearch();
        $searchModel->property_type = $type;
        $list_title ='Default for All City';
        if($type > 0){
            $property_data = \app\models\Properties::find()
                ->where(['id' => $type])
                ->one();
            /*echo "<pre>";
            print_r($property_data);
            die;*/
            $list_title = $property_data->title;
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'list_title' => $list_title,
            'ptype' => $type,
        ]);
    }

    public function actionIndexAllProperties($type=0)
    {
        $searchModel = new MarketCommentarySearch();
        $searchModel->property_type = $type;

        $property_data = \app\models\Properties::find()
            ->where(['commentary' => 1])
            ->all();

        $list_title ='Default for All City';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index-all-properties', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'list_title' => $list_title,
            'property_data' => $property_data,
        ]);
    }
    /**
     * Displays a single MarketCommentary model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MarketCommentary model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($type=0)
    {
        $model = new MarketCommentary();

        if ($model->load(Yii::$app->request->post())) {
            $model->property_type = $type;
            if($model->save()){
                Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
                return $this->redirect(['index?type='.$type]);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }


        $model_data = MarketCommentary::find()->where(['property_type'=>$type])->orderBy('id DESC')->one();
        if(!empty($model_data)){
            $model->abu_dhabi_commentary = $model_data->abu_dhabi_commentary;
            $model->dubai_commentary = $model_data->dubai_commentary;
            $model->sharjah_commentary = $model_data->sharjah_commentary;
            $model->ajman_commentary = $model_data->ajman_commentary;
            $model->al_ain_commentary = $model_data->al_ain_commentary;
            $model->fujairah_commentary = $model_data->fujairah_commentary;
            $model->ras_al_khaimah_commentary = $model_data->ras_al_khaimah_commentary;
            $model->umm_al_quwain_commentary = $model_data->umm_al_quwain_commentary;
        }


        return $this->render('create', [
            'model' => $model,
            'property_type' => $type,
        ]);


    }

    /**
     * Updates an existing MarketCommentary model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
   /* public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }*/

    /**
     * Deletes an existing MarketCommentary model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
   /* public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/

    /**
     * Finds the MarketCommentary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MarketCommentary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MarketCommentary::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
