<?php

namespace app\controllers;

use Yii;
use app\models\InspectionTransferReasonLog;
use app\models\InspectionTransferReasonLogSearch;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * InspectionTransferReasonLogController implements the CRUD actions for InspectionTransferReasonLog model.
 */
class InspectionTransferReasonLogController extends DefController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all InspectionTransferReasonLog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InspectionTransferReasonLogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single InspectionTransferReasonLog model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

 

    /**
     * Updates an existing InspectionTransferReasonLog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if(!$model->save()){
                echo "<pre>";
                print_r($model->errors);
                die;
            }
            if($model->save()){
                Yii::$app->getSession()->addFlash('success', Yii::t('app','Information updated successfully'));
                return $this->redirect(['index']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing InspectionTransferReasonLog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the InspectionTransferReasonLog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InspectionTransferReasonLog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InspectionTransferReasonLog::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
