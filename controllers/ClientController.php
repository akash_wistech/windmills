<?php

namespace app\controllers;

use app\models\ClientEmailsSettings;
use app\models\CostDetails;
use app\models\Country;
use app\models\TempCopmany;
use app\models\ValuationApproversData;
use app\models\ValuationDetail;
use app\modules\wisnotify\listners\NotifyEvent;
use Yii;
use app\models\Company;
use app\models\CompanySearch;
use app\components\helpers\DefController;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\ValuationSearch;
use yii\web\UploadedFile;
use yii\helpers\Url;
use \app\models\User;
use \app\models\UserProfileInfo;
use yii2tech\csvgrid\CsvGrid;
use yii\data\ActiveDataProvider;
use app\models\Valuation;
use app\models\Buildings;
use app\models\Properties;
use app\models\ValuationPurposes;
use app\models\Zone;
use app\models\Communities;
use app\models\SubCommunities;
use yii\db\Query;


/**
 * ClientController implements the CRUD actions for Company model.
 */
class ClientController extends DefController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Company models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new CompanySearch();
        $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams, 'only_clients');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionFamilyGroups()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new CompanySearch();
        $searchModel->segment_type = 8;
        $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams, 'only_clients');

        return $this->render('index-public-private', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url' => 'client/pre-create-client-contact?s_type=8',
            'form_title' => 'Family Groups',
            's_type' => 8,
        ]);
    }

    public function actionInsuranceCompanies()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new CompanySearch();
        $searchModel->segment_type = 9;
        $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams, 'only_clients');

        return $this->render('index-public-private', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url' => 'client/pre-create-client-contact?s_type=9',
            'form_title' => 'Insurance Companies',
            's_type' => 9,
        ]);
    }

    public function actionLawFirms()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new CompanySearch();
        $searchModel->segment_type = 10;
        $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams, 'only_clients');

        return $this->render('index-public-private', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url' => 'client/pre-create-client-contact?s_type=10',
            'form_title' => 'Law Firms',
            's_type' => 10,
        ]);
    }

    public function actionPubliclyPrivateCompanies()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new CompanySearch();
        $searchModel->segment_type = 11;
        $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams, 'only_clients');

        return $this->render('index-public-private', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url' => 'client/pre-create-client-contact?s_type=11',
            'form_title' => 'Publicly/Private Companies',
            's_type' => 11,
        ]);
    }

    public function actionAuditFirms()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new CompanySearch();
        $searchModel->segment_type = 12;
        $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams, 'only_clients');

        return $this->render('index-public-private', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url' => 'client/pre-create-client-contact?s_type=12',
            'form_title' => 'Audit Firms',
            's_type' => 12,
        ]);
    }

    public function actionGovernmentAuthorties()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new CompanySearch();
        $searchModel->segment_type = 13;
        $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams, 'only_clients');

        return $this->render('index-public-private', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url' => 'client/pre-create-client-contact?s_type=13',
            'form_title' => 'Government Authorties',
            's_type' => 13,
        ]);
    }

    public function actionIndustrialClient()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new CompanySearch();
        $searchModel->segment_type = 14;
        $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams, 'only_clients');

        return $this->render('index-public-private', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url' => 'client/pre-create-client-contact?s_type=14',
            'form_title' => 'Industrial Client',
            's_type' => 14,
        ]);
    }

    public function actionAssetManagementCompanies()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new CompanySearch();
        $searchModel->segment_type = 15;
        $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams, 'only_clients');

        return $this->render('index-public-private', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url' => 'client/pre-create-client-contact?s_type=15',
            'form_title' => 'Asset Management Companies',
            's_type' => 15,
        ]);
    }

    public function actionPropertyManagers()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new CompanySearch();
        $searchModel->segment_type = 16;
        $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams, 'only_clients');

        return $this->render('index-public-private', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url' => 'client/pre-create-client-contact?s_type=16',
            'form_title' => 'Property Managers',
            's_type' => 16,
        ]);
    }

    public function actionOwnersAssociationCompanies()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new CompanySearch();
        $searchModel->segment_type = 17;
        $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams, 'only_clients');

        return $this->render('index-public-private', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url' => 'client/pre-create-client-contact?s_type=17',
            'form_title' => 'Owners Association Companies',
            's_type' => 17,
        ]);
    }

    public function actionIcapContacts()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new CompanySearch();
        $searchModel->segment_type = 7;
        $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams, 'only_clients');

        return $this->render('index-public-private', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url' => 'client/pre-create-client-contact?s_type=7',
            'form_title' => 'ICAP Contacts',
            's_type' => 7,
        ]);
    }

    public function actionIcaiContacts()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new CompanySearch();
        $searchModel->segment_type = 3;
        $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams, 'only_clients');

        return $this->render('index-public-private', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url' => 'client/pre-create-client-contact?s_type=3',
            'form_title' => 'ICAI Contacts',
            's_type' => 3,
        ]);
    }

    public function actionPropertyOwners()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new CompanySearch();
        $searchModel->segment_type = 2;
        $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams, 'only_clients');

        return $this->render('index-public-private', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url' => 'client/pre-create-client-contact?s_type=2',
            'form_title' => 'Property Owners Contacts',
            's_type' => 2,
        ]);
    }

    public function actionBilalContacts()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new CompanySearch();
        $searchModel->segment_type = 1;
        $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams, 'only_clients');

        return $this->render('index-public-private', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url' => 'client/pre-create-client-contact?s_type=1',
            'form_title' => 'Bilal Contacts',
            's_type' => 1,
        ]);
    }

    public function actionBrokerContacts()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new CompanySearch();
        $searchModel->segment_type = 4;
        $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams, 'only_clients');

        return $this->render('index-public-private', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url' => 'client/pre-create-client-contact?s_type=4',
            'form_title' => 'Broker Contacts',
            's_type' => 4,
        ]);
    }

    public function actionBankContacts()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new CompanySearch();
        $searchModel->segment_type = 6;
        $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams, 'only_clients');

        return $this->render('index-public-private', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url' => 'client/pre-create-client-contact?s_type=6',
            'form_title' => 'Bank Contacts',
            's_type' => 6,
        ]);
    }

    public function actionDevelopersContacts()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new CompanySearch();
        $searchModel->segment_type = 5;
        $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams, 'only_clients');

        return $this->render('index-public-private', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'create_url' => 'client/pre-create-client-contact?s_type=5',
            'form_title' => 'Developers Contacts',
            's_type' => 5,
        ]);
    }

    public function actionProspects()
    {
        $this->checkAdmin();
        $searchModel = new CompanySearch();
        $searchModel->data_type = 1;
        // print_r(Yii::$app->request->queryParams); die();
        $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams);

        // echo "<pre>"; print_r($dataProvider); echo "</pre>"; die();

        return $this->render('index_prospect', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreateProspect()
    {
        $this->checkSuperAdmin();

        $request = Yii::$app->request;

        $model = new Company();

        $model->status = 1;
        $model->data_type = 5;
        $model->allow_for_valuation = 1;
        $model->start_date = date("Y-m-d");

        if ($request->isAjax) {
            $model->scenario = 'simplecreate';
        } else {
            $model->scenario = 'normalcreate';
        }

        if ($model->load(Yii::$app->request->post())) {
            // echo "<pre>"; print_r(Yii::$app->request->post()); echo "</pre>"; die();
            if ($model->save()) {
                if ($request->isAjax) {
                    $msg['success'] = ['heading' => Yii::t('app', 'Saved'), 'msg' => Yii::t('app', 'Company saved successfully'), 'id' => $model->id, 'title' => $model->title];
                    echo json_encode($msg);
                    die();
                    exit;
                } else {
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                    return $this->redirect(['prospects']);
                }
            } else {
                if ($request->isAjax) {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    $msg['error'] = ['heading' => Yii::t('app', 'Error'), 'msg' => $val];
                                    echo json_encode($msg);
                                    die();
                                    exit;
                                }
                            }
                        }
                    }
                } else {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($request->isAjax) {
            return $this->renderAjax('_form_short', [
                'model' => $model,
            ]);
        } else {

            return $this->render('create-prospect', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Company model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->checkSuperAdmin();

        $request = Yii::$app->request;

        $model = new Company();
        $model->status = 1;
        $model->start_date = date("Y-m-d");

        if ($request->isAjax) {
            $model->scenario = 'simplecreate';
        } else {
            $model->scenario = 'normalcreate';
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {

                $model->signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                $signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                if ($model->signed_toe <> null) {
                    if ($model->signed_toe <> null && isset($signed_toe)) {
                        if ($model->upload($model->signed_toe, 'signed-toe')) {
                            $model->signed_toe = $model->signed_toe->baseName . '.' . $model->signed_toe->extension;
                            Yii::$app->db->createCommand()
                                ->update('company', ['signed_toe' => $model->signed_toe], 'id=' . $model->id . '')
                                ->execute();
                        }
                    }
                }
                if ($request->isAjax) {
                    $msg['success'] = ['heading' => Yii::t('app', 'Saved'), 'msg' => Yii::t('app', 'Company saved successfully'), 'id' => $model->id, 'title' => $model->title];
                    echo json_encode($msg);
                    die();
                    exit;
                } else {
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                    return $this->redirect(['index']);
                }
            } else {
                if ($request->isAjax) {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    $msg['error'] = ['heading' => Yii::t('app', 'Error'), 'msg' => $val];
                                    echo json_encode($msg);
                                    die();
                                    exit;
                                }
                            }
                        }
                    }
                } else {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                    }
                }
            }
        }
        if ($request->isAjax) {
            return $this->renderAjax('_form_short', [
                'model' => $model,
            ]);
        } else {

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreateBossContact()
    {
        $this->checkSuperAdmin();

        $request = Yii::$app->request;

        $model = new Company();
        $model->status = 1;
        $model->data_type = 6;
        $model->start_date = date("Y-m-d");

        if ($request->isAjax) {
            $model->scenario = 'simplecreate';
        } else {
            $model->scenario = 'normalcreate';
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {

                $model->signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                $signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                if ($model->signed_toe <> null) {
                    if ($model->signed_toe <> null && isset($signed_toe)) {
                        if ($model->upload($model->signed_toe, 'signed-toe')) {
                            $model->signed_toe = $model->signed_toe->baseName . '.' . $model->signed_toe->extension;
                            Yii::$app->db->createCommand()
                                ->update('company', ['signed_toe' => $model->signed_toe], 'id=' . $model->id . '')
                                ->execute();
                        }
                    }
                }
                if ($request->isAjax) {
                    $msg['success'] = ['heading' => Yii::t('app', 'Saved'), 'msg' => Yii::t('app', 'Company saved successfully'), 'id' => $model->id, 'title' => $model->title];
                    echo json_encode($msg);
                    die();
                    exit;
                } else {
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                    return $this->redirect(['contact/bilal-contacts']);
                }
            } else {
                if ($request->isAjax) {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    $msg['error'] = ['heading' => Yii::t('app', 'Error'), 'msg' => $val];
                                    echo json_encode($msg);
                                    die();
                                    exit;
                                }
                            }
                        }
                    }
                } else {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                    }
                }
            }
        }
        if ($request->isAjax) {
            return $this->renderAjax('_form_short', [
                'model' => $model,
            ]);
        } else {

            return $this->render('create-contact', [
                'model' => $model,
                'form_title' => 'Bilal\'s Contacts',
                'existing_valuation_contact' => 0,
                'bilal_contact' => 1,
                'valuation_contact' => 0,
                'prospect_contact' => 0,
                'property_owner_contact' => 0,
                'inquiry_valuations_contact' => 0,
                'icai_contact' => 0,
                'broker_contact' => 0,
                'developer_contact' => 0,
            ]);
        }
    }

    public function actionCreateProspectContact()
    {
        $this->checkSuperAdmin();

        $request = Yii::$app->request;

        $model = new Company();
        //$model->status = 1;
        $model->data_type = 7;
        $model->start_date = date("Y-m-d");

        if ($request->isAjax) {
            $model->scenario = 'simplecreate';
        } else {
            $model->scenario = 'normalcreate';
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {

                $model->signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                $signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                if ($model->signed_toe <> null) {
                    if ($model->signed_toe <> null && isset($signed_toe)) {
                        if ($model->upload($model->signed_toe, 'signed-toe')) {
                            $model->signed_toe = $model->signed_toe->baseName . '.' . $model->signed_toe->extension;
                            Yii::$app->db->createCommand()
                                ->update('company', ['signed_toe' => $model->signed_toe], 'id=' . $model->id . '')
                                ->execute();
                        }
                    }
                }
                if ($request->isAjax) {
                    $msg['success'] = ['heading' => Yii::t('app', 'Saved'), 'msg' => Yii::t('app', 'Company saved successfully'), 'id' => $model->id, 'title' => $model->title];
                    echo json_encode($msg);
                    die();
                    exit;
                } else {
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                    return $this->redirect(['index']);
                }
            } else {
                if ($request->isAjax) {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    $msg['error'] = ['heading' => Yii::t('app', 'Error'), 'msg' => $val];
                                    echo json_encode($msg);
                                    die();
                                    exit;
                                }
                            }
                        }
                    }
                } else {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                    }
                }
            }
        }
        if ($request->isAjax) {
            return $this->renderAjax('_form_short', [
                'model' => $model,
            ]);
        } else {

            return $this->render('create-contact', [
                'model' => $model,
                'form_title' => 'Prospects Contacts',
                'existing_valuation_contact' => 0,
                'bilal_contact' => 0,
                'valuation_contact' => 0,
                'prospect_contact' => 1,
                'property_owner_contact' => 0,
                'inquiry_valuations_contact' => 0,
                'icai_contact' => 0,
                'broker_contact' => 0,
                'developer_contact' => 0,
            ]);
        }
    }

    public function actionCreatePropertyOwnersContact()
    {
        $this->checkSuperAdmin();

        $request = Yii::$app->request;

        $model = new Company();
        // $model->status = 1;
        $model->data_type = 4;
        $model->start_date = date("Y-m-d");

        if ($request->isAjax) {
            $model->scenario = 'simplecreate';
        } else {
            $model->scenario = 'normalcreate';
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {

                $model->signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                $signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                if ($model->signed_toe <> null) {
                    if ($model->signed_toe <> null && isset($signed_toe)) {
                        if ($model->upload($model->signed_toe, 'signed-toe')) {
                            $model->signed_toe = $model->signed_toe->baseName . '.' . $model->signed_toe->extension;
                            Yii::$app->db->createCommand()
                                ->update('company', ['signed_toe' => $model->signed_toe], 'id=' . $model->id . '')
                                ->execute();
                        }
                    }
                }
                if ($request->isAjax) {
                    $msg['success'] = ['heading' => Yii::t('app', 'Saved'), 'msg' => Yii::t('app', 'Company saved successfully'), 'id' => $model->id, 'title' => $model->title];
                    echo json_encode($msg);
                    die();
                    exit;
                } else {
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                    return $this->redirect(['index']);
                }
            } else {
                if ($request->isAjax) {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    $msg['error'] = ['heading' => Yii::t('app', 'Error'), 'msg' => $val];
                                    echo json_encode($msg);
                                    die();
                                    exit;
                                }
                            }
                        }
                    }
                } else {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                    }
                }
            }
        }
        if ($request->isAjax) {
            return $this->renderAjax('_form_short', [
                'model' => $model,
            ]);
        } else {

            return $this->render('create-contact', [
                'model' => $model,
                'form_title' => 'Property Owners',
                'existing_valuation_contact' => 0,
                'bilal_contact' => 0,
                'valuation_contact' => 0,
                'prospect_contact' => 0,
                'property_owner_contact' => 1,
                'inquiry_valuations_contact' => 0,
                'icai_contact' => 0,
                'broker_contact' => 0,
                'developer_contact' => 0,
            ]);
        }
    }

    public function actionCreateValuationInquiryContact()
    {
        $this->checkSuperAdmin();

        $request = Yii::$app->request;

        $model = new Company();
        $model->status = 1;
        $model->data_type = 8;
        $model->start_date = date("Y-m-d");

        if ($request->isAjax) {
            $model->scenario = 'simplecreate';
        } else {
            $model->scenario = 'normalcreate';
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {

                $model->signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                $signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                if ($model->signed_toe <> null) {
                    if ($model->signed_toe <> null && isset($signed_toe)) {
                        if ($model->upload($model->signed_toe, 'signed-toe')) {
                            $model->signed_toe = $model->signed_toe->baseName . '.' . $model->signed_toe->extension;
                            Yii::$app->db->createCommand()
                                ->update('company', ['signed_toe' => $model->signed_toe], 'id=' . $model->id . '')
                                ->execute();
                        }
                    }
                }
                if ($request->isAjax) {
                    $msg['success'] = ['heading' => Yii::t('app', 'Saved'), 'msg' => Yii::t('app', 'Company saved successfully'), 'id' => $model->id, 'title' => $model->title];
                    echo json_encode($msg);
                    die();
                    exit;
                } else {
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                    return $this->redirect(['index']);
                }
            } else {
                if ($request->isAjax) {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    $msg['error'] = ['heading' => Yii::t('app', 'Error'), 'msg' => $val];
                                    echo json_encode($msg);
                                    die();
                                    exit;
                                }
                            }
                        }
                    }
                } else {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                    }
                }
            }
        }
        if ($request->isAjax) {
            return $this->renderAjax('_form_short', [
                'model' => $model,
            ]);
        } else {

            return $this->render('create-contact', [
                'model' => $model,
                'form_title' => 'Valuation Inquiry',
                'existing_valuation_contact' => 0,
                'bilal_contact' => 0,
                'valuation_contact' => 0,
                'prospect_contact' => 0,
                'property_owner_contact' => 1,
                'inquiry_valuations_contact' => 0,
                'icai_contact' => 0,
                'broker_contact' => 0,
                'developer_contact' => 0,
            ]);
        }
    }

    public function actionCreateIciaContact()
    {
        $this->checkSuperAdmin();

        $request = Yii::$app->request;

        $model = new Company();
        $model->status = 1;
        $model->data_type = 9;
        $model->start_date = date("Y-m-d");

        if ($request->isAjax) {
            $model->scenario = 'simplecreate';
        } else {
            $model->scenario = 'normalcreate';
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {

                $model->signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                $signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                if ($model->signed_toe <> null) {
                    if ($model->signed_toe <> null && isset($signed_toe)) {
                        if ($model->upload($model->signed_toe, 'signed-toe')) {
                            $model->signed_toe = $model->signed_toe->baseName . '.' . $model->signed_toe->extension;
                            Yii::$app->db->createCommand()
                                ->update('company', ['signed_toe' => $model->signed_toe], 'id=' . $model->id . '')
                                ->execute();
                        }
                    }
                }
                if ($request->isAjax) {
                    $msg['success'] = ['heading' => Yii::t('app', 'Saved'), 'msg' => Yii::t('app', 'Company saved successfully'), 'id' => $model->id, 'title' => $model->title];
                    echo json_encode($msg);
                    die();
                    exit;
                } else {
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                    return $this->redirect(['index']);
                }
            } else {
                if ($request->isAjax) {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    $msg['error'] = ['heading' => Yii::t('app', 'Error'), 'msg' => $val];
                                    echo json_encode($msg);
                                    die();
                                    exit;
                                }
                            }
                        }
                    }
                } else {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                    }
                }
            }
        }
        if ($request->isAjax) {
            return $this->renderAjax('_form_short', [
                'model' => $model,
            ]);
        } else {

            return $this->render('create-contact', [
                'model' => $model,
                'form_title' => 'Icia Contacts',
                'existing_valuation_contact' => 0,
                'bilal_contact' => 0,
                'valuation_contact' => 0,
                'prospect_contact' => 0,
                'property_owner_contact' => 0,
                'inquiry_valuations_contact' => 0,
                'icai_contact' => 1,
                'broker_contact' => 0,
                'developer_contact' => 0,
            ]);
        }
    }

    public function actionCreateBrokerContact()
    {
        $this->checkSuperAdmin();

        $request = Yii::$app->request;

        $model = new Company();
        $model->status = 1;
        $model->data_type = 10;
        $model->start_date = date("Y-m-d");

        if ($request->isAjax) {
            $model->scenario = 'simplecreate';
        } else {
            $model->scenario = 'normalcreate';
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {

                $model->signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                $signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                if ($model->signed_toe <> null) {
                    if ($model->signed_toe <> null && isset($signed_toe)) {
                        if ($model->upload($model->signed_toe, 'signed-toe')) {
                            $model->signed_toe = $model->signed_toe->baseName . '.' . $model->signed_toe->extension;
                            Yii::$app->db->createCommand()
                                ->update('company', ['signed_toe' => $model->signed_toe], 'id=' . $model->id . '')
                                ->execute();
                        }
                    }
                }
                if ($request->isAjax) {
                    $msg['success'] = ['heading' => Yii::t('app', 'Saved'), 'msg' => Yii::t('app', 'Company saved successfully'), 'id' => $model->id, 'title' => $model->title];
                    echo json_encode($msg);
                    die();
                    exit;
                } else {
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                    return $this->redirect(['index']);
                }
            } else {
                if ($request->isAjax) {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    $msg['error'] = ['heading' => Yii::t('app', 'Error'), 'msg' => $val];
                                    echo json_encode($msg);
                                    die();
                                    exit;
                                }
                            }
                        }
                    }
                } else {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                    }
                }
            }
        }
        if ($request->isAjax) {
            return $this->renderAjax('_form_short', [
                'model' => $model,
            ]);
        } else {

            return $this->render('create-contact', [
                'model' => $model,
                'form_title' => 'Broker Contacts',
                'existing_valuation_contact' => 0,
                'bilal_contact' => 0,
                'valuation_contact' => 0,
                'prospect_contact' => 0,
                'property_owner_contact' => 0,
                'inquiry_valuations_contact' => 0,
                'icai_contact' => 0,
                'broker_contact' => 1,
                'developer_contact' => 0,
            ]);
        }
    }

    public function actionCreateDevelopersContact()
    {
        $this->checkSuperAdmin();

        $request = Yii::$app->request;

        $model = new Company();
        //$model->status = 1;
        $model->data_type = 11;
        $model->allow_for_valuation = 1;
        $model->start_date = date("Y-m-d");

        if ($request->isAjax) {
            $model->scenario = 'simplecreate';
        } else {
            $model->scenario = 'normalcreate';
        }

        if ($model->load(Yii::$app->request->post())) {

            if ($model->save()) {

                $model->signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                $signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                if ($model->signed_toe <> null) {
                    if ($model->signed_toe <> null && isset($signed_toe)) {
                        if ($model->upload($model->signed_toe, 'signed-toe')) {
                            $model->signed_toe = $model->signed_toe->baseName . '.' . $model->signed_toe->extension;
                            Yii::$app->db->createCommand()
                                ->update('company', ['signed_toe' => $model->signed_toe], 'id=' . $model->id . '')
                                ->execute();
                        }
                    }
                }
                if ($request->isAjax) {
                    //  $msg['success'] = ['heading' => Yii::t('app', 'Saved'), 'msg' => Yii::t('app', 'Company saved successfully gg'), 'id' => $model->id, 'title' => $model->title];
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                    return $this->redirect(['client/updatec/' . $model->id]);

                    echo json_encode($msg);
                    die();
                    exit;
                } else {
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                    return $this->redirect(['index']);
                }
            } else {
                if ($request->isAjax) {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    $msg['error'] = ['heading' => Yii::t('app', 'Error'), 'msg' => $val];
                                    echo json_encode($msg);
                                    die();
                                    exit;
                                }
                            }
                        }
                    }
                } else {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                    }
                }
            }
        }
        if ($request->isAjax) {
            return $this->renderAjax('_form_short', [
                'model' => $model,
            ]);
        } else {

            return $this->render('create-contact', [
                'model' => $model,
                'form_title' => 'Developer Contacts',
                'existing_valuation_contact' => 0,
                'bilal_contact' => 0,
                'valuation_contact' => 0,
                'prospect_contact' => 0,
                'property_owner_contact' => 0,
                'inquiry_valuations_contact' => 0,
                'icai_contact' => 0,
                'broker_contact' => 0,
                'developer_contact' => 1,
            ]);
        }
    }

    public function actionPreCreateClientContact($s_type = null)
    {
        $this->checkSuperAdmin();


        $request = Yii::$app->request;
        if ($s_type <> null) {
            $client_segment = Yii::$app->appHelperFunctions->clientContactTypes[$s_type];
        } else {
            $client_segment = 'Create Contact';
        }

        $model = new Company();

        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['client/updatec/' . $model->id]);
        }


        return $this->render('pre-create-contact', [
            'model' => $model,
            'form_title' => $client_segment,
            's_type' => $s_type,
        ]);
    }

    public function actionCreateClientContact($s_type = null)
    {
        $this->checkSuperAdmin();

        $request = Yii::$app->request;
        if ($s_type <> null) {
            $client_segment = Yii::$app->appHelperFunctions->clientContactTypes[$s_type];
        } else {
            $client_segment = 'Create Contact';
        }

        /*      $session = Yii::$app->session;

              if($session->has('new_contact')){

              }
              echo "<pre>";
              print_r($session);
              die;*/


        $model = new Company();
        //$model->status = 1;
        $model->data_type = $s_type;

        $model->allow_for_valuation = 1;
        $model->start_date = date("Y-m-d");

        if ($request->isAjax) {
            $model->scenario = 'simplecreate';
        } else {
            $model->scenario = 'normalcreate';
        }
        $positions = ArrayHelper::map(\app\models\JobTitle::find()->where(['status' => 1])->all(), 'id', 'title');
        $departments = ArrayHelper::map(\app\models\Department::find()->where(['status' => 1])->all(), 'id', 'title');
        $countries = ArrayHelper::map(\app\models\Country::find()->where(['status' => 1])->all(), 'id', 'title');
        /* echo "<pre>";
         print_r($_SESSION['contacts_data']);
         die;*/
        if ($model->load(Yii::$app->request->post())) {
            $model->segment_type = $s_type;
            if ($model->save()) {

                $model->signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                $signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                if ($model->signed_toe <> null) {
                    if ($model->signed_toe <> null && isset($signed_toe)) {
                        if ($model->upload($model->signed_toe, 'signed-toe')) {
                            $model->signed_toe = $model->signed_toe->baseName . '.' . $model->signed_toe->extension;
                            Yii::$app->db->createCommand()
                                ->update('company', ['signed_toe' => $model->signed_toe], 'id=' . $model->id . '')
                                ->execute();
                        }
                    }
                }
                if ($request->isAjax) {
                    //  $msg['success'] = ['heading' => Yii::t('app', 'Saved'), 'msg' => Yii::t('app', 'Company saved successfully gg'), 'id' => $model->id, 'title' => $model->title];
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                    //return $this->redirect(['client/update/' . $model->id]);
                    if ($model->segment_type == 1) {
                        return $this->redirect(['client/bilal-contacts']);
                    } else if ($model->segment_type == 2) {
                        return $this->redirect(['client/property-owners']);
                    }else if ($model->segment_type == 3) {
                        return $this->redirect(['client/icai-contacts']);
                    }else if ($model->segment_type == 4) {
                        return $this->redirect(['client/broker-contacts']);
                    }else if ($model->segment_type == 5) {
                        return $this->redirect(['client/developers-contacts']);
                    }else if ($model->segment_type == 6) {
                        return $this->redirect(['client/banks-contacts']);
                    }else if ($model->segment_type == 7) {
                        return $this->redirect(['client/icap-contacts']);
                    }else if ($model->segment_type == 8) {
                        return $this->redirect(['client/family-groups']);
                    }else if ($model->segment_type == 9) {
                        return $this->redirect(['client/insurance-companies']);
                    }else if ($model->segment_type == 10) {
                        return $this->redirect(['client/law-firms']);
                    }else if ($model->segment_type == 11) {
                        return $this->redirect(['client/publicly-private-companies']);
                    }else if ($model->segment_type == 12) {
                        return $this->redirect(['client/audit-firms']);
                    }else if ($model->segment_type == 13) {
                        return $this->redirect(['client/government-authorties']);
                    }else if ($model->segment_type == 14) {
                        return $this->redirect(['client/industrial-client']);
                    }else if ($model->segment_type == 15) {
                        return $this->redirect(['client/asset-management-companies']);
                    }else if ($model->segment_type == 16) {
                        return $this->redirect(['client/property-managers']);
                    }else if ($model->segment_type == 17) {
                        return $this->redirect(['client/owners-association-companies']);
                    } else {
                        return $this->redirect(['client/update/' . $model->id]);
                    }
                    return $this->redirect(['client/update/' . $model->id]);

                    echo json_encode($msg);
                    die();
                    exit;
                } else {
                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                    if ($model->segment_type == 1) {
                        return $this->redirect(['client/bilal-contacts']);
                    } else if ($model->segment_type == 2) {
                        return $this->redirect(['client/property-owners']);
                    }else if ($model->segment_type == 3) {
                        return $this->redirect(['client/icai-contacts']);
                    }else if ($model->segment_type == 4) {
                        return $this->redirect(['client/broker-contacts']);
                    }else if ($model->segment_type == 5) {
                        return $this->redirect(['client/developers-contacts']);
                    }else if ($model->segment_type == 6) {
                        return $this->redirect(['client/banks-contacts']);
                    }else if ($model->segment_type == 7) {
                        return $this->redirect(['client/icap-contacts']);
                    }else if ($model->segment_type == 8) {
                        return $this->redirect(['client/family-groups']);
                    }else if ($model->segment_type == 9) {
                        return $this->redirect(['client/insurance-companies']);
                    }else if ($model->segment_type == 10) {
                        return $this->redirect(['client/law-firms']);
                    }else if ($model->segment_type == 11) {
                        return $this->redirect(['client/publicly-private-companies']);
                    }else if ($model->segment_type == 12) {
                        return $this->redirect(['client/audit-firms']);
                    }else if ($model->segment_type == 13) {
                        return $this->redirect(['client/government-authorties']);
                    }else if ($model->segment_type == 14) {
                        return $this->redirect(['client/industrial-client']);
                    }else if ($model->segment_type == 15) {
                        return $this->redirect(['client/asset-management-companies']);
                    }else if ($model->segment_type == 16) {
                        return $this->redirect(['client/property-managers']);
                    }else if ($model->segment_type == 17) {
                        return $this->redirect(['client/owners-association-companies']);
                    } else {
                        return $this->redirect(['client/update/' . $model->id]);
                    }
                    return $this->redirect(['client/update/' . $model->id]);
                    
                }
            } else {
                if ($request->isAjax) {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    $msg['error'] = ['heading' => Yii::t('app', 'Error'), 'msg' => $val];
                                    echo json_encode($msg);
                                    die();
                                    exit;
                                }
                            }
                        }
                    }
                } else {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                    }
                }
            }
        }
        if ($request->isAjax) {
            return $this->renderAjax('_form_short', [
                'model' => $model,
            ]);
        } else {
            session_start();          // Start the session
            $_SESSION['contacts_data'] = array();

            return $this->render('create-contact-new', [
                'model' => $model,
                'positions' => $positions,
                'departments' => $departments,
                'countries' => $countries,
                'form_title' => $client_segment,
                'existing_valuation_contact' => 0,
                'bilal_contact' => 0,
                'valuation_contact' => 0,
                'prospect_contact' => 0,
                'property_owner_contact' => 0,
                'inquiry_valuations_contact' => 0,
                'icai_contact' => 0,
                'broker_contact' => 0,
                'developer_contact' => 1,
            ]);
        }
    }


    public function actionAddnewcontact()
    {
        $this->checkSuperAdmin();

        $form_data = Yii::$app->request->post();

        $session = Yii::$app->session;
        // $session->set('contacts_dataw', $sess);
        $html = '';
        if ($form_data <> null && !empty($form_data)) {
            if ($session->has('contacts_data')) {
                $emails_array = array();
                if ($session['contacts_data'] <> null and !empty($session['contacts_data'])) {
                    foreach ($session['contacts_data'] as $ke => $contact_data) {
                        $emails_array[] = $contact_data['email'];

                    }
                }
                if (in_array($form_data['email'], $emails_array)) {

                } else {
                    $_SESSION['contacts_data'][] = $form_data;
                }

                if ($session['contacts_data'] <> null and !empty($session['contacts_data'])) {
                    foreach ($session['contacts_data'] as $key => $_contact) {
                        $position = \app\models\JobTitle::find()->where(['id' => $_contact['position']])->one();
                        $html .= '<tr>';
                        $html .= '<td>' . $_contact['firstName'] . '</td>';
                        $html .= '<td>' . $_contact['lastName'] . '</td>';
                        $html .= '<td>' . $_contact['email'] . '</td>';
                        $html .= '<td>' . $position->title . '</td>';
                        $html .= '<td>' . $_contact['country_code'] . $_contact['mobile1'] . '</td>';


                        $html .= '<td>
    <button type="button" data-toggle="tooltip" title="Add"
            class="btn btn-primary create-instructing-person"><i
            class="fa fa-minus-square"></i></button>
   
</td>';
                        $html .= '</tr>';

                    }

                    $html .= '<tr>';
                    $html .= '<td colspan="5"></td>';
                    $html .= '<td>';
                    $html .= ' <button type="button" data-toggle="modal" data-target="#contactModal" title="Add"
                                        class="btn btn-primary create-instructing-person"><i
                                            class="fa fa-plus-square"></i></button>';

                    $html .= '</td>';

                    $html .= '</tr>';

                }

                /*   echo "<pre>";
                   print_r($session['contacts_data']);
                   die('');*/


            }

        }
        return json_encode(['html' => $html]);

    }

    public function actionCheckEmailExistence()
    {


        $email = \Yii::$app->request->post('email');

        $user = User::findOne(['email' => $email]);

        if ($user) {
            return json_encode(['exists' => 1]);
        } else {
            return json_encode(['exists' => 0]);
        }
    }

    public function actionGetCities($countryId)
    {
        $country = Country::find()->where(['id' => $countryId])->one();
        $cities = Zone::find()->where(['country_id' => $countryId])->all();
        $cityOptions = '';
        foreach ($cities as $city) {
            $cityOptions .= "<option value='{$city->id}'>{$city->title}</option>";
        }

        return json_encode(['cities' => $cityOptions, 'phone_code' => $country->phone_code]);
    }


    public function actionSearch($q = null)
    {

        $results = (new \yii\db\Query())
            ->select('id,title,status')
            ->from('company')
            ->where(['LIKE', 'title', '%' . $q . '%', false])
            ->all();


        //$results = []; // Query your database or data source to fetch search results based on $q

        // Format results for Select2
        foreach ($results as $result) {
            $status = ' (Unverified)';
            if ($result['status'] == 1) {
                $status = ' (Verified)';
            }
            $results[] = ['id' => $result['id'], 'text' => $result['title'] . $status];
        }

        return json_encode(['results' => $results]);
    }

    public function actionClientContactUpdate($id)

    {

        $searchModel = new ValuationSearch();
        $searchModel->client_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 3);

        $pcontact = User::find()
            ->where(['company_id' => $id, 'primary_contact' => 1])
            ->innerJoin(UserProfileInfo::tableName(), UserProfileInfo::tableName() . ".user_id=" . User::tableName() . ".id")
            ->one();
        $redirect_url = 'index';
        if ($pcontact->bilal_contact == 1) {
            $redirect_url = 'contact/bilal-contacts';
        } else if ($pcontact->existing_valuation_contact == 1) {

        }
        /* echo "<pre>";
         print_r($pcontact);
         die;*/

        $this->checkLogin();

        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post())) {

            // dd(Yii::$app->request->post());
            if ($pcontact->prospect_contact == 1) {
                if ($model->status == 1 && ((Yii::$app->user->identity->id == 1) || (Yii::$app->user->identity->id == 33) || (Yii::$app->user->identity->id == 110217))) {
                    $model->allow_for_valuation = 1;
                }

            }
            if ($model->save()) {


                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information saved successfully'));
                // return $this->redirect(['contact/index']);
                return $this->redirect(['client/updatec/' . $model->id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('formupdate', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'existing_valuation_contact' => $pcontact->existing_valuation_contact,
            'bilal_contact' => $pcontact->bilal_contact,
            'valuation_contact' => $pcontact->valuation_contact,
            'prospect_contact' => $pcontact->prospect_contact,
            'property_owner_contact' => $pcontact->property_owner_contact,
            'inquiry_valuations_contact' => $pcontact->inquiry_valuations_contact,
            'icai_contact' => $pcontact->icai_contact,
            'broker_contact' => $pcontact->broker_contact,
            'developer_contact' => $pcontact->developer_contact,
        ]);
    }

    /**
     * Updates an existing Company model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate_old($id)

    {
        $searchModel = new ValuationSearch();
        $searchModel->client_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 3);

        $this->checkLogin();
        $model = $this->findModel($id);
        $model->manager_id = ArrayHelper::map($model->managerIdz, "staff_id", "staff_id");

        if ($model->load(Yii::$app->request->post())) {
//            echo"<pre>"; print_r(Yii::$app->request->post()); echo"</pre>"; die();
            if ($model->save()) {

                //$model->signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                $signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                if ($signed_toe <> null) {
                    if (isset($signed_toe)) {
                        $model->signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                        if ($model->upload($model->signed_toe, 'signed-toe')) {
                            $model->signed_toe = $model->signed_toe->baseName . '.' . $model->signed_toe->extension;
                            Yii::$app->db->createCommand()
                                ->update('company', ['signed_toe' => $model->signed_toe], 'id=' . $model->id . '')
                                ->execute();
                        }
                    }
                }
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information saved successfully'));
                return $this->redirect(['index']);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id)

    {
        $searchModel = new ValuationSearch();
        $searchModel->client_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 3);

        $this->checkLogin();
        $model = $this->findModel($id);
        $model->manager_id = ArrayHelper::map($model->managerIdz, "staff_id", "staff_id");

        if ($model->load(Yii::$app->request->post())) {
//            echo"<pre>"; print_r(Yii::$app->request->post()); echo"</pre>"; die();
            if ($model->save()) {

                //$model->signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                $signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                if ($signed_toe <> null) {
                    if (isset($signed_toe)) {
                        $model->signed_toe = UploadedFile::getInstance($model, 'signed_toe');
                        if ($model->upload($model->signed_toe, 'signed-toe')) {
                            $model->signed_toe = $model->signed_toe->baseName . '.' . $model->signed_toe->extension;
                            Yii::$app->db->createCommand()
                                ->update('company', ['signed_toe' => $model->signed_toe], 'id=' . $model->id . '')
                                ->execute();
                        }
                    }
                }
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information saved successfully'));
                return $this->redirect(['index']);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('_form', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdatec($id)

    {

        $searchModel = new ValuationSearch();
        $searchModel->client_id = $id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 3);

        $pcontact = User::find()
            ->where(['company_id' => $id, 'primary_contact' => 1])
            ->innerJoin(UserProfileInfo::tableName(), UserProfileInfo::tableName() . ".user_id=" . User::tableName() . ".id")
            ->one();
        $redirect_url = 'index';
        if ($pcontact->bilal_contact == 1) {
            $redirect_url = 'contact/bilal-contacts';
        } else if ($pcontact->existing_valuation_contact == 1) {

        }
        /* echo "<pre>";
         print_r($pcontact);
         die;*/

        $this->checkLogin();

        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post())) {

            // dd(Yii::$app->request->post());
            if ($pcontact->prospect_contact == 1) {
                if ($model->status == 1 && ((Yii::$app->user->identity->id == 1) || (Yii::$app->user->identity->id == 33) || (Yii::$app->user->identity->id == 110217))) {
                    $model->allow_for_valuation = 1;
                }

            }
            if ($model->save()) {


                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information saved successfully'));
                // return $this->redirect(['contact/index']);
                return $this->redirect(['client/updatec/' . $model->id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('formupdate', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'existing_valuation_contact' => $pcontact->existing_valuation_contact,
            'bilal_contact' => $pcontact->bilal_contact,
            'valuation_contact' => $pcontact->valuation_contact,
            'prospect_contact' => $pcontact->prospect_contact,
            'property_owner_contact' => $pcontact->property_owner_contact,
            'inquiry_valuations_contact' => $pcontact->inquiry_valuations_contact,
            'icai_contact' => $pcontact->icai_contact,
            'broker_contact' => $pcontact->broker_contact,
            'developer_contact' => $pcontact->developer_contact,
        ]);
    }

    /**
     * Creates a new model.
     */
    protected function newModel()
    {
        $model = new Company;
        return $model;
    }

    /**
     * Finds the Company model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Company the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }


    public function actionPdf($id)
    {


        $model = $this->findModel($id);


        // Include the main TCPDF library (search for installation path).
        require_once(__DIR__ . '/../components/tcpdf/MyPDF.php');

        // create new PDF document
        $pdf = new \MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        //  $pdf->model = $model;

        $pdf->report_type = 2;

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle('Client Report');
        $pdf->SetSubject('Valuation Report');
        $pdf->SetKeywords('');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(8, 20, 8);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        // ---------------------------------------------------------
        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('arialn', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P', 'A4');

        $bodydata = \app\models\SpecialAssumptionreport::findOne(1);


        $valuation_data = \app\models\Valuation::find()->where(['client_id' => $id])->andWhere(['!=', 'valuation_status', 5])->asArray()->all();


        $html = '

      <style>

      .bold{
        font-weight:bold;
      }
      table.main-class{
        border-top: 1px solid gray;
        border-bottom: 1px solid gray;
        border-left: 1px solid gray;
        border-right: 1px solid gray;
        font-size:10px;
        text-align:left;
        font-weight:bold;
      }

    tr:nth-child(even) {
      background-color: #dddddd;
    }

    td.table_of_content{
       color:#0D47A1;
       font-size:11px;
       font-weight:bold;
       border-bottom: 1px solid #BBDEFB;
       text-align:center;
    }
      </style>



      <p>To,</p>
      <p>' . $model->title . '</p>
      <h3><u>Subject: Periodical Valuation Status Report as of ' . date('l, jS \of F, Y') . '</u></h3><br>
      <p>' . $bodydata['status_report'] . '</p><br>
      <table class="main-class" cellspacing="2" cellpadding="5" style="border: 1px dashed #64B5F6;">
      <tr style="background-color:#ECEFF1; text-align:center">
      <td class="table_of_content"><br><b>NO</b></td>
      <td class="table_of_content" colspan="2"><br><b>Instruction Date</b></td>
      <td class="table_of_content" colspan="2"><br><b>Inspection Date</b></td>
      <td class="table_of_content" colspan="2"><br><b>Target Submission Date</b></td>
      <td class="table_of_content" colspan="2"><br><b>Client Reference</b></td>
      <td class="table_of_content" colspan="2"><br><b>Property Category</b></td>
      <td class="table_of_content" colspan="2"><br><b>Property Type</b></td>
      <td class="table_of_content" colspan="2"><br><b>Building Info</b></td>
      <td class="table_of_content" colspan="2"><br><b>Sub Community</b></td>
      </tr>

      ';
        $i = 1;
        $j = 1;

        foreach ($valuation_data as $value) {

            // $valuation_add = ValuationApproversData::find()->where(['valuation_id' => $value['id'], 'approver_type' => 'approver', 'status' => 'Approve'])->one();

            // if (!empty($valuation_add)) {
            //
            // } else {
            $inspectionDate = \app\models\ScheduleInspection::find()->where(['valuation_id' => $value['id']])->one();
            $propetyType = \app\models\Properties::find()->where(['id' => $value['property_id']])->one();
            $building_info = \app\models\Buildings::find()->where(['id' => $value['building_info']])->one();
            $subCommunity = \app\models\SubCommunities::find()->where(['id' => $building_info['sub_community']])->one();

            $instruction_date = ($value['instruction_date'] <> null) ? date('d-m-Y', strtotime($value['instruction_date'])) : 'Not yet scheduled';
            $inspection_date = ($inspectionDate['inspection_date'] <> null) ? date('d-m-Y', strtotime($inspectionDate['inspection_date'])) : 'Not yet scheduled';
            $target_date = ($value['target_date'] <> null) ? date('d-m-Y', strtotime($value['target_date'])) : 'Not yet scheduled';

            // echo "<pre>";
            // print_r($subCommunity['title']);
            // echo "<pre>";
            // die();

            if ($j == 1) {
                $j++;
                $val = '';
            } elseif ($j == 2) {
                $j = 1;
                $val = 'style="background-color:#ECEFF1; "';
            }

            $html .= '
                  <tr ' . $val . '>
                  <td>' . $i++ . '</td>
                  <td colspan="2">' . $instruction_date . '</td>
                  <td colspan="2">' . $inspection_date . '</td>
                  <td colspan="2">' . $target_date . '</td>
                  <td colspan="2">' . $value['client_reference'] . '</td>
                  <td colspan="2">' . Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$value['property_category']] . '</td>
                  <td colspan="2">' . $propetyType['title'] . '</td>
                  <td colspan="2">' . $building_info['title'] . '</td>
                  <td colspan="2">' . $subCommunity['title'] . '</td>
                  </tr>';

            //}

        }

        $html .= '</table>';

        if ($i == 1) {
            $html = '';
        }
        $pdf->writeHTML($html, true, false, false, false, '');

        // ---------------------------------------------------------
        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $pdf->Output($value['reference_number'] . '.pdf', 'I');
    }


    public function actionSendpdf($id)

    {
        // echo $id;
        // die();
        //  $model = ValuationApproversData::find()->where(['id'=>$id ])->one();
        //$valuation = Valuation::find()->where(['id'=>$model->valuation_id])->one();
        $client = Company::find()->where(['id' => $id])->one();

        $url = Url::toRoute(['pdf', 'id' => $id]);

        $notifyData = [
            'client' => $client,
            'attachments' => [],
            'replacements' => [
                '{clientName}' => $valuation->client->title,
                '{pdflink}' => $url,

            ],
        ];
        // print_r($valuation);
        // die();
        \app\modules\wisnotify\listners\NotifyEvent::fire('Client.pdf.send', $notifyData);
        return $this->redirect(['client/update/' . $id]);
    }


    public function actionSendemail($id)
    {

        $model = $this->findModel($id);
        //Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document]
        if ($model->primaryContact->email != null) {

            //$url = Url::toRoute(['client/pdf', 'id' =>$model['id']]);
            $val = Yii::$app->helperFunctions->getDonwloadpdf($model['id']);
            if ($val != null) {
                $attachments = $val;
                $notifyData = [
                    'client' => $model,
                    'attachments' => $attachments,
                    'replacements' => [
                        '{clientName}' => $model['title'],
                    ],
                ];

                \app\modules\wisnotify\listners\NotifyEvent::fire('Client.pdf.send', $notifyData);

            } else {
                $attachments = '';
            }
        }

        echo "working";
        die();
    }


    public function actionSavePreviousCustomersToQuickbooks()
    {
        $models = Company::find()
            ->orWhere(['quickbooks_registration_id' => null])->all();
        foreach ($models as $model) {
            // echo "<pre>"; print_r($model); echo "</pre>"; die();
            $model->save();
        }
        Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Saved Successfully!'));
        return $this->redirect(['index']);


    }

    public function actionRemoveAttachment($id)
    {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;


        $model = ClientEmailsSettings::findOne($id);

        if ($model !== null) {
            $model->delete();
            return ['status' => 'OK', 'message' => 'Email deleted successfully.'];
        } else {
            throw new HttpException(422, 'Invalid request id', 422);
        }
    }


    public function actionOtherInstructingPersons_old()
    {
        $post = Yii::$app->request->post();
        // print_r($post['keyword']); die();
        if ($post['client_id'] <> null) {
            $results = \app\models\OtherInstructingPerson::find()->where(['client_id' => $post['client_id']])->all();
            // echo "<pre>"; print_r($results); echo "</pre>"; die();

            $html = '';
            $html .= '<div class="col-sm-4 appentWali-row">';
            $html .= '<div class="form-group other-instructing-person-field has-success">';
            $html .= '<label class="control-label" for="other-instructing-person">Instructing Person</label>';
            $html .= '<select id="other-instructing-person" class="form-control" name="' . $post['keyword'] . '[other_instructing_person]" aria-invalid="false">';
            $html .= '<option value="">Select</option>';

            foreach ($results as $key => $value) {
                $selected = '';
                if ($value->id == $post['other_instructing_person_id']) {
                    $selected = 'selected';
                }
                $html .= '<option value="' . $value->id . '" ' . $selected . '>' . $value->detail . '</option>';
            }

            $html .= '</select>';
            $html .= '<div class="help-block"></div>';
            $html .= '</div>';
            $html .= '</div>';


            return $html;
        } else {
            return '';
        }
    }

    public function actionOtherInstructingPersons()
    {
        $post = Yii::$app->request->post();
        // print_r($post['keyword']); die();
        if ($post['client_id'] <> null) {
            $results = //\app\models\InstructingPersonInfo::find()->where(['client_id'=>$post['client_id']])->asArray()->all();
                \app\models\User::find()
                    ->where(['company_id' => $post['client_id']])
                    ->innerJoin(\app\models\UserProfileInfo::tableName(), \app\models\UserProfileInfo::tableName() . ".user_id=" . \app\models\User::tableName() . ".id")
                    // ->asArray()
                    ->all();
            // echo "<pre>"; print_r($results->); echo "</pre>"; die();

            $html = '';
           // $html .= '<div class="col-sm-4 appentWali-row">';
            $html .= '<div class="form-group other-instructing-person-field has-success">';
            $html .= '<label class="control-label" for="other-instructing-person">Instructing Person</label>';
            $html .= '<select id="other-instructing-person" class="form-control" name="' . $post['keyword'] . '[other_instructing_person]" aria-invalid="false">';
            $html .= '<option value="">Select</option>';
            $contact_ids = array();
            $selected_check = 0;
            foreach ($results as $key => $value) {
                $contact_ids[] = $value->id;
            }
            if (in_array($post['other_instructing_person_id'], $contact_ids)) {
                $selected_check = 1;
            }

            foreach ($results as $key => $value) {
                // echo "<pre>"; print_r($post['other_instructing_person_id']); echo "</pre>"; die();
                $profileData = \app\models\UserProfileInfo::find()->where(['user_id'=>$value->id])->one();
                $fullName = $value->firstname . ' ' . $value->lastname . ' ('.$value->email.')';
                $id = (int)$value->id . '<br>';
                $other_instructing_person_id = (int)$post['other_instructing_person_id'] . '<br>';
                if ($selected_check == 1) {

                    $selected = '';
                    if ($id == $other_instructing_person_id) {
                        $selected = 'selected';
                    }
                } else {


                    if ($value->profileInfo->primary_contact == 1) {
                        $selected = 'selected';
                    }
                }
                $html .= '<option data_phone="'.$profileData->mobile.'" data_email="'.$value->email.'" value="' . $value->id . '" ' . $selected . '>' . $fullName . '</option>';
            }

            $html .= '</select>';
            $html .= '<div class="help-block"></div>';
          //  $html .= '</div>';
            $html .= '</div>';


            return $html;
        } else {
            return '';
        }
    }

    // for instruction person in crmquotations
    public function actionOtherInstructingPersonsCrm()
    {
        $post = Yii::$app->request->post();
        // print_r($post['keyword']); die();
        if ($post['client_name'] <> null) {
            $results = \app\models\User::find()
                    ->where(['company_id' => $post['client_name']])
                    ->innerJoin(\app\models\UserProfileInfo::tableName(), \app\models\UserProfileInfo::tableName() . ".user_id=" . \app\models\User::tableName() . ".id")
                    ->andWhere([\app\models\UserProfileInfo::tableName() . '.primary_contact' => 1])
                    // ->asArray()
                    ->all();
            // echo "<pre>"; print_r($results->); echo "</pre>"; die();
            //echo "<pre>"; print_r($results[0]->email); echo "</pre>"; die();
            

            $html = '';
           // $html .= '<div class="col-sm-4 appentWali-row">';
            $html .= '<div class="form-group other-instructing-person-field has-success">';
            $html .= '<label class="control-label" for="other-instructing-person">Instruction Person Name <span style="color:red">*</span></label>';
            $html .= '<select id="other-instructing-person" class="form-control" name="' . $post['keyword'] . '[instruction_person]" aria-invalid="false">';
            $html .= '<option value="">Select</option>';

            // $contact_ids = array();
            // $selected_check = 0;
            // foreach ($results as $key => $value) {
            //     $contact_ids[] = $value->id;
            // }
            // if (in_array($post['other_instructing_person_id'], $contact_ids)) {
            //     $selected_check = 1;
            // }

            foreach ($results as $key => $value) {
                // echo "<pre>"; print_r($post['other_instructing_person_id']); echo "</pre>"; die();
                $profileData = \app\models\UserProfileInfo::find()->where(['user_id'=>$value->id])->one();
                $fullName = $value->firstname . ' ' . $value->lastname . ' ('.$value->email.')';
                // $id = (int)$value->id . '<br>';
                // $other_instructing_person_id = (int)$post['other_instructing_person_id'] . '<br>';
                // if ($selected_check == 1) {

                //     $selected = '';
                //     if ($id == $other_instructing_person_id) {
                //         $selected = 'selected';
                //     }
                // } else {


                //     if ($value->profileInfo->primary_contact == 1) {
                //         $selected = 'selected';
                //     }
                // }
                $selected = 'selected';
                $html .= '<option data_phone="'.$profileData->mobile.'" data_email="'.$value->email.'" value="' . $value->id . '" ' . $selected . '>' . $fullName . '</option>';

                // $html .= '<input type="text" id="" class="form-control" name="' . $post['keyword'] . '[instruction_person]" value="'  . $fullName . '" readonly >';
            }

            $html .= '</select>';
            
            $html .= '<input type="hidden" id="" class="form-control" name="' . $post['keyword'] . '[instructing_party_company]" value="'  . $results[0]->firstname .' ' . $results[0]->lastname . '" readonly >';
            $html .= '<input type="hidden" id="" class="form-control" name="' . $post['keyword'] . '[instructing_party_email]" value="'  . $results[0]->email . '" readonly >';
            $html .= '<div class="help-block"></div>';


          //  $html .= '</div>';
            $html .= '</div>';


            return $html;
        } else {
            return '';
        }
    }


    public function actionOtherInstructingPersonsCrmEnabled()
    {
        $post = Yii::$app->request->post();
        
        $savedInstructionPersonId = isset($post['instruction_person_id']) ? $post['instruction_person_id'] : null;

        if ($post['client_name'] !== null) {
            $results = \app\models\User::find()
                ->where(['company_id' => $post['client_name']])
                ->andWhere(['status' => 1])
                // ->andWhere(['verified' => 1])
                ->innerJoin(\app\models\UserProfileInfo::tableName(), \app\models\UserProfileInfo::tableName() . ".user_id=" . \app\models\User::tableName() . ".id")
                ->all();

            $html = '';
            $html .= '<div class="form-group other-instructing-person-field has-success">';
            $html .= '<label class="control-label" for="other-instructing-person">Instruction Person Name <span style="color:red">*</span></label>';
            $html .= '<select id="other-instructing-person" class="form-control" name="' . $post['keyword'] . '[instruction_person]" aria-invalid="false" onchange="updateHiddenFields()">';
            $html .= '<option value="">Select</option>';

            $defaultSelected = false;

            foreach ($results as $key => $value) {
                $profileData = \app\models\UserProfileInfo::find()->where(['user_id' => $value->id])->one();
                $fullName = $value->firstname . ' ' . $value->lastname . ' (' . $value->email . ')';
                $selected = '';

                // Check if this user is the primary contact
                if (($profileData->primary_contact == 1 && !$defaultSelected && !$savedInstructionPersonId) || ($savedInstructionPersonId && $value->id == $savedInstructionPersonId)) {
                    $selected = 'selected';
                    $defaultSelected = true; // Ensure only one is selected
                }

                $html .= '<option data-phone="' . $profileData->mobile . '" data-email="' . $value->email . '" data-fullname="' . $value->firstname . ' ' . $value->lastname . '" value="' . $value->id . '" ' . $selected . '>' . $fullName . '</option>';
            }

            $html .= '</select>';

            // Hidden input fields to store selected person's details
            $html .= '<input type="hidden" id="instructing-party-company" class="form-control" name="' . $post['keyword'] . '[instructing_party_company]" readonly>';
            $html .= '<input type="hidden" id="instructing-party-email" class="form-control" name="' . $post['keyword'] . '[instructing_party_email]" readonly>';
            
            $html .= '<div class="help-block"></div>';
            $html .= '</div>';

            // JavaScript to update hidden fields
            $html .= '<script>
                        function updateHiddenFields() { 
                            var selectedOption = document.getElementById("other-instructing-person").selectedOptions[0];
                            document.getElementById("instructing-party-company").value = selectedOption.getAttribute("data-fullname");
                            document.getElementById("instructing-party-email").value = selectedOption.getAttribute("data-email");
                        }
                        
                        //window.onload = function() {
                            // Update hidden fields based on the currently selected option on page load
                            updateHiddenFields();
                        //}
                    </script>';

            return $html;
        } else {
            return '';
        }
    }



    public function actionClientinvoice()
    {
        $post = Yii::$app->request->post();
        // print_r($post['keyword']); die();

        if ($post['client_id'] <> null) {
            $model = $this->findModel($post['client_id']);


            return $model->client_invoice_customer;
        } else {
            return '';
        }
    }


    public function actionImport()
    {
        $model = $this->newModel();

        if ($model->validate()) {
            if (UploadedFile::getInstance($model, 'importfile')) {
                $importedFile = UploadedFile::getInstance($model, 'importfile');
                if (!empty($importedFile)) {
                    $pInfo = pathinfo($importedFile->name);
                    $ext = $pInfo['extension'];
                    if (in_array($ext, $model->allowedFileTypes)) {
                        $content = file_get_contents($importedFile->tempName);
                        if (preg_match('/\<\?php/i', $content)) {
                        } else {
                            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                            $charactersLength = strlen($characters);
                            $randomString = '';
                            for ($i = 0; $i < 10; $i++) {
                                $randomString .= $characters[rand(0, $charactersLength - 1)];
                            }
                            $model->importfile = Yii::$app->params['contacts_abs_path'] . $importedFile->name;
                            // echo $model->importfile; die;
                            $importedFile->saveAs($model->importfile);
                            return $this->redirect(['upload', 'file_path' => $model->importfile]);
                        }
                    }
                }
            }
        }

        return $this->render('import', [
            'model' => $model,
        ]);
    }

    public function actionUpload($file_path, $s = 0)
    {
        $csvFile = new \SplFileObject($file_path, 'r');
        $csvFile->seek($s);
        $n = 0;
        while (!$csvFile->eof()) {
            $line = $csvFile->fgetcsv();
            $company = (isset($line[0]) ? trim($line[0]) : '');
            $country = (isset($line[1]) ? trim($line[1]) : '');
            $address = (isset($line[2]) ? trim($line[2]) : '');
            $firstname = (isset($line[3]) ? trim($line[3]) : '');
            $lastname = (isset($line[4]) ? trim($line[4]) : '');
            $mobile_number = (isset($line[5]) ? trim($line[5]) : '');
            $phone_number = (isset($line[6]) ? trim($line[6]) : '');
            $job_title = (isset($line[7]) ? trim($line[7]) : '');
            $email = (isset($line[8]) ? trim($line[8]) : '');
            $statusWord = (isset($line[9]) ? trim($line[9]) : '');

            if ($s >= 1) {
                if ($line[0] <> null) {
                    // echo "Line:- <pre>"; print_r($line); echo "</pre>End Line<br><br>"; //die();

                    $address = utf8_encode($address);
                    $company = utf8_encode($company);
                    $job_title = utf8_encode($job_title);

                    if ($firstname == null AND $firstname == '') {
                        $firstnameExplode = explode('@', $email);
                        if ($firstnameExplode <> null) $firstname = $firstnameExplode[0];
                    }

                    if ($mobile_number <> null) {
                        if (strpos($mobile_number, '/') !== false) {
                            $explode_mobile = explode('/', $mobile_number);
                            $mobile_number = $explode_mobile[0];
                        }
                        $mobile_number = str_replace(" ", "", $mobile_number);
                        $mobile_number = str_replace(".", "", $mobile_number);
                        $mobile_number = preg_replace('/[^0-9,.+]/', '', $mobile_number);
                    }


                    $phone_1 = '';
                    $phone_2 = '';

                    if ($phone_number <> null) {
                        if (strpos($phone_number, '/') !== false) {
                            $explode_phone = explode('/', $phone_number);
                            $phone_1 = $explode_phone[0];
                            $phone_2 = $explode_phone[1];
                        } else {
                            $phone_1 = $phone_number;
                        }
                        $phone_1 = str_replace(" ", "", $phone_1);
                        $phone_1 = str_replace(".", "", $phone_1);
                        $phone_1 = preg_replace('/[^0-9,.+]/', '', $phone_1);

                        $phone_2 = str_replace(" ", "", $phone_2);
                        $phone_2 = str_replace(".", "", $phone_2);
                        $phone_2 = preg_replace('/[^0-9,.+]/', '', $phone_2);
                    }

                    if ($mobile_number == '' AND $mobile_number == null) {
                        if ($phone_1 <> null) {
                            $mobile_number = $phone_1;
                        } elseif ($phone_2 <> null) {
                            $mobile_number = $phone_2;
                        }
                    }

                    $status = 2;
                    if ($statusWord == 'Active') {
                        $status = 1;
                    }

                    $country_id = \app\models\Country::find()->select(['id'])->where(['title' => trim($country)])->one();

                    if ($job_title <> null) {
                        $job_title_id = \app\models\JobTitle::find()->select(['id'])->where(['title' => trim($job_title)])->one();
                        if ($job_title_id == null) {
                            $job_title_id = new \app\models\JobTitle;
                            $job_title_id->title = $job_title;
                            $job_title_id->status = 1;
                            if (!$job_title_id->save()) {
                                if ($job_title_id->hasErrors()) {
                                    foreach ($job_title_id->getErrors() as $error) {
                                        if (count($error) > 0) {
                                            foreach ($error as $key => $val) {
                                                Yii::$app->getSession()->addFlash('error', $val);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $data = [
                        'company' => trim($company),
                        'address' => trim($address),
                        'country' => trim($country),
                        'firstname' => trim($firstname),
                        'lastname' => trim($lastname),
                        'mobile_number' => trim($mobile_number),
                        'phone_1' => trim($phone_1),
                        'phone_2' => trim($phone_2),
                        'email' => trim($email),
                        'job_title_id' => trim($job_title_id->id),
                        'status' => trim($status),
                        'country_id' => trim($country_id->id),
                    ];


                    if ($email <> null) {
                        $this->saveCompany($data);
                        // die('bla bla bla');
                    }
                }
            }

            if ($n > 5) {
                echo 'Still Importing...<script>window.location.href="' . Url::to(['upload', 'file_path' => $file_path, 's' => $s]) . '"</script>';
                die();
                exit();
                break;
            }
            $s++;
            $n++;
        }
        $csvFile = null;
        if ($csvFile == null) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Contacts imported successfully'));
            return $this->redirect(['import']);
        }
    }


    public function saveCompany($data = '')
    {
        // echo "saveCompany:- <pre>"; print_r($data); echo "</pre> End saveCompany<br><br>"; //die();
        $model = \app\models\Company::find()
            ->select(['title', 'country_id', 'address', 'status', 'data_type', 'id'])
            ->where(['title' => $data['company'], 'address' => $data['address'], 'country_id' => $data['country_id'], 'data_type' => 1])
            ->one();
        // echo "Model:- <pre>"; print_r($model); echo "</pre>End Model<br><br>"; //die();

        if ($model == null) {
            // die('null');
            $model = $this->newModel();
        }
        // die('not null');
        $model->title = $data['company'];
        $model->country_id = ($data['country_id'] <> null) ? $data['country_id'] : '';
        $model->address = $data['address'];
        $model->status = $data['status'];
        $model->data_type = 1;
        if ($model->save()) {
            $data['company_id'] = $model->id;
            $this->saveContact($data);
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            // echo $data['company']."- ".$val."<br>";
                            Yii::$app->getSession()->addFlash('error', $company . "- " . $val);
                        }
                    }
                }
                // die();
            }
        }
    }

    public function saveContact($data = '')
    {
        // echo "saveContact:- <pre>"; print_r($data); echo "</pre> End saveContact<br><br>"; //die();
        if ($data['firstname'] <> null AND $data['email'] <> null) {
            $contact = User::find()
                // ->select([
                //     User::tableName().'.user_type',
                //     User::tableName().'.status',
                //     User::tableName().'.company_id',
                //     User::tableName().'.firstname',
                //     User::tableName().'.lastname',
                //     User::tableName().'.email',
                //     UserProfileInfo::tableName().'.job_title_id',
                //     UserProfileInfo::tableName().'.mobile',
                //     UserProfileInfo::tableName().'.phone1',
                //     UserProfileInfo::tableName().'.phone2',
                //     UserProfileInfo::tableName().'.primary_contact',
                //   ])
                ->where(['company_id' => $data['company_id'], 'user_type' => 0])
                ->innerJoin(UserProfileInfo::tableName(), UserProfileInfo::tableName() . ".user_id=" . User::tableName() . ".id")
                ->one();
            // echo "contact:- <pre>"; print_r($contact); echo "</pre> End contact<br><br>"; //die();

            if ($contact == null) {
                // die('null');
                $contact = new \app\models\User;
            }
            // die('not null');
            $contact->user_type = 0;
            $contact->status = 1;
            $contact->primary_contact = 0;
            $contact->company_id = $data['company_id'];
            $contact->firstname = $data['firstname'];
            $contact->lastname = ($data['lastname'] != null AND $data['lastname'] != '') ? $data['lastname'] : '-';
            $contact->job_title_id = $data['job_title_id'];
            $contact->mobile = $data['mobile_number'];
            $contact->phone1 = $data['phone_1'];
            $contact->phone2 = $data['phone_2'];
            $contact->email = $data['email'];
            if ($contact->save()) {
            } else {
                if ($contact->hasErrors()) {
                    foreach ($contact->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                // echo $data['company']."- ".$val."<br>";
                                Yii::$app->getSession()->addFlash('error', $company . "- " . $val);
                            }
                        }
                    }
                    // die();
                }
            }
        }
    }

    public function actionExportValuations($id = null)
    {
        $id = 183;
        $model = Company::findOne($id);
        $valuations = Valuation::find()
            ->select([
                'plot_unit_number' => 'CONCAT(' . Valuation::tableName() . '.plot_number," - ",' . Valuation::tableName() . '.unit_number)',
                Valuation::tableName() . '.id',
                Valuation::tableName() . '.reference_number',
                Valuation::tableName() . '.target_date',
                Valuation::tableName() . '.client_name_passport',
                Valuation::tableName() . '.client_reference',
                Valuation::tableName() . '.valuation_status',
                Valuation::tableName() . '.parent_id',
                Valuation::tableName() . '.revised_reason',
                Valuation::tableName() . '.inspection_type',
                Valuation::tableName() . '.created_at',
                Buildings::tableName() . '.title as Building',
                Company::tableName() . '.title as client',
                Properties::tableName() . '.title as property',
                ValuationPurposes::tableName() . '.title as purpose_of_valuation',
                Zone::tableName() . '.title as city_emirates',
                Communities::tableName() . '.title as sector_community',
                SubCommunities::tableName() . '.title as area_development',
            ])
            ->leftJoin(Company::tableName(), Company::tableName() . '.id=' . Valuation::tableName() . '.client_id')
            ->leftJoin(Buildings::tableName(), Buildings::tableName() . '.id=' . Valuation::tableName() . '.building_info')
            ->leftJoin(Properties::tableName(), Properties::tableName() . '.id=' . Valuation::tableName() . '.property_id')
            ->leftJoin(ValuationPurposes::tableName(), ValuationPurposes::tableName() . '.id=' . Valuation::tableName() . '.purpose_of_valuation')
            ->leftJoin(Zone::tableName(), Zone::tableName() . '.id=' . Buildings::tableName() . '.city')
            ->leftJoin(Communities::tableName(), Communities::tableName() . '.id=' . Buildings::tableName() . '.community')
            ->leftJoin(SubCommunities::tableName(), SubCommunities::tableName() . '.id=' . Buildings::tableName() . '.sub_community')
            ->where([Valuation::tableName() . '.client_id' => $id])
            ->asArray();
        // ->all();
        // echo "<pre>"; print_r(count($valuations)); echo "</pre>"; echo "<br>";;
        // echo "<pre>"; print_r($valuations); echo "</pre>"; die;

        $exporter = new CsvGrid([
            'dataProvider' => new ActiveDataProvider([
                'query' => $valuations,
            ]),

            'columns' => [
                [
                    'attribute' => 'reference_number',
                    'label' => 'Windmills Ref. No.',
                ],
                [
                    'attribute' => 'created_at',
                    'label' => 'Date Received Request',
                    'format' => ['date', 'php:d-F-Y '],
                ],
                [
                    'attribute' => 'created_at',
                    'label' => 'Time Received of the Request',
                    'format' => 'time',
                ],
                [
                    'attribute' => '',
                    'label' => 'No of Working Days (after inspection an documents received)',
                    'value' => function ($model) {
                        $approver_date = Yii::$app->appHelperFunctions->getApproverDate($model);
                        if ($approver_date <> null) {
                            $date1 = date_create(date('Y-m-d', strtotime($model['target_date'])));
                            $date2 = date_create(date('Y-m-d', strtotime($approver_date)));
                            $diff = date_diff($date1, $date2);
                            if ($diff <> null) {
                                return $diff->d;
                            }
                        }
                    },
                ],
                [
                    'attribute' => 'plot_unit_number',
                    'label' => 'Plot No. / Unit No.',
                ],
                [
                    'attribute' => 'sector_community',
                    'label' => 'Sector / Community',
                ],
                [
                    'attribute' => 'area_development',
                    'label' => 'Area / Development',
                ],
                [
                    'attribute' => 'city_emirates',
                    'label' => 'City / Emirate',
                ],
                [
                    'attribute' => 'client_name_passport',
                    'label' => 'Customer Name',
                ],
                [
                    'attribute' => 'client_reference',
                    'label' => 'ADIB Finnone',
                ],
                [
                    'attribute' => 'valuation_status',
                    'label' => 'Report Status',
                    'value' => function ($model) {
                        if ($model['valuation_status'] == 5) {
                            return 'Delivered';
                        } else {
                            return "In Progress";
                        }
                    },
                ],
                [
                    'attribute' => '',
                    'label' => 'Inspection Status (Inspected / Not Inspected )',
                    'value' => function ($model) {
                        if ($model['valuation_status'] = 0 || $model['valuation_status'] == 1 || $model['valuation_status'] == 2) {
                            return 'Not Inspected';
                        } else {
                            return "Inspected";
                        }
                    },
                ],
                [
                    'attribute' => '',
                    'label' => 'Remarks',
                    'value' => function ($model) {
                        if ($model['parent_id'] <> null) {
                            return Yii::$app->appHelperFunctions->revisedReasons[$model['revised_reason']];
                        }
                    },
                ],
                [
                    'attribute' => '',
                    'label' => 'Report Inspection Type (Full Eva , Desk Top )',
                    'value' => function ($model) {
                        if ($model['inspection_type'] <> null) {
                            return Yii::$app->appHelperFunctions->inspectionTypeArr[$model['inspection_type']];
                        }
                    },
                ],
                [
                    'attribute' => '',
                    'label' => 'Assignment Type',
                    'value' => function ($model) {
                        if ($model['inspection_type'] <> null) {
                            return Yii::$app->appHelperFunctions->inspectionTypeArr[$model['inspection_type']];
                        }
                    },
                ],
                [
                    'attribute' => '',
                    'label' => 'Priority (Normal /Urgent )',
                    'value' => function ($model) {
                        $approver_date = Yii::$app->appHelperFunctions->getApproverDate($model);
                        if ($approver_date <> null) {
                            if (date('Y-m-d', strtotime($model['target_date'])) == date('Y-m-d', strtotime($approver_date)) AND $model['valuation_status'] == 5) {
                                return "Urgent";
                            } else {
                                return "Normal";
                            }
                        }
                    },
                ],
                [
                    'attribute' => '',
                    'label' => 'Report Delivery Date',
                    'format' => ['date', 'php:d-F-Y '],
                    'value' => function ($model) {
                        $approver_date = Yii::$app->appHelperFunctions->getApproverDate($model);
                        if ($approver_date <> null) {
                            return $approver_date;
                        }
                    },
                ],
            ],

        ]);

        $file_nameCsv = $model->title . '-valuations.csv';
        $file_nameXls = $model->title . '-valuations.xls';
        $file_nameXlxs = $model->title . '-valuations.xlsx';
        $exporter->export()->saveAs('uploads/client_valuation_csv/' . $file_nameCsv);
        $file_pathCsv = Yii::$app->params['client_valuations'] . $file_nameCsv;

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();


        /* Set CSV parsing options */
        $reader->setDelimiter(',');
        $reader->setEnclosure('"');
        $reader->setSheetIndex(0);
        /* Load a CSV file and save as a XLS */
        $spreadsheet = $reader->load($file_pathCsv);

        // $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xls($spreadsheet);
        // $writer->save('uploads/client_valuation_csv/'.$file_nameXls);
        // $file_pathXls = Yii::$app->params['client_valuations'].$file_nameXls;

        $writer2 = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);


        $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
        $spreadsheet->getActiveSheet()->setTitle("Daily Status");

        $rightAlignColumnsArr = ['B', 'C', 'D', 'J', 'Q'];
        $leftAlignColumnsArr = ['A', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'O', 'P'];

        foreach ($rightAlignColumnsArr as $key => $columnId) {
            $spreadsheet->getActiveSheet()->getStyle($columnId)->getAlignment()->setHorizontal('right');
        }
        foreach ($leftAlignColumnsArr as $key => $columnId) {
            $spreadsheet->getActiveSheet()->getStyle($columnId)->getAlignment()->setHorizontal('left');
        }


        $styleArray = [
            'font' => [
                'bold' => true,
                'size' => 10
            ],

            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'DDE7F3',
                ],
                'endColor' => [
                    'argb' => 'DDE7F3',
                ],
            ],
        ];

        $spreadsheet->getActiveSheet()->getStyle('A1:Q1')->applyFromArray($styleArray)->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->freezePane('D2');

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(14);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(14);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(14);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(14);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(14);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(50);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(15);

        $spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(64);

        $styleArray2 = [
            'borders' => [
                'right' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
                'left' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];

        $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('H1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('I1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('J1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('K1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('L1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('M1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('N1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('O1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('P1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('Q1')->applyFromArray($styleArray2);


        $spreadsheet->getActiveSheet()->setAutoFilter('A1:Q1');


        $styleArray3 = [
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFFF00',
                ],
                'endColor' => [
                    'argb' => 'FFFF00',
                ],
            ],
        ];

        $spreadsheet->getActiveSheet()->getStyle('K1:M1')->applyFromArray($styleArray3)->getAlignment()->setWrapText(true);

        $writer2->save('uploads/client_valuation_csv/' . $file_nameXlxs);
        $file_pathXlsx = Yii::$app->params['client_valuations'] . $file_nameXlxs;

        if (file_exists($file_pathXlsx)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($file_pathXlsx) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file_pathXlsx));
            readfile($file_pathXlsx);
            // exit;
        }
        // unlink($file_pathCsv);
        // unlink($file_pathXls);
        //  unlink($file_pathXlsx);

        $spreadsheet->disconnectWorksheets();
        // return $exporter->export()->send($model->title.'-valuations.csv');
        die("export successfully");
    }

    public function actionExportAjman($id = null)
    {

        $file_pathCsv = Yii::$app->params['client_valuations'] . 'ajman-valuationst8.xlsx';
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $detail= \app\models\ValuationDetailData::find()->where(['valuation_id' => $id])->one();

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

        $model = Valuation::findOne($id);
        $configuration = \app\models\ConfigurationFiles::find()->select(['rep_image'])->where(['valuation_id' => $model->id, "checked_image"=>'on'])->asArray()->all();


        if (in_array($model->property_id, [1,2,6])){
            $configuration_bathroom = \app\models\ConfigurationFiles::find()->select(['rep_image'])->where(['valuation_id' => $model->id, "checked_image"=>'on',"type"=>'config_bathrooms'])->asArray()->one();
            $configuration[2]['rep_image'] = $configuration_bathroom['rep_image'];

            $configuration_kitchen = \app\models\ConfigurationFiles::find()->select(['rep_image'])->where(['valuation_id' => $model->id, "checked_image"=>'on',"type"=>'config_kitchen'])->asArray()->one();
            $configuration[3]['rep_image'] = $configuration_kitchen['rep_image'];

            $configuration_living = \app\models\ConfigurationFiles::find()->select(['rep_image'])->where(['valuation_id' => $model->id, "checked_image"=>'on',"type"=>'config_living_area'])->asArray()->one();
            $configuration[4]['rep_image'] = $configuration_living['rep_image'];

            $configuration_general_elevation = \app\models\ConfigurationFiles::find()->select(['rep_image'])->where(['valuation_id' => $model->id, "checked_image"=>'on',"type"=>'config_general_elevation'])->asArray()->one();
            $configuration[5]['rep_image'] = $configuration_general_elevation['rep_image'];
        }else{
            $configuration = \app\models\ConfigurationFiles::find()->select(['rep_image'])->where(['valuation_id' => $model->id, "checked_image"=>'on'])->andWhere(['not', ['type' => 'config_general_elevation']])->asArray()->all();
            $configuration_general_elevation = \app\models\ConfigurationFiles::find()->select(['rep_image'])->where(['valuation_id' => $model->id, "checked_image"=>'on',"type"=>'config_general_elevation'])->asArray()->one();
            $configuration[5]['rep_image'] = $configuration_general_elevation['rep_image'];
        }
        /*  echo "<pre>";
          print_r($configuration);
          die;*/

        /* Load a CSV file and save as a XLS */
        $spreadsheet = $reader->load($file_pathCsv);
        $sheet = $spreadsheet->getActiveSheet();
        $mage_height = 215;
        $mage_width = 160;
        $offset_x = 25;
        $offset_y = 10;
        $owners= \app\models\ValuationOwners::find()->where(['valuation_id'=>$model->id])->all();
        $income_val= \app\models\IncomeApproach::find()->where(['valuation_id'=>$model->id])->one();
        $costDetails = CostDetails::find()->where(['valuation_id' => $id])->one();
        $approver_data = \app\models\ValuationApproversData::find()->where(['valuation_id' => $model->id,'approver_type' => 'approver'])->one();
        if ($costDetails != null) {
            $land_price = $costDetails->lands_price.' per Sq ft';
        }else{
            $land_price='';
        }
        $woners_name = "";
        foreach ($owners as $record){
            $woners_name .= $record->name." ".$record->lastname.", ";
        }
        $sheet->setCellValue('E3', $model->reference_number);
        $sheet->setCellValue('I3', $model->property->title);
        $sheet->setCellValue('G4', Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category]);
        $sheet->setCellValue('E4', $model->building->communities->title.'/'.$model->building->subCommunities->title);
        $sheet->setCellValue('C4', $model->title_deed);
        $submission_approval_date= date('d/m/Y',strtotime($detail->valuation_date));
        $sheet->setCellValue('B3', $submission_approval_date);

        $sheet->setCellValue('B5', ucfirst($model->client->client_type));
        $sheet->setCellValue('B6', $woners_name);
        $sheet->setCellValue('B7', number_format($income_val->gross_source));
        $sheet->setCellValue('B8', $model->property->title);
        $lon=  $model->inspectProperty->longitude;
        $lan = $model->inspectProperty->latitude;
        $fence = $model->inspectProperty->length_of_fence;
        $map = 'http://maps.google.com/maps?q='.$lan.','.$lon;
        //   $map_html = '<a href="'.$map.'" target="_blank"> Map test</a>';

        $sheet->setCellValue('D25', $fence);
        $sheet->setCellValue('B16', $map);
        $sheet->setCellValue('B17', Yii::$app->appHelperFunctions->valuationApproachListArr[$model->property->valuation_approach]);
        $sheet->setCellValue('B18', $model->land_size);
        $sheet->setCellValue('F19', $land_price);
        $sheet->setCellValue('H21', Yii::$app->appHelperFunctions->wmFormate($model->inspectProperty->built_up_area));
        $sheet->setCellValue('I21', Yii::$app->appHelperFunctions->wmFormate($approver_data->estimated_market_value));
        $sheet->setCellValue('D21',$model->property->title );
        $sheet->setCellValue('I26', Yii::$app->appHelperFunctions->wmFormate($approver_data->estimated_market_value));
        $sheet->setCellValue('F26', Yii::$app->appHelperFunctions->wmFormate($approver_data->estimated_market_value));

        if($model->inspectProperty->date_of_completion_certificate <> null) {
            $sheet->setCellValue('F21', date('d/m/Y', strtotime($model->inspectProperty->date_of_completion_certificate)));
           $year = $model->inspectProperty->estimated_age;
            $sheet->setCellValue('G21',$year.' Years' );


        }else{
            $sheet->setCellValue('F21','Not Available' );
            $sheet->setCellValue('G21',0 );
        }
        if($model->inspectProperty->date_of_building_permit <> null) {
            $sheet->setCellValue('E21', date('d/m/Y', strtotime($model->inspectProperty->date_of_building_permit)));
        }else{
            $sheet->setCellValue('E21','Not Available' );
        }

        if($model->inspection_type == 3){
            $sheet->setCellValue('D15','Not Applicable' );
        }else {


            if (isset($configuration[0]['rep_image']) && $configuration[0]['rep_image'] <> null) {
                $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $drawing->setName('pic_7');
                $drawing->setDescription('pic_7');
                $drawing->setPath($configuration[0]['rep_image']); // put your path and image here
                $drawing->setOffsetX($offset_x);
                $drawing->setOffsetY($offset_y);
                $drawing->getShadow()->setVisible(true);
                $drawing->setHeight($mage_height);
                $drawing->setWidth($mage_width);
                $drawing->getShadow()->setVisible(true);
                $drawing->getShadow()->setDirection(45);
                $drawing->setCoordinates('B15');
                $drawing->setWorksheet($spreadsheet->getActiveSheet());
            }

            if (isset($configuration[1]['rep_image']) && $configuration[1]['rep_image'] <> null) {
                $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $drawing->setName('pic_8');
                $drawing->setDescription('pic_8');
                $drawing->setPath($configuration[1]['rep_image']); // put your path and image here
                $drawing->setOffsetX($offset_x);
                $drawing->setOffsetY($offset_y);
                $drawing->getShadow()->setVisible(true);
                $drawing->setHeight($mage_height);
                $drawing->setWidth($mage_width);
                $drawing->getShadow()->setVisible(true);
                $drawing->getShadow()->setDirection(45);
                $drawing->setCoordinates('C15');
                $drawing->setWorksheet($spreadsheet->getActiveSheet());
            }

            if (isset($configuration[2]['rep_image']) && $configuration[2]['rep_image'] <> null) {
                $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $drawing->setName('pic_3');
                $drawing->setDescription('pic_3');
                $drawing->setPath($configuration[2]['rep_image']); // put your path and image here
                $drawing->setOffsetX($offset_x);
                $drawing->setOffsetY($offset_y);
                $drawing->getShadow()->setVisible(true);
                $drawing->setHeight($mage_height);
                $drawing->setWidth($mage_width);
                $drawing->getShadow()->setVisible(true);
                $drawing->getShadow()->setDirection(45);
                $drawing->setCoordinates('D15');
                $drawing->setWorksheet($spreadsheet->getActiveSheet());
            }

            if (isset($configuration[3]['rep_image']) && $configuration[3]['rep_image'] <> null) {
                $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $drawing->setName('pic_4');
                $drawing->setDescription('pic_4');
                $drawing->setPath($configuration[3]['rep_image']); // put your path and image here
                $drawing->setOffsetX($offset_x);
                $drawing->setOffsetY($offset_y);
                $drawing->getShadow()->setVisible(true);
                $drawing->setHeight($mage_height);
                $drawing->setWidth($mage_width);
                $drawing->getShadow()->setVisible(true);
                $drawing->getShadow()->setDirection(45);
                $drawing->setCoordinates('E15');
                $drawing->setWorksheet($spreadsheet->getActiveSheet());


            }

            if (isset($configuration[4]['rep_image']) && $configuration[4]['rep_image'] <> null) {
                $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $drawing->setName('pic_5');
                $drawing->setDescription('pic_5');
                $drawing->setPath($configuration[4]['rep_image']); // put your path and image here
                $drawing->setOffsetX($offset_x);
                $drawing->setOffsetY($offset_y);
                $drawing->getShadow()->setVisible(true);
                $drawing->setHeight($mage_height);
                $drawing->setWidth($mage_width);
                $drawing->getShadow()->setVisible(true);
                $drawing->getShadow()->setDirection(45);
                $drawing->setCoordinates('F15');
                $drawing->setWorksheet($spreadsheet->getActiveSheet());
            }

            if (isset($configuration[5]['rep_image']) && $configuration[5]['rep_image'] <> null) {
                $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $drawing->setName('pic_6');
                $drawing->setDescription('pic_6');
                $drawing->setPath($configuration[5]['rep_image']); // put your path and image here
                $drawing->setOffsetX($offset_x);
                $drawing->setOffsetY($offset_y);
                $drawing->getShadow()->setVisible(true);
                $drawing->setHeight($mage_height);
                $drawing->setWidth($mage_width);
                $drawing->getShadow()->setVisible(true);
                $drawing->getShadow()->setDirection(45);
                $drawing->setCoordinates('G15');
                $drawing->setWorksheet($spreadsheet->getActiveSheet());
            }
        }


        $writer2 = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $file_nameXlxs = 'ajman-valuationst-'.$id.'.xlsx';
        if (file_exists(Yii::$app->params['client_valuations']. $file_nameXlxs)) {
            unlink(Yii::$app->params['client_valuations']. $file_nameXlxs);
        }


        $writer2->save('uploads/client_valuation_csv/' . $file_nameXlxs);


        $file_pathXlsx = Yii::$app->params['client_valuations'] . $file_nameXlxs;
        // return $file_pathXlsx;
        return $file_pathXlsx;

      /*  if (file_exists($file_pathXlsx)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($file_pathXlsx) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file_pathXlsx));
            readfile($file_pathXlsx);
            // exit;
        }*/
        // unlink($file_pathCsv);
        // unlink($file_pathXls);
        //  unlink($file_pathXlsx);


    }

    public function actionExportAjmanDownload($id = null)
    {

        $file_pathCsv = Yii::$app->params['client_valuations'] . 'ajman-valuationst8.xlsx';
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $detail= \app\models\ValuationDetailData::find()->where(['valuation_id' => $id])->one();

        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

        $model = Valuation::findOne($id);
        $configuration = \app\models\ConfigurationFiles::find()->select(['rep_image'])->where(['valuation_id' => $model->id, "checked_image"=>'on'])->asArray()->all();



        if (in_array($model->property_id, [1,2,6])){
            $configuration_bathroom = \app\models\ConfigurationFiles::find()->select(['rep_image'])->where(['valuation_id' => $model->id, "checked_image"=>'on',"type"=>'config_bathrooms'])->asArray()->one();
            $configuration[2]['rep_image'] = $configuration_bathroom['rep_image'];

            $configuration_kitchen = \app\models\ConfigurationFiles::find()->select(['rep_image'])->where(['valuation_id' => $model->id, "checked_image"=>'on',"type"=>'config_kitchen'])->asArray()->one();
            $configuration[3]['rep_image'] = $configuration_kitchen['rep_image'];

            $configuration_living = \app\models\ConfigurationFiles::find()->select(['rep_image'])->where(['valuation_id' => $model->id, "checked_image"=>'on',"type"=>'config_living_area'])->asArray()->one();
            $configuration[4]['rep_image'] = $configuration_living['rep_image'];

            $configuration_general_elevation = \app\models\ConfigurationFiles::find()->select(['rep_image'])->where(['valuation_id' => $model->id, "checked_image"=>'on',"type"=>'config_general_elevation'])->asArray()->one();
            $configuration[5]['rep_image'] = $configuration_general_elevation['rep_image'];

        }else{
            $configuration = \app\models\ConfigurationFiles::find()->select(['rep_image'])->where(['valuation_id' => $model->id, "checked_image"=>'on'])->andWhere(['not', ['type' => 'config_general_elevation']])->asArray()->all();
            $configuration_general_elevation = \app\models\ConfigurationFiles::find()->select(['rep_image'])->where(['valuation_id' => $model->id, "checked_image"=>'on',"type"=>'config_general_elevation'])->asArray()->one();
            $configuration[5]['rep_image'] = $configuration_general_elevation['rep_image'];
        }

        /*  echo "<pre>";
          print_r($configuration);
          die;*/

        /* Load a CSV file and save as a XLS */
        $spreadsheet = $reader->load($file_pathCsv);
        $sheet = $spreadsheet->getActiveSheet();
        $mage_height = 215;
        $mage_width = 160;
        $offset_x = 25;
        $offset_y = 10;
        $owners= \app\models\ValuationOwners::find()->where(['valuation_id'=>$model->id])->all();
        $income_val= \app\models\IncomeApproach::find()->where(['valuation_id'=>$model->id])->one();
        $costDetails = CostDetails::find()->where(['valuation_id' => $id])->one();



        $approver_data = \app\models\ValuationApproversData::find()->where(['valuation_id' => $model->id,'approver_type' => 'approver'])->one();
        if ($costDetails != null) {
            $land_price = $costDetails->lands_price.' per Sq ft';
        }else{
            $land_price='';
        }

        $woners_name = "";
        foreach ($owners as $record){
            $woners_name .= $record->name." ".$record->lastname.", ";
        }
        $sheet->setCellValue('E3', $model->reference_number);
        $sheet->setCellValue('I3', $model->property->title);
        $sheet->setCellValue('G4', Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category]);
        $sheet->setCellValue('E4', $model->building->communities->title.'/'.$model->building->subCommunities->title);
        $sheet->setCellValue('C4', $model->title_deed);
        $submission_approval_date= date('d/m/Y',strtotime($detail->valuation_date));
        $sheet->setCellValue('B3', $submission_approval_date);

        $sheet->setCellValue('B5', ucfirst($model->client->client_type));
        $sheet->setCellValue('B6', $woners_name);
        $sheet->setCellValue('B7', number_format($income_val->gross_source));
        $sheet->setCellValue('B8', $model->property->title);
        $lon=  $model->inspectProperty->longitude;
        $lan = $model->inspectProperty->latitude;
        $fence = $model->inspectProperty->length_of_fence;
        $map = 'http://maps.google.com/maps?q='.$lan.','.$lon;
        //   $map_html = '<a href="'.$map.'" target="_blank"> Map test</a>';

        $sheet->setCellValue('D25', $fence);
        $sheet->setCellValue('B16', $map);
        $sheet->setCellValue('B17', Yii::$app->appHelperFunctions->valuationApproachListArr[$model->property->valuation_approach]);
        $sheet->setCellValue('B18', $model->land_size);
        $sheet->setCellValue('F19', $land_price);
        $sheet->setCellValue('H21', Yii::$app->appHelperFunctions->wmFormate($model->inspectProperty->built_up_area));
        $sheet->setCellValue('I21', Yii::$app->appHelperFunctions->wmFormate($approver_data->estimated_market_value));
        $sheet->setCellValue('D21',$model->property->title );
        $sheet->setCellValue('I26', Yii::$app->appHelperFunctions->wmFormate($approver_data->estimated_market_value));
        $sheet->setCellValue('F26', Yii::$app->appHelperFunctions->wmFormate($approver_data->estimated_market_value));

        if($model->inspectProperty->date_of_completion_certificate <> null) {
            $sheet->setCellValue('F21', date('d/m/Y', strtotime($model->inspectProperty->date_of_completion_certificate)));
            $year = $model->inspectProperty->estimated_age;
            $sheet->setCellValue('G21',$year.' Years' );
          //  $sheet->setCellValue('G21',$years.' Years' );


        }else{
            $sheet->setCellValue('F21','Not Available' );
            $sheet->setCellValue('G21',0 );
        }
        if($model->inspectProperty->date_of_building_permit <> null) {
            $sheet->setCellValue('E21', date('d/m/Y', strtotime($model->inspectProperty->date_of_building_permit)));
        }else{
            $sheet->setCellValue('E21','Not Available' );
        }
        if($model->inspection_type == 3){
            $sheet->setCellValue('D15','Not Applicable' );
        }else {

            if (isset($configuration[0]['rep_image']) && $configuration[0]['rep_image'] <> null) {
                $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $drawing->setName('pic_7');
                $drawing->setDescription('pic_7');
                $drawing->setPath($configuration[0]['rep_image']); // put your path and image here
                $drawing->setOffsetX($offset_x);
                $drawing->setOffsetY($offset_y);
                $drawing->getShadow()->setVisible(true);
                $drawing->setHeight($mage_height);
                $drawing->setWidth($mage_width);
                $drawing->getShadow()->setVisible(true);
                $drawing->getShadow()->setDirection(45);
                $drawing->setCoordinates('B15');
                $drawing->setWorksheet($spreadsheet->getActiveSheet());
            }

            if (isset($configuration[1]['rep_image']) && $configuration[1]['rep_image'] <> null) {
                $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $drawing->setName('pic_8');
                $drawing->setDescription('pic_8');
                $drawing->setPath($configuration[1]['rep_image']); // put your path and image here
                $drawing->setOffsetX($offset_x);
                $drawing->setOffsetY($offset_y);
                $drawing->getShadow()->setVisible(true);
                $drawing->setHeight($mage_height);
                $drawing->setWidth($mage_width);
                $drawing->getShadow()->setVisible(true);
                $drawing->getShadow()->setDirection(45);
                $drawing->setCoordinates('C15');
                $drawing->setWorksheet($spreadsheet->getActiveSheet());
            }

            if (isset($configuration[2]['rep_image']) && $configuration[2]['rep_image'] <> null) {
                $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $drawing->setName('pic_3');
                $drawing->setDescription('pic_3');
                $drawing->setPath($configuration[2]['rep_image']); // put your path and image here
                $drawing->setOffsetX($offset_x);
                $drawing->setOffsetY($offset_y);
                $drawing->getShadow()->setVisible(true);
                $drawing->setHeight($mage_height);
                $drawing->setWidth($mage_width);
                $drawing->getShadow()->setVisible(true);
                $drawing->getShadow()->setDirection(45);
                $drawing->setCoordinates('D15');
                $drawing->setWorksheet($spreadsheet->getActiveSheet());
            }

            if (isset($configuration[3]['rep_image']) && $configuration[3]['rep_image'] <> null) {
                $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $drawing->setName('pic_4');
                $drawing->setDescription('pic_4');
                $drawing->setPath($configuration[3]['rep_image']); // put your path and image here
                $drawing->setOffsetX($offset_x);
                $drawing->setOffsetY($offset_y);
                $drawing->getShadow()->setVisible(true);
                $drawing->setHeight($mage_height);
                $drawing->setWidth($mage_width);
                $drawing->getShadow()->setVisible(true);
                $drawing->getShadow()->setDirection(45);
                $drawing->setCoordinates('E15');
                $drawing->setWorksheet($spreadsheet->getActiveSheet());


            }

            if (isset($configuration[4]['rep_image']) && $configuration[4]['rep_image'] <> null) {
                $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $drawing->setName('pic_5');
                $drawing->setDescription('pic_5');
                $drawing->setPath($configuration[4]['rep_image']); // put your path and image here
                $drawing->setOffsetX($offset_x);
                $drawing->setOffsetY($offset_y);
                $drawing->getShadow()->setVisible(true);
                $drawing->setHeight($mage_height);
                $drawing->setWidth($mage_width);
                $drawing->getShadow()->setVisible(true);
                $drawing->getShadow()->setDirection(45);
                $drawing->setCoordinates('F15');
                $drawing->setWorksheet($spreadsheet->getActiveSheet());
            }

            if (isset($configuration[5]['rep_image']) && $configuration[5]['rep_image'] <> null) {
                $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                $drawing->setName('pic_6');
                $drawing->setDescription('pic_6');
                $drawing->setPath($configuration[5]['rep_image']); // put your path and image here
                $drawing->setOffsetX($offset_x);
                $drawing->setOffsetY($offset_y);
                $drawing->getShadow()->setVisible(true);
                $drawing->setHeight($mage_height);
                $drawing->setWidth($mage_width);
                $drawing->getShadow()->setVisible(true);
                $drawing->getShadow()->setDirection(45);
                $drawing->setCoordinates('G15');
                $drawing->setWorksheet($spreadsheet->getActiveSheet());
            }

        }

        $writer2 = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $file_nameXlxs = 'ajman-valuationst-'.$id.'.xlsx';
        if (file_exists(Yii::$app->params['client_valuations']. $file_nameXlxs)) {
            unlink(Yii::$app->params['client_valuations']. $file_nameXlxs);
        }


        $writer2->save('uploads/client_valuation_csv/' . $file_nameXlxs);


        $file_pathXlsx = Yii::$app->params['client_valuations'] . $file_nameXlxs;
       // return $file_pathXlsx;

          if (file_exists($file_pathXlsx)) {
              header('Content-Description: File Transfer');
              header('Content-Type: application/octet-stream');
              header('Content-Disposition: attachment; filename="' . basename($file_pathXlsx) . '"');
              header('Expires: 0');
              header('Cache-Control: must-revalidate');
              header('Pragma: public');
              header('Content-Length: ' . filesize($file_pathXlsx));
              readfile($file_pathXlsx);
              // exit;
          }
        // unlink($file_pathCsv);
        // unlink($file_pathXls);
        //  unlink($file_pathXlsx);


    }

    public function actionDocAjmanDownload($id = null)
    {

        $file_pathCsv = Yii::$app->params['client_valuations'] . 'doc_ajman.xlsx';

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $detail= \app\models\ValuationDetailData::find()->where(['valuation_id' => $id])->one();
        $model_detail = ValuationDetail::find()->where(['valuation_id' => $id])->one();
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

        $model = Valuation::findOne($id);
        if($model_detail->unit_number <> null && $model_detail->unit_number !=0 && is_numeric($model_detail->unit_number)){
             $address = 'Unit Number '.$model_detail->unit_number.', '.$model->building->title.', '.$model->building->subCommunities->title.', '.$model->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city].', UAE.';
        }else{
            $address = 'Plot Number '.$model->plot_number.', '.$model->building->title.', '.$model->building->subCommunities->title.', '.$model->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city].', UAE.';
        }


        /* Load a CSV file and save as a XLS */
        $spreadsheet = $reader->load($file_pathCsv);
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('F7', '1');
        $sheet->setCellValue('F8', trim($model->inspectProperty->longitude));
        $sheet->setCellValue('F9', trim($model->inspectProperty->latitude));
        $sheet->setCellValue('F10', $address);
        $city ='';
        if($model->building->city == 3506){
            $city= 'AUH';
        }else if($model->building->city == 3507){
            $city= 'AJM';
        }else if($model->building->city == 3508){
            $city= 'FJR';
        }else if($model->building->city == 3509){
            $city= 'SHJ';
        }else if($model->building->city == 3510){
            $city= 'DXB';
        }else if($model->building->city == 3511){
            $city= 'RAK';
        }else if($model->building->city == 3512){
            $city= 'UAQ';
        }else if($model->building->city == 4260){
            $city= 'Al Ain';
        }




        $sheet->setCellValue('F11', $city);
        $sheet->setCellValue('F12', Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category]);
        $sheet->setCellValue('F13', $model->inspectProperty->full_building_floors);
        $sheet->setCellValue('F14', $model->floor_number);
        $sheet->setCellValue('F15', 'Not Available');
        $bua = '';
        if($model->inspectProperty->built_up_area > 0){
            $bua= 'BUA:'.number_format($model->inspectProperty->built_up_area * 0.092903	).' Sq mt.';
        }

        $sheet->setCellValue('F16', $bua);
        $sheet->setCellValue('F17', $model->inspectProperty->estimated_age);
        if($model->inspectProperty->completion_status==100){
            $sheet->setCellValue('F18', 'Ready');
        }else{
            $sheet->setCellValue('F18', 'Under Construction');
        }



        $writer2 = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $file_nameXlxs = 'ajman-valuation-'.$model->reference_number.'.xlsx';
        if (file_exists(Yii::$app->params['client_valuations']. $file_nameXlxs)) {
            unlink(Yii::$app->params['client_valuations']. $file_nameXlxs);
        }


        $writer2->save('uploads/client_valuation_csv/' . $file_nameXlxs);


        $file_pathXlsx = Yii::$app->params['client_valuations'] . $file_nameXlxs;
        // return $file_pathXlsx;

        if (file_exists($file_pathXlsx)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($file_pathXlsx) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file_pathXlsx));
            readfile($file_pathXlsx);
            // exit;
        }
        // unlink($file_pathCsv);
        // unlink($file_pathXls);
        //  unlink($file_pathXlsx);


    }
    public function actionDocAjman($id = null)
    {

        $file_pathCsv = Yii::$app->params['client_valuations'] . 'doc_ajman.xlsx';

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $detail= \app\models\ValuationDetailData::find()->where(['valuation_id' => $id])->one();
        $model_detail = ValuationDetail::find()->where(['valuation_id' => $id])->one();
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

        $model = Valuation::findOne($id);
        if($model_detail->unit_number <> null && $model_detail->unit_number !=0 && is_numeric($model_detail->unit_number)){
            $address = 'Unit Number '.$model_detail->unit_number.', '.$model->building->title.', '.$model->building->subCommunities->title.', '.$model->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city].', UAE.';
        }else{
            $address = 'Plot Number '.$model->plot_number.', '.$model->building->title.', '.$model->building->subCommunities->title.', '.$model->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city].', UAE.';
        }


        /* Load a CSV file and save as a XLS */
        $spreadsheet = $reader->load($file_pathCsv);
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('F7', '1');
        $sheet->setCellValue('F8', trim($model->inspectProperty->longitude));
        $sheet->setCellValue('F9', trim($model->inspectProperty->latitude));
        $sheet->setCellValue('F10', $address);
        $city ='';
        if($model->building->city == 3506){
            $city= 'AUH';
        }else if($model->building->city == 3507){
            $city= 'AJM';
        }else if($model->building->city == 3508){
            $city= 'FJR';
        }else if($model->building->city == 3509){
            $city= 'SHJ';
        }else if($model->building->city == 3510){
            $city= 'DXB';
        }else if($model->building->city == 3511){
            $city= 'RAK';
        }else if($model->building->city == 3512){
            $city= 'UAQ';
        }else if($model->building->city == 4260){
            $city= 'Al Ain';
        }




        $sheet->setCellValue('F11', $city);
        $sheet->setCellValue('F12', Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$model->property_category]);
        $sheet->setCellValue('F13', $model->inspectProperty->full_building_floors);
        $sheet->setCellValue('F14', $model->floor_number);
        $sheet->setCellValue('F15', 'Not Available');
        $bua = '';
        if($model->inspectProperty->built_up_area > 0){
            $bua= 'BUA:'.number_format($model->inspectProperty->built_up_area * 0.092903	).' Sq mt.';
        }

        $sheet->setCellValue('F16', $bua);
        $sheet->setCellValue('F17', $model->inspectProperty->estimated_age);
        if($model->inspectProperty->completion_status==100){
            $sheet->setCellValue('F18', 'Ready');
        }else{
            $sheet->setCellValue('F18', 'Under Construction');
        }



        $writer2 = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $file_nameXlxs = 'ajman-valuation-'.$model->reference_number.'.xlsx';
        if (file_exists(Yii::$app->params['client_valuations']. $file_nameXlxs)) {
            unlink(Yii::$app->params['client_valuations']. $file_nameXlxs);
        }


        $writer2->save('uploads/client_valuation_csv/' . $file_nameXlxs);


        $file_pathXlsx = Yii::$app->params['client_valuations'] . $file_nameXlxs;
         return $file_pathXlsx;


    }

    public function actionIndex1()
    {
        $id = 87;
        $model = Company::findOne($id);
        $client = Company::findOne(87);
        $valuations = Valuation::find()
            ->select([
                'plot_unit_number' => 'CONCAT(' . Valuation::tableName() . '.plot_number," - ",' . Valuation::tableName() . '.unit_number)',
                Valuation::tableName() . '.id',
                Valuation::tableName() . '.reference_number',
                Valuation::tableName() . '.target_date',
                Valuation::tableName() . '.client_name_passport',
                Valuation::tableName() . '.client_reference',
                Valuation::tableName() . '.valuation_status',
                Valuation::tableName() . '.parent_id',
                Valuation::tableName() . '.revised_reason',
                Valuation::tableName() . '.inspection_type',
                Valuation::tableName() . '.created_at',
                Buildings::tableName() . '.title as Building',
                Company::tableName() . '.title as client',
                Properties::tableName() . '.title as property',
                ValuationPurposes::tableName() . '.title as purpose_of_valuation',
                Zone::tableName() . '.title as city_emirates',
                Communities::tableName() . '.title as sector_community',
                SubCommunities::tableName() . '.title as area_development',
            ])
            ->leftJoin(Company::tableName(), Company::tableName() . '.id=' . Valuation::tableName() . '.client_id')
            ->leftJoin(Buildings::tableName(), Buildings::tableName() . '.id=' . Valuation::tableName() . '.building_info')
            ->leftJoin(Properties::tableName(), Properties::tableName() . '.id=' . Valuation::tableName() . '.property_id')
            ->leftJoin(ValuationPurposes::tableName(), ValuationPurposes::tableName() . '.id=' . Valuation::tableName() . '.purpose_of_valuation')
            ->leftJoin(Zone::tableName(), Zone::tableName() . '.id=' . Buildings::tableName() . '.city')
            ->leftJoin(Communities::tableName(), Communities::tableName() . '.id=' . Buildings::tableName() . '.community')
            ->leftJoin(SubCommunities::tableName(), SubCommunities::tableName() . '.id=' . Buildings::tableName() . '.sub_community')
            ->where([Valuation::tableName() . '.client_id' => $id])
            ->asArray();

        // ->all();
        // echo "<pre>"; print_r(count($valuations)); echo "</pre>"; echo "<br>";;
        // echo "<pre>"; print_r($valuations); echo "</pre>"; die;

        $exporter = new CsvGrid([
            'dataProvider' => new ActiveDataProvider([
                'query' => $valuations,
            ]),

            'columns' => [
                [
                    'attribute' => 'reference_number',
                    'label' => 'Windmills Ref. No.',
                ],
                [
                    'attribute' => 'created_at',
                    'label' => 'Date Received Request',
                    'format' => ['date', 'php:d-F-Y '],
                ],
                [
                    'attribute' => 'created_at',
                    'label' => 'Time Received of the Request',
                    'format' => 'time',
                ],
                [
                    'attribute' => '',
                    'label' => 'No of Working Days (after inspection an documents received)',
                    'value' => function ($model) {
                        $approver_date = Yii::$app->appHelperFunctions->getApproverDate($model);
                        if ($approver_date <> null) {
                            $date1 = date_create(date('Y-m-d', strtotime($model['target_date'])));
                            $date2 = date_create(date('Y-m-d', strtotime($approver_date)));
                            $diff = date_diff($date1, $date2);
                            if ($diff <> null) {
                                return $diff->d;
                            }
                        }
                    },
                ],
                [
                    'attribute' => 'plot_unit_number',
                    'label' => 'Plot No. / Unit No.',
                ],
                [
                    'attribute' => 'sector_community',
                    'label' => 'Sector / Community',
                ],
                [
                    'attribute' => 'area_development',
                    'label' => 'Area / Development',
                ],
                [
                    'attribute' => 'city_emirates',
                    'label' => 'City / Emirate',
                ],
                [
                    'attribute' => 'client_name_passport',
                    'label' => 'Customer Name',
                ],
                [
                    'attribute' => 'client_reference',
                    'label' => 'ADIB Finnone',
                ],
                [
                    'attribute' => 'valuation_status',
                    'label' => 'Report Status',
                    'value' => function ($model) {
                        if ($model['valuation_status'] == 5) {
                            return 'Delivered';
                        } else {
                            return "In Progress";
                        }
                    },
                ],
                [
                    'attribute' => '',
                    'label' => 'Inspection Status (Inspected / Not Inspected )',
                    'value' => function ($model) {
                        if ($model['valuation_status'] = 0 || $model['valuation_status'] == 1 || $model['valuation_status'] == 2) {
                            return 'Not Inspected';
                        } else {
                            return "Inspected";
                        }
                    },
                ],
                [
                    'attribute' => '',
                    'label' => 'Remarks',
                    'value' => function ($model) {
                        if ($model['parent_id'] <> null) {
                            return Yii::$app->appHelperFunctions->revisedReasons[$model['revised_reason']];
                        }
                    },
                ],
                [
                    'attribute' => '',
                    'label' => 'Report Inspection Type (Full Eva , Desk Top )',
                    'value' => function ($model) {
                        if ($model['inspection_type'] <> null) {
                            return Yii::$app->appHelperFunctions->inspectionTypeArr[$model['inspection_type']];
                        }
                    },
                ],
                [
                    'attribute' => '',
                    'label' => 'Assignment Type',
                    'value' => function ($model) {
                        if ($model['inspection_type'] <> null) {
                            return Yii::$app->appHelperFunctions->inspectionTypeArr[$model['inspection_type']];
                        }
                    },
                ],
                [
                    'attribute' => '',
                    'label' => 'Priority (Normal /Urgent )',
                    'value' => function ($model) {
                        $approver_date = Yii::$app->appHelperFunctions->getApproverDate($model);
                        if ($approver_date <> null) {
                            if (date('Y-m-d', strtotime($model['target_date'])) == date('Y-m-d', strtotime($approver_date)) AND $model['valuation_status'] == 5) {
                                return "Urgent";
                            } else {
                                return "Normal";
                            }
                        }
                    },
                ],
                [
                    'attribute' => '',
                    'label' => 'Report Delivery Date',
                    'format' => ['date', 'php:d-F-Y '],
                    'value' => function ($model) {
                        $approver_date = Yii::$app->appHelperFunctions->getApproverDate($model);
                        if ($approver_date <> null) {
                            return $approver_date;
                        }
                    },
                ],
            ],

        ]);

        $file_nameCsv = $model->title . '-valuations.csv';
        $file_nameXls = $model->title . '-valuations.xls';
        $file_nameXlxs = $model->title . '-valuations.xlsx';
        $exporter->export()->saveAs('uploads/client_valuation_csv/' . $file_nameCsv);
        $file_pathCsv = Yii::$app->params['client_valuations'] . $file_nameCsv;
        error_log("new3");
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();


        /* Set CSV parsing options */
        $reader->setDelimiter(',');
        $reader->setEnclosure('"');
        $reader->setSheetIndex(0);
        /* Load a CSV file and save as a XLS */
        $spreadsheet = $reader->load($file_pathCsv);

        // $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xls($spreadsheet);
        // $writer->save('uploads/client_valuation_csv/'.$file_nameXls);
        // $file_pathXls = Yii::$app->params['client_valuations'].$file_nameXls;

        $writer2 = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);


        $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
        $spreadsheet->getActiveSheet()->setTitle("Daily Status");

        $rightAlignColumnsArr = ['B', 'C', 'D', 'J', 'Q'];
        $leftAlignColumnsArr = ['A', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'O', 'P'];

        foreach ($rightAlignColumnsArr as $key => $columnId) {
            $spreadsheet->getActiveSheet()->getStyle($columnId)->getAlignment()->setHorizontal('right');
        }
        foreach ($leftAlignColumnsArr as $key => $columnId) {
            $spreadsheet->getActiveSheet()->getStyle($columnId)->getAlignment()->setHorizontal('left');
        }


        $styleArray = [
            'font' => [
                'bold' => true,
                'size' => 10
            ],

            'alignment' => [
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'DDE7F3',
                ],
                'endColor' => [
                    'argb' => 'DDE7F3',
                ],
            ],
        ];

        $spreadsheet->getActiveSheet()->getStyle('A1:Q1')->applyFromArray($styleArray)->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->freezePane('D2');

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(14);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(14);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(14);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(14);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(14);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(18);
        $spreadsheet->getActiveSheet()->getColumnDimension('M')->setWidth(50);
        $spreadsheet->getActiveSheet()->getColumnDimension('N')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('P')->setWidth(25);
        $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setWidth(15);

        $spreadsheet->getActiveSheet()->getRowDimension('1')->setRowHeight(64);

        $styleArray2 = [
            'borders' => [
                'right' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
                'left' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
                'bottom' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];

        $spreadsheet->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('C1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('D1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('E1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('G1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('H1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('I1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('J1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('K1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('L1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('M1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('N1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('O1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('P1')->applyFromArray($styleArray2);
        $spreadsheet->getActiveSheet()->getStyle('Q1')->applyFromArray($styleArray2);


        $spreadsheet->getActiveSheet()->setAutoFilter('A1:Q1');


        $styleArray3 = [
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFFF00',
                ],
                'endColor' => [
                    'argb' => 'FFFF00',
                ],
            ],
        ];

        $spreadsheet->getActiveSheet()->getStyle('K1:M1')->applyFromArray($styleArray3)->getAlignment()->setWrapText(true);

        $writer2->save('uploads/client_valuation_csv/' . $file_nameXlxs);
        $file_pathXlsx = Yii::$app->params['client_valuations'] . $file_nameXlxs;

        if (file_exists($file_pathXlsx)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($file_pathXlsx) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file_pathXlsx));
            readfile($file_pathXlsx);
            // exit;
        }
        $attachments = [$file_pathXlsx];
        $notifyData = [
            'client' => $client,
            'attachments' => $attachments,
        ];

        //  NotifyEvent::fire('Client.valuations.send', $notifyData);

        echo "Emails Send Successfully;";
    }


    public function actionOtherInstructingPersonsDetails()
    {
        $post = Yii::$app->request->post();
        // print_r($post['keyword']); die();
        if ($post['client_id'] <> null) {
            $results = //\app\models\InstructingPersonInfo::find()->where(['client_id'=>$post['client_id']])->asArray()->all();
                \app\models\User::find()
                    ->where(['company_id' => $post['client_id']])
                    ->innerJoin(\app\models\UserProfileInfo::tableName(), \app\models\UserProfileInfo::tableName() . ".user_id=" . \app\models\User::tableName() . ".id")
                    ->andWhere([\app\models\UserProfileInfo::tableName() . '.primary_contact' => 1])

                    // ->asArray()
                    ->all();
            // echo "<pre>"; print_r($results->); echo "</pre>"; die();
            
            $profileData = \app\models\UserProfileInfo::find()->where(['user_id'=>$results[0]->id])->one();
            
            $html = '';
           
            $html .= '<div id="other-instructing-person-details" data_other_instructing_person="'.$results[0]->id.'" data_phone="'.$profileData->mobile.'" data_email="'.$results[0]->email.'" data_firstname="'.$results[0]->firstname.'" data_lastname="'.$results[0]->lastname.'"></div>';


            return $html;
        } else {
            return '';
        }
    }

    public function actionMakeprimarycontact($id) {
        $this->checkLogin();
        $user = User::find()->select(['id','company_id'])->where(['id'=>$id])->one();

        $company_contacts = User::find()->select(['id'])->where(['company_id' => $user->company_id])->all();

        foreach ($company_contacts as $key => $contact) {
            $user_prof = UserProfileInfo::findOne(['user_id' => $contact->id]);
            if ($user_prof->user_id == $user->id) {
                UserProfileInfo::updateAll(['primary_contact' => 1], ['user_id' => $user_prof->user_id]);
            }else {
                UserProfileInfo::updateAll(['primary_contact' => 2], ['user_id' => $user_prof->user_id]);
            }
        }

        Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Primary contact updated successfully'));
        return $this->redirect(['client/update', 'id' => $user->company_id]);
    }

    public function actionAnnualValuationReminderEmail()
    {

        $clients = Company::find()
        ->where(['status' => 1, 'allow_for_valuation' => 1])
        ->andWhere(['or', ['client_type' => 'corporate'], ['client_type' => 'individual']])
        ->all();

        if($clients !== null){ 
            
            foreach($clients as $key => $client){
                
                $valuations = Valuation::find()
                ->where(['client_id' => $client->id, 'valuation_status' => 5])
                ->orderBy(['id' => SORT_DESC])
                ->all();

                
                if (count($valuations) > 1 && $client->email_reminder_status != 1 ) {
                    // if ($client->id == 37 ) {

                        $date_12months_ago = date('Y-m-d', strtotime('-12 months'));

                        
                        if (strtotime($valuations[0]->submission_approver_date) <= strtotime($date_12months_ago . ' 23:59:59')) {
                            
                            // echo $valuations[0]->client->title.'<br>'.$valuations[0]->reference_number.'<br>';
                            // echo $valuations[0]->submission_approver_date.'<br><br>';

                            $notifyData = [
                                'client' => $valuations[0]->client,
                                'attachments' => [],
                                // 'subject' => $email_subject,    
                                'replacements' => [
                                    '{wmReferenceNumber}' => $valuations[0]->reference_number,
                                    '{property}' => $valuations[0]->building->title,
                                    '{clientName}' => $valuations[0]->client->title,
                                    '{valuationDate}' => date('d-M-Y', strtotime($valuations[0]->submission_approver_date)),
                                ],
                            ];

                            if($client->client_type != "bank") {
                                 \app\modules\wisnotify\listners\NotifyEvent::fireAnnualReminderEmail('client.annual.valuation.reminder', $notifyData);
                            }
                            
                            Yii::$app->db->createCommand()
                            ->update(Company::tableName(), ['email_reminder_status' => 1, 'email_reminder_date' => date('Y-m-d H:i:s')], ['id' => $client->id])
                            ->execute();
                        }

                        
                    // }
                }
            }

        }

    }

    public function actionResetAnnualReminderEmail()
    {
        $clients = Company::find()
        ->where(['status' => 1, 'allow_for_valuation' => 1, 'email_reminder_status' => 1])
        ->andWhere(['or', ['client_type' => 'corporate'], ['client_type' => 'individual']])
        ->all();       

        if ($clients !== null) {
            foreach ($clients as $client) {
                $latestValuation = Valuation::find()
                    ->where(['client_id' => $client->id, 'valuation_status' => 5])
                    ->andWhere(['>', 'submission_approver_date', $client->email_reminder_date])
                    ->orderBy(['submission_approver_date' => SORT_DESC])
                    ->one();

                if ($latestValuation !== null) {
                    $date_12months_ago = date('Y-m-d', strtotime('-12 months'));

                    if (strtotime($latestValuation->submission_approver_date) <= strtotime($date_12months_ago . ' 23:59:59')) {
                        Yii::$app->db->createCommand()
                            ->update(Company::tableName(), ['email_reminder_status' => 0, 'email_reminder_date' => null], ['id' => $client->id])
                            ->execute();

                    }
                }
            }
        }
    }

    public function actionMonthlystatsbankfile($id = null)
    {





        $file_pathCsv = Yii::$app->params['client_valuations'] . 'monthly_valuation_report.xlsx';
        $client = Company::find()->where(['id'=> $id])->one();
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        /* Load a CSV file and save as a XLS */
        $spreadsheet = $reader->load($file_pathCsv);
        $sheet = $spreadsheet->getActiveSheet();
        $month =  date("F", strtotime("first day of previous month"));
      //  $month = 'September';
        $sheet->setCellValue('A1', $month);
        //$sheet->setCellValue('A1', 'July');



        $first_day_last_month = date("Y-m-d", strtotime("first day of last month")) . ' 00:00:00';
        $last_day_last_month = date("Y-m-d", strtotime("last day of last month")) . ' 23:59:59';

      //  $first_day_last_month = '2024-09-01 00:00:00';
     //  $last_day_last_month = '2024-09-30 23:23:59';

      /*  echo $first_day_last_month.'<br>';
        echo $last_day_last_month;
        die;*/

        $valuations = Valuation::find()
            ->select([
                Valuation::tableName() . '.id',
                Valuation::tableName() . '.client_reference',
                Valuation::tableName() . '.target_date',
                Valuation::tableName() . '.client_name_passport',
                Valuation::tableName() . '.title_deed',
                Valuation::tableName() . '.submission_approver_date',
                Valuation::tableName() . '.parent_id',
                Buildings::tableName() . '.title as Building',
            ])
            ->leftJoin(Buildings::tableName(), Buildings::tableName() . '.id=' . Valuation::tableName() . '.building_info')
            ->where([Valuation::tableName() . '.valuation_status' => 5])
            ->andWhere([Valuation::tableName() . '.client_id' => $id])
            ->andWhere(['valuation.parent_id' => null])
            ->andFilterWhere([
                'between', 'valuation.submission_approver_date', $first_day_last_month, $last_day_last_month
            ])
            ->asArray()->all();


        if($valuations <> null && !empty($valuations)) {
            $main_array_vals = array();
            $parent_array_vals = array();

            foreach ($valuations as $key => $data_set_a) {
                if ($data_set_a['parent_id'] <> null) {
                    $parent_array_vals[] = $data_set_a['parent_id'];
                }
            }


            foreach ($valuations as $key => $data_set_b) {
                if (!in_array($data_set_b['id'], $parent_array_vals)) {
                    $main_array_vals[] = $data_set_b;
                }
            }


            foreach ($main_array_vals as $key => $data_set) {


                $model = Valuation::findOne($data_set['id']);
                $detail_main= \app\models\ValuationDetail::find()->where(['valuation_id' => $model->id])->one();
                $sea_level='';
              if($detail_main->elevation_sea_level ==  1001){
                $sea_level= 'Not Applicable';

               }else if($detail_main->elevation_sea_level ==  1002){
                $sea_level = 'Not Known';
                }else{
               $sea_level = $detail_main->elevation_sea_level;
              }
                $land = array(4, 5, 20, 23, 26, 29, 39, 46, 47, 48, 49, 50, 53);
                if (in_array($model->property_id, $land)) {
                    $completion_year = 'Not Applicable';
                } else {
                    $completion_year = ((int)date('Y')) - round($model->inspectProperty->estimated_age) . ' ';
                }
                $address='';
                $unitNumber = ($model->unit_number <> "" || $model->unit_number == 0) ? $model->unit_number : "Not Applicable";

                if($model->unit_number > 0) {
                    $address.= 'Unit Number '. $unitNumber.', ';
                }
                $address.= $model->building->title.', '.$model->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city];


                $floor_number = '';

                if(in_array($model->property_id, [1,12,37,28,17])){
                    $floor_text = "Ground floor";
                }else{
                    $floor_text = "Not Applicable";
                }
                $floor_number =  ($model->floor_number > 0)?  $model->floor_number: $floor_text;

                if($model->valuation_approach == 1){
                    if($model->inspectProperty->full_building_floors <> null){
                        $totalBuildingFloor = " + ". $model->inspectProperty->full_building_floors . " Building floors" ;
                    }
                    else {
                        $totalBuildingFloor = "";
                    }
                }
                else {
                    $totalBuildingFloor = " + ". $model->inspectProperty->full_building_floors. " Building floors";
                }

                if($model->property->title == 'Villa') {
                    if ($model->inspectProperty->number_of_basement > 0 && $model->inspectProperty->number_of_basement == 1 ) {
                        $total_building_floors = "Basement + " .Yii::$app->appHelperFunctions->listingLevelsListArr[$model->inspectProperty->number_of_levels];
                    }else if($model->inspectProperty->number_of_basement > 1){
                        $total_building_floors = $model->inspectProperty->number_of_basement. " Basement + Ground " . $totalBuildingFloor;
                    }else{
                        $total_building_floors = Yii::$app->appHelperFunctions->listingLevelsListArr[$model->inspectProperty->number_of_levels];
                    }

                }else {
                    if($model->inspectProperty->number_of_mezannines > 0){
                        if ($model->inspectProperty->number_of_mezannines == 1) {
                            if ($model->inspectProperty->number_of_basement > 0 && $model->inspectProperty->number_of_basement == 1) {
                                $total_building_floors = "Basement + Ground + Mezzanine " . $totalBuildingFloor;
                            } else if($model->inspectProperty->number_of_basement > 1){
                                $total_building_floors = $model->inspectProperty->number_of_basement. " Basements + Ground + Mezzanine " . $totalBuildingFloor;
                            }else {
                                $total_building_floors = "Ground + Mezzanine " . $totalBuildingFloor;
                            }
                        } else if($model->inspectProperty->number_of_mezannines > 1){
                            if ($model->inspectProperty->number_of_basement > 0 && $model->inspectProperty->number_of_basement == 1) {
                                $total_building_floors = "Basement + Ground + ". $model->inspectProperty->number_of_mezannines ." Mezannines " . $totalBuildingFloor;
                            } else if($model->inspectProperty->number_of_basement > 1){
                                $total_building_floors = $model->inspectProperty->number_of_basement. " Basements + Ground + ". $model->inspectProperty->number_of_mezannines ." Mezannines " . $totalBuildingFloor;
                            }else {
                                $total_building_floors = "Ground + ". $model->inspectProperty->number_of_mezannines ." Mezannines " . $totalBuildingFloor;
                            }
                        }
                    }
                    else{
                        if ($model->inspectProperty->number_of_basement > 0 && $model->inspectProperty->number_of_basement == 1) {
                            $total_building_floors = "Basement + Ground " . $totalBuildingFloor;
                        } else if($model->inspectProperty->number_of_basement > 1){
                            $total_building_floors = $model->inspectProperty->number_of_basement. " Basement + Ground " . $totalBuildingFloor;
                        }else {
                            $total_building_floors = "Ground " . $totalBuildingFloor;
                        }
                    }
                }



                $plot_size = ($model->land_size > 0) ? Yii::$app->appHelperFunctions->wmFormate($model->land_size) : '';
                $approver_data = ValuationApproversData::find()->where(['valuation_id' => $data_set['id'], 'approver_type' => 'approver'])->one();
                $estimate_price_byapprover = number_format($approver_data->estimated_market_value);

                $owners = \app\models\ValuationOwners::find()->where(['valuation_id' => $data_set['id']])->all();
                $owners_name = "";
                foreach ($owners as $record) {
                    $owners_name .= $record->name . " " . $record->lastname . ", ";
                }


                $sheet->setCellValue('A' . ($key + 3), ($key + 1));
                $sheet->setCellValue('B' . ($key + 3), $data_set['client_reference']);
                $sheet->setCellValue('C' . ($key + 3), date('d-M-Y', strtotime($data_set['submission_approver_date'])));
                $sheet->setCellValue('D' . ($key + 3), $address);
                $sheet->setCellValue('E' . ($key + 3), $model->property->title);
                $sheet->setCellValue('F' . ($key + 3), number_format($model->inspectProperty->built_up_area, 2));
                $sheet->setCellValue('G' . ($key + 3), $plot_size);
                $sheet->setCellValue('H' . ($key + 3), $floor_number);
                $sheet->setCellValue('I' . ($key + 3), $total_building_floors);
                $sheet->setCellValue('J' . ($key + 3), $model->inspectProperty->latitude);
                $sheet->setCellValue('K' . ($key + 3), $model->inspectProperty->longitude);
                $sheet->setCellValue('L' . ($key + 3), round($model->inspectProperty->estimated_age));
                $sheet->setCellValue('M' . ($key + 3), $completion_year);
                $sheet->setCellValue('N' . ($key + 3), $sea_level);
                $sheet->setCellValue('O' . ($key + 3), $estimate_price_byapprover);



            }
        }


        $writer2 = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $file_nameXlxs = 'client-monthly-valuation-'.$month.'-'.$client->id.'-'.$client->title.'.xlsx';;

        if (file_exists(Yii::$app->params['client_valuations']. $file_nameXlxs)) {
            //unlink(Yii::$app->params['client_valuations']. $file_nameXlxs);
        }
        $writer2->save('uploads/client_valuation_csv/' . $file_nameXlxs);

       // $writer2->save($fullPath);

        $file_pathXlsx = Yii::$app->params['client_valuations'] . $file_nameXlxs;

       /* echo $file_pathXlsx;
        die;*/

         return $file_pathXlsx;

       /* if (file_exists($file_pathXlsx)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($file_pathXlsx) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file_pathXlsx));
            readfile($file_pathXlsx);
            // exit;
        }*/



        $attachments = [$file_pathXlsx];
        $notifyData = [
            'subject'=> 'Monthly Valuations Report -'. $month,
            'g_subject'=> 'Monthly Valuations Report -'. $month,
            'client' => $client,
            'attachments' => $attachments,
            'replacements'=>[
                '{month}'=>$month,
            ],
        ];


        // NotifyEvent::fireToGovt('ajman.valuations.send', $notifyData);
         exit;
        // unlink($file_pathCsv);
        // unlink($file_pathXls);
        //  unlink($file_pathXlsx);


    }


    public function actionMonthlystattattached($id = null)
    {




        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');

        $clients = Company::find()->where(['client_type'=> 'bank','monthly_email'=> 1,'status'=> 1,'id'=> 119711])->all();
       /* echo "<pre>";
        print_r($clients);
        die;*/


        $month =  date('m') - 1;
        $year = date('Y');
        if($month == 0){
            $month= 1;

        }

        /*   echo "<pre>";
           print_r(count($clients));
           die;*/
        foreach ($clients as $client) {
            if($client->id == 119711) {

                $valuations_month_number = Valuation::find()->where(['client_id' => $client->id])
                    ->andWhere('valuation_status=5')
                    ->andWhere(['valuation.parent_id' => null])
                    ->andFilterWhere([
                        'between', 'submission_approver_date', '2024-11-01 00:00:00', '2024-11-30 23:59:59'
                    ])
                    ->all();
                $valuations_year_number = [];

                 $valuations_year_number = Valuation::find()->where(['client_id' => $client->id])
                     ->andWhere('valuation_status=5')
                     ->andFilterWhere([
                         'between', 'submission_approver_date', '2024-01-01 00:00:00', '2024-11-30 23:59:59'
                     ])
                     ->andWhere(['valuation.parent_id' => null])
                     ->all();

                $last_month = date('F', strtotime('last month'));
                $date_value = $last_month . ' ' . date('Y');
                $subject = 'Valuation Instructions to Windmills in ' . $date_value;

                if (count($valuations_month_number) > 0) {
                    echo $client->id . '<br>' . $client->title . '<br>' . $client->primaryContact->email . '<br> total:' . count($valuations_year_number) . '<br> this month:' . count($valuations_month_number) . '<br><br>';


                    $curl_handle_1 = curl_init();
                    curl_setopt($curl_handle_1, CURLOPT_URL, Url::toRoute(['client/monthlystatsbankfile', 'id' => $client->id]));
                    curl_setopt($curl_handle_1, CURLOPT_CONNECTTIMEOUT, 2);
                    curl_setopt($curl_handle_1, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl_handle_1, CURLOPT_USERAGENT, 'Maxima');

                    $val2 = curl_exec($curl_handle_1);
                    $ip = curl_getinfo($curl_handle_1, CURLINFO_PRIMARY_IP);
                    curl_close($curl_handle_1);

                    $attachments[] = $val2;

                    $notifyData = [
                        'client' => $client,
                        'subject' => $subject,
                        'attachments' => $attachments,
                        'uid' => $client->id,
                        'replacements' => [
                            '{clientName}' => $client->title,
                            '{total_valuation}' => count($valuations_year_number),
                            '{date_value}' => $date_value,
                            '{this_month_valuation}' => count($valuations_month_number),

                        ],
                    ];


                    if (count($valuations_month_number) > 0) {
                        \app\modules\wisnotify\listners\NotifyEvent::fireMonthlyAllBanks('valuation.monthlyzero', $notifyData);

                    } else if (count($valuations_month_number) < 10) {
                        //  \app\modules\wisnotify\listners\NotifyEvent::fireMonthlyAllBanks('valuation.monthlylessthanten', $notifyData);
                    }
                     Yii::$app->db->createCommand()
                         ->update('company', ['monthly_email' => 2], 'id='.$client->id .'')
                         ->execute();
                }
            }

        }
        die('done');






        $file_pathCsv = Yii::$app->params['client_valuations'] . 'monthly_valuation_report.xlsx';

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        /* Load a CSV file and save as a XLS */
        $spreadsheet = $reader->load($file_pathCsv);
        $sheet = $spreadsheet->getActiveSheet();
        $month =  date("F", strtotime("first day of previous month"));
        $month = 'September';
        $sheet->setCellValue('A1', $month);
        //$sheet->setCellValue('A1', 'July');



        $first_day_last_month = date("Y-m-d", strtotime("first day of last month")) . ' 00:00:00';
        $last_day_last_month = date("Y-m-d", strtotime("last day of last month")) . ' 23:59:59';

        $first_day_last_month = '2024-09-01 00:00:00';
        $last_day_last_month = '2024-09-30 23:23:59';

        /*  echo $first_day_last_month.'<br>';
          echo $last_day_last_month;
          die;*/

        $valuations = Valuation::find()
            ->select([
                Valuation::tableName() . '.id',
                Valuation::tableName() . '.client_reference',
                Valuation::tableName() . '.target_date',
                Valuation::tableName() . '.client_name_passport',
                Valuation::tableName() . '.title_deed',
                Valuation::tableName() . '.submission_approver_date',
                Valuation::tableName() . '.parent_id',
                Buildings::tableName() . '.title as Building',
            ])
            ->leftJoin(Buildings::tableName(), Buildings::tableName() . '.id=' . Valuation::tableName() . '.building_info')
            ->where([Valuation::tableName() . '.valuation_status' => 5])
            ->andWhere([Valuation::tableName() . '.client_id' => 4])
            ->andWhere(['valuation.parent_id' => null])
            ->andFilterWhere([
                'between', 'valuation.submission_approver_date', $first_day_last_month, $last_day_last_month
            ])
            ->asArray()->all();


        if($valuations <> null && !empty($valuations)) {
            $main_array_vals = array();
            $parent_array_vals = array();

            foreach ($valuations as $key => $data_set_a) {
                if ($data_set_a['parent_id'] <> null) {
                    $parent_array_vals[] = $data_set_a['parent_id'];
                }
            }


            foreach ($valuations as $key => $data_set_b) {
                if (!in_array($data_set_b['id'], $parent_array_vals)) {
                    $main_array_vals[] = $data_set_b;
                }
            }


            foreach ($main_array_vals as $key => $data_set) {


                $model = Valuation::findOne($data_set['id']);
                $detail_main= \app\models\ValuationDetail::find()->where(['valuation_id' => $model->id])->one();
                $sea_level='';
                if($detail_main->elevation_sea_level ==  1001){
                    $sea_level= 'Not Applicable';

                }else if($detail_main->elevation_sea_level ==  1002){
                    $sea_level = 'Not Known';
                }else{
                    $sea_level = $detail_main->elevation_sea_level;
                }
                $land = array(4, 5, 20, 23, 26, 29, 39, 46, 47, 48, 49, 50, 53);
                if (in_array($model->property_id, $land)) {
                    $completion_year = 'Not Applicable';
                } else {
                    $completion_year = ((int)date('Y')) - round($model->inspectProperty->estimated_age) . ' ';
                }
                $address='';
                $unitNumber = ($model->unit_number <> "" || $model->unit_number == 0) ? $model->unit_number : "Not Applicable";

                if($model->unit_number > 0) {
                    $address.= 'Unit Number '. $unitNumber.', ';
                }
                $address.= $model->building->title.', '.$model->building->communities->title.', '.Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city];


                $floor_number = '';

                if(in_array($model->property_id, [1,12,37,28,17])){
                    $floor_text = "Ground floor";
                }else{
                    $floor_text = "Not Applicable";
                }
                $floor_number =  ($model->floor_number > 0)?  $model->floor_number: $floor_text;

                if($model->valuation_approach == 1){
                    if($model->inspectProperty->full_building_floors <> null){
                        $totalBuildingFloor = " + ". $model->inspectProperty->full_building_floors . " Building floors" ;
                    }
                    else {
                        $totalBuildingFloor = "";
                    }
                }
                else {
                    $totalBuildingFloor = " + ". $model->inspectProperty->full_building_floors. " Building floors";
                }

                if($model->property->title == 'Villa') {
                    if ($model->inspectProperty->number_of_basement > 0 && $model->inspectProperty->number_of_basement == 1 ) {
                        $total_building_floors = "Basement + " .Yii::$app->appHelperFunctions->listingLevelsListArr[$model->inspectProperty->number_of_levels];
                    }else if($model->inspectProperty->number_of_basement > 1){
                        $total_building_floors = $model->inspectProperty->number_of_basement. " Basement + Ground " . $totalBuildingFloor;
                    }else{
                        $total_building_floors = Yii::$app->appHelperFunctions->listingLevelsListArr[$model->inspectProperty->number_of_levels];
                    }

                }else {
                    if($model->inspectProperty->number_of_mezannines > 0){
                        if ($model->inspectProperty->number_of_mezannines == 1) {
                            if ($model->inspectProperty->number_of_basement > 0 && $model->inspectProperty->number_of_basement == 1) {
                                $total_building_floors = "Basement + Ground + Mezzanine " . $totalBuildingFloor;
                            } else if($model->inspectProperty->number_of_basement > 1){
                                $total_building_floors = $model->inspectProperty->number_of_basement. " Basements + Ground + Mezzanine " . $totalBuildingFloor;
                            }else {
                                $total_building_floors = "Ground + Mezzanine " . $totalBuildingFloor;
                            }
                        } else if($model->inspectProperty->number_of_mezannines > 1){
                            if ($model->inspectProperty->number_of_basement > 0 && $model->inspectProperty->number_of_basement == 1) {
                                $total_building_floors = "Basement + Ground + ". $model->inspectProperty->number_of_mezannines ." Mezannines " . $totalBuildingFloor;
                            } else if($model->inspectProperty->number_of_basement > 1){
                                $total_building_floors = $model->inspectProperty->number_of_basement. " Basements + Ground + ". $model->inspectProperty->number_of_mezannines ." Mezannines " . $totalBuildingFloor;
                            }else {
                                $total_building_floors = "Ground + ". $model->inspectProperty->number_of_mezannines ." Mezannines " . $totalBuildingFloor;
                            }
                        }
                    }
                    else{
                        if ($model->inspectProperty->number_of_basement > 0 && $model->inspectProperty->number_of_basement == 1) {
                            $total_building_floors = "Basement + Ground " . $totalBuildingFloor;
                        } else if($model->inspectProperty->number_of_basement > 1){
                            $total_building_floors = $model->inspectProperty->number_of_basement. " Basement + Ground " . $totalBuildingFloor;
                        }else {
                            $total_building_floors = "Ground " . $totalBuildingFloor;
                        }
                    }
                }



                $plot_size = ($model->land_size > 0) ? Yii::$app->appHelperFunctions->wmFormate($model->land_size) : '';
                $approver_data = ValuationApproversData::find()->where(['valuation_id' => $data_set['id'], 'approver_type' => 'approver'])->one();
                $estimate_price_byapprover = number_format($approver_data->estimated_market_value);

                $owners = \app\models\ValuationOwners::find()->where(['valuation_id' => $data_set['id']])->all();
                $owners_name = "";
                foreach ($owners as $record) {
                    $owners_name .= $record->name . " " . $record->lastname . ", ";
                }


                $sheet->setCellValue('A' . ($key + 3), ($key + 1));
                $sheet->setCellValue('B' . ($key + 3), $data_set['client_reference']);
                $sheet->setCellValue('C' . ($key + 3), date('d-M-Y', strtotime($data_set['submission_approver_date'])));
                $sheet->setCellValue('D' . ($key + 3), $address);
                $sheet->setCellValue('E' . ($key + 3), $model->property->title);
                $sheet->setCellValue('F' . ($key + 3), number_format($model->inspectProperty->built_up_area, 2));
                $sheet->setCellValue('G' . ($key + 3), $plot_size);
                $sheet->setCellValue('H' . ($key + 3), $floor_number);
                $sheet->setCellValue('I' . ($key + 3), $total_building_floors);
                $sheet->setCellValue('J' . ($key + 3), $model->inspectProperty->latitude);
                $sheet->setCellValue('K' . ($key + 3), $model->inspectProperty->longitude);
                $sheet->setCellValue('L' . ($key + 3), round($model->inspectProperty->estimated_age));
                $sheet->setCellValue('M' . ($key + 3), $completion_year);
                $sheet->setCellValue('N' . ($key + 3), $sea_level);
                $sheet->setCellValue('O' . ($key + 3), $estimate_price_byapprover);



            }
        }


        $writer2 = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $file_nameXlxs = 'client-monthly-valuation-'.$month.'-'.$client->title.'.xlsx';

        if (file_exists(Yii::$app->params['client_valuations']. $file_nameXlxs)) {
            //unlink(Yii::$app->params['client_valuations']. $file_nameXlxs);
        }
        $writer2->save('uploads/client_valuation_csv/' . $file_nameXlxs);

        // $writer2->save($fullPath);

        $file_pathXlsx = Yii::$app->params['client_valuations'] . $file_nameXlxs;

        echo $file_pathXlsx;
        die;

        // return $file_pathXlsx;

        /* if (file_exists($file_pathXlsx)) {
             header('Content-Description: File Transfer');
             header('Content-Type: application/octet-stream');
             header('Content-Disposition: attachment; filename="' . basename($file_pathXlsx) . '"');
             header('Expires: 0');
             header('Cache-Control: must-revalidate');
             header('Pragma: public');
             header('Content-Length: ' . filesize($file_pathXlsx));
             readfile($file_pathXlsx);
             // exit;
         }*/



        $attachments = [$file_pathXlsx];
        $notifyData = [
            'subject'=> 'Monthly Valuations Report -'. $month,
            'g_subject'=> 'Monthly Valuations Report -'. $month,
            'client' => $client,
            'attachments' => $attachments,
            'replacements'=>[
                '{month}'=>$month,
            ],
        ];


        // NotifyEvent::fireToGovt('ajman.valuations.send', $notifyData);
        exit;
        // unlink($file_pathCsv);
        // unlink($file_pathXls);
        //  unlink($file_pathXlsx);


    }





    public function actionAjmanReraFollowup($id = null)
    {

        $frequncy =  Yii::$app->appHelperFunctions->getValSetting('ajman_rera_followup_email_frequency');


        $valuations = Valuation::find()
            ->select([
                Valuation::tableName() . '.id',
                Valuation::tableName() . '.reference_number',
                Valuation::tableName() . '.target_date',
                Valuation::tableName() . '.client_name_passport',
                Valuation::tableName() . '.title_deed',
                Valuation::tableName() . '.submission_approver_date',
                Valuation::tableName() . '.parent_id',
                Valuation::tableName() . '.ajman_email_status',
                Valuation::tableName() . '.ajman_follow_up_date',
                Valuation::tableName() . '.ajman_approved',
                Buildings::tableName() . '.title as Building',
            ])
            ->leftJoin(Buildings::tableName(), Buildings::tableName() . '.id=' . Valuation::tableName() . '.building_info')
            ->where([Buildings::tableName() . '.city' => 3507])
            ->andWhere([Valuation::tableName() . '.valuation_status' => 5])
            ->andWhere([Valuation::tableName() . '.ajman_approved' => [0,2]])
            ->andWhere(['>', Valuation::tableName() . '.id', 13963])
            ->asArray()->all();



            if($valuations<> null && !empty($valuations)){

                $main_array_vals = array();
                $main_array_vals_clients = array();
                $parent_array_vals = array();

                foreach ($valuations as $key => $data_set_a) {
                    if ($data_set_a['parent_id'] <> null) {
                        $parent_array_vals[] = $data_set_a['parent_id'];
                    }
                }


                foreach ($valuations as $key => $data_set_b) {
                    if (!in_array($data_set_b['id'], $parent_array_vals)) {
                        if($data_set_b['ajman_follow_up_date'] <> null){

                        }else{
                            $data_set_b['ajman_follow_up_date'] =  date('Y-m-d',strtotime($data_set_b['submission_approver_date']));
                        }
                        $main_array_vals_clients[] = $data_set_b;
                        if($data_set_b['ajman_approved'] == 0) {
                            $main_array_vals[] = $data_set_b;
                        }
                    }
                }

               /* echo "<pre>";
                print_r($main_array_vals_clients);
                print_r($main_array_vals);
                die;*/

                foreach ($main_array_vals_clients as $key => $valuation){

                    if($valuation['ajman_follow_up_date'] <> null){

                        $current_date = date('Y-m-d');
                        $follow_up_date = $valuation['ajman_follow_up_date'];
                        $date1 = new \DateTime($current_date);
                        $date2 =  new \DateTime($follow_up_date);

                        $interval = $date1->diff($date2);

                        if($interval->days >= $frequncy){


                            $valuation = Valuation::find()->where(['id' => $valuation['id']])->one();
                            $uid= $valuation->id;
                            if(isset($valuation->quotation_id) && $valuation->quotation_id <> null){
                                $uid = 'crm'.$valuation->quotation_id;
                            }
                            $inspectionEmail ='';
                          //client email
                            $notifyData = [
                                'client' => $valuation->client,
                                'attachments' => [],
                                'uid' => $uid,
                                'subject' => $valuation->email_subject,
                                'valuer' => $valuation->approver->email,
                                'inspector' => $inspectionEmail,
                                'replacements' => [
                                    '{clientName}' => $valuation->client->title,
                                ],
                            ];
                            \app\modules\wisnotify\listners\NotifyEvent::fire1('ajmanrera.followupclient.send', $notifyData);

                        }
                    }

                }
                foreach ($main_array_vals as $key => $valuation_ajman){

                    if($valuation_ajman['ajman_follow_up_date'] <> null){

                        $current_date = date('Y-m-d');
                        $follow_up_date = $valuation_ajman['ajman_follow_up_date'];
                        $date1 = new \DateTime($current_date);
                        $date2 =  new \DateTime($follow_up_date);

                        $interval = $date1->diff($date2);

                        if($interval->days >= $frequncy){


                            $valuation = Valuation::find()->where(['id' => $valuation_ajman['id']])->one();
                            $uid= $valuation->id;
                            if(isset($valuation->quotation_id) && $valuation->quotation_id <> null){
                                $uid = 'crm'.$valuation->quotation_id;
                            }
                            $inspectionEmail ='';

                            //govt email
                            $subject = "Windmills Valuation Report -".$valuation->reference_number;
                            $client = $valuation->client;
                            $attachments=[];
                            $notifyData = [
                                'client' => $client,
                                'attachments' => $attachments,
                                'g_subject'=>$subject
                            ];
                              \app\modules\wisnotify\listners\NotifyEvent::fireToGovt('ajmanrera.followupgovt.send', $notifyData);

                             Yii::$app->db->createCommand()->update('valuation', ['ajman_follow_up_date' => date('Y-m-d')], 'id='.$valuation->id .'')->execute();
                        }
                    }

                }
            }

        die('done');
        echo "<pre>";
        print_r($valuations);
        die;

    }




}
