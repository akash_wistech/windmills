<?php

namespace app\controllers;

use Yii;
use app\models\CustomField;
use app\models\CustomFieldSearch;
use app\models\CustomFieldSubOption;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* CustomFieldController implements the CRUD actions for CustomField model.
*/
class CustomFieldController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all CustomField models.
  * @return mixed
  */
  public function actionIndex()
  {
    $this->checkAdmin();
    $searchModel = new CustomFieldSearch();
    $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new CustomField model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkSuperAdmin();
    $model = new CustomField();
    $model->status=1;

    $request = Yii::$app->request;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Custom field saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }
    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
  * Updates an existing CustomField model.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionUpdate($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app','ustom field saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('update', [
      'model' => $model,
    ]);
  }

  /**
  * Creates a new model.
  */
  protected function newModel()
  {
    $model = new CustomField;
    return $model;
  }

  public function actionSubOptionStatus($field_id,$id)
  {
    $this->checkAdmin();
    $modelField = $this->findModel($field_id);
    $model=$this->findFieldOptionModel($field_id,$id);
    $model->updateStatus();
    $msg['success']=['heading'=>Yii::t('app','Updated'),'msg'=>Yii::t('app','Field Option updated successfully')];
    echo json_encode($msg);
    exit;
  }

  public function actionDeleteSubOption($field_id,$id)
  {
    $this->checkAdmin();
    $modelField = $this->findModel($field_id);
    $model = $this->findFieldOptionModel($field_id,$id);
    $model->softDelete();
    $msg['success']=['heading'=>Yii::t('app','Deleted'),'msg'=>Yii::t('app','Field Option removed successfully')];
    echo json_encode($msg);
    exit;
  }

  /**
  * Finds the CustomField model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return CustomField the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = CustomField::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }

  /**
  * Finds the MediaTypeFieldSubOption model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return MediaTypeFieldSubOption the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findFieldOptionModel($field_id,$id)
  {
    if (($model = CustomFieldSubOption::findOne(['id'=>$id,'custom_field_id'=>$field_id])) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
