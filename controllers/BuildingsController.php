<?php

namespace app\controllers;

use app\models\Country;
use app\models\Zone;
use Yii;
use app\models\Buildings;
use app\models\BuildingsSearch;
use app\components\helpers\DefController;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\BuildingsImportForm;
use app\models\Communities;

/**
 * BuildingsController implements the CRUD actions for Buildings model.
 */
class BuildingsController extends DefController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        // Check if the user is logged in
        if (Yii::$app->user->isGuest) {
            // Redirect to the login page
           // return Yii::$app->response->redirect(['site/login'])->send();
        }
    }
    /**
     * Lists all Buildings models.
     * @return mixed
     */
    public function actionIndex()
    {



        /* $all_buildings = (new \yii\db\Query())
             ->select('id,estimated_age,year_of_construction')
             ->from('buildings')
             ->all();

        $current_year = date('Y').'<br>';



        foreach($all_buildings as $building){

            $construction_year  = date("Y",strtotime("-".round($building['estimated_age'])." year"));



            Yii::$app->db->createCommand()
                ->update('buildings' , [ 'year_of_construction' => $construction_year ] , [ 'id' => $building['id']])
                ->execute();
        }
        $all_buildings_new = (new \yii\db\Query())
            ->select('id,estimated_age,year_of_construction')
            ->from('buildings')
            ->all();

        echo "<pre>";
        print_r($all_buildings_new);
        die;*/
        $this->checkLogin();
        $searchModel = new BuildingsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Buildings model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Buildings model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->checkLogin();
        $model = new Buildings();


        if ($model->load(Yii::$app->request->post())) {
            
            $model->status_verified = $model->status;
            $this->StatusVerify($model);

            if($model->save()){         
                $this->makeHistory([
                    'model' => $model, 
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                    'action' => 'data_created',
                    'verify_field' => 'status',
                ]);
                Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
                return $this->redirect(['index']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    public function actionCreateCrm($id,$property_index)
    {
        $this->checkLogin();
        $model = new Buildings();


        if ($model->load(Yii::$app->request->post())) {

            /*echo "<pre>";
            print_r($model);
            die;*/
            if($model->save($model)){

                Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
                return $this->redirect(['crm-quotations/step_1?id='.$id.'&property_index='.$property_index]);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }
        return $this->renderAjax('create_crm', [
            'model' => $model,
        ]);
    }
    /**
     * Updates an existing Buildings model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $this->checkLogin();
        $model = $this->findModel($id);
        $old_verify_status = $model->status;
        $model->status_verified = $model->status;

        if ($model->load(Yii::$app->request->post())) {
            $model->status_verified = $model->status;
            $this->StatusVerify($model);

            if($model->save()){

                $this->makeHistory([
                    'model' => $model, 
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                    'action' => 'data_updated',
                    'verify_field' => 'status',
                    'old_verify_status' => $old_verify_status,
                ]);
                Yii::$app->getSession()->addFlash('success', Yii::t('app','Information updated successfully'));
                return $this->redirect(['index']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Buildings model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    /**
     * Displays a single ListingsTransactions model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDetail($id)
    {
        $this->checkLogin();
        $detail = Buildings::findOne($id);
        // $community = Communities::findOne($id);
        $city=Zone::find()
            ->select([
                'country_id',
            ])
            ->where(['id' => $detail->city])
            ->orderBy(['title' => SORT_ASC])->asArray()->one();
        $country = Country::find()
            ->select([
                'title',
            ])
            ->where(['id' => $city['country_id']])->asArray()->one();
        $detail->city = Yii::$app->appHelperFunctions->emiratedListArr[$detail->city];
        $detail->community = $detail->communities->title;
        $detail->sub_community = $detail->subCommunities->title;
        $property_type = $detail->property->id;
        $valuation_approach = $detail->property->valuation_approach;
        // $detail->com_city = Yii::$app->appHelperFunctions->emiratedListArr[$community->city];

        return json_encode(array('detail' => $detail->attributes,'ptype'=>$property_type, 'valuation_approach'=>$valuation_approach,'country'=>$country['title']));
    }

    public function actionCommunity($id)
    {
        $community = Communities::findOne($id);
        // $city=Zone::find()
        //     ->select([
        //         'country_id',
        //     ])
        //     ->where(['id' => $detail->city])
        //     ->orderBy(['title' => SORT_ASC])->asArray()->one();
        // $country = Country::find()
        //     ->select([
        //         'title',
        //     ])
        //     ->where(['id' => $city['country_id']])->asArray()->one();
        $community->city = Yii::$app->appHelperFunctions->emiratedListArr[$community->city];

        return json_encode(array('community' => $community->attributes));
    }

    public function actionCity($id)
    {
        $cities = Zone::find()->select(['title'])->where(['country_id' => $id])->column();
        return json_encode(array('cities' => $cities));
    }


    /**
     * Import for SoldTransaction.
     * If import is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionImport()
    {
        $model = new BuildingsImportForm();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->getSession()->setFlash('success', Yii::t('app','Data imported successfully'));
                return $this->redirect(['index']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('import', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Buildings model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Buildings the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Buildings::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
