<?php

namespace app\controllers;

use Yii;

use yii\filters\VerbFilter;
use app\components\helpers\DefController;
use app\models\AssetManager;
use app\models\AssetManagerSearch;

/**
 * AssetManagerController implements the CRUD actions for Asset model.
 */
class AssetManagerController extends DefController
{
    public function actionIndex()
    {
        $searchModel = new AssetManagerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    { 
        if(!empty(Yii::$app->request->post()))
        {
            $model = new AssetManager();
            $model->transaction_type = Yii::$app->request->post()['transaction_type'];
            if( Yii::$app->request->post()['transaction_type'] == "asset" )
            {
                $model->asset_name = Yii::$app->request->post()['asset_name'];
                if( isset(Yii::$app->request->post()['asset_category_id']) )
                {
                    $parts = explode(",", Yii::$app->request->post()['asset_category_id']);
    
                    $model->category_id = $parts[0];
                    $model->category_name = $parts[1];
                }
                $model->depreciation = Yii::$app->request->post()['depreciation'];
                $model->brand = Yii::$app->request->post()['brand'];
            }elseif( Yii::$app->request->post()['transaction_type'] == "consumable" || Yii::$app->request->post()['transaction_type'] == "service"  )
            {
                $model->asset_name = Yii::$app->request->post()['expense_name'];
                if( isset(Yii::$app->request->post()['expense_category_id']) )
                {
                    $parts = explode(",", Yii::$app->request->post()['expense_category_id']);
    
                    $model->category_id = $parts[0];
                    $model->category_name = $parts[1];
                }
            }

            $model->status = Yii::$app->request->post()['status'];
            $model->created_by = Yii::$app->user->identity->id;
            $model->created_at = date("Y/m/d H:i:s");
            $model->save();
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = AssetManager::findOne(['id' => $id]);
        if(!empty(Yii::$app->request->post()))
        {
            $model->transaction_type = Yii::$app->request->post()['transaction_type'];
            if( Yii::$app->request->post()['transaction_type'] == "asset" )
            {
                $model->asset_name = Yii::$app->request->post()['asset_name'];
                if( isset(Yii::$app->request->post()['asset_category_id']) )
                {
                    $parts = explode(",", Yii::$app->request->post()['asset_category_id']);
    
                    $model->category_id = $parts[0];
                    $model->category_name = $parts[1];
                }
                $model->depreciation = Yii::$app->request->post()['depreciation'];
                $model->brand = Yii::$app->request->post()['brand'];
            }elseif( Yii::$app->request->post()['transaction_type'] == "consumable" || Yii::$app->request->post()['transaction_type'] == "service"  )
            {
                $model->asset_name = Yii::$app->request->post()['expense_name'];
                if( isset(Yii::$app->request->post()['expense_category_id']) )
                {
                    $parts = explode(",", Yii::$app->request->post()['expense_category_id']);
    
                    $model->category_id = $parts[0];
                    $model->category_name = $parts[1];
                }
                $model->depreciation = NULL;
                $model->brand = Yii::$app->request->post()['brand'];
            }

            $model->status = Yii::$app->request->post()['status'];
            $model->save();
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = AssetManager::findOne(['id' => $id])->delete();

        return $this->redirect(['index']);
    }

    public function actionRegister($id = null)
    {

        require_once( __DIR__ .'/../components/tcpdf/AssetRegister.php');
        // create new PDF document
        $pdf = new \AssetRegister(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetSubject('AssetRegister');

        // set default header data
        // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(10, 35, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('times', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P','A4');

        $qpdf=Yii::$app->controller->renderPartial('/asset-manager/register_pdf',[

        ]);

        $pdf->writeHTML($qpdf, true, false, false, false, '');
        $pdf->Output('Register-'.$model->reference_number, 'I');
        exit;

    }
}
