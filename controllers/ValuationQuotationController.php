<?php

namespace app\controllers;

use app\models\QuotationFeeMasterFile;
use Yii;
use app\models\ValuationQuotation;
use app\models\ProposalMasterFile;
use app\models\ProposalStandardReport;
use app\models\ProposalBcsReport;
use app\models\ProposalPmeReport;
use app\models\ValuationQuotationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Buildings;
use app\models\ValuationConflict;
use app\models\Company;
use app\models\Valuation;
use app\models\ValuationForQuotation;
use app\models\ValuationOwners;
use app\models\PreviousTransactions;
use app\models\Properties;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\components\helpers\Utf8Helper;
use app\models\MediaManager;
use app\models\AssetCategory;
use app\components\helpers\DefController;

/**
 * ValuationQuotationController implements the CRUD actions for ValuationQuotation model.
 */
class ValuationQuotationController extends DefController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ValuationQuotation models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ValuationQuotationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new ValuationQuotation();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single ValuationQuotation model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $model->client->title;

        if ($model->client_name > 0) {
            //total valuations againts  selected customer in quotation from
            $numOfValuations = Valuation::find()->where(['client_id' => $model->client_name])->asArray()->all();
            //get ids of valuations (againts selected client)
            $selectedClientValuations = yii::$app->quotationHelperFunctions->getValuationOfSelectedClient($model->client_name);

            $related_to_buyer_check = PreviousTransactions::find()
                ->where(['client_name' => $model->client->title])
                ->all();

            $owners_in_valuation = ArrayHelper::map(\app\models\ValuationOwners::find()->where(['valuation_id' => $selectedClientValuations])->all(), 'id', 'name');

            $owners_valutions_names = ArrayHelper::map(\app\models\ValuationOwners::find()->where(['name' => $owners_in_valuation])->all(), 'id', 'name');

            $owners_previous_data = PreviousTransactions::find()->where(['client_name' => $owners_valutions_names])->all();

            $related_to_client_check = PreviousTransactions::find()->where(['client_name' => $model->client->title])->all();


            //get properties from clild table
            $properties = yii::$app->quotationHelperFunctions->getChildTableData($id);
            if ($properties != '') {
                $condition[] = 'or';
                foreach ($properties as $property) {
                    $condition[] = ['building_info' => $property['building'], 'unit_number' => $property['unit_number']];
                }

                $related_to_property_check = PreviousTransactions::find()->Where($condition)->all();
            }


            if ($model !== null) {
            } else {
                $model = new ValuationQuotation();
                if ($related_to_buyer_check <> null && count($related_to_buyer_check) > 0) {
                    $model->related_to_buyer = 'Yes';
                }
                if ($owners_previous_data <> null && count($owners_previous_data) > 0) {
                    $model->related_to_owner = 'Yes';
                }
                if ($related_to_client_check <> null && count($related_to_client_check) > 0) {
                    $model->related_to_client = 'Yes';
                }
                if ($related_to_property_check <> null && count($related_to_property_check) > 0) {
                    $model->related_to_property = 'Yes';
                }
            }
        }
        // echo $model->related_to_property;
        // die();

        return $this->render('view', [
            'model' => $model,
            'buyer_data' => $related_to_buyer_check,
            'seller_data' => $owners_previous_data,
            'client_data' => $related_to_client_check,
            'property_data' => $related_to_property_check,
        ]);
    }

    /**
     * Creates a new ValuationQuotation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ValuationQuotation();
        $model->reference_number = Yii::$app->crmQuotationHelperFunctions->uniqueReference;
        $model->inquiry_date = date("Y-m-d");
        $model->expiry_date = Date('y:m:d', strtotime('+14 days'));
        $model->valuer_name = 21;
        if ($model->save()) {
            // Yii::$app->getSession()->addFlash('success', Yii::t('app','Upload Successfully'));
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    // if(count($error)>0){
                    foreach ($error as $key => $val) {
                        Yii::$app->getSession()->addFlash('error', $val);
                    }
                    // }
                }
            }
        }
    }

    /**
     * Updates an existing ValuationQuotation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->client->title;
        $model->scenario = 'clientDetails';
        $old_numberOfProperties = $model->no_of_properties;

        // echo $old_numberOfProperties.','.$model->no_of_properties;die(); 

        if ($model->client_name > 0) {
            // echo "client conflict";
            // die();
            //total valuations againts  selected customer in quotation from
            $numOfValuations = Valuation::find()->where(['client_id' => $model->client_name])->asArray()->all();
            //get ids of valuations (againts selected client)
            $selectedClientValuations = yii::$app->quotationHelperFunctions->getValuationOfSelectedClient($model->client_name);

            $related_to_buyer_check = PreviousTransactions::find()
                ->where(['client_name' => $model->client_customer_name])
                ->all();
            // echo "<pre>";
            // print_r($related_to_buyer_check);
            // echo "</pre>";
            // die();

            $owners_in_valuation = ArrayHelper::map(\app\models\ValuationOwners::find()->where(['valuation_id' => $selectedClientValuations])->all(), 'id', 'name');

            $owners_valutions_names = ArrayHelper::map(\app\models\ValuationOwners::find()->where(['name' => $owners_in_valuation])->all(), 'id', 'name');

            $owners_previous_data = PreviousTransactions::find()->where(['client_name' => $owners_valutions_names])->all();
            // echo "<pre>";
            // print_r($owners_previous_data);
            // echo "</pre>";
            // die();
            $related_to_client_check = PreviousTransactions::find()->where(['client_name' => $model->client->title])->all();
            // echo "<pre>";
            // print_r($related_to_client_check);
            // echo "</pre>";
            // die();


            //get properties from clild table
            $properties = yii::$app->quotationHelperFunctions->getChildTableData($id);
            if ($properties != '') {
                $condition[] = 'or';
                foreach ($properties as $property) {
                    $condition[] = ['building_info' => $property['building'], 'unit_number' => $property['unit_number']];
                }

                $related_to_property_check = PreviousTransactions::find()->Where($condition)->all();
            }

            // echo "<pre>";
            // print_r($related_to_client_check);
            // echo "</pre>";
            // die();


            if ($model !== null) {
                if ($related_to_buyer_check <> null && count($related_to_buyer_check) > 0) {
                    $model->related_to_buyer = 'Yes';
                }
                if ($owners_previous_data <> null && count($owners_previous_data) > 0) {
                    $model->related_to_owner = 'Yes';
                }
                if ($related_to_client_check <> null && count($related_to_client_check) > 0) {
                    $model->related_to_client = 'Yes';
                }
                if ($related_to_property_check <> null && count($related_to_property_check) > 0) {
                    $model->related_to_property = 'Yes';
                }
            } else {
                $model = new ValuationQuotation();
                if ($related_to_buyer_check <> null && count($related_to_buyer_check) > 0) {
                    $model->related_to_buyer = 'Yes';
                }
                if ($owners_previous_data <> null && count($owners_previous_data) > 0) {
                    $model->related_to_owner = 'Yes';
                }
                if ($related_to_client_check <> null && count($related_to_client_check) > 0) {
                    $model->related_to_client = 'Yes';
                }
                if ($related_to_property_check <> null && count($related_to_property_check) > 0) {
                    $model->related_to_property = 'Yes';
                }
            }

        }



        if ($model->load(Yii::$app->request->post())) {

            $num_of_properties = $model->no_of_properties;
            if ($num_of_properties == "other") {
                $model->no_of_properties = $model->other;
            }

            if ($model->no_of_properties > 0) {
                $model->scenario = 'update';
            }

            // if ($old_numberOfProperties>0 && $old_numberOfProperties!= $model->no_of_properties) {
            //     $model->calculate_all = true;
            // }

            if ($model->save()) {
                return $this->redirect(['update', 'id' => $model->id]);
            } else {
                // echo "updated";die();
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                //Yii::$app->getSession()->addFlash('error', $val);
                            }

                        }
                    }
                }
            }
        }
        // echo $model->related_to_buyer;
        // die();
        return $this->render('update', [
            'model' => $model,
            'buyer_data' => $related_to_buyer_check,
            'seller_data' => $owners_previous_data,
            'client_data' => $related_to_client_check,
            'property_data' => $related_to_property_check,
            // 'm_calc' => $m_calc,

        ]);
    }

    /**
     * Deletes an existing ValuationQuotation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ValuationQuotation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ValuationQuotation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ValuationQuotation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    // public function actionProperty($number)
    // {
    //   // echo "string";
    //   // die();
    //     return $this->renderAjax('subject_property_form', [
    //          'number' => $number,
    //     ]);
    // }

    public function actionDetail($id)
    {
        // echo "string";
        // die();
        $detail = Buildings::findOne($id);
        $detail->city = Yii::$app->appHelperFunctions->emiratedListArr[$detail->city];
        $detail->community = $detail->communities->title;
        $detail->sub_community = $detail->subCommunities->title;
        $valuation_approach = $detail->property->valuation_approach;
        $required_documents = $detail->property->required_documents;
        $explodeRequiredDocuments = (explode(",", $required_documents));

        return json_encode(array('detail' => $detail->attributes, 'valuation_approach' => $valuation_approach, 'required_documents' => $explodeRequiredDocuments));
    }



    public function actionClientDetail($id)
    {
        $client = Company::findOne($id);
        // echo "<pre>"; print_r($client->primaryContact); echo "</pre>"; die();
        $clientReference = $client->client_reference;
        $clientCustomerName = $client->primaryContact->firstname . ' ' . $client->primaryContact->lastname;
        $email = $client->primaryContact->email;
        $phoneNumber = $client->primaryContact->profileInfo->mobile;
        // echo "<pre>";
        // print_r($phoneNumber);
        // echo "</pre>";
        //  die();
        return json_encode(
            array(
                'clientReference' => $clientReference,
                'clientCustomerName' => $clientCustomerName,
                'email' => $email,
                'phoneNumber' => $phoneNumber
            )
        );
    }



    public function actionProposalStandardReport()
    {
        $model = ProposalStandardReport::findOne(1);
        $old_verify_status = $model->status_verified;

        if ($model->load(Yii::$app->request->post())) {
            $this->StatusVerify($model);

            if ($model->save()) {
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => 'app\models\ProposalStandardReport',
                    'action' => 'data_updated',
                    'verify_field' => 'status_verified',
                    'old_verify_status' => $old_verify_status,
                ]);

                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->refresh();
            }
        }
        return $this->render('standard_report', [
            'model' => $model,
        ]);

    }

    public function actionMasterFile()
    {
        // echo "you are in controller";
        // die();
        $model = new ProposalMasterFile();
        if ($model->load(Yii::$app->request->post())) {
            // echo "<pre>";
            // print_r($model);
            // echo "</pre>";
            // die();

            ProposalMasterFile::deleteAll();

            foreach ($model->client_type as $key1 => $client_type_data) {
                $forBank = new ProposalMasterFile();
                $forBank->heading = 'Client Type';
                $forBank->sub_heading = $key1;
                $forBank->values = $client_type_data;
                $forBank->save();
            }
            foreach ($model->subject_property as $key2 => $subject_property_data) {
                $forSubjectProperty = new ProposalMasterFile();
                $forSubjectProperty->heading = 'Subject Property';
                $forSubjectProperty->sub_heading = $key2;
                $forSubjectProperty->values = $subject_property_data;
                $forSubjectProperty->save();
            }
            foreach ($model->city as $key3 => $city_data) {
                $forCity = new ProposalMasterFile();
                $forCity->heading = 'City';
                $forCity->sub_heading = $key3;
                $forCity->values = $city_data;
                $forCity->save();
            }
            foreach ($model->number_of_properties as $key4 => $number_of_properties_data) {
                // print_r($model->number_of_properties);
                // die();
                $forNumOfProperties = new ProposalMasterFile();
                $forNumOfProperties->heading = 'Number of Properties';
                $forNumOfProperties->sub_heading = $key4;
                $forNumOfProperties->values = $number_of_properties_data;
                $forNumOfProperties->save();
                // if ($forNumOfProperties->save()) {
                //   echo "success";
                // }else {
                //   print_r($forNumOfProperties->getErrors());
                // }
                // die();
            }
            foreach ($model->payment_terms as $key5 => $payment_terms_data) {
                // print_r($key5.','.$payment_terms_data);
                // die();
                $forPaymentTerms = new ProposalMasterFile();
                $forPaymentTerms->heading = 'Payment Terms';
                $forPaymentTerms->sub_heading = $key5;
                $forPaymentTerms->values = $payment_terms_data;
                $forPaymentTerms->save();
            }
            foreach ($model->tenure as $key6 => $tenure_data) {
                $forTenure = new ProposalMasterFile();
                $forTenure->heading = 'Tenure';
                $forTenure->sub_heading = $key6;
                $forTenure->values = $tenure_data;
                $forTenure->save();
            }
            foreach ($model->complexity as $key7 => $complexity_data) {
                $forComplexity = new ProposalMasterFile();
                $forComplexity->heading = 'Complexity';
                $forComplexity->sub_heading = $key7;
                $forComplexity->values = $complexity_data;
                $forComplexity->save();
            }
            foreach ($model->new_repeat_valuation as $key8 => $new_repeat_valuation_data) {
                $forNewRepeatValuationData = new ProposalMasterFile();
                $forNewRepeatValuationData->heading = 'New Repeat Valuation Data';
                $forNewRepeatValuationData->sub_heading = $key8;
                $forNewRepeatValuationData->values = $new_repeat_valuation_data;
                $forNewRepeatValuationData->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key9 => $built_up_area_of_subject_property_data) {
                $forNewRepeatValuationData = new ProposalMasterFile();
                $forNewRepeatValuationData->heading = 'Build up Area of Subject Property';
                $forNewRepeatValuationData->sub_heading = $key9;
                $forNewRepeatValuationData->values = $built_up_area_of_subject_property_data;
                $forNewRepeatValuationData->save();
            }
            foreach ($model->number_of_units_land as $key10 => $number_of_units_land_data) {
                $forNumOfUnits = new ProposalMasterFile();
                $forNumOfUnits->heading = 'Number of Units Land';
                $forNumOfUnits->sub_heading = $key10;
                $forNumOfUnits->values = $number_of_units_land_data;
                $forNumOfUnits->save();
            }
            foreach ($model->type_of_valuation as $key11 => $type_of_valuation_data) {
                $forTypeOfValuation = new ProposalMasterFile();
                $forTypeOfValuation->heading = 'Type of Valuation';
                $forTypeOfValuation->sub_heading = $key11;
                $forTypeOfValuation->values = $type_of_valuation_data;
                $forTypeOfValuation->save();
            }
            foreach ($model->number_of_comparables as $key12 => $number_of_comparables_data) {
                $forNumberOfComparablesData = new ProposalMasterFile();
                $forNumberOfComparablesData->heading = 'Number of Comparables Data';
                $forNumberOfComparablesData->sub_heading = $key12;
                $forNumberOfComparablesData->values = $number_of_comparables_data;
                $forNumberOfComparablesData->save();

                // if ($forNumberOfComparablesData->save()) {
                //   echo "success";
                // }else {
                //   print_r($forNumberOfComparablesData->getErrors());
                // }
                // die();
            }
            foreach ($model->valuation_approach as $key13 => $valuation_approach_data) {
                $forValuationApproach = new ProposalMasterFile();
                $forValuationApproach->heading = 'Valuation Approach';
                $forValuationApproach->sub_heading = $key13;
                $forValuationApproach->values = $valuation_approach_data;
                $forValuationApproach->save();
            }
            foreach ($model->base_fee_building as $key14 => $base_fee_data) {
                $forBaseFee = new ProposalMasterFile();
                $forBaseFee->heading = 'Base Fee Building';
                $forBaseFee->sub_heading = $key14;
                $forBaseFee->values = $base_fee_data;
                $forBaseFee->save();
            }
            foreach ($model->base_fee_others as $key15 => $base_fee_others) {
                // echo $key15; die();
                $forBaseFee = new ProposalMasterFile();
                $forBaseFee->heading = 'Base Fee Others';
                $forBaseFee->sub_heading = $key15;
                $forBaseFee->values = $base_fee_others;
                $forBaseFee->save();
            }
            foreach ($model->no_of_property_dis as $key16 => $no_of_property_dis) {
                // echo $key16; die();
                $forBaseFee = new ProposalMasterFile();
                $forBaseFee->heading = 'No Of Property Discount';
                $forBaseFee->sub_heading = $key16;
                $forBaseFee->values = $no_of_property_dis;
                $forBaseFee->save();
            }
            foreach ($model->number_of_units_building as $key17 => $number_of_units_building) {
                // echo $key17; die();
                $forBaseFee = new ProposalMasterFile();
                $forBaseFee->heading = 'No Of Units Building';
                $forBaseFee->sub_heading = $key17;
                $forBaseFee->values = $number_of_units_building;
                $forBaseFee->save();
            }
            foreach ($model->land as $key18 => $land) {
                // echo $key18; die();
                $forBaseFee = new ProposalMasterFile();
                $forBaseFee->heading = 'Land';
                $forBaseFee->sub_heading = $key18;
                $forBaseFee->values = $land;
                $forBaseFee->save();
            }


            // foreach ($model->relative_discount as $key15 => $relative_discount_data) {
            //     $forRelativeDiscountData = new ProposalMasterFile();
            //     $forRelativeDiscountData->heading = 'Relative Discount Data';
            //     $forRelativeDiscountData->sub_heading = $key15;
            //     $forRelativeDiscountData->values = $relative_discount_data;
            //     $forRelativeDiscountData->save();
            // }
            foreach ($model->first_time_discount as $key15 => $first_time_data) {
                $forBaseFee = new ProposalMasterFile();
                $forBaseFee->heading = 'First Time Discount';
                $forBaseFee->sub_heading = $key15;
                $forBaseFee->values = $first_time_data;
                $forBaseFee->save();
            }
            foreach ($model->general_discount as $key16 => $general_discount_data) {
                $forBaseFee = new ProposalMasterFile();
                $forBaseFee->heading = 'General Discount';
                $forBaseFee->sub_heading = $key16;
                $forBaseFee->values = $general_discount_data;
                $forBaseFee->save();
            }
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        return $this->render('master_file', [
            'model' => $model,
        ]);
    }



    public function assignVerification($model, $status)
    {
        if ($status == 1) {
            $model->status_verified = $status;
            $model->status_verified_at = date('Y-m-d H:i:s');
            $model->status_verified_by = Yii::$app->user->identity->id;
        }
    }

    //market
    public function actionMasterFileMarket()
    {
        $model = new QuotationFeeMasterFile();

        //verification process
        $file_type = 'market';
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['approach_type' => $file_type])->scalar();
        $model->status_verified = $old_verify_status;


        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['approach_type' => $file_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['approach_type' => $model->approach_type, 'property_type' => null])
                ->execute();

            foreach ($model->client_type as $key1 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key1;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->subject_property as $key2 => $subject_property_data) {
                $subject_property = new QuotationFeeMasterFile();
                $subject_property->heading = 'subject_property';
                $subject_property->sub_heading = $key2;
                $subject_property->values = $subject_property_data;
                $subject_property->approach_type = $model->approach_type;
                $this->assignVerification($subject_property, $model->status_verified);
                $subject_property->save();
            }
            foreach ($model->city as $key3 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key3;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->number_of_properties as $key4 => $number_of_properties_data) {
                // print_r($model->number_of_properties);
                // die();
                $number_of_properties = new QuotationFeeMasterFile();
                $number_of_properties->heading = 'number_of_properties';
                $number_of_properties->sub_heading = $key4;
                $number_of_properties->values = $number_of_properties_data;
                $number_of_properties->approach_type = $model->approach_type;
                $this->assignVerification($number_of_properties, $model->status_verified);
                $number_of_properties->save();
                // if ($forNumOfProperties->save()) {
                //   echo "success";
                // }else {
                //   print_r($forNumOfProperties->getErrors());
                // }
                // die();
            }
            foreach ($model->payment_terms as $key5 => $payment_terms_data) {
                // print_r($key5.','.$payment_terms_data);
                // die();
                $payment_terms = new QuotationFeeMasterFile();
                $payment_terms->heading = 'payment_terms';
                $payment_terms->sub_heading = $key5;
                $payment_terms->values = $payment_terms_data;
                $payment_terms->approach_type = $model->approach_type;
                $this->assignVerification($payment_terms, $model->status_verified);
                $payment_terms->save();
            }
            foreach ($model->tenure as $key6 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key6;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key7 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key7;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->new_repeat_valuation as $key8 => $new_repeat_valuation_data) {

                $new_repeat_valuation = new QuotationFeeMasterFile();
                $new_repeat_valuation->heading = 'new_repeat_valuation_data';
                $new_repeat_valuation->sub_heading = $key8;
                $new_repeat_valuation->values = $new_repeat_valuation_data;
                $new_repeat_valuation->approach_type = $model->approach_type;
                $this->assignVerification($new_repeat_valuation, $model->status_verified);

                if (!$new_repeat_valuation->save()) {
                    echo "<pre>";
                    print_r($new_repeat_valuation_data->errors);
                    die;
                }


            }

            foreach ($model->built_up_area_of_subject_property as $key9 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key9;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }

            foreach ($model->built_up_area_of_subject_property_b_land as $key9_b_land => $built_up_area_of_subject_property_b_land_data) {
                $build_up_area_of_subject_property_b_land = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property_b_land->heading = 'built_up_area_of_subject_property_b_land';
                $build_up_area_of_subject_property_b_land->sub_heading = $key9_b_land;
                $build_up_area_of_subject_property_b_land->values = $built_up_area_of_subject_property_b_land_data;
                $build_up_area_of_subject_property_b_land->approach_type = $model->approach_type;
                $this->assignVerification($build_up_area_of_subject_property_b_land, $model->status_verified);
                $build_up_area_of_subject_property_b_land->save();
            }

            foreach ($model->other_intended_users as $key9_other_users => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key9_other_users;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->upgrades_ratings as $key10 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key10;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key11 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key11;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            foreach ($model->number_of_comparables as $key12 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key12;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();

            }
            foreach ($model->valuation_approach as $key13 => $valuation_approach_data) {
                $valuation_approach = new QuotationFeeMasterFile();
                $valuation_approach->heading = 'valuation_approach';
                $valuation_approach->sub_heading = $key13;
                $valuation_approach->values = $valuation_approach_data;
                $valuation_approach->approach_type = $model->approach_type;
                $this->assignVerification($valuation_approach, $model->status_verified);
                $valuation_approach->save();
            }

            foreach ($model->no_of_property_discount as $key14 => $no_of_property_dis) {
                // echo $key16; die();
                $no_of_property_discount = new QuotationFeeMasterFile();
                $no_of_property_discount->heading = 'no_of_property_discount';
                $no_of_property_discount->sub_heading = $key14;
                $no_of_property_discount->values = $no_of_property_dis;
                $no_of_property_discount->approach_type = $model->approach_type;
                $this->assignVerification($no_of_property_discount, $model->status_verified);
                $no_of_property_discount->save();
            }

            foreach ($model->land as $key18 => $land_data) {
                // echo $key18; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key18;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }

            foreach ($model->first_time_discount as $key15 => $first_time_data) {
                $first_time_discount = new QuotationFeeMasterFile();
                $first_time_discount->heading = 'first_time_discount';
                $first_time_discount->sub_heading = $key15;
                $first_time_discount->values = $first_time_data;
                $first_time_discount->approach_type = $model->approach_type;
                $this->assignVerification($first_time_discount, $model->status_verified);
                $first_time_discount->save();
            }


            foreach ($model->no_of_units_same_building_discount as $key16 => $no_of_units_same_building_discount_data) {
                $no_of_units_same_building_discount = new QuotationFeeMasterFile();
                $no_of_units_same_building_discount->heading = 'no_of_units_same_building_discount';
                $no_of_units_same_building_discount->sub_heading = $key16;
                $no_of_units_same_building_discount->values = $no_of_units_same_building_discount_data;
                $no_of_units_same_building_discount->approach_type = $model->approach_type;
                $this->assignVerification($no_of_units_same_building_discount, $model->status_verified);
                $no_of_units_same_building_discount->save();
            }

            foreach ($model->urgency_fee as $key17 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key17;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }

            foreach ($model->last_three_years_finance as $key17 => $last_three_years_finance_data) {
                $last_three_years_finance = new QuotationFeeMasterFile();
                $last_three_years_finance->heading = 'last_three_years_finance';
                $last_three_years_finance->sub_heading = $key17;
                $last_three_years_finance->values = $last_three_years_finance_data;
                $last_three_years_finance->approach_type = $model->approach_type;
                $this->assignVerification($last_three_years_finance, $model->status_verified);
                $last_three_years_finance->save();
            }


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $file_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_market';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //income
    public function actionMasterFileIncome()
    {
        $model = new QuotationFeeMasterFile();
        //verification process by usama
        $file_type = 'income';
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['approach_type' => $file_type])->scalar();
        $model->status_verified = $old_verify_status;
        //end by usama
        if ($model->load(Yii::$app->request->post())) {


            if (QuotationFeeMasterFile::find()->where(['approach_type' => $file_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }


            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['approach_type' => $model->approach_type, 'property_type' => null])
                ->execute();

            foreach ($model->client_type as $key1 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key1;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $this->assignVerification($client_type, $model->status_verified);


                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->subject_property as $key2 => $subject_property_data) {
                $subject_property = new QuotationFeeMasterFile();
                $subject_property->heading = 'subject_property';
                $subject_property->sub_heading = $key2;
                $subject_property->values = $subject_property_data;
                $subject_property->approach_type = $model->approach_type;
                $this->assignVerification($subject_property, $model->status_verified);
                $subject_property->save();
            }
            foreach ($model->city as $key3 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key3;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->number_of_properties as $key4 => $number_of_properties_data) {
                // print_r($model->number_of_properties);
                // die();
                $number_of_properties = new QuotationFeeMasterFile();
                $number_of_properties->heading = 'number_of_properties';
                $number_of_properties->sub_heading = $key4;
                $number_of_properties->values = $number_of_properties_data;
                $number_of_properties->approach_type = $model->approach_type;
                $this->assignVerification($number_of_properties, $model->status_verified);
                $number_of_properties->save();
                // if ($forNumOfProperties->save()) {
                //   echo "success";
                // }else {
                //   print_r($forNumOfProperties->getErrors());
                // }
                // die();
            }
            foreach ($model->payment_terms as $key5 => $payment_terms_data) {
                // print_r($key5.','.$payment_terms_data);
                // die();
                $payment_terms = new QuotationFeeMasterFile();
                $payment_terms->heading = 'payment_terms';
                $payment_terms->sub_heading = $key5;
                $payment_terms->values = $payment_terms_data;
                $payment_terms->approach_type = $model->approach_type;
                $this->assignVerification($payment_terms, $model->status_verified);
                $payment_terms->save();
            }

            foreach ($model->number_of_units_building as $key6 => $number_of_units_building_data) {
                // echo $key17; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key6;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->tenure as $key6tn => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key6tn;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key7 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key7;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->new_repeat_valuation as $key8 => $new_repeat_valuation_data) {

                $new_repeat_valuation = new QuotationFeeMasterFile();
                $new_repeat_valuation->heading = 'new_repeat_valuation_data';
                $new_repeat_valuation->sub_heading = $key8;
                $new_repeat_valuation->values = $new_repeat_valuation_data;
                $new_repeat_valuation->approach_type = $model->approach_type;
                $this->assignVerification($new_repeat_valuation, $model->status_verified);

                if (!$new_repeat_valuation->save()) {
                    echo "<pre>";
                    print_r($new_repeat_valuation_data->errors);
                    die;
                }


            }
            foreach ($model->land as $key18 => $land_data) {
                // echo $key18; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key18;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->number_of_types as $key18types => $land_data) {
                // echo $key18; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'number_of_types';
                $land->sub_heading = $key18types;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }


            foreach ($model->built_up_area_of_subject_property as $key9 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key9;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->other_intended_users as $key9_other_users => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key9_other_users;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->upgrades_ratings as $key10 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key10;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key11 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key11;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }

            foreach ($model->number_of_comparables as $key12 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key12;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();

            }
            foreach ($model->valuation_approach as $key13 => $valuation_approach_data) {
                $valuation_approach = new QuotationFeeMasterFile();
                $valuation_approach->heading = 'valuation_approach';
                $valuation_approach->sub_heading = $key13;
                $valuation_approach->values = $valuation_approach_data;
                $valuation_approach->approach_type = $model->approach_type;
                $this->assignVerification($valuation_approach, $model->status_verified);
                $valuation_approach->save();
            }

            foreach ($model->no_of_property_discount as $key14 => $no_of_property_dis) {
                // echo $key16; die();
                $no_of_property_discount = new QuotationFeeMasterFile();
                $no_of_property_discount->heading = 'no_of_property_discount';
                $no_of_property_discount->sub_heading = $key14;
                $no_of_property_discount->values = $no_of_property_dis;
                $no_of_property_discount->approach_type = $model->approach_type;
                $this->assignVerification($no_of_property_discount, $model->status_verified);
                $no_of_property_discount->save();
            }

            foreach ($model->land as $key18 => $land_data) {
                // echo $key18; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key18;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }

            foreach ($model->first_time_discount as $key15 => $first_time_data) {
                $first_time_discount = new QuotationFeeMasterFile();
                $first_time_discount->heading = 'first_time_discount';
                $first_time_discount->sub_heading = $key15;
                $first_time_discount->values = $first_time_data;
                $first_time_discount->approach_type = $model->approach_type;
                $this->assignVerification($first_time_discount, $model->status_verified);
                $first_time_discount->save();
            }


            foreach ($model->no_of_units_same_building_discount as $key16 => $no_of_units_same_building_discount_data) {
                $no_of_units_same_building_discount = new QuotationFeeMasterFile();
                $no_of_units_same_building_discount->heading = 'no_of_units_same_building_discount';
                $no_of_units_same_building_discount->sub_heading = $key16;
                $no_of_units_same_building_discount->values = $no_of_units_same_building_discount_data;
                $no_of_units_same_building_discount->approach_type = $model->approach_type;
                $this->assignVerification($no_of_units_same_building_discount, $model->status_verified);
                $no_of_units_same_building_discount->save();
            }


            foreach ($model->urgency_fee as $key17 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key17;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $file_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }


        $view = 'master_file_income';



        return $this->render($view, [
            'model' => $model,
        ]);
    }

    //profit
    public function actionMasterFileProfit()
    {
        $model = new QuotationFeeMasterFile();

        //by usama   
        $file_type = 'profit';
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['approach_type' => $file_type])->scalar();
        // dd($old_verify_status);
        $model->status_verified = $old_verify_status;
        //end by usama

        if ($model->load(Yii::$app->request->post())) {

            // dd($model->status_verified);

            if (QuotationFeeMasterFile::find()->where(['approach_type' => $file_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }


            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['approach_type' => $model->approach_type, 'property_type' => null])
                ->execute();

            foreach ($model->client_type as $key1 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key1;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $this->assignVerification($client_type, $model->status_verified);


                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->subject_property as $key2 => $subject_property_data) {
                $subject_property = new QuotationFeeMasterFile();
                $subject_property->heading = 'subject_property';
                $subject_property->sub_heading = $key2;
                $subject_property->values = $subject_property_data;
                $subject_property->approach_type = $model->approach_type;
                $this->assignVerification($subject_property, $model->status_verified);
                $subject_property->save();
            }
            foreach ($model->city as $key3 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key3;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->number_of_properties as $key4 => $number_of_properties_data) {
                // print_r($model->number_of_properties);
                // die();
                $number_of_properties = new QuotationFeeMasterFile();
                $number_of_properties->heading = 'number_of_properties';
                $number_of_properties->sub_heading = $key4;
                $number_of_properties->values = $number_of_properties_data;
                $number_of_properties->approach_type = $model->approach_type;
                $this->assignVerification($number_of_properties, $model->status_verified);
                $number_of_properties->save();
                // if ($forNumOfProperties->save()) {
                //   echo "success";
                // }else {
                //   print_r($forNumOfProperties->getErrors());
                // }
                // die();
            }
            foreach ($model->payment_terms as $key5 => $payment_terms_data) {
                // print_r($key5.','.$payment_terms_data);
                // die();
                $payment_terms = new QuotationFeeMasterFile();
                $payment_terms->heading = 'payment_terms';
                $payment_terms->sub_heading = $key5;
                $payment_terms->values = $payment_terms_data;
                $payment_terms->approach_type = $model->approach_type;
                $this->assignVerification($payment_terms, $model->status_verified);
                $payment_terms->save();
            }

            foreach ($model->number_of_rooms_building as $key6 => $number_of_units_building_data) {
                // echo $key17; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_rooms_building';
                $number_of_units_building->sub_heading = $key6;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->complexity as $key7 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key7;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->new_repeat_valuation as $key8 => $new_repeat_valuation_data) {

                $new_repeat_valuation = new QuotationFeeMasterFile();
                $new_repeat_valuation->heading = 'new_repeat_valuation_data';
                $new_repeat_valuation->sub_heading = $key8;
                $new_repeat_valuation->values = $new_repeat_valuation_data;
                $new_repeat_valuation->approach_type = $model->approach_type;
                $this->assignVerification($new_repeat_valuation, $model->status_verified);

                if (!$new_repeat_valuation->save()) {
                    echo "<pre>";
                    print_r($new_repeat_valuation_data->errors);
                    die;
                }


            }

            foreach ($model->built_up_area_of_subject_property as $key9 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key9;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->upgrades_ratings as $key10 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key10;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key11 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key11;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            foreach ($model->number_of_comparables as $key12 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key12;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();

            }
            foreach ($model->valuation_approach as $key13 => $valuation_approach_data) {
                $valuation_approach = new QuotationFeeMasterFile();
                $valuation_approach->heading = 'valuation_approach';
                $valuation_approach->sub_heading = $key13;
                $valuation_approach->values = $valuation_approach_data;
                $valuation_approach->approach_type = $model->approach_type;
                $this->assignVerification($valuation_approach, $model->status_verified);
                $valuation_approach->save();
            }

            foreach ($model->no_of_property_discount as $key14 => $no_of_property_dis) {
                // echo $key16; die();
                $no_of_property_discount = new QuotationFeeMasterFile();
                $no_of_property_discount->heading = 'no_of_property_discount';
                $no_of_property_discount->sub_heading = $key14;
                $no_of_property_discount->values = $no_of_property_dis;
                $no_of_property_discount->approach_type = $model->approach_type;
                $this->assignVerification($no_of_property_discount, $model->status_verified);
                $no_of_property_discount->save();
            }

            foreach ($model->land as $key18 => $land_data) {
                // echo $key18; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key18;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->number_of_types as $key18types => $land_data) {
                // echo $key18; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'number_of_types';
                $land->sub_heading = $key18types;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }

            foreach ($model->restaurant as $key18restaurant => $restaurant_data) {
                // echo $key18; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'restaurant';
                $land->sub_heading = $key18restaurant;
                $land->values = $restaurant_data;
                $land->approach_type = $model->approach_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }

            foreach ($model->ballrooms as $key18ballrooms => $ballrooms_data) {
                // echo $key18; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'ballrooms';
                $land->sub_heading = $key18ballrooms;
                $land->values = $ballrooms_data;
                $land->approach_type = $model->approach_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }

            foreach ($model->atms as $key18atms => $atms_data) {
                // echo $key18; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'atms';
                $land->sub_heading = $key18atms;
                $land->values = $atms_data;
                $land->approach_type = $model->approach_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->retails_units as $key18retails_units => $retails_units_data) {
                // echo $key18; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'retails_units';
                $land->sub_heading = $key18retails_units;
                $land->values = $retails_units_data;
                $land->approach_type = $model->approach_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }

            foreach ($model->retails_units as $key18retails_units => $retails_units_data) {
                // echo $key18; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'retails_units';
                $land->sub_heading = $key18retails_units;
                $land->values = $retails_units_data;
                $land->approach_type = $model->approach_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }

            foreach ($model->retails_units as $key18retails_units => $retails_units_data) {
                // echo $key18; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'retails_units';
                $land->sub_heading = $key18retails_units;
                $land->values = $retails_units_data;
                $land->approach_type = $model->approach_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }

            foreach ($model->bars as $key18bars => $bars_data) {
                // echo $key18; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'bars';
                $land->sub_heading = $key18bars;
                $land->values = $bars_data;
                $land->approach_type = $model->approach_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->health_club as $key18health_club => $health_club_data) {
                // echo $key18; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'health_club';
                $land->sub_heading = $key18health_club;
                $land->values = $health_club_data;
                $land->approach_type = $model->approach_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }

            foreach ($model->meeting_rooms as $key18meeting_rooms => $meeting_rooms_data) {
                // echo $key18; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'meeting_rooms';
                $land->sub_heading = $key18meeting_rooms;
                $land->values = $meeting_rooms_data;
                $land->approach_type = $model->approach_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->spa as $key18spa => $spa_data) {
                // echo $key18; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'spa';
                $land->sub_heading = $key18spa;
                $land->values = $spa_data;
                $land->approach_type = $model->approach_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }

            foreach ($model->beach_access as $key18beach_access => $beach_access_data) {
                // echo $key18; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'beach_access';
                $land->sub_heading = $key18beach_access;
                $land->values = $beach_access_data;
                $land->approach_type = $model->approach_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->parking_sale as $key18parking_sale => $parking_sale_data) {
                // echo $key18; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'parking_sale';
                $land->sub_heading = $key18parking_sale;
                $land->values = $parking_sale_data;
                $land->approach_type = $model->approach_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->night_clubs as $key18night_clubs => $night_clubs_data) {
                // echo $key18; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'night_clubs';
                $land->sub_heading = $key18night_clubs;
                $land->values = $night_clubs_data;
                $land->approach_type = $model->approach_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->other_intended_users as $key9_other_users => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key9_other_users;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }

            foreach ($model->first_time_discount as $key15 => $first_time_data) {
                $first_time_discount = new QuotationFeeMasterFile();
                $first_time_discount->heading = 'first_time_discount';
                $first_time_discount->sub_heading = $key15;
                $first_time_discount->values = $first_time_data;
                $first_time_discount->approach_type = $model->approach_type;
                $this->assignVerification($first_time_discount, $model->status_verified);
                $first_time_discount->save();
            }


            foreach ($model->no_of_units_same_building_discount as $key16 => $no_of_units_same_building_discount_data) {
                $no_of_units_same_building_discount = new QuotationFeeMasterFile();
                $no_of_units_same_building_discount->heading = 'no_of_units_same_building_discount';
                $no_of_units_same_building_discount->sub_heading = $key16;
                $no_of_units_same_building_discount->values = $no_of_units_same_building_discount_data;
                $no_of_units_same_building_discount->approach_type = $model->approach_type;
                $this->assignVerification($no_of_units_same_building_discount, $model->status_verified);
                $no_of_units_same_building_discount->save();
            }

            foreach ($model->urgency_fee as $key17 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key17;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }

            foreach ($model->last_three_years_finance as $key18 => $last_three_years_finance_data) {
                $last_three_years_finance = new QuotationFeeMasterFile();
                $last_three_years_finance->heading = 'last_three_years_finance';
                $last_three_years_finance->sub_heading = $key18;
                $last_three_years_finance->values = $last_three_years_finance_data;
                $last_three_years_finance->approach_type = $model->approach_type;
                $this->assignVerification($last_three_years_finance, $model->status_verified);
                $last_three_years_finance->save();
            }

            foreach ($model->ten_years_projections as $key19 => $ten_years_projections_data) {
                $last_three_years_finance = new QuotationFeeMasterFile();
                $last_three_years_finance->heading = 'ten_years_projections';
                $last_three_years_finance->sub_heading = $key19;
                $last_three_years_finance->values = $ten_years_projections_data;
                $last_three_years_finance->approach_type = $model->approach_type;
                $this->assignVerification($last_three_years_finance, $model->status_verified);
                $last_three_years_finance->save();
            }


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $file_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }


        $view = 'master_file_profit';



        return $this->render($view, [
            'model' => $model,
        ]);
    }

    public function actionDiscount()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $post = Yii::$app->request->post();

        $totalFee = yii::$app->quotationHelperFunctions->getTotalFee($post['QrecommendedFee'], $post['relative_discount']);
        $totalFeeInt = (float) $totalFee;
        return $totalFeeInt;
    }



    public function actionQpdf($id)
    {
        $model = ValuationQuotation::findOne($id);
        $condition = true;
        $model->quotationPdf($condition);
    }


    public function actionToe($id)
    {
        $model = ValuationQuotation::findOne($id);
        $condition = true;
        $model->toePdf($condition);
    }


    public function actionSendQuotation($id)
    {
        // echo "string";die();
        $model = ValuationQuotation::find()->where(['id' => $id])->one();
        // print_r($model);die();
        if ($model != null) {
            $condition = false;
            $model->quotationPdf($condition);

            $model->quotation_status = 1;
            // $model->toe_final_fee = $model->quotation_recommended_fee;
            // $model->toe_final_turned_around_time = $model->quotation_turn_around_time;

            if ($model->save()) {
                yii::$app->getsession()->addFlash('success', yii::t('app', 'Quotation Send successfully'));
                return $this->redirect(['update', 'id' => $model->id]);

            } else {
                if ($child->hasErrors()) {
                    foreach ($child->getErrors() as $errors) {

                        foreach ($errors as $key => $val) {
                            //die($val);
                            yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

    }

    public function actionSendToe($id)
    {
        $model = ValuationQuotation::find()->where(['id' => $id])->one(); //done
        $properties = yii::$app->quotationHelperFunctions->getMultipleProperties($id); //done
        if ($properties != null) {
            // echo "<pre>";
            // print_r($properties);
            // echo "</pre>";
            // die();
            foreach ($properties as $value) {
                // print_r($model->building_number);
                // die();
                $result = Valuation::find()->
                    where([
                        'quotation_id' => $model->id,
                        'client_id' => $model->client_name,
                        'building_info' => $value->building,
                        'property_id' => $value->property,
                        'property_category' => $value->property_category
                    ])->
                    one();
                // echo "<pre>";
                // print_r($result);
                // echo "</pre>";
                // die();
                if ($result == null) {

                    $valuation = new ValuationForQuotation;
                    $valuation->quotation_id = $model->id;
                    $valuation->reference_number = Yii::$app->appHelperFunctions->uniqueReference;
                    $valuation->client_id = $model->client_name;
                    $valuation->purpose_of_valuation = $model->purpose_of_valuation;
                    $valuation->building_info = $value->building;
                    $valuation->property_id = $value->property;
                    $valuation->property_category = $value->property_category;
                    $valuation->community = $value->community;
                    $valuation->sub_community = $value->sub_community;
                    $valuation->tenure = $value->tenure;
                    $valuation->unit_number = $value->unit_number;
                    $valuation->city = $value->city;
                    $valuation->building_number = $value->building_number;
                    $valuation->plot_number = $value->plot_number;
                    $valuation->street = $value->street;
                    $valuation->floor_number = $value->floor_number;
                    // $valuation->land_size = $value->land_size;
                    $valuation->valuation_status = 1;
                    if ($valuation->save()) {
                        // echo "Saved ".$valuation->id.'<br>';
                        yii::$app->getsession()->addFlash('success', yii::t('app', 'Uploaded successfully'));
                    } else {
                        if ($valuation->hasErrors()) {
                            foreach ($valuation->getErrors() as $errors) {

                                foreach ($errors as $key => $val) {
                                    //die($val);
                                    yii::$app->getSession()->addFlash('error', $val);
                                    echo $val . '</br>';
                                }
                            }
                        }
                    }
                }

            }
        }

        $model->quotation_status = 2;
        $condition = false;
        $model->toePdf($condition);
        if ($model->save()) {
            yii::$app->getsession()->addFlash('success', yii::t('app', 'TOE send successfully'));
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            if ($child->hasErrors()) {
                foreach ($child->getErrors() as $errors) {

                    foreach ($errors as $key => $val) {
                        //die($val);
                        yii::$app->getSession()->addFlash('error', $val);
                    }
                }
            }
        }

    }



    public function actionPaymentDelete($id)
    {
        $model = $this->findModel($id);
        if ($id != null) {
            // delete payment_slip  when user select delete
            // $url = Yii::$app->get('s3bucket')->getUrl($model->payment_slip);
            // $url->delete();
            // $model->payment_slip = '';
            $model->save();

        }
    }
    public function actionToeDelete($id)
    {
        $model = $this->findModel($id);
        if ($id != null) {
            // delete toe_document when user select delete
            // $url = Yii::$app->get('s3bucket')->getUrl($model->toe_document);
            // $url->delete();
            // $model->toe_document = '';
            $model->save();

        }
    }

    public function actionDocuments()
    {
        $post = Yii::$app->request->post();
        // echo "<pre>";
        // print_r($post);
        // echo "</pre>";
        // die();
        $model = new ValuationQuotation;
        return $this->renderAjax('document_html', ['property_id' => $post['property_id'], 'property_index' => $post['index']]);
    }

    public function actionUpload()
    {
        $json = [];
        $post = Yii::$app->request->post();
        // echo "<pre>";
        // print_r($_FILES);
        // echo "</pre>";
        // die();

        if (!$json) {
            $file = \yii\web\UploadedFile::getInstanceByName('file');

            if (!empty($file->name)) {
                // Sanitize the filename
                $filename = basename(html_entity_decode($file->name, ENT_QUOTES, 'UTF-8'));
                $extension = $file->extension;
                $name = $file->baseName;

                // Validate the filename length
                if ((Utf8Helper::utf8_strlen($filename) < 3) || (Utf8Helper::utf8_strlen($filename) > 255)) {
                    $json['error'] = Yii::$app->params['error_filename'];
                }

                // Allowed file extension types
                $allowed = [
                    'jpg',
                    'jpeg',
                    'gif',
                    'png',
                    'pdf',
                    'doc',
                    'docx',
                    'xlsx',
                    'xls'
                ];

                if (!in_array(Utf8Helper::utf8_strtolower(Utf8Helper::utf8_substr(strrchr($filename, '.'), 1)), $allowed)) {
                    $json['error'] = Yii::$app->params['error_filetype'];
                }

                // Allowed file mime types
                $allowed = [
                    'image/jpeg',
                    'image/pjpeg',
                    'image/png',
                    'image/x-png',
                    'image/gif',
                    'application/pdf',
                    'application/msword',
                    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                    'application/vnd.ms-excel'
                ];

                if (!in_array($file->type, $allowed)) {
                    $json['error'] = Yii::$app->params['error_filetype'];
                }

                // Return any upload error
                if ($file->error != UPLOAD_ERR_OK) {
                    $json['error'] = Yii::$app->params['error_upload' . $file->error];
                }
            } else {
                $json['error'] = Yii::$app->params['error_upload'];
            }
        }

        if (!$json) {
            $path = 'quotation/requiredDocs/';
            $uniqueName = trim((str_replace(' ', '', $name) . uniqid()));

            $randomName = $string = preg_replace('/\s+/', '', $uniqueName) . '.' . $extension;

            $uploadObject = Yii::$app->get('s3bucket')->upload($path . $randomName, $file->tempName);

            if ($uploadObject) {
                // check if CDN host is available then upload and get cdn URL.
                if (Yii::$app->get('s3bucket')->cdnHostname) {
                    $url = Yii::$app->get('s3bucket')->getCdnUrl($path . $randomName);
                } else {
                    $url = Yii::$app->get('s3bucket')->getUrl($path . $randomName);
                }

                // Save Data into Database
                $data = ['name' => $file->name, 'type' => 'file', 'href' => $url, 'path' => $path . $randomName];
                $this->saveObject($data);
                $json['file'] = $data;
                $json['success'] = Yii::$app->params['text_uploaded'];
            } else {
                $json['error'] = Yii::$app->params['error_upload'];
            }

            /* $data = ['name' => "download (1).jpg" ,'type' => "file", 'href' => "https://wis-windmills.s3.ap-southeast-1.amazonaws.com/quotation/requiredDocs/download%281%2960fbcd31892ba.jpg", 'path' => "quotation/requiredDocs/download(1)60fbcd31892ba.jpg" . $randomName ];

             $json['file']= $data;
             $json['success'] = Yii::$app->params['text_uploaded'];*/
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        // echo "<pre>";
        // print_r($json);
        // echo "</pre>";
        // die();
        return $json;

    }

    public function saveObject($data)
    {

        $folderObj = new MediaManager();
        $folderObj->attributes = $data;
        $folderObj->created_at = date('Y-m-d h:i:s');

        return $folderObj->save();
    }


    public function actionCalculation()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $post = Yii::$app->request->post();
        // echo "<pre>";
        // print_r($post);
        // echo "</pre>";
        // die();
        $amount = yii::$app->propertyCalcHelperFunction->getAmount($post);
        return $amount;

    }



    //bcs
    public function actionBcsServices()
    {
        $model = new QuotationFeeMasterFile();
        //verification process by usama
        $file_type = 'bcs';
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['approach_type' => $file_type])->scalar();
        $model->status_verified = $old_verify_status;
        //end by usama
        if ($model->load(Yii::$app->request->post())) {


            if (QuotationFeeMasterFile::find()->where(['approach_type' => $file_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }


            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['approach_type' => $model->approach_type])
                ->execute();

            foreach ($model->client_type as $key1 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key1;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $this->assignVerification($client_type, $model->status_verified);


                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            
            foreach ($model->subject_property as $key2 => $subject_property_data) {
                $subject_property = new QuotationFeeMasterFile();
                $subject_property->heading = 'subject_property';
                $subject_property->sub_heading = $key2;
                $subject_property->values = $subject_property_data;
                $subject_property->approach_type = $model->approach_type;
                $this->assignVerification($subject_property, $model->status_verified);
                $subject_property->save();
            }

            foreach ($model->bcs_type as $key2bcs => $bcs_type_data) {
                $bcs_type = new QuotationFeeMasterFile();
                $bcs_type->heading = 'bcs_type';
                $bcs_type->sub_heading = $key2bcs;
                $bcs_type->values = $bcs_type_data;
                $bcs_type->approach_type = $model->approach_type;
                $this->assignVerification($bcs_type, $model->status_verified);
                $bcs_type->save();
            }

            foreach ($model->city as $key3 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key3;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->number_of_properties as $key4 => $number_of_properties_data) {
                // print_r($model->number_of_properties);
                // die();
                $number_of_properties = new QuotationFeeMasterFile();
                $number_of_properties->heading = 'number_of_properties';
                $number_of_properties->sub_heading = $key4;
                $number_of_properties->values = $number_of_properties_data;
                $number_of_properties->approach_type = $model->approach_type;
                $this->assignVerification($number_of_properties, $model->status_verified);
                $number_of_properties->save();
                // if ($forNumOfProperties->save()) {
                //   echo "success";
                // }else {
                //   print_r($forNumOfProperties->getErrors());
                // }
                // die();
            }
            foreach ($model->payment_terms as $key5 => $payment_terms_data) {
                // print_r($key5.','.$payment_terms_data);
                // die();
                $payment_terms = new QuotationFeeMasterFile();
                $payment_terms->heading = 'payment_terms';
                $payment_terms->sub_heading = $key5;
                $payment_terms->values = $payment_terms_data;
                $payment_terms->approach_type = $model->approach_type;
                $this->assignVerification($payment_terms, $model->status_verified);
                $payment_terms->save();
            }

            foreach ($model->number_of_units_building as $key6 => $number_of_units_building_data) {
                // echo $key17; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key6;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->tenure as $key6tn => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key6tn;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            // foreach ($model->complexity as $key7 => $complexity_data) {
            //     $complexity = new QuotationFeeMasterFile();
            //     $complexity->heading = 'complexity';
            //     $complexity->sub_heading = $key7;
            //     $complexity->values = $complexity_data;
            //     $complexity->approach_type = $model->approach_type;
            //     $this->assignVerification($complexity, $model->status_verified);
            //     $complexity->save();
            // }
            foreach ($model->drawings_available as $key7da => $drawings_available_data) {
                $drawings_available = new QuotationFeeMasterFile();
                $drawings_available->heading = 'drawings_available';
                $drawings_available->sub_heading = $key7da;
                $drawings_available->values = $drawings_available_data;
                $drawings_available->approach_type = $model->approach_type;
                $this->assignVerification($drawings_available, $model->status_verified);
                $drawings_available->save();
            }
            foreach ($model->new_repeat_valuation as $key8 => $new_repeat_valuation_data) {

                $new_repeat_valuation = new QuotationFeeMasterFile();
                $new_repeat_valuation->heading = 'new_repeat_valuation_data';
                $new_repeat_valuation->sub_heading = $key8;
                $new_repeat_valuation->values = $new_repeat_valuation_data;
                $new_repeat_valuation->approach_type = $model->approach_type;
                $this->assignVerification($new_repeat_valuation, $model->status_verified);

                if (!$new_repeat_valuation->save()) {
                    echo "<pre>";
                    print_r($new_repeat_valuation_data->errors);
                    die;
                }


            }
            // foreach ($model->land as $key18 => $land_data) {
            //     // echo $key18; die();
            //     $land = new QuotationFeeMasterFile();
            //     $land->heading = 'land';
            //     $land->sub_heading = $key18;
            //     $land->values = $land_data;
            //     $land->approach_type = $model->approach_type;
            //     $this->assignVerification($land, $model->status_verified);
            //     $land->save();
            // }
            // foreach ($model->number_of_types as $key18types => $land_data) {
            //     // echo $key18; die();
            //     $land = new QuotationFeeMasterFile();
            //     $land->heading = 'number_of_types';
            //     $land->sub_heading = $key18types;
            //     $land->values = $land_data;
            //     $land->approach_type = $model->approach_type;
            //     $this->assignVerification($land, $model->status_verified);
            //     $land->save();
            // }


            foreach ($model->built_up_area_of_subject_property as $key9 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key9;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            // foreach ($model->other_intended_users as $key9_other_users => $other_intended_users_data) {
            //     $other_intended_users = new QuotationFeeMasterFile();
            //     $other_intended_users->heading = 'other_intended_users';
            //     $other_intended_users->sub_heading = $key9_other_users;
            //     $other_intended_users->values = $other_intended_users_data;
            //     $other_intended_users->approach_type = $model->approach_type;
            //     $this->assignVerification($other_intended_users, $model->status_verified);
            //     $other_intended_users->save();
            // }
            foreach ($model->upgrades_ratings as $key10 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key10;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key11 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key11;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }

            // foreach ($model->number_of_comparables as $key12 => $number_of_comparables_data) {
            //     $number_of_comparables = new QuotationFeeMasterFile();
            //     $number_of_comparables->heading = 'number_of_comparables_data';
            //     $number_of_comparables->sub_heading = $key12;
            //     $number_of_comparables->values = $number_of_comparables_data;
            //     $number_of_comparables->approach_type = $model->approach_type;
            //     $this->assignVerification($number_of_comparables, $model->status_verified);
            //     $number_of_comparables->save();

            // }
            foreach ($model->valuation_approach as $key13 => $valuation_approach_data) {
                $valuation_approach = new QuotationFeeMasterFile();
                $valuation_approach->heading = 'valuation_approach';
                $valuation_approach->sub_heading = $key13;
                $valuation_approach->values = $valuation_approach_data;
                $valuation_approach->approach_type = $model->approach_type;
                $this->assignVerification($valuation_approach, $model->status_verified);
                $valuation_approach->save();
            }

            // foreach ($model->no_of_property_discount as $key14 => $no_of_property_dis) {
            //     // echo $key16; die();
            //     $no_of_property_discount = new QuotationFeeMasterFile();
            //     $no_of_property_discount->heading = 'no_of_property_discount';
            //     $no_of_property_discount->sub_heading = $key14;
            //     $no_of_property_discount->values = $no_of_property_dis;
            //     $no_of_property_discount->approach_type = $model->approach_type;
            //     $this->assignVerification($no_of_property_discount, $model->status_verified);
            //     $no_of_property_discount->save();
            // }

            foreach ($model->no_of_assignment_discount as $key14 => $no_of_property_dis) {
                // echo $key16; die();
                $no_of_assignment_discount = new QuotationFeeMasterFile();
                $no_of_assignment_discount->heading = 'no_of_assignment_discount';
                $no_of_assignment_discount->sub_heading = $key14;
                $no_of_assignment_discount->values = $no_of_property_dis;
                $no_of_assignment_discount->approach_type = $model->approach_type;
                $this->assignVerification($no_of_assignment_discount, $model->status_verified);
                $no_of_assignment_discount->save();
            }

            foreach ($model->land as $key18 => $land_data) {
                // echo $key18; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key18;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }

            foreach ($model->first_time_discount as $key15 => $first_time_data) {
                $first_time_discount = new QuotationFeeMasterFile();
                $first_time_discount->heading = 'first_time_discount';
                $first_time_discount->sub_heading = $key15;
                $first_time_discount->values = $first_time_data;
                $first_time_discount->approach_type = $model->approach_type;
                $this->assignVerification($first_time_discount, $model->status_verified);
                $first_time_discount->save();
            }


            // foreach ($model->no_of_units_same_building_discount as $key16 => $no_of_units_same_building_discount_data) {
            //     $no_of_units_same_building_discount = new QuotationFeeMasterFile();
            //     $no_of_units_same_building_discount->heading = 'no_of_units_same_building_discount';
            //     $no_of_units_same_building_discount->sub_heading = $key16;
            //     $no_of_units_same_building_discount->values = $no_of_units_same_building_discount_data;
            //     $no_of_units_same_building_discount->approach_type = $model->approach_type;
            //     $this->assignVerification($no_of_units_same_building_discount, $model->status_verified);
            //     $no_of_units_same_building_discount->save();
            // }
            
            foreach ($model->no_of_buildings_same_community_discount as $key16 => $no_of_buildings_same_community_discount_data) {
                $no_of_buildings_same_community_discount = new QuotationFeeMasterFile();
                $no_of_buildings_same_community_discount->heading = 'no_of_buildings_same_community_discount';
                $no_of_buildings_same_community_discount->sub_heading = $key16;
                $no_of_buildings_same_community_discount->values = $no_of_buildings_same_community_discount_data;
                $no_of_buildings_same_community_discount->approach_type = $model->approach_type;
                $this->assignVerification($no_of_buildings_same_community_discount, $model->status_verified);
                $no_of_buildings_same_community_discount->save();
            }


            foreach ($model->urgency_fee as $key17 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key17;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $file_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }


        $view = 'bcs_services';



        return $this->render($view, [
            'model' => $model,
        ]);
    }



    public function actionQuotationFeeMaster(){
        return $this->render('quotation_fee_master', [
        ]);
    }


    //Residential land for villa
    public function actionMasterFileResidentialLandVilla()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        $file_type = 'market';
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        // $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['approach_type' => $file_type])->scalar();
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                // 'file_type' => $file_type,
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_residential_land_villa';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //Apartment
    public function actionMasterFileApartment()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        $file_type = 'market';
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_apartment';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //Penthouse
    public function actionMasterFilePenthouse()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        $file_type = 'market';
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_penthouse';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //Townhouse
    public function actionMasterFileTownhouse()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        $file_type = 'market';
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_townhouse';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //Villa
    public function actionMasterFileVilla()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        $file_type = 'market';
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_villa';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //Shop
    public function actionMasterFileShop()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        $file_type = 'market';
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_shop';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //Office
    public function actionMasterFileOffice()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        $file_type = 'market';
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_office';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //Warehouse
    public function actionMasterFileWarehouse()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        $file_type = 'market';
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {


            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_warehouse';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //Building Land
    public function actionMasterFileBuildingLand()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        $file_type = 'market';
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_building_land';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //Building 
    public function actionMasterFileBuilding()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        $file_type = 'market';
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $property_type = ($property_type <> null ) ? $property_type : 'building';
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_building';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //Building Residential
    public function actionMasterFileBuildingResidential()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        // $file_type = 'market';
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $property_type = ($property_type <> null ) ? $property_type : 'building_residential';
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_building_residential';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //Building Commercial
    public function actionMasterFileBuildingCommercial()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        // $file_type = 'market';
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $property_type = ($property_type <> null ) ? $property_type : 'building_commercial';
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_building_commercial';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //Building Mixeduse
    public function actionMasterFileBuildingMixeduse()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        // $file_type = 'market';
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $property_type = ($property_type <> null ) ? $property_type : 'building_mixeduse';
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_building_mixeduse';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //Villa Compound 
    public function actionMasterFileVillaCompound()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        $file_type = 'market';
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_villa_compound';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //Warehouse Compound 
    public function actionMasterFileWarehouseCompound()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        $file_type = 'market';
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_warehouse_compound';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //Labour Accommodation
    public function actionMasterFileLabourAccommodation()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        $file_type = 'market';
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_labour_accommodation';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //Hotel
    public function actionMasterFileHotel()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        $file_type = 'market';
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_hotel';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //Hospital
    public function actionMasterFileHospital()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        $file_type = 'income';
        $approach_type = Yii::$app->request->post('QuotationFeeMasterFile')['approach_type'];
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_hospital';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //School Leased
    public function actionMasterFileSchoolLeased()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        $file_type = 'income';
        $approach_type = Yii::$app->request->post('QuotationFeeMasterFile')['approach_type'];
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_school_leased';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //School Operated
    public function actionMasterFileSchoolOperated()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        $file_type = 'profit';
        $approach_type = Yii::$app->request->post('QuotationFeeMasterFile')['approach_type'];
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_school_operated';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //Mall Leased
    public function actionMasterFileMallLeased()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        $file_type = 'market';
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_mall_leased';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //Mall Operated
    public function actionMasterFileMallOperated()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        $file_type = 'market';
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_mall_operated';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }


    // Fee discount
    public function actionMasterFileFeeDiscount()
    {
        $model = new QuotationFeeMasterFile();

        //verification process
        $file_type = 'discount';
        // $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];

        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['approach_type' => $file_type])->scalar();
        $model->status_verified = $old_verify_status;


        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['approach_type' => $file_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['approach_type' => $model->approach_type, 'property_type' => null])
                ->execute();

            foreach ($model->payment_terms as $key5 => $payment_terms_data) {
                // print_r($key5.','.$payment_terms_data);
                // die();
                $payment_terms = new QuotationFeeMasterFile();
                $payment_terms->heading = 'payment_terms';
                $payment_terms->sub_heading = $key5;
                $payment_terms->values = $payment_terms_data;
                $payment_terms->approach_type = $model->approach_type;
                $this->assignVerification($payment_terms, $model->status_verified);
                $payment_terms->save();
            }
            foreach ($model->new_repeat_valuation as $key8 => $new_repeat_valuation_data) {

                $new_repeat_valuation = new QuotationFeeMasterFile();
                $new_repeat_valuation->heading = 'new_repeat_valuation_data';
                $new_repeat_valuation->sub_heading = $key8;
                $new_repeat_valuation->values = $new_repeat_valuation_data;
                $new_repeat_valuation->approach_type = $model->approach_type;
                $this->assignVerification($new_repeat_valuation, $model->status_verified);

                if (!$new_repeat_valuation->save()) {
                    echo "<pre>";
                    print_r($new_repeat_valuation_data->errors);
                    die;
                }
            }
            foreach ($model->valuation_approach as $key13 => $valuation_approach_data) {
                $valuation_approach = new QuotationFeeMasterFile();
                $valuation_approach->heading = 'valuation_approach';
                $valuation_approach->sub_heading = $key13;
                $valuation_approach->values = $valuation_approach_data;
                $valuation_approach->approach_type = $model->approach_type;
                $this->assignVerification($valuation_approach, $model->status_verified);
                $valuation_approach->save();
            }
            foreach ($model->no_of_property_discount as $key14 => $no_of_property_dis) {
                // echo $key16; die();
                $no_of_property_discount = new QuotationFeeMasterFile();
                $no_of_property_discount->heading = 'no_of_property_discount';
                $no_of_property_discount->sub_heading = $key14;
                $no_of_property_discount->values = $no_of_property_dis;
                $no_of_property_discount->approach_type = $model->approach_type;
                $this->assignVerification($no_of_property_discount, $model->status_verified);
                $no_of_property_discount->save();
            }
            foreach ($model->first_time_discount as $key15 => $first_time_data) {
                $first_time_discount = new QuotationFeeMasterFile();
                $first_time_discount->heading = 'first_time_discount';
                $first_time_discount->sub_heading = $key15;
                $first_time_discount->values = $first_time_data;
                $first_time_discount->approach_type = $model->approach_type;
                $this->assignVerification($first_time_discount, $model->status_verified);
                $first_time_discount->save();
            }
            foreach ($model->no_of_units_same_building_discount as $key16 => $no_of_units_same_building_discount_data) {
                $no_of_units_same_building_discount = new QuotationFeeMasterFile();
                $no_of_units_same_building_discount->heading = 'no_of_units_same_building_discount';
                $no_of_units_same_building_discount->sub_heading = $key16;
                $no_of_units_same_building_discount->values = $no_of_units_same_building_discount_data;
                $no_of_units_same_building_discount->approach_type = $model->approach_type;
                $this->assignVerification($no_of_units_same_building_discount, $model->status_verified);
                $no_of_units_same_building_discount->save();
            }
            foreach ($model->last_three_years_finance as $key17 => $last_three_years_finance_data) {
                $last_three_years_finance = new QuotationFeeMasterFile();
                $last_three_years_finance->heading = 'last_three_years_finance';
                $last_three_years_finance->sub_heading = $key17;
                $last_three_years_finance->values = $last_three_years_finance_data;
                $last_three_years_finance->approach_type = $model->approach_type;
                $this->assignVerification($last_three_years_finance, $model->status_verified);
                $last_three_years_finance->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $file_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_fee_discount';


        return $this->render($view, [
            'model' => $model,
        ]);
    }

    //Hotel Apartment
    public function actionMasterFileHotelApartment()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        $file_type = 'market';
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $property_type = ($property_type <> null ) ? $property_type : 'hotel_apartment';
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_hotel_apartment';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }

    //Building Hotel Apartments
    public function actionMasterFileBuildingHotelApartment()
    {
        $model = new QuotationFeeMasterFile();

        $properties = ArrayHelper::map(\app\models\Properties::find()->where(['status' => 1])->orderBy(['title' => SORT_ASC])->all(), 'id', 'title');

        //verification process
        // $file_type = 'market';
        $property_type = Yii::$app->request->post('QuotationFeeMasterFile')['property_type'];
        $property_type = ($property_type <> null ) ? $property_type : 'building_hotel_apartment';
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['property_type' => $property_type])->scalar();
        $model->status_verified = $old_verify_status;

        if ($model->load(Yii::$app->request->post())) {

            if (QuotationFeeMasterFile::find()->where(['property_type' => $property_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }

            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['property_type' => $property_type])
                ->execute();

            foreach ($model->property_fee as $key1 => $property_fee_data) {
                $property_fee = new QuotationFeeMasterFile();
                $property_fee->heading = 'property_fee';
                $property_fee->sub_heading = $key1;
                $property_fee->values = $property_fee_data;
                $property_fee->approach_type = $model->approach_type;
                $property_fee->property_type = $model->property_type;
                $this->assignVerification($property_fee, $model->status_verified);
                $property_fee->save();
            }
            foreach ($model->client_type as $key2 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key2;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $client_type->property_type = $model->property_type;
                $this->assignVerification($client_type, $model->status_verified);
                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            foreach ($model->city as $key2 => $city_data) {
                $city = new QuotationFeeMasterFile();
                $city->heading = 'city';
                $city->sub_heading = $key2;
                $city->values = $city_data;
                $city->approach_type = $model->approach_type;
                $city->property_type = $model->property_type;
                $this->assignVerification($city, $model->status_verified);
                $city->save();
            }
            foreach ($model->tenure as $key3 => $tenure_data) {
                $tenure = new QuotationFeeMasterFile();
                $tenure->heading = 'tenure';
                $tenure->sub_heading = $key3;
                $tenure->values = $tenure_data;
                $tenure->approach_type = $model->approach_type;
                $tenure->property_type = $model->property_type;
                $this->assignVerification($tenure, $model->status_verified);
                $tenure->save();
            }
            foreach ($model->complexity as $key4 => $complexity_data) {
                $complexity = new QuotationFeeMasterFile();
                $complexity->heading = 'complexity';
                $complexity->sub_heading = $key4;
                $complexity->values = $complexity_data;
                $complexity->approach_type = $model->approach_type;
                $complexity->property_type = $model->property_type;
                $this->assignVerification($complexity, $model->status_verified);
                $complexity->save();
            }
            foreach ($model->land as $key5 => $land_data) {
                // echo $key5; die();
                $land = new QuotationFeeMasterFile();
                $land->heading = 'land';
                $land->sub_heading = $key5;
                $land->values = $land_data;
                $land->approach_type = $model->approach_type;
                $land->property_type = $model->property_type;
                $this->assignVerification($land, $model->status_verified);
                $land->save();
            }
            foreach ($model->built_up_area_of_subject_property as $key6 => $built_up_area_of_subject_property_data) {
                $build_up_area_of_subject_property = new QuotationFeeMasterFile();
                $build_up_area_of_subject_property->heading = 'built_up_area_of_subject_property';
                $build_up_area_of_subject_property->sub_heading = $key6;
                $build_up_area_of_subject_property->values = $built_up_area_of_subject_property_data;
                $build_up_area_of_subject_property->approach_type = $model->approach_type;
                $build_up_area_of_subject_property->property_type = $model->property_type;
                $this->assignVerification($build_up_area_of_subject_property, $model->status_verified);
                $build_up_area_of_subject_property->save();
            }
            foreach ($model->net_leasable_area as $key7 => $net_leasable_area_data) {
                $net_leasable_area = new QuotationFeeMasterFile();
                $net_leasable_area->heading = 'net_leasable_area';
                $net_leasable_area->sub_heading = $key7;
                $net_leasable_area->values = $net_leasable_area_data;
                $net_leasable_area->approach_type = $model->approach_type;
                $net_leasable_area->property_type = $model->property_type;
                $this->assignVerification($net_leasable_area, $model->status_verified);
                $net_leasable_area->save();
            }
            foreach ($model->number_of_units_building as $key8 => $number_of_units_building_data) {
                // echo $key8; die();
                $number_of_units_building = new QuotationFeeMasterFile();
                $number_of_units_building->heading = 'number_of_units_building';
                $number_of_units_building->sub_heading = $key8;
                $number_of_units_building->values = $number_of_units_building_data;
                $number_of_units_building->approach_type = $model->approach_type;
                $number_of_units_building->property_type = $model->property_type;
                $this->assignVerification($number_of_units_building, $model->status_verified);
                $number_of_units_building->save();
            }
            foreach ($model->types_of_property_unit as $key9 => $types_of_property_unit_data) {
                $types_of_property_unit = new QuotationFeeMasterFile();
                $types_of_property_unit->heading = 'types_of_property_unit';
                $types_of_property_unit->sub_heading = $key9;
                $types_of_property_unit->values = $types_of_property_unit_data;
                $types_of_property_unit->approach_type = $model->approach_type;
                $types_of_property_unit->property_type = $model->property_type;
                $this->assignVerification($types_of_property_unit, $model->status_verified);
                $types_of_property_unit->save();
            }
            foreach ($model->number_of_types as $key10 => $number_of_types_data) {
                // echo $key18; die();
                $number_of_types = new QuotationFeeMasterFile();
                $number_of_types->heading = 'number_of_types';
                $number_of_types->sub_heading = $key10;
                $number_of_types->values = $number_of_types_data;
                $number_of_types->approach_type = $model->approach_type;
                $number_of_types->property_type = $model->property_type;
                $this->assignVerification($number_of_types, $model->status_verified);
                $number_of_types->save();
            }
            foreach ($model->number_of_comparables as $key11 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key11;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $number_of_comparables->property_type = $model->property_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }
            foreach ($model->other_intended_users as $key12 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key12;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $other_intended_users->property_type = $model->property_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->urgency_fee as $key13 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key13;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $urgency_fee->property_type = $model->property_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }
            foreach ($model->upgrades_ratings as $key14 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key14;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $upgrades_ratings->property_type = $model->property_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key15 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key15;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $type_of_valuation->property_type = $model->property_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }
            


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $property_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        $view = 'master_file_building_hotel_apartment';


        return $this->render($view, [
            'model' => $model,
            'properties' => $properties,
        ]);
    }


    //pme
    public function actionPmeServices()
    {
        $model = new QuotationFeeMasterFile();
        $asset_categories = AssetCategory::find()->all();
        // $asset_categories = AssetCategory::find()->asArray()->all();


        //verification process by usama
        $file_type = 'pme';
        $old_verify_status = QuotationFeeMasterFile::find()->select('status_verified')->where(['approach_type' => $file_type])->scalar();
        $model->status_verified = $old_verify_status;
        //end by usama
        if ($model->load(Yii::$app->request->post())) {


            if (QuotationFeeMasterFile::find()->where(['approach_type' => $file_type])->count() > 0) {
                $action = "data_updated";
            } else {
                $action = "data_created";
            }


            Yii::$app->db->createCommand()
                ->delete('quotation_fee_master_file', ['approach_type' => $model->approach_type])
                ->execute();

            foreach ($model->asset_category as $key0 => $asset_category_data) {
                $asset_category = new QuotationFeeMasterFile();
                $asset_category->heading = 'asset_category';
                $asset_category->sub_heading = $key0;
                $asset_category->values = $asset_category_data;
                $asset_category->approach_type = $model->approach_type;
                $this->assignVerification($asset_category, $model->status_verified);
                $asset_category->save();
            }

            foreach ($model->client_type as $key1 => $client_type_data) {
                $client_type = new QuotationFeeMasterFile();
                $client_type->heading = 'client_type';
                $client_type->sub_heading = $key1;
                $client_type->values = $client_type_data;
                $client_type->approach_type = $model->approach_type;
                $this->assignVerification($client_type, $model->status_verified);


                if (!$client_type->save()) {
                    echo "<pre>";
                    print_r($client_type->errors);
                    die;
                }
            }
            
            // foreach ($model->subject_property as $key2 => $subject_property_data) {
            //     $subject_property = new QuotationFeeMasterFile();
            //     $subject_property->heading = 'subject_property';
            //     $subject_property->sub_heading = $key2;
            //     $subject_property->values = $subject_property_data;
            //     $subject_property->approach_type = $model->approach_type;
            //     $this->assignVerification($subject_property, $model->status_verified);
            //     $subject_property->save();
            // }

            

            foreach ($model->location as $key3 => $location_data) {
                $location = new QuotationFeeMasterFile();
                $location->heading = 'location';
                $location->sub_heading = $key3;
                $location->values = $location_data;
                $location->approach_type = $model->approach_type;
                $this->assignVerification($location, $model->status_verified);
                $location->save();
            }

            foreach ($model->no_of_asset as $key4 => $no_of_asset_data) {
                $no_of_asset = new QuotationFeeMasterFile();
                $no_of_asset->heading = 'no_of_asset';
                $no_of_asset->sub_heading = $key4;
                $no_of_asset->values = $no_of_asset_data;
                $no_of_asset->approach_type = $model->approach_type;
                $this->assignVerification($no_of_asset, $model->status_verified);
                $no_of_asset->save();
            }

            foreach ($model->payment_terms as $key5 => $payment_terms_data) {
                $payment_terms = new QuotationFeeMasterFile();
                $payment_terms->heading = 'payment_terms';
                $payment_terms->sub_heading = $key5;
                $payment_terms->values = $payment_terms_data;
                $payment_terms->approach_type = $model->approach_type;
                $this->assignVerification($payment_terms, $model->status_verified);
                $payment_terms->save();
            }

            foreach ($model->asset_age as $key6 => $asset_age_data) {
                // echo $key17; die();
                $asset_age = new QuotationFeeMasterFile();
                $asset_age->heading = 'asset_age';
                $asset_age->sub_heading = $key6;
                $asset_age->values = $asset_age_data;
                $asset_age->approach_type = $model->approach_type;
                $this->assignVerification($asset_age, $model->status_verified);
                $asset_age->save();
            }
            
            
            foreach ($model->asset_complexity as $key9 => $asset_complexity_data) {
                $asset_complexity = new QuotationFeeMasterFile();
                $asset_complexity->heading = 'asset_complexity';
                $asset_complexity->sub_heading = $key9;
                $asset_complexity->values = $asset_complexity_data;
                $asset_complexity->approach_type = $model->approach_type;
                $this->assignVerification($asset_complexity, $model->status_verified);
                $asset_complexity->save();
            }
            
            
            foreach ($model->new_repeat_valuation as $key8 => $new_repeat_valuation_data) {

                $new_repeat_valuation = new QuotationFeeMasterFile();
                $new_repeat_valuation->heading = 'new_repeat_valuation_data';
                $new_repeat_valuation->sub_heading = $key8;
                $new_repeat_valuation->values = $new_repeat_valuation_data;
                $new_repeat_valuation->approach_type = $model->approach_type;
                $this->assignVerification($new_repeat_valuation, $model->status_verified);

                if (!$new_repeat_valuation->save()) {
                    echo "<pre>";
                    print_r($new_repeat_valuation_data->errors);
                    die;
                }


            }
            


            
            foreach ($model->other_intended_users as $key9 => $other_intended_users_data) {
                $other_intended_users = new QuotationFeeMasterFile();
                $other_intended_users->heading = 'other_intended_users';
                $other_intended_users->sub_heading = $key9;
                $other_intended_users->values = $other_intended_users_data;
                $other_intended_users->approach_type = $model->approach_type;
                $this->assignVerification($other_intended_users, $model->status_verified);
                $other_intended_users->save();
            }
            foreach ($model->upgrades_ratings as $key10 => $upgrades_ratings_data) {
                $upgrades_ratings = new QuotationFeeMasterFile();
                $upgrades_ratings->heading = 'upgrades_ratings';
                $upgrades_ratings->sub_heading = $key10;
                $upgrades_ratings->values = $upgrades_ratings_data;
                $upgrades_ratings->approach_type = $model->approach_type;
                $this->assignVerification($upgrades_ratings, $model->status_verified);
                $upgrades_ratings->save();
            }
            foreach ($model->type_of_valuation as $key11 => $type_of_valuation_data) {
                $type_of_valuation = new QuotationFeeMasterFile();
                $type_of_valuation->heading = 'type_of_valuation';
                $type_of_valuation->sub_heading = $key11;
                $type_of_valuation->values = $type_of_valuation_data;
                $type_of_valuation->approach_type = $model->approach_type;
                $this->assignVerification($type_of_valuation, $model->status_verified);
                $type_of_valuation->save();
            }

            foreach ($model->number_of_comparables as $key12 => $number_of_comparables_data) {
                $number_of_comparables = new QuotationFeeMasterFile();
                $number_of_comparables->heading = 'number_of_comparables_data';
                $number_of_comparables->sub_heading = $key12;
                $number_of_comparables->values = $number_of_comparables_data;
                $number_of_comparables->approach_type = $model->approach_type;
                $this->assignVerification($number_of_comparables, $model->status_verified);
                $number_of_comparables->save();
            }

            
            foreach ($model->valuation_approach as $key13 => $valuation_approach_data) {
                $valuation_approach = new QuotationFeeMasterFile();
                $valuation_approach->heading = 'valuation_approach';
                $valuation_approach->sub_heading = $key13;
                $valuation_approach->values = $valuation_approach_data;
                $valuation_approach->approach_type = $model->approach_type;
                $this->assignVerification($valuation_approach, $model->status_verified);
                $valuation_approach->save();
            }

            foreach ($model->no_of_location as $key14 => $no_of_location_data) {
                $no_of_location = new QuotationFeeMasterFile();
                $no_of_location->heading = 'no_of_location';
                $no_of_location->sub_heading = $key14;
                $no_of_location->values = $no_of_location_data;
                $no_of_location->approach_type = $model->approach_type;
                $this->assignVerification($no_of_location, $model->status_verified);
                $no_of_location->save();
            }

            // foreach ($model->working_days as $key15 => $working_days_data) {
            //     $working_days = new QuotationFeeMasterFile();
            //     $working_days->heading = 'working_days';
            //     $working_days->sub_heading = $key15;
            //     $working_days->values = $working_days_data;
            //     $working_days->approach_type = $model->approach_type;
            //     $this->assignVerification($working_days, $model->status_verified);
            //     $working_days->save();
            // }

            

            foreach ($model->first_time_discount as $key15 => $first_time_data) {
                $first_time_discount = new QuotationFeeMasterFile();
                $first_time_discount->heading = 'first_time_discount';
                $first_time_discount->sub_heading = $key15;
                $first_time_discount->values = $first_time_data;
                $first_time_discount->approach_type = $model->approach_type;
                $this->assignVerification($first_time_discount, $model->status_verified);
                $first_time_discount->save();
            }


            


            foreach ($model->urgency_fee as $key17 => $urgency_fee_data) {
                $urgency_fee = new QuotationFeeMasterFile();
                $urgency_fee->heading = 'urgency_fee';
                $urgency_fee->sub_heading = $key17;
                $urgency_fee->values = $urgency_fee_data;
                $urgency_fee->approach_type = $model->approach_type;
                $this->assignVerification($urgency_fee, $model->status_verified);
                $urgency_fee->save();
            }


            $history = [
                'model' => $model,
                'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                'action' => $action,
                'rec_type' => 'masterfile',
                'file_type' => $file_type,
                'old_verify_status' => $old_verify_status,
            ];
            $this->makeMasterFilesHistory($history);


            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->refresh();
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }


        $view = 'pme_services';



        return $this->render($view, [
            'model' => $model,
            'asset_categories' => $asset_categories,
        ]);
    }


    public function actionProposalBcsServiceReport()
    {
        $model = ProposalBcsReport::findOne(1);
        $old_verify_status = $model->status_verified;

        if ($model->load(Yii::$app->request->post())) {
            $this->StatusVerify($model);

            if ($model->save()) {
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => 'app\models\ProposalBcsReport',
                    'action' => 'data_updated',
                    'verify_field' => 'status_verified',
                    'old_verify_status' => $old_verify_status,
                ]);

                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->refresh();
            }
        }
        return $this->render('bcs_service_report', [
            'model' => $model,
        ]);

    }


    public function actionProposalPmeServiceReport()
    {
        $model = ProposalPmeReport::findOne(1);
        $old_verify_status = $model->status_verified;

        if ($model->load(Yii::$app->request->post())) {
            $this->StatusVerify($model);

            if ($model->save()) {
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => 'app\models\ProposalPmeReport',
                    'action' => 'data_updated',
                    'verify_field' => 'status_verified',
                    'old_verify_status' => $old_verify_status,
                ]);

                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->refresh();
            }
        }
        return $this->render('pme_service_report', [
            'model' => $model,
        ]);

    }



}
