<?php

namespace app\controllers;

use Yii;

use yii\filters\VerbFilter;
use app\components\helpers\DefController;
use app\models\CategoryManager;
use app\models\CategoryManagerSearch;

/**
 * CategoryManagerController implements the CRUD actions for Category model.
 */
class CategoryManagerController extends DefController
{
    public function actionIndex()
    {
        $searchModel = new CategoryManagerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    { 
        if(!empty(Yii::$app->request->post()))
        {
            $model = new CategoryManager();
            $model->category_name = Yii::$app->request->post()['category_name'];
            $model->status = Yii::$app->request->post()['status'];
            $model->created_by = Yii::$app->user->identity->id;
            $model->created_at = date("Y/m/d H:i:s");
            $model->save();
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = CategoryManager::findOne(['id' => $id]);
        if(!empty(Yii::$app->request->post()))
        {
            
            $model->category_name = Yii::$app->request->post()['category_name'];
            $model->status = Yii::$app->request->post()['status'];
            $model->created_by = Yii::$app->user->identity->id;
            $model->created_at = date("Y/m/d H:i:s");
            $model->save();
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = CategoryManager::findOne(['id' => $id])->delete();

        return $this->redirect(['index']);
    }
}
