<?php

namespace app\models;

use Yii;
use app\components\models\ActiveRecordFull;

/**
 * This is the model class for table "auto_lististings".
 *
 * @property int $id
 * @property int|null $relax_records_limit
 * @property int|null $relax_bua_from
 * @property int|null $relax_bua_to
 * @property int|null $relax_price_from
 * @property int|null $relax_price_to
 * @property int|null $relax_date
 * @property int|null $strict_records_limit
 * @property int|null $strict_bua_from
 * @property int|null $strict_bua_to
 * @property int|null $strict_price_from
 * @property int|null $strict_price_to
 * @property int|null $strict_date
 * @property int|null $moderate_records_limit
 * @property int|null $moderate_bua_from
 * @property int|null $moderate_bua_to
 * @property int|null $moderate_price_from
 * @property int|null $moderate_price_to
 * @property int|null $moderate_date
 * @property int|null $mode
 */
class AutoListings extends \yii\db\ActiveRecord
{


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auto_listings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['relax_records_limit_from', 'relax_records_limit_to', 'relax_bua_percentage', 'relax_price_percentage', 'relax_date', 'strict_records_limit_from', 'strict_records_limit_to', 'strict_bua_percentage', 'strict_price_percentage', 'strict_date', 'moderate_records_limit_from', 'moderate_records_limit_to', 'moderate_bua_percentage', 'moderate_price_percentage', 'moderate_date'], 'integer'],
             [['data_type',],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'relax_records_limit_from' => 'Relax Records Limit',
            'relax_records_limit_to' => 'Relax Bua From',
            'relax_bua_percentage' => 'Relax Bua To',
            'relax_price_percentage' => 'Relax Price From',
            'relax_date' => 'Relax Price To',
            'strict_records_limit_from' => 'Relax Date',
            'strict_records_limit_to' => 'Strict Records Limit',
            'strict_bua_percentage' => 'Strict Bua From',
            'strict_price_percentage' => 'Strict Bua To',
            'strict_date' => 'Strict Price From',
            'moderate_records_limit_from' => 'Strict Price To',
            'moderate_records_limit_to' => 'Strict Date',
            'moderate_bua_percentage' => 'Moderate Records Limit',
            'moderate_price_percentage' => 'Moderate Bua From',
            'moderate_date' => 'Moderate Bua To',

        ];
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecTitle()
    {
        return $this->id;
    }

    /**
     * returns main title/name of row for status and delete loging
     */
    public function getRecType()
    {
        return 'AutoListings';
    }
}
