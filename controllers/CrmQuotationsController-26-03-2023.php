<?php

namespace app\controllers;

use app\models\AutoCrmInvoices;
use app\models\Company;
use app\models\ConfigurationFiles;
use app\models\CrmInspectProperty;
use app\models\CrmQuotationConfigraions;
use app\models\CrmReceivedDocs;
use app\models\CrmReceivedProperties;
use app\models\CrmScheduleInspection;
use app\models\CrmValuationConflict;
use app\models\InspectProperty;
use app\models\PreviousTransactions;
use app\models\ProposalMasterFile;
use app\models\ReceivedDocs;
use app\models\ReceivedDocsFiles;
use app\models\ScheduleInspection;
use app\models\Valuation;
use app\models\ValuationApproversData;
use app\models\ValuationConfiguration;
use app\models\ValuationConflict;
use app\models\ValuationOwners;
use app\models\CrmQuotationOwner;
use app\models\CrmReceivedDocsFiles;
use Yii;
use app\models\CrmQuotations;
use app\models\CrmQuotationsSearch;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\helpers\Url;
use  \app\modules\wisnotify\listners\NotifyEvent;

/**
 * CrmQuotationsController implements the CRUD actions for CrmQuotations model.
 */
class CrmQuotationsController extends DefController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CrmQuotations models.
     * @return mixed
     */
    public function actionIndex()
    {



        $this->checkLogin();
        $searchModel = new CrmQuotationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new CrmQuotations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->checkLogin();
        $date = date("Y-m-d h:i:s");
        $model = new CrmQuotations();
        $model->reference_number = Yii::$app->crmQuotationHelperFunctions->uniqueReference;
        $model->quotation_status = 0;
        $model->status_change_date = $date;
        $model->inquiry_received_date = $date;






        if ($model->load(Yii::$app->request->post())) {
            if( $model->advance_payment_terms =='25%' || $model->advance_payment_terms =='50%' || $model->advance_payment_terms =='75%'){
                $advance_payment =explode("%",$model->advance_payment_terms);
                $model->first_half_payment = $advance_payment[0];
                $model->second_half_payment = 100 - $advance_payment[0];
                $model->payment_status = 1;
            }else{
                $model->payment_status = 0;
            }
            // echo "<pre>"; print_r(Yii::$app->request->post());   echo "</pre>"; die();

            if($model->save()){
                Yii::$app->crmQuotationHelperFunctions->addStatusHistory($model);

                Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
                return $this->redirect(['crm-quotations/step_0?id=' . $model->id]);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }
        // die('here');


        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionStep_0($id, $showAlert=null)
    {

        $this->checkLogin();
        $model = $this->findModel($id);
        $reference = $model->reference_number;
        $previous = AutoCrmInvoices::find()->where(['qoutation_id'=>$id])->asArray()->one();
        if($previous <> null){
           // $reference = $previous['client_type'].' - '.$previous['prefix'].' - '.$previous['current_year'].' - '.$previous['current_year_number'].' - '.$previous['general_number'];
            $reference = $previous['client_type'].' - '.$previous['prefix'].' - '.$previous['current_year'].' - '.$previous['current_year_number'];
        }
        if ($model->load(Yii::$app->request->post())) {
            if( $model->advance_payment_terms =='25%' || $model->advance_payment_terms =='50%' || $model->advance_payment_terms =='75%'){
                $advance_payment =explode("%",$model->advance_payment_terms);
                $model->first_half_payment = $advance_payment[0];
                $model->second_half_payment = 100 - $advance_payment[0];
                $model->payment_status = 1;
            }else{
                $model->payment_status = 0;
            }
            // echo "<pre>"; print_r(Yii::$app->request->post());   echo "</pre>"; die();

            if($model->save()){
                /*echo $model->inspection_type;
                die;*/
                Yii::$app->db->createCommand()->update('crm_received_properties', ['inspection_type' => $model->inspection_type], 'quotation_id='.$model->id .'')->execute();
                Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
                return $this->redirect(['crm-quotations/step_0?id=' . $id]);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }


        return $this->render('steps/_step0', [
            'model' => $model,
            'quotation' => $model,
            'showAlert' => $showAlert,
            'reference'=>$reference
        ]);
    }
    public function actionStep_1($id,$property_index)
    {
        $this->checkLogin();
        $quotation = $this->findModel($id);
        $model = CrmReceivedProperties::find()->where(['quotation_id' => $id,'property_index' => $property_index])->one();
        if ($model !== null) {
            $model->city = Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city];
            $model->community = $model->building->communities->title;
            $model->sub_community = $model->building->subCommunities->title;

        } else {
            // die('kj');
            $model = new CrmReceivedProperties();
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->quotation_id = $id;
            $model->property_index = $property_index;
            $model->inspection_type = $quotation->inspection_type;
            if ($model->save()) {

                $previous = AutoCrmInvoices::find()->where(['qoutation_id'=>$id])->asArray()->one();
                if($previous <> null){

                }else{
                    if($model->property->prefix <> null){
                        $prefix = Yii::$app->appHelperFunctions->crmInvoiceArr[$model->property->prefix];
                    }else{
                        $prefix = 'REQ';
                    }

                    if($quotation->client->client_type ==  "bank"){
                        $clinet_type = 'B';
                    }else if($quotation->client->client_type ==  "corporate"){
                        $clinet_type = 'C';
                    }else{
                        $clinet_type = 'I';
                    }

                    $previous_record = AutoCrmInvoices::find()
                        ->select([
                            'id',
                            'current_year_number',
                        ])->where(['current_year' => date('Y')])
                        ->orderBy(['id' => SORT_DESC])->asArray()->one();

                    $current_year_number = str_pad($previous_record['current_year_number']+1, 4, "0", STR_PAD_LEFT);
                    $prev_ref = explode("-", $quotation->reference_number);
                    if($id > 1019) {
                        $prefix_number = trim($prev_ref[4]);
                    }else{
                        $prefix_number = $prev_ref[2];
                    }



                    $crm_inv_reference = new AutoCrmInvoices();
                    $crm_inv_reference->qoutation_id = $id;
                    $crm_inv_reference->client_type = $clinet_type;
                    $crm_inv_reference->prefix = $prefix;
                    $crm_inv_reference->current_year = date('Y');
                    $crm_inv_reference->current_year_number	 = $current_year_number;
                    $crm_inv_reference->general_number	 = $prefix_number;
                    $crm_inv_reference->created_at	 = date('Y-m-d h:i:s');
                    $crm_inv_reference->created_by	 = \Yii::$app->user->identity->id;
                    $crm_inv_reference->save();
                }
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['crm-quotations/step_1?id=' . $id.'&property_index='.$property_index]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }



        return $this->render('steps/_step1', [
            'model' => $model,
            'quotation' => $quotation,
            'property_index' => $property_index,
        ]);
    }

    public function actionStep_2($id,$property_index)
    {
        $this->checkLogin();
        $quotation = $this->findModel($id);
        $model = CrmReceivedDocs::find()->where(['quotation_id' => $id,'property_index' => $property_index])->one();
        $valuation = CrmReceivedProperties::find()->where(['quotation_id' => $id,'property_index' => $property_index])->one();


        if ($model !== null) {

        } else {
            $model = new CrmReceivedDocs();
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->quotation_id = $id;
            $model->property_index = $property_index;
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['crm-quotations/step_2?id=' . $id.'&property_index='.$property_index]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('steps/_step2', [
            'model' => $model,
            'quotation' => $quotation,
            'valuation' => $valuation,
            'property_index' => $property_index,
        ]);
    }

    public function actionStep_3($id,$property_index)
    {
        $this->checkLogin();
        $quotation = $this->findModel($id);
        $model = CrmValuationConflict::find()->where(['quotation_id' => $id,'property_index' => $property_index])->one();
        $valuation = CrmReceivedProperties::find()->where(['quotation_id' => $id,'property_index' => $property_index])->one();

        $client =  Company::find()->where(['id' => $quotation->client_name])->one();

        $related_to_buyer_check =  PreviousTransactions::find()->where(['client_name' => $valuation->client_name_passport])->all();

        $owners_in_valuation = ArrayHelper::map(\app\models\CrmQuotationOwner::find()->where(['quotation_id' => $id,'property_index' => $property_index])->all(), 'id', 'name');
        $owners_valutions_names = ArrayHelper::map(\app\models\ValuationOwners::find()->where(['name' => $owners_in_valuation])->all(), 'id', 'name');


        $owners_previous_data = PreviousTransactions::find()->where(['client_name' => $owners_valutions_names])->all();

        $related_to_client_check =  PreviousTransactions::find()->where(['client_name' => $client->title])->all();
        $related_to_property_check =  PreviousTransactions::find()->where(['building_info' => $valuation->building_info,'unit_number'=>$valuation->unit_number])->all();



        if ($model !== null) {
        } else {
            $model = new CrmValuationConflict();
            if($related_to_buyer_check <> null && count($related_to_buyer_check)>0){
                $model->related_to_buyer = 'Yes';
            }
            if($owners_previous_data <> null && count($owners_previous_data)>0){
                $model->related_to_seller = 'Yes';
            }
            if($related_to_client_check <> null && count($related_to_client_check)>0){
                $model->related_to_client = 'Yes';
            }
            if($related_to_property_check <> null && count($related_to_property_check)>0){
                $model->related_to_property = 'Yes';
            }

        }

        if ($model->load(Yii::$app->request->post())) {
            $model->quotation_id = $id;
            $model->property_index = $property_index;
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['crm-quotations/step_3?id=' . $id.'&property_index='.$property_index]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error)
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {{
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                            }
                        }
                }
            }
        }

        return $this->render('steps/_step3', [
            'model' => $model,
            'valuation' => $valuation,
            'quotation' => $quotation,
            'buyer_data' => $related_to_buyer_check,
            'seller_data' => $owners_previous_data,
            'client_data' => $related_to_client_check,
            'property_data' => $related_to_property_check,
            'property_index' => $property_index,
        ]);
    }

    public function actionStep_4($id,$property_index)
    {
        $this->checkLogin();
        $quotation = $this->findModel($id);
        $model = CrmScheduleInspection::find()->where(['quotation_id' => $id,'property_index' => $property_index])->one();
        $valuation = CrmReceivedProperties::find()->where(['quotation_id' => $id,'property_index' => $property_index])->one();

        if ($model !== null) {

        } else {
            $model = new CrmScheduleInspection();
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->quotation_id = $id;
            $model->property_index = $property_index;
            if ($model->save()) {

                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['crm-quotations/step_4?id=' . $id.'&property_index='.$property_index]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('steps/_step4', [
            'model' => $model,
            'quotation' => $quotation,
            'property_index' => $property_index,
            'valuation' => $valuation,
        ]);
    }

    public function actionStep_5($id,$property_index)
    {
        $this->checkLogin();
        $quotation = $this->findModel($id);
        $model = CrmInspectProperty::find()->where(['quotation_id' => $id,'property_index' => $property_index])->one();
        $valuation = CrmReceivedProperties::find()->where(['quotation_id' => $id,'property_index' => $property_index])->one();
        if ($model !== null) {

        } else {

            $model = new CrmInspectProperty();
            $model->makani_number = $valuation->building->makani_number;
            $model->latitude = $valuation->building->latitude;
            $model->longitude = $valuation->building->longitude;
            $model->property_placement = $valuation->building->property_placement;
            $model->property_visibility = $valuation->building->property_visibility;
            $model->property_exposure = $valuation->building->property_exposure;
            $model->property_category = $valuation->building->property_category;
            $model->property_condition = $valuation->building->property_condition;
            $model->development_type = $valuation->building->development_type;
            $model->finished_status = $valuation->building->finished_status;
            $model->developer_id = $valuation->building->developer_id;
            $model->estimated_age = $valuation->building->estimated_age;
            $model->estimated_remaining_life = $valuation->property->age - $valuation->building->estimated_age;
            $model->number_of_basement = $valuation->building->number_of_basement;
            $model->pool = $valuation->building->pool;
            $model->other_facilities = $valuation->building->other_facilities;
            $model->completion_status = $valuation->building->completion_status;
            $model->landscaping = $valuation->building->landscaping;
            $model->white_goods = $valuation->building->white_goods;
            $model->furnished = $valuation->building->furnished;
            $model->utilities_connected = $valuation->building->utilities_connected;
            $model->location_highway_drive = $valuation->building->location_highway_drive;
            $model->location_school_drive = $valuation->building->location_school_drive;
            $model->location_mall_drive = $valuation->building->location_mall_drive;
            $model->location_sea_drive = $valuation->building->location_sea_drive;
            $model->location_park_drive = $valuation->building->location_park_drive;
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->quotation_id = $id;
            $model->property_index = $property_index;
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['crm-quotations/step_5?id=' . $id.'&property_index='.$property_index]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }


        }

        return $this->render('steps/_step5', [
            'model' => $model,
            'valuation' => $valuation,
            'quotation' => $quotation,
            'property_index' => $property_index,
        ]);
    }

    public function actionStep_6($id,$property_index)
    {
        $this->checkLogin();
        $quotation = $this->findModel($id);
        $configuration = CrmInspectProperty::find()->where(['quotation_id' => $id,'property_index' => $property_index])->one();
        $valuation = CrmReceivedProperties::find()->where(['quotation_id' => $id,'property_index' => $property_index])->one();
        $model = CrmQuotationConfigraions::find()->where(['quotation_id' => $id,'property_index' => $property_index])->one();

        if ($model !== null) {

        } else {
            $model = new CrmQuotationConfigraions();
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->quotation_id = $id;
            $model->property_index = $property_index;

            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['crm-quotations/step_6?id=' . $id.'&property_index='.$property_index]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('steps/_step6', [
            'model' => $model,
            'valuation' => $valuation,
            'configuration' => $configuration,
            'quotation' => $quotation,
            'property_index' => $property_index,
        ]);
    }

    public function actionStep_7($id,$property_index)
    {
        $this->checkLogin();
        $quotation = $this->findModel($id);
        $model = CrmReceivedProperties::find()->where(['quotation_id' => $id,'property_index' => $property_index])->one();

        /* $inspect_property = CrmInspectProperty::find()->where(['quotation_id' => $id,'property_index' => $property_index])->one();
         $schecdule_inspections = CrmScheduleInspection::find()->where(['quotation_id' => $id,'property_index' => $property_index])->one();
         //Fee criteria
         $fee_parameters = array();
         $fee_parameters['clientType']= $quotation->client->client_type;
         $fee_parameters['paymentTerms']= $quotation->advance_payment_terms;
         $fee_parameters['property']= $model->property_id;
         $fee_parameters['city']= $model->building->city;
         $fee_parameters['tenure']= $model->tenure;
         $fee_parameters['complexity']= $model->complexity;
         $fee_parameters['repeat_valuation']= $model->repeat_valuation;
         $fee_parameters['built_up_area']= $inspect_property->built_up_area;
         $fee_parameters['type_of_valuation']= $schecdule_inspections->7inspection_type;
         $fee_parameters['number_of_comparables']= $model->number_of_comparables;
         $fee_parameters['no_of_units']= $model->no_of_units;
         $fee_parameters['land_size']= $model->land_size;


         $amount = yii::$app->propertyCalcHelperFunction->getAmount_new($fee_parameters);


         if(isset($amount['fee'])){
             $model->recommended_fee = $amount['fee'];
         }else{
             $model->recommended_fee = $amount['fee'];
         }


         if($model->quotation_fee <> null){

         }else{
             $model->quotation_fee = $model->recommended_fee;
         }

         if($model->toe_fee <> null && $model->toe_fee !=='0.00'){

         }else{
             $model->toe_fee = $model->quotation_fee;
         }
         if($model->toe_tat <> null && $model->toe_fee !=='0.00'){

         }else{
             $model->toe_tat = $model->tat;
         }*/

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['crm-quotations/step_7?id=' . $id . '&property_index=' . $property_index]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }


        return $this->render('steps/_step7', [
            'model' => $model,
            'quotation' => $quotation,
            'property_index' => $property_index,
        ]);
    }


    public function actionStep_8($id)
    {
        $this->checkLogin();
        $model = $this->findModel($id);
        $quotation = $this->findModel($id);

        //if($quotation->quotation_status >=0 && $quotation->quotation_status < 1) {

        $properties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();

        foreach ($properties as $key => $property_data) {
            $inspect_property = CrmInspectProperty::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
            $schecdule_inspections = CrmScheduleInspection::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
            $property = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
            //Fee criteria
            $fee_parameters = array();
            $fee_parameters['clientType'] = $quotation->client->client_type;
            $fee_parameters['paymentTerms'] = $quotation->advance_payment_terms;
            $fee_parameters['property'] = $property->property_id;
            $fee_parameters['city'] = $property->building->city;
            $fee_parameters['tenure'] = $property->tenure;
            $fee_parameters['complexity'] = str_replace('-','_',$property->complexity);
            $fee_parameters['repeat_valuation'] = $property->repeat_valuation;
            if ($quotation->id > 508) {
                $fee_parameters['built_up_area'] = $property->built_up_area;
            } else {
                $fee_parameters['built_up_area'] = $inspect_property->built_up_area;
            }

            $fee_parameters['type_of_valuation'] = $property->inspection_type;
            $fee_parameters['number_of_comparables'] = $property->number_of_comparables;
            $fee_parameters['no_of_units'] = $property->no_of_units;
            $fee_parameters['land_size'] = ($property->land_size <> null) ? $property->land_size : 0;
            $fee_parameters['upgrades'] = ($property->upgrades <> null) ? $property->upgrades : 0;
            $fee_parameters['other_intended_users'] = ($quotation->other_intended_users <> null) ? 'yes' : 'no';
            $fee_parameters['approach_type'] = Yii::$app->appHelperFunctions->valuationApproachListArr[$property->valuation_approach];

             $amount = yii::$app->propertyCalcHelperFunction->getAmount_auto($fee_parameters);
             echo "<pre>";
             print_r($amount);
             die;
            $ApprovedArr = [2, 4];

            if (!in_array($model->quotation_status, $ApprovedArr)) {

                if ($quotation->id > 938) {
                   // if ($property_data->id == 2234) {
                    $amount = yii::$app->propertyCalcHelperFunction->getAmount_new($fee_parameters);

                   // }
                } else {

                        $amount = yii::$app->propertyCalcHelperFunction->getAmount_new_938($fee_parameters);

                }


                $tat = $property->property->tat;

                if ($quotation->client->client_type == 'bank') {

                    if ($quotation->id > 938) {
                        $fee_array = yii::$app->propertyCalcHelperFunction->getAmount_new($fee_parameters);
                    } else {

                            $fee_array = Yii::$app->appHelperFunctions->getClientRevenueQoutaion($quotation->client_name, $property->inspection_type, $property->tenure, $property->building->city);

                    }
                    //
                    /*  echo "<pre>";
                      print_r($fee_array);
                      die;*/
                    /*if($fee_array['tat'] <> null){
                        $tat = $fee_array['tat'];
                    }*/


                    if ($fee_array['fee'] <> null) {
                        $model->relative_discount_toe = $model->relative_discount;
                        if ($model->relative_discount <> null) {

                        } else {
                            $model->relative_discount = $quotation->client->relative_discount;
                        }
                        if ($model->relative_discount_toe <> null) {

                        } else {
                            $model->relative_discount_toe = $quotation->client->relative_discount;
                        }
                        $amount['fee'] = $fee_array['fee'];


                    } else {
                        //$amount = yii::$app->propertyCalcHelperFunction->getAmount_new($fee_parameters);
                        if ($quotation->id > 938) {
                            $amount = yii::$app->propertyCalcHelperFunction->getAmount_new($fee_parameters);
                        } else {
                            $amount = yii::$app->propertyCalcHelperFunction->getAmount_new_938($fee_parameters);
                        }
                    }
                } else {
                    //$amount = yii::$app->propertyCalcHelperFunction->getAmount_new($fee_parameters);
                    if ($quotation->id > 938) {
                        $amount = yii::$app->propertyCalcHelperFunction->getAmount_new($fee_parameters);
                    } else {
                        $amount = yii::$app->propertyCalcHelperFunction->getAmount_new_938($fee_parameters);
                    }
                }
                if ($id == 167) {
                    //  $amount = yii::$app->propertyCalcHelperFunction->getAmount_new($fee_parameters);
                    if ($quotation->id > 938) {
                        $amount = yii::$app->propertyCalcHelperFunction->getAmount_new($fee_parameters);
                    } else {
                        $amount = yii::$app->propertyCalcHelperFunction->getAmount_new_938($fee_parameters);
                    }
                }


                if (isset($amount['fee'])) {
                    $property->recommended_fee = $amount['fee'];
                } else {
                    $property->recommended_fee = $amount['fee'];
                }


                if ($property->quotation_fee <> null) {

                } else {
                    $property->quotation_fee = $property->recommended_fee;
                }

                if ($property->tat <> null || $property->tat == 0) {

                } else {
                    $property->tat = $tat;
                }
                if ($property->toe_fee <> null && $property->toe_fee !== '0.00') {

                } else {
                    $property->toe_fee = $property->quotation_fee;
                }
                if ($property->toe_tat <> null && $property->toe_fee !== '0.00') {

                } else {
                    $property->toe_tat = $property->tat;
                }

                if (!$property->save()) {
                    echo "<pre>";
                    print_r($property);
                    print_r($property->errors);
                    die;
                }
            }
            //  }


        }


        $recommended_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('recommended_fee');
        $quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');
        $quotation_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('tat');

        $toe_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_fee');
        $toe_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_tat');

        $receivedProperties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();

        $model->final_fee_approved = $quotation_fee_total;
        $model->quotation_turn_around_time = $quotation_tat_total;
        $model->save();
        $quotation_readonly = '';
        $toe_readonly = '';
        if(($model->quotation_status >=0 && $model->quotation_status < 1) || ($model->status_approve == 'Pending') || ($model->status_approve == 'Reject')){
            $quotation_readonly = '';
        }else{
            $quotation_readonly = 'readonly';
        }

        if($model->quotation_status >1 && $model->quotation_status <= 2){
            $toe_readonly = '';
        }else{
            $toe_readonly = 'readonly';
        }

        /*if($model->toe_fee <> null){

        }else{
            $model->toe_fee = $model->recommended_fee;
        }*/

        if($model->quotation_status==11){
            $model->status_approve = 'cancelled';
        }else if($model->quotation_status==12){
            $model->status_approve = 'regretted';
        }



        if ($model->load(Yii::$app->request->post())) {
            // echo "<pre>"; print_r($model); echo "</pre>"; die();


            $date = date("Y-m-d h:i:s");

            if($model->status_approve == 'Approve'){
                $model->quotation_status = 2;
                $model->approved_date = $date;

            }else if($model->status_approve == 'Reject'){
                $model->quotation_status = 7;
                $model->quotation_rejected_date = $date;
            }
            else if($model->status_approve == 'Pending'){
                $model->quotation_status = 9;
            }
            else if($model->status_approve == 'on-hold'){
                $model->quotation_status = 10;
                $model->on_hold_date = $date;
            }
            else if($model->status_approve == 'cancelled'){
                $model->quotation_status = 11;
                $model->cancelled_date = $date;
            }
            else if($model->status_approve == 'regretted'){
                $model->quotation_status = 12;
                $model->regretted_date = $date;
            }
            else{
                unset($model->status_approve);
            }
            $model->relative_discount_toe = $model->relative_discount;

            if ($model->quotation_status<>null) {
                $model->status_change_date = $date;
            }else{
            }

            /*if($model->id == 1191){

                $model->relative_discount = '0';
                $model->relative_discount_toe = '0';

            }*/
            if ($model->save()) {

                Yii::$app->crmQuotationHelperFunctions->addStatusHistory($model);



                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['crm-quotations/step_8?id='.$id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        // echo "<pre>";
        // print_r($model);
        // echo "</pre>";
        // die();


        return $this->render('steps/_step8', [
            'model' => $model,
            'quotation_readonly' => $quotation_readonly,
            'toe_readonly' => $toe_readonly,
            'recommended_fee' => $recommended_fee_total,
            'quotation_fee_total' => $quotation_fee_total,
            'quotation_tat_total' => $quotation_tat_total,
            'toe_fee_total' => $toe_fee_total,
            'toe_tat_total' => $toe_tat_total,
            'receivedProperties' => $receivedProperties,
            'quotation' => $model,
        ]);
    }

    public function actionStep_9_01_02_2022($id)
    {
        $this->checkLogin();
        $model = $this->findModel($id);
        $quotation = $this->findModel($id);

        if($quotation->quotation_status >=0 && $quotation->quotation_status < 1) {

            $properties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();

            foreach ($properties as $key => $property_data) {
                $inspect_property = CrmInspectProperty::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
                $schecdule_inspections = CrmScheduleInspection::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
                $property = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
                //Fee criteria
                $fee_parameters = array();
                $fee_parameters['clientType'] = $quotation->client->client_type;
                $fee_parameters['paymentTerms'] = $quotation->advance_payment_terms;
                $fee_parameters['property'] = $property->property_id;
                $fee_parameters['city'] = $property->building->city;
                $fee_parameters['tenure'] = $property->tenure;
                $fee_parameters['complexity'] = $property->complexity;
                $fee_parameters['repeat_valuation'] = $property->repeat_valuation;
                if($quotation->id > 508){
                    $fee_parameters['built_up_area'] = $property->built_up_area;
                }else{
                    $fee_parameters['built_up_area'] = $inspect_property->built_up_area;
                }
                // $fee_parameters['built_up_area'] = $inspect_property->built_up_area;
                $fee_parameters['type_of_valuation'] = $property->inspection_type;
                $fee_parameters['number_of_comparables'] = $property->number_of_comparables;
                $fee_parameters['no_of_units'] = $property->no_of_units;
                $fee_parameters['land_size'] = $property->land_size;
                $fee_parameters['upgrades'] = ($property->upgrades <> null)?$property->upgrades:0 ;

                if($quotation->id >938) {
                    $amount = yii::$app->propertyCalcHelperFunction->getAmount_new($fee_parameters);
                }else{
                    $amount = yii::$app->propertyCalcHelperFunction->getAmount_new_938($fee_parameters);
                }


                if (isset($amount['fee'])) {
                    $property->recommended_fee = $amount['fee'];
                } else {
                    $property->recommended_fee = $amount['fee'];
                }


                if ($property->quotation_fee <> null) {

                } else {
                    $property->quotation_fee = $property->recommended_fee;
                }

                if ($property->toe_fee <> null && $property->toe_fee !== '0.00') {

                } else {
                    $property->toe_fee = $property->quotation_fee;
                }
                if ($property->toe_tat <> null && $property->toe_fee !== '0.00') {

                } else {
                    $property->toe_tat = $property->tat;
                }

                $property->save();
            }
        }





        $recommended_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('recommended_fee');
        $quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');
        $quotation_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('tat');

        $toe_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_fee');
        $toe_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_tat');

        $receivedProperties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();
//Start By Usama
        $discount = 0;
        $VAT = 0;
        if ($model->relative_discount_toe!=null) {
            $discount =  yii::$app->quotationHelperFunctions->getDiscountRupee($toe_fee_total,$model->relative_discount_toe);
        }
        $netValuationFee = $toe_fee_total-$discount;
        if($model->client->vat == 1){
            $VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
        }
        $finalFeePayable = $netValuationFee+$VAT;

        $model->grand_final_toe_fee = $finalFeePayable;
        //End By Usama
        $model->final_fee_approved = $quotation_fee_total;
        $model->quotation_turn_around_time = $quotation_tat_total;
        $model->toe_final_fee = $toe_fee_total;
        $model->toe_final_turned_around_time = $toe_tat_total;

        $model->save();
        $quotation_readonly = '';
        $toe_readonly = '';
        if($model->quotation_status >=0 && $model->quotation_status < 1){
            $quotation_readonly = '';
        }else{
            $quotation_readonly = 'readonly';
        }

        if(($model->quotation_status >1 && $model->quotation_status <= 2) || ($model->status_approve_toe == 'Pending') || ($model->status_approve_toe == 'Reject')){
            $toe_readonly = '';
        }else{
            $toe_readonly = 'readonly';
        }

        /*if($model->toe_fee <> null){

        }else{
            $model->toe_fee = $model->recommended_fee;
        }*/



        if ($model->load(Yii::$app->request->post())) {
            $date = date("Y-m-d h:i:s");
            if($model->status_approve_toe == 'Approve'){
                $model->quotation_status = 4;
            }else if($model->status_approve_toe == 'Reject'){
                $model->quotation_status = 8;
                $model->toe_rejected_date = $date;

            }else if($model->status_approve_toe == 'Pending'){
                $model->quotation_status = 9;
            }

            if ($model->quotation_status<>null) {
                $model->status_change_date = $date;
            }else{
            }


            if ($model->save()) {
                Yii::$app->crmQuotationHelperFunctions->addStatusHistory($model);
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['crm-quotations/step_9?id='.$id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }


        return $this->render('steps/_step9', [
            'model' => $model,
            'quotation_readonly' => $quotation_readonly,
            'toe_readonly' => $toe_readonly,
            'recommended_fee' => $recommended_fee_total,
            'quotation_fee_total' => $quotation_fee_total,
            'quotation_tat_total' => $quotation_tat_total,
            'toe_fee_total' => $toe_fee_total,
            'toe_tat_total' => $toe_tat_total,
            'receivedProperties' => $receivedProperties,
            'quotation' => $model,
        ]);
    }

    public function actionStep_9($id)
    {
        $this->checkLogin();
        $model = $this->findModel($id);
        $quotation = $this->findModel($id);

        if($quotation->quotation_status >=0 && $quotation->quotation_status < 1) {

            $properties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();

            foreach ($properties as $key => $property_data) {
                $inspect_property = CrmInspectProperty::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
                $schecdule_inspections = CrmScheduleInspection::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
                $property = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
                //Fee criteria
                $fee_parameters = array();
                $fee_parameters['clientType'] = $quotation->client->client_type;
                $fee_parameters['paymentTerms'] = $quotation->advance_payment_terms;
                $fee_parameters['property'] = $property->property_id;
                $fee_parameters['city'] = $property->building->city;
                $fee_parameters['tenure'] = $property->tenure;
                $fee_parameters['complexity'] = $property->complexity;
                $fee_parameters['repeat_valuation'] = $property->repeat_valuation;
                if($quotation->id > 508){
                    $fee_parameters['built_up_area'] = $property->built_up_area;
                }else{
                    $fee_parameters['built_up_area'] = $inspect_property->built_up_area;
                }
                // $fee_parameters['built_up_area'] = $inspect_property->built_up_area;
                $fee_parameters['type_of_valuation'] = $property->inspection_type;
                $fee_parameters['number_of_comparables'] = $property->number_of_comparables;
                $fee_parameters['no_of_units'] = $property->no_of_units;
                $fee_parameters['land_size'] = $property->land_size;
                $fee_parameters['upgrades'] = ($property->upgrades <> null)?$property->upgrades:0 ;
                $ApprovedArr = [4];
                if(!in_array($model->quotation_status, $ApprovedArr)) {
                    if ($quotation->id > 0) {
                        $amount = yii::$app->propertyCalcHelperFunction->getAmount_new($fee_parameters);
                    } else {
                        $amount = yii::$app->propertyCalcHelperFunction->getAmount_new_938($fee_parameters);
                    }


                    if (isset($amount['fee'])) {
                        $property->recommended_fee = $amount['fee'];
                    } else {
                        $property->recommended_fee = $amount['fee'];
                    }


                    if ($property->quotation_fee <> null) {

                    } else {
                        $property->quotation_fee = $property->recommended_fee;
                    }

                    if ($property->toe_fee <> null && $property->toe_fee !== '0.00') {

                    } else {
                        $property->toe_fee = $property->quotation_fee;
                    }
                    if ($property->toe_tat <> null && $property->toe_fee !== '0.00') {

                    } else {
                        $property->toe_tat = $property->tat;
                    }

                    $property->save();
                }
            }
        }

        $recommended_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('recommended_fee');
        $quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');
        $quotation_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('tat');

        $toe_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_fee');
        $toe_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_tat');

        $receivedProperties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();
//Start By Usama
        $discount = 0;
        $VAT = 0;
        if ($model->relative_discount_toe!=null) {
            $discount =  yii::$app->quotationHelperFunctions->getDiscountRupee($toe_fee_total,$model->relative_discount_toe);
        }
        $netValuationFee = $toe_fee_total-$discount;
        if($model->client->vat == 1){
            $VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
        }
        $finalFeePayable = $netValuationFee+$VAT;
        if($model->id > 987) {
            //by usama
            $total_no_of_prperties = count($receivedProperties);
            $model->no_of_property_discount = 0;
            if ($total_no_of_prperties > 5) {
                $result = ProposalMasterFile::find()->where(['heading' => 'No Of Property Discount', 'sub_heading' => 6])->one();
                $model->no_of_property_discount = $result['values'];
            } else if ($total_no_of_prperties > 0 && $total_no_of_prperties < 6) {
                $result = ProposalMasterFile::find()->where(['heading' => 'No Of Property Discount', 'sub_heading' => $total_no_of_prperties])->one();

                $model->no_of_property_discount = $result['values'];
            }

            $clinet_first_time_check = Valuation::find()->where(['client_id' => $model->client_name])->one();
            if ($clinet_first_time_check == null) {
                $first_time_fee = ProposalMasterFile::find()->where(['heading' => 'First Time Discount', 'sub_heading' => 'first-time-discount'])->one();
                $model->first_time_discount = $first_time_fee['values'];
            }

            $general_discount_fee = ProposalMasterFile::find()->where(['heading' => 'General Discount', 'sub_heading' => 'general-discount'])->one();
            if ($general_discount_fee <> null) {
                $model->general_discount = $general_discount_fee['values'];
            }
        }
        //end usama


        $model->grand_final_toe_fee = $finalFeePayable;
        //End By Usama
        $model->final_fee_approved = $quotation_fee_total;
        $model->quotation_turn_around_time = $quotation_tat_total;
        $model->toe_final_fee = $toe_fee_total;
        $model->toe_final_turned_around_time = $toe_tat_total;

        $model->save();
        $quotation_readonly = '';
        $toe_readonly = '';
        if($model->quotation_status >=0 && $model->quotation_status < 1){
            $quotation_readonly = '';
        }else{
            $quotation_readonly = 'readonly';
        }

        if(($model->quotation_status >1 && $model->quotation_status <= 2) || ($model->status_approve_toe == 'Pending') || ($model->status_approve_toe == 'Reject')){
            $toe_readonly = '';
        }else{
            $toe_readonly = 'readonly';
        }

        /*if($model->toe_fee <> null){

        }else{
            $model->toe_fee = $model->recommended_fee;
        }*/



        if ($model->load(Yii::$app->request->post())) {
            $date = date("Y-m-d h:i:s");
            if($model->status_approve_toe == 'Approve'){
                $model->quotation_status = 4;
            }else if($model->status_approve_toe == 'Reject'){
                $model->quotation_status = 8;
                $model->toe_rejected_date = $date;

            }else if($model->status_approve_toe == 'Pending'){
                $model->quotation_status = 9;
            }

            if ($model->quotation_status<>null) {
                $model->status_change_date = $date;
            }else{
            }


            if ($model->save()) {
                Yii::$app->crmQuotationHelperFunctions->addStatusHistory($model);
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['crm-quotations/step_9?id='.$id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }


        return $this->render('steps/_step9', [
            'model' => $model,
            'quotation_readonly' => $quotation_readonly,
            'toe_readonly' => $toe_readonly,
            'recommended_fee' => $recommended_fee_total,
            'quotation_fee_total' => $quotation_fee_total,
            'quotation_tat_total' => $quotation_tat_total,
            'toe_fee_total' => $toe_fee_total,
            'toe_tat_total' => $toe_tat_total,
            'receivedProperties' => $receivedProperties,
            'quotation' => $model,
        ]);
    }

    /*   public function actionStep_9_old($id)
       {
           $model = $this->findModel($id);
           $recommended_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('recommended_fee');
           $quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');
           $quotation_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('tat');
           $quotation_toe_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_fee');
           $quotation_toe_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_tat');
           $receivedProperties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();






                   $model->toe_final_fee = $quotation_toe_fee_total;



               $model->toe_final_turned_around_time = $quotation_toe_tat_total;




           if ($model->load(Yii::$app->request->post())) {
               if($model->status_approve == 'Approve'){
                   $model->quotation_status = 2;
               }
               if ($model->save()) {
                   Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                   return $this->redirect(['crm-quotations/step_9?id='.$id]);
               } else {
                   if ($model->hasErrors()) {
                       foreach ($model->getErrors() as $error) {
                           if (count($error) > 0) {
                               foreach ($error as $key => $val) {
                                   Yii::$app->getSession()->addFlash('error', $val);
                               }
                           }
                       }
                   }
               }
           }


           return $this->render('steps/_step9', [
               'model' => $model,
               'recommended_fee' => $recommended_fee_total,
               'quotation_fee_total' => $quotation_fee_total,
               'quotation_tat_total' => $quotation_tat_total,
               'toe_fee_total' => $quotation_fee_total,
               'toe_tat_total' => $quotation_tat_total,
               'receivedProperties' => $receivedProperties,
               'quotation' => $model,
           ]);
       }*/

    public function actionStep_10_old($id)
    {
        $this->checkLogin();
        $model = $this->findModel($id);
        $receivedProperties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();
        if (Yii::$app->request->isPost) {
            $model->toe_image = UploadedFile::getInstance($model, 'toe_image');

            if ($model->toe_image<>null) {
                if ($model->upload()) {
                    $model->toe_image =$model->toe_image->baseName . '.' . $model->toe_image->extension;
                    if($model->quotation_status > 0 && $model->quotation_status <= 4){
                        $date = date("Y-m-d h:i:s");
                        $model->toe_signed_and_received = 5;
                        $model->toe_signed_and_received_date = $date;
                    }
                    if(!$model->save()){
                        echo "<pre>";
                        print_r($model->errors);
                        die;
                    }else{

                        Yii::$app->crmQuotationHelperFunctions->addStatusHistory($model, 10);
                    }
                    return $this->redirect(['crm-quotations/step_10?id=' . $id]);
                }else {

                }
            }
            else{

                return $this->redirect(['crm-quotations/step_10?id=' . $id]);
            }
        }
        return $this->render('steps/_step10', [
            'model' => $model,
            'receivedProperties' => $receivedProperties,
            'quotation' => $model
        ]);
    }
    public function actionStep_10($id)
    {
        $this->checkLogin();
        $model = $this->findModel($id);
        $old_toe_image = $model->toe_image;
        $receivedProperties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();

        if (Yii::$app->request->isPost) {

            $model->toe_image = UploadedFile::getInstance($model, 'toe_image');
           /* echo "<pre>";
            print_r($_POST['CrmQuotations']['payment_status']);
            die;*/

            if ($model->toe_image<>null) {
                if ($model->upload()) {
                    $model->toe_image =$model->toe_image->baseName . '.' . $model->toe_image->extension;
                    if($model->quotation_status > 0 && $model->quotation_status <= 4){
                        $date = date("Y-m-d h:i:s");
                        $model->toe_signed_and_received = 5;
                        $model->toe_signed_and_received_date = $date;
                    }
                    if(isset($_POST['CrmQuotations']['payment_status']) && $_POST['CrmQuotations']['payment_status']<>null) {

                        $model->payment_status = $_POST['CrmQuotations']['payment_status'];
                    }
                    if(!$model->save()){
                        echo "<pre>";
                        print_r($model->errors);
                        die;
                    }else{
                        Yii::$app->crmQuotationHelperFunctions->addStatusHistory($model, 10);
                        if($model->converted == 0) {
                            $val_idz = [];

                            //convert to valutaion
                            foreach ($receivedProperties as $key => $property) {
                                $property_detail = CrmReceivedProperties::find()->where(['id' => $property->id])->one();
                                $discount = 0;
                                if ($model->relative_discount_toe!=null) {

                                    $discount =  yii::$app->quotationHelperFunctions->getDiscountRupee($property_detail->toe_fee,$model->relative_discount_toe);

                                }

                                $netValuationFee = $property_detail->toe_fee-$discount;
                                $discount_no_of_properties = 0;
                                if ($model->no_of_property_discount!=null) {
                                    $discount_no_of_properties =  yii::$app->quotationHelperFunctions->getDiscountRupeeNoOfProperties($property_detail->toe_fee,$model->no_of_property_discount);
                                    $netValuationFee = $netValuationFee-$discount_no_of_properties;
                                }

                                $VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
                                $finalFeePayable = $netValuationFee+$VAT;

                                $property_detail->converted_fee = $finalFeePayable;
                                $property_detail->save();



                                //Step 1
                                $valuation = new Valuation();
                                $property_detail->instruction_date = $model->instruction_date;
                                $property_detail->target_date = $model->target_date;
                                $valuation->setAttributes($property->attributes);
                                $valuation->instruction_date = $model->instruction_date;
                                $valuation->target_date = $model->target_date;
                                $valuation->no_of_owners = 0;
                                $valuation->valuation_scope = $model->scope_of_service;
                                unset($valuation->id);
                                $valuation->reference_number = Yii::$app->appHelperFunctions->uniqueReference;
                                //  $valuation->reference_number = 'REV-2022-6479';
                                $valuation->client_id = $model->client_name;
                                $valuation->email_subject = $valuation->reference_number;
                                $valuation->unit_number = ($property->unit_number <> null)?$property->unit_number: 0;
                                $valuation->quotation_property = $property->id;
                                $valuation->quotation_id = $id;
                                if($model->payment_status == 1){


                                    $valuation->fee= number_format(($netValuationFee * ('0.'.$model->first_half_payment)), 2, '.', '');
                                }else {
                                    $valuation->fee = $netValuationFee;
                                }
                                $valuation->total_fee = $netValuationFee;
                                $valuation->valuation_status = 1;
                                $valuation->client_fixed_fee_check =1;
                                $valuation->client_name_passport = ($property->client_name_passport <> null)?$property->client_name_passport: '-';
                                $valuation->service_officer_name = ($property->service_officer_name <> null)?$property->service_officer_name: 142;
                                $valuation->land_size = ($property->land_size <> null)?$property->land_size: 0;
                                $valuation->building_info = $property->building_info;
                                $valuation->client_invoice_type = $model->client_invoice_type;
                                if (!$valuation->save()) {
                                    echo "<pre>";
                                    print_r($valuation->errors);
                                    die('here');

                                }else{
                                    $val_idz[] = $valuation->id;
                                }


                                //Step 2
                                $CrmReceivedDocs = CrmReceivedDocs::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();

                                $owners = \app\models\CrmQuotationOwner::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->asArray()->all();
                                $received_docs = \app\models\CrmReceivedDocsFiles::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->asArray()->all();
                                $step_2 = new ReceivedDocs();
                                unset($step_2->id);
                                $step_2->setAttributes($CrmReceivedDocs->attributes);
                                $step_2->valuation_id = $valuation->id;
                                $step_2->save();
                                if (!$step_2->save()) {
                                    echo "<pre>";
                                    print_r($step_2->errors);
                                    die;

                                }


                                // Save all owners terms
                                /* foreach ($owners as $owner_data) {
                                     $owner_data_detail = new ValuationOwners();
                                     $owner_data_detail->name = $owner_data['name'];
                                     $owner_data_detail->percentage = $owner_data['percentage'];
                                     $owner_data_detail->index_id = $owner_data['index_id'];
                                     $owner_data_detail->valuation_id = $valuation->id;
                                     // $owner_data_detail->save();
                                     if (!$owner_data_detail->save()) {
                                         echo "<pre>";
                                         print_r($step_2->errors);
                                         die;

                                     }
                                 }*/

                                // Save all Documents terms
                                foreach ($received_docs as $received_doc) {
                                    $received_data_detail = new ReceivedDocsFiles();
                                    $received_data_detail->document_id = $received_doc['document_id'];
                                    $received_data_detail->attachment = $received_doc['attachment'];
                                    $received_data_detail->valuation_id = $valuation->id;
                                    $received_data_detail->save();
                                }


                                //Step 3
                                $step_3_data = CrmValuationConflict::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();
                                unset($step_3_data->id);
                                $step_3 = new ValuationConflict();
                                $step_3->setAttributes($step_3_data->attributes);
                                $step_3->valuation_id = $valuation->id;
                                if (!$step_3->save()) {
                                    echo "<pre>";
                                    print_r($step_3->errors);
                                    die;
                                }


                                /*     //step 4
                                     $step_4_data = CrmScheduleInspection::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();
                                     unset($step_4_data->id);
                                     $step_4 = new ScheduleInspection();
                                     $step_4->setAttributes($step_4_data->attributes);
                                     $step_4->valuation_id = $valuation->id;
                                     if (!$step_4->save()) {
                                         echo "<pre>";
                                         print_r($step_4->errors);
                                         die;

                                     }

                                     //step 5
                                     $step_5_data = CrmInspectProperty::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();

                                     unset($step_5_data->id);
                                     $step_5 = new InspectProperty();
                                     $step_5->setAttributes($step_5_data->attributes);

                                     $step_5->valuation_id = $valuation->id;;

                                     if (!$step_5->save()) {
                                         echo "<pre>";
                                         print_r($step_5->errors);
                                         die;
                                     }*/

                                /*   //step 6
                                   $step_6_data = CrmQuotationConfigraions::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();
                                   $configurationFiles = \app\models\CrmQuotationConfigraionsFiles::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->asArray()->all();
                                   unset($step_6_data->id);
                                   $step_6 = new ValuationConfiguration();
                                   $step_6->setAttributes($step_6_data->attributes);
                                   $step_6->valuation_id = $valuation->id;
                                   if (!$step_6->save()) {
                                       echo "<pre>";
                                       print_r($step_6->errors);
                                       die;
                                   }*/

                                /* if (!empty($configurationFiles)) {
                                     foreach ($configurationFiles as $config_data) {
                                         $config_detail = new ConfigurationFiles();
                                         $config_detail->type = $config_data['type'];
                                         $config_detail->floor = $config_data['floor'];
                                         $config_detail->flooring = $config_data['flooring'];
                                         $config_detail->ceilings = $config_data['ceilings'];
                                         $config_detail->speciality = $config_data['speciality'];
                                         $config_detail->upgrade = $config_data['upgrade'];
                                         $config_detail->attachment = $config_data['attachment'];
                                         $config_detail->index_id = $config_data['index_id'];
                                         $config_detail->valuation_id = $valuation->id;
                                         $config_detail->checked_image = $config_data['checked_image'];
                                         $config_detail->save();
                                     }
                                 }
                                 $step_6_latest = ValuationConfiguration::find()->where(['valuation_id' => $valuation->id])->one();
                                 if ($step_6_data->over_all_upgrade <> null) {
                                     \Yii::$app->db->createCommand("UPDATE valuation_configuration SET over_all_upgrade=" . $step_6_data->over_all_upgrade . " WHERE id=" . $step_6_latest->id)->execute();
                                 }*/


                            }

                            \Yii::$app->db->createCommand("UPDATE crm_quotations SET converted=" . 1 . " WHERE id=" . $id)->execute();
                            if($model->payment_status == 1) {

                            }
                            // dd($val_idz[0]);
                            if(is_array($val_idz) && $val_idz<>null)
                            {
                                $valuation = Valuation::findOne($val_idz[0]);

                                $curl_handle_1=curl_init();
                                if($model->payment_status == 1) {
                                    curl_setopt($curl_handle_1, CURLOPT_URL,Url::toRoute([ 'valuation/invoice_toe_pdf_half_first', 'id' => $valuation->id ]));
                                }else {
                                    curl_setopt($curl_handle_1, CURLOPT_URL, Url::toRoute(['valuation/invoice_toe_pdf', 'id' => $valuation->id]));
                                }
                                curl_setopt($curl_handle_1, CURLOPT_CONNECTTIMEOUT, 2);

                                curl_setopt($curl_handle_1, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($curl_handle_1, CURLOPT_USERAGENT, 'Maxima');

                                $path = curl_exec($curl_handle_1);
                                $ip = curl_getinfo($curl_handle_1,CURLINFO_PRIMARY_IP);
                                curl_close($curl_handle_1);
                                $attachments[]=$path;

                                $notifyData = [
                                    'client' => $valuation->client,
                                    'uid' => $valuation->id,
                                    'attachments' => $attachments,
                                    'subject' => $valuation->email_subject,
                                    //'valuer' => $valuation->approver->email,
                                    'valuer' => '',
                                    'replacements'=>[
                                        '{clientName}'=>   $valuation->client->title,
                                    ],
                                ];

                                if($model->payment_status == 1) {
                                    \app\modules\wisnotify\listners\NotifyEvent::fire1('quotation.taxinvoiceFirstHalf', $notifyData);
                                }else {
                                  //a  \app\modules\wisnotify\listners\NotifyEvent::fire1('quotation.taxinvoice', $notifyData);
                                }

                              //  \app\modules\wisnotify\listners\NotifyEvent::fire1('quotation.taxinvoiceFirstHalf', $notifyData);

                            }

                        }
                        Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Converted to Valuation successfully'));
                        return $this->redirect(['crm-quotations/step_10?id='.$id]);

                    }
                    return $this->redirect(['crm-quotations/step_10?id=' . $id]);
                }else {

                }
            }
           else{

                return $this->redirect(['crm-quotations/step_10?id=' . $id]);
            }
        }
        return $this->render('steps/_step10', [
            'model' => $model,
            'receivedProperties' => $receivedProperties,
            'quotation' => $model
        ]);
    }

    public function actionStep_11($id)
    {
        $this->checkLogin();
        $model = $this->findModel($id);
        $receivedProperties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();

        if (Yii::$app->request->isPost) {

            $payment_image ='';
            $payment_image2 ='';
            if(UploadedFile::getInstance($model, 'payment_image')<> null){
                $model->payment_image = UploadedFile::getInstance($model, 'payment_image');
                $payment_image =  UploadedFile::getInstance($model, 'payment_image');
            }

            if(UploadedFile::getInstance($model, 'payment_image_2')<> null){
                $model->payment_image_2 = UploadedFile::getInstance($model, 'payment_image_2');
                $payment_image2 =  UploadedFile::getInstance($model, 'payment_image_2');
            }
/*
            $model->payment_image = UploadedFile::getInstance($model, 'payment_image');
            $model->payment_image_2 = UploadedFile::getInstance($model, 'payment_image_2');*/
            if ($payment_image<>null) {
                if ($model->uploadpayment()) {

                    $model->payment_image =$model->payment_image->baseName . '.' . $model->payment_image->extension;

                    if($model->quotation_status >0 && $model->quotation_status <= 5){
                        $date = date("Y-m-d h:i:s");
                        $model->quotation_status = 6;
                        $model->status_change_date = $date;
                        $model->payment_received_date = $date;
                    }
                    if ($model->save()) {

                        $valuations = Valuation::find()->where(['quotation_id' => $model->id])->all();
                        foreach ($valuations as $key => $valuation) {
                            $approvers_data = ValuationApproversData::find()->where(['valuation_id' => $valuation->id, 'approver_type' => 'approver'])->one();
                            Yii::$app->helperFunctions->getModelStepSubmit($approvers_data,$valuation);
                        }





                        Yii::$app->crmQuotationHelperFunctions->addStatusHistory($model);



                        Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Payment updated successfully'));
                        return $this->redirect(['crm-quotations/step_11?id='.$id]);
                    } else {
                        if ($model->hasErrors()) {
                            foreach ($model->getErrors() as $error) {
                                if (count($error) > 0) {
                                    foreach ($error as $key => $val) {
                                        Yii::$app->getSession()->addFlash('error', $val);
                                    }
                                }
                            }
                        }
                    }
                }else {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                    }
                }
            }
            if ($payment_image2<>null) {

                if ($model->uploadpayment2()) {

                    $model->payment_image_2 =$model->payment_image_2->baseName . '.' . $model->payment_image_2->extension;

                    if ($model->save()) {
                        Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Payment updated successfully'));
                        return $this->redirect(['crm-quotations/step_11?id='.$id]);
                    } else {
                        if ($model->hasErrors()) {
                            foreach ($model->getErrors() as $error) {
                                if (count($error) > 0) {
                                    foreach ($error as $key => $val) {
                                        Yii::$app->getSession()->addFlash('error', $val);
                                    }
                                }
                            }
                        }
                    }
                }else {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                    }
                }
            }
            else{
                return $this->redirect(['crm-quotations/step_11?id=' . $id]);
            }
        }
        return $this->render('steps/_step11', [
            'model' => $model,
            'receivedProperties' => $receivedProperties,
            'quotation' => $model,
        ]);
    }



    public function actionRevise($id)
    {
        //step 1
        $model_old = $this->findModel($id);
        $quoatation_history = CrmQuotations::find()->where(['parent_id' => $id])->orderBy(['id' => 'DESC'])->all();
        $quoatation_root = CrmQuotations::find()->where(['id' => $model_old->root_id])->one();
        $quoatation_history_root = Valuation::find()->where(['root_id' => $model_old->root_id])->orderBy(['id' => 'DESC'])->all();


        unset($model_old->id);
        unset($model_old->status);
        unset($model_old->status_approve);
        unset($model_old->status_approve_toe);
        unset($model_old->created_by);
        unset($model_old->created_at);
        unset($model_old->updated_at);
        unset($model_old->updated_by);
        //  unset($model_old->trashed);
        unset($model_old->deleted_at);
        unset($model_old->deleted_by);


        $model_old->parent_id = $id;
        if (($model_old->root_id <> null && $model_old->root_id > 0)) {
            $model_old->reference_number = $quoatation_root->reference_number . '-V-' . (count($quoatation_history_root) + 1);
        } else if ($quoatation_history <> null && count($quoatation_history) > 0) {
            $model_old->reference_number = $model_old->reference_number . '-V-' . (count($quoatation_history) + 1);
        } else {
            $model_old->root_id = $id;
            $model_old->reference_number = $model_old->reference_number . '-V-1';
        }
        $model = new CrmQuotations();
        $model->setAttributes($model_old->attributes);

        $model->parent_id = $id;
        $model->quotation_status = 0;
        $model->status = 0;


        if ($model->save()) {

            $receivedProperties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();

            foreach ($receivedProperties as $key => $property) {
                //step 1
                $property_detail = CrmReceivedProperties::find()->where(['id' => $property->id])->one();
                unset($property_detail->id);
                $new_property_detail = new CrmReceivedProperties();
                $new_property_detail->setAttributes($property_detail->attributes);
                $new_property_detail->quotation_id = $model->id;
                $new_property_detail->property_index =  $property->property_index;


                if (!$new_property_detail->save()) {
                    echo "<pre>";
                    print_r($new_property_detail->errors);
                    die;

                }


                //step 2
                $CrmReceivedDocs = CrmReceivedDocs::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();
                $owners = \app\models\CrmQuotationOwner::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->asArray()->all();
                $received_docs = \app\models\CrmReceivedDocsFiles::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->asArray()->all();
                $step_2 = new CrmReceivedDocs();
                $step_2->setAttributes($CrmReceivedDocs->attributes);
                unset($step_2->id);
                $step_2->quotation_id = $model->id;
                $step_2->property_index = $property->property_index;
                $step_2->save();
                if (!$step_2->save()) {
                    echo "<pre>";
                    print_r($step_2->errors);
                    die;

                }


                // Save all owners terms
                foreach ($owners as $owner_data) {
                    $owner_data_detail = new \app\models\CrmQuotationOwner();
                    $owner_data_detail->name = $owner_data['name'];
                    $owner_data_detail->percentage = $owner_data['percentage'];
                    $owner_data_detail->index_id = $owner_data['index_id'];
                    $owner_data_detail->quotation_id = $model->id;
                    $owner_data_detail->property_index = $property->property_index;
                    // $owner_data_detail->save();
                    if (!$owner_data_detail->save()) {
                        echo "<pre>";
                        print_r($step_2->errors);
                        die;

                    }
                }

                // Save all Documents terms
                foreach ($received_docs as $received_doc) {
                    $received_data_detail = new \app\models\CrmReceivedDocsFiles();
                    $received_data_detail->document_id = $received_doc['document_id'];
                    $received_data_detail->attachment = $received_doc['attachment'];
                    $received_data_detail->quotation_id = $model->id;
                    $received_data_detail->property_index = $property->property_index;
                    $received_data_detail->save();
                }


                //Step 3
                $step_3_data = CrmValuationConflict::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();
                unset($step_3_data->id);
                $step_3 = new CrmValuationConflict();
                $step_3->setAttributes($step_3_data->attributes);
                $step_3->quotation_id = $model->id;
                $step_3->property_index = $property->property_index;
                if (!$step_3->save()) {
                    echo "<pre>";
                    print_r($step_3->errors);
                    die;

                }

                /* //step 4
                 $step_4_data = CrmScheduleInspection::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();
                 unset($step_4_data->id);
                 $step_4 = new CrmScheduleInspection();
                 $step_4->setAttributes($step_4_data->attributes);
                 $step_4->quotation_id = $model->id;
                 $step_4->property_index = $property->property_index;
                 if (!$step_4->save()) {
                     echo "<pre>";
                     print_r($step_4->errors);
                     die;

                 }

                 //step 5
                 $step_5_data = CrmInspectProperty::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();

                 unset($step_5_data->id);
                 $step_5 = new CrmInspectProperty();
                 $step_5->setAttributes($step_5_data->attributes);
                 $step_5->quotation_id = $model->id;
                 $step_5->property_index = $property->property_index;
                 if (!$step_5->save()) {
                     echo "<pre>";
                     print_r($step_5->errors);
                     die;
                 }*/



            }
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->redirect(['crm-quotations/step_0?id=' . $model->id]);
        } else {
            /* echo "<pre>";
             print_r($model->errors);
             die;*/
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        die('Process is in progress.');

    }



    /**
     * Displays a single CrmQuotations model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }



    /**
     * Updates an existing CrmQuotations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CrmQuotations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        // echo $id; die;
        $result = $this->findModel($id);
        $result->softDelete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CrmQuotations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CrmQuotations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CrmQuotations::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModel1($id)
    {
        if (($model = Valuation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionQpdf($id)
    {

        $model = CrmQuotations::findOne($id);
        $condition = true;
        $model->quotationPdf($condition);
    }

    public function actionSendQuotation($id)
    {
        // echo "string";die();
        $model = CrmQuotations::find()->where(['id'=>$id])->one();
        // print_r($model);die();
        if ($model!= null) {
            $condition = false;
            $model->quotationPdf($condition);

            if($model->quotation_status <=0){
                $date = date("Y-m-d h:i:s");
                $model->quotation_status = 1;
                $model->status_change_date = $date;
                $model->quotation_sent_date = $date;
            }

            if ($model->save()) {
                Yii::$app->crmQuotationHelperFunctions->addStatusHistory($model);
                yii::$app->getsession()->addFlash('success', yii::t('app', 'Quotation Send successfully'));
                return  $this->redirect(['crm-quotations/step_0?id=' . $model->id]);

            }
            else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $errors) {

                        foreach ($errors as $key => $val) {
                            //die($val);
                            yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

    }
    public function actionToe($id)
    {
        $model = CrmQuotations::findOne($id);
        $condition = true;
        $model->toePdf($condition);
    }

    public function actionSendToe($id)
    {
        $model = CrmQuotations::find()->where(['id'=>$id])->one();//done
        $properties = yii::$app->quotationHelperFunctions->getMultiplePropertiesNew($id);//done

        //  $model->quotation_status =2;
       // if($model->quotation_status >0 && $model->quotation_status <= 2){
            $date = date("Y-m-d h:i:s");
            $model->quotation_status = 3;
            $model->toe_sent_date = $date;
       // }
        $condition = false;
        $model->toePdf($condition);
        if ($model->save()) {
            Yii::$app->crmQuotationHelperFunctions->addStatusHistory($model);

            yii::$app->getsession()->addFlash('success', yii::t('app', 'TOE send successfully'));
            return  $this->redirect(['crm-quotations/step_0?id=' . $model->id]);
        }
        else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $errors) {

                    foreach ($errors as $key => $val) {
                        //die($val);
                        yii::$app->getSession()->addFlash('error', $val);
                    }
                }
            }
        }

    }
    public function actionQuotationPdf2($id = null, $condition)
    {
        $model = new CrmQuotations;

        $model->GetInvoice($id, $condition);


        // $quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');

        // require_once( __DIR__ .'/../components/tcpdf/ProformaInvoice.php');
        // // create new PDF document
        // $pdf = new \ProformaInvoice(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // // set document information
        // $pdf->SetCreator(PDF_CREATOR);
        // $pdf->SetAuthor('Windmills');
        // $pdf->SetTitle($model->reference_number);
        // $pdf->SetSubject('Quotation');
        // $pdf->SetKeywords($model->reference_number);

        // // set default header data
        // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // // set header and footer fonts
        // $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        // $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // // set default monospaced font
        // $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // // set margins
        // $pdf->SetMargins(10, 35, 10);
        // $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        // $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // // set auto page breaks
        // $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        // $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        // // set default font subsetting mode
        // $pdf->setFontSubsetting(true);

        // // Set font
        // // dejavusans is a UTF-8 Unicode font, if you only need to
        // // print standard ASCII chars, you can use core fonts like
        // // helvetica or times to reduce file size.
        // $pdf->SetFont('times', '', 14, '', true);

        // // Add a page
        // // This method has several options, check the source code documentation for more information.
        // $pdf->AddPage('P','A4');

        // $qpdf=Yii::$app->controller->renderPartial('/crm-quotations/qpdf',[
        //     'id'=>$id,
        //     'model'=>$model,
        //     'quotation_fee_total' => $quotation_fee_total,
        // ]);

        //     $pdf->writeHTML($qpdf, true, false, false, false, '');
        //     $pdf->Output('Quotation-'.$model->reference_number, 'I');

    }
    public function actionQuotationPdf3($id = null)
    {
        $model = $this->findModel($id);
        $quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');

        require_once( __DIR__ .'/../components/tcpdf/ProformaInvoice.php');
        // create new PDF document
        $pdf = new \ProformaInvoice(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle($model->reference_number);
        $pdf->SetSubject('Quotation');
        $pdf->SetKeywords($model->reference_number);

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(10, 35, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('times', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P','A4');

        $qpdf=Yii::$app->controller->renderPartial('/crm-quotations/qpdf',[
            'id'=>$id,
            'model'=>$model,
            'quotation_fee_total' => $quotation_fee_total,
        ]);

        $pdf->writeHTML($qpdf, true, false, false, false, '');
        $pdf->Output('Quotation-'.$model->reference_number, 'I');
        exit;

    }



    public function actionReceivedQuotations()
    {
        $date = date('Y-m-d', strtotime('-5 days'));
        $quotations = \app\models\CrmQuotations::find()
            ->where(['quotation_status'=>0])
            ->Andwhere(['>', 'created_at', $date])
            ->asArray()
            ->all();
        echo "<pre>"; print_r($quotations); echo "</pre>"; die();
    }


    public function actionOnholdQuotations()
    {
        // echo "http://windmills-1.local/crm-quotations/step_0?id=361"; echo "<br>";


        $date = date('Y-m-d', strtotime('-10 days'));
        $quotations = \app\models\CrmQuotations::find()
            ->where(['quotation_status'=>10])
            ->Andwhere(['<=', 'created_at', $date])
            ->asArray()
            ->all();

        $dateTwo = date('Y-m-d', strtotime('-12 days'));
        $quotationsTwo = \app\models\CrmQuotations::find()
            ->where(['quotation_status'=>10])
            ->Andwhere(['<=', 'created_at', $dateTwo])
            ->asArray()
            ->all();

        // echo $dateTwo;
        // echo "<pre>"; print_r($quotationsTwo); echo "</pre>"; die();

        //send email to business team
        $html  = '';
        $i =1;
        foreach ($quotations as $key => $quotation) {
            $html .= $i.') '.Url::base()."/crm-quotations/step_8?id=".$quotation['id']."<br>";
            $i++;
        }

        $notifyData1 = [
            'replacements'=>[
                '{days}'=>10,
                '{status}'=>'On Hold',
                '{html}'=>$html,
            ],
        ];

        $event1 = 'send.QuotationsToBusinessTeam';
        $this->SendFire($notifyData1, $event1);


        //send email to Bilal Moti
        $html2  = '';
        $j=1;
        foreach ($quotationsTwo as $key => $quotation) {
            $html2 .= $j.') '.Url::base()."/crm-quotations/step_8?id=".$quotation['id']."<br>";
            $j++;
        }

        $notifyData2 = [
            'replacements'=>[
                '{days}'=>12,
                '{status}'=>'On Hold',
                '{html}'=>$html2,
            ],
        ];
        $event2 = 'send.QuotationsToCEO';
        $this->SendFire($notifyData2, $event2);


        // die('end');
    }





    public function actionGetActiveQuotations()
    {
        $quotations = \app\models\CrmQuotations::find()
            ->where(['quotation_status'=>0])
            ->where(['<=', 'created_at', date('Y-m-d', strtotime('-5 days'))])
            ->asArray()
            ->all();
        $html1 = '';
        $i=1;
        foreach($quotations as $quotation){
            $html .= $i.') '.Url::base()."/crm-quotations/step_8?id=".$quotation['id']."<br>";
            $i++;
        }

        // echo "<pre>"; print_r($html); echo "</pre>"; die();

        $notifyData1 = [
            'replacements'=>[
                '{days}'=>5,
                '{status}'=>'Active',
                '{html}'=>$html
            ],
        ];
        $event1 = 'send.QuotationsToBusinessTeam';
        $this->SendFire($notifyData1, $event1);

        $quotations2 = \app\models\CrmQuotations::find()
            ->where(['quotation_status'=>0])
            ->where(['<=', 'created_at', date('Y-m-d', strtotime('-7 days'))])
            ->asArray()
            ->all();

        $html1 = '';
        $j=1;
        foreach($quotations2 as $quotation){
            $html .= $j.') '.Url::base()."/crm-quotations/step_8?id=".$quotation['id']."<br>";
            $j++;
        }

        $notifyData2 = [
            'replacements'=>[
                '{days}'=>7,
                '{status}'=>'Active',
                '{html}'=>$html
            ],
        ];
        $event2 = 'send.QuotationsToCEO';
        $this->SendFire($notifyData2, $event2);
    }





    public function SendFire($notifyData='', $keyword='')
    {
        NotifyEvent::fire2($keyword, $notifyData);
    }



    public function actionCopyProperty($id,$property_index)
    {
        // echo "Quotation_id:- ".$id."<br>Property Index:- ".$property_index; die();
        $check = CrmQuotations::findOne($id);
        $countProperties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->count();
        if ($countProperties < $check->no_of_properties) {

            $index = '';
            for($i=0; $i < $check->no_of_properties; $i++){
                // echo $i; echo "<br>"; //die;
                $indexCheck  = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => $i])->one();
                // echo "<pre>"; print_r($indexCheck); echo "</pre>"; die;
                if ($indexCheck==null) {
                    $index = $i;
                    break;
                }
            }

            // $indexes  = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => CrmReceivedProperties::find()->max('id')])->one();
            // $index = $indexes->property_index+1;
            // echo $index; die;

            //saveing step_1
            $property = CrmReceivedProperties::find()->where(['quotation_id' => $id,'property_index' => $property_index])->one();
            $modelCrmReceivedProperties    = new CrmReceivedProperties;
            $modelCrmReceivedProperties->no_of_owners = $property->no_of_owners;
            $modelCrmReceivedProperties->service_officer_name = $property->service_officer_name;
            $modelCrmReceivedProperties->instruction_date = $property->instruction_date;
            $modelCrmReceivedProperties->target_date = $property->target_date;
            $modelCrmReceivedProperties->building_info = $property->building_info;
            $modelCrmReceivedProperties->property_id = $property->property_id;
            $modelCrmReceivedProperties->client_name_passport = $property->client_name_passport;
            $modelCrmReceivedProperties->property_category = $property->property_category;
            $modelCrmReceivedProperties->community = $property->community;
            $modelCrmReceivedProperties->sub_community = $property->sub_community;
            $modelCrmReceivedProperties->tenure = $property->tenure;
            $modelCrmReceivedProperties->unit_number = $property->unit_number;
            $modelCrmReceivedProperties->city = $property->city;
            $modelCrmReceivedProperties->payment_plan = $property->payment_plan;
            $modelCrmReceivedProperties->building_number = $property->building_number;
            $modelCrmReceivedProperties->plot_number = $property->plot_number;
            $modelCrmReceivedProperties->street = $property->street;
            $modelCrmReceivedProperties->floor_number = $property->floor_number;
            $modelCrmReceivedProperties->instruction_person = $property->instruction_person;
            $modelCrmReceivedProperties->land_size = $property->land_size;
            $modelCrmReceivedProperties->purpose_of_valuation = $property->purpose_of_valuation;
            $modelCrmReceivedProperties->valuation_scope = $property->valuation_scope;
            $modelCrmReceivedProperties->special_assumption = $property->special_assumption;
            $modelCrmReceivedProperties->general_asumption = $property->general_asumption;
            $modelCrmReceivedProperties->approval_status = $property->approval_status;
            $modelCrmReceivedProperties->quotation_id = $id;
            $modelCrmReceivedProperties->property_index = $index;
            $modelCrmReceivedProperties->complexity = $property->complexity;
            $modelCrmReceivedProperties->type_of_valuation = $property->type_of_valuation;
            $modelCrmReceivedProperties->number_of_comparables = $property->number_of_comparables;
            $modelCrmReceivedProperties->no_of_units = $property->no_of_units;
            $modelCrmReceivedProperties->repeat_valuation = $property->repeat_valuation;
            $modelCrmReceivedProperties->recommended_fee = $property->recommended_fee;
            $modelCrmReceivedProperties->quotation_fee = $property->quotation_fee;
            $modelCrmReceivedProperties->tat = $property->tat;
            $modelCrmReceivedProperties->toe_fee = $property->toe_fee;
            $modelCrmReceivedProperties->toe_tat = $property->toe_tat;
            $modelCrmReceivedProperties->valuation_approach = $property->valuation_approach;
            $modelCrmReceivedProperties->converted_fee = $property->converted_fee;
            $modelCrmReceivedProperties->inspection_type = $property->inspection_type;
            $modelCrmReceivedProperties->built_up_area = $property->built_up_area;
            if(!$modelCrmReceivedProperties->save()){
                if ($modelCrmReceivedProperties->hasErrors()) {
                    foreach ($modelCrmReceivedProperties->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }

            //saveing step_2
            $propertyReceivedDocs = CrmReceivedDocs::find()->where(['quotation_id' => $id,'property_index' => $property_index])->one();
            $modelCrmReceivedDocs = new CrmReceivedDocs;
            $modelCrmReceivedDocs->quotation_id = $id;
            $modelCrmReceivedDocs->property_index = $index;
            if ($modelCrmReceivedDocs->save()) {
                $owners_data = CrmQuotationOwner::find()->where(['quotation_id' => $id,'property_index' => $property_index])->all();
                if ($owners_data<>null) {
                    foreach ($owners_data as $owner_data) {
                        // echo "<pre>"; print_r($owner_data); echo "</pre>"; die;
                        $owner_data_detail = new CrmQuotationOwner();
                        $owner_data_detail->name = $owner_data['name'];
                        $owner_data_detail->percentage = $owner_data['percentage'];
                        $owner_data_detail->index_id = $owner_data['index_id'];
                        $owner_data_detail->quotation_id = $id;
                        $owner_data_detail->property_index = $index;
                        if (!$owner_data_detail->save()){
                            if ($owner_data_detail->hasErrors()) {
                                foreach ($owner_data_detail->getErrors() as $error) {
                                    if (count($error) > 0) {
                                        foreach ($error as $key => $val) {
                                            Yii::$app->getSession()->addFlash('error', $val);
                                        }
                                    }
                                }
                            }
                        };
                    }
                }

                $documents = CrmReceivedDocsFiles::find()->where(['quotation_id' => $id,'property_index' => $property_index])->all();
                if ($documents<>null) {
                    foreach ($documents as $received_doc) {
                        $owner_data_detail = new CrmReceivedDocsFiles();
                        $owner_data_detail->document_id = $received_doc['document_id'];
                        $owner_data_detail->attachment = $received_doc['attachment'];
                        $owner_data_detail->doc_insert_date = $received_doc['doc_insert_date'];
                        $owner_data_detail->quotation_id = $id;
                        $owner_data_detail->property_index = $index;
                        if (!$owner_data_detail->save()){
                            if ($owner_data_detail->hasErrors()) {
                                foreach ($owner_data_detail->getErrors() as $error) {
                                    if (count($error) > 0) {
                                        foreach ($error as $key => $val) {
                                            Yii::$app->getSession()->addFlash('error', $val);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }else{
                if ($modelCrmReceivedDocs->hasErrors()) {
                    foreach ($modelCrmReceivedDocs->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }

            //saveing step_3
            $conflict = CrmValuationConflict::find()->where(['quotation_id' => $id,'property_index' => $property_index])->one();
            // echo "<pre>"; print_r($conflict); echo "</pre>"; die();
            if ($conflict<>null) {
                $modelConflict = new CrmValuationConflict();
                $modelConflict->quotation_id = $id;
                $modelConflict->property_index = $index;
                $modelConflict->related_to_buyer = $conflict->related_to_buyer;
                $modelConflict->related_to_seller = $conflict->related_to_seller;
                $modelConflict->related_to_client = $conflict->related_to_client;
                $modelConflict->related_to_property = $conflict->related_to_property;
                $modelConflict->related_to_buyer_reason = $conflict->related_to_buyer_reason;
                $modelConflict->related_to_seller_reason = $conflict->related_to_seller_reason;
                $modelConflict->related_to_client_reason = $conflict->related_to_client_reason;
                $modelConflict->related_to_property_reason = $conflict->related_to_property_reason;
                if (!$modelConflict->save()) {
                    if ($modelConflict->hasErrors()) {
                        foreach ($modelConflict->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                    }
                }
            }
            Yii::$app->getSession()->setFlash('success', Yii::t('app','Property Duplicate Successfully'));
            return $this->redirect(['crm-quotations/step_0?id=' . $id]);

        }else{
            Yii::$app->getSession()->setFlash('error', Yii::t('app','All Properties Are Reserved'));
            return $this->redirect(['crm-quotations/step_0?id=' . $id.'&&showAlert=1']);
        }



    }


    public function actionTrashProperty($id,$property_index)
    {
        CrmReceivedProperties::deleteAll(['quotation_id' => $id,'property_index' => $property_index]);
        CrmQuotationOwner::deleteAll(['quotation_id' => $id,'property_index' => $property_index]);
        CrmReceivedDocs::deleteAll(['quotation_id' => $id,'property_index' => $property_index]);
        CrmReceivedDocsFiles::deleteAll(['quotation_id' => $id,'property_index' => $property_index]);
        CrmValuationConflict::deleteAll(['quotation_id' => $id,'property_index' => $property_index]);

        Yii::$app->getSession()->setFlash('success', Yii::t('app','Property Deleted Successfully'));
        return $this->redirect(['crm-quotations/step_0?id=' . $id]);
    }




    public function actionStatusSummary($id=null)
    {
        $logs = \app\models\QuotationStatusHistory::find()->where(['quotation_id'=>$id])->orderBy(['date' => SORT_DESC,])->asArray()->all();
        // echo "<pre>"; print_r($logs); echo "</pre>"; die();
        return $this->render('logs', [
            'logs' => $logs,
        ]);
    }


    public function actionTrashedIndex()
    {
        $searchModel = new \app\models\CrmTrashedQuotationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('trashed-index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionTrashIndex()
    {
        $searchModel = new \app\models\CrmTrashedQuotationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataprovider->pagination = false;


        return $this->render('trashed-index-own', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionPermentlyDelete($id)
    {
        // echo $id; die;

        $model = $this->findModel($id);
        if($model->delete()){
            \app\models\CrmInspectProperty::deleteAll(['quotation_id' => $id]);
            \app\models\CrmQuotationConfigraions::deleteAll(['quotation_id' => $id]);
            \app\models\CrmQuotationConfigraionsFiles::deleteAll(['quotation_id' => $id]);
            \app\models\CrmQuotationOwner::deleteAll(['quotation_id' => $id]);
            \app\models\CrmReceivedDocs::deleteAll(['quotation_id' => $id]);
            \app\models\CrmReceivedDocsFiles::deleteAll(['quotation_id' => $id]);
            \app\models\CrmReceivedProperties::deleteAll(['quotation_id' => $id]);
            \app\models\CrmScheduleInspection::deleteAll(['quotation_id' => $id]);
            \app\models\CrmValuationConflict::deleteAll(['quotation_id' => $id]);
        }
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Quotations Permently Delete successfully'));
        return $this->redirect(['trashed-index']);
    }

    public function actionMoveToActive($id)
    {
        $model = $this->findModel($id);
        Yii::$app->db->createCommand()
            ->update('crm_quotations', ['trashed' => null], ['id' => $model->id])
            ->execute();
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Quotations Active successfully'));
        return $this->redirect(['trashed-index']);
    }


    public function actionDeleteAll()
    {
        $idz = ArrayHelper::map(\app\models\CrmQuotations::find()
            ->where(['trashed' => 1])
            ->select(['id'])
            ->all(), 'id', 'id');
        // echo "<pre>"; print_r($idz); echo "</pre>"; die();
        CrmQuotations::deleteAll(['in' , 'id' , $idz]);

        \app\models\CrmInspectProperty::deleteAll(['quotation_id' => $idz]);
        \app\models\CrmQuotationConfigraions::deleteAll(['quotation_id' => $idz]);
        \app\models\CrmQuotationConfigraionsFiles::deleteAll(['quotation_id' => $idz]);
        \app\models\CrmQuotationOwner::deleteAll(['quotation_id' => $idz]);
        \app\models\CrmReceivedDocs::deleteAll(['quotation_id' => $idz]);
        \app\models\CrmReceivedDocsFiles::deleteAll(['quotation_id' => $idz]);
        \app\models\CrmReceivedProperties::deleteAll(['quotation_id' => $idz]);
        \app\models\CrmScheduleInspection::deleteAll(['quotation_id' => $idz]);
        \app\models\CrmValuationConflict::deleteAll(['quotation_id' => $idz]);

        // echo "<pre>"; print_r($idz); echo "</pre>"; die();
        Yii::$app->getSession()->setFlash('success', Yii::t('app','All Quotations Permently Deleted Form Trash'));
        return $this->redirect(['trashed-index']);
    }


    public function actionActiveAll()
    {
        Yii::$app->db->createCommand()
            ->update('crm_quotations', ['trashed' => null], ['trashed' => 1])
            ->execute();
        Yii::$app->getSession()->setFlash('success', Yii::t('app','All quotations Active successfully'));
        return $this->redirect(['trashed-index']);
    }

    public function actionCheckVerification(){
        $post = Yii::$app->request->post();
        if($post['quotation_id']<>null){
            $countProperties = CrmReceivedProperties::find()->where(['quotation_id'=>$post['quotation_id']])->count();
            $countVerified = CrmReceivedProperties::find()->where(['quotation_id'=>$post['quotation_id'], 'status_verified'=>1])->count();
            if($countProperties == $countVerified){
                $data = [
                    'msg'=>'allowAction',
                ];
                return json_encode($data);
            }else{
                $data = [
                    'msg'=>'actionNotAllowed',
                ];
                return json_encode($data);
            }
        }
    }

    public function actionQuotation_tax_invoice($id)
    {

        $model = CrmQuotations::find()->where(['id' => $id])->one();

        if($model->quotation_status > 3 && $model->quotation_status <= 6) {
            if ($model->toe_signed_and_received == 5) {

        $branch_address= \app\models\Branch::find()->where(['zone_list' => 3510])->one();
        $properties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();
        $netQuotationFee = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');
        $netToeFee = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_fee');
        $countProperties = count($properties);

        $relative_discount_toe = $total_discount = $total_discount_amount = 0;
        $first_time_discount = $discount_no_of_properties = $general_discount = 0;

        if($countProperties > 5) {
            $result = ProposalMasterFile::find()->where(['heading'=>'No Of Property Discount', 'sub_heading'=>6])->one();
            $model->no_of_property_discount =  $result['values'];
        }
        else if($countProperties > 0 && $countProperties < 6) {
            $result = ProposalMasterFile::find()->where(['heading'=>'No Of Property Discount', 'sub_heading'=>$countProperties])->one();
            $model->no_of_property_discount =  $result['values'];
        }

        $clientFirstTimeDiscount = Valuation::find()->where(['client_id'=>$model->client_name])->one();
        if($clientFirstTimeDiscount == null) {
            $first_time_fee = ProposalMasterFile::find()->where(['heading'=>'First Time Discount', 'sub_heading'=>'first-time-discount'])->one();
            $model->first_time_discount =  $first_time_fee['values'];
        }

        $generalDicsountFee = ProposalMasterFile::find()->where(['heading'=>'General Discount', 'sub_heading'=>'general-discount'])->one();
        if ($generalDicsountFee <> null) {
            $model->general_discount =  $generalDicsountFee['values'];
        }


        if ($model->relative_discount_toe > 0) {
            $relative_discount_toe =  yii::$app->quotationHelperFunctions->getDiscountRupee($netToeFee, $model->relative_discount_toe);
            $total_discount= $total_discount + $model->relative_discount_toe;
            $total_discount_amount = $total_discount_amount + $relative_discount_toe;
        }
        $netAfterDiscountFee = $netToeFee-$relative_discount_toe;

        if ($model->first_time_discount!=null) {
            $first_time_discount =  yii::$app->quotationHelperFunctions->getDiscountRupeeNoOfProperties($netToeFee,$model->first_time_discount);
            $netAfterDiscountFee =$netAfterDiscountFee-$first_time_discount;
            $total_discount= $total_discount + $model->first_time_discount;
            $total_discount_amount = $total_discount_amount + $first_time_discount;
        }

        if ($model->no_of_property_discount!=null) {
            $discount_no_of_properties =  yii::$app->quotationHelperFunctions->getDiscountRupeeNoOfProperties($netToeFee,$model->no_of_property_discount);
            $netAfterDiscountFee =$netAfterDiscountFee-$discount_no_of_properties;
            $total_discount= $total_discount + $model->no_of_property_discount;
            $total_discount_amount = $total_discount_amount + $discount_no_of_properties;
        }

        if ($model->general_discount!=null) {
            $general_discount =  yii::$app->quotationHelperFunctions->getDiscountRupeeNoOfProperties($netToeFee,$model->general_discount);
            $netAfterDiscountFee =$netAfterDiscountFee-$general_discount;
            $total_discount= $total_discount + $model->general_discount;
            $total_discount_amount = $total_discount_amount + $general_discount;
        }

        if($model->client->vat == 1){
            $vat = yii::$app->quotationHelperFunctions->getVatTotal($netAfterDiscountFee);
        }
        $finalFeePayable = $netAfterDiscountFee+$vat;

        $fee_to_words = yii::$app->quotationHelperFunctions->numberTowords($finalFeePayable);


        if ($model->advance_payment_terms<>null AND $model->advance_payment_terms>0) {
            $advance_payment_terms = $model->advance_payment_terms;
        }else{
            $advance_payment_terms = '0%';
        }

        $fee = [
            'netQuotationFee'           => $netQuotationFee,
            'netToeFee'                 => $netToeFee,
            'relative_discount_toe'     => $relative_discount_toe,
            'first_time_discount'       => $first_time_discount,
            'discount_no_of_properties' => $discount_no_of_properties,
            'general_discount'          => $general_discount,
            'total_discount'            => $total_discount,
            'total_discount_amount'     => $total_discount_amount,
            'netAfterDiscountFee'       => $netAfterDiscountFee,
            'finalFeePayable'           => $finalFeePayable,
            'fee_to_words'              => $fee_to_words,
            'vat'                       => $vat,
        ];


        require_once( __DIR__ .'/../components/tcpdf/TaxInvoice.php');
        // create new PDF document
        $pdf = new \TaxInvoice(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle($model->reference_number);
        $pdf->SetSubject('Tax Invoice');
        $pdf->SetKeywords($model->reference_number);

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(10, 35, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);
        $pdf->invoice_text = 'Tax Invoice';
        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('times', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P','A4');

        $qpdf=Yii::$app->controller->renderPartial('quotation_tax_invoice',[
            'model'=>$model,
            'branch_address' => $branch_address,
            'properties' => $properties,
            'fee' => $fee,
            'advance_payment_terms' => $advance_payment_terms,
        ]);

        $pdf->writeHTML($qpdf, true, false, false, false, '');
        $pdf->Output('Valuation-'.$model->reference_number, 'I');
        exit;

            }

        }else{
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }

    }
    
    
    public function actionStep_12($id)
    {
        
        $this->checkLogin();
        $model      = $this->findModel($id);
        $quotation  = $this->findModel($id);
        $properties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();
        
        $amount = array();
        $netQuotationFee = 0;
        foreach($properties as $key => $value)
        {
            $inspect_property = CrmInspectProperty::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
            $schecdule_inspections = CrmScheduleInspection::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
            $property = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();


            //Fee criteria
            $fee_parameters = array();
            $fee_parameters['building_title'] = $property->building->title;
            $fee_parameters['clientType'] = $quotation->client->client_type;
            $fee_parameters['paymentTerms'] = $quotation->advance_payment_terms;
            $fee_parameters['property'] = $property->property_id;
            $fee_parameters['city'] = $property->building->city;
            $fee_parameters['tenure'] = $property->tenure;
            $fee_parameters['complexity'] = $property->complexity;
            $fee_parameters['repeat_valuation'] = $property->repeat_valuation;
            if ($quotation->id > 508) {
                $fee_parameters['built_up_area'] = $property->built_up_area;
            } else {
                $fee_parameters['built_up_area'] = $inspect_property->built_up_area;
            }
            $fee_parameters['type_of_valuation'] = $property->inspection_type;
            $fee_parameters['number_of_comparables'] = $property->number_of_comparables;
            $fee_parameters['no_of_units'] = $property->no_of_units;
            $fee_parameters['land_size'] = ($property->land_size <> null) ? $property->land_size : 0;
            $fee_parameters['upgrades'] = ($property->upgrades <> null) ? $property->upgrades : 0;


            $ApprovedArr = [2, 4];

            if (!in_array($model->quotation_status, $ApprovedArr)) {

                if ($quotation->client->client_type == 'bank') {
                    if ($quotation->id > 938) {
                        $fee_array = yii::$app->propertyCalcHelperFunction->getQuotationSummary($fee_parameters);
                    }else {
                        $fee_array = Yii::$app->propertyCalcHelperFunction->getClientRevenueQoutaionSummary($quotation->client_name, $property->inspection_type, $property->tenure, $property->building->city, $property->building->title);
                        // dd($fee_array);
                    }
                                       
                    if ($fee_array['fee'] <> null) {
                        $amount[] = $fee_array;
                    } 
                    else {
                        if ($quotation->id > 938) {
                            $amount[] = yii::$app->propertyCalcHelperFunction->getQuotationSummary($fee_parameters);
                        } else {
                            $amount[] = yii::$app->propertyCalcHelperFunction->getPreviousQuotationSummary($fee_parameters);
                        }
                    }
                }
                else{
                    if ($quotation->id > 938) {
                        $amount[] = yii::$app->propertyCalcHelperFunction->getQuotationSummary($fee_parameters);
                    }else {
                        $amount[] = yii::$app->propertyCalcHelperFunction->getPreviousQuotationSummary($fee_parameters);
                    }
                } 

            }
        }

        // $netQuotationFee = 0;
        // foreach($amount as $amt){
        //     $netQuotationFee += $amt['total_fee'];
        // }


        $total_discount=0;
        $total_discount_amount=0;
        $discount = 0;
        $discount_no_of_properties  = 0;
        $discount_first_time=0;
        $VAT = 0;
        $netValuationFee = 0;
        
        $quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');
        
        if ($model->relative_discount!=null) {
            $discount =  yii::$app->quotationHelperFunctions->getDiscountRupee($quotation_fee_total,$model->relative_discount);
            $total_discount= $total_discount + $model->relative_discount;
            $total_discount_amount = $total_discount_amount + $discount;
        }
        

        $netValuationFee = $quotation_fee_total-$discount;

        if ($model->first_time_discount!=null) {
            $first_time_discount =  yii::$app->quotationHelperFunctions->getDiscountRupeeNoOfProperties($quotation_fee_total,$model->first_time_discount);
            $netValuationFee =$netValuationFee-$first_time_discount;
            $total_discount= $total_discount + $model->first_time_discount;
            $total_discount_amount = $total_discount_amount + $first_time_discount;
        }

        if ($model->no_of_property_discount!=null) {
            $discount_no_of_properties =  yii::$app->quotationHelperFunctions->getDiscountRupeeNoOfProperties($quotation_fee_total,$model->no_of_property_discount);
            $netValuationFee =$netValuationFee-$discount_no_of_properties;
            $total_discount= $total_discount + $model->no_of_property_discount;
            $total_discount_amount = $total_discount_amount + $discount_no_of_properties;
        }

        if ($model->general_discount!=null) {
            $general_discount =  yii::$app->quotationHelperFunctions->getDiscountRupeeNoOfProperties($quotation_fee_total,$model->general_discount);

            $netValuationFee =$netValuationFee-$general_discount;
            $total_discount= $total_discount + $model->general_discount;
            $total_discount_amount = $total_discount_amount + $general_discount;
        }

        if($model->client->vat == 1){
            $VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
        }

        $finalFeePayable = $netValuationFee+$VAT;
        

        return $this->render('steps/_step12', [
            'model' => $model,
            'quotation' => $quotation,
            'amount' => $amount,
            'netQuotationFee' => $quotation_fee_total,
            'netValuationFee' => $netValuationFee,
            'discount' => $discount,
            'first_time_discount' => $first_time_discount,
            'discount_no_of_properties' => $discount_no_of_properties,
            'general_discount' => $general_discount,
            'total_discount' => $total_discount,
            'total_discount_amount' => $total_discount_amount,
            'VAT' => $VAT,
            'finalFeePayable' => $finalFeePayable,
        ]);



    }
}