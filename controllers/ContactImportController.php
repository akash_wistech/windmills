<?php
namespace app\controllers;

use Yii;
use app\components\helpers\DefController;
use yii\web\UploadedFile;
use yii\helpers\Url;
use yii\web\Request;
use yii\helpers\ArrayHelper;


class ContactImportController extends DefController
{
    public function actionImportPropertyOwner()
    {
        $title = 'Import Property Owner Contacts';
        $call_back_url = 'import-property-owner';

        $file = UploadedFile::getInstanceByName('contacts_csv_file');
        if ($file !== null) {
            // dd($file);
            // $filename = 'property_owners-'.$file->baseName .'-'. date('Y-m-d-H-i-s') . '.' . $file->extension;
            $filename = 'property_owners-' . date('Y-m-d-H-i-s') . '.' . $file->extension;
            $file->saveAs('uploads/import_contacts_csv/' . $filename);
            $file_path = (new Request())->getHostInfo() . Url::to('@web/uploads/import_contacts_csv/' . $filename);
            return $this->redirect(['savepropertyowner', 'file_path' => $file_path]);

        }

        return $this->render('import', [
            'title' => $title,
            'call_back_url' => $call_back_url,
        ]);
    }



    public function actionSavepropertyowner($file_path = null, $i = 0, $s = 0)
    {
        $csvFile = new \SplFileObject($file_path, 'r');

        //skip rows
        if ($s > 0) {
            echo "i is: " . $i . ' and s is ' . $s . '<br>';

            for ($j = 0; $j < $s; $j++) {
                $line = $csvFile->fgetcsv();
            }
        }

        while (!$csvFile->eof()) {
            $line = $csvFile->fgetcsv();
            // dd($line);

            $phone2 = '';
            $mobile = '';
            $city = '';
            $address = '';

            $name = (isset($line[0]) ? trim($line[0]) : '');
            $email = (isset($line[1]) ? trim($line[1]) : '');
            $phone = (isset($line[2]) ? trim($line[2]) : '');

            // $country                 = (isset($line[3]) ? trim($line[3]) : '');
            // $city                    = (isset($line[4]) ? trim($line[4]) : '');
            // $address                 = (isset($line[3]) ? trim($line[3]) : '');
            // $phone2                  = (isset($line[3]) ? trim($line[6]) : '');



            $name = trim(preg_replace('/[^A-Za-z0-9 ]/', '', $name));
            // $email = trim(preg_replace('/[^A-Za-z0-9 ]/', '', $email));

            if ($i > 0) {
                // dd($line);
                $companyToSave = $name;
                if ($companyToSave == null) {
                    $name_explode = explode('@', $email);
                    if ($name_explode <> null)
                        $name = $name_explode[0];
                }
                $companyToSave = $name;

                // dd($companyToSave);

                // $city_id = '';
                // if($emirate == 'Dubai') { $city_id = 3510; }
                // if($emirate == 'Abu Dhabi') { $city_id = 3506; }
                // if($emirate == 'Sharjah') { $city_id = 3509; }
                // if($emirate == 'Ajman') { $city_id = 3507; }
                // if($emirate == 'Ras Al Khaimah' || $emirate == 'Ras al-khaimah') { $city_id = 3511; }
                // if($emirate == 'Umm Al Quwain') { $city_id = 3512; }
                // if($emirate == 'Al Ain') { $city_id = 4260; }
                // if($emirate == 'Fujairah') { $city_id = 3508; }

                if ($email <> null) {
                    $user = \app\models\User::find()->where(['email' => trim($email)])->one();
                    // dd($email);
                    if ($user <> null) {
                        //if user exists in database then update property_owner_contact to 1;
                        Yii::$app->db->createCommand()
                            ->update('user', ['property_owner_contact' => 1], ['id' => $user->id])
                            ->execute();
                        //if any valuation exists against this user then update existing_valuation_contact to 1;
                        $valuation = \app\models\Valuation::find()->where(['client_id' => $user->company_id, 'valuation_status' => 5])->one();
                        if ($valuation <> null) {
                            Yii::$app->db->createCommand()
                                ->update('user', ['existing_valuation_contact' => 1], ['company_id' => $valuation->client_id])
                                ->execute();
                        }

                    } else {
                        // dd($line);
                        //save company with the name of contact because there is no company name column in csv file;
                        $companyModel = \app\models\Company::find()->where(['title' => $companyToSave])->one();
                        if ($companyModel == null) {
                            $companyModel = new \app\models\Company;
                            $companyModel->title = $companyToSave;
                            $companyModel->status = 2;
                            $companyModel->allow_for_valuation = 0;
                            $companyModel->save();
                        }

                        // if($companyModel->save()){
                        // dd($companyModel);

                        //save user
                        $userToSave = new \app\models\User;
                        $userToSave->user_type = 0;
                        $userToSave->company_id = $companyModel->id;
                        $userToSave->email = $email;
                        $userToSave->firstname = $name;
                        $userToSave->lastname = '-';
                        $userToSave->status = 1;
                        $userToSave->property_owner_contact = 1;
                        $userToSave->is_save = 1;
                        // dd($userToSave);
                        if ($userToSave->save()) {
                            // dd($userToSave);
                            // dd($line);
                            //now save user profile info
                            $userProfileModel = new \app\models\UserProfileInfo;
                            $userProfileModel->user_id = $userToSave->id;
                            $userProfileModel->primary_contact = 0;
                            $userProfileModel->mobile = $mobile;
                            $userProfileModel->phone1 = $phone;
                            $userProfileModel->phone2 = $phone2;
                            $userProfileModel->city = $city;
                            if ($userProfileModel->save()) {
                                // dd($userProfileModel);
                            } else {
                                if ($userProfileModel->hasErrors()) {
                                    echo "userProfileModel err";
                                    dd($userProfileModel->getErrors());
                                }
                            }
                        } else {
                            if ($userToSave->hasErrors()) {
                                // if($userToSave->getErrors()['email'][0] == "Email is not a valid email address."){
                                // echo $name."<br>";
                                // echo $email."<br>";
                                // echo $phone."<br>";
                                // }
                                // dd($userToSave->getErrors());

                                // save into database table 'OrphanContacts'
                                $orp_contact = \app\models\OrphanContacts::find()->where(['name' => $name])->one();
                                if ($orp_contact == null) {
                                    $orp_contact = new \app\models\OrphanContacts;
                                    $orp_contact->type = 'property_owner_contact';
                                    $orp_contact->name = $name;
                                    $orp_contact->email = $email;
                                    $orp_contact->mobile = $mobile;
                                    $orp_contact->phone_1 = $phone;
                                    $orp_contact->phone_2 = $phone2;
                                    $orp_contact->address = $address;
                                    $orp_contact->country = $country;
                                    $orp_contact->city = $city;
                                    $orp_contact->error = json_encode($userToSave->getErrors());
                                    $orp_contact->created_date = date('Y-m-d H:i:s');
                                    if ($orp_contact->save()) {
                                        // echo "orp_contact saved with err";
                                        // dd($orp_contact);
                                    } else {
                                        if ($orp_contact->hasErrors()) {
                                            echo "orp_contact err";
                                            dd($orp_contact->getErrors());
                                        }
                                    }
                                } else {
                                    // dd("orp company|name exists");
                                }
                            }
                        }
                        // }else{
                        //     if ($companyModel->hasErrors()) {
                        //         echo "companyModel err"; dd($companyModel->getErrors());
                        //     }
                        // }
                    }
                    // dd("stop"); 
                } else {
                    // save into database table 'OrphanContacts'
                    $orp_contact = \app\models\OrphanContacts::find()->where(['name' => $name])->one();
                    if ($orp_contact == null) {
                        $orp_contact = new \app\models\OrphanContacts;
                        $orp_contact->type = 'property_owner_contact';
                        $orp_contact->name = $name;
                        $orp_contact->email = $email;
                        $orp_contact->phone_1 = $phone;
                        $orp_contact->phone_2 = $phone2;
                        $orp_contact->address = $address;
                        $orp_contact->country = $country;
                        $orp_contact->city = $city;
                        $orp_contact->mobile = $mobile;
                        $orp_contact->error = "email not found in csv";
                        $orp_contact->created_date = date('Y-m-d H:i:s');
                        if ($orp_contact->save()) {
                            // dd($orp_contact);
                        } else {
                            if ($orp_contact->hasErrors()) {
                                echo "orp_contact err else";
                                dd($orp_contact->getErrors());
                            }
                        }
                    } else {
                        // dd("orp company|name exists");
                    }
                }
            }

            if ($i >= 300) {
                // dd($i);
                echo "i is: " . $i . ' and s is ' . $s . '<br>';
                echo 'Still Importing...<script>window.location.href="'
                    . Url::to(['savepropertyowner', 'file_path' => $file_path, 'i' => 1, 's' => $s]) .
                    '"</script>';
                die();
                exit();
                break;
            }

            $i++;
            $s++;

        }
        Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Owner Contacts Imported Successfully'));
        return $this->redirect(['import-property-owner']);


    }

    public function falseBehaviors($model)
    {
        foreach ($model->behaviors() as $behavior) {
            $behavior->off(ActiveRecord::EVENT_BEFORE_INSERT);
            $behavior->off(ActiveRecord::EVENT_BEFORE_UPDATE);
            $behavior->off(ActiveRecord::EVENT_AFTER_INSERT);
            $behavior->off(ActiveRecord::EVENT_AFTER_UPDATE);
        }
    }



    public function actionMakeInquiryContacts()
    {
        $quotations = \app\models\CrmQuotations::find()
            ->select(['crm_quotations.client_name'])
            ->leftJoin('valuation', '`valuation`.`client_id` = `crm_quotations`.`client_name`')
            // ->where(['converted' => null])
            ->andWhere(['not in', 'valuation.client_id', 'crm-quotations.client_name'])
            ->groupBy('client_name')->asArray()->all();
        // dd(count($quotations));


        // if(count($quotations)>0){
        //     $i=1;
        //   foreach($quotations as $key=>$quotation){
        //     Yii::$app->db->createCommand()
        //       ->update('user', ['inquiry_valuations_contact' => 1], ['company_id' => $quotation['client_name']])
        //       ->execute();
        //       $i++;
        //   }
        //   dd($i." contacts converted to inquiry_valuations_contact");
        // }
    }






    public function actionImportValuationContacts()
    {
        $title = 'Import Valuation Contacts';
        $call_back_url = 'import-valuation-contacts';

        $file = UploadedFile::getInstanceByName('contacts_csv_file');
        if ($file !== null) {
            // dd($file);
            $filename = 'valuation_contacts-' . $file->baseName . '-' . date('Y-m-d-H-i-s') . '.' . $file->extension;
            $file->saveAs('uploads/import_contacts_csv/' . $filename);
            $file_path = (new Request())->getHostInfo() . Url::to('@web/uploads/import_contacts_csv/' . $filename);
            return $this->redirect(['savevaluationcontacts', 'file_path' => $file_path]);

        }

        return $this->render('import', [
            'title' => $title,
            'call_back_url' => $call_back_url,
        ]);
    }


    public function actionSavevaluationcontacts($file_path = null, $i = 0, $s = 0)
    {
        $csvFile = new \SplFileObject($file_path, 'r');

        //skip rows
        if ($s > 0) {
            echo "i is: " . $i . ' and s is ' . $s . '<br>';

            for ($j = 0; $j < $s; $j++) {
                $line = $csvFile->fgetcsv();
            }
        }



        while (!$csvFile->eof()) {
            $line = $csvFile->fgetcsv();
            // dd($line);
            $name = (isset($line[0]) ? trim($line[0]) : '');
            $mobile = (isset($line[1]) ? trim($line[1]) : '');
            $email = (isset($line[2]) ? trim($line[2]) : '');
            $company = (isset($line[3]) ? trim($line[3]) : '');
            $designation = (isset($line[4]) ? trim($line[4]) : '');

            $name_explode = explode(' ', $name);
            $firstname = $name_explode[0];
            $lastname = $name_explode[1].' '.$name_explode[2];
            

            if ($i > 0) {

                $companyToSave = $company;
                // dd($companyToSave);
                // if($companyToSave==null){
                //     $name_explode = explode('@', $email);
                //     if($name_explode<>null)$company=$name_explode[0];
                // }
                // $companyToSave = $company;  

                if ($email <> null) {
                    $user = \app\models\User::find()->where(['email' => trim($email)])->one();
                    // dd($email);
                    if ($user <> null) {
                        // dd($user->email);
                        //if user exists in database then update property_owner_contact to 1;
                        // Yii::$app->db->createCommand()
                        //     ->update('user', ['valuation_contact' => 1], ['id' => $user->id])
                        //     ->execute();
                        // //if any valuation exists against this user then update existing_valuation_contact to 1;
                        // $valuation = \app\models\Valuation::find()->where(['client_id' => $user->company_id, 'valuation_status' => 5])->one();
                        // if ($valuation <> null) {
                        //     Yii::$app->db->createCommand()
                        //         ->update('user', ['existing_valuation_contact' => 1], ['company_id' => $valuation->client_id])
                        //         ->execute();
                        // }


                        // save into database table 'OrphanContacts'
                        $orp_contact = \app\models\OrphanContacts::find()->where(['email' => $email])->one();
        
                        if ($orp_contact == null) {
                            $orp_contact = new \app\models\OrphanContacts;
                        }
                        $orp_contact->type = 'existing_contact';
                        $orp_contact->name = $name;
                        $orp_contact->email = $email;
                        $orp_contact->mobile = $mobile;
                        $orp_contact->company = $company;
                        $orp_contact->designation = $designation;
                        
    
                        if ($orp_contact->save()) {
                             //dd($orp_contact);
                        } else {
                            if ($orp_contact->hasErrors()) {
                                echo "orp_contact err else";
                                dd($orp_contact->getErrors());
                            }
                        }

                    } else {
                        // dd($line);

                        if ($companyToSave <> null && $companyToSave <> 'NA' && $companyToSave <> 'None') {
                            //save company with the name of contact because there is no company name column in csv file;
                            $companyModel = \app\models\Company::find()->where(['title' => $companyToSave])->one();
                            // dd($companyModel);


                            if ($companyModel == null) {
                                $companyModel = new \app\models\Company;
                                $companyModel->title = $companyToSave;
                                $companyModel->status = 2;
                                $companyModel->nick_name = $companyToSave;
                                $companyModel->segment_type = 3;
                                $companyModel->allow_for_valuation = 1;
                                $companyModel->save();


                                //dd($companyModel);

                                // if($companyModel->save()){
                                //     dd($companyModel);
                                // }

                                //save user
                                $userToSave = new \app\models\User;
                                $userToSave->user_type = 0;
                                $userToSave->company_id = $companyModel->id;
                                $userToSave->email = $email;
                                $userToSave->firstname = $firstname;
                                $userToSave->lastname = $lastname;
                                $userToSave->status = 1;
                                $userToSave->valuation_contact = 1;
                                $userToSave->is_save = 1;
                                //dd($userToSave);

                                if ($userToSave->save()) {
                                    // dd($userToSave);
                                    // dd($line);

                                    $job_title = \app\models\JobTitle::find()->where(['title'=> $designation])->one();
                                    
                                    //now save user profile info
                                    $userProfileModel = new \app\models\UserProfileInfo;
                                    $userProfileModel->user_id = $userToSave->id;
                                    $userProfileModel->job_title_id = $job_title->id;
                                    $userProfileModel->primary_contact = 1;
                                    $userProfileModel->mobile = $mobile;
                                    
                                    if ($userProfileModel->save()) {
                                        // dd($userProfileModel);
                                    } else {
                                        if ($userProfileModel->hasErrors()) {
                                            echo "userProfileModel err";
                                            dd($userProfileModel->getErrors());
                                        }
                                    }
                                } else {
                                    if ($userToSave->hasErrors()) {
                                        // dd($userToSave->getErrors());

                                        // save into database table 'OrphanContacts'
                                        $orp_contact = \app\models\OrphanContacts::find()->where(['email' => $email])->one();
                                        if ($orp_contact == null) {
                                            $orp_contact = new \app\models\OrphanContacts;
                                        }
                                        $orp_contact->type = 'valuation_contact';
                                        $orp_contact->name = $name;
                                        $orp_contact->email = $email;
                                        $orp_contact->mobile = $mobile;
                                        $orp_contact->company = $company;
                                        $orp_contact->designation = $designation;
                                        $orp_contact->error = json_encode($userToSave->getErrors());
                                        if ($orp_contact->save()) {
                                            // echo "orp_contact saved with err";
                                            //     dd($orp_contact);
                                        } else {
                                            if ($orp_contact->hasErrors()) {
                                                echo "orp_contact err";
                                                dd($orp_contact->getErrors());
                                            }
                                        }
                                    }
                                }
                                // }else{
                                //     if ($companyModel->hasErrors()) {
                                //         echo "companyModel err"; dd($companyModel->getErrors());
                                //     }
                                // }
                            }
                            else {

                                // save into database table 'OrphanContacts'
                                $orp_contact = \app\models\OrphanContacts::find()->where(['email' => $email])->one();
            
                                if ($orp_contact == null) {
                                    $orp_contact = new \app\models\OrphanContacts;
                                }
                                $orp_contact->type = 'existingcompany_contact';
                                $orp_contact->name = $name;
                                $orp_contact->email = $email;
                                $orp_contact->mobile = $mobile;
                                $orp_contact->company = $company;
                                $orp_contact->designation = $designation;
                                //dd($orp_contact);
            
                                if ($orp_contact->save()) {
                                    //dd($orp_contact);
                                } else {
                                    if ($orp_contact->hasErrors()) {
                                        echo "orp_contact err else";
                                        dd($orp_contact->getErrors());
                                    }
                                }

                            }
                            

                        }
                        else {
                            // save into database table 'OrphanContacts'
                            $orp_contact = \app\models\OrphanContacts::find()->where(['email' => $email])->one();
        
                            if ($orp_contact == null) {
                                $orp_contact = new \app\models\OrphanContacts;
                            }
                            $orp_contact->type = 'nocompany_contact';
                            $orp_contact->name = $name;
                            $orp_contact->email = $email;
                            $orp_contact->mobile = $mobile;
                            $orp_contact->company = $company;
                            $orp_contact->designation = $designation;
                            //dd($orp_contact);
        
                            if ($orp_contact->save()) {
                                 //dd($orp_contact);
                            } else {
                                if ($orp_contact->hasErrors()) {
                                    echo "orp_contact err else";
                                    dd($orp_contact->getErrors());
                                }
                            }
                        }
                    }
                    // dd("stop"); 
                } else {
                    // save into database table 'OrphanContacts'
                    $orp_contact = \app\models\OrphanContacts::find()->where(['email' => $email])->one();

                    if ($orp_contact == null) {
                        $orp_contact = new \app\models\OrphanContacts;
                    }
                    $orp_contact->type = 'noemail_contact';
                    $orp_contact->name = $name;
                    $orp_contact->email = $email;
                    $orp_contact->mobile = $mobile;
                    $orp_contact->company = $company;
                    $orp_contact->designation = $designation;
                    //dd($orp_contact);

                    if ($orp_contact->save()) {
                        // dd($orp_contact);
                    } else {
                        if ($orp_contact->hasErrors()) {
                            echo "orp_contact err else";
                            dd($orp_contact->getErrors());
                        }
                    }
                }
            }

            if ($i >= 1000) {
                echo 'Still Importing...<script>window.location.href="'
                    . Url::to(['savevaluationcontacts', 'file_path' => $file_path, 'i' => 1, 's' => $s]) .
                    '"</script>';
                die();
                exit();
                break;
            }

            $i++;
            $s++;

        }
        return $this->redirect(['import-valuation-contacts']);


    }



    public function actionConvertOwnerClient()
    {
        $users = \app\models\User::find()->select(['company_id'])->where(['property_owner_contact' => 1])->groupBy('company_id')->AsArray()->all();

        foreach ($users as $key => $user) {
            Yii::$app->db->createCommand()
                ->update('company', ['data_type' => 3], ['id' => $user['company_id']])
                ->execute();
        }

    }

    public function actionConvertNonValClient()
    {
        $client_ids = ArrayHelper::map(\app\models\Valuation::find()->groupBy('client_id')->asArray()->all(), "client_id", "client_id");

        foreach ($client_ids as $id) {
            Yii::$app->db->createCommand()
                ->update('company', ['allow_for_valuation' => 1], ['id' => $id])
                ->execute();
        }

    }


}