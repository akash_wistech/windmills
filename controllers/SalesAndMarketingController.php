<?php

namespace app\controllers;

use Yii;
use app\models\SalesAndMarketing;
use app\models\SalesAndMarketingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\SalesAndMarketingAttendees;
use app\components\helpers\DefController;


/**
* SalesAndMarketingController implements the CRUD actions for SalesAndMarketing model.
*/
class SalesAndMarketingController extends DefController
{
    /**
    * {@inheritdoc}
    */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        // Check if the user is logged in
        if (Yii::$app->user->isGuest) {
            // Redirect to the login page
            return Yii::$app->response->redirect(['site/login'])->send();
        }
    }
    /**
    * Lists all SalesAndMarketing models.
    * @return mixed
    */
    public function actionIndex()
    {
        $searchModel = new SalesAndMarketingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
    * Displays a single SalesAndMarketing model.
    * @param integer $id
    * @return mixed
    * @throws NotFoundHttpException if the model cannot be found
    */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreateRemeeting($parent_id)
    {
        $disabled = false;
        if (Yii::$app->request->post()) {
            // dd(Yii::$app->request->post());
            $model = $this->saveReScheduleMeeting(Yii::$app->request->post(), $parent_id);
            
            $this->StatusVerify($model);
                $this->makeHistory([
                    'model' => $model, 
                    'model_name' => get_class($model),
                    'action' => 'data_created',
                    'verify_field' => 'status_verified',
                ]);

            if ($model->email_status == 0 && $model->status_verified == 1) {
                $meeting_date = date('Y-m-d',strtotime($model->date));
                $date_now = date("Y-m-d");

                if ($meeting_date == $date_now) {
                   // Yii::$app->smHelper->sendEmail($model);
                } else {
                }
            }
                
                Yii::$app->getSession()->setFlash('success', Yii::t('app','Meeting Re-Schedule successfully'));
                return $this->redirect(['index']);
                
            
        }else{
            if($parent_id<>null && $parent_id>0){
                $model = $this->findModel($parent_id);
                if($model->meeting_status == 2){
                    $current_time = date("H:i");
                    $oneHourInCurrentTime = date("H:i", strtotime($current_time . '+1 hour'));
                    $model->lat = $model->latitude;
                    $model->lng = $model->longitude;
                    $model->location = $model->meeting_place;
                    $model->date = date("Y-m-d");
                    $model->time = $current_time;
                    $model->meeting_end_time = $oneHourInCurrentTime;
                    $model->meeting_status = 1;
                    $model->status_verified = 2;
                    $disabled = true;
                    
                    return $this->render('create', [
                        'model' => $model,
                        'disabled' => $disabled,
                    ]);
                }else{
                    Yii::$app->getSession()->setFlash('error', Yii::t('app','Meeting Status is Not postpone!'));
                    return $this->redirect(['index']);
                }
            }
        }

        
    }
    
    /**
    * Creates a new SalesAndMarketing model.
    * If creation is successful, the browser will be redirected to the 'view' page.
    * @return mixed
    */
    public function actionCreate()
    {
        $model = new SalesAndMarketing();
        $disabled = false;

        if ($model->load(Yii::$app->request->post())) {
            // dd(Yii::$app->request->post());
            $this->StatusVerify($model);
            if($model->save()){
                $this->makeHistory([
                    'model' => $model, 
                    'model_name' => get_class($model),
                    'action' => 'data_created',
                    'verify_field' => 'status_verified',
                ]);

                if ($model->email_status == 0 && $model->status_verified == 1) {
                    $meeting_date = date('Y-m-d',strtotime($model->date));
                    $date_now = date("Y-m-d");


                        Yii::$app->smHelper->sendEmail($model);

                }
                
                Yii::$app->getSession()->setFlash('success', Yii::t('app','Information Saved successfully'));
                return $this->redirect(['index']);
            }
            
        }
        return $this->render('create', [
            'model' => $model,
            'disabled' => $disabled,
        ]);
    }


    /**
    * Updates an existing SalesAndMarketing model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $id
    * @return mixed
    * @throws NotFoundHttpException if the model cannot be found
    */
    public function actionUpdate($id)
    {
 
        $model = $this->findModel($id);
        $old_verify_status = $model->status_verified;
        
        // if($model->smAttendees<>null){
        //     $attendeeIds = \yii\helpers\ArrayHelper::getColumn($model->smAttendees, 'sales_and_marketing_attendees_id');
        //     $model->attendees = $attendeeIds;
        // }
        $model->lat = $model->latitude;
        $model->lng = $model->longitude;
        $model->location = $model->meeting_place;
        
        if ($model->load(Yii::$app->request->post())) {
            // dd(Yii::$app->request->post());
            if(isset(Yii::$app->request->post()['SalesAndMarketing']['feedback_email_status'])){
                $model->feedback_email_status = Yii::$app->request->post()['SalesAndMarketing']['feedback_email_status'];
            }else{
                $model->feedback_email_status = 'off';
            }
            
            $this->StatusVerify($model);
            if($model->save()) {
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => get_class($model),
                    'action' => 'data_updated',
                    'verify_field' => 'status_verified',
                    'old_verify_status' => $old_verify_status,
                ]);

                if ($model->email_status == 0 && $model->status_verified == 1) {
                    $meeting_date = date('Y-m-d',strtotime($model->date));
                    $date_now = date("Y-m-d");


                        Yii::$app->smHelper->sendEmail($model);

                }



                /*if($model->status_verified==1 && $model->is_feedback_email_send !=1){
                    $date_now = date("Y-m-d"); // this format is string comparable
                   // echo $meeting_date = date('Y-m-d',strtotime($model->date));


                    if ($date_now > '2023-07-09') {


                        Yii::$app->smHelper->sendFeedbackEmail($model);
                        die('here');
                    }else{

                    }*/


                
                // if($model->feedback_email_value==1 && $model->is_feedback_email_send !=1 && $model->status_verified==1){
                //     //send feedback email to client
                //     Yii::$app->smHelper->sendFeedbackEmail($model);
                // }

                
                
                Yii::$app->getSession()->setFlash('success', Yii::t('app','Information Updated successfully'));
                return $this->redirect(['index']);
            }
        }
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    /**
    * Deletes an existing SalesAndMarketing model.
    * If deletion is successful, the browser will be redirected to the 'index' page.
    * @param integer $id
    * @return mixed
    * @throws NotFoundHttpException if the model cannot be found
    */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        
        return $this->redirect(['index']);
    }
    
    /**
    * Finds the SalesAndMarketing model based on its primary key value.
    * If the model is not found, a 404 HTTP exception will be thrown.
    * @param integer $id
    * @return SalesAndMarketing the loaded model
    * @throws NotFoundHttpException if the model cannot be found
    */
    protected function findModel($id)
    {
        if (($model = SalesAndMarketing::findOne($id)) !== null) {
            return $model;
        }
        
        throw new NotFoundHttpException('The requested page does not exist.');
    }




    public function saveReScheduleMeeting($post_request, $parent_id=null)
    {
        $post_request = $post_request['SalesAndMarketing'];
        // dd($post_request);
        
        $model = $this->findModel($parent_id);
        
        if ($model<>null){
            $newModel = new SalesAndMarketing();
            $excludedAttributes = [
                'id', 'date','time', 'meeting_end_time', 'email_status', 'reschedule', 'latitude', 'longitude', 'status_verified', 'status_verified_at', 'status_verified_by', 'created_at', 'created_by', 'updated_at', 'updated_by','meeting_place','location_url','meeting_status','rejected_reason','postpone_reason',
                'services_required', 'frequency_of_service_required','others_firms_client_have_worked_with','send_corporate_profile','send_fee_tariff','arrange_recent_market_research_presentation','send_market_research_reports_newsletter','arrange_nda','sign_client_nda','arrange_sla','sign_client_sla',
                'is_client_happy','was_our_last_valuation_amount_acceptable','is_fee_acceptable', 'is_tat_acceptable','arrange_meeting_for_ceo', 'arrange_meeting_for_chairman','deliver_business_cards_to_crm_team','procure_through_portal','schedule_next_follow_up_by_meeting','schedule_next_follow_up_by_call_required',
                'starting_km','end_km','parking_fee','attach_km_photos','attach_parking_ticket','feedback_email_status','feedback_email_value','is_feedback_email_send',
            ];
            // dd($excludedAttributes);
    
            foreach ($model->attributes() as $attribute) {
                if (!in_array($attribute, $excludedAttributes)) {
                    $newModel->$attribute = $model->$attribute;
                }
            }       
            
            $newModel->lat = $post_request['lat'];
            $newModel->lng = $post_request['lng'];
            $newModel->meeting_place = $post_request['meeting_place'];
            $newModel->location_url = $post_request['location_url'];
            $newModel->date = $post_request['date'];
            $newModel->time = $post_request['time'];
            $newModel->meeting_end_time = $post_request['meeting_end_time'];
            $newModel->status_verified = ($post_request['status_verified']<>null) ? $post_request['status_verified'] : '';
            $newModel->reschedule = 1;
            $newModel->meeting_status = 1;
            $newModel->reschedule_parent = $model->id;
            // dd($newModel);


            if(isset($post_request['client_attendee']) && $post_request['client_attendee']<>null)
            {
                $newModel->client_attendee = $post_request['client_attendee'];
            }
            if(isset($post_request['wm_attendee']) && $post_request['wm_attendee']<>null)
            {
                $newModel->wm_attendee = $post_request['wm_attendee'];
            }
            
            if ($newModel->save()){

                $attendees = SalesAndMarketingAttendees::find()->where(['sales_and_marketing_id'=>$model->id])->all();
                
                foreach($attendees as $key => $attendee){
                    $newAttendeeModel = new SalesAndMarketingAttendees();
                    $excludedAttendeeAttributes = [ 'id', 'sales_and_marketing_id' ];
                    foreach ($attendee->attributes() as $attribute) {
                        if (!in_array($attribute, $excludedAttendeeAttributes)) {
                            $newAttendeeModel->$attribute = $attendee->$attribute;
                        }
                    }
                    $newAttendeeModel->sales_and_marketing_id = $newModel->id;
                    
                    $newAttendeeModel->save();
                }
            }
            return $newModel;
            // return $this->redirect(['index']);
        }
    }







    public function actionReschedulesMeeting($id)
    {
        $model = $this->findModel($id);
        if ($model<>null){
            $newModel = new SalesAndMarketing();
    
            $excludedAttributes = [
                'id', 
                'time', 
                'meeting_end_time', 
                'email_status', 
                'reschedule', 
                'latitude', 
                'longitude', 
                'status_verified', 
                'status_verified_at', 
                'status_verified_by', 
                'created_at', 
                'created_by', 
                'updated_at', 
                'updated_by'
            ];
    
            foreach ($model->attributes() as $attribute) {
                if (!in_array($attribute, $excludedAttributes)) {
                    $newModel->$attribute = $model->$attribute;
                }
            }        
            $newModel->lat = $model->latitude;
            $newModel->lng = $model->longitude;
            $newModel->time = substr($model->time, 0, 5);
            $newModel->meeting_end_time = substr($model->meeting_end_time, 0, 5);
            $newModel->status_verified = 2;
            $newModel->reschedule = 1;
            $newModel->reschedule_parent = $model->id;
            
            if ($newModel->save()){

                $attendees = SalesAndMarketingAttendees::find()->where(['sales_and_marketing_id'=>$model->id])->all();
                foreach($attendees as $key => $attendee){
                    $newAttendeeModel = new SalesAndMarketingAttendees();
                    $excludedAttendeeAttributes = [ 'id', 'sales_and_marketing_id' ];
                    foreach ($attendee->attributes() as $attribute) {
                        if (!in_array($attribute, $excludedAttendeeAttributes)) {
                            $newAttendeeModel->$attribute = $attendee->$attribute;
                        }
                    }
                    $newAttendeeModel->sales_and_marketing_id = $newModel->id;
                    $newAttendeeModel->save();
                }
            }
            Yii::$app->getSession()->setFlash('success', Yii::t('app','Meeting Re-Schedule successfully'));
            // return $this->redirect(['update', 'id' => $newModel->id]);
            return $this->redirect(['index',]);
        }


        
    }
    
    
    
    public function actionValuationsToInspect()
    {
        $inspections = \app\models\ScheduleInspection::find()
        // ->innerJoinWith('valuation')
        ->innerJoin('valuation', 'schedule_inspection.valuation_id = valuation.id')
        ->where(['<>','valuation.valuation_status', 5])
        ->orderBy(['inspection_date' => SORT_DESC])
        ->all();
        // dd($inspections);

        return $this->render('valuations-to-inspect', [
            'inspections' => $inspections,
        ]);
    }
    
    
    
    
    
    
    
    
    


}
