<?php
// namespace app\commands;
namespace app\controllers;

use Yii;
use app\models\CrmQuotations;
use app\models\CrmQuotationFollowupEmail;
use app\models\CrmQuotationFollowupEmailSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\helpers\DefController;

class CrmQuotationFollowUpEmailController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionQuotationVerifiedFollowUp()
    {
        // $this->checkLogin();
        $model = CrmQuotationFollowupEmail::find()->all();
        $searchModel = new CrmQuotationFollowupEmailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->isPjax) {
            return $this->renderPartial('quotation-verified-follow-up', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
        } else {
            return $this->render('quotation-verified-follow-up', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
        }
    }

    public function actionQuotationVerifiedFollowupEmail()
    {
        // $quotations = \app\models\CrmQuotations::find()->where(['quotation_status'=>2])->all();
        $quotations = \app\models\CrmQuotations::find()->where(['quotation_status'=>2])->andWhere(['>', 'id', 3056])->all();

        // dd($quotations);
        
        if($quotations<>null){
            foreach($quotations as $key => $quotation){

                $follow_ups = CrmQuotationFollowupEmail::find()
                ->where(['quotation_id'=>$quotation->id, 'action'=>'QuotationVerifiedFollowUpEmail'])
                ->asArray()->all();

                $follow_up_checker = CrmQuotationFollowupEmail::find()
                ->where(['quotation_id'=>$quotation->id, 'action'=>'QuotationVerifiedFollowUpEmail'])
                ->asArray()->one();

                
                $no_of_reminder_sent = count($follow_ups);

                if($follow_up_checker<>null){
                    $created_date = date("Y-m-d", strtotime($follow_up_checker['date']));
                }else{
                    $created_date = date("Y-m-d", strtotime($quotation->approved_date));
                }
                $clientSegment = \app\models\ClientSegmentFile::find()
                ->where(['client_type'=>$quotation->client->client_type])
                ->andWhere(['status_verified'=>1])
                ->asArray()->one();
                
                $no_of_reminder = $clientSegment['quotation_followup_email_no'];
                $days_to_add = $clientSegment['quotation_followup_period__days'];

                $threeMinutesAgo = date('Y-m-d H:i:s', strtotime('-3 minutes'));

                
                //add days
                $today_date = date("Y-m-d");
                $follow_up_date = date("Y-m-d", strtotime("$created_date + $days_to_add days"));

                // if($quotation->client->client_type == "bank"){ 
                //     if($quotation->final_fee_approved >5000){
                //         $follow_up_date = date("Y-m-d", strtotime("$created_date + $days_to_add days"));
                //     }
                // }else { 
                //     if($no_of_reminder_sent == 0 ){
                //         $follow_up_date = date("Y-m-d", strtotime("$created_date + $days_to_add days"));
                //     }else if($no_of_reminder_sent == 1){
                //         $follow_up_date = date("Y-m-d", strtotime("$created_date + 7 days"));
                //     }else if($no_of_reminder_sent == 2){
                //         $follow_up_date = date("Y-m-d", strtotime("$created_date + 7 days"));
                //     }else if($no_of_reminder_sent == 3){
                //         $follow_up_date = date("Y-m-d", strtotime("$created_date + 7 days"));
                //     }else if($no_of_reminder_sent == 4){
                //         $follow_up_date = date("Y-m-d", strtotime("$created_date + 7 days"));
                //     }else if($no_of_reminder_sent == 5){
                //         $follow_up_date = date("Y-m-d", strtotime("$created_date + 30 days"));
                //     }
                // }


                // $follow_up_date = '2024-07-26'; //comment when testing is done;
                // dd($follow_up_date,$today_date);

                if (strtotime($follow_up_date) == strtotime($today_date)) {
                    if($no_of_reminder_sent < $no_of_reminder){
                        $notifyData = [
                            'client' => $quotation->client,
                            'attachments' => [],
                            'replacements'=>[
                                '{clientName}'=>$quotation->client->title,
                            ],
                        ];
                        //sending email
                        \app\modules\wisnotify\listners\NotifyEvent::fireQuotationFollowupEmail('quotation.verified', $notifyData);
                        //email sending entry
                        $email_entry = new \app\models\CrmQuotationFollowupEmail;
                        $email_entry->quotation_id  = $quotation->id;
                        $email_entry->client_id     = $quotation->client_name;
                        $email_entry->date          = date('Y-m-d H:i:s');
                        $email_entry->action        = 'QuotationVerifiedFollowUpEmail';
                        $email_entry->created_at    = date('Y-m-d H:i:s');
                        if($email_entry->save()){
                            echo "QuotationVerifiedFollowUpEmail sent";
                        }
                    }else{
                        echo "Total number of reminders reached";
                    }

                }
                else{
                    echo "Date not match";
                }
            }
        }
    }
}