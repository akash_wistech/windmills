<?php

namespace app\controllers;

use Yii;

use yii\filters\VerbFilter;
use app\components\helpers\DefController;
use app\models\InstitutionManager;
use app\models\InstitutionManagerSearch;

/**
 * InstitutionManagerController implements the CRUD actions for Tasks model.
 */
class InstitutionManagerController extends DefController
{
    public function actionIndex()
    {
        $searchModel = new InstitutionManagerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        if(!empty(Yii::$app->request->post()))
        {
            $model = new InstitutionManager();
            $model->institute_name = Yii::$app->request->post()['institute_name'];
            $model->country_id = Yii::$app->request->post()['country_id'];
            $model->status = Yii::$app->request->post()['status'];
            $model->created_by = Yii::$app->user->identity->id;
            $model->created_at = date("Y/m/d H:i:s");
            $model->save();
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {

        $model = InstitutionManager::findOne(['id' => $id]);
        if(!empty(Yii::$app->request->post()))
        {
            $model->institute_name = Yii::$app->request->post()['institute_name'];
            $model->country_id = Yii::$app->request->post()['country_id'];
            $model->status = Yii::$app->request->post()['status'];
            $model->created_by = Yii::$app->user->identity->id;
            $model->created_at = date("Y/m/d H:i:s");
            $model->save();
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = InstitutionManager::findOne(['id' => $id])->delete();

        return $this->redirect(['index']);
    }
}
