<?php

namespace app\controllers;

use Yii;

use yii\filters\VerbFilter;
use app\components\helpers\DefController;
use app\models\TaskManager;
use app\models\TaskManagerSearch;

/**
 * TaskManagerController implements the CRUD actions for Tasks model.
 */
class TaskManagerController extends DefController
{
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        // Check if the user is logged in
        if (Yii::$app->user->isGuest) {
            // Redirect to the login page
            return Yii::$app->response->redirect(['site/login'])->send();
        }
    }
    public function actionIndex()
    {
        $searchModel = new TaskManagerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
       // $dataProvider = $searchModel->search(Yii::$app->user->identity->id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {

        $StartDate = new \DateTime(Yii::$app->request->post()['start_date']);
        $formatStartDate = $StartDate->format('Y-m-d');

        $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
        $formatEndDate = $EndDate->format('Y-m-d');

        if($formatStartDate > $formatEndDate)
        {
            Yii::$app->getSession()->addFlash('error', "End Date Must be greater than Start Date!");
            $referrer = Yii::$app->request->referrer;
            return $this->redirect($referrer ? $referrer : ['index']);
        }

        $user_id = Yii::$app->user->identity->id;

        $user_info = (new \yii\db\Query())
            ->select('user_profile_info.department_id')
            ->from('user_profile_info')
            ->where(['user_profile_info.user_id' => $user_id])
            ->one();

        $dept_info = (new \yii\db\Query())
            ->select(['department.id','department.title'])
            ->from('department')
            ->where(['department.id' => $user_info['department_id']])
            ->one();

        // dd(Yii::$app->request->post()['frequency_start']);

        if( isset(Yii::$app->request->post()['frequency']) && Yii::$app->request->post()['frequency'] == "daily" ){
            $StartDate = new \DateTime(Yii::$app->request->post()['start_date']);
            $formatStartDate = $StartDate->format('Y-m-d');

            $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
            $formatEndDate = $EndDate->format('Y-m-d');

            if($formatStartDate != $formatEndDate)
            {
                Yii::$app->getSession()->addFlash('error', "Start Date & End Date Must be Same for Daily Frequency!");
                $referrer = Yii::$app->request->referrer;
                return $this->redirect($referrer ? $referrer : ['index']);
            }

            // dd(Yii::$app->request->post());

            $frequencStart = new \DateTime(Yii::$app->request->post()['frequency_start']); // Replace with your actual start date
            $endDate = clone $frequencStart;
            $frequencyEnd = new \DateTime(Yii::$app->request->post()['frequency_end']);

            $interval = $frequencStart->diff($frequencyEnd);
            $numberOfDays = $interval->days;

            // dd($numberOfDays);

            $endDate->modify('+'.$numberOfDays.' days'); // Add number of days according to frequency

            $currentDate = clone $frequencStart;

            while ($currentDate <= $endDate) {
                if ($currentDate->format('N') <= 5) {
                    //Adding Tasks
                    $model = new TaskManager();
                    $model->frequency = Yii::$app->request->post()['frequency'];
                    // $model->department = $dept_info['title'];
                    $model->department = Yii::$app->request->post()['department'];
                    $model->module = Yii::$app->request->post()['module'];
                    $model->type = Yii::$app->request->post()['type'];
                    $model->modification = Yii::$app->request->post()['modification'];
                    $model->priority = Yii::$app->request->post()['priority'];
                    $model->description = Yii::$app->request->post()['description'];
                    $model->start_date = $currentDate->format("Y/m/d H:i:s");
                    $model->end_date = $currentDate->format("Y/m/d H:i:s");

                    $dateTime = new \DateTime(Yii::$app->request->post()['meeting_date']);
                    $formattedEndDate = $dateTime->format('Y-m-d');

                    $dateCurrent = new \DateTime($currentDate->format("Y/m/d H:i:s"));
                    $formattedCurrentDate = $dateCurrent->format('Y-m-d');

                    // print_r($formattedEndDate);
                    // echo "<br>";
                    // print_r($formattedCurrentDate);die;

                    if( $formattedCurrentDate == $formattedEndDate  )
                    {
                        $model->meeting_date = Yii::$app->request->post()['meeting_date'];
                        $model->expiry_date = Yii::$app->request->post()['expiry_date'];
                    }else{
                        $model->meeting_date = $currentDate->format("Y/m/d H:i:s");
                        $model->expiry_date = $currentDate->format("Y/m/d H:i:s");
                        // $interval = $dateCurrent->diff($dateTime);
                        // $numberOfDays = $interval->days;

                        // // Add day to the date
                        // $dateTime->add(new \DateInterval('P'.$numberOfDays.'D'));
                        // $model->meeting_date = $dateTime->format("Y/m/d H:i:s");
                    }

                    $model->responsibility = Yii::$app->request->post()['responsibility'];
                    $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                    $model->entered_by = Yii::$app->request->post()['entered_by'];
                    $model->created_by = Yii::$app->request->post()['entered_by'];
                    $model->verified_by = Yii::$app->request->post()['verified_by'];
                    $model->agreed_by = Yii::$app->request->post()['agreed_by'];

                    if(Yii::$app->request->post()['status'] == "completed")
                    {

                        $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                        $formattedTargetDate = $dateTime->format('Y-m-d');

                        $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                        $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                        // print_r($formattedTargetDate);
                        // echo "<br>";
                        // print_r($formatteddateTimeToday);die;

                        if( $formatteddateTimeToday > $formattedTargetDate  )
                        {
                            $model->status = "overdue";
                            $model->completed_date = date("Y/m/d H:i:s");
                        }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                        {
                            $model->status = "completed";
                            $model->completed_date = date("Y/m/d H:i:s");
                        }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                        {
                            $model->status = "completed_ahead";
                            $model->completed_date = date("Y/m/d H:i:s");
                        }
                    }else{
                        $model->status = Yii::$app->request->post()['status'];
                    }

                    $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                    $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                    $model->approved_by = Yii::$app->request->post()['approved_by'];
                    $model->created_at = date("Y/m/d H:i:s");
                    $model->save();
                    //end ending tasks
                }
                $currentDate->modify('+1 day');
            }
            return $this->redirect(['index']);
        }elseif( isset(Yii::$app->request->post()['frequency']) && Yii::$app->request->post()['frequency'] == "monthly" ) {
            $frequencStart = new \DateTime(Yii::$app->request->post()['frequency_start']); // Replace with your actual start date
            $frequencyEnd = new \DateTime(Yii::$app->request->post()['frequency_end']);

            // Calculate the difference in months
            $interval = $frequencStart->diff($frequencyEnd);
            $months = $interval->y * 12 + $interval->m;

            for ($i = 0; $i <= $months; $i++) {
                $startDate = new \DateTime(Yii::$app->request->post()['start_date']);
                $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
                $MeetingDate = new \DateTime(Yii::$app->request->post()['meeting_date']);
                $ExpiryDate = new \DateTime(Yii::$app->request->post()['expiry_date']);

                $currentStartDate = clone $startDate;
                $currentEndDate = clone $EndDate;
                $MeetingDate = clone $MeetingDate;
                $ExpiryDate = clone $ExpiryDate;

                $currentStartDate->modify("+$i months");
                $currentEndDate->modify("+$i months");
                $MeetingDate->modify("+$i months");
                $ExpiryDate->modify("+$i months");

                // echo "Task " .$currentStartDate->format('Y-m-d') .' '. $currentEndDate->format('Y-m-d') . PHP_EOL;
                // echo "<br>";

                $model = new TaskManager();
                $model->frequency = Yii::$app->request->post()['frequency'];
                // $model->department = $dept_info['title'];
                $model->department = Yii::$app->request->post()['department'];
                $model->module = Yii::$app->request->post()['module'];
                $model->type = Yii::$app->request->post()['type'];
                $model->modification = Yii::$app->request->post()['modification'];
                $model->priority = Yii::$app->request->post()['priority'];
                $model->description = Yii::$app->request->post()['description'];
                $model->start_date = $currentStartDate->format("Y/m/d H:i:s");
                $model->end_date = $currentEndDate->format("Y/m/d H:i:s");
                $model->meeting_date = $MeetingDate->format("Y/m/d H:i:s");
                $model->expiry_date = $ExpiryDate->format("Y/m/d H:i:s");

                $model->responsibility = Yii::$app->request->post()['responsibility'];
                $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                $model->entered_by = Yii::$app->request->post()['entered_by'];
                $model->created_by = Yii::$app->request->post()['entered_by'];
                $model->verified_by = Yii::$app->request->post()['verified_by'];
                $model->agreed_by = Yii::$app->request->post()['agreed_by'];


                if(Yii::$app->request->post()['status'] == "completed")
                {

                    $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                    $formattedTargetDate = $dateTime->format('Y-m-d');

                    $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                    $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                    // print_r($formattedTargetDate);
                    // echo "<br>";
                    // print_r($formatteddateTimeToday);die;

                    if( $formatteddateTimeToday > $formattedTargetDate  )
                    {
                        $model->status = "overdue";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                    {
                        $model->status = "completed";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                    {
                        $model->status = "completed_ahead";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }
                }else{
                    $model->status = Yii::$app->request->post()['status'];
                }

                $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                $model->approved_by = Yii::$app->request->post()['approved_by'];
                $model->created_at = date("Y/m/d H:i:s");
                $model->save();
            }
            return $this->redirect(['index']);
        }elseif( isset(Yii::$app->request->post()['frequency']) && Yii::$app->request->post()['frequency'] == "yearly" ) {
            $frequencStart = new \DateTime(Yii::$app->request->post()['frequency_start']); // Replace with your actual start date
            $frequencyEnd = new \DateTime(Yii::$app->request->post()['frequency_end']);

            // Calculate the difference in years
            $interval = $frequencStart->diff($frequencyEnd);
            $numberOfYears = $interval->y;

            for ($i = 0; $i <= $numberOfYears; $i++) {
                $startDate = new \DateTime(Yii::$app->request->post()['start_date']);
                $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
                $MeetingDate = new \DateTime(Yii::$app->request->post()['meeting_date']);
                $ExpiryDate = new \DateTime(Yii::$app->request->post()['expiry_date']);
                $currentStartDate = clone $startDate;
                $currentEndDate = clone $EndDate;
                $MeetingDate = clone $MeetingDate;
                $ExpiryDate = clone $ExpiryDate;

                $currentStartDate->modify("+$i year");
                $currentEndDate->modify("+$i year");
                $MeetingDate->modify("+$i year");
                $ExpiryDate->modify("+$i year");

                // echo "Task " .$currentStartDate->format('Y-m-d') .' '. $currentEndDate->format('Y-m-d') . PHP_EOL;
                // echo "<br>";

                $model = new TaskManager();
                $model->frequency = Yii::$app->request->post()['frequency'];
                // $model->department = $dept_info['title'];
                $model->department = Yii::$app->request->post()['department'];
                $model->module = Yii::$app->request->post()['module'];
                $model->type = Yii::$app->request->post()['type'];
                $model->modification = Yii::$app->request->post()['modification'];
                $model->priority = Yii::$app->request->post()['priority'];
                $model->description = Yii::$app->request->post()['description'];
                $model->start_date = $currentStartDate->format("Y/m/d H:i:s");
                $model->end_date = $currentEndDate->format("Y/m/d H:i:s");
                $model->meeting_date = $MeetingDate->format("Y/m/d H:i:s");
                $model->expiry_date = $ExpiryDate->format("Y/m/d H:i:s");

                $model->responsibility = Yii::$app->request->post()['responsibility'];
                $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                $model->entered_by = Yii::$app->request->post()['entered_by'];
                $model->created_by = Yii::$app->request->post()['entered_by'];
                $model->verified_by = Yii::$app->request->post()['verified_by'];
                $model->agreed_by = Yii::$app->request->post()['agreed_by'];

                if(Yii::$app->request->post()['status'] == "completed")
                {

                    $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                    $formattedTargetDate = $dateTime->format('Y-m-d');

                    $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                    $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                    // print_r($formattedTargetDate);
                    // echo "<br>";
                    // print_r($formatteddateTimeToday);die;

                    if( $formatteddateTimeToday > $formattedTargetDate  )
                    {
                        $model->status = "overdue";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                    {
                        $model->status = "completed";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                    {
                        $model->status = "completed_ahead";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }
                }else{
                    $model->status = Yii::$app->request->post()['status'];
                }

                $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                $model->approved_by = Yii::$app->request->post()['approved_by'];
                $model->created_at = date("Y/m/d H:i:s");
                $model->save();
            }
            return $this->redirect(['index']);
        }elseif( isset(Yii::$app->request->post()['frequency']) && Yii::$app->request->post()['frequency'] == "weekly" ) {

            $frequencStart = new \DateTime(Yii::$app->request->post()['frequency_start']); // Replace with your actual start date
            $frequencyEnd = new \DateTime(Yii::$app->request->post()['frequency_end']);

            $currentDate = clone $frequencStart;


            // Iterate through each week and perform the task
            $count=0;
            while ($currentDate <= $frequencyEnd) {

                if($count==0)
                {
                    $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
                    $MeetingDate = new \DateTime(Yii::$app->request->post()['meeting_date']);
                    $ExpiryDate = new \DateTime(Yii::$app->request->post()['expiry_date']);

                    $currentEndDate = clone $EndDate;
                    $currentMeetingDate = clone $MeetingDate;
                    $currentExpiryDate = clone $MeetingDate;
                }


                $model = new TaskManager();
                $model->frequency = Yii::$app->request->post()['frequency'];
                // $model->department = $dept_info['title'];
                $model->department = Yii::$app->request->post()['department'];
                $model->module = Yii::$app->request->post()['module'];
                $model->type = Yii::$app->request->post()['type'];
                $model->modification = Yii::$app->request->post()['modification'];
                $model->priority = Yii::$app->request->post()['priority'];
                $model->description = Yii::$app->request->post()['description'];
                $model->start_date = $currentDate->format("Y/m/d H:i:s");
                $model->end_date = $currentEndDate->format("Y/m/d H:i:s");
                $model->meeting_date = $currentMeetingDate->format("Y/m/d H:i:s");
                $model->expiry_date = $currentExpiryDate->format("Y/m/d H:i:s");

                $model->responsibility = Yii::$app->request->post()['responsibility'];
                $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                $model->entered_by = Yii::$app->request->post()['entered_by'];
                $model->created_by = Yii::$app->request->post()['entered_by'];
                $model->verified_by = Yii::$app->request->post()['verified_by'];
                $model->agreed_by = Yii::$app->request->post()['agreed_by'];

                if(Yii::$app->request->post()['status'] == "completed")
                {

                    $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                    $formattedTargetDate = $dateTime->format('Y-m-d');

                    $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                    $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                    // print_r($formattedTargetDate);
                    // echo "<br>";
                    // print_r($formatteddateTimeToday);die;

                    if( $formatteddateTimeToday > $formattedTargetDate  )
                    {
                        $model->status = "overdue";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                    {
                        $model->status = "completed";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                    {
                        $model->status = "completed_ahead";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }
                }else{
                    $model->status = Yii::$app->request->post()['status'];
                }

                $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                $model->approved_by = Yii::$app->request->post()['approved_by'];
                $model->created_at = date("Y/m/d H:i:s");
                $model->save();


                $currentDate->modify('+7 days');
                $currentEndDate->modify('+7 days');
                $currentMeetingDate->modify('+7 days');
                $count++;
            }
            return $this->redirect(['index']);

        }elseif( isset(Yii::$app->request->post()['frequency']) && Yii::$app->request->post()['frequency'] == "bi-annual" ) {

            $frequencStart = new \DateTime(Yii::$app->request->post()['frequency_start']); // Replace with your actual start date
            $frequencyEnd = new \DateTime(Yii::$app->request->post()['frequency_end']);

            $currentDate = clone $frequencStart;


            // Iterate through each week and perform the task
            $count=0;
            while ($currentDate <= $frequencyEnd) {

                if($count==0)
                {
                    $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
                    $MeetingDate = new \DateTime(Yii::$app->request->post()['meeting_date']);
                    $ExpiryDate = new \DateTime(Yii::$app->request->post()['expiry_date']);

                    $currentEndDate = clone $EndDate;
                    $currentMeetingDate = clone $MeetingDate;
                    $currentExpiryDate = clone $ExpiryDate;
                }


                $model = new TaskManager();
                $model->frequency = Yii::$app->request->post()['frequency'];
                // $model->department = $dept_info['title'];
                $model->department = Yii::$app->request->post()['department'];
                $model->module = Yii::$app->request->post()['module'];
                $model->type = Yii::$app->request->post()['type'];
                $model->modification = Yii::$app->request->post()['modification'];
                $model->priority = Yii::$app->request->post()['priority'];
                $model->description = Yii::$app->request->post()['description'];
                $model->start_date = $currentDate->format("Y/m/d H:i:s");
                $model->end_date = $currentEndDate->format("Y/m/d H:i:s");
                $model->meeting_date = $currentMeetingDate->format("Y/m/d H:i:s");
                $model->expiry_date = $currentExpiryDate->format("Y/m/d H:i:s");

                $model->responsibility = Yii::$app->request->post()['responsibility'];
                $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                $model->entered_by = Yii::$app->request->post()['entered_by'];
                $model->created_by = Yii::$app->request->post()['entered_by'];
                $model->verified_by = Yii::$app->request->post()['verified_by'];
                $model->agreed_by = Yii::$app->request->post()['agreed_by'];

                if(Yii::$app->request->post()['status'] == "completed")
                {

                    $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                    $formattedTargetDate = $dateTime->format('Y-m-d');

                    $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                    $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                    // print_r($formattedTargetDate);
                    // echo "<br>";
                    // print_r($formatteddateTimeToday);die;

                    if( $formatteddateTimeToday > $formattedTargetDate  )
                    {
                        $model->status = "overdue";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                    {
                        $model->status = "completed";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                    {
                        $model->status = "completed_ahead";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }
                }else{
                    $model->status = Yii::$app->request->post()['status'];
                }

                $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                $model->approved_by = Yii::$app->request->post()['approved_by'];
                $model->created_at = date("Y/m/d H:i:s");
                $model->save();


                $currentDate->modify('+6 months');
                $currentEndDate->modify('+6 months');
                $currentMeetingDate->modify('+6 months');
                $count++;
            }
            return $this->redirect(['index']);

        }elseif( isset(Yii::$app->request->post()['frequency']) && Yii::$app->request->post()['frequency'] == "quarterly" ) {

            $frequencStart = new \DateTime(Yii::$app->request->post()['frequency_start']); // Replace with your actual start date
            $frequencyEnd = new \DateTime(Yii::$app->request->post()['frequency_end']);

            $currentDate = clone $frequencStart;


            // Iterate through each week and perform the task
            $count=0;
            while ($currentDate <= $frequencyEnd) {

                if($count==0)
                {
                    $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
                    $MeetingDate = new \DateTime(Yii::$app->request->post()['meeting_date']);
                    $ExpiryDate = new \DateTime(Yii::$app->request->post()['expiry_date']);

                    $currentEndDate = clone $EndDate;
                    $currentMeetingDate = clone $MeetingDate;
                    $currentExpiryDate = clone $ExpiryDate;
                }


                $model = new TaskManager();
                $model->frequency = Yii::$app->request->post()['frequency'];
                // $model->department = $dept_info['title'];
                $model->department = Yii::$app->request->post()['department'];
                $model->module = Yii::$app->request->post()['module'];
                $model->type = Yii::$app->request->post()['type'];
                $model->modification = Yii::$app->request->post()['modification'];
                $model->priority = Yii::$app->request->post()['priority'];
                $model->description = Yii::$app->request->post()['description'];
                $model->start_date = $currentDate->format("Y/m/d H:i:s");
                $model->end_date = $currentEndDate->format("Y/m/d H:i:s");
                $model->meeting_date = $currentMeetingDate->format("Y/m/d H:i:s");
                $model->expiry_date = $currentExpiryDate->format("Y/m/d H:i:s");

                $model->responsibility = Yii::$app->request->post()['responsibility'];
                $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                $model->entered_by = Yii::$app->request->post()['entered_by'];
                $model->created_by = Yii::$app->request->post()['entered_by'];
                $model->verified_by = Yii::$app->request->post()['verified_by'];
                $model->agreed_by = Yii::$app->request->post()['agreed_by'];

                if(Yii::$app->request->post()['status'] == "completed")
                {

                    $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                    $formattedTargetDate = $dateTime->format('Y-m-d');

                    $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                    $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                    // print_r($formattedTargetDate);
                    // echo "<br>";
                    // print_r($formatteddateTimeToday);die;

                    if( $formatteddateTimeToday > $formattedTargetDate  )
                    {
                        $model->status = "overdue";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                    {
                        $model->status = "completed";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                    {
                        $model->status = "completed_ahead";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }
                }else{
                    $model->status = Yii::$app->request->post()['status'];
                }

                $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                $model->approved_by = Yii::$app->request->post()['approved_by'];
                $model->created_at = date("Y/m/d H:i:s");
                $model->save();


                $currentDate->modify('+3 months');
                $currentEndDate->modify('+3 months');
                $currentMeetingDate->modify('+3 months');
                $count++;
            }
            return $this->redirect(['index']);

        }else{
            if(!empty(Yii::$app->request->post()))
            {
                $model = new TaskManager();
                $model->frequency = Yii::$app->request->post()['frequency'];
                // $model->department = $dept_info['title'];
                $model->department = Yii::$app->request->post()['department'];
                $model->module = Yii::$app->request->post()['module'];
                $model->type = Yii::$app->request->post()['type'];
                $model->modification = Yii::$app->request->post()['modification'];
                $model->priority = Yii::$app->request->post()['priority'];
                $model->description = Yii::$app->request->post()['description'];
                $model->start_date = Yii::$app->request->post()['start_date'];
                $model->end_date = Yii::$app->request->post()['end_date'];
                $model->target_date = Yii::$app->request->post()['target_date'];
                $model->live_date = Yii::$app->request->post()['live_date'];
                $model->meeting_date = Yii::$app->request->post()['meeting_date'];
                $model->responsibility = Yii::$app->request->post()['responsibility'];
                $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                $model->entered_by = Yii::$app->request->post()['entered_by'];
                $model->created_by = Yii::$app->request->post()['entered_by'];
                $model->verified_by = Yii::$app->request->post()['verified_by'];
                $model->agreed_by = Yii::$app->request->post()['agreed_by'];
                $model->expiry_date = Yii::$app->request->post()['expiry_date'];

                if(Yii::$app->request->post()['status'] == "completed")
                {

                    $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                    $formattedTargetDate = $dateTime->format('Y-m-d');

                    $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                    $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                    // print_r($formattedTargetDate);
                    // echo "<br>";
                    // print_r($formatteddateTimeToday);die;

                    if( $formatteddateTimeToday > $formattedTargetDate  )
                    {
                        $model->status = "overdue";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                    {
                        $model->status = "completed";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                    {
                        $model->status = "completed_ahead";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }
                }else{
                    $model->status = Yii::$app->request->post()['status'];
                }

                $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                $model->approved_by = Yii::$app->request->post()['approved_by'];
                $model->created_at = date("Y/m/d H:i:s");
                $model->save();
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $StartDate = new \DateTime(Yii::$app->request->post()['start_date']);
        $formatStartDate = $StartDate->format('Y-m-d');

        $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
        $formatEndDate = $EndDate->format('Y-m-d');

        if($formatStartDate > $formatEndDate)
        {
            Yii::$app->getSession()->addFlash('error', "End Date Must be greater than Start Date!");
            $referrer = Yii::$app->request->referrer;
            return $this->redirect($referrer ? $referrer : ['index']);
        }

        $user_id = Yii::$app->user->identity->id;

        $user_info = (new \yii\db\Query())
            ->select('user_profile_info.department_id')
            ->from('user_profile_info')
            ->where(['user_profile_info.user_id' => $user_id])
            ->one();

        $dept_info = (new \yii\db\Query())
            ->select(['department.id','department.title'])
            ->from('department')
            ->where(['department.id' => $user_info['department_id']])
            ->one();

        $model = TaskManager::findOne(['id' => $id]);
        if(!empty(Yii::$app->request->post()))
        {
            $model->frequency = Yii::$app->request->post()['frequency'];
            // $model->department = $dept_info['title'];
            $model->department = Yii::$app->request->post()['department'];
            $model->module = Yii::$app->request->post()['module'];
            $model->type = Yii::$app->request->post()['type'];
            $model->modification = Yii::$app->request->post()['modification'];
            $model->priority = Yii::$app->request->post()['priority'];
            $model->description = Yii::$app->request->post()['description'];
            $model->start_date = Yii::$app->request->post()['start_date'];
            $model->end_date = Yii::$app->request->post()['end_date'];
            $model->target_date = Yii::$app->request->post()['target_date'];
            $model->live_date = Yii::$app->request->post()['live_date'];
            $model->meeting_date = Yii::$app->request->post()['meeting_date'];
            $model->expiry_date = Yii::$app->request->post()['expiry_date'];
            $model->responsibility = Yii::$app->request->post()['responsibility'];
            $model->assigned_to = Yii::$app->request->post()['assigned_to'];
            $model->entered_by = Yii::$app->request->post()['entered_by'];
            $model->created_by = Yii::$app->request->post()['entered_by'];
            $model->verified_by = Yii::$app->request->post()['verified_by'];
            $model->agreed_by = Yii::$app->request->post()['agreed_by'];

            if(Yii::$app->request->post()['status'] == "completed")
            {

                $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                $formattedTargetDate = $dateTime->format('Y-m-d');

                $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                // print_r($formattedTargetDate);
                // echo "<br>";
                // print_r($formatteddateTimeToday);die;

                if( $formatteddateTimeToday > $formattedTargetDate  )
                {
                    $model->status = "overdue";
                    $model->completed_date = date("Y/m/d H:i:s");
                }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                {
                    $model->status = "completed";
                    $model->completed_date = date("Y/m/d H:i:s");
                }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                {
                    $model->status = "completed_ahead";
                    $model->completed_date = date("Y/m/d H:i:s");
                }
            }else{
                $model->status = Yii::$app->request->post()['status'];
            }

            $model->frequency_start = Yii::$app->request->post()['frequency_start'];
            $model->frequency_end = Yii::$app->request->post()['frequency_end'];
            $model->approved_by = Yii::$app->request->post()['approved_by'];
            $model->updated_at = date("Y/m/d H:i:s");

            $model->save();
            return $this->redirect(['index']);
        }


        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = TaskManager::findOne(['id' => $id])->delete();

        return $this->redirect(['index']);
    }
}
