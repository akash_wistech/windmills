<?php

namespace app\controllers;

use Yii;

use yii\filters\VerbFilter;
use app\components\helpers\DefController;
use app\models\ExpenseManager;
use app\models\ExpenseManagerSearch;

/**
 * ExpenseController implements the CRUD actions for Expense model.
 */
class ExpenseManagerController extends DefController
{
    public function actionIndex()
    {
        $searchModel = new ExpenseManagerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexTravel()
    {
        $searchModel = new ExpenseManagerSearch();
        $dataProvider = $searchModel->searchTravel(Yii::$app->request->queryParams);

        return $this->render('index_travel', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        // Yii::$app->getSession()->addFlash('error', "End Date Must be greater than Start Date!");
        // $referrer = Yii::$app->request->referrer;
        // return $this->redirect($referrer ? $referrer : ['index']);

        $user_id = Yii::$app->user->identity->id;

        $user_info = (new \yii\db\Query())
            ->select('user_profile_info.department_id')
            ->from('user_profile_info')
            ->where(['user_profile_info.user_id' => $user_id])
            ->one();

        $dept_info = (new \yii\db\Query())
            ->select(['department.id','department.title'])
            ->from('department')
            ->where(['department.id' => $user_info['department_id']])
            ->one();


        // dd(Yii::$app->request->post());
        if(!empty(Yii::$app->request->post()))
        {
            // dd(Yii::$app->request->post());
            // Check if Quotation 1
            if (isset($_FILES["car_image"]) && $_FILES["car_image"]["error"] == UPLOAD_ERR_OK) {
                // Get file details
                $fileTmpPath = $_FILES["car_image"]["tmp_name"];
                $fileName = $_FILES["car_image"]["name"];
                $fileSize = $_FILES["car_image"]["size"];
                $fileType = $_FILES["car_image"]["type"];

                // Specify the directory where you want to save the uploaded file
                $uploadDir = 'uploads/quotations/';

                // Create the directory if it doesn't exist
                if (!file_exists($uploadDir)) {
                    mkdir($uploadDir, 0777, true);
                }

                // Move the uploaded file to the specified directory
                $destPath_1 = $uploadDir . $fileName;
                if (move_uploaded_file($fileTmpPath, $destPath_1)) {
                    Yii::$app->getSession()->addFlash('success', "Car Image Uploaded Successfully");
                } else {
                    Yii::$app->getSession()->addFlash('error', "Unable to upload Car Image");
                }
            } else {
                Yii::$app->getSession()->addFlash('error', "Car Image Not Selected");
            }

            // Check if Quotation 2
            if (isset($_FILES["start_image"]) && $_FILES["start_image"]["error"] == UPLOAD_ERR_OK) {
                // Get file details
                $fileTmpPath = $_FILES["start_image"]["tmp_name"];
                $fileName = $_FILES["start_image"]["name"];
                $fileSize = $_FILES["start_image"]["size"];
                $fileType = $_FILES["start_image"]["type"];

                // Specify the directory where you want to save the uploaded file
                $uploadDir = 'uploads/quotations/';

                // Create the directory if it doesn't exist
                if (!file_exists($uploadDir)) {
                    mkdir($uploadDir, 0777, true);
                }

                // Move the uploaded file to the specified directory
                $destPath_2 = $uploadDir . $fileName;
                if (move_uploaded_file($fileTmpPath, $destPath_2)) {
                    Yii::$app->getSession()->addFlash('success', "Start Image Uploaded Successfully");
                } else {
                    Yii::$app->getSession()->addFlash('error', "Unable to upload Start Image");
                }
            } else {
                Yii::$app->getSession()->addFlash('error', "Start Image Not Selected");
            }

            // Check if Quotation 1
            if (isset($_FILES["end_image"]) && $_FILES["end_image"]["error"] == UPLOAD_ERR_OK) {
                // Get file details
                $fileTmpPath = $_FILES["end_image"]["tmp_name"];
                $fileName = $_FILES["end_image"]["name"];
                $fileSize = $_FILES["end_image"]["size"];
                $fileType = $_FILES["end_image"]["type"];

                // Specify the directory where you want to save the uploaded file
                $uploadDir = 'uploads/quotations/';

                // Create the directory if it doesn't exist
                if (!file_exists($uploadDir)) {
                    mkdir($uploadDir, 0777, true);
                }

                // Move the uploaded file to the specified directory
                $destPath_3 = $uploadDir . $fileName;
                if (move_uploaded_file($fileTmpPath, $destPath_3)) {
                    Yii::$app->getSession()->addFlash('success', "End Image Uploaded Successfully");
                } else {
                    Yii::$app->getSession()->addFlash('error', "Unable to upload End Image");
                }
            } else {
                Yii::$app->getSession()->addFlash('error', "End Image Not Selected");
            }

            $model = new ExpenseManager();
            $model->department = $dept_info['title'];

            if($dept_info['title'] == "Valuations")
            {
                $model->reference_number = Yii::$app->request->post()['reference_number'];
                $model->start_reading = Yii::$app->request->post()['start_reading'];
                $model->end_reading = Yii::$app->request->post()['end_reading'];
                $model->start_date = Yii::$app->request->post()['start_date'];
                $model->end_date = Yii::$app->request->post()['end_date'];
                $model->total_km = Yii::$app->request->post()['total_km'];
                $model->val_amount = Yii::$app->request->post()['val_amount'];
                $model->transport_mode = Yii::$app->request->post()['transport_mode'];

                if(Yii::$app->request->post()['transport_mode'] == "company")
                {
                    $model->car_type = Yii::$app->request->post()['car_type'];
                }

                $model->image_1 = $destPath_1;
                $model->image_2 = $destPath_2;
                $model->image_3 = $destPath_3;
            }else{
                $model->val_amount = Yii::$app->request->post()['amount'];
            }

            $model->asset_location = Yii::$app->request->post()['asset_location'];
            $model->purpose = Yii::$app->request->post()['purpose'];
            $model->pay_bank = Yii::$app->request->post()['pay_bank'];
            $model->applicant = Yii::$app->request->post()['applicant'];
            $model->status = Yii::$app->request->post()['status'];
            $model->entered_date = date("Y/m/d H:i:s");
            $model->save();
            return $this->redirect(['index-travel']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $user_id = Yii::$app->user->identity->id;
        $user_info = (new \yii\db\Query())
            ->select('user_profile_info.department_id')
            ->from('user_profile_info')
            ->where(['user_profile_info.user_id' => $user_id])
            ->one();

        $dept_info = (new \yii\db\Query())
            ->select(['department.id','department.title'])
            ->from('department')
            ->where(['department.id' => $user_info['department_id']])
            ->one();

        $model = ExpenseManager::findOne(['id' => $id]);
        if(!empty(Yii::$app->request->post()))
        {

            // dd(Yii::$app->request->post());
            // Check if Quotation 1
            if (isset($_FILES["car_image"]) && $_FILES["car_image"]["error"] == UPLOAD_ERR_OK) {
                // Get file details
                $fileTmpPath = $_FILES["car_image"]["tmp_name"];
                $fileName = $_FILES["car_image"]["name"];
                $fileSize = $_FILES["car_image"]["size"];
                $fileType = $_FILES["car_image"]["type"];

                // Specify the directory where you want to save the uploaded file
                $uploadDir = 'uploads/quotations/';

                // Create the directory if it doesn't exist
                if (!file_exists($uploadDir)) {
                    mkdir($uploadDir, 0777, true);
                }

                // Move the uploaded file to the specified directory
                $destPath_1 = $uploadDir . $fileName;
                if (move_uploaded_file($fileTmpPath, $destPath_1)) {
                    Yii::$app->getSession()->addFlash('success', "Car Image Uploaded Successfully");
                } else {
                    Yii::$app->getSession()->addFlash('error', "Unable to upload Car Image");
                }
            } else {
                Yii::$app->getSession()->addFlash('error', "Car Image Not Selected");
            }

            // Check if Quotation 2
            if (isset($_FILES["start_image"]) && $_FILES["start_image"]["error"] == UPLOAD_ERR_OK) {
                // Get file details
                $fileTmpPath = $_FILES["start_image"]["tmp_name"];
                $fileName = $_FILES["start_image"]["name"];
                $fileSize = $_FILES["start_image"]["size"];
                $fileType = $_FILES["start_image"]["type"];

                // Specify the directory where you want to save the uploaded file
                $uploadDir = 'uploads/quotations/';

                // Create the directory if it doesn't exist
                if (!file_exists($uploadDir)) {
                    mkdir($uploadDir, 0777, true);
                }

                // Move the uploaded file to the specified directory
                $destPath_2 = $uploadDir . $fileName;
                if (move_uploaded_file($fileTmpPath, $destPath_2)) {
                    Yii::$app->getSession()->addFlash('success', "Start Image Uploaded Successfully");
                } else {
                    Yii::$app->getSession()->addFlash('error', "Unable to upload Start Image");
                }
            } else {
                Yii::$app->getSession()->addFlash('error', "Start Image Not Selected");
            }

            // Check if Quotation 1
            if (isset($_FILES["end_image"]) && $_FILES["end_image"]["error"] == UPLOAD_ERR_OK) {
                // Get file details
                $fileTmpPath = $_FILES["end_image"]["tmp_name"];
                $fileName = $_FILES["end_image"]["name"];
                $fileSize = $_FILES["end_image"]["size"];
                $fileType = $_FILES["end_image"]["type"];

                // Specify the directory where you want to save the uploaded file
                $uploadDir = 'uploads/quotations/';

                // Create the directory if it doesn't exist
                if (!file_exists($uploadDir)) {
                    mkdir($uploadDir, 0777, true);
                }

                // Move the uploaded file to the specified directory
                $destPath_3 = $uploadDir . $fileName;
                if (move_uploaded_file($fileTmpPath, $destPath_3)) {
                    Yii::$app->getSession()->addFlash('success', "End Image Uploaded Successfully");
                } else {
                    Yii::$app->getSession()->addFlash('error', "Unable to upload End Image");
                }
            } else {
                Yii::$app->getSession()->addFlash('error', "End Image Not Selected");
            }

            $model->department = $dept_info['title'];

            if($dept_info['title'] == "Valuations")
            {
                $model->reference_number = Yii::$app->request->post()['reference_number'];
                $model->start_reading = Yii::$app->request->post()['start_reading'];
                $model->end_reading = Yii::$app->request->post()['end_reading'];
                $model->start_date = Yii::$app->request->post()['start_date'];
                $model->end_date = Yii::$app->request->post()['end_date'];
                $model->total_km = Yii::$app->request->post()['total_km'];
                $model->val_amount = Yii::$app->request->post()['val_amount'];
                $model->transport_mode = Yii::$app->request->post()['transport_mode'];

                if(Yii::$app->request->post()['transport_mode'] == "company")
                {
                    $model->car_type = Yii::$app->request->post()['car_type'];
                }

                if( isset($destPath_1) )
                {
                    $model->image_1 = $destPath_1;
                }
                if( isset($destPath_2) )
                {
                    $model->image_2 = $destPath_2;
                }
                if( isset($destPath_3) )
                {
                    $model->image_3 = $destPath_3;
                }
            }else{
                $model->val_amount = Yii::$app->request->post()['amount'];
            }

            $model->asset_location = Yii::$app->request->post()['asset_location'];
            $model->purpose = Yii::$app->request->post()['purpose'];
            $model->pay_bank = Yii::$app->request->post()['pay_bank'];
            $model->applicant = Yii::$app->request->post()['applicant'];
            $model->status = Yii::$app->request->post()['status'];

            $model->save();
            return $this->redirect(['index-travel']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = ExpenseManager::findOne(['id' => $id]);

        if($model->accounting_entry == "old")
        {
            $model = ExpenseManager::findOne(['id' => $id])->delete();
            return $this->redirect(['index']);
        }else{

            // zoho integration
            Yii::$app->appHelperFunctions->ZohoRefereshToken();
            $AccessToken = Yii::$app->appHelperFunctions->getSetting('zohobooks_access_token');
            $OrganizationID = Yii::$app->appHelperFunctions->getSetting('zohobooks_organization_id');

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://www.zohoapis.com/books/v3/expenses/'.$model->zoho_expense_id.'?organization_id='.$OrganizationID,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'DELETE',
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Zoho-oauthtoken'.' '.$AccessToken,
                ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            $response = json_decode($response);

            if($response->code == 0)
            {
                $model = ExpenseManager::findOne(['id' => $id])->delete();
                Yii::$app->getSession()->addFlash('success', Yii::t('app', $response->message));
            }else{
                Yii::$app->getSession()->addFlash('error', Yii::t('app', $response->message));
            }

            return $this->redirect(['index']);
        }
    }

    public function actionCreateAsset()
    {
        // dd(Yii::$app->request->post());
        $StartDate = new \DateTime(Yii::$app->request->post()['start_date']);
        $formatStartDate = $StartDate->format('Y-m-d');

        $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
        $formatEndDate = $EndDate->format('Y-m-d');

        if($formatStartDate > $formatEndDate)
        {
            Yii::$app->getSession()->addFlash('error', "End Date Must be greater than Start Date!");
            $referrer = Yii::$app->request->referrer;
            return $this->redirect($referrer ? $referrer : ['index']);
        }

        $user_id = Yii::$app->user->identity->id;

        $user_info = (new \yii\db\Query())
            ->select('user_profile_info.department_id')
            ->from('user_profile_info')
            ->where(['user_profile_info.user_id' => $user_id])
            ->one();

        $dept_info = (new \yii\db\Query())
            ->select(['department.id','department.title'])
            ->from('department')
            ->where(['department.id' => $user_info['department_id']])
            ->one();

        // dd(Yii::$app->request->post()['frequency_start']);

        if( isset(Yii::$app->request->post()['frequency']) && Yii::$app->request->post()['frequency'] == "daily" ){
            $StartDate = new \DateTime(Yii::$app->request->post()['start_date']);
            $formatStartDate = $StartDate->format('Y-m-d');

            $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
            $formatEndDate = $EndDate->format('Y-m-d');

            if($formatStartDate != $formatEndDate)
            {
                Yii::$app->getSession()->addFlash('error', "Start Date & End Date Must be Same for Daily Frequency!");
                $referrer = Yii::$app->request->referrer;
                return $this->redirect($referrer ? $referrer : ['index']);
            }

            // dd(Yii::$app->request->post());

            $frequencStart = new \DateTime(Yii::$app->request->post()['frequency_start']); // Replace with your actual start date
            $endDate = clone $frequencStart;
            $frequencyEnd = new \DateTime(Yii::$app->request->post()['frequency_end']);

            $interval = $frequencStart->diff($frequencyEnd);
            $numberOfDays = $interval->days;

            // dd($numberOfDays);

            $endDate->modify('+'.$numberOfDays.' days'); // Add number of days according to frequency

            $currentDate = clone $frequencStart;

            while ($currentDate <= $endDate) {
                if ($currentDate->format('N') <= 5) {
                    //Adding Expenses
                    $model = new ExpenseManager();
                    $model->frequency = Yii::$app->request->post()['frequency'];
                    // $model->department = $dept_info['title'];
                    $model->department = Yii::$app->request->post()['department'];
                    $model->module = Yii::$app->request->post()['module'];
                    $model->type = Yii::$app->request->post()['type'];
                    $model->modification = Yii::$app->request->post()['modification'];
                    $model->priority = Yii::$app->request->post()['priority'];
                    $model->description = Yii::$app->request->post()['description'];
                    $model->start_date = $currentDate->format("Y/m/d H:i:s");
                    $model->end_date = $currentDate->format("Y/m/d H:i:s");

                    $dateTime = new \DateTime(Yii::$app->request->post()['meeting_date']);
                    $formattedEndDate = $dateTime->format('Y-m-d');

                    $dateCurrent = new \DateTime($currentDate->format("Y/m/d H:i:s"));
                    $formattedCurrentDate = $dateCurrent->format('Y-m-d');

                    // print_r($formattedEndDate);
                    // echo "<br>";
                    // print_r($formattedCurrentDate);die;

                    if( $formattedCurrentDate == $formattedEndDate  )
                    {
                        $model->meeting_date = Yii::$app->request->post()['meeting_date'];
                        $model->expiry_date = Yii::$app->request->post()['expiry_date'];
                    }else{
                        $model->meeting_date = $currentDate->format("Y/m/d H:i:s");
                        $model->expiry_date = $currentDate->format("Y/m/d H:i:s");
                        // $interval = $dateCurrent->diff($dateTime);
                        // $numberOfDays = $interval->days;

                        // // Add day to the date
                        // $dateTime->add(new \DateInterval('P'.$numberOfDays.'D'));
                        // $model->meeting_date = $dateTime->format("Y/m/d H:i:s");
                    }

                    $model->responsibility = Yii::$app->request->post()['responsibility'];
                    $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                    $model->entered_by = Yii::$app->request->post()['entered_by'];
                    $model->created_by = Yii::$app->request->post()['entered_by'];
                    $model->verified_by = Yii::$app->request->post()['verified_by'];
                    $model->agreed_by = Yii::$app->request->post()['agreed_by'];

                    if(Yii::$app->request->post()['status'] == "completed")
                    {

                        $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                        $formattedTargetDate = $dateTime->format('Y-m-d');

                        $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                        $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                        // print_r($formattedTargetDate);
                        // echo "<br>";
                        // print_r($formatteddateTimeToday);die;

                        if( $formatteddateTimeToday > $formattedTargetDate  )
                        {
                            $model->status = "overdue";
                            $model->completed_date = date("Y/m/d H:i:s");
                        }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                        {
                            $model->status = "completed";
                            $model->completed_date = date("Y/m/d H:i:s");
                        }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                        {
                            $model->status = "completed_ahead";
                            $model->completed_date = date("Y/m/d H:i:s");
                        }
                    }else{
                        $model->status = Yii::$app->request->post()['status'];
                    }

                    $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                    $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                    $model->approved_by = Yii::$app->request->post()['approved_by'];
                    $model->created_at = date("Y/m/d H:i:s");
                    $model->save();
                    //end ending expense
                }
                $currentDate->modify('+1 day');
            }
            return $this->redirect(['index']);
        }elseif( isset(Yii::$app->request->post()['frequency']) && Yii::$app->request->post()['frequency'] == "monthly" ) {
            $frequencStart = new \DateTime(Yii::$app->request->post()['frequency_start']); // Replace with your actual start date
            $frequencyEnd = new \DateTime(Yii::$app->request->post()['frequency_end']);

            // Calculate the difference in months
            $interval = $frequencStart->diff($frequencyEnd);
            $months = $interval->y * 12 + $interval->m;

            for ($i = 0; $i <= $months; $i++) {
                $startDate = new \DateTime(Yii::$app->request->post()['start_date']);
                $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
                $MeetingDate = new \DateTime(Yii::$app->request->post()['meeting_date']);
                $ExpiryDate = new \DateTime(Yii::$app->request->post()['expiry_date']);

                $currentStartDate = clone $startDate;
                $currentEndDate = clone $EndDate;
                $MeetingDate = clone $MeetingDate;
                $ExpiryDate = clone $ExpiryDate;

                $currentStartDate->modify("+$i months");
                $currentEndDate->modify("+$i months");
                $MeetingDate->modify("+$i months");
                $ExpiryDate->modify("+$i months");

                // echo "expense " .$currentStartDate->format('Y-m-d') .' '. $currentEndDate->format('Y-m-d') . PHP_EOL;
                // echo "<br>";

                $model = new ExpenseManager();
                $model->frequency = Yii::$app->request->post()['frequency'];
                // $model->department = $dept_info['title'];
                $model->department = Yii::$app->request->post()['department'];
                $model->module = Yii::$app->request->post()['module'];
                $model->type = Yii::$app->request->post()['type'];
                $model->modification = Yii::$app->request->post()['modification'];
                $model->priority = Yii::$app->request->post()['priority'];
                $model->description = Yii::$app->request->post()['description'];
                $model->start_date = $currentStartDate->format("Y/m/d H:i:s");
                $model->end_date = $currentEndDate->format("Y/m/d H:i:s");
                $model->meeting_date = $MeetingDate->format("Y/m/d H:i:s");
                $model->expiry_date = $ExpiryDate->format("Y/m/d H:i:s");

                $model->responsibility = Yii::$app->request->post()['responsibility'];
                $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                $model->entered_by = Yii::$app->request->post()['entered_by'];
                $model->created_by = Yii::$app->request->post()['entered_by'];
                $model->verified_by = Yii::$app->request->post()['verified_by'];
                $model->agreed_by = Yii::$app->request->post()['agreed_by'];


                if(Yii::$app->request->post()['status'] == "completed")
                {

                    $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                    $formattedTargetDate = $dateTime->format('Y-m-d');

                    $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                    $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                    // print_r($formattedTargetDate);
                    // echo "<br>";
                    // print_r($formatteddateTimeToday);die;

                    if( $formatteddateTimeToday > $formattedTargetDate  )
                    {
                        $model->status = "overdue";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                    {
                        $model->status = "completed";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                    {
                        $model->status = "completed_ahead";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }
                }else{
                    $model->status = Yii::$app->request->post()['status'];
                }

                $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                $model->approved_by = Yii::$app->request->post()['approved_by'];
                $model->created_at = date("Y/m/d H:i:s");
                $model->save();
            }
            return $this->redirect(['index']);
        }elseif( isset(Yii::$app->request->post()['frequency']) && Yii::$app->request->post()['frequency'] == "yearly" ) {
            $frequencStart = new \DateTime(Yii::$app->request->post()['frequency_start']); // Replace with your actual start date
            $frequencyEnd = new \DateTime(Yii::$app->request->post()['frequency_end']);

            // Calculate the difference in years
            $interval = $frequencStart->diff($frequencyEnd);
            $numberOfYears = $interval->y;

            for ($i = 0; $i <= $numberOfYears; $i++) {
                $startDate = new \DateTime(Yii::$app->request->post()['start_date']);
                $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
                $MeetingDate = new \DateTime(Yii::$app->request->post()['meeting_date']);
                $ExpiryDate = new \DateTime(Yii::$app->request->post()['expiry_date']);
                $currentStartDate = clone $startDate;
                $currentEndDate = clone $EndDate;
                $MeetingDate = clone $MeetingDate;
                $ExpiryDate = clone $ExpiryDate;

                $currentStartDate->modify("+$i year");
                $currentEndDate->modify("+$i year");
                $MeetingDate->modify("+$i year");
                $ExpiryDate->modify("+$i year");

                // echo "expense " .$currentStartDate->format('Y-m-d') .' '. $currentEndDate->format('Y-m-d') . PHP_EOL;
                // echo "<br>";

                $model = new ExpenseManager();
                $model->frequency = Yii::$app->request->post()['frequency'];
                // $model->department = $dept_info['title'];
                $model->department = Yii::$app->request->post()['department'];
                $model->module = Yii::$app->request->post()['module'];
                $model->type = Yii::$app->request->post()['type'];
                $model->modification = Yii::$app->request->post()['modification'];
                $model->priority = Yii::$app->request->post()['priority'];
                $model->description = Yii::$app->request->post()['description'];
                $model->start_date = $currentStartDate->format("Y/m/d H:i:s");
                $model->end_date = $currentEndDate->format("Y/m/d H:i:s");
                $model->meeting_date = $MeetingDate->format("Y/m/d H:i:s");
                $model->expiry_date = $ExpiryDate->format("Y/m/d H:i:s");

                $model->responsibility = Yii::$app->request->post()['responsibility'];
                $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                $model->entered_by = Yii::$app->request->post()['entered_by'];
                $model->created_by = Yii::$app->request->post()['entered_by'];
                $model->verified_by = Yii::$app->request->post()['verified_by'];
                $model->agreed_by = Yii::$app->request->post()['agreed_by'];

                if(Yii::$app->request->post()['status'] == "completed")
                {

                    $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                    $formattedTargetDate = $dateTime->format('Y-m-d');

                    $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                    $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                    // print_r($formattedTargetDate);
                    // echo "<br>";
                    // print_r($formatteddateTimeToday);die;

                    if( $formatteddateTimeToday > $formattedTargetDate  )
                    {
                        $model->status = "overdue";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                    {
                        $model->status = "completed";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                    {
                        $model->status = "completed_ahead";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }
                }else{
                    $model->status = Yii::$app->request->post()['status'];
                }

                $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                $model->approved_by = Yii::$app->request->post()['approved_by'];
                $model->created_at = date("Y/m/d H:i:s");
                $model->save();
            }
            return $this->redirect(['index']);
        }elseif( isset(Yii::$app->request->post()['frequency']) && Yii::$app->request->post()['frequency'] == "weekly" ) {

            $frequencStart = new \DateTime(Yii::$app->request->post()['frequency_start']); // Replace with your actual start date
            $frequencyEnd = new \DateTime(Yii::$app->request->post()['frequency_end']);

            $currentDate = clone $frequencStart;


            // Iterate through each week and perform the expense
            $count=0;
            while ($currentDate <= $frequencyEnd) {

                if($count==0)
                {
                    $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
                    $MeetingDate = new \DateTime(Yii::$app->request->post()['meeting_date']);
                    $ExpiryDate = new \DateTime(Yii::$app->request->post()['expiry_date']);

                    $currentEndDate = clone $EndDate;
                    $currentMeetingDate = clone $MeetingDate;
                    $currentExpiryDate = clone $MeetingDate;
                }


                $model = new ExpenseManager();
                $model->frequency = Yii::$app->request->post()['frequency'];
                // $model->department = $dept_info['title'];
                $model->department = Yii::$app->request->post()['department'];
                $model->module = Yii::$app->request->post()['module'];
                $model->type = Yii::$app->request->post()['type'];
                $model->modification = Yii::$app->request->post()['modification'];
                $model->priority = Yii::$app->request->post()['priority'];
                $model->description = Yii::$app->request->post()['description'];
                $model->start_date = $currentDate->format("Y/m/d H:i:s");
                $model->end_date = $currentEndDate->format("Y/m/d H:i:s");
                $model->meeting_date = $currentMeetingDate->format("Y/m/d H:i:s");
                $model->expiry_date = $currentExpiryDate->format("Y/m/d H:i:s");

                $model->responsibility = Yii::$app->request->post()['responsibility'];
                $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                $model->entered_by = Yii::$app->request->post()['entered_by'];
                $model->created_by = Yii::$app->request->post()['entered_by'];
                $model->verified_by = Yii::$app->request->post()['verified_by'];
                $model->agreed_by = Yii::$app->request->post()['agreed_by'];

                if(Yii::$app->request->post()['status'] == "completed")
                {

                    $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                    $formattedTargetDate = $dateTime->format('Y-m-d');

                    $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                    $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                    // print_r($formattedTargetDate);
                    // echo "<br>";
                    // print_r($formatteddateTimeToday);die;

                    if( $formatteddateTimeToday > $formattedTargetDate  )
                    {
                        $model->status = "overdue";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                    {
                        $model->status = "completed";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                    {
                        $model->status = "completed_ahead";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }
                }else{
                    $model->status = Yii::$app->request->post()['status'];
                }

                $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                $model->approved_by = Yii::$app->request->post()['approved_by'];
                $model->created_at = date("Y/m/d H:i:s");
                $model->save();


                $currentDate->modify('+7 days');
                $currentEndDate->modify('+7 days');
                $currentMeetingDate->modify('+7 days');
                $count++;
            }
            return $this->redirect(['index']);

        }elseif( isset(Yii::$app->request->post()['frequency']) && Yii::$app->request->post()['frequency'] == "bi-annual" ) {

            $frequencStart = new \DateTime(Yii::$app->request->post()['frequency_start']); // Replace with your actual start date
            $frequencyEnd = new \DateTime(Yii::$app->request->post()['frequency_end']);

            $currentDate = clone $frequencStart;


            // Iterate through each week and perform the expense
            $count=0;
            while ($currentDate <= $frequencyEnd) {

                if($count==0)
                {
                    $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
                    $MeetingDate = new \DateTime(Yii::$app->request->post()['meeting_date']);
                    $ExpiryDate = new \DateTime(Yii::$app->request->post()['expiry_date']);

                    $currentEndDate = clone $EndDate;
                    $currentMeetingDate = clone $MeetingDate;
                    $currentExpiryDate = clone $ExpiryDate;
                }


                $model = new ExpenseManager();
                $model->frequency = Yii::$app->request->post()['frequency'];
                // $model->department = $dept_info['title'];
                $model->department = Yii::$app->request->post()['department'];
                $model->module = Yii::$app->request->post()['module'];
                $model->type = Yii::$app->request->post()['type'];
                $model->modification = Yii::$app->request->post()['modification'];
                $model->priority = Yii::$app->request->post()['priority'];
                $model->description = Yii::$app->request->post()['description'];
                $model->start_date = $currentDate->format("Y/m/d H:i:s");
                $model->end_date = $currentEndDate->format("Y/m/d H:i:s");
                $model->meeting_date = $currentMeetingDate->format("Y/m/d H:i:s");
                $model->expiry_date = $currentExpiryDate->format("Y/m/d H:i:s");

                $model->responsibility = Yii::$app->request->post()['responsibility'];
                $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                $model->entered_by = Yii::$app->request->post()['entered_by'];
                $model->created_by = Yii::$app->request->post()['entered_by'];
                $model->verified_by = Yii::$app->request->post()['verified_by'];
                $model->agreed_by = Yii::$app->request->post()['agreed_by'];

                if(Yii::$app->request->post()['status'] == "completed")
                {

                    $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                    $formattedTargetDate = $dateTime->format('Y-m-d');

                    $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                    $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                    // print_r($formattedTargetDate);
                    // echo "<br>";
                    // print_r($formatteddateTimeToday);die;

                    if( $formatteddateTimeToday > $formattedTargetDate  )
                    {
                        $model->status = "overdue";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                    {
                        $model->status = "completed";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                    {
                        $model->status = "completed_ahead";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }
                }else{
                    $model->status = Yii::$app->request->post()['status'];
                }

                $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                $model->approved_by = Yii::$app->request->post()['approved_by'];
                $model->created_at = date("Y/m/d H:i:s");
                $model->save();


                $currentDate->modify('+6 months');
                $currentEndDate->modify('+6 months');
                $currentMeetingDate->modify('+6 months');
                $count++;
            }
            return $this->redirect(['index']);

        }else{
            // dd(Yii::$app->request->post());
            if(!empty(Yii::$app->request->post()))
            {
                // dd(Yii::$app->request->post());
                // Check if Quotation 1
                if (isset($_FILES["image_1"]) && $_FILES["image_1"]["error"] == UPLOAD_ERR_OK) {
                    // Get file details
                    $fileTmpPath = $_FILES["image_1"]["tmp_name"];
                    $fileName = $_FILES["image_1"]["name"];
                    $fileSize = $_FILES["image_1"]["size"];
                    $fileType = $_FILES["image_1"]["type"];

                    // Specify the directory where you want to save the uploaded file
                    $uploadDir = 'uploads/quotations/';

                    // Create the directory if it doesn't exist
                    if (!file_exists($uploadDir)) {
                        mkdir($uploadDir, 0777, true);
                    }

                    // Move the uploaded file to the specified directory
                    $destPath_1 = $uploadDir . $fileName;
                    if (move_uploaded_file($fileTmpPath, $destPath_1)) {
                        Yii::$app->getSession()->addFlash('success', "Quotation # 1 Uploaded Successfully");
                    } else {
                        Yii::$app->getSession()->addFlash('error', "Unable to upload Quotation # 1");
                    }
                } else {
                    Yii::$app->getSession()->addFlash('error', "Quotation # 1 Not Selected");
                }

                // Check if Quotation 2
                if (isset($_FILES["image_2"]) && $_FILES["image_2"]["error"] == UPLOAD_ERR_OK) {
                    // Get file details
                    $fileTmpPath = $_FILES["image_2"]["tmp_name"];
                    $fileName = $_FILES["image_2"]["name"];
                    $fileSize = $_FILES["image_2"]["size"];
                    $fileType = $_FILES["image_2"]["type"];

                    // Specify the directory where you want to save the uploaded file
                    $uploadDir = 'uploads/quotations/';

                    // Create the directory if it doesn't exist
                    if (!file_exists($uploadDir)) {
                        mkdir($uploadDir, 0777, true);
                    }

                    // Move the uploaded file to the specified directory
                    $destPath_2 = $uploadDir . $fileName;
                    if (move_uploaded_file($fileTmpPath, $destPath_2)) {
                        Yii::$app->getSession()->addFlash('success', "Quotation # 2 Uploaded Successfully");
                    } else {
                        Yii::$app->getSession()->addFlash('error', "Unable to upload Quotation # 2");
                    }
                } else {
                    Yii::$app->getSession()->addFlash('error', "Quotation # 2 Not Selected");
                }

                // Check if Quotation 1
                if (isset($_FILES["image_3"]) && $_FILES["image_3"]["error"] == UPLOAD_ERR_OK) {
                    // Get file details
                    $fileTmpPath = $_FILES["image_3"]["tmp_name"];
                    $fileName = $_FILES["image_3"]["name"];
                    $fileSize = $_FILES["image_3"]["size"];
                    $fileType = $_FILES["image_3"]["type"];

                    // Specify the directory where you want to save the uploaded file
                    $uploadDir = 'uploads/quotations/';

                    // Create the directory if it doesn't exist
                    if (!file_exists($uploadDir)) {
                        mkdir($uploadDir, 0777, true);
                    }

                    // Move the uploaded file to the specified directory
                    $destPath_3 = $uploadDir . $fileName;
                    if (move_uploaded_file($fileTmpPath, $destPath_3)) {
                        Yii::$app->getSession()->addFlash('success', "Quotation # 3 Uploaded Successfully");
                    } else {
                        Yii::$app->getSession()->addFlash('error', "Unable to upload Quotation # 3");
                    }
                } else {
                    Yii::$app->getSession()->addFlash('error', "Quotation # 3 Not Selected");
                }

                $model = new ExpenseManager();
                $model->transaction_type = Yii::$app->request->post()['transaction_type'];
                $model->action = Yii::$app->request->post()['action'];
                $model->purpose = Yii::$app->request->post()['purpose'];

                if(isset(Yii::$app->request->post()['asset_id']))
                {
                    $model->asset_id = Yii::$app->request->post()['asset_id'];
                }elseif(isset(Yii::$app->request->post()['expense_id']))
                {
                    $model->asset_id = Yii::$app->request->post()['expense_id'];
                }

                $model->category_id = Yii::$app->request->post()['category_id'];
                $model->category_name = Yii::$app->request->post()['category_name'];
                $model->depriciation = Yii::$app->request->post()['depriciation'];
                $model->priority = Yii::$app->request->post()['priority'];
                $model->frequency = Yii::$app->request->post()['frequency'];
                $model->asset_location = Yii::$app->request->post()['asset_location'];
                $model->accounting_entry = Yii::$app->request->post()['accounting_entry'];
                $model->brand = Yii::$app->request->post()['brand'];
                $model->quantity = Yii::$app->request->post()['quantity'];
                $model->status = Yii::$app->request->post()['status'];
                $model->payment_mode = Yii::$app->request->post()['payment_mode'];
                $model->amount = Yii::$app->request->post()['amount'];
                $model->asset_life = Yii::$app->request->post()['asset_life'];

                if( Yii::$app->request->post()['accounting_entry'] == "new" && isset(Yii::$app->request->post()['vendor']) && Yii::$app->request->post()['payment_mode'] == "credit" )
                {
                    $model->pay_bank = Yii::$app->request->post()['pay_bank'];
                    $parts = explode(",", Yii::$app->request->post()['vendor']);

                    $model->vendor_id = $parts[0];
                    $model->vendor_name = $parts[1];
                }

                if( isset(Yii::$app->request->post()['accounting_entry']) && Yii::$app->request->post()['accounting_entry'] == "new" && Yii::$app->request->post()['payment_mode'] == "cash" )
                {
                    $model->pay_bank = Yii::$app->request->post()['pay_bank'];
                }

                $model->first_supplier = Yii::$app->request->post()['first_supplier'];
                $model->second_supplier = Yii::$app->request->post()['second_supplier'];
                $model->third_supplier = Yii::$app->request->post()['third_supplier'];
                $model->image_1 = $destPath_1;
                $model->image_2 = $destPath_2;
                $model->image_3 = $destPath_3;
                $model->applicant = Yii::$app->request->post()['applicant'];
                $model->department = Yii::$app->request->post()['department'];
                $model->approved_by = Yii::$app->request->post()['approved_by'];
                $model->approvers_dept = Yii::$app->request->post()['approvers_dept'];
                $model->asset_owner = Yii::$app->request->post()['asset_owner'];
                $model->entered_date = date("Y/m/d H:i:s");
                $model->approved_date = Yii::$app->request->post()['approved_date'];
                $model->purchasing_date = Yii::$app->request->post()['purchasing_date'];
                $model->target_date = Yii::$app->request->post()['target_date'];
                $model->collection_date = Yii::$app->request->post()['collection_date'];
                $model->meeting_date = Yii::$app->request->post()['meeting_date'];
                $model->start_date = Yii::$app->request->post()['start_date'];
                $model->expiry_date = Yii::$app->request->post()['expiry_date'];
                $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                $model->created_by = Yii::$app->user->identity->id;
                $model->created_at = date("Y/m/d H:i:s");

                $model->save();
                return $this->redirect(['index']);
            }
        }

        return $this->render('create_asset', [
            'model' => $model,
        ]);
    }

    public function actionUpdateAsset($id)
    {
        $StartDate = new \DateTime(Yii::$app->request->post()['start_date']);
        $formatStartDate = $StartDate->format('Y-m-d');

        $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
        $formatEndDate = $EndDate->format('Y-m-d');

        if($formatStartDate > $formatEndDate)
        {
            Yii::$app->getSession()->addFlash('error', "End Date Must be greater than Start Date!");
            $referrer = Yii::$app->request->referrer;
            return $this->redirect($referrer ? $referrer : ['index']);
        }

        $user_id = Yii::$app->user->identity->id;

        $user_info = (new \yii\db\Query())
            ->select('user_profile_info.department_id')
            ->from('user_profile_info')
            ->where(['user_profile_info.user_id' => $user_id])
            ->one();

        $dept_info = (new \yii\db\Query())
            ->select(['department.id','department.title'])
            ->from('department')
            ->where(['department.id' => $user_info['department_id']])
            ->one();

        $model = ExpenseManager::findOne(['id' => $id]);
        if(!empty(Yii::$app->request->post()))
        {

            if( Yii::$app->request->post()['action'] == "throw" )
            {
                Yii::$app->controller->actionDelete($model->id);
                return $this->redirect(['index']);
            }

            // dd(Yii::$app->request->post());
            // Check if Quotation 1
            if (isset($_FILES["image_1"]) && $_FILES["image_1"]["error"] == UPLOAD_ERR_OK) {
                // Get file details
                $fileTmpPath = $_FILES["image_1"]["tmp_name"];
                $fileName = $_FILES["image_1"]["name"];
                $fileSize = $_FILES["image_1"]["size"];
                $fileType = $_FILES["image_1"]["type"];

                // Specify the directory where you want to save the uploaded file
                $uploadDir = 'uploads/quotations/';

                // Create the directory if it doesn't exist
                if (!file_exists($uploadDir)) {
                    mkdir($uploadDir, 0777, true);
                }

                // Move the uploaded file to the specified directory
                $destPath_1 = $uploadDir . $fileName;
                if (move_uploaded_file($fileTmpPath, $destPath_1)) {
                    Yii::$app->getSession()->addFlash('success', "Quotation # 1 Uploaded Successfully");
                } else {
                    Yii::$app->getSession()->addFlash('error', "Unable to upload Quotation # 1");
                }
            } else {
                Yii::$app->getSession()->addFlash('error', "Quotation # 1 Not Selected");
            }

            // Check if Quotation 2
            if (isset($_FILES["image_2"]) && $_FILES["image_2"]["error"] == UPLOAD_ERR_OK) {
                // Get file details
                $fileTmpPath = $_FILES["image_2"]["tmp_name"];
                $fileName = $_FILES["image_2"]["name"];
                $fileSize = $_FILES["image_2"]["size"];
                $fileType = $_FILES["image_2"]["type"];

                // Specify the directory where you want to save the uploaded file
                $uploadDir = 'uploads/quotations/';

                // Create the directory if it doesn't exist
                if (!file_exists($uploadDir)) {
                    mkdir($uploadDir, 0777, true);
                }

                // Move the uploaded file to the specified directory
                $destPath_2 = $uploadDir . $fileName;
                if (move_uploaded_file($fileTmpPath, $destPath_2)) {
                    Yii::$app->getSession()->addFlash('success', "Quotation # 2 Uploaded Successfully");
                } else {
                    Yii::$app->getSession()->addFlash('error', "Unable to upload Quotation # 2");
                }
            } else {
                Yii::$app->getSession()->addFlash('error', "Quotation # 2 Not Selected");
            }

            // Check if Quotation 1
            if (isset($_FILES["image_3"]) && $_FILES["image_3"]["error"] == UPLOAD_ERR_OK) {
                // Get file details
                $fileTmpPath = $_FILES["image_3"]["tmp_name"];
                $fileName = $_FILES["image_3"]["name"];
                $fileSize = $_FILES["image_3"]["size"];
                $fileType = $_FILES["image_3"]["type"];

                // Specify the directory where you want to save the uploaded file
                $uploadDir = 'uploads/quotations/';

                // Create the directory if it doesn't exist
                if (!file_exists($uploadDir)) {
                    mkdir($uploadDir, 0777, true);
                }

                // Move the uploaded file to the specified directory
                $destPath_3 = $uploadDir . $fileName;
                if (move_uploaded_file($fileTmpPath, $destPath_3)) {
                    Yii::$app->getSession()->addFlash('success', "Quotation # 3 Uploaded Successfully");
                } else {
                    Yii::$app->getSession()->addFlash('error', "Unable to upload Quotation # 3");
                }
            } else {
                Yii::$app->getSession()->addFlash('error', "Quotation # 3 Not Selected");
            }

            $model->transaction_type = Yii::$app->request->post()['transaction_type'];
            $model->action = Yii::$app->request->post()['action'];
            $model->purpose = Yii::$app->request->post()['purpose'];

            if(isset(Yii::$app->request->post()['asset_id']))
            {
                $model->asset_id = Yii::$app->request->post()['asset_id'];
            }elseif(isset(Yii::$app->request->post()['expense_id']))
            {
                $model->asset_id = Yii::$app->request->post()['expense_id'];
            }

            $model->category_id = Yii::$app->request->post()['category_id'];
            $model->category_name = Yii::$app->request->post()['category_name'];
            $model->depriciation = Yii::$app->request->post()['depriciation'];
            $model->priority = Yii::$app->request->post()['priority'];
            $model->frequency = Yii::$app->request->post()['frequency'];
            $model->asset_location = Yii::$app->request->post()['asset_location'];
            $model->accounting_entry = Yii::$app->request->post()['accounting_entry'];
            $model->brand = Yii::$app->request->post()['brand'];
            $model->quantity = Yii::$app->request->post()['quantity'];
            $model->status = Yii::$app->request->post()['status'];
            $model->payment_mode = Yii::$app->request->post()['payment_mode'];
            $model->amount = Yii::$app->request->post()['amount'];
            $model->asset_life = Yii::$app->request->post()['asset_life'];

            if( Yii::$app->request->post()['payment_mode'] == "credit" && Yii::$app->request->post()['accounting_entry'] == "new" && isset(Yii::$app->request->post()['payment_mode']) )
            {
                $model->pay_bank = Yii::$app->request->post()['pay_bank'];
                $parts = explode(",", Yii::$app->request->post()['vendor']);

                $model->vendor_id = $parts[0];
                $model->vendor_name = $parts[1];
            }

            if( Yii::$app->request->post()['payment_mode'] == "cash" && isset(Yii::$app->request->post()['payment_mode']) && Yii::$app->request->post()['accounting_entry'] == "new" )
            {
                $model->pay_bank = Yii::$app->request->post()['pay_bank'];
            }

            $model->first_supplier = Yii::$app->request->post()['first_supplier'];
            $model->second_supplier = Yii::$app->request->post()['second_supplier'];
            $model->third_supplier = Yii::$app->request->post()['third_supplier'];
            if( isset($destPath_1) )
            {
                $model->image_1 = $destPath_1;
            }
            if( isset($destPath_2) )
            {
                $model->image_2 = $destPath_2;
            }
            if( isset($destPath_3) )
            {
                $model->image_3 = $destPath_3;
            }
            $model->applicant = Yii::$app->request->post()['applicant'];
            $model->department = Yii::$app->request->post()['department'];
            $model->approved_by = Yii::$app->request->post()['approved_by'];
            $model->approvers_dept = Yii::$app->request->post()['approvers_dept'];
            $model->asset_owner = Yii::$app->request->post()['asset_owner'];
            $model->entered_date = date("Y/m/d H:i:s");
            $model->approved_date = Yii::$app->request->post()['approved_date'];
            $model->purchasing_date = Yii::$app->request->post()['purchasing_date'];
            $model->target_date = Yii::$app->request->post()['target_date'];
            $model->collection_date = Yii::$app->request->post()['collection_date'];
            $model->meeting_date = Yii::$app->request->post()['meeting_date'];
            $model->start_date = Yii::$app->request->post()['start_date'];
            $model->expiry_date = Yii::$app->request->post()['expiry_date'];
            $model->frequency_start = Yii::$app->request->post()['frequency_start'];
            $model->frequency_end = Yii::$app->request->post()['frequency_end'];
            $model->updated_by = Yii::$app->user->identity->id;
            $model->updated_at = date("Y/m/d H:i:s");

            // if(Yii::$app->request->post()['status'] == "completed")
            // {

            //     $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
            //     $formattedTargetDate = $dateTime->format('Y-m-d');

            //     $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
            //     $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

            //     // print_r($formattedTargetDate);
            //     // echo "<br>";
            //     // print_r($formatteddateTimeToday);die;

            //     if( $formatteddateTimeToday > $formattedTargetDate  )
            //     {
            //         $model->status = "overdue";
            //         $model->completed_date = date("Y/m/d H:i:s");
            //     }elseif( $formatteddateTimeToday == $formattedTargetDate  )
            //     {
            //         $model->status = "completed";
            //         $model->completed_date = date("Y/m/d H:i:s");
            //     }elseif( $formattedTargetDate > $formatteddateTimeToday  )
            //     {
            //         $model->status = "completed_ahead";
            //         $model->completed_date = date("Y/m/d H:i:s");
            //     }
            // }else{
            //     $model->status = Yii::$app->request->post()['status'];
            // }
            $model->save();
            return $this->redirect(['index']);
        }


        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionCreateConsumable()
    {
        // dd(Yii::$app->request->post());
        $StartDate = new \DateTime(Yii::$app->request->post()['start_date']);
        $formatStartDate = $StartDate->format('Y-m-d');

        $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
        $formatEndDate = $EndDate->format('Y-m-d');

        if($formatStartDate > $formatEndDate)
        {
            Yii::$app->getSession()->addFlash('error', "End Date Must be greater than Start Date!");
            $referrer = Yii::$app->request->referrer;
            return $this->redirect($referrer ? $referrer : ['index']);
        }

        $user_id = Yii::$app->user->identity->id;

        $user_info = (new \yii\db\Query())
            ->select('user_profile_info.department_id')
            ->from('user_profile_info')
            ->where(['user_profile_info.user_id' => $user_id])
            ->one();

        $dept_info = (new \yii\db\Query())
            ->select(['department.id','department.title'])
            ->from('department')
            ->where(['department.id' => $user_info['department_id']])
            ->one();

        // dd(Yii::$app->request->post()['frequency_start']);

        if( isset(Yii::$app->request->post()['frequency']) && Yii::$app->request->post()['frequency'] == "daily" ){
            $StartDate = new \DateTime(Yii::$app->request->post()['start_date']);
            $formatStartDate = $StartDate->format('Y-m-d');

            $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
            $formatEndDate = $EndDate->format('Y-m-d');

            if($formatStartDate != $formatEndDate)
            {
                Yii::$app->getSession()->addFlash('error', "Start Date & End Date Must be Same for Daily Frequency!");
                $referrer = Yii::$app->request->referrer;
                return $this->redirect($referrer ? $referrer : ['index']);
            }

            // dd(Yii::$app->request->post());

            $frequencStart = new \DateTime(Yii::$app->request->post()['frequency_start']); // Replace with your actual start date
            $endDate = clone $frequencStart;
            $frequencyEnd = new \DateTime(Yii::$app->request->post()['frequency_end']);

            $interval = $frequencStart->diff($frequencyEnd);
            $numberOfDays = $interval->days;

            // dd($numberOfDays);

            $endDate->modify('+'.$numberOfDays.' days'); // Add number of days according to frequency

            $currentDate = clone $frequencStart;

            while ($currentDate <= $endDate) {
                if ($currentDate->format('N') <= 5) {
                    //Adding Expenses
                    $model = new ExpenseManager();
                    $model->frequency = Yii::$app->request->post()['frequency'];
                    // $model->department = $dept_info['title'];
                    $model->department = Yii::$app->request->post()['department'];
                    $model->module = Yii::$app->request->post()['module'];
                    $model->type = Yii::$app->request->post()['type'];
                    $model->modification = Yii::$app->request->post()['modification'];
                    $model->priority = Yii::$app->request->post()['priority'];
                    $model->description = Yii::$app->request->post()['description'];
                    $model->start_date = $currentDate->format("Y/m/d H:i:s");
                    $model->end_date = $currentDate->format("Y/m/d H:i:s");

                    $dateTime = new \DateTime(Yii::$app->request->post()['meeting_date']);
                    $formattedEndDate = $dateTime->format('Y-m-d');

                    $dateCurrent = new \DateTime($currentDate->format("Y/m/d H:i:s"));
                    $formattedCurrentDate = $dateCurrent->format('Y-m-d');

                    // print_r($formattedEndDate);
                    // echo "<br>";
                    // print_r($formattedCurrentDate);die;

                    if( $formattedCurrentDate == $formattedEndDate  )
                    {
                        $model->meeting_date = Yii::$app->request->post()['meeting_date'];
                        $model->expiry_date = Yii::$app->request->post()['expiry_date'];
                    }else{
                        $model->meeting_date = $currentDate->format("Y/m/d H:i:s");
                        $model->expiry_date = $currentDate->format("Y/m/d H:i:s");
                        // $interval = $dateCurrent->diff($dateTime);
                        // $numberOfDays = $interval->days;

                        // // Add day to the date
                        // $dateTime->add(new \DateInterval('P'.$numberOfDays.'D'));
                        // $model->meeting_date = $dateTime->format("Y/m/d H:i:s");
                    }

                    $model->responsibility = Yii::$app->request->post()['responsibility'];
                    $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                    $model->entered_by = Yii::$app->request->post()['entered_by'];
                    $model->created_by = Yii::$app->request->post()['entered_by'];
                    $model->verified_by = Yii::$app->request->post()['verified_by'];
                    $model->agreed_by = Yii::$app->request->post()['agreed_by'];

                    if(Yii::$app->request->post()['status'] == "completed")
                    {

                        $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                        $formattedTargetDate = $dateTime->format('Y-m-d');

                        $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                        $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                        // print_r($formattedTargetDate);
                        // echo "<br>";
                        // print_r($formatteddateTimeToday);die;

                        if( $formatteddateTimeToday > $formattedTargetDate  )
                        {
                            $model->status = "overdue";
                            $model->completed_date = date("Y/m/d H:i:s");
                        }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                        {
                            $model->status = "completed";
                            $model->completed_date = date("Y/m/d H:i:s");
                        }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                        {
                            $model->status = "completed_ahead";
                            $model->completed_date = date("Y/m/d H:i:s");
                        }
                    }else{
                        $model->status = Yii::$app->request->post()['status'];
                    }

                    $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                    $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                    $model->approved_by = Yii::$app->request->post()['approved_by'];
                    $model->created_at = date("Y/m/d H:i:s");
                    $model->save();
                    //end ending expense
                }
                $currentDate->modify('+1 day');
            }
            return $this->redirect(['index']);
        }elseif( isset(Yii::$app->request->post()['frequency']) && Yii::$app->request->post()['frequency'] == "monthly" ) {
            $frequencStart = new \DateTime(Yii::$app->request->post()['frequency_start']); // Replace with your actual start date
            $frequencyEnd = new \DateTime(Yii::$app->request->post()['frequency_end']);

            // Calculate the difference in months
            $interval = $frequencStart->diff($frequencyEnd);
            $months = $interval->y * 12 + $interval->m;

            for ($i = 0; $i <= $months; $i++) {
                $startDate = new \DateTime(Yii::$app->request->post()['start_date']);
                $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
                $MeetingDate = new \DateTime(Yii::$app->request->post()['meeting_date']);
                $ExpiryDate = new \DateTime(Yii::$app->request->post()['expiry_date']);

                $currentStartDate = clone $startDate;
                $currentEndDate = clone $EndDate;
                $MeetingDate = clone $MeetingDate;
                $ExpiryDate = clone $ExpiryDate;

                $currentStartDate->modify("+$i months");
                $currentEndDate->modify("+$i months");
                $MeetingDate->modify("+$i months");
                $ExpiryDate->modify("+$i months");

                // echo "expense " .$currentStartDate->format('Y-m-d') .' '. $currentEndDate->format('Y-m-d') . PHP_EOL;
                // echo "<br>";

                $model = new ExpenseManager();
                $model->frequency = Yii::$app->request->post()['frequency'];
                // $model->department = $dept_info['title'];
                $model->department = Yii::$app->request->post()['department'];
                $model->module = Yii::$app->request->post()['module'];
                $model->type = Yii::$app->request->post()['type'];
                $model->modification = Yii::$app->request->post()['modification'];
                $model->priority = Yii::$app->request->post()['priority'];
                $model->description = Yii::$app->request->post()['description'];
                $model->start_date = $currentStartDate->format("Y/m/d H:i:s");
                $model->end_date = $currentEndDate->format("Y/m/d H:i:s");
                $model->meeting_date = $MeetingDate->format("Y/m/d H:i:s");
                $model->expiry_date = $ExpiryDate->format("Y/m/d H:i:s");

                $model->responsibility = Yii::$app->request->post()['responsibility'];
                $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                $model->entered_by = Yii::$app->request->post()['entered_by'];
                $model->created_by = Yii::$app->request->post()['entered_by'];
                $model->verified_by = Yii::$app->request->post()['verified_by'];
                $model->agreed_by = Yii::$app->request->post()['agreed_by'];


                if(Yii::$app->request->post()['status'] == "completed")
                {

                    $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                    $formattedTargetDate = $dateTime->format('Y-m-d');

                    $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                    $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                    // print_r($formattedTargetDate);
                    // echo "<br>";
                    // print_r($formatteddateTimeToday);die;

                    if( $formatteddateTimeToday > $formattedTargetDate  )
                    {
                        $model->status = "overdue";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                    {
                        $model->status = "completed";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                    {
                        $model->status = "completed_ahead";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }
                }else{
                    $model->status = Yii::$app->request->post()['status'];
                }

                $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                $model->approved_by = Yii::$app->request->post()['approved_by'];
                $model->created_at = date("Y/m/d H:i:s");
                $model->save();
            }
            return $this->redirect(['index']);
        }elseif( isset(Yii::$app->request->post()['frequency']) && Yii::$app->request->post()['frequency'] == "yearly" ) {
            $frequencStart = new \DateTime(Yii::$app->request->post()['frequency_start']); // Replace with your actual start date
            $frequencyEnd = new \DateTime(Yii::$app->request->post()['frequency_end']);

            // Calculate the difference in years
            $interval = $frequencStart->diff($frequencyEnd);
            $numberOfYears = $interval->y;

            for ($i = 0; $i <= $numberOfYears; $i++) {
                $startDate = new \DateTime(Yii::$app->request->post()['start_date']);
                $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
                $MeetingDate = new \DateTime(Yii::$app->request->post()['meeting_date']);
                $ExpiryDate = new \DateTime(Yii::$app->request->post()['expiry_date']);
                $currentStartDate = clone $startDate;
                $currentEndDate = clone $EndDate;
                $MeetingDate = clone $MeetingDate;
                $ExpiryDate = clone $ExpiryDate;

                $currentStartDate->modify("+$i year");
                $currentEndDate->modify("+$i year");
                $MeetingDate->modify("+$i year");
                $ExpiryDate->modify("+$i year");

                // echo "expense " .$currentStartDate->format('Y-m-d') .' '. $currentEndDate->format('Y-m-d') . PHP_EOL;
                // echo "<br>";

                $model = new ExpenseManager();
                $model->frequency = Yii::$app->request->post()['frequency'];
                // $model->department = $dept_info['title'];
                $model->department = Yii::$app->request->post()['department'];
                $model->module = Yii::$app->request->post()['module'];
                $model->type = Yii::$app->request->post()['type'];
                $model->modification = Yii::$app->request->post()['modification'];
                $model->priority = Yii::$app->request->post()['priority'];
                $model->description = Yii::$app->request->post()['description'];
                $model->start_date = $currentStartDate->format("Y/m/d H:i:s");
                $model->end_date = $currentEndDate->format("Y/m/d H:i:s");
                $model->meeting_date = $MeetingDate->format("Y/m/d H:i:s");
                $model->expiry_date = $ExpiryDate->format("Y/m/d H:i:s");

                $model->responsibility = Yii::$app->request->post()['responsibility'];
                $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                $model->entered_by = Yii::$app->request->post()['entered_by'];
                $model->created_by = Yii::$app->request->post()['entered_by'];
                $model->verified_by = Yii::$app->request->post()['verified_by'];
                $model->agreed_by = Yii::$app->request->post()['agreed_by'];

                if(Yii::$app->request->post()['status'] == "completed")
                {

                    $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                    $formattedTargetDate = $dateTime->format('Y-m-d');

                    $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                    $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                    // print_r($formattedTargetDate);
                    // echo "<br>";
                    // print_r($formatteddateTimeToday);die;

                    if( $formatteddateTimeToday > $formattedTargetDate  )
                    {
                        $model->status = "overdue";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                    {
                        $model->status = "completed";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                    {
                        $model->status = "completed_ahead";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }
                }else{
                    $model->status = Yii::$app->request->post()['status'];
                }

                $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                $model->approved_by = Yii::$app->request->post()['approved_by'];
                $model->created_at = date("Y/m/d H:i:s");
                $model->save();
            }
            return $this->redirect(['index']);
        }elseif( isset(Yii::$app->request->post()['frequency']) && Yii::$app->request->post()['frequency'] == "weekly" ) {

            $frequencStart = new \DateTime(Yii::$app->request->post()['frequency_start']); // Replace with your actual start date
            $frequencyEnd = new \DateTime(Yii::$app->request->post()['frequency_end']);

            $currentDate = clone $frequencStart;


            // Iterate through each week and perform the expense
            $count=0;
            while ($currentDate <= $frequencyEnd) {

                if($count==0)
                {
                    $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
                    $MeetingDate = new \DateTime(Yii::$app->request->post()['meeting_date']);
                    $ExpiryDate = new \DateTime(Yii::$app->request->post()['expiry_date']);

                    $currentEndDate = clone $EndDate;
                    $currentMeetingDate = clone $MeetingDate;
                    $currentExpiryDate = clone $MeetingDate;
                }


                $model = new ExpenseManager();
                $model->frequency = Yii::$app->request->post()['frequency'];
                // $model->department = $dept_info['title'];
                $model->department = Yii::$app->request->post()['department'];
                $model->module = Yii::$app->request->post()['module'];
                $model->type = Yii::$app->request->post()['type'];
                $model->modification = Yii::$app->request->post()['modification'];
                $model->priority = Yii::$app->request->post()['priority'];
                $model->description = Yii::$app->request->post()['description'];
                $model->start_date = $currentDate->format("Y/m/d H:i:s");
                $model->end_date = $currentEndDate->format("Y/m/d H:i:s");
                $model->meeting_date = $currentMeetingDate->format("Y/m/d H:i:s");
                $model->expiry_date = $currentExpiryDate->format("Y/m/d H:i:s");

                $model->responsibility = Yii::$app->request->post()['responsibility'];
                $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                $model->entered_by = Yii::$app->request->post()['entered_by'];
                $model->created_by = Yii::$app->request->post()['entered_by'];
                $model->verified_by = Yii::$app->request->post()['verified_by'];
                $model->agreed_by = Yii::$app->request->post()['agreed_by'];

                if(Yii::$app->request->post()['status'] == "completed")
                {

                    $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                    $formattedTargetDate = $dateTime->format('Y-m-d');

                    $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                    $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                    // print_r($formattedTargetDate);
                    // echo "<br>";
                    // print_r($formatteddateTimeToday);die;

                    if( $formatteddateTimeToday > $formattedTargetDate  )
                    {
                        $model->status = "overdue";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                    {
                        $model->status = "completed";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                    {
                        $model->status = "completed_ahead";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }
                }else{
                    $model->status = Yii::$app->request->post()['status'];
                }

                $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                $model->approved_by = Yii::$app->request->post()['approved_by'];
                $model->created_at = date("Y/m/d H:i:s");
                $model->save();


                $currentDate->modify('+7 days');
                $currentEndDate->modify('+7 days');
                $currentMeetingDate->modify('+7 days');
                $count++;
            }
            return $this->redirect(['index']);

        }elseif( isset(Yii::$app->request->post()['frequency']) && Yii::$app->request->post()['frequency'] == "bi-annual" ) {

            $frequencStart = new \DateTime(Yii::$app->request->post()['frequency_start']); // Replace with your actual start date
            $frequencyEnd = new \DateTime(Yii::$app->request->post()['frequency_end']);

            $currentDate = clone $frequencStart;


            // Iterate through each week and perform the expense
            $count=0;
            while ($currentDate <= $frequencyEnd) {

                if($count==0)
                {
                    $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
                    $MeetingDate = new \DateTime(Yii::$app->request->post()['meeting_date']);
                    $ExpiryDate = new \DateTime(Yii::$app->request->post()['expiry_date']);

                    $currentEndDate = clone $EndDate;
                    $currentMeetingDate = clone $MeetingDate;
                    $currentExpiryDate = clone $ExpiryDate;
                }


                $model = new ExpenseManager();
                $model->frequency = Yii::$app->request->post()['frequency'];
                // $model->department = $dept_info['title'];
                $model->department = Yii::$app->request->post()['department'];
                $model->module = Yii::$app->request->post()['module'];
                $model->type = Yii::$app->request->post()['type'];
                $model->modification = Yii::$app->request->post()['modification'];
                $model->priority = Yii::$app->request->post()['priority'];
                $model->description = Yii::$app->request->post()['description'];
                $model->start_date = $currentDate->format("Y/m/d H:i:s");
                $model->end_date = $currentEndDate->format("Y/m/d H:i:s");
                $model->meeting_date = $currentMeetingDate->format("Y/m/d H:i:s");
                $model->expiry_date = $currentExpiryDate->format("Y/m/d H:i:s");

                $model->responsibility = Yii::$app->request->post()['responsibility'];
                $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                $model->entered_by = Yii::$app->request->post()['entered_by'];
                $model->created_by = Yii::$app->request->post()['entered_by'];
                $model->verified_by = Yii::$app->request->post()['verified_by'];
                $model->agreed_by = Yii::$app->request->post()['agreed_by'];

                if(Yii::$app->request->post()['status'] == "completed")
                {

                    $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                    $formattedTargetDate = $dateTime->format('Y-m-d');

                    $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                    $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                    // print_r($formattedTargetDate);
                    // echo "<br>";
                    // print_r($formatteddateTimeToday);die;

                    if( $formatteddateTimeToday > $formattedTargetDate  )
                    {
                        $model->status = "overdue";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                    {
                        $model->status = "completed";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                    {
                        $model->status = "completed_ahead";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }
                }else{
                    $model->status = Yii::$app->request->post()['status'];
                }

                $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                $model->approved_by = Yii::$app->request->post()['approved_by'];
                $model->created_at = date("Y/m/d H:i:s");
                $model->save();


                $currentDate->modify('+6 months');
                $currentEndDate->modify('+6 months');
                $currentMeetingDate->modify('+6 months');
                $count++;
            }
            return $this->redirect(['index']);

        }else{
            // dd(Yii::$app->request->post());
            if(!empty(Yii::$app->request->post()))
            {
                // dd(Yii::$app->request->post());
                // Check if Quotation 1
                if (isset($_FILES["image_1"]) && $_FILES["image_1"]["error"] == UPLOAD_ERR_OK) {
                    // Get file details
                    $fileTmpPath = $_FILES["image_1"]["tmp_name"];
                    $fileName = $_FILES["image_1"]["name"];
                    $fileSize = $_FILES["image_1"]["size"];
                    $fileType = $_FILES["image_1"]["type"];

                    // Specify the directory where you want to save the uploaded file
                    $uploadDir = 'uploads/quotations/';

                    // Create the directory if it doesn't exist
                    if (!file_exists($uploadDir)) {
                        mkdir($uploadDir, 0777, true);
                    }

                    // Move the uploaded file to the specified directory
                    $destPath_1 = $uploadDir . $fileName;
                    if (move_uploaded_file($fileTmpPath, $destPath_1)) {
                        Yii::$app->getSession()->addFlash('success', "Quotation # 1 Uploaded Successfully");
                    } else {
                        Yii::$app->getSession()->addFlash('error', "Unable to upload Quotation # 1");
                    }
                } else {
                    Yii::$app->getSession()->addFlash('error', "Quotation # 1 Not Selected");
                }

                // Check if Quotation 2
                if (isset($_FILES["image_2"]) && $_FILES["image_2"]["error"] == UPLOAD_ERR_OK) {
                    // Get file details
                    $fileTmpPath = $_FILES["image_2"]["tmp_name"];
                    $fileName = $_FILES["image_2"]["name"];
                    $fileSize = $_FILES["image_2"]["size"];
                    $fileType = $_FILES["image_2"]["type"];

                    // Specify the directory where you want to save the uploaded file
                    $uploadDir = 'uploads/quotations/';

                    // Create the directory if it doesn't exist
                    if (!file_exists($uploadDir)) {
                        mkdir($uploadDir, 0777, true);
                    }

                    // Move the uploaded file to the specified directory
                    $destPath_2 = $uploadDir . $fileName;
                    if (move_uploaded_file($fileTmpPath, $destPath_2)) {
                        Yii::$app->getSession()->addFlash('success', "Quotation # 2 Uploaded Successfully");
                    } else {
                        Yii::$app->getSession()->addFlash('error', "Unable to upload Quotation # 2");
                    }
                } else {
                    Yii::$app->getSession()->addFlash('error', "Quotation # 2 Not Selected");
                }

                // Check if Quotation 1
                if (isset($_FILES["image_3"]) && $_FILES["image_3"]["error"] == UPLOAD_ERR_OK) {
                    // Get file details
                    $fileTmpPath = $_FILES["image_3"]["tmp_name"];
                    $fileName = $_FILES["image_3"]["name"];
                    $fileSize = $_FILES["image_3"]["size"];
                    $fileType = $_FILES["image_3"]["type"];

                    // Specify the directory where you want to save the uploaded file
                    $uploadDir = 'uploads/quotations/';

                    // Create the directory if it doesn't exist
                    if (!file_exists($uploadDir)) {
                        mkdir($uploadDir, 0777, true);
                    }

                    // Move the uploaded file to the specified directory
                    $destPath_3 = $uploadDir . $fileName;
                    if (move_uploaded_file($fileTmpPath, $destPath_3)) {
                        Yii::$app->getSession()->addFlash('success', "Quotation # 3 Uploaded Successfully");
                    } else {
                        Yii::$app->getSession()->addFlash('error', "Unable to upload Quotation # 3");
                    }
                } else {
                    Yii::$app->getSession()->addFlash('error', "Quotation # 3 Not Selected");
                }

                $model = new ExpenseManager();
                $model->transaction_type = Yii::$app->request->post()['transaction_type'];
                $model->action = Yii::$app->request->post()['action'];
                $model->purpose = Yii::$app->request->post()['purpose'];

                if(isset(Yii::$app->request->post()['asset_id']))
                {
                    $model->asset_id = Yii::$app->request->post()['asset_id'];
                }elseif(isset(Yii::$app->request->post()['expense_id']))
                {
                    $model->asset_id = Yii::$app->request->post()['expense_id'];
                }

                $model->category_id = Yii::$app->request->post()['category_id'];
                $model->category_name = Yii::$app->request->post()['category_name'];
                $model->depriciation = Yii::$app->request->post()['depriciation'];
                $model->priority = Yii::$app->request->post()['priority'];
                $model->frequency = Yii::$app->request->post()['frequency'];
                $model->asset_location = Yii::$app->request->post()['asset_location'];
                $model->accounting_entry = Yii::$app->request->post()['accounting_entry'];
                $model->brand = Yii::$app->request->post()['brand'];
                $model->quantity = Yii::$app->request->post()['quantity'];
                $model->status = Yii::$app->request->post()['status'];
                $model->payment_mode = Yii::$app->request->post()['payment_mode'];
                $model->amount = Yii::$app->request->post()['amount'];
                $model->asset_life = Yii::$app->request->post()['asset_life'];

                if( Yii::$app->request->post()['accounting_entry'] == "new" && isset(Yii::$app->request->post()['vendor']) && Yii::$app->request->post()['payment_mode'] == "credit" )
                {
                    $model->pay_bank = Yii::$app->request->post()['pay_bank'];
                    $parts = explode(",", Yii::$app->request->post()['vendor']);

                    $model->vendor_id = $parts[0];
                    $model->vendor_name = $parts[1];
                }

                if( isset(Yii::$app->request->post()['accounting_entry']) && Yii::$app->request->post()['accounting_entry'] == "new" && Yii::$app->request->post()['payment_mode'] == "cash" )
                {
                    $model->pay_bank = Yii::$app->request->post()['pay_bank'];
                }

                $model->first_supplier = Yii::$app->request->post()['first_supplier'];
                $model->second_supplier = Yii::$app->request->post()['second_supplier'];
                $model->third_supplier = Yii::$app->request->post()['third_supplier'];
                $model->image_1 = $destPath_1;
                $model->image_2 = $destPath_2;
                $model->image_3 = $destPath_3;
                $model->applicant = Yii::$app->request->post()['applicant'];
                $model->department = Yii::$app->request->post()['department'];
                $model->approved_by = Yii::$app->request->post()['approved_by'];
                $model->approvers_dept = Yii::$app->request->post()['approvers_dept'];
                $model->asset_owner = Yii::$app->request->post()['asset_owner'];
                $model->entered_date = date("Y/m/d H:i:s");
                $model->approved_date = Yii::$app->request->post()['approved_date'];
                $model->purchasing_date = Yii::$app->request->post()['purchasing_date'];
                $model->target_date = Yii::$app->request->post()['target_date'];
                $model->collection_date = Yii::$app->request->post()['collection_date'];
                $model->meeting_date = Yii::$app->request->post()['meeting_date'];
                $model->start_date = Yii::$app->request->post()['start_date'];
                $model->expiry_date = Yii::$app->request->post()['expiry_date'];
                $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                $model->created_by = Yii::$app->user->identity->id;
                $model->created_at = date("Y/m/d H:i:s");

                $model->save();
                return $this->redirect(['index']);
            }
        }

        return $this->render('create_consumable', [
            'model' => $model,
        ]);
    }

    public function actionUpdateConsumable($id)
    {
        $StartDate = new \DateTime(Yii::$app->request->post()['start_date']);
        $formatStartDate = $StartDate->format('Y-m-d');

        $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
        $formatEndDate = $EndDate->format('Y-m-d');

        if($formatStartDate > $formatEndDate)
        {
            Yii::$app->getSession()->addFlash('error', "End Date Must be greater than Start Date!");
            $referrer = Yii::$app->request->referrer;
            return $this->redirect($referrer ? $referrer : ['index']);
        }

        $user_id = Yii::$app->user->identity->id;

        $user_info = (new \yii\db\Query())
            ->select('user_profile_info.department_id')
            ->from('user_profile_info')
            ->where(['user_profile_info.user_id' => $user_id])
            ->one();

        $dept_info = (new \yii\db\Query())
            ->select(['department.id','department.title'])
            ->from('department')
            ->where(['department.id' => $user_info['department_id']])
            ->one();

        $model = ExpenseManager::findOne(['id' => $id]);
        if(!empty(Yii::$app->request->post()))
        {

            if( Yii::$app->request->post()['action'] == "throw" )
            {
                Yii::$app->controller->actionDelete($model->id);
                return $this->redirect(['index']);
            }

            // dd(Yii::$app->request->post());
            // Check if Quotation 1
            if (isset($_FILES["image_1"]) && $_FILES["image_1"]["error"] == UPLOAD_ERR_OK) {
                // Get file details
                $fileTmpPath = $_FILES["image_1"]["tmp_name"];
                $fileName = $_FILES["image_1"]["name"];
                $fileSize = $_FILES["image_1"]["size"];
                $fileType = $_FILES["image_1"]["type"];

                // Specify the directory where you want to save the uploaded file
                $uploadDir = 'uploads/quotations/';

                // Create the directory if it doesn't exist
                if (!file_exists($uploadDir)) {
                    mkdir($uploadDir, 0777, true);
                }

                // Move the uploaded file to the specified directory
                $destPath_1 = $uploadDir . $fileName;
                if (move_uploaded_file($fileTmpPath, $destPath_1)) {
                    Yii::$app->getSession()->addFlash('success', "Quotation # 1 Uploaded Successfully");
                } else {
                    Yii::$app->getSession()->addFlash('error', "Unable to upload Quotation # 1");
                }
            } else {
                Yii::$app->getSession()->addFlash('error', "Quotation # 1 Not Selected");
            }

            // Check if Quotation 2
            if (isset($_FILES["image_2"]) && $_FILES["image_2"]["error"] == UPLOAD_ERR_OK) {
                // Get file details
                $fileTmpPath = $_FILES["image_2"]["tmp_name"];
                $fileName = $_FILES["image_2"]["name"];
                $fileSize = $_FILES["image_2"]["size"];
                $fileType = $_FILES["image_2"]["type"];

                // Specify the directory where you want to save the uploaded file
                $uploadDir = 'uploads/quotations/';

                // Create the directory if it doesn't exist
                if (!file_exists($uploadDir)) {
                    mkdir($uploadDir, 0777, true);
                }

                // Move the uploaded file to the specified directory
                $destPath_2 = $uploadDir . $fileName;
                if (move_uploaded_file($fileTmpPath, $destPath_2)) {
                    Yii::$app->getSession()->addFlash('success', "Quotation # 2 Uploaded Successfully");
                } else {
                    Yii::$app->getSession()->addFlash('error', "Unable to upload Quotation # 2");
                }
            } else {
                Yii::$app->getSession()->addFlash('error', "Quotation # 2 Not Selected");
            }

            // Check if Quotation 1
            if (isset($_FILES["image_3"]) && $_FILES["image_3"]["error"] == UPLOAD_ERR_OK) {
                // Get file details
                $fileTmpPath = $_FILES["image_3"]["tmp_name"];
                $fileName = $_FILES["image_3"]["name"];
                $fileSize = $_FILES["image_3"]["size"];
                $fileType = $_FILES["image_3"]["type"];

                // Specify the directory where you want to save the uploaded file
                $uploadDir = 'uploads/quotations/';

                // Create the directory if it doesn't exist
                if (!file_exists($uploadDir)) {
                    mkdir($uploadDir, 0777, true);
                }

                // Move the uploaded file to the specified directory
                $destPath_3 = $uploadDir . $fileName;
                if (move_uploaded_file($fileTmpPath, $destPath_3)) {
                    Yii::$app->getSession()->addFlash('success', "Quotation # 3 Uploaded Successfully");
                } else {
                    Yii::$app->getSession()->addFlash('error', "Unable to upload Quotation # 3");
                }
            } else {
                Yii::$app->getSession()->addFlash('error', "Quotation # 3 Not Selected");
            }

            $model->transaction_type = Yii::$app->request->post()['transaction_type'];
            $model->action = Yii::$app->request->post()['action'];
            $model->purpose = Yii::$app->request->post()['purpose'];

            if(isset(Yii::$app->request->post()['asset_id']))
            {
                $model->asset_id = Yii::$app->request->post()['asset_id'];
            }elseif(isset(Yii::$app->request->post()['expense_id']))
            {
                $model->asset_id = Yii::$app->request->post()['expense_id'];
            }

            $model->category_id = Yii::$app->request->post()['category_id'];
            $model->category_name = Yii::$app->request->post()['category_name'];
            $model->depriciation = Yii::$app->request->post()['depriciation'];
            $model->priority = Yii::$app->request->post()['priority'];
            $model->frequency = Yii::$app->request->post()['frequency'];
            $model->asset_location = Yii::$app->request->post()['asset_location'];
            $model->accounting_entry = Yii::$app->request->post()['accounting_entry'];
            $model->brand = Yii::$app->request->post()['brand'];
            $model->quantity = Yii::$app->request->post()['quantity'];
            $model->status = Yii::$app->request->post()['status'];
            $model->payment_mode = Yii::$app->request->post()['payment_mode'];
            $model->amount = Yii::$app->request->post()['amount'];
            $model->asset_life = Yii::$app->request->post()['asset_life'];

            if( Yii::$app->request->post()['payment_mode'] == "credit" && Yii::$app->request->post()['accounting_entry'] == "new" && isset(Yii::$app->request->post()['payment_mode']) )
            {
                $model->pay_bank = Yii::$app->request->post()['pay_bank'];
                $parts = explode(",", Yii::$app->request->post()['vendor']);

                $model->vendor_id = $parts[0];
                $model->vendor_name = $parts[1];
            }

            if( Yii::$app->request->post()['payment_mode'] == "cash" && isset(Yii::$app->request->post()['payment_mode']) && Yii::$app->request->post()['accounting_entry'] == "new" )
            {
                $model->pay_bank = Yii::$app->request->post()['pay_bank'];
            }

            $model->first_supplier = Yii::$app->request->post()['first_supplier'];
            $model->second_supplier = Yii::$app->request->post()['second_supplier'];
            $model->third_supplier = Yii::$app->request->post()['third_supplier'];
            if( isset($destPath_1) )
            {
                $model->image_1 = $destPath_1;
            }
            if( isset($destPath_2) )
            {
                $model->image_2 = $destPath_2;
            }
            if( isset($destPath_3) )
            {
                $model->image_3 = $destPath_3;
            }
            $model->applicant = Yii::$app->request->post()['applicant'];
            $model->department = Yii::$app->request->post()['department'];
            $model->approved_by = Yii::$app->request->post()['approved_by'];
            $model->approvers_dept = Yii::$app->request->post()['approvers_dept'];
            $model->asset_owner = Yii::$app->request->post()['asset_owner'];
            $model->entered_date = date("Y/m/d H:i:s");
            $model->approved_date = Yii::$app->request->post()['approved_date'];
            $model->purchasing_date = Yii::$app->request->post()['purchasing_date'];
            $model->target_date = Yii::$app->request->post()['target_date'];
            $model->collection_date = Yii::$app->request->post()['collection_date'];
            $model->meeting_date = Yii::$app->request->post()['meeting_date'];
            $model->start_date = Yii::$app->request->post()['start_date'];
            $model->expiry_date = Yii::$app->request->post()['expiry_date'];
            $model->frequency_start = Yii::$app->request->post()['frequency_start'];
            $model->frequency_end = Yii::$app->request->post()['frequency_end'];
            $model->updated_by = Yii::$app->user->identity->id;
            $model->updated_at = date("Y/m/d H:i:s");

            // if(Yii::$app->request->post()['status'] == "completed")
            // {

            //     $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
            //     $formattedTargetDate = $dateTime->format('Y-m-d');

            //     $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
            //     $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

            //     // print_r($formattedTargetDate);
            //     // echo "<br>";
            //     // print_r($formatteddateTimeToday);die;

            //     if( $formatteddateTimeToday > $formattedTargetDate  )
            //     {
            //         $model->status = "overdue";
            //         $model->completed_date = date("Y/m/d H:i:s");
            //     }elseif( $formatteddateTimeToday == $formattedTargetDate  )
            //     {
            //         $model->status = "completed";
            //         $model->completed_date = date("Y/m/d H:i:s");
            //     }elseif( $formattedTargetDate > $formatteddateTimeToday  )
            //     {
            //         $model->status = "completed_ahead";
            //         $model->completed_date = date("Y/m/d H:i:s");
            //     }
            // }else{
            //     $model->status = Yii::$app->request->post()['status'];
            // }
            $model->save();
            return $this->redirect(['index']);
        }


        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionCreateService()
    {
        // dd(Yii::$app->request->post());
        $StartDate = new \DateTime(Yii::$app->request->post()['start_date']);
        $formatStartDate = $StartDate->format('Y-m-d');

        $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
        $formatEndDate = $EndDate->format('Y-m-d');

        if($formatStartDate > $formatEndDate)
        {
            Yii::$app->getSession()->addFlash('error', "End Date Must be greater than Start Date!");
            $referrer = Yii::$app->request->referrer;
            return $this->redirect($referrer ? $referrer : ['index']);
        }

        $user_id = Yii::$app->user->identity->id;

        $user_info = (new \yii\db\Query())
            ->select('user_profile_info.department_id')
            ->from('user_profile_info')
            ->where(['user_profile_info.user_id' => $user_id])
            ->one();

        $dept_info = (new \yii\db\Query())
            ->select(['department.id','department.title'])
            ->from('department')
            ->where(['department.id' => $user_info['department_id']])
            ->one();

        // dd(Yii::$app->request->post()['frequency_start']);

        if( isset(Yii::$app->request->post()['frequency']) && Yii::$app->request->post()['frequency'] == "daily" ){
            $StartDate = new \DateTime(Yii::$app->request->post()['start_date']);
            $formatStartDate = $StartDate->format('Y-m-d');

            $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
            $formatEndDate = $EndDate->format('Y-m-d');

            if($formatStartDate != $formatEndDate)
            {
                Yii::$app->getSession()->addFlash('error', "Start Date & End Date Must be Same for Daily Frequency!");
                $referrer = Yii::$app->request->referrer;
                return $this->redirect($referrer ? $referrer : ['index']);
            }

            // dd(Yii::$app->request->post());

            $frequencStart = new \DateTime(Yii::$app->request->post()['frequency_start']); // Replace with your actual start date
            $endDate = clone $frequencStart;
            $frequencyEnd = new \DateTime(Yii::$app->request->post()['frequency_end']);

            $interval = $frequencStart->diff($frequencyEnd);
            $numberOfDays = $interval->days;

            // dd($numberOfDays);

            $endDate->modify('+'.$numberOfDays.' days'); // Add number of days according to frequency

            $currentDate = clone $frequencStart;

            while ($currentDate <= $endDate) {
                if ($currentDate->format('N') <= 5) {
                    //Adding Expenses
                    $model = new ExpenseManager();
                    $model->frequency = Yii::$app->request->post()['frequency'];
                    // $model->department = $dept_info['title'];
                    $model->department = Yii::$app->request->post()['department'];
                    $model->module = Yii::$app->request->post()['module'];
                    $model->type = Yii::$app->request->post()['type'];
                    $model->modification = Yii::$app->request->post()['modification'];
                    $model->priority = Yii::$app->request->post()['priority'];
                    $model->description = Yii::$app->request->post()['description'];
                    $model->start_date = $currentDate->format("Y/m/d H:i:s");
                    $model->end_date = $currentDate->format("Y/m/d H:i:s");

                    $dateTime = new \DateTime(Yii::$app->request->post()['meeting_date']);
                    $formattedEndDate = $dateTime->format('Y-m-d');

                    $dateCurrent = new \DateTime($currentDate->format("Y/m/d H:i:s"));
                    $formattedCurrentDate = $dateCurrent->format('Y-m-d');

                    // print_r($formattedEndDate);
                    // echo "<br>";
                    // print_r($formattedCurrentDate);die;

                    if( $formattedCurrentDate == $formattedEndDate  )
                    {
                        $model->meeting_date = Yii::$app->request->post()['meeting_date'];
                        $model->expiry_date = Yii::$app->request->post()['expiry_date'];
                    }else{
                        $model->meeting_date = $currentDate->format("Y/m/d H:i:s");
                        $model->expiry_date = $currentDate->format("Y/m/d H:i:s");
                        // $interval = $dateCurrent->diff($dateTime);
                        // $numberOfDays = $interval->days;

                        // // Add day to the date
                        // $dateTime->add(new \DateInterval('P'.$numberOfDays.'D'));
                        // $model->meeting_date = $dateTime->format("Y/m/d H:i:s");
                    }

                    $model->responsibility = Yii::$app->request->post()['responsibility'];
                    $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                    $model->entered_by = Yii::$app->request->post()['entered_by'];
                    $model->created_by = Yii::$app->request->post()['entered_by'];
                    $model->verified_by = Yii::$app->request->post()['verified_by'];
                    $model->agreed_by = Yii::$app->request->post()['agreed_by'];

                    if(Yii::$app->request->post()['status'] == "completed")
                    {

                        $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                        $formattedTargetDate = $dateTime->format('Y-m-d');

                        $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                        $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                        // print_r($formattedTargetDate);
                        // echo "<br>";
                        // print_r($formatteddateTimeToday);die;

                        if( $formatteddateTimeToday > $formattedTargetDate  )
                        {
                            $model->status = "overdue";
                            $model->completed_date = date("Y/m/d H:i:s");
                        }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                        {
                            $model->status = "completed";
                            $model->completed_date = date("Y/m/d H:i:s");
                        }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                        {
                            $model->status = "completed_ahead";
                            $model->completed_date = date("Y/m/d H:i:s");
                        }
                    }else{
                        $model->status = Yii::$app->request->post()['status'];
                    }

                    $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                    $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                    $model->approved_by = Yii::$app->request->post()['approved_by'];
                    $model->created_at = date("Y/m/d H:i:s");
                    $model->save();
                    //end ending expense
                }
                $currentDate->modify('+1 day');
            }
            return $this->redirect(['index']);
        }elseif( isset(Yii::$app->request->post()['frequency']) && Yii::$app->request->post()['frequency'] == "monthly" ) {
            $frequencStart = new \DateTime(Yii::$app->request->post()['frequency_start']); // Replace with your actual start date
            $frequencyEnd = new \DateTime(Yii::$app->request->post()['frequency_end']);

            // Calculate the difference in months
            $interval = $frequencStart->diff($frequencyEnd);
            $months = $interval->y * 12 + $interval->m;

            for ($i = 0; $i <= $months; $i++) {
                $startDate = new \DateTime(Yii::$app->request->post()['start_date']);
                $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
                $MeetingDate = new \DateTime(Yii::$app->request->post()['meeting_date']);
                $ExpiryDate = new \DateTime(Yii::$app->request->post()['expiry_date']);

                $currentStartDate = clone $startDate;
                $currentEndDate = clone $EndDate;
                $MeetingDate = clone $MeetingDate;
                $ExpiryDate = clone $ExpiryDate;

                $currentStartDate->modify("+$i months");
                $currentEndDate->modify("+$i months");
                $MeetingDate->modify("+$i months");
                $ExpiryDate->modify("+$i months");

                // echo "expense " .$currentStartDate->format('Y-m-d') .' '. $currentEndDate->format('Y-m-d') . PHP_EOL;
                // echo "<br>";

                $model = new ExpenseManager();
                $model->frequency = Yii::$app->request->post()['frequency'];
                // $model->department = $dept_info['title'];
                $model->department = Yii::$app->request->post()['department'];
                $model->module = Yii::$app->request->post()['module'];
                $model->type = Yii::$app->request->post()['type'];
                $model->modification = Yii::$app->request->post()['modification'];
                $model->priority = Yii::$app->request->post()['priority'];
                $model->description = Yii::$app->request->post()['description'];
                $model->start_date = $currentStartDate->format("Y/m/d H:i:s");
                $model->end_date = $currentEndDate->format("Y/m/d H:i:s");
                $model->meeting_date = $MeetingDate->format("Y/m/d H:i:s");
                $model->expiry_date = $ExpiryDate->format("Y/m/d H:i:s");

                $model->responsibility = Yii::$app->request->post()['responsibility'];
                $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                $model->entered_by = Yii::$app->request->post()['entered_by'];
                $model->created_by = Yii::$app->request->post()['entered_by'];
                $model->verified_by = Yii::$app->request->post()['verified_by'];
                $model->agreed_by = Yii::$app->request->post()['agreed_by'];


                if(Yii::$app->request->post()['status'] == "completed")
                {

                    $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                    $formattedTargetDate = $dateTime->format('Y-m-d');

                    $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                    $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                    // print_r($formattedTargetDate);
                    // echo "<br>";
                    // print_r($formatteddateTimeToday);die;

                    if( $formatteddateTimeToday > $formattedTargetDate  )
                    {
                        $model->status = "overdue";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                    {
                        $model->status = "completed";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                    {
                        $model->status = "completed_ahead";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }
                }else{
                    $model->status = Yii::$app->request->post()['status'];
                }

                $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                $model->approved_by = Yii::$app->request->post()['approved_by'];
                $model->created_at = date("Y/m/d H:i:s");
                $model->save();
            }
            return $this->redirect(['index']);
        }elseif( isset(Yii::$app->request->post()['frequency']) && Yii::$app->request->post()['frequency'] == "yearly" ) {
            $frequencStart = new \DateTime(Yii::$app->request->post()['frequency_start']); // Replace with your actual start date
            $frequencyEnd = new \DateTime(Yii::$app->request->post()['frequency_end']);

            // Calculate the difference in years
            $interval = $frequencStart->diff($frequencyEnd);
            $numberOfYears = $interval->y;

            for ($i = 0; $i <= $numberOfYears; $i++) {
                $startDate = new \DateTime(Yii::$app->request->post()['start_date']);
                $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
                $MeetingDate = new \DateTime(Yii::$app->request->post()['meeting_date']);
                $ExpiryDate = new \DateTime(Yii::$app->request->post()['expiry_date']);
                $currentStartDate = clone $startDate;
                $currentEndDate = clone $EndDate;
                $MeetingDate = clone $MeetingDate;
                $ExpiryDate = clone $ExpiryDate;

                $currentStartDate->modify("+$i year");
                $currentEndDate->modify("+$i year");
                $MeetingDate->modify("+$i year");
                $ExpiryDate->modify("+$i year");

                // echo "expense " .$currentStartDate->format('Y-m-d') .' '. $currentEndDate->format('Y-m-d') . PHP_EOL;
                // echo "<br>";

                $model = new ExpenseManager();
                $model->frequency = Yii::$app->request->post()['frequency'];
                // $model->department = $dept_info['title'];
                $model->department = Yii::$app->request->post()['department'];
                $model->module = Yii::$app->request->post()['module'];
                $model->type = Yii::$app->request->post()['type'];
                $model->modification = Yii::$app->request->post()['modification'];
                $model->priority = Yii::$app->request->post()['priority'];
                $model->description = Yii::$app->request->post()['description'];
                $model->start_date = $currentStartDate->format("Y/m/d H:i:s");
                $model->end_date = $currentEndDate->format("Y/m/d H:i:s");
                $model->meeting_date = $MeetingDate->format("Y/m/d H:i:s");
                $model->expiry_date = $ExpiryDate->format("Y/m/d H:i:s");

                $model->responsibility = Yii::$app->request->post()['responsibility'];
                $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                $model->entered_by = Yii::$app->request->post()['entered_by'];
                $model->created_by = Yii::$app->request->post()['entered_by'];
                $model->verified_by = Yii::$app->request->post()['verified_by'];
                $model->agreed_by = Yii::$app->request->post()['agreed_by'];

                if(Yii::$app->request->post()['status'] == "completed")
                {

                    $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                    $formattedTargetDate = $dateTime->format('Y-m-d');

                    $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                    $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                    // print_r($formattedTargetDate);
                    // echo "<br>";
                    // print_r($formatteddateTimeToday);die;

                    if( $formatteddateTimeToday > $formattedTargetDate  )
                    {
                        $model->status = "overdue";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                    {
                        $model->status = "completed";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                    {
                        $model->status = "completed_ahead";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }
                }else{
                    $model->status = Yii::$app->request->post()['status'];
                }

                $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                $model->approved_by = Yii::$app->request->post()['approved_by'];
                $model->created_at = date("Y/m/d H:i:s");
                $model->save();
            }
            return $this->redirect(['index']);
        }elseif( isset(Yii::$app->request->post()['frequency']) && Yii::$app->request->post()['frequency'] == "weekly" ) {

            $frequencStart = new \DateTime(Yii::$app->request->post()['frequency_start']); // Replace with your actual start date
            $frequencyEnd = new \DateTime(Yii::$app->request->post()['frequency_end']);

            $currentDate = clone $frequencStart;


            // Iterate through each week and perform the expense
            $count=0;
            while ($currentDate <= $frequencyEnd) {

                if($count==0)
                {
                    $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
                    $MeetingDate = new \DateTime(Yii::$app->request->post()['meeting_date']);
                    $ExpiryDate = new \DateTime(Yii::$app->request->post()['expiry_date']);

                    $currentEndDate = clone $EndDate;
                    $currentMeetingDate = clone $MeetingDate;
                    $currentExpiryDate = clone $MeetingDate;
                }


                $model = new ExpenseManager();
                $model->frequency = Yii::$app->request->post()['frequency'];
                // $model->department = $dept_info['title'];
                $model->department = Yii::$app->request->post()['department'];
                $model->module = Yii::$app->request->post()['module'];
                $model->type = Yii::$app->request->post()['type'];
                $model->modification = Yii::$app->request->post()['modification'];
                $model->priority = Yii::$app->request->post()['priority'];
                $model->description = Yii::$app->request->post()['description'];
                $model->start_date = $currentDate->format("Y/m/d H:i:s");
                $model->end_date = $currentEndDate->format("Y/m/d H:i:s");
                $model->meeting_date = $currentMeetingDate->format("Y/m/d H:i:s");
                $model->expiry_date = $currentExpiryDate->format("Y/m/d H:i:s");

                $model->responsibility = Yii::$app->request->post()['responsibility'];
                $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                $model->entered_by = Yii::$app->request->post()['entered_by'];
                $model->created_by = Yii::$app->request->post()['entered_by'];
                $model->verified_by = Yii::$app->request->post()['verified_by'];
                $model->agreed_by = Yii::$app->request->post()['agreed_by'];

                if(Yii::$app->request->post()['status'] == "completed")
                {

                    $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                    $formattedTargetDate = $dateTime->format('Y-m-d');

                    $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                    $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                    // print_r($formattedTargetDate);
                    // echo "<br>";
                    // print_r($formatteddateTimeToday);die;

                    if( $formatteddateTimeToday > $formattedTargetDate  )
                    {
                        $model->status = "overdue";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                    {
                        $model->status = "completed";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                    {
                        $model->status = "completed_ahead";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }
                }else{
                    $model->status = Yii::$app->request->post()['status'];
                }

                $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                $model->approved_by = Yii::$app->request->post()['approved_by'];
                $model->created_at = date("Y/m/d H:i:s");
                $model->save();


                $currentDate->modify('+7 days');
                $currentEndDate->modify('+7 days');
                $currentMeetingDate->modify('+7 days');
                $count++;
            }
            return $this->redirect(['index']);

        }elseif( isset(Yii::$app->request->post()['frequency']) && Yii::$app->request->post()['frequency'] == "bi-annual" ) {

            $frequencStart = new \DateTime(Yii::$app->request->post()['frequency_start']); // Replace with your actual start date
            $frequencyEnd = new \DateTime(Yii::$app->request->post()['frequency_end']);

            $currentDate = clone $frequencStart;


            // Iterate through each week and perform the expense
            $count=0;
            while ($currentDate <= $frequencyEnd) {

                if($count==0)
                {
                    $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
                    $MeetingDate = new \DateTime(Yii::$app->request->post()['meeting_date']);
                    $ExpiryDate = new \DateTime(Yii::$app->request->post()['expiry_date']);

                    $currentEndDate = clone $EndDate;
                    $currentMeetingDate = clone $MeetingDate;
                    $currentExpiryDate = clone $ExpiryDate;
                }


                $model = new ExpenseManager();
                $model->frequency = Yii::$app->request->post()['frequency'];
                // $model->department = $dept_info['title'];
                $model->department = Yii::$app->request->post()['department'];
                $model->module = Yii::$app->request->post()['module'];
                $model->type = Yii::$app->request->post()['type'];
                $model->modification = Yii::$app->request->post()['modification'];
                $model->priority = Yii::$app->request->post()['priority'];
                $model->description = Yii::$app->request->post()['description'];
                $model->start_date = $currentDate->format("Y/m/d H:i:s");
                $model->end_date = $currentEndDate->format("Y/m/d H:i:s");
                $model->meeting_date = $currentMeetingDate->format("Y/m/d H:i:s");
                $model->expiry_date = $currentExpiryDate->format("Y/m/d H:i:s");

                $model->responsibility = Yii::$app->request->post()['responsibility'];
                $model->assigned_to = Yii::$app->request->post()['assigned_to'];
                $model->entered_by = Yii::$app->request->post()['entered_by'];
                $model->created_by = Yii::$app->request->post()['entered_by'];
                $model->verified_by = Yii::$app->request->post()['verified_by'];
                $model->agreed_by = Yii::$app->request->post()['agreed_by'];

                if(Yii::$app->request->post()['status'] == "completed")
                {

                    $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
                    $formattedTargetDate = $dateTime->format('Y-m-d');

                    $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
                    $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

                    // print_r($formattedTargetDate);
                    // echo "<br>";
                    // print_r($formatteddateTimeToday);die;

                    if( $formatteddateTimeToday > $formattedTargetDate  )
                    {
                        $model->status = "overdue";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formatteddateTimeToday == $formattedTargetDate  )
                    {
                        $model->status = "completed";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }elseif( $formattedTargetDate > $formatteddateTimeToday  )
                    {
                        $model->status = "completed_ahead";
                        $model->completed_date = date("Y/m/d H:i:s");
                    }
                }else{
                    $model->status = Yii::$app->request->post()['status'];
                }

                $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                $model->approved_by = Yii::$app->request->post()['approved_by'];
                $model->created_at = date("Y/m/d H:i:s");
                $model->save();


                $currentDate->modify('+6 months');
                $currentEndDate->modify('+6 months');
                $currentMeetingDate->modify('+6 months');
                $count++;
            }
            return $this->redirect(['index']);

        }else{
            // dd(Yii::$app->request->post());
            if(!empty(Yii::$app->request->post()))
            {
                // dd(Yii::$app->request->post());
                // Check if Quotation 1
                if (isset($_FILES["image_1"]) && $_FILES["image_1"]["error"] == UPLOAD_ERR_OK) {
                    // Get file details
                    $fileTmpPath = $_FILES["image_1"]["tmp_name"];
                    $fileName = $_FILES["image_1"]["name"];
                    $fileSize = $_FILES["image_1"]["size"];
                    $fileType = $_FILES["image_1"]["type"];

                    // Specify the directory where you want to save the uploaded file
                    $uploadDir = 'uploads/quotations/';

                    // Create the directory if it doesn't exist
                    if (!file_exists($uploadDir)) {
                        mkdir($uploadDir, 0777, true);
                    }

                    // Move the uploaded file to the specified directory
                    $destPath_1 = $uploadDir . $fileName;
                    if (move_uploaded_file($fileTmpPath, $destPath_1)) {
                        Yii::$app->getSession()->addFlash('success', "Quotation # 1 Uploaded Successfully");
                    } else {
                        Yii::$app->getSession()->addFlash('error', "Unable to upload Quotation # 1");
                    }
                } else {
                    Yii::$app->getSession()->addFlash('error', "Quotation # 1 Not Selected");
                }

                // Check if Quotation 2
                if (isset($_FILES["image_2"]) && $_FILES["image_2"]["error"] == UPLOAD_ERR_OK) {
                    // Get file details
                    $fileTmpPath = $_FILES["image_2"]["tmp_name"];
                    $fileName = $_FILES["image_2"]["name"];
                    $fileSize = $_FILES["image_2"]["size"];
                    $fileType = $_FILES["image_2"]["type"];

                    // Specify the directory where you want to save the uploaded file
                    $uploadDir = 'uploads/quotations/';

                    // Create the directory if it doesn't exist
                    if (!file_exists($uploadDir)) {
                        mkdir($uploadDir, 0777, true);
                    }

                    // Move the uploaded file to the specified directory
                    $destPath_2 = $uploadDir . $fileName;
                    if (move_uploaded_file($fileTmpPath, $destPath_2)) {
                        Yii::$app->getSession()->addFlash('success', "Quotation # 2 Uploaded Successfully");
                    } else {
                        Yii::$app->getSession()->addFlash('error', "Unable to upload Quotation # 2");
                    }
                } else {
                    Yii::$app->getSession()->addFlash('error', "Quotation # 2 Not Selected");
                }

                // Check if Quotation 1
                if (isset($_FILES["image_3"]) && $_FILES["image_3"]["error"] == UPLOAD_ERR_OK) {
                    // Get file details
                    $fileTmpPath = $_FILES["image_3"]["tmp_name"];
                    $fileName = $_FILES["image_3"]["name"];
                    $fileSize = $_FILES["image_3"]["size"];
                    $fileType = $_FILES["image_3"]["type"];

                    // Specify the directory where you want to save the uploaded file
                    $uploadDir = 'uploads/quotations/';

                    // Create the directory if it doesn't exist
                    if (!file_exists($uploadDir)) {
                        mkdir($uploadDir, 0777, true);
                    }

                    // Move the uploaded file to the specified directory
                    $destPath_3 = $uploadDir . $fileName;
                    if (move_uploaded_file($fileTmpPath, $destPath_3)) {
                        Yii::$app->getSession()->addFlash('success', "Quotation # 3 Uploaded Successfully");
                    } else {
                        Yii::$app->getSession()->addFlash('error', "Unable to upload Quotation # 3");
                    }
                } else {
                    Yii::$app->getSession()->addFlash('error', "Quotation # 3 Not Selected");
                }

                $model = new ExpenseManager();
                $model->transaction_type = Yii::$app->request->post()['transaction_type'];
                $model->action = Yii::$app->request->post()['action'];
                $model->purpose = Yii::$app->request->post()['purpose'];

                if(isset(Yii::$app->request->post()['asset_id']))
                {
                    $model->asset_id = Yii::$app->request->post()['asset_id'];
                }elseif(isset(Yii::$app->request->post()['expense_id']))
                {
                    $model->asset_id = Yii::$app->request->post()['expense_id'];
                }

                $model->category_id = Yii::$app->request->post()['category_id'];
                $model->category_name = Yii::$app->request->post()['category_name'];
                $model->depriciation = Yii::$app->request->post()['depriciation'];
                $model->priority = Yii::$app->request->post()['priority'];
                $model->frequency = Yii::$app->request->post()['frequency'];
                $model->asset_location = Yii::$app->request->post()['asset_location'];
                $model->accounting_entry = Yii::$app->request->post()['accounting_entry'];
                $model->brand = Yii::$app->request->post()['brand'];
                $model->quantity = Yii::$app->request->post()['quantity'];
                $model->status = Yii::$app->request->post()['status'];
                $model->payment_mode = Yii::$app->request->post()['payment_mode'];
                $model->amount = Yii::$app->request->post()['amount'];
                $model->asset_life = Yii::$app->request->post()['asset_life'];

                if( Yii::$app->request->post()['accounting_entry'] == "new" && isset(Yii::$app->request->post()['vendor']) && Yii::$app->request->post()['payment_mode'] == "credit" )
                {
                    $model->pay_bank = Yii::$app->request->post()['pay_bank'];
                    $parts = explode(",", Yii::$app->request->post()['vendor']);

                    $model->vendor_id = $parts[0];
                    $model->vendor_name = $parts[1];
                }

                if( isset(Yii::$app->request->post()['accounting_entry']) && Yii::$app->request->post()['accounting_entry'] == "new" && Yii::$app->request->post()['payment_mode'] == "cash" )
                {
                    $model->pay_bank = Yii::$app->request->post()['pay_bank'];
                }

                $model->first_supplier = Yii::$app->request->post()['first_supplier'];
                $model->second_supplier = Yii::$app->request->post()['second_supplier'];
                $model->third_supplier = Yii::$app->request->post()['third_supplier'];
                $model->image_1 = $destPath_1;
                $model->image_2 = $destPath_2;
                $model->image_3 = $destPath_3;
                $model->applicant = Yii::$app->request->post()['applicant'];
                $model->department = Yii::$app->request->post()['department'];
                $model->approved_by = Yii::$app->request->post()['approved_by'];
                $model->approvers_dept = Yii::$app->request->post()['approvers_dept'];
                $model->asset_owner = Yii::$app->request->post()['asset_owner'];
                $model->entered_date = date("Y/m/d H:i:s");
                $model->approved_date = Yii::$app->request->post()['approved_date'];
                $model->purchasing_date = Yii::$app->request->post()['purchasing_date'];
                $model->target_date = Yii::$app->request->post()['target_date'];
                $model->collection_date = Yii::$app->request->post()['collection_date'];
                $model->meeting_date = Yii::$app->request->post()['meeting_date'];
                $model->start_date = Yii::$app->request->post()['start_date'];
                $model->expiry_date = Yii::$app->request->post()['expiry_date'];
                $model->frequency_start = Yii::$app->request->post()['frequency_start'];
                $model->frequency_end = Yii::$app->request->post()['frequency_end'];
                $model->created_by = Yii::$app->user->identity->id;
                $model->created_at = date("Y/m/d H:i:s");

                $model->save();
                return $this->redirect(['index']);
            }
        }

        return $this->render('create_service', [
            'model' => $model,
        ]);
    }

    public function actionUpdateService($id)
    {
        $StartDate = new \DateTime(Yii::$app->request->post()['start_date']);
        $formatStartDate = $StartDate->format('Y-m-d');

        $EndDate = new \DateTime(Yii::$app->request->post()['end_date']);
        $formatEndDate = $EndDate->format('Y-m-d');

        if($formatStartDate > $formatEndDate)
        {
            Yii::$app->getSession()->addFlash('error', "End Date Must be greater than Start Date!");
            $referrer = Yii::$app->request->referrer;
            return $this->redirect($referrer ? $referrer : ['index']);
        }

        $user_id = Yii::$app->user->identity->id;

        $user_info = (new \yii\db\Query())
            ->select('user_profile_info.department_id')
            ->from('user_profile_info')
            ->where(['user_profile_info.user_id' => $user_id])
            ->one();

        $dept_info = (new \yii\db\Query())
            ->select(['department.id','department.title'])
            ->from('department')
            ->where(['department.id' => $user_info['department_id']])
            ->one();

        $model = ExpenseManager::findOne(['id' => $id]);
        if(!empty(Yii::$app->request->post()))
        {

            if( Yii::$app->request->post()['action'] == "throw" )
            {
                Yii::$app->controller->actionDelete($model->id);
                return $this->redirect(['index']);
            }

            // dd(Yii::$app->request->post());
            // Check if Quotation 1
            if (isset($_FILES["image_1"]) && $_FILES["image_1"]["error"] == UPLOAD_ERR_OK) {
                // Get file details
                $fileTmpPath = $_FILES["image_1"]["tmp_name"];
                $fileName = $_FILES["image_1"]["name"];
                $fileSize = $_FILES["image_1"]["size"];
                $fileType = $_FILES["image_1"]["type"];

                // Specify the directory where you want to save the uploaded file
                $uploadDir = 'uploads/quotations/';

                // Create the directory if it doesn't exist
                if (!file_exists($uploadDir)) {
                    mkdir($uploadDir, 0777, true);
                }

                // Move the uploaded file to the specified directory
                $destPath_1 = $uploadDir . $fileName;
                if (move_uploaded_file($fileTmpPath, $destPath_1)) {
                    Yii::$app->getSession()->addFlash('success', "Quotation # 1 Uploaded Successfully");
                } else {
                    Yii::$app->getSession()->addFlash('error', "Unable to upload Quotation # 1");
                }
            } else {
                Yii::$app->getSession()->addFlash('error', "Quotation # 1 Not Selected");
            }

            // Check if Quotation 2
            if (isset($_FILES["image_2"]) && $_FILES["image_2"]["error"] == UPLOAD_ERR_OK) {
                // Get file details
                $fileTmpPath = $_FILES["image_2"]["tmp_name"];
                $fileName = $_FILES["image_2"]["name"];
                $fileSize = $_FILES["image_2"]["size"];
                $fileType = $_FILES["image_2"]["type"];

                // Specify the directory where you want to save the uploaded file
                $uploadDir = 'uploads/quotations/';

                // Create the directory if it doesn't exist
                if (!file_exists($uploadDir)) {
                    mkdir($uploadDir, 0777, true);
                }

                // Move the uploaded file to the specified directory
                $destPath_2 = $uploadDir . $fileName;
                if (move_uploaded_file($fileTmpPath, $destPath_2)) {
                    Yii::$app->getSession()->addFlash('success', "Quotation # 2 Uploaded Successfully");
                } else {
                    Yii::$app->getSession()->addFlash('error', "Unable to upload Quotation # 2");
                }
            } else {
                Yii::$app->getSession()->addFlash('error', "Quotation # 2 Not Selected");
            }

            // Check if Quotation 1
            if (isset($_FILES["image_3"]) && $_FILES["image_3"]["error"] == UPLOAD_ERR_OK) {
                // Get file details
                $fileTmpPath = $_FILES["image_3"]["tmp_name"];
                $fileName = $_FILES["image_3"]["name"];
                $fileSize = $_FILES["image_3"]["size"];
                $fileType = $_FILES["image_3"]["type"];

                // Specify the directory where you want to save the uploaded file
                $uploadDir = 'uploads/quotations/';

                // Create the directory if it doesn't exist
                if (!file_exists($uploadDir)) {
                    mkdir($uploadDir, 0777, true);
                }

                // Move the uploaded file to the specified directory
                $destPath_3 = $uploadDir . $fileName;
                if (move_uploaded_file($fileTmpPath, $destPath_3)) {
                    Yii::$app->getSession()->addFlash('success', "Quotation # 3 Uploaded Successfully");
                } else {
                    Yii::$app->getSession()->addFlash('error', "Unable to upload Quotation # 3");
                }
            } else {
                Yii::$app->getSession()->addFlash('error', "Quotation # 3 Not Selected");
            }

            $model->transaction_type = Yii::$app->request->post()['transaction_type'];
            $model->action = Yii::$app->request->post()['action'];
            $model->purpose = Yii::$app->request->post()['purpose'];

            if(isset(Yii::$app->request->post()['asset_id']))
            {
                $model->asset_id = Yii::$app->request->post()['asset_id'];
            }elseif(isset(Yii::$app->request->post()['expense_id']))
            {
                $model->asset_id = Yii::$app->request->post()['expense_id'];
            }

            $model->category_id = Yii::$app->request->post()['category_id'];
            $model->category_name = Yii::$app->request->post()['category_name'];
            $model->depriciation = Yii::$app->request->post()['depriciation'];
            $model->priority = Yii::$app->request->post()['priority'];
            $model->frequency = Yii::$app->request->post()['frequency'];
            $model->asset_location = Yii::$app->request->post()['asset_location'];
            $model->accounting_entry = Yii::$app->request->post()['accounting_entry'];
            $model->brand = Yii::$app->request->post()['brand'];
            $model->quantity = Yii::$app->request->post()['quantity'];
            $model->status = Yii::$app->request->post()['status'];
            $model->payment_mode = Yii::$app->request->post()['payment_mode'];
            $model->amount = Yii::$app->request->post()['amount'];
            $model->asset_life = Yii::$app->request->post()['asset_life'];

            if( Yii::$app->request->post()['payment_mode'] == "credit" && Yii::$app->request->post()['accounting_entry'] == "new" && isset(Yii::$app->request->post()['payment_mode']) )
            {
                $model->pay_bank = Yii::$app->request->post()['pay_bank'];
                $parts = explode(",", Yii::$app->request->post()['vendor']);

                $model->vendor_id = $parts[0];
                $model->vendor_name = $parts[1];
            }

            if( Yii::$app->request->post()['payment_mode'] == "cash" && isset(Yii::$app->request->post()['payment_mode']) && Yii::$app->request->post()['accounting_entry'] == "new" )
            {
                $model->pay_bank = Yii::$app->request->post()['pay_bank'];
            }

            $model->first_supplier = Yii::$app->request->post()['first_supplier'];
            $model->second_supplier = Yii::$app->request->post()['second_supplier'];
            $model->third_supplier = Yii::$app->request->post()['third_supplier'];
            if( isset($destPath_1) )
            {
                $model->image_1 = $destPath_1;
            }
            if( isset($destPath_2) )
            {
                $model->image_2 = $destPath_2;
            }
            if( isset($destPath_3) )
            {
                $model->image_3 = $destPath_3;
            }
            $model->applicant = Yii::$app->request->post()['applicant'];
            $model->department = Yii::$app->request->post()['department'];
            $model->approved_by = Yii::$app->request->post()['approved_by'];
            $model->approvers_dept = Yii::$app->request->post()['approvers_dept'];
            $model->asset_owner = Yii::$app->request->post()['asset_owner'];
            $model->entered_date = date("Y/m/d H:i:s");
            $model->approved_date = Yii::$app->request->post()['approved_date'];
            $model->purchasing_date = Yii::$app->request->post()['purchasing_date'];
            $model->target_date = Yii::$app->request->post()['target_date'];
            $model->collection_date = Yii::$app->request->post()['collection_date'];
            $model->meeting_date = Yii::$app->request->post()['meeting_date'];
            $model->start_date = Yii::$app->request->post()['start_date'];
            $model->expiry_date = Yii::$app->request->post()['expiry_date'];
            $model->frequency_start = Yii::$app->request->post()['frequency_start'];
            $model->frequency_end = Yii::$app->request->post()['frequency_end'];
            $model->updated_by = Yii::$app->user->identity->id;
            $model->updated_at = date("Y/m/d H:i:s");

            // if(Yii::$app->request->post()['status'] == "completed")
            // {

            //     $dateTime = new \DateTime(Yii::$app->request->post()['end_date']);
            //     $formattedTargetDate = $dateTime->format('Y-m-d');

            //     $dateTimeToday = new \DateTime(date("Y/m/d H:i:s"));
            //     $formatteddateTimeToday = $dateTimeToday->format('Y-m-d');

            //     // print_r($formattedTargetDate);
            //     // echo "<br>";
            //     // print_r($formatteddateTimeToday);die;

            //     if( $formatteddateTimeToday > $formattedTargetDate  )
            //     {
            //         $model->status = "overdue";
            //         $model->completed_date = date("Y/m/d H:i:s");
            //     }elseif( $formatteddateTimeToday == $formattedTargetDate  )
            //     {
            //         $model->status = "completed";
            //         $model->completed_date = date("Y/m/d H:i:s");
            //     }elseif( $formattedTargetDate > $formatteddateTimeToday  )
            //     {
            //         $model->status = "completed_ahead";
            //         $model->completed_date = date("Y/m/d H:i:s");
            //     }
            // }else{
            //     $model->status = Yii::$app->request->post()['status'];
            // }
            $model->save();
            return $this->redirect(['index']);
        }


        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionCreateOt()
    {
        // Yii::$app->getSession()->addFlash('error', "End Date Must be greater than Start Date!");
        // $referrer = Yii::$app->request->referrer;
        // return $this->redirect($referrer ? $referrer : ['index']);

        $user_id = Yii::$app->user->identity->id;

        $user_info = (new \yii\db\Query())
            ->select(['user_profile_info.department_id' , 'user_profile_info.basic_salary' ])
            ->from('user_profile_info')
            ->where(['user_profile_info.user_id' => $user_id])
            ->one();

        $dept_info = (new \yii\db\Query())
            ->select(['department.id','department.title'])
            ->from('department')
            ->where(['department.id' => $user_info['department_id']])
            ->one();


        // dd(Yii::$app->request->post());
        if(!empty(Yii::$app->request->post()))
        {

            $model = new ExpenseManager();
            $model->purpose = Yii::$app->request->post()['purpose'];
            $model->start_date = Yii::$app->request->post()['start_date'];

            $dateObj = new \DateTime(Yii::$app->request->post()['start_date']);
            $dayOfWeek = $dateObj->format('l');

            if( $dayOfWeek == "Saturday" || $dayOfWeek == "Sunday" )
            {
                $salary = $user_info['basic_salary']*12;
                $salary = number_format($salary/365,2);
                $salary = number_format($salary/9,2);
                $salary = Yii::$app->request->post()['total_time']*$salary;
                $model->val_amount = number_format($salary*1.5,2);
            }else{
                $salary = $user_info['basic_salary']*12;
                $salary = number_format($salary/365,2);
                $salary = number_format($salary/9,2);
                $salary = Yii::$app->request->post()['total_time']*$salary;
                $model->val_amount = number_format($salary*1.25,2);
            }

            $model->val_amount = number_format($model->val_amount,2);

            $model->end_date = Yii::$app->request->post()['end_date'];
            $model->start_time = Yii::$app->request->post()['start_time'];
            $model->end_time = Yii::$app->request->post()['end_time'];
            $model->total_time = Yii::$app->request->post()['total_time'];
            $model->department = $dept_info['title'];
            $model->applicant = Yii::$app->request->post()['applicant'];
            $model->status = Yii::$app->request->post()['status'];
            $model->entered_date = date("Y/m/d H:i:s");
            $model->save();
            return $this->redirect(['index-ot']);
        }

        return $this->render('create_ot', [
            'model' => $model,
        ]);
    }

    public function actionUpdateOt($id)
    {
        // $user_id = Yii::$app->user->identity->id;
        $model = ExpenseManager::findOne(['id' => $id]);
        $user_id = $model->applicant;
        $user_info = (new \yii\db\Query())
            ->select(['user_profile_info.department_id' , 'user_profile_info.basic_salary' ])
            ->from('user_profile_info')
            ->where(['user_profile_info.user_id' => $user_id])
            ->one();

        $dept_info = (new \yii\db\Query())
            ->select(['department.id','department.title'])
            ->from('department')
            ->where(['department.id' => $user_info['department_id']])
            ->one();

        if(!empty(Yii::$app->request->post()))
        {
            $model->purpose = Yii::$app->request->post()['purpose'];
            $model->start_date = Yii::$app->request->post()['start_date'];

            $dateObj = new \DateTime(Yii::$app->request->post()['start_date']);
            $dayOfWeek = $dateObj->format('l');

            if( $dayOfWeek == "Saturday" || $dayOfWeek == "Sunday" )
            {
                $salary = $user_info['basic_salary']*12;
                $salary = number_format($salary/365,2);
                $salary = number_format($salary/9,2);
                $salary = Yii::$app->request->post()['total_time']*$salary;
                $model->val_amount = number_format($salary*1.5,2);
            }else{
                $salary = $user_info['basic_salary']*12;
                $salary = number_format($salary/365,2);
                $salary = number_format($salary/9,2);
                $salary = Yii::$app->request->post()['total_time']*$salary;
                $model->val_amount = number_format($salary*1.25,2);
            }

            $model->val_amount = number_format($model->val_amount,2);

            $model->end_date = Yii::$app->request->post()['end_date'];
            $model->start_time = Yii::$app->request->post()['start_time'];
            $model->end_time = Yii::$app->request->post()['end_time'];
            $model->total_time = Yii::$app->request->post()['total_time'];
            $model->department = $dept_info['title'];
            $model->applicant = Yii::$app->request->post()['applicant'];
            $model->status = Yii::$app->request->post()['status'];
            $model->entered_date = date("Y/m/d H:i:s");
            $model->save();
            return $this->redirect(['index-ot']);
        }

        return $this->render('update_ot', [
            'model' => $model,
        ]);
    }

    public function actionDeleteOt($id)
    {
        $model = ExpenseManager::findOne(['id' => $id])->delete();
        return $this->redirect(['index-ot']);
    }

    public function actionIndexOt()
    {
        $searchModel = new ExpenseManagerSearch();
        $dataProvider = $searchModel->searchOt(Yii::$app->request->queryParams);

        $totalTime = $dataProvider->query->sum('total_time');
        $totalAmt = $dataProvider->query->sum('val_amount');

        return $this->render('index_ot', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'totalTime' => $totalTime,
            'totalAmt' => $totalAmt,
        ]);
    }

}
