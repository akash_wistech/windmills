<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\ChangePasswordForm;
use app\models\User;
use app\models\ProfileForm;

class AccountController extends DefController
{
  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['update-profile','change-password'],
        'rules' => [
          [
            'actions' => ['update-profile','change-password'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
    ];
  }

  /**
   * Change Password action.
   *
   * @return Response|string
   */
  public function actionChangePassword()
  {
    $this->checkLogin();
    $model = new ChangePasswordForm();
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Password changed successfully'));
        return $this->redirect(['site/index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('change_password', [
      'model' => $model,
    ]);
  }

  /**
  * Displays homepage.
  *
  * @return string
  */
  public function actionUpdateProfile()
  {
    $this->checkLogin();
    $model = new ProfileForm;
    $oldEmail=Yii::$app->user->identity->email;
    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Profile updated successfully'));
        return $this->redirect(['site/index']);
      }else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
								Yii::$app->getSession()->addFlash('error', $val);
							}
						}
					}
				}
      }
    }
    return $this->render('profile',['model'=>$model]);
  }
}
