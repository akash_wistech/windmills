<?php

namespace app\controllers;

use Yii;
use app\models\VoneValuations;
use app\models\VoneValuationsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\helpers\DefController;

/**
 * VoneValuationsController implements the CRUD actions for VoneValuations model.
 */
class VoneValuationsController extends DefController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all VoneValuations models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VoneValuationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionIndexMistake()
    {
        $grid_view_id = 'grid_mistake';
        $searchModel = new VoneValuationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $grid_view_id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => 'Process Mistake Identified',
        ]);
    }
    public function actionIndexContested()
    {
        $grid_view_id = 'grid_contested';
        $searchModel = new VoneValuationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $grid_view_id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => 'Process Valuation Contested by Client',
        ]);
    }
    public function actionIndexReminder()
    {
        $grid_view_id = 'grid_reminder';
        $searchModel = new VoneValuationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $grid_view_id);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => 'Process Update  Reminder by Client',
        ]);
    }

    /**
     * Displays a single VoneValuations model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new VoneValuations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VoneValuations();

        if ($model->load(Yii::$app->request->post())){

            $contested = [1, 6, 2, 7,5];
            $mistake = [3];
            $reminder = [8];

            $template_id = '';
            if (in_array($model->reason, $contested)) {
                $model->grid_view_id = 'grid_contested'; //Process  Valuation Contested by Client
                $redirect_to = 'index-contested';
                $template_id = 'v1.contested';
            }
            if (in_array($model->reason, $mistake)) {
                $model->grid_view_id = 'grid_mistake'; //Process Mistake Identified
                $redirect_to = 'index-mistake';
                $template_id = 'v1.error.mistake';
            }
            if (in_array($model->reason, $reminder)) {
                $model->grid_view_id = 'grid_reminder'; //Reminder By Client
                $redirect_to = 'index-reminder';
                $template_id = 'v1.client.reminder';
            }

        
                $this->StatusVerify($model);
                if($model->save()){
                    $this->makeHistory([
                        'model' => $model, 
                        'model_name' => get_class($model),
                        'action' => 'data_created',
                        'verify_field' => 'status_verified',
                    ]);

                    $approver_ids = [21]; //laxmi kumar user id
                    $cc_user_ids = [14,142,6319, 38];
                    $notifyData = [
                        'approver' => $approver_ids,
                        'cc_user_ids' => $cc_user_ids,
                        'attachments' => [],
                        'replacements'=>[
                            '{client_reference}' => $model->client_reference,
                            '{wm_reference}' => $model->wm_reference,
                        ],
                    ];
                    //\app\modules\wisnotify\listners\NotifyEvent::fireErrorEmail($template_id, $notifyData);

                    Yii::$app->db->createCommand()
                    ->update(VoneValuations::tableName(), [ 'email_status' => 1 ], [ 'id' => $model->id ])
                    ->execute();
                    Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
                    return $this->redirect([$redirect_to]);
                }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing VoneValuations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_verify_status = $model->status_verified;
        
        if ($model->load(Yii::$app->request->post())){
            // dd(Yii::$app->request->post());
            $this->StatusVerify($model);
            if($model->save()){
                    
                $this->makeHistory([
                    'model' => $model, 
                    'model_name' => get_class($model),
                    'action' => 'data_updated',
                    'verify_field' => 'status_verified',
                    'old_verify_status' => $old_verify_status,
                ]);

                if($model->grid_view_id=='grid_contested'){
                    $redirect_to = 'index-contested';
                }
                if($model->grid_view_id=='grid_mistake'){
                    $redirect_to = 'index-mistake';
                }
                if($model->grid_view_id=='grid_reminder'){
                    $redirect_to = 'index-reminder';
                }
                return $this->redirect([$redirect_to]);


            }
        }
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing VoneValuations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the VoneValuations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VoneValuations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VoneValuations::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
