<?php

namespace app\controllers;

use Yii;
use app\models\PropertyfinderListData;
use app\models\PropertyfinderListDataSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PropertyfinderListDataController implements the CRUD actions for PropertyfinderListData model.
 */
class PropertyfinderListDataController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PropertyfinderListData models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PropertyfinderListDataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PropertyfinderListData model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PropertyfinderListData model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PropertyfinderListData();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PropertyfinderListData model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PropertyfinderListData model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PropertyfinderListData model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PropertyfinderListData the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PropertyfinderListData::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    
    public function actionDetail($city_id, $purpose)
    {
        // require __DIR__ . '/../components/helpers/simple_html_dom.php';
        $randomString = Yii::$app->propertyFinderHelperFunctions->generateRandomString();
        $data = [];
        $query = \app\models\PropertyfinderByCityDltUrl::find()->where(['status'=>0, 'city_id'=>$city_id, 'purpose'=>$purpose])->asArray()->limit(15)->all();
        if ($query<>null) {
            foreach ($query as $query) {
                // Yii::$app->db->createCommand()->update('propertyfinder_by_city_dlt_url', ['status' => 2], [ 'url'=>$query['url'], 'city_id' => $query['city_id'], 'purpose'=>$purpose ])->execute();
                
                // $url = 'https://usama-com.stackstaging.com/maxima_latest/crawl-helper/index?url='.$query['url'].'';
                // echo $url; die;
                try {
                    //1st try
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $query['url']);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $response = curl_exec($ch);
                    // echo"<pre>"; print_r($response); echo"</pre>"; die;

                    // file_put_contents('uploads/pf-files/file-'.$randomString.'.txt', $response);
                    // die("here");
                    
                    $data['url'] = $query['url'];
                    
                    $tel_Position = strpos($response, 'tel:');
                    if ($tel_Position<>null) {
                        $nextChar = substr($response, $tel_Position, 17);
                    }
                    $val = preg_replace('/[^0-9--.]/', '', $nextChar);
                    
                    $data['agent_telephone'] = $val;
                    $data['agent_telephone'] = trim("+".$val);
                    
                    $data['agent_title'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.agent_title');
                    $data['agent_name'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.agent_name');
                    $data['agent_location'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.broker_location');
                    $data['agent_company'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.broker_name');
                    
                    $data['property_name'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_name');
                    $data['property_type'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_type');
                    $data['bathrooms'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_bathrooms');
                    $data['bedrooms'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_bedrooms');
                    $data['property_category'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_sub_category');
                    $data['property_completion'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_completion');
                    $data['listed'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_listed_days');
                    $data['property_listing_depth'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_listing_depth');
                    $data['city'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_location_city');
                    $data['community'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_location_community');
                    $data['sub_community'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_location_sub_community');
                    $data['building_info'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_location_tower');
                    $data['listings_price'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_price');
                    $data['reference'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_reference');
                    $data['property_size'] = Yii::$app->propertyFinderHelperFunctions->PropertyDetail($response , 'window.tealium.property_size_sqft');
                    
                    //date formatting
                    $data['listed'] = str_replace(" ", "-", $data['listed']);
                    $data['listed'] = date("Y-m-d", strtotime($data['listed']));
                    
                    //reference number formatting
                    $data['reference'] = str_replace(" ", "-", $data['reference']);
                    
                    //building info changes
                    if ($data['building_info']=='' AND $data['building_info']==null) {
                        $data['building_info'] = $data['sub_community'];
                    };
                    $data['description'] = ''; //strtolower(trim($response));
                    
                    echo "<pre>"; print_r($data); echo "</pre>"; die();
                    $this->getDecision($data);
                    
                    curl_close($ch);
                }
                catch ( Exception $err ) {
                    //1st catch
                    $error = $err->getMessage();
                    echo "1st Catch cc<br>";
                    print_r($error);
                }       
            }
        }
    }
}
