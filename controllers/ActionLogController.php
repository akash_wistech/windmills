<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\ActionLog;
use app\models\ActionLogSubject;
use app\models\ALCommentForm;
use app\models\ALActionForm;
use app\models\ALCallForm;
use app\models\ALTimerForm;
use app\models\ALCalendarEventForm;
use app\models\AttachmentForm;

class ActionLogController extends DefController
{
  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['save'],
        'rules' => [
          [
            'actions' => ['save'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
    ];
  }

  /**
  * Save log.
  *
  * @return Response|string
  */
  public function actionSave($ltype,$mtype,$mid)
  {
    $this->checkLogin();
    if($ltype=='comment')$model = new ALCommentForm();
    if($ltype=='action')$model = new ALActionForm();
    if($ltype=='call')$model = new ALCallForm();
    if($ltype=='timer')$model = new ALTimerForm();
    if($ltype=='calendar')$model = new ALCalendarEventForm();
    $model->module_type=$mtype;
    $model->module_id=$mid;
    $model->rec_type=$ltype;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        $msg['success']=['heading'=>Yii::t('app','Saved'),'msg'=>Yii::t('app','Saved successfully')];
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>$val];
              }
            }
          }
        }
      }
    }
    echo json_encode($msg);
    die();
    exit;
  }

  /**
  * Creates a new Calendar Event model.
  * If creation is successful, the browser will reload the calendar
  * @return mixed
  */
  public function actionCreateEvent()
  {
    $this->checkSuperAdmin();
    $model = new ALCalendarEventForm();

    $request = Yii::$app->request;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Event saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }
    return $this->renderAjax('calendar_event', [
      'model' => $model,
    ]);
  }

  /**
   * Updates an existing model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionUpdate($id)
  {
    $this->checkLogin();
    $modelLog = $this->findModel($id);
    if($modelLog->rec_type=='comment')$model = new ALCommentForm();
    if($modelLog->rec_type=='action')$model = new ALActionForm();
    if($modelLog->rec_type=='call')$model = new ALCallForm();
    if($modelLog->rec_type=='timer')$model = new ALTimerForm();
    if($modelLog->rec_type=='calendar')$model = new ALCalendarEventForm();
    $model->id=$modelLog->id;
    $model->module_type=$modelLog->module_type;
    $model->module_id=$modelLog->module_id;
    $model->rec_type=$modelLog->rec_type;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        $msg['success']=['heading'=>Yii::t('app','Success'),'msg'=>Yii::t('app','Information saved successfully')];
      }else{
				if($model->hasErrors()){
					foreach($model->getErrors() as $error){
						if(count($error)>0){
							foreach($error as $key=>$val){
                $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>$val];
							}
						}
					}
				}
      }
      echo json_encode($msg);
      die();
      exit;
    }

    return $this->renderAjax('update', [
        'modelLog' => $modelLog,
        'model' => $model,
    ]);
  }

  /**
  * Mark an action as completed
  *
  * @return Response|json
  */
  public function actionCompleted($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);
    $connection = \Yii::$app->db;
    $connection->createCommand("update ".ActionLogSubject::tableName()." set status=:status,completed_at=:completed_at where action_log_id=:action_log_id",[
      ':status' => 1, ':completed_at' => date("Y-m-d H:i:s"),':action_log_id' => $model->id,
    ])
    ->execute();
    $msg['success']=['heading'=>Yii::t('app','Success'),'msg'=>Yii::t('app','Action completed successfully')];
    echo json_encode($msg);
    die();
    exit;
  }

  /**
  * Mark an action as completed
  *
  * @return Response|json
  */
  public function actionDelete($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);
    $model->Softdelete();
    $msg['success']=['heading'=>Yii::t('app','Success'),'msg'=>Yii::t('app','Action deleted successfully')];
    echo json_encode($msg);
    die();
    exit;
  }

  /**
  * Save Attachments
  *
  * @return Response|json
  */
  public function actionSaveAttachment($type,$mid)
  {
    $this->checkLogin();
    $modelALAttachment = new AttachmentForm;
    $modelALAttachment->rec_type='attachment';
    $modelALAttachment->module_type=$type;
    $modelALAttachment->module_id=$mid;
    if ($modelALAttachment->load(Yii::$app->request->post())) {
      //$modelALAttachment->attachment_file=$fileName;
      if($modelALAttachment->save()){
        $msg['success']=['heading'=>Yii::t('app','Success'),'msg'=>Yii::t('app','Attachments saved successfully')];
      }else{
        if($modelALAttachment->hasErrors()){
          foreach($modelALAttachment->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>$val];
              }
            }
          }
        }
      }
      echo json_encode($msg);
      die();
      exit;
    }
  }

  /**
  * Finds the AdminGroup model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return AdminGroup the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = ActionLog::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException(Yii::t('app','The requested page does not exist.'));
    }
  }
}
