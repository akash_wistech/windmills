<?php

namespace app\controllers;

use app\models\AutoLinksSold;
use app\models\Buildings;
use app\models\Company;
use app\models\SoldData;
use app\models\Valuation;
use Yii;
use app\models\SoldTransactionAd;
use app\models\SoldTransactionAdSearch;
use app\models\SoldTransactionAdImportForm;
use app\components\helpers\DefController;
use app\models\SoldTransactionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Symfony\Component\DomCrawler\Crawler;
use yii\db\Expression;

/**
 * SoldTransactionAdController implements the CRUD actions for SoldTransactionAd model.
 */
class SoldTransactionAdController extends DefController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        // Check if the user is logged in
        if (Yii::$app->user->isGuest) {
            // Redirect to the login page
            return Yii::$app->response->redirect(['site/login'])->send();
        }
    }

    /**
     * Lists all SoldTransactionAd models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SoldTransactionSearch();
        $searchModel->sold_db_id = 1;

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSharjahLists()
    {
        $this->checkLogin();
        $all_data = \app\models\SharjahListings::find()->orderBy([
            'id' => SORT_DESC,
        ])->all();


        return $this->render('sharjah_list', [
            'all_data' => $all_data,
        ]);
    }

    public function actionSoldNotSaved()
    {
        $this->checkLogin();
        $all_data = \app\models\SoldDataUpgrade::find()
            ->where(['inserted'=>0])
            ->orderBy([
            'id' => SORT_DESC,
        ])->all();

      /*  echo "<pre>";
        print_r($all_data);
        die;*/
        return $this->render('sold_not_saved_list', [
            'all_data' => $all_data,
            'title_view' => 'Sold Transactions Not Saved',
        ]);
    }

    public function actionSoldNotSavedImport()
    {
        $this->checkLogin();
        $all_data = \app\models\SoldDataUpgrade::find()
            ->where(['inserted'=>0])
            ->orderBy([
                'id' => SORT_DESC,
            ])->all();


        foreach ($all_data as $record){
            $buildingRow = Buildings::find()
                ->where(['=', 'title', trim($record->building_name)])
                ->orWhere(['=', 'reidin_title', trim($record->building_name)])
                ->asArray()->one();


            if ($buildingRow != null) {


                $comparison = SoldTransactionAd::find()
                    ->where(['building_info' => $buildingRow['id'],
                        'no_of_bedrooms' => $record->no_of_rooms,
                        'built_up_area' => $record->bua,
                        //'unit_number' => $unit_number,
                        'land_size' => $record->plot_area,
                        'transaction_date' => $record->sold_date,
                        'listings_price' => $record->price,
                        'price_per_sqt' => $record->price_sqf,
                        // 'reidin_ref_number' => $reidin_ref_number,
                    ])->one();

                if ($comparison == null) {
                    // echo "Record Found"; die();
                    $buildingId = $buildingRow['id'];




                    $model = new SoldTransactionAd;
                    $model->scenario = 'import';
                    $model->building_info = $buildingId;
                    if($record->no_of_rooms != 'Unknown' && $record->no_of_rooms != 'PENTHOUSE' && $record->no_of_rooms <> null){
                        $model->no_of_bedrooms = $record->no_of_rooms;
                    }
                    //  $model->no_of_bedrooms = ($rooms <> null && ($rooms != 'Unknown')) ? $rooms : 0;
                    $model->built_up_area = ($record->bua <> null && $record->bua !== '-') ? $record->bua : 0;
                    $model->land_size = ($record->plot_area <> null && $record->plot_area !== '-') ? $record->plot_area : 0;;
                    $model->unit_number = ($record->unit_number <> null && $record->unit_number !== '-') ? $record->unit_number : '';
                    $model->floor_number = ($record->floor_number <> null && $record->floor_number !== '-') ? $record->floor_number : '';
                    $model->balcony_size = ($record->balcony_size <> null && $record->balcony_size !== '-') ? $record->balcony_size : '';
                    $model->parking_space_number = ($record->parking_space_number <> null && $record->parking_space_number !== '-') ? $record->parking_space_number : '';
                    $model->transaction_type = ($record->transaction_type <> null) ? $record->transaction_type : '';
                    $model->sub_type = ($record->sub_type <> null && $record->sub_type !== '-') ? $record->sub_type : '';
                    $model->sales_sequence = ($record->sales_sequence <> null && $record->sales_sequence !== '-') ? $record->sales_sequence : '';
                    $model->reidin_ref_number = ($record->reidin_ref_number <> null && $record->reidin_ref_number !== '-') ? $record->reidin_ref_number : '';
                    $model->reidin_community = ($record->reidin_community <> null && $record->reidin_community !== '-') ? $record->reidin_community : '';
                    $model->reidin_property_type = ($record->reidin_property_type <> null && $record->reidin_property_type !== '-') ? $record->reidin_property_type : '';
                    $model->reidin_developer = ($record->reidin_developer <> null && $record->reidin_developer !== '-') ? $record->reidin_developer : '';

                    $model->transaction_date = $record->sold_date;
                    $model->listings_price = ($record->price <> null && $record->price !== '-') ? $record->price : 0;

                    $model->property_category = $buildingRow['property_category'];
                    $model->location = $buildingRow['location'];
                    $model->tenure = $buildingRow['tenure'];
                    $model->utilities_connected = $buildingRow['utilities_connected'];
                    $model->development_type = $buildingRow['development_type'];
                    $model->property_placement = $buildingRow['property_placement'];
                    $model->property_visibility = $buildingRow['property_visibility'];
                    $model->property_exposure = $buildingRow['property_exposure'];
                    $model->property_condition = $buildingRow['property_condition'];
                    $model->pool = $buildingRow['pool'];
                    $model->gym = $buildingRow['gym'];
                    $model->play_area = $buildingRow['play_area'];
                    // $model->other_facilities = $buildingRow['other_facilities'];
                    $model->landscaping = $buildingRow['landscaping'];
                    $model->white_goods = $buildingRow['white_goods'];
                    $model->furnished = $buildingRow['furnished'];
                    $model->finished_status = $buildingRow['finished_status'];


                    $model->price_per_sqt = ($record->price_sqf <> null && $record->price_sqf !== '#VALUE!') ? $record->price_sqf : 0;;

                    /*  echo "<pre>";
                      print_r($model);
                      die;*/
                    if ($model->save()) {
                        Yii::$app->db->createCommand()->update('sold_data_upgrade', ['inserted' => 1], ['id' => $record->id])->execute();

                    } else {
                        if ($model->hasErrors()) {
                            foreach ($model->getErrors() as $error) {
                                if (count($error) > 0) {
                                    foreach ($error as $key => $val) {
                                        echo $val.'<br>';

                                    }
                                }
                            }
                        }
                        die();
                        $errNames .= '<br />' . $buildingName;
                        $unsaved++;
                    }


                }else{
                    Yii::$app->db->createCommand()->update('sold_data_upgrade', ['inserted' => 2], ['id' => $record->id])->execute();
                }


            }else{
                Yii::$app->db->createCommand()->update('sold_data_upgrade', ['inserted' => 3], ['id' => $record->id])->execute();
            }

        }

        die('here');




        echo "<pre>";
        print_r($all_data);
        die;


        return $this->render('sold_not_saved_list', [
            'all_data' => $all_data,
            'title_view' => 'Sold Transactions Not Saved',
        ]);
    }


    public function actionSoldNotSavedDuplicates()
    {
        $this->checkLogin();
        $all_data = \app\models\SoldDataUpgrade::find()
           // ->where(['data_type'=>1])
            ->where(['inserted'=>0])
            ->orderBy([
                'id' => SORT_DESC,
            ])->all();


        return $this->render('sold_not_saved_list', [
            'all_data' => $all_data,
            'title_view' => 'Sold Transactions Duplicates',
        ]);
    }


    public function actionSoldAd()
    {
        $this->checkLogin();
        $all_data = \app\models\SoldAd::find()
            ->where(['status'=>0])
            ->orderBy([
                'id' => SORT_DESC,
            ])->all();


        return $this->render('sold_ad_list', [
            'all_data' => $all_data,
            'title_view' => 'Sold Transactions Abu Dabhi',
        ]);
    }


    /**
     * Displays a single SoldTransactionAd model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SoldTransactionAd model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SoldTransactionAd();
        $model->scenario='create';

        if ($model->load(Yii::$app->request->post())) {
            $model->status = $model->status_verified;
            $this->StatusVerify($model);
            if($model->save()){
                $this->makeHistory([
                    'model' => $model, 
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                    'action' => 'data_created',
                    'verify_field' => 'status',
                ]);
                Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
                return $this->redirect(['index']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    public function findbuilding($query)
    {

        $length = strlen($query['building_info']); //echo $length."<br>";
        for ($i = 1; $i <= $length; $i++) {

            $remove_char = mb_substr($query['building_info'], '-'.$i);
            $trim_building_info  =  rtrim($query['building_info'], $remove_char);

            $building = \app\models\Buildings::find()
                ->where(new Expression(' title LIKE "%'.$trim_building_info.'%" '))
                ->andWhere(['city' => $query['city_id']])
                ->asArray()
                ->one();

            if($building<>null){
                return $building;
                break;
            }
        }

    }

    public function actionScrapeTableData()
    {

      /*  for($i=1;$i<115276;$i++){
            $record = new AutoLinksSold();
            $record->link= 'https://www.bayut.com/property-market-analysis?page='.$i;
            $record->save();
        }
        die('here');*/

       // https://www.bayut.com/property-market-analysis?page=

/*        $clients = Company::find()->where(['client_type'=> 'bank','status'=>1])->all();
        // $client = Company::find()->where(['client_type'=> 'bank','id'=> 9166])->one();

        $month =  date('m') - 1;
        $year = date('Y');
        if($month == 0){
            $month= 1;
        }
        foreach ($clients as $client) {
            $valuations_month_number = Valuation::find()->where(['client_id' => $client->id])
                ->andWhere('valuation_status=5')
                ->andFilterWhere([
                    'between', 'submission_approver_date', '2023-05-01 00:00:00','2023-05-31 23:59:59'
                ])
                ->all();

            $valuations_year_number = Valuation::find()->where(['client_id' => $client->id])

                ->andWhere('valuation_status=5')
                ->andFilterWhere([
                    'between', 'submission_approver_date', '2023-01-01 00:00:00','2023-05-31 23:59:59'
                ])
                ->all();

            $last_month = date('F', strtotime('last month'));
            $date_value = $last_month . ' ' . date('Y');
            $subject = 'Valuation Instructions to Windmills in ' . $date_value;
            echo "<br><br><br>". $client->title.'<br>';
            echo  count($valuations_year_number)."<br>";
            echo  count($valuations_year_number)."<br>";
            echo  count($valuations_month_number)."<br><br>";

        }

        die;*/




        $all_links = AutoLinksSold::find()->where(['status'=>0])->all();

        $saved=0;
        $unsaved=0;
        $errNames='';
        $change=array();
        $curl = curl_init();

            foreach ($all_links as $key => $link_data){
                $query = Yii::$app->db->createCommand()
                    ->update('auto_links_sold', ['status' => 1], 'id = ' . $link_data->id . '')
                    ->execute();
                $url = $link_data->link;
               

            curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://api.scrapingant.com/v2/general?url='.$url.'%2F&x-api-key=fc0416bae97a47e4a51af30e212232d1',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
            ));

            $response = curl_exec($curl);

            curl_close($curl);


            // Load the HTML content of the page
            $html = $response;

            $crawler = new Crawler($html);
            $title = $crawler->filter('table')->text();
            $table = $crawler->filter('table')->first();
            $rows = $table->filter('tr')->each(function (Crawler $row, $i) {
                return $row->filter('td')->each(function (Crawler $cell, $j) {
                    return $cell->text();
                });
            });
            $result = array_values(array_filter($rows));

            if (!empty($result)) {
                foreach ($result as $key => $line) {
                    $buildingName = $line[0];
                    $type = $line[1];
                    $rooms = $line[2];
                    $bua_raw = str_replace(" sqft", "", $line[3]);
                    $rooms = str_replace(" B/R", "", $rooms);
                    $bua = str_replace(",", "", $bua_raw);
                    $plotArea = str_replace(",", "", $line[4]);
                    $unit_number = str_replace(",", "", $line[5]);

                    $date = str_replace(' ', '-', trim($line[6]));
                    // echo $date; die();
                    $date = date("Y-m-d", strtotime($date));
                    $price_raw = str_replace("AED", "", $line[7]);
                    $price = str_replace(",", "", $price_raw);
                    $price = str_replace(",", "", $price);


                    if ($bua <> null) {
                        $pricePerSqt = number_format(floatval($price) / floatval($bua));
                    } else {
                        $pricePerSqt = 0;
                    }
                    /*  echo "<pre>";
                                    // print_r($price_raw);
                                     print_r($price);
                                  //   print_r($line);
                                     die;*/
                    $query['building_info'] = $buildingName;
                    $query['city_id'] = 3510;
                    $building_find = \app\models\Buildings::find()
                        ->where(new Expression(' title LIKE "%' . $query['building_info'] . '%" '))
                        ->andWhere(['city' => $query['city_id']])
                        ->asArray()
                        ->one();

                    if ($building_find <> null) {
                        $buildingRow = $building_find;
                    } else {
                        $buildingRow = $this->findbuilding($query);
                    }
                    if ($buildingRow == null) {
                        $buildingRow = $this->findbuilding($query);
                    }


                    $buildingRow = Buildings::find()->where(['=', 'title', trim($buildingName)])->asArray()->one();

                    if ($buildingRow <> null && $buildingRow['id'] <> null) {

                        $comparison = SoldTransactionAd::find()
                            ->where(['building_info' => $buildingRow['id'],
                                'no_of_bedrooms' => $rooms,
                                'built_up_area' => $bua,
                                'land_size' => $plotArea,
                                'transaction_date' => $date,
                                'listings_price' => $price,
                                'price_per_sqt' => $pricePerSqt])
                            // ->asArray()
                            ->one();

                        if ($comparison == null) {
                            // echo "Record Found"; die();
                            $buildingId = $buildingRow['id'];


                            $model = new SoldTransactionAd;
                            $model->scenario = 'import';
                            $model->building_info = $buildingId;
                            if($rooms != 'Unknown' && $rooms != 'PENTHOUSE' && $rooms <> null){
                                $model->no_of_bedrooms = $rooms;
                            }

                            $model->built_up_area = ($bua <> null && $bua !== '-') ? $bua : 0;
                            $model->land_size = ($plotArea <> null && $plotArea !== '-') ? floatval($plotArea) : 0;;
                            //$model->type=$type;
                            $model->transaction_date = $date;
                            $model->listings_price = ($price <> null && $price !== '-') ? floatval($price) : 0;

                            $model->property_category = $buildingRow['property_category'];
                            $model->location = $buildingRow['location'];
                            $model->tenure = $buildingRow['tenure'];
                            $model->utilities_connected = $buildingRow['utilities_connected'];
                            $model->development_type = $buildingRow['development_type'];
                            $model->property_placement = $buildingRow['property_placement'];
                            $model->property_visibility = $buildingRow['property_visibility'];
                            $model->property_exposure = $buildingRow['property_exposure'];
                            $model->property_condition = $buildingRow['property_condition'];
                            $model->pool = $buildingRow['pool'];
                            $model->gym = $buildingRow['gym'];
                            $model->play_area = $buildingRow['play_area'];
                            // $model->other_facilities = $buildingRow['other_facilities'];
                            $model->landscaping = $buildingRow['landscaping'];
                            $model->white_goods = $buildingRow['white_goods'];
                            $model->furnished = $buildingRow['furnished'];
                            $model->finished_status = $buildingRow['finished_status'];


                            $model->price_per_sqt = floatval($pricePerSqt);
                            $model->list_type = 1;
                            if ($model->save()) {
                                $saved++;
                            } else {
                                if ($model->hasErrors()) {
                                    foreach ($model->getErrors() as $error) {
                                        if (count($error) > 0) {
                                            foreach ($error as $key => $val) {
                                                echo $val;
                                            }
                                        }
                                    }
                                }

                                $errNames .= '<br />' . $buildingName;
                                $unsaved++;
                            }


                        }


                    } else {

                        $soldmax = new SoldData();
                        $soldmax->building_title = $buildingName;
                        $soldmax->property_type = $type;
                        $soldmax->bedrooms = $rooms;
                        $soldmax->bua = $bua;
                        $soldmax->plot_area = $plotArea;
                        $soldmax->unit_number = $unit_number;
                        $soldmax->listing_date = $date;
                        $soldmax->price = $price;
                        $soldmax->created_at = date('Y-m-d h:i:s');
                        $soldmax->save();


                        $change[] = $buildingName;
                    }


                }
                $query = Yii::$app->db->createCommand()
                    ->update('auto_links_sold', ['status' => 1], 'id = ' . $link_data->id . '')
                    ->execute();

            }
        }

    }



    /**
     * Import for SoldTransactionAd.
     * If import is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionImport()
    {
      //  die('Suspended');
        $model = new SoldTransactionAdImportForm();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->getSession()->setFlash('success', Yii::t('app','Data imported successfully'));
                return $this->redirect(['index']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('import', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SoldTransactionAd model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        echo "<pre>";
        print_r($model);
        die;
        $old_verify_status = $model->status;
        $model->status_verified = $model->status;

        if ($model->load(Yii::$app->request->post())) {
            $model->status = $model->status_verified;
            $this->StatusVerify($model);
            if($model->save()){
                $this->makeHistory([
                    'model' => $model, 
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                    'action' => 'data_updated',
                    'verify_field' => 'status',
                    'old_verify_status' => $old_verify_status,
                ]);
                Yii::$app->getSession()->addFlash('success', Yii::t('app','Information updated successfully'));
                return $this->redirect(['index']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SoldTransactionAd model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    public function actionDeleteDuplicate($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Information deleted successfully'));
        return $this->redirect(['reports/sold-duplicates']);
    }

    public function actionUpdateDuplicates($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->getSession()->addFlash('success', Yii::t('app','Information updated successfully'));
                return $this->redirect(['reports/sold-duplicates']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the SoldTransactionAd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SoldTransactionAd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SoldTransactionAd::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
