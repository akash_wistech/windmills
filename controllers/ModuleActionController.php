<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\ModuleEmail;
use app\models\ModuleNumber;
use app\models\ModuleAddress;
use app\models\ModuleCompany;

class ModuleActionController extends DefController
{
  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['delete-address'],
        'rules' => [
          [
            'actions' => ['delete-address'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
    ];
  }

  /**
   * Deletes an existing module email model.
   * @param string $mt
   * @param integer $mid
   * @param string $email
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionDeleteEmail($mt,$mid,$email)
  {
    $this->checkLogin();
    $model=ModuleEmail::find()->where(['email'=>$email,'module_type'=>$mt,'module_id'=>$mid])->one();
    if($model!=null){
      $model->delete();
      $msg['success']=['heading'=>Yii::t('app','Deleted'),'msg'=>Yii::t('app','Email deleted successfully')];
      echo json_encode($msg);
      die();
      exit;
    }else{
      throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
  }

  /**
   * Deletes an existing module number model.
   * @param string $mt
   * @param integer $mid
   * @param string $email
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionDeleteNumber($mt,$mid,$number)
  {
    $this->checkLogin();
    $model=ModuleNumber::find()->where(['phone'=>$number,'module_type'=>$mt,'module_id'=>$mid])->one();
    if($model!=null){
      $model->delete();
      $msg['success']=['heading'=>Yii::t('app','Deleted'),'msg'=>Yii::t('app','Number deleted successfully')];
      echo json_encode($msg);
      die();
      exit;
    }else{
      throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
  }

  /**
   * Deletes an existing module company model.
   * @param string $mt
   * @param integer $mid
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionDeleteCompany($mt,$mid,$id)
  {
    $this->checkLogin();
    $model=ModuleCompany::find()->where(['id'=>$id,'module_type'=>$mt,'module_id'=>$mid])->one();
    if($model!=null){
      $model->delete();
      $msg['success']=['heading'=>Yii::t('app','Deleted'),'msg'=>Yii::t('app','Company deleted successfully')];
      echo json_encode($msg);
      die();
      exit;
    }else{
      throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
  }

  /**
   * Deletes an existing module address model.
   * @param string $mt
   * @param integer $mid
   * @param integer $id
   * @return mixed
   * @throws NotFoundHttpException if the model cannot be found
   */
  public function actionDeleteAddress($mt,$mid,$id)
  {
    $this->checkLogin();
    $model=ModuleAddress::find()->where(['id'=>$id,'module_type'=>$mt,'module_id'=>$mid])->one();
    if($model!=null){
      $model->softDelete();
      $msg['success']=['heading'=>Yii::t('app','Deleted'),'msg'=>Yii::t('app','Address deleted successfully')];
      echo json_encode($msg);
      die();
      exit;
    }else{
      throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
  }
}
