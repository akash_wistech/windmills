<?php

namespace app\controllers;

use Yii;
use app\models\PredefinedList;
use app\models\PredefinedListSearch;
use app\models\PredefinedListCountryDetail;
use app\models\PredefinedListZoneDetail;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * PredefinedListController implements the CRUD actions for PredefinedList model.
 */
class PredefinedListController extends DefController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PredefinedList models.
     * @return mixed
     */
    public function actionIndex()
    {
      $this->checkAdmin();
      $searchModel = new PredefinedListSearch();
      $searchModel->parent=0;
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

      return $this->render('index', [
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
      ]);
    }

    /**
     * Creates a new PredefinedList model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
      $this->checkAdmin();
        $model = new PredefinedList();
        $model->parent=0;
        $model->status=1;

        if ($model->load(Yii::$app->request->post())) {
          if($model->save()){
            if(Yii::$app->request->isAjax){
              $msg['success']=['heading'=>Yii::t('app','Saved'),'msg'=>Yii::t('app','Option saved successfully')];
              echo json_encode($msg);
              exit;
            }else{
              Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
              return $this->redirect(['update','id'=>$model->id]);
            }
          }else{
            if($model->hasErrors()){
              foreach($model->getErrors() as $error){
                if(count($error)>0){
                  foreach($error as $key=>$val){
                    if(Yii::$app->request->isAjax){
                      $msg=[];
                      $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>$val];
                      echo json_encode($msg);
                      exit;
                    }else{
                      Yii::$app->getSession()->addFlash('error', $val);
                    }
                  }
                }
              }
            }
          }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
      $this->checkAdmin();
      $model = $this->findModel($id);

      if ($model->load(Yii::$app->request->post())) {
        if($model->save()){
          Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
          return $this->redirect(['index']);
        }else{
  				if($model->hasErrors()){
  					foreach($model->getErrors() as $error){
  						if(count($error)>0){
  							foreach($error as $key=>$val){
  								Yii::$app->getSession()->addFlash('error', $val);
  							}
  						}
  					}
  				}
        }
      }

      $subModel = new PredefinedList;
      $subModel->parent=$model->id;
      $subModel->status=1;

      $searchModel = new PredefinedListSearch();
      $searchModel->parent = $model->id;
      $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

      return $this->render('update', [
          'model' => $model,
          'subModel' => $subModel,
          'searchModel' => $searchModel,
          'dataProvider' => $dataProvider,
      ]);
    }

    public function actionUpdateSubOptions($id,$c,$v)
    {
      $this->checkAdmin();

      $set_country_id=Yii::$app->appHelperFunctions->getSetting('country_list_id');
      $set_zone_id=Yii::$app->appHelperFunctions->getSetting('zone_list_id');
      $connection = \Yii::$app->db;

      $model = $this->findModel($id);
      if($c==1)$model->title=$v;
      if($c==2)$model->other_name=$v;
      if($c==3)$model->status=$v;
      //Country
      if($model->parent==$set_country_id && $c>3){
        if($c==4)$colName='iso_code_2';
        if($c==5)$colName='iso_code_3';
        if($c==6)$colName='phone_code';
        $connection->createCommand("update ".PredefinedListCountryDetail::tableName()." set ".$colName."=:colVal where id=:id",
        [
          ':colVal' => $v,
          ':id' => $model->id,
        ])
        ->execute();
      }
      //State/Zone
      if($model->parent==$set_zone_id && $c>7){
        if($c==7)$colName='country_id';
        if($c==8)$colName='code';
        $connection->createCommand("update ".PredefinedListZoneDetail::tableName()." set ".$colName."=:colVal where id=:id",
        [
          ':colVal' => $v,
          ':id' => $model->id,
        ])
        ->execute();
      }
      if(!$model->save()){
        print_r($model->getErrors());
        die();
      }
      echo "done";
      exit;
    }

    /**
     * Deletes an existing model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
      $this->checkAdmin();
      $this->findModel($id)->softDelete();
      if(Yii::$app->request->isAjax){
        echo "done";
        exit;
      }else{
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
        return $this->redirect(Yii::$app->request->referrer ?: ['index']);
      }
    }

    /**
     * Finds the PredefinedList model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PredefinedList the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PredefinedList::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
