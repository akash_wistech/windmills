<?php

namespace app\controllers;

use Yii;
use app\models\UaeListData;
use app\models\UaeListDataSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;
use app\models\BuildingForSave;

class UaeListDataController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        // Check if the user is logged in
        if (Yii::$app->user->isGuest) {
            // Redirect to the login page
            return Yii::$app->response->redirect(['site/login'])->send();
        }
    }
    
    public function actionIndex()
    {
        // die("here");
        $searchModel = new UaeListDataSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionCreate()
    {
        $model = new UaeListData();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        
        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        
        return $this->render('update', [
            'model' => $model,
        ]);
    }
    
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        
        return $this->redirect(['index']);
    }
    
    protected function findModel($id)
    {
        if (($model = UaeListData::findOne($id)) !== null) {
            return $model;
        }
        
        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionMigrateData()
    {
        
        $query = UaeListData::find()
        ->where(['move_to_listing'=>0])
        // ->limit(500)
        ->asArray()
        ->all();
        // echo"<pre>"; print_r($query); echo"</pre>"; die;
        
        if($query<>null AND count($query)>0) {
            if (Yii::$app->request->post('total_count')<>null && Yii::$app->request->post('total_count')>0) {
                $total_count = Yii::$app->request->post('total_count');
                // echo "if ". $total_count; //die;
            }else{
                $total_count = count($query);
                // echo "else ". $total_count; //die;
            }
            if (Yii::$app->request->post('saved_listings')<>null && Yii::$app->request->post('saved_listings')>0) {
                // echo Yii::$app->request->post('saved_listings'); die();
                $saved_listings = Yii::$app->request->post('saved_listings');
            }else{
                $saved_listings = 0;
            }
            
            
            
            $i = 1;
            foreach ($query as $key => $query) {
                // echo"<pre>"; print_r($query); echo"</pre>"; die;
                $model = \app\models\ListingsTransactions::find()->where(['listings_reference'=>$query['listings_reference']])->one();
                if($model==null){
                    $building = \app\models\Buildings::find()
                    ->where(['city' => $query['city_id']])
                    ->andWhere(new Expression(' title LIKE "%'.$query['building_info'].'%" '))
                    ->one();
                    // echo"<pre>"; print_r($building); echo"</pre>"; die();
                    
                    
                    if($building<>null && $building->id<>null) {
                        $model= new \app\models\ListingsTransactions;
                        $model->listings_reference   = $query['listings_reference'];
                        $model->source               = $query['source'];
                        $model->listing_website_link = $query['listing_website_link'];
                        $model->listing_date         = $query['listing_date'];
                        $model->building_info        = $building->id;
                        $model->no_of_bedrooms       = $query['no_of_bedrooms'];
                        $model->built_up_area        = $query['built_up_area'];
                        $model->land_size            = $query['land_size'];
                        $model->listings_price       = $query['listings_price'];
                        $model->listings_rent        = $query['listings_rent'];
                        $model->final_price          = $query['final_price'];
                        $model->status               = 2;
                        
                        $model->property_category    = 1; //static
                        $model->tenure               = 1; //static
                        $model->unit_number          = 'Not Known'; //static
                        if($model->save()) {
                            Yii::$app->db->createCommand()->update('uae_list_data', [ 'move_to_listing' => 1 ], [ 'id'=>$query['id'] ])->execute();
                            // die("saved");
                        }
                        if($model->hasErrors()){
                            foreach($model->getErrors() as $error){
                                if(count($error)>0){
                                    foreach($error as $key=>$val){
                                        echo $val. "<br>";
                                    }   
                                }
                            }
                            die();
                        }
                    }else{
                        Yii::$app->db->createCommand()->update('uae_list_data', [ 'move_to_listing' => 2 ], [ 'id'=>$query['id'] ])->execute();
                        $queryCheck = BuildingForSave::find()->where(['building_name'=>$query['building_info'], 'city'=>$query['city_id']])->one();
                        if($queryCheck==null){
                            $saveModel = new BuildingForSave;
                            $saveModel->building_name = $query['building_info'];
                            $saveModel->city 		  = $query['city_id'];
                            $saveModel->save();
                            
                            if($saveModel->hasErrors()){
                                foreach($saveModel->getErrors() as $error){
                                    if(count($error)>0){
                                        foreach($error as $key=>$val){
                                            echo $val. "<br>";
                                        }
                                    }
                                }
                                die();
                            }
                        }
                    }
                    
                    
                    
                }else{
                    Yii::$app->db->createCommand()->update('uae_list_data', [ 'move_to_listing' => 1 ], [ 'id'=>$query['id'] ])->execute();
                }
                
                
                if($i >= 300) {
                    // die("here");
                    // echo " total saved ".$i+$saved_listings; //die;
                    // echo " total count ".$total_count; //die;
                    
                    $val = $i+$saved_listings;
                    $val2 = $val/$total_count;
                    $progress_value = $val2*100;
                    // echo " progress_value ".$progress_value; 
                    // die;
                    
                    $dataArr = [
                        "msg"             => "still_saving",
                        "progress"        => $progress_value,
                        "total_count"     => $total_count,
                        "saved_listings"  => $i + $saved_listings,
                    ];
                    echo json_encode($dataArr); die;
                }
                $i++;
            }              
            
        }
        
        $dataArr = [
            "msg"  => "completed_successfully",
            "progress"  => 100,
        ];
        echo json_encode($dataArr); die;
    }
}
