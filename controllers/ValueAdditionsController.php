<?php

namespace app\controllers;

use Yii;
use app\models\ValueAdditions;
use app\models\ValueAdditionsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\helpers\DefController;

/**
 * GeneralAsumptionController implements the CRUD actions for GeneralAsumption model.
 */
class ValueAdditionsController extends DefController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all GeneralAsumption models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ValueAdditionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionMsecretary($t)
    {

        $title= Yii::$app->appHelperFunctions->polisciesCategory[$t];

        return $this->render('policy_view', [
            'model' => $this->findModel($t),
            't' => $t,
            'title' => $title
        ]);
    }

    public function actionMmanagment($t)
    {

        $title= Yii::$app->appHelperFunctions->polisciesCategory[$t];

        return $this->render('policy_view', [
            'model' => $this->findModel($t),
            't' => $t,
            'title' => $title
        ]);
    }

    public function actionBsale($t)
    {

        $title= Yii::$app->appHelperFunctions->polisciesCategory[$t];

        return $this->render('policy_view', [
            'model' => $this->findModel($t),
            't' => $t,
            'title' => $title
        ]);
    }
    public function actionBmarketing($t)
    {

        $title= Yii::$app->appHelperFunctions->polisciesCategory[$t];

        return $this->render('policy_view', [
            'model' => $this->findModel($t),
            't' => $t,
            'title' => $title
        ]);
    }
    public function actionBproposal($t)
    {

        $title= Yii::$app->appHelperFunctions->polisciesCategory[$t];

        return $this->render('policy_view', [
            'model' => $this->findModel($t),
            't' => $t,
            'title' => $title
        ]);
    }
    public function actionBempanelment($t)
    {

        $title= Yii::$app->appHelperFunctions->polisciesCategory[$t];

        return $this->render('policy_view', [
            'model' => $this->findModel($t),
            't' => $t,
            'title' => $title
        ]);
    }

    public function actionOfinance($t)
    {

        $title= Yii::$app->appHelperFunctions->polisciesCategory[$t];

        return $this->render('policy_view', [
            'model' => $this->findModel($t),
            't' => $t,
            'title' => $title
        ]);
    }

    public function actionOhr($t)
    {

        $title= Yii::$app->appHelperFunctions->polisciesCategory[$t];

        return $this->render('policy_view', [
            'model' => $this->findModel($t),
            't' => $t,
            'title' => $title
        ]);
    }

    public function actionOit($t)
    {

        $title= Yii::$app->appHelperFunctions->polisciesCategory[$t];

        return $this->render('policy_view', [
            'model' => $this->findModel($t),
            't' => $t,
            'title' => $title
        ]);
    }
    public function actionOlegal($t)
    {

        $title= Yii::$app->appHelperFunctions->polisciesCategory[$t];

        return $this->render('policy_view', [
            'model' => $this->findModel($t),
            't' => $t,
            'title' => $title
        ]);
    }
    public function actionOadmin($t)
    {

        $title= Yii::$app->appHelperFunctions->polisciesCategory[$t];

        return $this->render('policy_view', [
            'model' => $this->findModel($t),
            't' => $t,
            'title' => $title
        ]);
    }
    public function actionSrev($t)
    {

        $title= Yii::$app->appHelperFunctions->polisciesCategory[$t];

        return $this->render('policy_view', [
            'model' => $this->findModel($t),
            't' => $t,
            'title' => $title
        ]);
    }
    public function actionSmev($t)
    {

        $title= Yii::$app->appHelperFunctions->polisciesCategory[$t];

        return $this->render('policy_view', [
            'model' => $this->findModel($t),
            't' => $t,
            'title' => $title
        ]);
    }
    public function actionSrfs($t)
    {

        $title= Yii::$app->appHelperFunctions->polisciesCategory[$t];

        return $this->render('policy_view', [
            'model' => $this->findModel($t),
            't' => $t,
            'title' => $title
        ]);
    }
    public function actionSbcs($t)
    {

        $title= Yii::$app->appHelperFunctions->polisciesCategory[$t];

        return $this->render('policy_view', [
            'model' => $this->findModel($t),
            't' => $t,
            'title' => $title
        ]);
    }

    /**
     * Displays a single GeneralAsumption model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new GeneralAsumption model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ValueAdditions();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->status_verified = $model->status;
            $this->StatusVerify($model);
            if($model->save()) {
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                    'action' => 'data_created',
                    'verify_field' => 'status',
                ]);
                Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));

                return $this->redirect(['site/index']);
            }
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing GeneralAsumption model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_verify_status = $model->status;

        if ($model->load(Yii::$app->request->post())) {

            $model->status_verified = $model->status;
            $this->StatusVerify($model);
            if($model->save()){

                $this->makeHistory([
                    'model' => $model,
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                    'action' => 'data_updated',
                    'verify_field' => 'status',
                    'old_verify_status' => $old_verify_status,
                ]);


                Yii::$app->getSession()->addFlash('success', Yii::t('app','Information updated successfully'));
                if(Yii::$app->appHelperFunctions->polisciesCategoryslug[$id] <> null){
                    return $this->redirect(['ValueAdditions/'.Yii::$app->appHelperFunctions->polisciesCategoryslug[$id].'?t='.$id]);
                }

                return $this->redirect(['index']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing GeneralAsumption model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the GeneralAsumption model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GeneralAsumption the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ValueAdditions::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
