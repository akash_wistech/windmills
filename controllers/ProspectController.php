<?php

namespace app\controllers;

use Yii;
use app\models\Prospect;
use app\models\ProspectSearch;
use app\models\Company;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* ProspectController implements the CRUD actions for Prospect model.
*/
class ProspectController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all Prospect models.
  * @return mixed
  */
  public function actionIndex()
  {
    $this->checkAdmin();
      if(!in_array( Yii::$app->user->identity->id, [1,14])) {
          Yii::$app->getSession()->addFlash('error', "Permission denied!");
          return $this->redirect(['site/index']);
      }
    $searchModel = new ProspectSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new Prospect model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkSuperAdmin();
      if(!in_array( Yii::$app->user->identity->id, [1,14])) {
          Yii::$app->getSession()->addFlash('error', "Permission denied!");
          return $this->redirect(['site/index']);
      }
    $model = new Prospect();

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Prospect saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }
    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
  * Updates an existing Prospect model.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionUpdate($id)
  {
    $this->checkLogin();
      if(!in_array( Yii::$app->user->identity->id, [1,14])) {
          Yii::$app->getSession()->addFlash('error', "Permission denied!");
          return $this->redirect(['site/index']);
      }
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Prospect saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('update', [
      'model' => $model,
    ]);
  }

  /**
  * Converts a prospect to client
  */
  public function actionConvert($id)
  {
    $prospect = $this->findModel($id);

    $model = new Company();
    $model->prospect_id = $prospect->id;
    $model->title = $prospect->company_name;
    $model->firstname = $prospect->firstname;
    $model->lastname = $prospect->lastname;
    $model->email = $prospect->email;
    $model->mobile = $prospect->mobile;
    $model->status=1;
    $model->start_date=date("Y-m-d");

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        $msg['success']=['heading'=>Yii::t('app','Saved'),'msg'=>Yii::t('app','Converted successfully')];
      }else{
        if($model->hasErrors()){
            foreach($model->getErrors() as $error){
                if(count($error)>0){
                    foreach($error as $key=>$val){
                        $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>$val];
                    }
                }
            }
        }
      }
      echo json_encode($msg);
      die();
      exit;
    }

    return $this->renderAjax('/client/_form', [
      'model' => $model,
    ]);
  }

  /**
  * Creates a new model.
  */
  protected function newModel()
  {
    $model = new Prospect;
    return $model;
  }

  /**
  * Finds the Prospect model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Prospect the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = Prospect::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
