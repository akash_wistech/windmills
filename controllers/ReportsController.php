<?php

namespace app\controllers;

use app\models\Company;
use app\models\InspectProperty;
use app\models\PropertiesSearch;
use app\models\ScheduleInspection;
use app\models\User;
use app\models\ValuationDetail;
use app\models\ValuationApproversData;
use app\models\ValuationReportsSearch;
use DateTime;
use Yii;

use app\models\Valuation;
use app\models\StaffSearch;
use app\models\ValuationSearch;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Properties;

class ReportsController extends DefController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        // Check if the user is logged in
        if (Yii::$app->user->isGuest) {
            // Redirect to the login page
            return Yii::$app->response->redirect(['site/login'])->send();
        }
    }
    /**
     * Displays Setting Page.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->checkLogin();
        die();
        $searchModel = new ValuationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('status-report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionStatusReport()
    {
        $this->checkLogin();
        $searchModel = new ValuationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('status-report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionStatusReportSign()
    {
        $this->checkLogin();
        $searchModel = new ValuationSearch();
        $searchModel->include_signature=true;
        $dataProvider = $searchModel->search_signature(Yii::$app->request->queryParams);
        return $this->render('status-report-signature', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionStatusReportNotSign()
    {
        $this->checkLogin();
        $searchModel = new ValuationSearch();
        $searchModel->include_signature=false;
        $searchModel->valuation_status=5;
        $dataProvider = $searchModel->search_signature(Yii::$app->request->queryParams);
        return $this->render('status-report-not-signature', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }



    public function actionValuationStatus()
    {
        return $this->render('valuation-status');
    }

    public function actionClientRevenueAll()
    {
        $this->checkLogin();
        // if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019,6319])) {
        //     Yii::$app->getSession()->addFlash('error', "Permission denied!");
        //     return $this->redirect(['site/index']);
        // }
        $holidays = array();
        $user=null;
        $type='All';
        $custom = 0;
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_revenue_all(Yii::$app->request->queryParams);
        // $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType();
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];

                    $Date=(explode(" - ",$custome_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];

                    $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);
                }
            }else{
                $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
            }


            /*            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                        $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                        $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                        $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);*/
        }else{
            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(0);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);
        }

        /*if (!empty($allvalues)){
            echo "<pre>";
            print_r($allvalues);
            die;
        }*/



        return $this->render('client_revenue_all', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'custom' => $custom,
            'allvalues' => $allvalues['total_values'],
            'allvalues_count' => $allvalues['total_valuations_count'],
            'revevnue_all_types' => $revevnue_all_types,
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,

        ]);
    }

    public function actionClientRevenueAllnew()
    {


        // if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
        //     Yii::$app->getSession()->addFlash('error', "Permission denied!");
        //     return $this->redirect(['site/index']);
        // }

        $holidays = array();
        $user=null;
        $type='All';
        $heading_a='All Time';
        $heading_b='All Time';
        $custom = 0;
        $bank_val_difference = 0;
        $indi_val_difference = 0;
        $corp_val_difference = 0;

        $bank_rev_difference = 0;
        $indi_rev_difference = 0;
        $corp_rev_difference = 0;



        $searchModel = new ValuationReportsSearch();
        $params = Yii::$app->request->queryParams;
        $params['ValuationReportsSearch']['time_period'] =0;
        $dataProvider = $searchModel->search_revenue_all_a(Yii::$app->request->queryParams);

        $dataProvider_all = $searchModel->search_revenue_all($params);
        $dataProvider_a_data = $searchModel->search_revenue_all_a_data(Yii::$app->request->queryParams);
        $dataProvider_b_data = $searchModel->search_revenue_all_b_data(Yii::$app->request->queryParams);
        $bank_val_a = 0;
        $bank_rev_a = 0;
        $corp_val_a = 0;
        $corp_rev_a = 0;
        $ind_val_a = 0;
        $ind_rev_a = 0;
        $bank_val_b = 0;
        $bank_rev_b = 0;
        $corp_val_b = 0;
        $corp_rev_b = 0;
        $ind_val_b = 0;
        $ind_rev_b = 0;
        foreach ($dataProvider_a_data as $a_datum) {
            if (isset($a_datum->client_type) && $a_datum->client_type == 'bank') {
                $bank_val_a = $a_datum->total_valuations;
                $bank_rev_a = $a_datum->client_revenue;
            }
            if (isset($a_datum->client_type) && $a_datum->client_type == 'corporate') {
                $corp_val_a = $a_datum->total_valuations;
                $corp_rev_a = $a_datum->client_revenue;
            }
            if (isset($a_datum->client_type) && $a_datum->client_type == 'individual') {
                $ind_val_a = $a_datum->total_valuations;
                $ind_rev_a = $a_datum->client_revenue;
            }
        }

        foreach ($dataProvider_b_data as $b_datum) {
            if (isset($b_datum->client_type) && $b_datum->client_type == 'bank') {
                $bank_val_b = $b_datum->total_valuations;
                $bank_rev_b = $b_datum->client_revenue;
            }
            if (isset($b_datum->client_type) && $b_datum->client_type == 'corporate') {
                $corp_val_b = $b_datum->total_valuations;
                $corp_rev_b = $b_datum->client_revenue;
            }
            if (isset($b_datum->client_type) && $b_datum->client_type == 'individual') {
                $ind_val_b = $b_datum->total_valuations;
                $ind_rev_b = $b_datum->client_revenue;
            }
        }

        if($bank_val_b !=0){

            $bank_val_difference = number_format(((($bank_val_a -  $bank_val_b)/ $bank_val_b) * 100),2);
        }
        if($bank_rev_b !=0){

            $bank_rev_difference = number_format(((($bank_rev_a -  $bank_rev_b)/ $bank_rev_b) * 100),2);
        }

        if($corp_val_b !=0){

            $corp_val_difference = number_format(((($corp_val_a -  $corp_val_b)/ $corp_val_b) * 100),2);
        }
        if($corp_rev_b !=0){

            $corp_rev_difference = number_format(((($corp_rev_a -  $corp_rev_b)/ $corp_rev_b) * 100),2);
        }

        if($ind_val_b !=0){

            $indi_val_difference = number_format(((($ind_val_a -  $ind_val_b)/ $ind_val_b) * 100),2);
        }
        if($ind_rev_b !=0){

            $corp_rev_difference = number_format(((($ind_rev_a -  $ind_rev_b)/ $ind_rev_b) * 100),2);
        }


        // $searchModel_b = new ValuationReportsSearch();
        $dataProvider_b = $searchModel->search_revenue_all_b(Yii::$app->request->queryParams);
        // $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType();
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 4){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];

                    $Date=(explode(" - ",$custome_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];

                    $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(9,$custome_date_btw);
                    $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(9,$custome_date_btw);
                    $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);

                    $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal($date_filter_a);
                    $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType($date_filter_a);
                    //   $date_array = Yii::$app->appHelperFunctions->getFilterDates($date_filter_a);
                    $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);

                    $allvalues_b = Yii::$app->appHelperFunctions->getRevenueTotal($date_filter_b);
                    $revevnue_all_types_b = Yii::$app->appHelperFunctions->getRevenueTotalByType($date_filter_b);
                    //   $date_array_b = Yii::$app->appHelperFunctions->getFilterDates($date_filter_b);
                    $total_working_days_b = Yii::$app->appHelperFunctions->getWorkingDays($date_array_b['start_date'],$date_array_b['end_date'],$holidays);


                }
            }else{
                if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 1){
                    $date_filter_a= 2;
                    $date_filter_b= 5;
                    $heading_a='This Month';
                    $heading_b='Last Month';
                }else if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 2){
                    $date_filter_a= 3;
                    $date_filter_b= 6;
                    $heading_a='This Quarter';
                    $heading_b='Last Quarter';
                }else if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 3){
                    $date_filter_a= 4;
                    $date_filter_b= 7;
                    $heading_a='This Year';
                    $heading_b='Last Year';
                }else{
                    $date_filter_a= 0;
                    $date_filter_b= 0;
                }




                $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal($date_filter_a);
                $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType($date_filter_a);
                $date_array = Yii::$app->appHelperFunctions->getFilterDates($date_filter_a);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);

                $allvalues_b = Yii::$app->appHelperFunctions->getRevenueTotal($date_filter_b);
                $revevnue_all_types_b = Yii::$app->appHelperFunctions->getRevenueTotalByType($date_filter_b);
                $date_array_b = Yii::$app->appHelperFunctions->getFilterDates($date_filter_b);
                $total_working_days_b = Yii::$app->appHelperFunctions->getWorkingDays($date_array_b['start_date'],$date_array_b['end_date'],$holidays);

            }


            /*            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                        $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                        $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                        $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);*/
        }else{
            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(0);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);

            $allvalues_b = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $revevnue_all_types_b = Yii::$app->appHelperFunctions->getRevenueTotalByType(0);
            $total_working_days_b = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);
        }

        /*if (!empty($allvalues)){
            echo "<pre>";
            print_r($allvalues);
            die;
        }*/



        return $this->render('client_revenue_allnew', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProvider_b' => $dataProvider_b,
            'dataProvider_all' => $dataProvider_all,
            'user' => $user,
            'type' => $type,
            'custom' => $custom,
            'allvalues' => $allvalues['total_values'],
            'allvalues_count' => $allvalues['total_valuations_count'],
            'revevnue_all_types' => $revevnue_all_types,
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,

            'allvalues_b' => $allvalues_b['total_values'],
            'allvalues_count_b' => $allvalues_b['total_valuations_count'],
            'revevnue_all_types_b' => $revevnue_all_types_b,
            'total_rows_b' => (!empty($dataProvider_b->getTotalCount()) && $dataProvider_b->getTotalCount() > 0) ? $dataProvider_b->getTotalCount(): 1 ,
            'total_working_days_b' => $total_working_days_b,
            'heading_a' => $heading_a,
            'heading_b' => $heading_b,
            'bank_val_difference' => $bank_val_difference,
            'indi_val_difference' => $indi_val_difference,
            'corp_val_difference' => $corp_val_difference,
            'bank_rev_difference' => $bank_rev_difference,
            'indi_rev_difference' => $indi_rev_difference,
            'corp_rev_difference' => $corp_rev_difference,

        ]);
    }
    public function actionClientRevenueBanks()
    {
        $this->checkLogin();

        $holidays = array();
        $user=null;
        $custom = 0;
        $type='Banks';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_banks(Yii::$app->request->queryParams);

        // $view = 'client_revenue_cvo';
        // if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
        //     $view = 'client_revenue_cvo';
        // }else{
        //     $view = 'client_revenue';
        // }

        $view = 'client_revenue_banks';

        return $this->render($view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'custom' => $custom,
        ]);
    }
    public function actionClientRevenueBanks_old_25()
    {
        $this->checkLogin();
        /*  if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
              Yii::$app->getSession()->addFlash('error', "Permission denied!");
              return $this->redirect(['site/index']);
          }*/
        $holidays = array();
        $user=null;
        $custom = 0;
        $type='Banks';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_banks(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];

                    $Date=(explode(" - ",$custome_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];

                    $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);
                }
            }else{
                $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
            }

            /*
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);*/
        }else{
            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');

            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(0);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);
        }
        $view = 'client_revenue_cvo';
        if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
            $view = 'client_revenue_cvo';
        }else{
            $view = 'client_revenue';
        }

        return $this->render($view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'custom' => $custom,
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
            'allvalues' => $revevnue_all_types['bank'],
        ]);
    }

    //New Function For Banks Revenue
    public function actionClientRevenueBanksStatistics()
    {
        // dd(Yii::$app->request->queryParams);
        $this->checkLogin();
        /*  if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
              Yii::$app->getSession()->addFlash('error', "Permission denied!");
              return $this->redirect(['site/index']);
          }*/
        $holidays = array();
        $user=null;
        $custom = 0;
        $searchModel = new ValuationReportsSearch();

        $dataProvider = $searchModel->search_banks(Yii::$app->request->queryParams);

        if(Yii::$app->request->queryParams['Valuation']['valuation_month'] != " ") {
            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];
                    $Date=(explode(" - ",$custome_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];

                    $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);
                }
            }else{
                $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
            }

            /*
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);*/
        }else{

            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');

            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(0);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);
        }

        $duration = Yii::$app->request->queryParams['Valuation']['valuation_month'];
        $duration_yearly = Yii::$app->request->queryParams['Valuation']['valuation_year'];

        if(Yii::$app->request->queryParams['Valuation']['valuation_type'] == 'corporate')
        {
            $type = Yii::$app->request->queryParams['Valuation']['valuation_type'];
        }else{
            $type='banks';
        }


        $view = 'client_revenue_cvo';
        if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
            $view = 'client_revenue_cvo';
        }else{
            //created new view revenue by banks
            $view = 'revenue_by_banks';
        }

        return $this->render($view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'duration' => $duration,
            'duration_yearly' => $duration_yearly,
            'user' => $user,
            'type' => $type,
            'custom' => $custom,
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
            'allvalues' => $revevnue_all_types['bank'],
        ]);
    }

    //New Function For Banks Revenue
    public function actionClientRevenueBanksFee()
    {
        // dd('here');
        $this->checkLogin();
        /*  if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['site/index']);
        }*/
        $holidays = array();
        $user=null;
        $custom = 0;
        $searchModel = new ValuationReportsSearch();

        $dataProvider = $searchModel->search_banks(Yii::$app->request->queryParams);

        if(Yii::$app->request->queryParams['Valuation']['valuation_month'] != " ") {
            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];
                    $Date=(explode(" - ",$custome_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];

                    $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);
                }
            }else{
                $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
            }

            /*
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);*/
        }else{

            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');

            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(0);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);
        }

        if(Yii::$app->request->queryParams['Valuation']['valuation_type'] == 'corporate')
        {
            $type = Yii::$app->request->queryParams['Valuation']['valuation_type'];
        }else{
            $type='banks';
        }

        $duration = Yii::$app->request->queryParams['Valuation']['valuation_month'];
        $duration_yearly = Yii::$app->request->queryParams['Valuation']['valuation_year'];
        $view = 'client_revenue_cvo';
        if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
            $view = 'client_revenue_cvo';
        }else{
            //created new view revenue by banks
            $view = 'revenue_by_banks_fee';
        }

        return $this->render($view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'duration' => $duration,
            'duration_yearly' => $duration_yearly,
            'user' => $user,
            'type' => $type,
            'custom' => $custom,
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
            'allvalues' => $revevnue_all_types['bank'],
        ]);
    }

    public function actionClientRevenueCorporate()
    {
        $this->checkLogin();
        // if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
        //     Yii::$app->getSession()->addFlash('error', "Permission denied!");
        //     return $this->redirect(['site/index']);
        // }
        $holidays = array();
        $user=null;
        $custom = 0;
        $type='Corporate';

        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_corporate(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {


            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];

                    $Date=(explode(" - ",$custome_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];

                    $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);
                }
            }else{
                $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
            }

            /*

                        $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                        $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                        $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                        $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);*/
        }else{
            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(0);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);
        }

        return $this->render('client_revenue', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'custom' => $custom,
            'allvalues' => $revevnue_all_types['corporate'],
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
        ]);
    }

    public function actionClientRevenueIndividual()
    {
        $this->checkLogin();
        // if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
        //     Yii::$app->getSession()->addFlash('error', "Permission denied!");
        //     return $this->redirect(['site/index']);
        // }
        $user=null;
        $custom=0;
        $holidays = array();
        $type='Individual';
        $searchModel = new ValuationReportsSearch();

        $dataProvider = $searchModel->search_individual(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];

                    $Date=(explode(" - ",$custome_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];

                    $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);
                }
            }else{
                $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
            }




        }else{
            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(0);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);
        }

        return $this->render('client_revenue', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'custom' => $custom,
            'allvalues' => $revevnue_all_types['individual'],
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
        ]);
    }
    public function actionClientRevenueByCities()
    {
        $this->checkLogin();
        // if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
        //     Yii::$app->getSession()->addFlash('error', "Permission denied!");
        //     return $this->redirect(['site/index']);
        // }
        $holidays = array();
        $user=null;
        $custom = 0;
        $type='Individual';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_revenue_cities(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];

                    $Date=(explode(" - ",$custome_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];

                    $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);
                }
            }else{
                $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
            }




            /*            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                        $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                        $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);*/



        }else{
            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);

        }

        return $this->render('client_revenue_cities', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'custom' => $custom,
            'type' => $type,
            'allvalues' => $allvalues['total_values'],
            'allvalues_count' => $allvalues['total_valuations_count'],
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
        ]);
    }

    public function actionClientRevenueByValuers()
    {
        $this->checkLogin();
        /* if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
             Yii::$app->getSession()->addFlash('error', "Permission denied!");
             return $this->redirect(['site/index']);
         }*/
        $holidays = array();
        $user=null;
        $type='Individual';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_revenue_valuers(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];

                    $Date=(explode(" - ",$custome_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];

                    $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);
                }
            }else{
                $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
            }
            /*  $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);*/
        }else{
            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);

        }

        $view = 'client_revenue_valuers';
        if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
            $view = 'client_revenue_valuers_cvo';
        }else{
            $view = 'client_revenue_valuers';
        }
        return $this->render($view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'allvalues' => $allvalues['total_values'],
            'allvalues_count' => $allvalues['total_valuations_count'],
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
        ]);
    }

    public function actionValuersPerformanceData_old()
    {
        $this->checkLogin();
        // echo Yii::$app->appHelperFunctions->getCalculateAvgVals3(26);
        // die;
        /* if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
             Yii::$app->getSession()->addFlash('error', "Permission denied!");
             return $this->redirect(['site/index']);
         }*/
        $holidays = array();
        $user=null;
        $type='Individual';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_valuers_performance(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];
                    $Date=(explode(" - ",$custome_date_btw));
                    $start_date = $Date[0].' 00:00:00';
                    $end_date = $Date[1].' 23:59:59';

                    $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);
                }
            }else{
                $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
                $start_date = $date_array['start_date'].' 00:00:00';
                $end_date = $date_array['end_date'].' 23:59:59';
            }


            /*  $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);*/
        }else{
            $start_date = '2021-04-28 00:00:00';
            $end_date = date('Y-m-d').' 23:59:59';
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);

        }
        // echo "<pre>";
        // echo $start_date;
        // echo "<br>";
        // echo $end_date;die;
        // $view = 'client_revenue_valuers';
        // if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
        //     $view = 'client_revenue_valuers_cvo';
        // }else{
        //     $view = 'valuers_performance';
        // }
        $view = 'valuers_performance';

        return $this->render($view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'allvalues' => $allvalues['total_values'],
            'allvalues_count' => $allvalues['total_valuations_count'],
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
            'start_date' => $start_date,
            'end_date' => $end_date,
        ]);
    }

    public function actionValuersPerformanceData()
    {
        $this->checkLogin();
        // echo Yii::$app->appHelperFunctions->getCalculateAvgVals3(26);
        // die;
        /* if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
             Yii::$app->getSession()->addFlash('error', "Permission denied!");
             return $this->redirect(['site/index']);
         }*/
        $holidays = array();
        $user=null;
        $type='Individual';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_valuers_performance(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];
                    $Date=(explode(" - ",$custome_date_btw));
                    $start_date = $Date[0].' 00:00:00';
                    $end_date = $Date[1].' 23:59:59';

                    $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);
                }
            }else{
                $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
                $start_date = $date_array['start_date'].' 00:00:00';
                $end_date = $date_array['end_date'] . ' 23:59:59';
                // $end_date = date('Y-m-d H:i:s', strtotime($endDateTime . ' +1 day'));
            }


            /*  $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);*/
        }else{
            if(Yii::$app->request->queryParams != "null")
            {
                $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(2);
                $date_array = Yii::$app->appHelperFunctions->getFilterDates(2);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
                $start_date = $date_array['start_date'].' 00:00:00';

                $end_date = $date_array['end_date'] . ' 23:59:59';
                // $end_date = date('Y-m-d H:i:s', strtotime($endDateTime . ' +1 day'));
            }else{
                $start_date = '2021-04-28 00:00:00';
                $end_date = date('Y-m-d').' 23:59:59';
                $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);

            }
        }
        // echo "<pre>";
        // echo $start_date;
        // echo "<br>";
        // echo $end_date;die;
        // $view = 'client_revenue_valuers';
        // if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
        //     $view = 'client_revenue_valuers_cvo';
        // }else{
        //     $view = 'valuers_performance';
        // }

        // echo "<pre>";
        // echo $start_date;
        // echo "<br>";
        // echo $end_date;
        // die;
        $view = 'valuers_performance';

        return $this->render($view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'allvalues' => $allvalues['total_values'],
            'allvalues_count' => $allvalues['total_valuations_count'],
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
            'start_date' => $start_date,
            'end_date' => $end_date,
        ]);
    }
    public function actionValuersPerformanceData_old_29()
    {
        $this->checkLogin();
        // echo Yii::$app->appHelperFunctions->getCalculateAvgVals3(26);
        // die;
        /* if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
             Yii::$app->getSession()->addFlash('error', "Permission denied!");
             return $this->redirect(['site/index']);
         }*/
        $holidays = array();
        $user=null;
        $type='Individual';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_valuers_performance(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];
                    $Date=(explode(" - ",$custome_date_btw));
                    $start_date = $Date[0].' 00:00:00';
                    $end_date = $Date[1].' 23:59:59';

                    $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);
                }
            }else{
                $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
                $start_date = $date_array['start_date'].' 00:00:00';

                $endDateTime = $date_array['end_date'] . ' 23:59:59';
                $end_date = date('Y-m-d H:i:s', strtotime($endDateTime . ' +1 day'));
            }


            /*  $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);*/
        }else{
            if(Yii::$app->request->queryParams != "null")
            {
                $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(2);
                $date_array = Yii::$app->appHelperFunctions->getFilterDates(2);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
                $start_date = $date_array['start_date'].' 00:00:00';

                $endDateTime = $date_array['end_date'] . ' 23:59:59';
                $end_date = date('Y-m-d H:i:s', strtotime($endDateTime . ' +1 day'));
            }else{
                $start_date = '2021-04-28 00:00:00';
                $end_date = date('Y-m-d').' 23:59:59';
                $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);

            }
        }
        // echo "<pre>";
        // echo $start_date;
        // echo "<br>";
        // echo $end_date;die;
        // $view = 'client_revenue_valuers';
        // if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
        //     $view = 'client_revenue_valuers_cvo';
        // }else{
        //     $view = 'valuers_performance';
        // }
        $view = 'valuers_performance';

        return $this->render($view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'allvalues' => $allvalues['total_values'],
            'allvalues_count' => $allvalues['total_valuations_count'],
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
            'start_date' => $start_date,
            'end_date' => $end_date,
        ]);
    }

    public function actionApproversPerformanceData()
    {
        $this->checkLogin();
        // echo Yii::$app->appHelperFunctions->getCalculateAvgVals3(26);
        // die;
        /* if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
             Yii::$app->getSession()->addFlash('error', "Permission denied!");
             return $this->redirect(['site/index']);
         }*/
        $holidays = array();
        $user=null;
        $type='Individual';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_approvers_performance(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];

                    $Date=(explode(" - ",$custome_date_btw));
                    $start_date = $Date[0].' 00:00:00';
                    $end_date = $Date[1].' 23:59:59';

                    $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);
                }
            }else{
                $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
                $start_date = $date_array['start_date'].' 00:00:00';
                $end_date = $date_array['end_date'].' 23:59:59';
            }


            /*  $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);*/
        }else{
            $start_date = '2021-04-28 00:00:00';
            $end_date = date('Y-m-d').' 23:59:59';
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);

        }
        // echo "<pre>";
        // echo $start_date;
        // echo "<br>";
        // echo $end_date;die;
        // $view = 'client_revenue_valuers';
        // if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
        //     $view = 'client_revenue_valuers_cvo';
        // }else{
        //     $view = 'approvers_performance';
        // }
        $view = 'approvers_performance';

        return $this->render($view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'allvalues' => $allvalues['total_values'],
            'allvalues_count' => $allvalues['total_valuations_count'],
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
            'start_date' => $start_date,
            'end_date' => $end_date,
        ]);
    }

    public function actionClientRevenueByInspect()
    {
        $this->checkLogin();
        /* if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
             Yii::$app->getSession()->addFlash('error', "Permission denied!");
             return $this->redirect(['site/index']);
         }*/
        $holidays = array();
        $user=null;
        $type='Individual';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_revenue_valuers_inspection(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];

                    $Date=(explode(" - ",$custome_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];

                    $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);
                }
            }else{
                $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
            }


            /*            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                        $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                        $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);*/
        }else{
            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);

        }
        $view = 'client_revenue_valuers';
        if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019,110465,111558,38])) {
            $view = 'client_revenue_valuers_cvo';
        }else{
            $view = 'client_revenue_inspector';
        }
        return $this->render($view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'allvalues' => $allvalues['total_values'],
            'allvalues_count' => $allvalues['total_valuations_count'],
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
        ]);
    }

    public function actionValOpteam()
    {
        $this->checkLogin();

        $holidays = array();
        $user=null;
        $type='Individual';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_revenue_valuers_inspection(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];

                    $Date=(explode(" - ",$custome_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];

                    $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);
                }
            }else{
                $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
            }


            /*            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                        $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                        $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);*/
        }else{
            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);

        }
        $view = 'client_revenue_valuers';
        if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019,110465])) {
            $view = 'client_revenue_valuers_cvo';
        }else{
            $view = 'client_revenue_inspector';
        }
        return $this->render($view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'allvalues' => $allvalues['total_values'],
            'allvalues_count' => $allvalues['total_valuations_count'],
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
        ]);
    }



    public function actionClientRevenueByPropertyType()
    {
        $this->checkLogin();
        // if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019,6319])) {
        //     Yii::$app->getSession()->addFlash('error', "Permission denied!");
        //     return $this->redirect(['site/index']);
        // }
        $holidays = array();
        $user=null;
        $custom = 0;
        $type='Individual';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_revenue_properties(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];

                    $Date=(explode(" - ",$custome_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];

                    $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);
                }
            }else{

                $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
            }





        }else{
            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);

        }


        return $this->render('client_revenue_properties', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'custom' => $custom,
            'type' => $type,
            'allvalues' => $allvalues['total_values'],
            'allvalues_count' => $allvalues['total_valuations_count'],
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
        ]);
    }


    public function actionLowHighValuations()
    {
        $this->checkLogin();
        /*  if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
              Yii::$app->getSession()->addFlash('error', "Permission denied!");
              return $this->redirect(['site/index']);
          }*/
        $user=null;
        $change_average =0;
        $total_rows =0;
        $average =0;
        $custom = 0;
        $type='Low-High Valuations';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_low_high_valuations(Yii::$app->request->queryParams);

        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];

                    $Date=(explode(" - ",$custome_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{

                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }



            $query = Valuation::find()
                ->select([
                    Valuation::tableName().'.id',
                    Valuation::tableName().'.parent_id',
                    Valuation::tableName().'.client_id',
                    Valuation::tableName().'.property_id',
                    Valuation::tableName().'.building_info',
                    Valuation::tableName().'.service_officer_name',
                    'estimated_market_value'=>ValuationApproversData::tableName().'.estimated_market_value',
                    'approver'=>ValuationApproversData::tableName().'.created_by',
                ])
                ->leftJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                //->where([Valuation::tableName().'.valuation_status' => 5])
                ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
                ->andWhere([Valuation::tableName().'.revised_reason' => [1,2]]);


            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
            $all_data = $query->all();

            $total_rows = count($all_data);

            foreach ($all_data as $record){
                if($record->valuationParent->approverData->estimated_market_value > $record->estimated_market_value){
                    $updated_amount = ($record->valuationParent->approverData->estimated_market_value - $record->estimated_market_value);
                    $change_average = $change_average -number_format( ($updated_amount/$record->valuationParent->approverData->estimated_market_value) * 100,2). ' %';
                }else{
                    $updated_amount = ($record->estimated_market_value - $record->valuationParent->approverData->estimated_market_value);
                    $change_average = $change_average + number_format( ($updated_amount/$record->valuationParent->approverData->estimated_market_value) * 100,2). ' %';
                }

            }


        }else{

            $query = Valuation::find()
                ->select([
                    Valuation::tableName().'.id',
                    Valuation::tableName().'.parent_id',
                    Valuation::tableName().'.client_id',
                    Valuation::tableName().'.property_id',
                    Valuation::tableName().'.building_info',
                    Valuation::tableName().'.service_officer_name',
                    'estimated_market_value'=>ValuationApproversData::tableName().'.estimated_market_value',
                    'approver'=>ValuationApproversData::tableName().'.created_by',
                ])
                ->leftJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                //->where([Valuation::tableName().'.valuation_status' => 5])
                ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
                ->andWhere([Valuation::tableName().'.revised_reason' => [1,2]]);

            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $query->andFilterWhere([
                'between', 'instruction_date', $start_date, $end_date
            ]);
            $all_data = $query->all();

            $total_rows = count($all_data);

            foreach ($all_data as $record){
                if($record->valuationParent->approverData->estimated_market_value > $record->estimated_market_value){
                    $updated_amount = ($record->valuationParent->approverData->estimated_market_value - $record->estimated_market_value);
                    $change_average = $change_average -number_format( ($updated_amount/$record->valuationParent->approverData->estimated_market_value) * 100,2). ' %';
                }else{
                    $updated_amount = ($record->estimated_market_value - $record->valuationParent->approverData->estimated_market_value);
                    $change_average = $change_average + number_format( ($updated_amount/$record->valuationParent->approverData->estimated_market_value) * 100,2). ' %';
                }

            }


        }

        if($total_rows == 0){

            $average = 0;

        }else{
            $average = number_format(($change_average/$total_rows),2);
        }


        return $this->render('low-high-valuations', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'custom' => $custom,
            'type' => $type,
            'change_average' => $change_average,
            'average' => $average,
        ]);
    }

    public function actionModifiedValuations()
    {
        $this->checkLogin();
        /*  if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
              Yii::$app->getSession()->addFlash('error', "Permission denied!");
              return $this->redirect(['site/index']);
          }*/
        $user=null;
        $type='Modified Valuations';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_modified_valuations(Yii::$app->request->queryParams);

        return $this->render('low-high-valuations', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
        ]);
    }

    public function actionErrorsValuations()
    {
        $this->checkLogin();
        /* if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
             Yii::$app->getSession()->addFlash('error', "Permission denied!");
             return $this->redirect(['site/index']);
         }*/
        $user=null;
        $type='Errors In Valuations';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_errors_valuations(Yii::$app->request->queryParams);


        return $this->render('low-high-valuations', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
        ]);
    }

    public function actionSoldDuplicates()
    {
        $this->checkLogin();
        $user=null;
        //   $type='all sold , property placement , exposure, view_type';
        $type='Sold Duplicates';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_sold_duplicates(Yii::$app->request->queryParams);

        if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){

            if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                $custom = 1;
            }
        }else{
            $custom = 0;
        }


        return $this->render('sold-duplicates', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'custom' => $custom,
        ]);
    }

    public function actionListDuplicatesReference()
    {
        $this->checkLogin();
        $user=null;
        //   $type='all sold , property placement , exposure, view_type';
        $type='List Duplicates';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_list_duplicates_reference(Yii::$app->request->queryParams);
        if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){

            if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                $custom = 1;
            }
        }else{
            $custom = 0;
        }

        return $this->render('list-duplicates-reference', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'custom' => $custom,
        ]);
    }

    public function actionListDuplicates()
    {
        $this->checkLogin();

        $user=null;
        //   $type='all sold , property placement , exposure, view_type';
        $type='List Duplicates';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_list_duplicates(Yii::$app->request->queryParams);
        if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){

            if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                $custom = 1;
            }
        }else{
            $custom = 0;
        }

        return $this->render('list-duplicates', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'custom' => $custom,
        ]);
    }


    public function actionSoldNotUploaded()
    {
        $this->checkLogin();
        $years = array_combine(range(date("Y"), 2018), range(date("Y"), 2018));
        $months = array(
            '01' => "January",
            '02' => "February",
            '03' => "March",
            '04' => "April",
            '05' => "May",
            '06' => "June",
            '07' => "July",
            '08' => "August",
            '09' => "September",
            '10' => "October",
            '11' => "Novemeber",
            '12' => "December"
        );

        /* $today = date('Y-m-d');
         $date = strtotime('2022-02-20');
         $day = date('l', strtotime('2022-02-20'));
         echo $day;
         die;*/
        $user=null;
        //   $type='all sold , property placement , exposure, view_type';
        $type='Sold Not Uploaded';
        $current_year = date('Y');
        $current_month = date('m');

        if(!empty(Yii::$app->request->queryParams)){
            $query = Yii::$app->request->queryParams;
            $current_year = $query['ValuationReportsSearch']['years'];
            $current_month = $query['ValuationReportsSearch']['months'];
        }

        $a_date = $current_year.'-'.$current_month."-01";
        $lastDayThisMonth =  date("Y-m-t", strtotime($a_date));

        $today_start_date = $current_year.'-'.$current_month.'-01 00:00:00';
        $today_end_date = $lastDayThisMonth.' 23:59:59';


        // Declare two dates
        $Date1 =  $current_year.'-'.$current_month.'-01';
        $Date2 = $lastDayThisMonth;




// Use strtotime function
        $Variable1 = strtotime($Date1);
        $Variable2 = strtotime($Date2);

// Use for loop to store dates into array
// 86400 sec = 24 hrs = 60*60*24 = 1 day
        for ($currentDate = $Variable1; $currentDate <= $Variable2;
             $currentDate += (86400)) {

            $Store = date('Y-m-d', $currentDate);
            $all_dates[] = $Store;
        }


        $sold_data = (new \yii\db\Query())
            ->select('created_at')
            ->from('sold_transaction')
            ->where(['between','created_at',$today_start_date, $today_end_date])
            ->all();
        $all_created_dates = array();

        foreach ($sold_data as $entry){
            $all_created_dates[] = date('Y-m-d',strtotime($entry['created_at']));
        }

        $results=array_diff($all_dates,$all_created_dates);

        $data = array();
        if(!empty($results)){
            foreach ($results as $key => $result){
                $day = date('l', strtotime($result));
                // if($day != 'Saturday' && $day != 'Sunday') {
                $data[]['created_at'] = $result;
                //}

            }
        }

        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_list_duplicates(Yii::$app->request->queryParams);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'attributes' => ['created_at'],
            ],
        ]);


        return $this->render('sold-not-uploaded', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'years' => $years,
            'months' => $months,
            'type' => $type,
        ]);
    }


    public function actionTatReports_old()
    {
        $this->checkLogin();
        /*  if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
              Yii::$app->getSession()->addFlash('error', "Permission denied!");
              return $this->redirect(['site/index']);
          }*/
        $sub_insp = 0;
        $sub_inst = 0;
        $insp_sub = 0;
        $app_insp = 0;
        $custom = 0;
        $holidays = array();
        $user=null;
        $type='Tat Reports';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_tat(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];

                    $Date=(explode(" - ",$custome_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{

                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query = Valuation::find()
                ->select([
                    Valuation::tableName().'.id',
                    Valuation::tableName().'.reference_number',
                    Valuation::tableName().'.instruction_date',
                    Valuation::tableName().'.created_at',
                    'submission_date' => ScheduleInspection::tableName().'.valuation_report_date',
                    'inspection_date' => ScheduleInspection::tableName().'.inspection_date',
                    'inspection_time' => ScheduleInspection::tableName().'.inspection_time',
                    Valuation::tableName().'.property_id'
                ])
                ->innerJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
                // ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->where([Valuation::tableName().'.valuation_status' => 5])
                ->andWhere([Valuation::tableName().'.parent_id' => null]);


            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
            $all_data = $query->all();
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
            foreach ($all_data as $record){

                $t1 = strtotime( $record->approverData->created_at );
                $t2 = strtotime( $record->inspection_date.' '.	$record->inspection_time.':00' );
                $diff = $t1 - $t2;
                $hours = $diff / ( 60 * 60 );
                $sub_insp = $sub_insp + abs($hours/24);

                /* $submission_date_sub_insp = date('Y-m-d', strtotime($record->approverData->created_at));;
                 $diff_sub_insp = strtotime($submission_date_sub_insp) - strtotime($record->inspection_date);
                 $sub_insp = $sub_insp + abs(round($diff_sub_insp / 86400));*/

                $instruction_time = date('H:i:s', strtotime($record->created_at));
                $t3 = strtotime( $record->approverData->created_at );
                $t4 = strtotime( $record->inspection_date.' '.	$record->inspection_time.':00' );
                $diff = $t3 - $t4;
                $hours = $diff / ( 60 * 60 );
                $sub_insp = $sub_insp + abs($hours/24);

                $submission_date_sub_ins = date('Y-m-d', strtotime($record->approverData->created_at));;
                $diff_sub_ins = strtotime($submission_date_sub_ins) - strtotime($record->instruction_date);
                $sub_inst = $sub_inst +  abs(round($diff_sub_ins / 86400));

                $diff_insp_sub = strtotime($record->inspection_date) - strtotime($record->instruction_date);
                $insp_sub =$insp_sub +  abs(round($diff_insp_sub / 86400));

                $submission_date_app_insp = date('Y-m-d', strtotime($record->approverData->created_at));;
                $diff_app_insp = strtotime($submission_date_app_insp) - strtotime($record->instruction_date);
                $app_insp = $app_insp + abs(round($diff_app_insp / 86400));
            }


        }else{

            $query = Valuation::find()
                ->select([
                    Valuation::tableName().'.id',
                    Valuation::tableName().'.reference_number',
                    Valuation::tableName().'.instruction_date',
                    Valuation::tableName().'.created_at',
                    'submission_date' => ScheduleInspection::tableName().'.valuation_report_date',
                    'inspection_date' => ScheduleInspection::tableName().'.inspection_date',
                    'inspection_time' => ScheduleInspection::tableName().'.inspection_time',
                    Valuation::tableName().'.property_id'
                ])
                ->innerJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->where([Valuation::tableName().'.valuation_status' => 5])
                ->andWhere([Valuation::tableName().'.parent_id' => null]);

            $date_array = Yii::$app->appHelperFunctions->getFilterDates(1);
            $from_date = $date_array['start_date'];
            $to_date = $date_array['end_date'];

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
            $all_data = $query->all();
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);
            foreach ($all_data as $record){

                $submission_date_sub_insp = date('Y-m-d', strtotime($record->approverData->created_at));;
                $diff_sub_insp = strtotime($submission_date_sub_insp) - strtotime($record->inspection_date);
                $sub_insp = $sub_insp + abs(round($diff_sub_insp / 86400));


                $submission_date_sub_ins = date('Y-m-d', strtotime($record->approverData->created_at));;
                $diff_sub_ins = strtotime($submission_date_sub_ins) - strtotime($record->instruction_date);
                $sub_inst = $sub_inst +  abs(round($diff_sub_ins / 86400));

                $diff_insp_sub = strtotime($record->inspection_date) - strtotime($record->instruction_date);
                $insp_sub =$insp_sub +  abs(round($diff_insp_sub / 86400));

                $submission_date_app_insp = date('Y-m-d', strtotime($record->approverData->created_at));;
                $diff_app_insp = strtotime($submission_date_app_insp) - strtotime($record->instruction_date);
                $app_insp = $app_insp + abs(round($diff_app_insp / 86400));
            }


        }

        return $this->render('tat', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'sub_insp' => $sub_insp,
            'sub_inst' => $sub_inst,
            'insp_sub' => $insp_sub,
            'app_insp' => $app_insp,
            'custom' => $custom,
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
        ]);
    }

    public function actionTatTestReports()
    {
        $this->checkLogin();
        /*  if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
              Yii::$app->getSession()->addFlash('error', "Permission denied!");
              return $this->redirect(['site/index']);
          }*/
        $sub_insp = 0;
        $sub_inst = 0;
        $insp_sub = 0;
        $app_insp = 0;
        $custom = 0;
        $holidays = array();
        $user=null;
        $type='Tat Reports';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_tat(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];

                    $Date=(explode(" - ",$custome_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{

                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query = Valuation::find()
                ->select([
                    Valuation::tableName().'.id',
                    Valuation::tableName().'.reference_number',
                    Valuation::tableName().'.instruction_date',
                    Valuation::tableName().'.created_at',
                    'submission_date' => ScheduleInspection::tableName().'.valuation_report_date',
                    'inspection_date' => ScheduleInspection::tableName().'.inspection_date',
                    'inspection_time' => ScheduleInspection::tableName().'.inspection_time',
                    Valuation::tableName().'.property_id'
                ])
                ->innerJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
                // ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->where([Valuation::tableName().'.valuation_status' => 5]);


            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
            $all_data = $query->all();
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
            foreach ($all_data as $record){

                $t1 = strtotime( $record->approverData->created_at );
                $t2 = strtotime( $record->inspection_date.' '.	$record->inspection_time.':00' );
                $diff = $t1 - $t2;
                $hours = $diff / ( 60 * 60 );
                $sub_insp = $sub_insp + abs($hours/24);

                /* $submission_date_sub_insp = date('Y-m-d', strtotime($record->approverData->created_at));;
                 $diff_sub_insp = strtotime($submission_date_sub_insp) - strtotime($record->inspection_date);
                 $sub_insp = $sub_insp + abs(round($diff_sub_insp / 86400));*/

                $instruction_time = date('H:i:s', strtotime($record->created_at));
                $t3 = strtotime( $record->approverData->created_at );
                $t4 = strtotime( $record->inspection_date.' '.	$record->inspection_time.':00' );
                $diff = $t3 - $t4;
                $hours = $diff / ( 60 * 60 );
                $sub_insp = $sub_insp + abs($hours/24);

                $submission_date_sub_ins = date('Y-m-d', strtotime($record->approverData->created_at));;
                $diff_sub_ins = strtotime($submission_date_sub_ins) - strtotime($record->instruction_date);
                $sub_inst = $sub_inst +  abs(round($diff_sub_ins / 86400));

                $diff_insp_sub = strtotime($record->inspection_date) - strtotime($record->instruction_date);
                $insp_sub =$insp_sub +  abs(round($diff_insp_sub / 86400));

                $submission_date_app_insp = date('Y-m-d', strtotime($record->approverData->created_at));;
                $diff_app_insp = strtotime($submission_date_app_insp) - strtotime($record->instruction_date);
                $app_insp = $app_insp + abs(round($diff_app_insp / 86400));
            }


        }else{

            $query = Valuation::find()
                ->select([
                    Valuation::tableName().'.id',
                    Valuation::tableName().'.reference_number',
                    Valuation::tableName().'.instruction_date',
                    Valuation::tableName().'.created_at',
                    'submission_date' => ScheduleInspection::tableName().'.valuation_report_date',
                    'inspection_date' => ScheduleInspection::tableName().'.inspection_date',
                    'inspection_time' => ScheduleInspection::tableName().'.inspection_time',
                    Valuation::tableName().'.property_id'
                ])
                ->innerJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->where([Valuation::tableName().'.valuation_status' => 5]);

            $date_array = Yii::$app->appHelperFunctions->getFilterDates(1);
            $from_date = $date_array['start_date'];
            $to_date = $date_array['end_date'];

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
            $all_data = $query->all();
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);
            foreach ($all_data as $record){

                $submission_date_sub_insp = date('Y-m-d', strtotime($record->approverData->created_at));;
                $diff_sub_insp = strtotime($submission_date_sub_insp) - strtotime($record->inspection_date);
                $sub_insp = $sub_insp + abs(round($diff_sub_insp / 86400));


                $submission_date_sub_ins = date('Y-m-d', strtotime($record->approverData->created_at));;
                $diff_sub_ins = strtotime($submission_date_sub_ins) - strtotime($record->instruction_date);
                $sub_inst = $sub_inst +  abs(round($diff_sub_ins / 86400));

                $diff_insp_sub = strtotime($record->inspection_date) - strtotime($record->instruction_date);
                $insp_sub =$insp_sub +  abs(round($diff_insp_sub / 86400));

                $submission_date_app_insp = date('Y-m-d', strtotime($record->approverData->created_at));;
                $diff_app_insp = strtotime($submission_date_app_insp) - strtotime($record->instruction_date);
                $app_insp = $app_insp + abs(round($diff_app_insp / 86400));
            }


        }

        return $this->render('tat', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'sub_insp' => $sub_insp,
            'sub_inst' => $sub_inst,
            'insp_sub' => $insp_sub,
            'app_insp' => $app_insp,
            'custom' => $custom,
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
        ]);
    }

    public function actionTatAheadReports()
    {
        $this->checkLogin();
        /*
                if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
                    Yii::$app->getSession()->addFlash('error', "Permission denied!");
                    return $this->redirect(['site/index']);
                }*/

        $user=null;
        $type='Ahead Of Time Reports';
        $action='tat-ahead-reports';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_tat_ahead(Yii::$app->request->queryParams);

        if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){

            if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                $custom = 1;
            }
        }else{
            $custom = 0;
        }

        if(Yii::$app->request->queryParams['ValuationReportsSearch']['page_title'] == "Total Ahead of Time Reports")
        {
            $page_title = "Total Ahead of Time Reports";
            return $this->render('@app/views/valuation/total_valuation_performance', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'user' => $user,
                'page_title' => $page_title,
                'custom' => $custom,
                'action' => $action,
            ]);
        }else{
            return $this->render('tat_ahead', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'user' => $user,
                'type' => $type,
                'custom' => $custom,
                'action' => $action,
            ]);
        }
    }
    public function actionTatOnTimeReports()
    {

        $this->checkLogin();


        /*  if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
              Yii::$app->getSession()->addFlash('error', "Permission denied!");
              return $this->redirect(['site/index']);
          }*/

        $user=null;
        $action='tat-on-time-reports';
        $type='On Time Reports';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_tat_ontime(Yii::$app->request->queryParams);

        if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){

            if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                $custom = 1;
            }
        }else{
            $custom = 0;
        }

        if(Yii::$app->request->queryParams['ValuationReportsSearch']['page_title'] == "Total On Time")
        {
            $page_title = "Total On Time";
            return $this->render('@app/views/valuation/total_valuation_performance', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'user' => $user,
                'page_title' => $page_title,
                'custom' => $custom,
                'action' => $action,
            ]);
        }else{
            return $this->render('tat_ahead', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'user' => $user,
                'type' => $type,
                'custom' => $custom,
                'action' => $action,
            ]);
        }

    }
    public function actionTatDelayReports()
    {


        $this->checkLogin();

        /*  if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
              Yii::$app->getSession()->addFlash('error', "Permission denied!");
              return $this->redirect(['site/index']);
          }*/

        $user=null;
        $type='Delay Reports';
        $action='tat-delay-reports';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_tat_delay(Yii::$app->request->queryParams);
        if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){

            if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                $custom = 1;
            }
        }else{
            $custom = 0;
        }

        if(Yii::$app->request->queryParams['ValuationReportsSearch']['page_title'] == "Total Delay Reports")
        {
            $page_title = "Total Delay Reports";
            return $this->render('@app/views/valuation/total_valuation_performance', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'user' => $user,
                'page_title' => $page_title,
                'custom' => $custom,
                'action' => $action,
            ]);
        }else{
            return $this->render('tat_ahead', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'user' => $user,
                'type' => $type,
                'custom' => $custom,
                'action' => $action,
            ]);
        }
    }

    public function actionGrossYieldByPropertyType()
    {
        $this->checkLogin();
        /*  if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
              Yii::$app->getSession()->addFlash('error', "Permission denied!");
              return $this->redirect(['site/index']);
          }*/

        $all_categories = Yii::$app->appHelperFunctions->propertiesCategoriesList;


        foreach ($all_categories as $key => $category){
            $searchModel = new PropertiesSearch();
            $searchModel->gross_category = $key;
            $dataProvider[$key] = $searchModel->search(Yii::$app->request->queryParams);
        }


        $user=null;
        $type='Property Type';
        $searchModel = new ValuationReportsSearch();
        $dataProvider_report_array = $searchModel->search_gy_properties(Yii::$app->request->queryParams);
        $dataProvider_report = $dataProvider_report_array['propert_array'];
        $dataProvider_report_avg = $dataProvider_report_array['calculation_array'];

        $dataProvider_report_compare_array = $searchModel->search_gy_properties_compare(Yii::$app->request->queryParams);
        $dataProvider_report_compare = $dataProvider_report_compare_array['propert_array'];
        $dataProvider_report_compare_avg = $dataProvider_report_compare_array['calculation_array'];



        if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){

            if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                $custom = 1;
            }
        }else{
            $custom = 0;
        }

        if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period_compare'] == 9){


            if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw_compare']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw_compare'] <> null ) {

                $custom_compare = 1;
            }
        }else{
            $custom_compare = 0;
        }




        /* echo "<pre>";
         print_r($dataProvider_report_avg);
         die;*/

        return $this->render('gy_properties', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

            'dataProvider_report' => $dataProvider_report,
            'dataProvider_report_compare' => $dataProvider_report_compare,

            'dataProvider_report_avg' => $dataProvider_report_avg,
            'dataProvider_report_compare_avg' => $dataProvider_report_compare_avg,


            // 'dataProvider_compare' => $dataProvider_compare,
            'user' => $user,
            'custom' => $custom,
            'custom_compare' => $custom_compare,
            'type' => $type,
        ]);
    }
    public function actionGrossYieldByPropertyType_old_18_07_2022()
    {
        $this->checkLogin();
        /*  if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
              Yii::$app->getSession()->addFlash('error', "Permission denied!");
              return $this->redirect(['site/index']);
          }*/
        $user=null;
        $type='Individual';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_gy_properties_18_07_2022(Yii::$app->request->queryParams);
        $dataProvider_compare = $searchModel->search_gy_properties_compare_18_07_2022(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);

        }else{
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);

        }

        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period_compare']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period_compare'] > 0) {
            $allvalues_compare = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period_compare']);

        }else{
            $allvalues_compare = Yii::$app->appHelperFunctions->getRevenueTotal(0);

        }

        return $this->render('gy_properties_18_07_2022', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProvider_compare' => $dataProvider_compare,
            'user' => $user,
            'type' => $type,
            'allvalues' => $allvalues['total_values'],
            'allvalues_compare' => $allvalues_compare['total_values'],
        ]);
    }

    public function actionGrossYieldByPropertyType_old()
    {
        $this->checkLogin();
        /*  if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
              Yii::$app->getSession()->addFlash('error', "Permission denied!");
              return $this->redirect(['site/index']);
          }*/
        $user=null;
        $type='Individual';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_gy_properties(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);

        }else{
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);

        }

        return $this->render('gy_properties', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'allvalues' => $allvalues['total_values'],
        ]);
    }


    public function actionValuationStatusAll()
    {
        $this->checkLogin();
        // if(!in_array( Yii::$app->user->identity->id, [1,14])) {
        //     Yii::$app->getSession()->addFlash('error', "Permission denied!");
        //     return $this->redirect(['site/index']);
        // }

        $data = Yii::$app->request->queryParams['Valuation'];

        return $this->render('valuation-status/index', [
            'data' => $data,
        ]);

    }


    public function actionBanksBusinessAnalysis_old()
    {

        // if(!in_array( Yii::$app->user->identity->id, [1,14])) {
        //     Yii::$app->getSession()->addFlash('error', "Permission denied!");
        //     return $this->redirect(['site/index']);
        // }

        $user=null;
        $type='Banks';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_banks_business(Yii::$app->request->queryParams);

        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] <> null) {
            $searchModel->time_period = Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'];
            if(Yii::$app->request->queryParams['ValuationReportsSearch']['year']<>null){
                $searchModel->year = Yii::$app->request->queryParams['ValuationReportsSearch']['year'];
            }
            else{
                $searchModel->year = date('Y');
            }
        }
        else{
            $searchModel->time_period = 1;
            $searchModel->year = date('Y');
        }

        if($searchModel->time_period == 1){

            $valuations = Yii::$app->revenueHelper->getClientsValByMonth($searchModel->time_period, $searchModel->year);

        }
        if($searchModel->time_period == 2){
            $valuations = Yii::$app->revenueHelper->getClientsValByQuarter($searchModel->time_period, $searchModel->year);
        }
        if($searchModel->time_period == 3){
            $valuations = Yii::$app->revenueHelper->getClientsHalfYearVal($searchModel->time_period, $searchModel->year);
        }
        if($searchModel->time_period == 4){
            $valuations = Yii::$app->revenueHelper->getClientsYearlyVal($searchModel->time_period, $searchModel->year);
        }
        $no_of_valuations = Yii::$app->revenueHelper->getNoOfValuations($searchModel->time_period, $searchModel->year);
        $total_revenue = Yii::$app->revenueHelper->getTotalRevenue($searchModel->time_period, $searchModel->year);


        return $this->render('client_revenue_business', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'valuations' => $valuations,
            'no_of_valuations' => $no_of_valuations,
            'total_revenue' => $total_revenue,
        ]);
    }



    public function actionUpdateApprovalDate()
    {
        $query = \app\models\ValuationApproversData::find()
            ->select(['valuation_id', 'created_at'])
            ->where(['approver_type'=>'approver', 'status'=>'Approve'])
            //->andWhere(['is', 'submission_approver_date', new \yii\db\Expression('null')])
            ->asArray()
            ->all();
        $i = 0;
        foreach( $query as $key => $value ){
            Yii::$app->db->createCommand()->update('valuation', [ 'submission_approver_date' => $value['created_at'] ], [ 'id' => $value['valuation_id'] ])->execute();
            $i++;
        }

        dd("count: ". count($query). " saved: ". $i);

    }

    public function actionClientRevenueOnly_old()
    {
        // if(!in_array( Yii::$app->user->identity->id, [1,14])) {
        //     Yii::$app->getSession()->addFlash('error', "Permission denied!");
        //     return $this->redirect(['site/index']);
        // }

        $user=null;
        $type='Banks';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_banks_new(Yii::$app->request->queryParams);

        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] <> null) {
            $searchModel->time_period = Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'];
            if(Yii::$app->request->queryParams['ValuationReportsSearch']['year']<>null){
                $searchModel->year = Yii::$app->request->queryParams['ValuationReportsSearch']['year'];
            }
            else{
                $searchModel->year = date('Y');
            }
        }
        else{
            $searchModel->time_period = 1;
            $searchModel->year = date('Y');
        }

        // dd($searchModel);

        if($searchModel->time_period == 1){
            $revenue = Yii::$app->revenueHelper->getClientsValByMonth($searchModel->time_period, $searchModel->year, 'client_revenue');
        }
        if($searchModel->time_period == 2){
            $revenue = Yii::$app->revenueHelper->getClientsValByQuarter($searchModel->time_period, $searchModel->year, 'client_revenue');
        }
        if($searchModel->time_period == 3){
            $revenue = Yii::$app->revenueHelper->getClientsHalfYearVal($searchModel->time_period, $searchModel->year, 'client_revenue');
        }
        if($searchModel->time_period == 4){
            $revenue = Yii::$app->revenueHelper->getClientsYearlyVal($searchModel->time_period, $searchModel->year, 'client_revenue');
        }


        // dd($revenue);

        // $no_of_valuations = Yii::$app->revenueHelper->getNoOfValuations($searchModel->time_period, $searchModel->year);
        $total_revenue = Yii::$app->revenueHelper->getTotalRevenue($searchModel->time_period, $searchModel->year);
        // dd($no_of_valuations);

        return $this->render('client_revenue_only', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'revenue' => $revenue,
            'no_of_valuations' => $no_of_valuations,
            'total_revenue' => $total_revenue,
        ]);
    }

    public function actionClientRevenueOnly()
    {

        // if(!in_array( Yii::$app->user->identity->id, [1,14])) {
        //     Yii::$app->getSession()->addFlash('error', "Permission denied!");
        //     return $this->redirect(['site/index']);
        // }

        $user=null;
        $type='Banks';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_banks_new(Yii::$app->request->queryParams);

        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] <> null) {
            $searchModel->time_period = Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'];
            if(Yii::$app->request->queryParams['ValuationReportsSearch']['year']<>null){
                $searchModel->year = Yii::$app->request->queryParams['ValuationReportsSearch']['year'];
            }
            else{
                $searchModel->year = date('Y');
            }
        }
        else{
            $searchModel->time_period = 1;
            $searchModel->year = date('Y');
        }

        // dd($searchModel);

        if($searchModel->time_period == 1){
            $revenue = Yii::$app->revenueHelper->getClientsValByMonth($searchModel->time_period, $searchModel->year, 'client_revenue');
        }
        if($searchModel->time_period == 2){
            $revenue = Yii::$app->revenueHelper->getClientsValByQuarter($searchModel->time_period, $searchModel->year, 'client_revenue');
        }
        if($searchModel->time_period == 3){
            $revenue = Yii::$app->revenueHelper->getClientsHalfYearVal($searchModel->time_period, $searchModel->year, 'client_revenue');
        }
        if($searchModel->time_period == 4){
            $revenue = Yii::$app->revenueHelper->getClientsYearlyVal($searchModel->time_period, $searchModel->year, 'client_revenue');
        }



        // $no_of_valuations = Yii::$app->revenueHelper->getNoOfValuations($searchModel->time_period, $searchModel->year);
        $total_revenue = Yii::$app->revenueHelper->getTotalRevenue($searchModel->time_period, $searchModel->year);
        // dd($no_of_valuations);

        $totalNoOfClients = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                Valuation::tableName().'.client_id',
            ])
            ->leftJoin(Company::tableName(),Valuation::tableName().'.client_id='.Company::tableName().'.id')
            ->where([Company::tableName().'.client_type' => 'bank'])
            ->groupBy(Valuation::tableName().'.client_id')
            ->count();

        return $this->render('client_revenue_only', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'revenue' => $revenue,
            'no_of_valuations' => $no_of_valuations,
            'total_revenue' => $total_revenue,
            'totalNoOfClients' => $totalNoOfClients,
        ]);
    }


    public function actionBanksBusinessAnalysis()
    {

        // if(!in_array( Yii::$app->user->identity->id, [1,14])) {
        //     Yii::$app->getSession()->addFlash('error', "Permission denied!");
        //     return $this->redirect(['site/index']);
        // }

        $user=null;
        $type='Banks';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_banks_business(Yii::$app->request->queryParams);

        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] <> null) {
            $searchModel->time_period = Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'];
            if(Yii::$app->request->queryParams['ValuationReportsSearch']['year']<>null){
                $searchModel->year = Yii::$app->request->queryParams['ValuationReportsSearch']['year'];
            }
            else{
                $searchModel->year = date('Y');
            }
        }
        else{
            $searchModel->time_period = 1;
            $searchModel->year = date('Y');
        }

        if($searchModel->time_period == 1){
            $valuations = Yii::$app->revenueHelper->getClientsValByMonth($searchModel->time_period, $searchModel->year);
        }
        if($searchModel->time_period == 2){
            $valuations = Yii::$app->revenueHelper->getClientsValByQuarter($searchModel->time_period, $searchModel->year);
        }
        if($searchModel->time_period == 3){
            $valuations = Yii::$app->revenueHelper->getClientsHalfYearVal($searchModel->time_period, $searchModel->year);
        }
        if($searchModel->time_period == 4){
            $valuations = Yii::$app->revenueHelper->getClientsYearlyVal($searchModel->time_period, $searchModel->year);
        }
        $no_of_valuations = Yii::$app->revenueHelper->getNoOfValuations($searchModel->time_period, $searchModel->year);
        $total_revenue = Yii::$app->revenueHelper->getTotalRevenue($searchModel->time_period, $searchModel->year);


        $totalNoOfClients = Valuation::find()
            ->select([
                Valuation::tableName().'.id',
                Valuation::tableName().'.client_id',
            ])
            ->leftJoin(Company::tableName(),Valuation::tableName().'.client_id='.Company::tableName().'.id')
            ->where([Company::tableName().'.client_type' => 'bank'])
            ->groupBy(Valuation::tableName().'.client_id')
            ->count();

        return $this->render('client_revenue_business', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'valuations' => $valuations,
            'no_of_valuations' => $no_of_valuations,
            'total_revenue' => $total_revenue,
            'totalNoOfClients' => $totalNoOfClients,
        ]);
    }


    public function actionTatReports__odd()
    {
        $this->checkLogin();
        /*  if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
              Yii::$app->getSession()->addFlash('error', "Permission denied!");
              return $this->redirect(['site/index']);
          }*/
        $sub_insp = 0;
        $sub_inst = 0;
        $insp_sub = 0;
        $app_insp = 0;
        $custom = 0;
        $holidays = array();
        $user=null;
        $febrianApproveCount = 0; // 38 id
        $LaxmiApproveCount   = 0; // 21 id
        $type='Tat Reports';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_tat(Yii::$app->request->queryParams);

        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];

                    $Date=(explode(" - ",$custome_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{

                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query = Valuation::find()
                ->select([
                    Valuation::tableName().'.id',
                    Valuation::tableName().'.reference_number',
                    Valuation::tableName().'.instruction_date',
                    Valuation::tableName().'.created_at',
                    Valuation::tableName().'.submission_approver_date',
                    'submission_date' => ScheduleInspection::tableName().'.valuation_report_date',
                    'inspection_date' => ScheduleInspection::tableName().'.inspection_date',
                    'inspection_time' => ScheduleInspection::tableName().'.inspection_time',
                    Valuation::tableName().'.property_id'
                ])
                ->innerJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
                // ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->where([Valuation::tableName().'.valuation_status' => 5])
                ->andWhere([Valuation::tableName().'.parent_id' => null]);


            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
            $all_data = $query->all();
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
            foreach ($all_data as $record){


                /* $submission_date_sub_insp = date('Y-m-d', strtotime($record->approverData->created_at));;
                 $diff_sub_insp = strtotime($submission_date_sub_insp) - strtotime($record->inspection_date);
                 $sub_insp = $sub_insp + abs(round($diff_sub_insp / 86400));*/

                $instruction_time = date('H:i:s', strtotime($record->created_at));
                $t3 = strtotime( $record->approverData->created_at );
                $t4 = strtotime( $record->inspection_date.' '.	$record->inspection_time.':00' );
                $diff = $t3 - $t4;
                $hours = $diff / ( 60 * 60 );
                $sub_insp = $sub_insp + abs($hours/24);

                $sub_insp = $sub_insp +  Yii::$app->appHelperFunctions->checkAndExcludeWeekends($record->inspection_date.' '.	$record->inspection_time.':00', $record->submission_approver_date);

                $sub_inst = $sub_inst +  Yii::$app->appHelperFunctions->checkAndExcludeWeekends($record->instruction_date.' '. date('H:i:s', strtotime($record->created_at)), $record->submission_approver_date);

                $insp_sub = $insp_sub +  Yii::$app->appHelperFunctions->checkAndExcludeWeekends($record->instruction_date.' '. date('H:i:s', strtotime($record->created_at)), $record->inspection_date.' '.	$record->inspection_time.':00');

                $submission_date_app_insp = date('Y-m-d', strtotime($record->approverData->created_at));;
                $diff_app_insp = strtotime($submission_date_app_insp) - strtotime($record->instruction_date);
                $app_insp = $app_insp + abs(round($diff_app_insp / 86400));

            }


        }else{

            $query = Valuation::find()
                ->select([
                    Valuation::tableName().'.id',
                    Valuation::tableName().'.reference_number',
                    Valuation::tableName().'.instruction_date',
                    Valuation::tableName().'.created_at',
                    Valuation::tableName().'.submission_approver_date',
                    'submission_date' => ScheduleInspection::tableName().'.valuation_report_date',
                    'inspection_date' => ScheduleInspection::tableName().'.inspection_date',
                    'inspection_time' => ScheduleInspection::tableName().'.inspection_time',
                    Valuation::tableName().'.property_id',
                    // 'created_approve_date' => ValuationApproversData::tableName().'.created_at',
                ])
                ->innerJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
                // ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->where([Valuation::tableName().'.valuation_status' => 5])
                ->andWhere([Valuation::tableName().'.parent_id' => null]);
            // ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver']);


            $date_array = Yii::$app->appHelperFunctions->getFilterDates(1);
            $from_date = $date_array['start_date'];
            $to_date = $date_array['end_date'];

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
            $all_data = $query->all();

            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);

            foreach ($all_data as $record){

                $approverDate = $record->submission_approver_date;
                // if($record->submission_approver_date == null) {
                //     $approverDate = $record->created_approve_date;
                // }

                $sub_inst_start = $record->instruction_date.' '. date('H:i:s', strtotime($record->created_at));

                $sub_inst =   $sub_inst + Yii::$app->appHelperFunctions->checkAndExcludeWeekends($sub_inst_start, $approverDate);

                $insp_sub = $insp_sub +  Yii::$app->appHelperFunctions->checkAndExcludeWeekends($record->instruction_date.' '. date('H:i:s', strtotime($record->created_at)), $record->inspection_date.' '.	$record->inspection_time.':00');

                $sub_insp = $sub_insp +  Yii::$app->appHelperFunctions->checkAndExcludeWeekends($record->inspection_date.' '.	$record->inspection_time.':00', $approverDate );

                $submission_date_app_insp = date('Y-m-d', strtotime($record->approverData->created_at));
                $diff_app_insp = strtotime($submission_date_app_insp) - strtotime($record->instruction_date);
                $app_insp = $app_insp + abs(round($diff_app_insp / 86400));

            }
        }

        return $this->render('tat', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'sub_insp' => $sub_insp,
            'sub_inst' => $sub_inst,
            'insp_sub' => $insp_sub,
            'app_insp' => $app_insp,
            'custom' => $custom,
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            // 'febrianApproveCount' => $febrianApproveCount,
            // 'LaxmiApproveCount' => $LaxmiApproveCount,
        ]);
    }


    public function actionChangeinstime()
    {

        $query = ScheduleInspection::find();
        $query = $query->all();

        foreach($query as $model)
        {
            if($model->inspection_time<>null){
                $time = $model->inspection_time.":00";
                $exp  = explode(":", $time);


                if( $exp[0] < 8 ){
                    $exp[0] = str_pad($exp[0]+12, 2, "0", STR_PAD_LEFT);
                    unset($exp[2]);
                    $time = implode(':', $exp);

                    //update inspection time
                    Yii::$app->db->createCommand()
                        ->update('schedule_inspection', ['inspection_time' => $time], ['id' => $model->id])
                        ->execute();
                }

            }
        }
    }


    public function actionClientRevenueByApprovers()
    {
        // $start = new DateTime('2023-01-01 12:00:00');
        // $end   = new DateTime('2023-01-31 12:00:00');
        // $interval = new DateInterval('PT1H');
        // $daterange = new DatePeriod($start, $interval ,$end);

        // $count = 0;
        // foreach($daterange as $date){
        //     if($date->format("N") < 6) {
        //         $count++;
        //     }
        // }

        // echo $start->format("Y-m-d H:i:s")."<br>";
        // echo $end->format("Y-m-d H:i:s")."<br>";
        // echo $count."<br>";
        // echo $count/24 ."<br>";
        // die;



        // if(!in_array( Yii::$app->user->identity->id, [1,14,110465,6319,21,86])) {
        //     Yii::$app->getSession()->addFlash('error', "Permission denied!");
        //     return $this->redirect(['site/index']);
        // }

        $user=null;
        $type='Individual';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_revenue_approvers(Yii::$app->request->queryParams);
        // dd($dataProvider->getTotalCount());
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
        }else{
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            // dd($allvalues);

        }

        return $this->render('client_revenue_approvers', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'allvalues' => $allvalues['total_values'],
        ]);
    }


    public function actionTatReports()
    {
        $this->checkLogin();
        /*  if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
              Yii::$app->getSession()->addFlash('error', "Permission denied!");
              return $this->redirect(['site/index']);
          }*/
        $sub_insp = 0;
        $sub_inst = 0;
        $insp_sub = 0;
        $app_insp = 0;
        $custom = 0;
        $holidays = array();
        $user=null;
        $febrianApproveCount = 0; // 38 id
        $LaxmiApproveCount   = 0; // 21 id
        $type='Tat Reports';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_tat(Yii::$app->request->queryParams);

        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];

                    $Date=(explode(" - ",$custome_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{

                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query = Valuation::find()
                ->select([
                    Valuation::tableName().'.id',
                    Valuation::tableName().'.reference_number',
                    Valuation::tableName().'.instruction_date',
                    Valuation::tableName().'.created_at',
                    Valuation::tableName().'.submission_approver_date',
                    'submission_date' => ScheduleInspection::tableName().'.valuation_report_date',
                    'inspection_date' => ScheduleInspection::tableName().'.inspection_date',
                    'inspection_time' => ScheduleInspection::tableName().'.inspection_time',
                    Valuation::tableName().'.property_id'
                ])
                ->innerJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->innerJoin(Company::tableName(),Company::tableName().'.id='.Valuation::tableName().'.client_id')
                // ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->where([Valuation::tableName().'.valuation_status' => 5])
                ->andWhere([Valuation::tableName().'.parent_id' => null]);


            $query->andFilterWhere([
                'between', Valuation::tableName().'.submission_approver_date', $from_date." 00:00:00", $to_date." 23:59:59",
                // 'between', 'instruction_date', $from_date, $to_date
            ]);

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['property_id'] <> null){
                $property_id = Yii::$app->request->queryParams['ValuationReportsSearch']['property_id'];
                $query->andWhere([Valuation::tableName().'.property_id' => $property_id]);
            }
            if(Yii::$app->request->queryParams['ValuationReportsSearch']['tenure'] <>null){
                $tenure = Yii::$app->request->queryParams['ValuationReportsSearch']['tenure'];
                $query->andWhere([Valuation::tableName().'.tenure' => $tenure]);
            }
            if(Yii::$app->request->queryParams['ValuationReportsSearch']['client_type'] <>null){
                $client_type = Yii::$app->request->queryParams['ValuationReportsSearch']['client_type'];
                $query->andWhere([Company::tableName().'.client_type' => $client_type]);
            }
            if(Yii::$app->request->queryParams['ValuationReportsSearch']['valuation_approach'] <>null){
                $valuation_approach = Yii::$app->request->queryParams['ValuationReportsSearch']['valuation_approach'];
                $query->innerJoin(Properties::tableName(),Properties::tableName().'.id='.Valuation::tableName().'.property_id');
                $query->andWhere([Properties::tableName().'.valuation_approach' => $valuation_approach]);
            }


            $all_data = $query->all();
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
            foreach ($all_data as $record){

                $inspection_time = '00:00:00';
                if($record->inspection_time<>null){
                    $inspection_time = $record->inspection_time.':00';
                }

                $sub_insp = $sub_insp +  Yii::$app->appHelperFunctions->checkAndExcludeWeekends($record->inspection_date.' '.	$inspection_time, $record->submission_approver_date);

                $sub_inst = $sub_inst +  Yii::$app->appHelperFunctions->checkAndExcludeWeekends($record->instruction_date.' '. date('H:i:s', strtotime($record->created_at)), $record->submission_approver_date);

                $insp_sub = $insp_sub +  Yii::$app->appHelperFunctions->checkAndExcludeWeekends($record->instruction_date.' '. date('H:i:s', strtotime($record->created_at)), $record->inspection_date.' '.	$inspection_time);
            }


        }else{
            $query = Valuation::find()
                ->select([
                    Valuation::tableName().'.id',
                    Valuation::tableName().'.reference_number',
                    Valuation::tableName().'.instruction_date',
                    Valuation::tableName().'.created_at',
                    Valuation::tableName().'.submission_approver_date',
                    'submission_date' => ScheduleInspection::tableName().'.valuation_report_date',
                    'inspection_date' => ScheduleInspection::tableName().'.inspection_date',
                    'inspection_time' => ScheduleInspection::tableName().'.inspection_time',
                    'inspection_created_at' => ScheduleInspection::tableName().'.created_at',
                    Valuation::tableName().'.property_id',
                ])
                ->innerJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->where([Valuation::tableName().'.valuation_status' => 5])
                ->andWhere([Valuation::tableName().'.parent_id' => null]);


            $date_array = Yii::$app->appHelperFunctions->getFilterDates(1);
            $from_date = $date_array['start_date'];
            $to_date = $date_array['end_date'];

            $query->andFilterWhere([
                'between', Valuation::tableName().'.submission_approver_date', $from_date." 00:00:00", $to_date." 23:59:59",
                // 'between', 'instruction_date', $from_date, $to_date
            ]);
            $all_data = $query->all();

            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);

            foreach ($all_data as $record){

                $approverDate = $record->submission_approver_date;

                $inspection_time = $record->inspection_time;
                if($record->inspection_time==null){
                    $inspection_time = '00:00:00';
                }

                $sub_inst_start = $record->instruction_date.' '. date('H:i:s', strtotime($record->created_at));
                $sub_inst =   $sub_inst + Yii::$app->appHelperFunctions->checkAndExcludeWeekends($sub_inst_start, $approverDate);

                $sub_insp = $sub_insp +  Yii::$app->appHelperFunctions->checkAndExcludeWeekends($record->inspection_date.' '. $inspection_time, $approverDate );

                $insp_sub = $insp_sub +  Yii::$app->appHelperFunctions->checkAndExcludeWeekends($record->instruction_date.' '. date('H:i:s', strtotime($record->created_at)), $record->inspection_date.' '.	$inspection_time);

            }
        }

        return $this->render('tat', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'sub_insp' => $sub_insp,
            'sub_inst' => $sub_inst,
            'insp_sub' => $insp_sub,
            'app_insp' => $app_insp,
            'custom' => $custom,
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
        ]);

        // if($type == "Tat Reports")
        // {
        //     return $this->render('@app/views/valuation/total_valuation_performance', [
        //         'searchModel' => $searchModel,
        //         'dataProvider' => $dataProvider,
        //         'user' => $user,
        //         'page_title' => $type,
        //         'sub_insp' => $sub_insp,
        //         'sub_inst' => $sub_inst,
        //         'insp_sub' => $insp_sub,
        //         'app_insp' => $app_insp,
        //         'custom' => $custom,
        //         'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
        //     ]);
        // }else{
        //     return $this->render('tat', [
        //         'searchModel' => $searchModel,
        //         'dataProvider' => $dataProvider,
        //         'user' => $user,
        //         'type' => $type,
        //         'sub_insp' => $sub_insp,
        //         'sub_inst' => $sub_inst,
        //         'insp_sub' => $insp_sub,
        //         'app_insp' => $app_insp,
        //         'custom' => $custom,
        //         'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
        //     ]);
        // }


    }

    public function actionTatReportsToday()
    {
        $this->checkLogin();
        /*  if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
              Yii::$app->getSession()->addFlash('error', "Permission denied!");
              return $this->redirect(['site/index']);
          }*/

        $diff_1 = 0;
        $diff_2 = 0;
        $diff_3 = 0;
        $diff_4 = 0;
        $diff_5 = 0;
        $diff_6 = 0;
        $diff_7 = 0;
        $diff_8 = 0;
        $diff_9 = 0;
        $diff_10 = 0;



        $sub_insp = 0;
        $sub_inst = 0;
        $insp_sub = 0;
        $app_insp = 0;
        $custom = 0;
        $holidays = array();
        $user=null;
        $febrianApproveCount = 0; // 38 id
        $LaxmiApproveCount   = 0; // 21 id
        $type='Tat Reports Today';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_tat_daily(Yii::$app->request->queryParams);

        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];

                    $Date=(explode(" - ",$custome_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];
                }
            }else{

                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $from_date = $date_array['start_date'];
                $to_date = $date_array['end_date'];
            }

            $query = Valuation::find()
                ->select([
                    Valuation::tableName().'.id',
                    Valuation::tableName().'.reference_number',
                    Valuation::tableName().'.instruction_date',
                    Valuation::tableName().'.created_at',
                    Valuation::tableName().'.submission_approver_date',
                    'submission_date' => ScheduleInspection::tableName().'.valuation_report_date',
                    'inspection_date' => ScheduleInspection::tableName().'.inspection_date',
                    'inspection_time' => ScheduleInspection::tableName().'.inspection_time',
                    Valuation::tableName().'.property_id'
                ])
                ->leftJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->leftJoin(Company::tableName(),Company::tableName().'.id='.Valuation::tableName().'.client_id')
                // ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->where([Valuation::tableName().'.valuation_status' => 5])
                ->andWhere([Valuation::tableName().'.parent_id' => null]);


            $query->andFilterWhere([
                'between', Valuation::tableName().'.submission_approver_date', $from_date." 00:00:00", $to_date." 23:59:59",
                // 'between', 'instruction_date', $from_date, $to_date
            ]);

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['property_id'] <> null){
                $property_id = Yii::$app->request->queryParams['ValuationReportsSearch']['property_id'];
                $query->andWhere([Valuation::tableName().'.property_id' => $property_id]);
            }
            if(Yii::$app->request->queryParams['ValuationReportsSearch']['tenure'] <>null){
                $tenure = Yii::$app->request->queryParams['ValuationReportsSearch']['tenure'];
                $query->andWhere([Valuation::tableName().'.tenure' => $tenure]);
            }
            if(Yii::$app->request->queryParams['ValuationReportsSearch']['client_type'] <>null){
                $client_type = Yii::$app->request->queryParams['ValuationReportsSearch']['client_type'];
                $query->andWhere([Company::tableName().'.client_type' => $client_type]);
            }
            if(Yii::$app->request->queryParams['ValuationReportsSearch']['valuation_approach'] <>null){
                $valuation_approach = Yii::$app->request->queryParams['ValuationReportsSearch']['valuation_approach'];
                $query->innerJoin(Properties::tableName(),Properties::tableName().'.id='.Valuation::tableName().'.property_id');
                $query->andWhere([Properties::tableName().'.valuation_approach' => $valuation_approach]);
            }


            $all_data = $query->all();
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
            foreach ($all_data as $record){

                $inspection_time = '00:00:00';
                if($record->inspection_time<>null){
                    $inspection_time = $record->inspection_time.':00';
                }

                $sub_insp = $sub_insp +  Yii::$app->appHelperFunctions->checkAndExcludeWeekends($record->inspection_date.' '.	$inspection_time, $record->submission_approver_date);

                $sub_inst = $sub_inst +  Yii::$app->appHelperFunctions->checkAndExcludeWeekends($record->instruction_date.' '. date('H:i:s', strtotime($record->created_at)), $record->submission_approver_date);

                $insp_sub = $insp_sub +  Yii::$app->appHelperFunctions->checkAndExcludeWeekends($record->instruction_date.' '. date('H:i:s', strtotime($record->created_at)), $record->inspection_date.' '.	$inspection_time);
            }


        }else{

            $query = Valuation::find()
                ->select([
                    Valuation::tableName().'.id',
                    Valuation::tableName().'.reference_number',
                    Valuation::tableName().'.instruction_date',
                    Valuation::tableName().'.instruction_time',
                    Valuation::tableName().'.service_officer_name',
                    Valuation::tableName().'.building_info',
                    Valuation::tableName().'.property_id',
                    Valuation::tableName().'.tenure',
                    Valuation::tableName().'.created_at',
                    Valuation::tableName().'.submission_approver_date',
                    'submission_date' => ScheduleInspection::tableName().'.valuation_report_date',
                    'inspection_date' => ScheduleInspection::tableName().'.inspection_date',
                    'inspection_time' => ScheduleInspection::tableName().'.inspection_time',
                    'inspection_officer_name' => ScheduleInspection::tableName().'.inspection_officer',
                    'inspection_done_date_time' => InspectProperty::tableName().'.inspection_done_date',
                    Valuation::tableName().'.property_id',
                    'arrived_date_time' => ValuationDetail::tableName().'.arrived_date_time',
                    'approver'=>ValuationApproversData::tableName().'.created_by'
                ])
                ->leftJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->leftJoin(ValuationDetail::tableName(),ValuationDetail::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->innerJoin(InspectProperty::tableName(),InspectProperty::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->innerJoin(Company::tableName(),Company::tableName().'.id='.Valuation::tableName().'.client_id')
                ->where([Valuation::tableName().'.valuation_status' => 5])
                ->andWhere([Valuation::tableName().'.parent_id' => null])
                ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver']);
            $from_date = date('Y-m-d');
            $to_date = date('Y-m-d');

            $query->andFilterWhere([
                'between', Valuation::tableName().'.submission_approver_date', $from_date." 00:00:00", $to_date." 23:59:59",
                // 'between', 'instruction_date', $from_date, $to_date
            ]);
            $all_data = $query->all();

            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);

            foreach ($all_data as $record){

                if($record->inspection_type != 3) {

                    if($record->instruction_date != '' && $record->created_at !='') {

                        // $startDate = $record->created_at;
                        $startDate = $record->instruction_date . ' ' . date('H:i:s', strtotime($record->instruction_time));
                        $endDate = $record->scheduleInspection->created_at;

                        $startDateTime = new DateTime($startDate); // Start date and time
                        $endDateTime = new DateTime($endDate);

                        $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);

                    }else{
                        $workingHours_1 = 0;
                    }
                    $diff_1 = $diff_1 + $workingHours_1;
                }

                if($record->inspection_type != 3) {

                    if ($record->scheduleInspection->created_at != '' && $record->created_at != '') {

                        $startDate = $record->created_at;
                        $endDate = $record->scheduleInspection->created_at;


                        $startDateTime = new DateTime($startDate); // Start date and time
                        $endDateTime = new DateTime($endDate);

                        $workingHours_2 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                        if ($record->reference_number == 'REV-2023-10737') {
                            $workingHours_2 = $workingHours_2 - 8.5;
                        }
                    } else {
                        $workingHours_2 = 0;
                    }
                    $diff_2 = $diff_2 + $workingHours_2;
                }

                if($record->inspection_type != 3) {
                    $inspection_time = $record->inspection_time . ":00";
                    if ($record->inspection_time == null) {
                        $inspection_time = '00:00:00';
                    }
                    if($record->scheduleInspection->created_at != '' && $record->scheduleInspection->inspection_date !='') {

                        $minus = 0;

                        $startDate = $record->scheduleInspection->inspection_date.' '.$inspection_time;
                        $endDate = $record->valuationDetail->arrived_date_time;

                        $startDateTime = new DateTime($startDate); // Start date and time
                        $endDateTime = new DateTime($endDate);

                        // dd($startDateTime);

                        $workingHours_1 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);

                    }else{
                        $workingHours_1 = 0;
                    }
                    $diff_10 = $diff_10 + $workingHours_1;
                }


                if($record->inspection_type != 3) {

                    $inspection_time = $record->inspection_time . ":00";
                    if ($record->inspection_time == null) {
                        $inspection_time = '00:00:00';
                    }
                    if($record->scheduleInspection->created_at != '' && $record->scheduleInspection->inspection_date !='') {

                        $minus = 0;

                        $startDate = $record->scheduleInspection->created_at;
                        $endDate = $record->scheduleInspection->inspection_date.' '.$inspection_time;


                        $startDateTime = new DateTime($startDate); // Start date and time
                        $endDateTime = new DateTime($endDate);

                        $workingHours_3 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                        if($record->reference_number == 'REV-2023-10772'){
                            $workingHours_3 = $workingHours_3 - 8.5;
                        }


                    }else{
                        $workingHours_3 = 0;
                    }
                    $diff_3 = $diff_3 + $workingHours_3;
                }

                if($record->inspection_type != 3) {

                    $inspection_time = $record->inspection_time . ":00";
                    if ($record->inspection_time == null) {
                        $inspection_time = '00:00:00';
                    }
                    if($record->inspection_done_date_time != '' && $record->scheduleInspection->inspection_date !='') {

                        $minus = 0;

                        //  $startDate = $record->scheduleInspection->created_at;
                        // $startDate = $record->scheduleInspection->inspection_date.' '.$inspection_time;
                        $startDate = $record->valuationDetail->arrived_date_time;
                        $endDate = $record->inspection_done_date_time;


                        $startDateTime = new DateTime($startDate); // Start date and time
                        $endDateTime = new DateTime($endDate);

                        $workingHours_4= Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                        /* if($record->reference_number == 'REV-2023-10772'){
                             $workingHours = $workingHours - 8.5;
                         }*/


                    }else{
                        $workingHours_4 = 0;
                    }

                }
                $diff_4 = $diff_4 + $workingHours_4;

                if($record->inspection_done_date_time != '' && $record->valuerData->created_at!='') {

                    $minus = 0;

                    $startDate = $record->inspection_done_date_time;
                    $endDate = $record->valuerData->created_at;


                    $startDateTime = new DateTime($startDate); // Start date and time
                    $endDateTime = new DateTime($endDate);

                    $workingHours_5= Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                    if($record->reference_number == 'REV-2023-10756'){
                        $workingHours_5 = $workingHours_5 - 8.5;
                    }


                }else{
                    $workingHours_5 = 0;
                }
                $diff_5 = $diff_5 + $workingHours_5;





                if($record->valuerData->created_at != '' && $record->valuerData->created_at!='') {

                    $minus = 0;



                    $startDate = $record->valuerData->created_at;
                    $endDate = $record->submission_approver_date;


                    $startDateTime = new DateTime($startDate); // Start date and time
                    $endDateTime = new DateTime($endDate);

                    $workingHours_6= Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                    /* if($record->reference_number == 'REV-2023-10756'){
                         $workingHours_2 = $workingHours_2 - 8.5;
                     }*/


                }else{
                    $workingHours_6 = 0;
                }
                $diff_6 = $diff_6 + $workingHours_6;


                if($record->inspection_type != 3) {
                    $inspection_time = $record->inspection_time . ":00";
                    if ($record->inspection_time == null) {
                        $inspection_time = '00:00:00';
                    }


                    if ($record->instruction_date != '' && $record->inspection_date != '') {
                        $startDate = $record->instruction_date . ' ' . date('H:i:s', strtotime($record->instruction_time));
                        $endDate = $record->inspection_date . ' ' . $inspection_time;


                        $startDateTime = new DateTime($startDate); // Start date and time
                        $endDateTime = new DateTime($endDate);

                        $workingHours_7 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                        if ($record->reference_number == 'REV-2023-10737' || $record->reference_number == 'REV-2023-10756') {
                            $workingHours_7 = $workingHours_7 - 8.5;
                        }
                    } else {
                        $workingHours_7 = 0;
                    }
                    $diff_7 = $diff_7 + $workingHours_7;
                }



                if($record->inspection_type != 3) {
                    $inspection_time = $record->inspection_time . ":00";
                    if ($record->inspection_time == null) {
                        $inspection_time = '00:00:00';
                    }

                    $startDate = $record->inspection_date . ' ' . $inspection_time;
                    $endDate = $record->submission_approver_date;

                    if ($record->inspection_date != '' && $record->submission_approver_date != '') {
                        $startDateTime = new DateTime($startDate); // Start date and time
                        $endDateTime = new DateTime($endDate);
                        $workingHours_8 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                        /* if($record->reference_number == 'REV-2023-10772' || $record->reference_number == 'REV-2023-10756'){
                             $workingHours = $workingHours - 8.5;
                         }*/
                    } else {
                        $workingHours_8 = 0;
                    }
                    $diff_8 = $diff_8 + $workingHours_8;
                }
                if($record->inspection_type != 3) {

                    $inspection_time = $record->inspection_time . ":00";
                    if ($record->inspection_time == null) {
                        $inspection_time = '00:00:00';
                    }

                    if($record->instruction_date !='' && $record->inspection_date !='') {

                        $startDate = $record->instruction_date . ' ' . date('H:i:s', strtotime($record->instruction_time));
                        $endDate = $record->inspection_date . ' ' . $inspection_time;

                        $startDateTime = new DateTime($startDate); // Start date and time
                        $endDateTime = new DateTime($endDate);
                        $workingHours_9 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                    }else{
                        $workingHours_9 =0;
                    }
                    // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                    //  $time_1 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate, $endDate), 1));
                    //  $time_1_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate, $endDate), 1));

                    if($record->inspection_date !='' && $record->submission_approver_date !='') {
                        $startDate_2 = $record->inspection_date . ' ' . $inspection_time;
                        $endDate_2 = $record->submission_approver_date;
                        $startDateTime = new DateTime($startDate_2); // Start date and time
                        $endDateTime = new DateTime($endDate_2);
                        /*echo $startDate_2.' '.$endDate_2;
                        die;*/
                        $workingHours_10 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);

                    }else{
                        $workingHours_10=0;
                    }

                    $workingHours_11 = $workingHours_9 + $workingHours_10;
                    if($record->reference_number == 'REV-2023-10737' || $record->reference_number == 'REV-2023-10756'){
                        $workingHours_11 = $workingHours_11 - 8.5;
                    }

                }else{


                    $startDate_2 = $record->instruction_date . ' ' . date('H:i:s', strtotime($record->instruction_time));
                    $endDate_2 = $record->submission_approver_date;
                    $startDateTime = new DateTime($startDate_2); // Start date and time
                    $endDateTime = new DateTime($endDate_2);
                    // print_r( abs ( number_format( Yii::$app->appHelperFunctions->checkAndExcludeWeekends( $startDate, $endDate  ), 2 ) )); echo "<br>";
                    //$time_2 = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends($startDate_2, $endDate_2), 1));
                    //$time_2_hours = abs(number_format(Yii::$app->appHelperFunctions->checkAndExcludeWeekends_hours($startDate_2, $endDate_2), 1));
                    if($record->instruction_date !='' && $record->submission_approver_date !='') {
                        $startDateTime = new DateTime($startDate_2); // Start date and time
                        $endDateTime = new DateTime($endDate_2);
                        $workingHours_9 = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                        if($record->reference_number == 'REV-2023-10772' || $record->reference_number == 'REV-2023-10756'){
                            $workingHours_9 = $workingHours_9 - 8.5;
                        }
                        $workingHours_11 = $workingHours_9;


                    }

                }
                $diff_9 = $diff_9 + $workingHours_11;


            }
        }

        return $this->render('tat_daily', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'sub_insp' => $sub_insp,
            'sub_inst' => $sub_inst,
            'insp_sub' => $insp_sub,
            'app_insp' => $app_insp,
            'custom' => $custom,
            'diff_1' => $diff_1,
            'diff_2' => $diff_2,
            'diff_3' => $diff_3,
            'diff_4' => $diff_4,
            'diff_5' => $diff_5,
            'diff_6' => $diff_6,
            'diff_7' => $diff_7,
            'diff_8' => $diff_8,
            'diff_9' => $diff_9,
            'diff_10' => $diff_10,
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
        ]);
    }


    public function actionOperationsteam()
    {
        $this->checkLogin();
        /* if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
             Yii::$app->getSession()->addFlash('error', "Permission denied!");
             return $this->redirect(['site/index']);
         }*/
        $holidays = array();
        $user=null;
        $type='Individual';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_operations_data(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];

                    $Date=(explode(" - ",$custome_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];

                  //  $allvalues = Yii::$app->appHelperFunctions->getOpertaionsValuationTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                   // $all_inspections = Yii::$app->appHelperFunctions->getOpertaionsInspectionTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);
                }
            }else{

                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
            }

        }else{
            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);

        }
        $total_valuations=0;
        $total_valuations_verifies=0;
        $total_inspections=0;
        $total_stored=0;
       // $custom = 0;
        $inspection_array=array();
        $stored_array=array();
        if(!empty($dataProvider->getModels())){
            foreach ($dataProvider->getModels() as $model_array){
                $total_valuations =  $total_valuations + $model_array->total_valuations;
                $created_by =  $model_array->created_by;
                $inspection_array[$created_by] = Yii::$app->appHelperFunctions->getOpertaionsInspectionTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw,$created_by);
                $total_inspections =  $total_inspections + $inspection_array[$created_by] ;
                $stored_array[$created_by] = Yii::$app->appHelperFunctions->getOpertaionsUnstoredTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw,$created_by);
                $total_stored = $total_stored + $stored_array[$created_by];
            }
        }
        $view = 'operations_performance';

     /*   echo "<pre>";
        print_r($inspection_array);
        die;*/


        return $this->render($view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'inspection_array' => $inspection_array,
            'total_inspections' => $total_inspections,
            'stored_array' => $stored_array,
            'total_stored' => $total_stored,
            'type' => $type,
            'custom' =>  $custom ,
            'allvaluesallvalues' => $allvalues['total_values'],
            'allvalues_count' => $total_valuations,
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
        ]);
    }
    public function actionDatateam()
    {
        $this->checkLogin();


        $data_team_members = User::find()
            ->select(['id'])
            ->where(['permission_group_id' => 38])
            ->column();


        /* if(!in_array( Yii::$app->user->identity->id, [1,14,33,6019])) {
             Yii::$app->getSession()->addFlash('error', "Permission denied!");
             return $this->redirect(['site/index']);
         }*/
        $holidays = array();
        $user=null;
        $type='Individual';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_data_team(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            if(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] == 9){
                $custom = 1;
                if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw']) && Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'] <> null ) {
                    $custome_date_btw= Yii::$app->request->queryParams['ValuationReportsSearch']['custom_date_btw'];

                    $Date=(explode(" - ",$custome_date_btw));
                    $from_date = $Date[0];
                    $to_date = $Date[1];

                    //  $allvalues = Yii::$app->appHelperFunctions->getOpertaionsValuationTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    // $all_inspections = Yii::$app->appHelperFunctions->getOpertaionsInspectionTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw);
                    $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);
                }
            }else{

                $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
                $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
            }

        }else{
            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);

        }
        $total_listings_amended=0;
        $total_listings_verified=0;
        $total_verified=0;
        $total_stored=0;
        // $custom = 0;
        $verified_array=array();
        $verified_array=array();
        $amended_array=array();
        $stored_array=array();
        if(!empty($dataProvider->getModels())){

            foreach ($dataProvider->getModels() as $model_array){

                //echo "<pre>";
                $verified_lists= Yii::$app->appHelperFunctions->getDataTeamVerifiedListings(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw,111500);
               // print_r($model_array);
               // die;

                $created_by =  $model_array->created_by;
                $verified_array[$created_by] = Yii::$app->appHelperFunctions->getDataTeamVerifiedListings(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'],$custome_date_btw,$created_by);
               if($verified_array[$created_by] > 0){
                   $verified_array[$created_by] = round($verified_array[$created_by]/2);
               }


                $amended_array[$created_by] = $model_array->total_listings;
                $total_listings_amended = $total_listings_amended +  $amended_array[$created_by];
                $total_listings_verified = $total_listings_verified +  $verified_array[$created_by];

            }
        }
        $view = 'datateam_performance';

        /*   echo "<pre>";
           print_r($inspection_array);
           die;*/


        return $this->render($view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'verified_array' => $verified_array,
            'amended_array' => $amended_array,
            'total_listings_amended' => $total_listings_amended,
            'total_listings_verified' => $total_listings_verified,
            'stored_array' => $stored_array,
            'total_stored' => $total_stored,
            'type' => $type,
            'custom' =>  $custom ,
            'allvaluesallvalues' => $total_listings_amended,
            'allvalues_count' => $total_listings_amended,
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
        ]);
    }

}
