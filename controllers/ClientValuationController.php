<?php

namespace app\controllers;

use Yii;
use app\models\CrmQuotations;
use app\models\CrmReceivedProperties;
use app\models\CrmReceivedDocs;
use app\models\CrmReceivedDocsFiles;
use app\models\Company;
use app\models\ClientValuation;
use app\models\ClientValuationDocs;
use app\models\ClientValuationDocsFiles;
use app\models\ReceivedDocs;
use app\models\ReceivedDocsFiles;
use app\models\Valuation;
use app\models\ValuationDetail;
use app\models\ValuationDetailData;
use app\models\ValuationApproversData;
use app\models\InspectProperty;
use app\models\ClientValuationSearch;
use app\models\ScheduleInspection;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\helpers\DefController;

/**
 * ClientValuationController implements the CRUD actions for ClientValuation model.
 */
class ClientValuationController extends DefController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }



    public function actionIndex()
    {
        $this->checkLogin();
        $model = ClientValuation::find()->all();
        $searchModel = new ClientValuationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        // return $this->render('index', [
        //     'searchModel' => $searchModel,
        //     'dataProvider' => $dataProvider,
        //     'model' => $model,
        // ]);

        if (Yii::$app->request->isPjax) {
            return $this->renderPartial('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
        } else {
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
        }
    }



    public function actionView($id)
    {
        $this->checkLogin();
        // $model = $this->findModel($id);
        // $model = ClientValuation::find()->where(['id'=>$id,'created_by'=>Yii::$app->user->identity->id]);
        $model = ClientValuation::find()
            ->where(['id' => $id])
            ->andWhere(['client_id' => Yii::$app->user->identity->company_id])
            ->one();

        $valuation = Valuation::find()->where(['client_module_id' => $id])->one();

        return $this->render('view', [
            'model' => $model,
            'valuation' => $valuation,
        ]);
    }



    public function actionCreate()
    {
        $this->checkLogin();

        $model = new ClientValuation();



        // dd(Yii::$app->request->post('key'));
        // $key = Yii::$app->request->post('key');
        // // $key = $_POST['key'];

        // $documents = ClientValuationDocs::find()->where(['key' => $key])->one();
        // if ($documents !== null) {
        // } else {
        //     $documents = new ClientValuationDocs();
        // }

        if ($model->load(Yii::$app->request->post())) {

            $key = $model->key;

            $documents = ClientValuationDocs::find()->where(['key' => $key])->one();
            if ($documents !== null) {
            } else {
                $documents = new ClientValuationDocs();
            }

            $documents->key = $key;
            $documents->property_id = $model->property_id;
            $documents->received_docs = $model->received_docs;
            // dd($documents);
            $documents->save();



            // for date format change
            if (isset($model->valuation_date) && $model->valuation_date !== "") {
                $model->valuation_date = date('Y-m-d', strtotime($model->valuation_date));
            } else {
                $model->valuation_date = "";
            }

            if (isset($model->inspection_date) && $model->inspection_date !== "") {
                $model->inspection_date = date('Y-m-d', strtotime($model->inspection_date));
            } else {
                $model->inspection_date = "";
            }

            $model->client_id = Yii::$app->user->identity->company_id;

            // $this->StatusVerify($model);



            if ($model->save()) {
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => get_class($model),
                    'action' => 'data_created',
                    'verify_field' => 'status_verified',
                ]);

                if (isset($_POST['save-btn']) && $_POST['save-btn'] == 'save') {
                    // On Save button click
                    Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                } elseif (isset($_POST['confirm-btn']) && $_POST['confirm-btn'] == 'confirm') {
                    // On Confirm button click
                } elseif (isset($_POST['verify-btn']) && $_POST['verify-btn'] == 'verify') {

                    // On Verify button click
                    $model->status_verified = 1;
                    $model->status_verified_at = date('Y-m-d H:i:s');
                    $model->status_verified_by = Yii::$app->user->identity->id;



                    if (!$model->save()) {
                        echo "<pre>";
                        print_r($model->errors);
                        die('here');
                    }

                    // if($model->client->instruction_via_quotation == 1){

                    //     $emailSubject = "Windmills Valuation Proposal - ". Yii::$app->crmQuotationHelperFunctions->uniqueReference;

                    //     $quotation = new CrmQuotations();
                    //     $quotation->reference_number = Yii::$app->crmQuotationHelperFunctions->uniqueReference;
                    //     $quotation->client_reference = $model->client_reference;
                    //     $quotation->client_name = (string)Yii::$app->user->identity->company_id;
                    //     $quotation->client_urgency = $model->urgency;
                    //     $quotation->instruction_date = date('Y-m-d');
                    //     $quotation->inquiry_received_time = date('H:m');
                    //     $quotation->inquiry_received_date = date('Y-m-d H:m:s');
                    //     $quotation->status_change_date = date('Y-m-d H:m:s');
                    //     $quotation->purpose_of_valuation = $model->purpose_of_valuation;
                    //     $quotation->inspection_type = $model->inspection_type;
                    //     $quotation->quotation_status = 0;
                    //     $quotation->status = 1;
                    //     $quotation->no_of_properties = 1;
                    //     $quotation->valuer_id = 21;
                    //     $quotation->valuation_date = $model->valuation_date;
                    //     $quotation->email_subject = $emailSubject;
                    //     $quotation->client_module_id = $model->id;
                    //     $quotation->trustee_id = Yii::$app->user->identity->company_id;
                    //     $quotation->type_of_service = 1;

                    //     // dd($quotation);


                    //     if (!$quotation->save()) {
                    //         echo "<pre>";
                    //         print_r($quotation->errors);
                    //         die('here');

                    //     } else{

                    //         $receivedProperty = new CrmReceivedProperties();
                    //         $receivedProperty->quotation_id = $quotation->id;
                    //         $receivedProperty->property_index = 0;
                    //         $receivedProperty->building_info = ($model->building_info <> null) ? $model->building_info : 10045;
                    //         $receivedProperty->building_number = $model->building_number;
                    //         $receivedProperty->community = $model->community;
                    //         $receivedProperty->sub_community = $model->sub_community;
                    //         $receivedProperty->city = $model->city;
                    //         $receivedProperty->plot_number = $model->plot_number;
                    //         $receivedProperty->street = $model->street;
                    //         $receivedProperty->property_id = $model->property_id;
                    //         $receivedProperty->property_category = $model->property_category;
                    //         $receivedProperty->tenure = $model->tenure;
                    //         $receivedProperty->unit_number = ($model->unit_number <> null) ? (string)$model->unit_number : "0";
                    //         $receivedProperty->floor_number = $model->floor_number;
                    //         $receivedProperty->no_of_units_value = $model->number_of_units;
                    //         $receivedProperty->built_up_area = ($model->built_up_area <> null) ? $model->built_up_area : 0 ;
                    //         $receivedProperty->net_leasable_area = ($model->net_leasable_area <> null) ? $model->net_leasable_area : 0 ;
                    //         $receivedProperty->land_size = ($model->plot_area <> null) ? $model->plot_area : 0 ;
                    //         $receivedProperty->complexity = strtolower($model->development_type);
                    //         $receivedProperty->ready = ($model->completion_status == 1) ? "Yes" : "No";

                    //         if (!$receivedProperty->save()) {
                    //             echo "<pre>";
                    //             print_r($receivedProperty->errors);
                    //             die('here');

                    //         }

                    //         // notification mail to wm
                    //         if ($model->email_status_wm != 1) {

                    //             if($model->urgency == 1){ $urgency = 'Urgent'; }
                    //             else{ $urgency = 'Normal'; }

                    //             $clientName = Company::find()
                    //                         ->select(['title'])
                    //                         ->where(['id' => Yii::$app->user->identity->company_id])
                    //                         ->scalar();

                    //             $ownerName = $model->client_customer_fname.' '.$model->client_customer_lname;
                    //             $applicantName = Yii::$app->user->identity->firstname.' '.Yii::$app->user->identity->lastname;
                    //             $clientContactName = $model->contact_person_fname.' '.$model->contact_person_lname;
                    //             $clientContactNumber = $model->contact_person_phone_code.$model->contact_person_phone;


                    //             $notifyData = [
                    //                 'client' => Yii::$app->user->identity->company_id,
                    //                 'attachments' => [],
                    //                 'subject' => $emailSubject,
                    //                 'uid' => $model->id,
                    //                 'replacements' => [
                    //                     '{urgency}' => $urgency,
                    //                     '{clientName}' => ucwords($clientName),
                    //                     '{ownerName}' => ucwords($ownerName),
                    //                     '{applicantName}' => ucwords($applicantName),
                    //                     '{clientContactName}' => $clientContactName,
                    //                     '{clientContactNumber}' => $clientContactNumber,
                    //                     '{propertyType}' => $model->property->title,
                    //                     '{valuationPurpose}' => Yii::$app->appHelperFunctions->purposeOfValuationArr[$model->purpose_of_valuation],
                    //                 ],
                    //             ];

                    //             // if (model->client_reference <> null) {

                    //                 \app\modules\wisnotify\listners\NotifyEvent::fire1('Received.Client.Valuation', $notifyData);

                    //                 Yii::$app->db->createCommand()->update('client_valuation', ['email_status_wm' => 1], 'id=' . $model->id . '')->execute();
                    //             // }
                    //         }

                    //     }

                    //     // $clientValuationDocs = ClientValuationDocs::find()->where(['valuation_id' => $id, 'property_id' => $model->property_id])->one();

                    //     // $received_docs = \app\models\ClientValuationDocsFiles::find()->where(['valuation_id' => $id])->asArray()->all();

                    //     $clientValuationDocs = ClientValuationDocs::find()->where(['key' => $key, 'property_id' => $model->property_id])->one();

                    //     $received_docs = \app\models\ClientValuationDocsFiles::find()->where(['key' => $key])->asArray()->all();

                    //     $crmReceivedDoc = new CrmReceivedDocs();
                    //     unset($crmReceivedDoc->id);
                    //     $crmReceivedDoc->setAttributes($clientValuationDocs->attributes);
                    //     $crmReceivedDoc->quotation_id = $quotation->id;
                    //     $crmReceivedDoc->property_index = 0;
                    //     $crmReceivedDoc->sendDocsEmail = false;
                    //     // $crmReceivedDoc->save();
                    //     if (!$crmReceivedDoc->save()) {
                    //         echo "<pre>";
                    //         print_r($crmReceivedDoc->errors);
                    //         die;

                    //     }

                    //     // Save all Documents terms
                    //     foreach ($received_docs as $received_doc) {
                    //         $received_data_detail = new CrmReceivedDocsFiles();
                    //         $received_data_detail->document_id = $received_doc['document_id'];
                    //         $received_data_detail->attachment = $received_doc['attachment'];
                    //         $received_data_detail->quotation_id = $quotation->id;
                    //         $received_data_detail->property_index = 0;
                    //         $received_data_detail->save();
                    //     }

                    //     Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Valuation verified successfully'));
                    //     return $this->redirect(['index']);
                    //     // dd($quotation);

                    // }
                    // else{

                    $emailSubject = "Valuation Instruction, Client Reference: " . $model->client_reference;

                    // valuation
                    $valuation = new Valuation();
                    $valuation->reference_number = Yii::$app->appHelperFunctions->uniqueReference;
                    $valuation->client_id = Yii::$app->user->identity->company_id;
                    $valuation->instruction_date = date('Y-m-d');
                    $valuation->instruction_time = date('H:m');
                    $valuation->purpose_of_valuation = $model->purpose_of_valuation;
                    $valuation->inspection_type = $model->inspection_type;
                    $valuation->client_reference = $model->client_reference;
                    $valuation->unit_number = ($model->unit_number <> null) ? $model->unit_number : 0;
                    $valuation->prefix_customer_name = $model->client_customer_prefix;
                    $valuation->client_name_passport = $model->client_customer_fname;
                    $valuation->client_lastname_passport = $model->client_customer_lname;
                    $valuation->building_info = ($model->building_info <> null) ? $model->building_info : 10045;
                    $valuation->community = $model->community;
                    $valuation->sub_community = $model->sub_community;
                    $valuation->city = $model->city;
                    $valuation->property_id = $model->property_id;
                    $valuation->property_category = $model->property_category;
                    $valuation->tenure = $model->tenure;
                    $valuation->building_number = $model->building_number;
                    $valuation->plot_number = $model->plot_number;
                    $valuation->street = $model->street;
                    $valuation->floor_number = $model->floor_number;
                    $valuation->prefix_instructing_person = $model->instructing_person_prefix;
                    $valuation->firstname_instructing_person = $model->instructing_person_fname;
                    $valuation->lastname_instructing_person = $model->instructing_person_lname;
                    $valuation->mobile_instructing_person = $model->instructing_person_phone_code . $model->instructing_person_phone;
                    $valuation->email_instructing_person = $model->instructing_person_email;
                    $valuation->prefix = $model->contact_person_prefix;
                    $valuation->contact_person_name = $model->contact_person_fname;
                    $valuation->contact_person_lastname = $model->contact_person_lname;
                    $valuation->phone_code = $model->contact_person_phone_code;
                    $valuation->contact_phone_no = $model->contact_person_phone;
                    $valuation->land_line_code = $model->contact_person_landline_code;
                    $valuation->land_line_no = $model->contact_person_landline;
                    $valuation->contact_email = $model->contact_person_email;
                    $valuation->urgency = $model->urgency;
                    $valuation->valuation_status = "1";
                    $valuation->service_officer_name = 142;
                    $valuation->land_size = 0;
                    $valuation->email_subject = $emailSubject;
                    $valuation->client_module_id = $model->id;
                    $valuation->trustee_id = Yii::$app->user->identity->company_id;

                    $valuation->valuation_approach = Yii::$app->appHelperFunctions->getValuationApproachByProperty($model->property_id);

                    // dd($valuation);
                    if (!$valuation->save()) {
                        echo "<pre>";
                        print_r($valuation->errors);
                        die('here');
                    } else {

                        // valuation details
                        $valuationDetail = new ValuationDetail();
                        $valuationDetail->building_info = ($model->building_info <> null) ? $model->building_info : 10045;
                        $valuationDetail->building_number = ($model->building_number <> null) ? "$model->building_number" : "0";
                        $valuationDetail->plot_number = $model->plot_number;
                        $valuationDetail->street = $model->street;
                        $valuationDetail->community = $model->community;
                        $valuationDetail->sub_community = $model->sub_community;
                        $valuationDetail->city = $model->city;
                        $valuationDetail->country = 0;
                        $valuationDetail->property_id = $model->property_id;
                        $valuationDetail->property_category = $model->property_category;
                        $valuationDetail->tenure = $model->tenure;
                        $valuationDetail->unit_number = $model->unit_number;
                        $valuationDetail->location_pin = $model->location_pin;
                        $valuationDetail->floor_number = $model->floor_number;
                        $valuationDetail->built_up_area = ($model->built_up_area <> null) ? $model->built_up_area : 0;
                        $valuationDetail->land_size = ($model->plot_area <> null) ? $model->plot_area : 0;
                        $valuationDetail->valuation_id = $valuation->id;
                        $valuationDetail->status = null;

                        if (!$valuationDetail->save()) {
                            echo "<pre>";
                            print_r($valuationDetail->errors);
                            die('here');
                        }


                        // valuation detail data
                        $valuationDetailData = new ValuationDetailData();
                        $valuationDetailData->valuation_date = ($model->valuation_date !== null) ? $model->valuation_date : null;
                        $valuationDetailData->purpose_of_valuation = $model->purpose_of_valuation;
                        $valuationDetailData->valuation_id = $valuation->id;

                        if (!$valuationDetailData->save()) {
                            echo "<pre>";
                            print_r($valuationDetailData->errors);
                            die('here');
                        }

                        // preferred inspection date time
                        if ($model->inspection_date <> "" || $model->inspection_time <> "") {
                            $preferredInspection = new ScheduleInspection();
                            $preferredInspection->inspection_date = ($model->inspection_date <> null) ? $model->inspection_date : '';
                            $preferredInspection->inspection_time = ($model->inspection_time <> null) ? $model->inspection_time : '00:00';
                            $preferredInspection->valuation_id = $valuation->id;
                            $preferredInspection->inspection_officer = 142;
                            if (!$preferredInspection->save()) {
                                echo "<pre>";
                                print_r($preferredInspection->errors);
                                die('here');
                            }
                        }


                        if ($model->email_status_wm != 1) {

                            if ($model->urgency == 1) {
                                $urgency = 'Urgent';
                            } else {
                                $urgency = 'Normal';
                            }

                            $clientName = Company::find()
                                ->select(['title'])
                                ->where(['id' => Yii::$app->user->identity->company_id])
                                ->scalar();

                            $ownerName = $model->client_customer_fname . ' ' . $model->client_customer_lname;
                            $applicantName = Yii::$app->user->identity->firstname . ' ' . Yii::$app->user->identity->lastname;
                            $clientContactName = $model->contact_person_fname . ' ' . $model->contact_person_lname;
                            $clientContactNumber = $model->contact_person_phone_code . $model->contact_person_phone;


                            $notifyData = [
                                'client' => Yii::$app->user->identity->company_id,
                                'attachments' => [],
                                'subject' => $emailSubject,
                                'uid' => $model->id,
                                'replacements' => [
                                    '{urgency}' => $urgency,
                                    '{clientName}' => ucwords($clientName),
                                    '{ownerName}' => ucwords($ownerName),
                                    '{applicantName}' => ucwords($applicantName),
                                    '{clientContactName}' => $clientContactName,
                                    '{clientContactNumber}' => $clientContactNumber,
                                    '{propertyType}' => $model->property->title,
                                    '{valuationPurpose}' => Yii::$app->appHelperFunctions->purposeOfValuationArr[$model->purpose_of_valuation],
                                ],
                            ];

                            // if (model->client_reference <> null) {

                            \app\modules\wisnotify\listners\NotifyEvent::fire1('Received.Client.Valuation', $notifyData);

                            Yii::$app->db->createCommand()->update('client_valuation', ['email_status_wm' => 1], 'id=' . $model->id . '')->execute();
                            // }
                        }
                    }

                    // $clientValuationDocs = ClientValuationDocs::find()->where(['valuation_id' => $id, 'property_id' => $model->property_id])->one();
                    $clientValuationDocs = ClientValuationDocs::find()->where(['key' => $key, 'property_id' => $model->property_id])->one();

                    // $received_docs = \app\models\ClientValuationDocsFiles::find()->where(['valuation_id' => $id])->asArray()->all();
                    $received_docs = \app\models\ClientValuationDocsFiles::find()->where(['key' => $key])->asArray()->all();
                    $step_2 = new ReceivedDocs();
                    unset($step_2->id);
                    $step_2->setAttributes($clientValuationDocs->attributes);
                    $step_2->valuation_id = $valuation->id;
                    // $step_2->save();
                    if (!$step_2->save()) {
                        echo "<pre>";
                        print_r($step_2->errors);
                        die;
                    }

                    // Save all Documents terms
                    foreach ($received_docs as $received_doc) {
                        $received_data_detail = new ReceivedDocsFiles();
                        $received_data_detail->document_id = $received_doc['document_id'];
                        $received_data_detail->attachment = $received_doc['attachment'];
                        $received_data_detail->doc_insert_date = $received_doc['doc_insert_date'];
                        $received_data_detail->valuation_id = $valuation->id;
                        $received_data_detail->save();
                    }

                    Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Valuation verified successfully'));
                    return $this->redirect(['index']);

                    // Yii::$app->controller->renderPartial('/client-valuation/proforma-invoice?id='.$valuation->id);

                    // }

                }


                return $this->redirect(['update?id=' . $model->id]);
            }
        }

        return $this->render('new_valuation_inquiry', [
            'model' => $model,
        ]);
    }



    public function actionUpdate($id)
    {
        $this->checkLogin();

        $model = $this->findModel($id);
        $old_verify_status = $model->status_verified;

        if ($model->status_verified == 1) {
            return $this->redirect(['index']);
        }

        // dd(Yii::$app->user->identity->firstname);

        if ($model->load(Yii::$app->request->post())) {

            // dd($model);
            $key = $model->key;

            $documents = ClientValuationDocs::find()->where(['key' => $key])->one();
            if ($documents !== null) {
            } else {
                $documents = new ClientValuationDocs();
            }

            $documents->key = $key;
            $documents->valuation_id = $id;
            $documents->property_id = $model->property_id;
            $documents->received_docs = $model->received_docs;
            $documents->save();


            // for date format change
            if (isset($model->valuation_date) && $model->valuation_date !== "") {
                $model->valuation_date = date('Y-m-d', strtotime($model->valuation_date));
            } else {
                $model->valuation_date = "";
            }

            if (isset($model->inspection_date) && $model->inspection_date !== "") {
                $model->inspection_date = date('Y-m-d', strtotime($model->inspection_date));
            } else {
                $model->inspection_date = "";
            }

            // $this->StatusVerify($model);

            if ($model->save()) {
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => get_class($model),
                    'action' => 'data_updated',
                    'verify_field' => 'status_verified',
                    'old_verify_status' => $old_verify_status,
                ]);

                if (isset($_POST['save-btn']) && $_POST['save-btn'] == 'save') {
                    // On Save button click
                    Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                } else if (isset($_POST['confirm-btn']) && $_POST['confirm-btn'] == 'confirm') {
                    // On Confirm button click
                } else if (isset($_POST['verify-btn']) && $_POST['verify-btn'] == 'verify') {

                    // On Verify button click
                    $model->status_verified = 1;
                    $model->status_verified_at = date('Y-m-d H:i:s');
                    $model->status_verified_by = Yii::$app->user->identity->id;



                    if (!$model->save()) {
                        echo "<pre>";
                        print_r($model->errors);
                        die('here');
                    }


                    // if($model->client->instruction_via_quotation == 1){

                    //     $emailSubject = "Windmills Valuation Proposal - ". Yii::$app->crmQuotationHelperFunctions->uniqueReference;

                    //     $quotation = new CrmQuotations();
                    //     $quotation->reference_number = Yii::$app->crmQuotationHelperFunctions->uniqueReference;
                    //     $quotation->client_reference = $model->client_reference;
                    //     $quotation->client_name = (string)Yii::$app->user->identity->company_id;
                    //     $quotation->client_urgency = $model->urgency;
                    //     $quotation->instruction_date = date('Y-m-d');
                    //     $quotation->inquiry_received_time = date('H:m');
                    //     $quotation->inquiry_received_date = date('Y-m-d H:m:s');
                    //     $quotation->status_change_date = date('Y-m-d H:m:s');
                    //     $quotation->purpose_of_valuation = $model->purpose_of_valuation;
                    //     $quotation->inspection_type = $model->inspection_type;
                    //     $quotation->quotation_status = 0;
                    //     $quotation->status = 1;
                    //     $quotation->no_of_properties = 1;
                    //     $quotation->valuer_id = 21;
                    //     $quotation->valuation_date = $model->valuation_date;
                    //     $quotation->email_subject = $emailSubject;
                    //     $quotation->client_module_id = $model->id;
                    //     $quotation->trustee_id = Yii::$app->user->identity->company_id;
                    //     $quotation->type_of_service = 1;

                    //     // dd($quotation);


                    //     if (!$quotation->save()) {
                    //         echo "<pre>";
                    //         print_r($quotation->errors);
                    //         die('here');

                    //     } else{

                    //         $receivedProperty = new CrmReceivedProperties();
                    //         $receivedProperty->quotation_id = $quotation->id;
                    //         $receivedProperty->property_index = 0;
                    //         $receivedProperty->building_info = ($model->building_info <> null) ? $model->building_info : 10045;
                    //         $receivedProperty->building_number = $model->building_number;
                    //         $receivedProperty->community = $model->community;
                    //         $receivedProperty->sub_community = $model->sub_community;
                    //         $receivedProperty->city = $model->city;
                    //         $receivedProperty->plot_number = $model->plot_number;
                    //         $receivedProperty->street = $model->street;
                    //         $receivedProperty->property_id = $model->property_id;
                    //         $receivedProperty->property_category = $model->property_category;
                    //         $receivedProperty->tenure = $model->tenure;
                    //         $receivedProperty->unit_number = ($model->unit_number <> null) ? (string)$model->unit_number : "0";
                    //         $receivedProperty->floor_number = $model->floor_number;
                    //         $receivedProperty->no_of_units_value = $model->number_of_units;
                    //         $receivedProperty->built_up_area = ($model->built_up_area <> null) ? $model->built_up_area : 0 ;
                    //         $receivedProperty->net_leasable_area = ($model->net_leasable_area <> null) ? $model->net_leasable_area : 0 ;
                    //         $receivedProperty->land_size = ($model->plot_area <> null) ? $model->plot_area : 0 ;
                    //         $receivedProperty->complexity = strtolower($model->development_type);
                    //         $receivedProperty->ready = ($model->completion_status == 1) ? "Yes" : "No";

                    //         if (!$receivedProperty->save()) {
                    //             echo "<pre>";
                    //             print_r($receivedProperty->errors);
                    //             die('here');

                    //         }

                    //         // notification mail to wm
                    //         if ($model->email_status_wm != 1) {

                    //             if($model->urgency == 1){ $urgency = 'Urgent'; }
                    //             else{ $urgency = 'Normal'; }

                    //             $clientName = Company::find()
                    //                         ->select(['title'])
                    //                         ->where(['id' => Yii::$app->user->identity->company_id])
                    //                         ->scalar();

                    //             $ownerName = $model->client_customer_fname.' '.$model->client_customer_lname;
                    //             $applicantName = Yii::$app->user->identity->firstname.' '.Yii::$app->user->identity->lastname;
                    //             $clientContactName = $model->contact_person_fname.' '.$model->contact_person_lname;
                    //             $clientContactNumber = $model->contact_person_phone_code.$model->contact_person_phone;


                    //             $notifyData = [
                    //                 'client' => Yii::$app->user->identity->company_id,
                    //                 'attachments' => [],
                    //                 'subject' => $emailSubject,
                    //                 'uid' => $model->id,
                    //                 'replacements' => [
                    //                     '{urgency}' => $urgency,
                    //                     '{clientName}' => ucwords($clientName),
                    //                     '{ownerName}' => ucwords($ownerName),
                    //                     '{applicantName}' => ucwords($applicantName),
                    //                     '{clientContactName}' => $clientContactName,
                    //                     '{clientContactNumber}' => $clientContactNumber,
                    //                     '{propertyType}' => $model->property->title,
                    //                     '{valuationPurpose}' => Yii::$app->appHelperFunctions->purposeOfValuationArr[$model->purpose_of_valuation],
                    //                 ],
                    //             ];

                    //             // if (model->client_reference <> null) {

                    //                 \app\modules\wisnotify\listners\NotifyEvent::fire1('Received.Client.Valuation', $notifyData);

                    //                 Yii::$app->db->createCommand()->update('client_valuation', ['email_status_wm' => 1], 'id=' . $model->id . '')->execute();
                    //             // }
                    //         }

                    //     }

                    //     $clientValuationDocs = ClientValuationDocs::find()->where(['valuation_id' => $id, 'property_id' => $model->property_id])->one();

                    //     $received_docs = \app\models\ClientValuationDocsFiles::find()->where(['valuation_id' => $id])->asArray()->all();

                    //     $crmReceivedDoc = new CrmReceivedDocs();
                    //     unset($crmReceivedDoc->id);
                    //     $crmReceivedDoc->setAttributes($clientValuationDocs->attributes);
                    //     $crmReceivedDoc->quotation_id = $quotation->id;
                    //     $crmReceivedDoc->property_index = 0;
                    //     $crmReceivedDoc->sendDocsEmail = false;
                    //     // $crmReceivedDoc->save();
                    //     if (!$crmReceivedDoc->save()) {
                    //         echo "<pre>";
                    //         print_r($crmReceivedDoc->errors);
                    //         die;

                    //     }

                    //     // Save all Documents terms
                    //     foreach ($received_docs as $received_doc) {
                    //         $received_data_detail = new CrmReceivedDocsFiles();
                    //         $received_data_detail->document_id = $received_doc['document_id'];
                    //         $received_data_detail->attachment = $received_doc['attachment'];
                    //         $received_data_detail->quotation_id = $quotation->id;
                    //         $received_data_detail->property_index = 0;
                    //         $received_data_detail->save();
                    //     }

                    //     Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Valuation verified successfully'));
                    //     return $this->redirect(['index']);

                    //     // dd($quotation);

                    // }
                    // else{

                    $emailSubject = "Valuation Instruction, Client Reference: " . $model->client_reference;

                    // valuation
                    $valuation = new Valuation();
                    $valuation->reference_number = Yii::$app->appHelperFunctions->uniqueReference;
                    $valuation->client_id = Yii::$app->user->identity->company_id;
                    $valuation->instruction_date = date('Y-m-d');
                    $valuation->instruction_time = date('H:m');
                    $valuation->purpose_of_valuation = $model->purpose_of_valuation;
                    $valuation->inspection_type = $model->inspection_type;
                    $valuation->client_reference = $model->client_reference;
                    $valuation->unit_number = ($model->unit_number <> null) ? $model->unit_number : 0;
                    $valuation->prefix_customer_name = $model->client_customer_prefix;
                    $valuation->client_name_passport = $model->client_customer_fname;
                    $valuation->client_lastname_passport = $model->client_customer_lname;
                    $valuation->building_info = ($model->building_info <> null) ? $model->building_info : 10045;
                    $valuation->community = $model->community;
                    $valuation->sub_community = $model->sub_community;
                    $valuation->city = $model->city;
                    $valuation->property_id = $model->property_id;
                    $valuation->property_category = $model->property_category;
                    $valuation->tenure = $model->tenure;
                    $valuation->building_number = $model->building_number;
                    $valuation->plot_number = $model->plot_number;
                    $valuation->street = $model->street;
                    $valuation->floor_number = $model->floor_number;
                    $valuation->prefix_instructing_person = $model->instructing_person_prefix;
                    $valuation->firstname_instructing_person = $model->instructing_person_fname;
                    $valuation->lastname_instructing_person = $model->instructing_person_lname;
                    $valuation->mobile_instructing_person = $model->instructing_person_phone_code . $model->instructing_person_phone;
                    $valuation->email_instructing_person = $model->instructing_person_email;
                    $valuation->prefix = $model->contact_person_prefix;
                    $valuation->contact_person_name = $model->contact_person_fname;
                    $valuation->contact_person_lastname = $model->contact_person_lname;
                    $valuation->phone_code = $model->contact_person_phone_code;
                    $valuation->contact_phone_no = $model->contact_person_phone;
                    $valuation->land_line_code = $model->contact_person_landline_code;
                    $valuation->land_line_no = $model->contact_person_landline;
                    $valuation->contact_email = $model->contact_person_email;
                    $valuation->urgency = $model->urgency;
                    $valuation->valuation_status = "1";
                    $valuation->service_officer_name = 142;
                    $valuation->land_size = 0;
                    $valuation->email_subject = $emailSubject;
                    $valuation->client_module_id = $model->id;
                    $valuation->trustee_id = Yii::$app->user->identity->company_id;

                    $valuation->valuation_approach = Yii::$app->appHelperFunctions->getValuationApproachByProperty($model->property_id);

                    // dd($valuation);
                    if (!$valuation->save()) {
                        echo "<pre>";
                        print_r($valuation->errors);
                        die('here');
                    } else {

                        // valuation details
                        $valuationDetail = new ValuationDetail();
                        $valuationDetail->building_info = ($model->building_info <> null) ? $model->building_info : 10045;
                        $valuationDetail->building_number = ($model->building_number <> null) ? "$model->building_number" : "0";
                        $valuationDetail->plot_number = $model->plot_number;
                        $valuationDetail->street = $model->street;
                        $valuationDetail->community = $model->community;
                        $valuationDetail->sub_community = $model->sub_community;
                        $valuationDetail->city = $model->city;
                        $valuationDetail->country = 0;
                        $valuationDetail->property_id = $model->property_id;
                        $valuationDetail->property_category = $model->property_category;
                        $valuationDetail->tenure = $model->tenure;
                        $valuationDetail->unit_number = $model->unit_number;
                        $valuationDetail->location_pin = $model->location_pin;
                        $valuationDetail->floor_number = $model->floor_number;
                        $valuationDetail->built_up_area = ($model->built_up_area <> null) ? $model->built_up_area : 0;
                        $valuationDetail->land_size = ($model->plot_area <> null) ? $model->plot_area : 0;
                        $valuationDetail->valuation_id = $valuation->id;
                        $valuationDetail->status = null;

                        if (!$valuationDetail->save()) {
                            echo "<pre>";
                            print_r($valuationDetail->errors);
                            die('here');
                        }


                        // valuation detail data
                        $valuationDetailData = new ValuationDetailData();
                        $valuationDetailData->valuation_date = ($model->valuation_date !== null) ? $model->valuation_date : null;
                        $valuationDetailData->purpose_of_valuation = $model->purpose_of_valuation;
                        $valuationDetailData->valuation_id = $valuation->id;

                        if (!$valuationDetailData->save()) {
                            echo "<pre>";
                            print_r($valuationDetailData->errors);
                            die('here');
                        }

                        // preferred inspection date time
                        if ($model->inspection_date <> "" || $model->inspection_time <> "") {
                            $preferredInspection = new ScheduleInspection();
                            $preferredInspection->inspection_date = ($model->inspection_date <> null) ? $model->inspection_date : '';
                            $preferredInspection->inspection_time = ($model->inspection_time <> null) ? $model->inspection_time : '00:00';
                            $preferredInspection->valuation_id = $valuation->id;
                            $preferredInspection->inspection_officer = 142;
                            if (!$preferredInspection->save()) {
                                echo "<pre>";
                                print_r($preferredInspection->errors);
                                die('here');
                            }
                        }


                        // notification mail to wm
                        if ($model->email_status_wm != 1) {

                            if ($model->urgency == 1) {
                                $urgency = 'Urgent';
                            } else {
                                $urgency = 'Normal';
                            }

                            $clientName = Company::find()
                                ->select(['title'])
                                ->where(['id' => Yii::$app->user->identity->company_id])
                                ->scalar();

                            $ownerName = $model->client_customer_fname . ' ' . $model->client_customer_lname;
                            $applicantName = Yii::$app->user->identity->firstname . ' ' . Yii::$app->user->identity->lastname;
                            $clientContactName = $model->contact_person_fname . ' ' . $model->contact_person_lname;
                            $clientContactNumber = $model->contact_person_phone_code . $model->contact_person_phone;


                            $notifyData = [
                                'client' => Yii::$app->user->identity->company_id,
                                'attachments' => [],
                                'subject' => $emailSubject,
                                'uid' => $model->id,
                                'replacements' => [
                                    '{urgency}' => $urgency,
                                    '{clientName}' => ucwords($clientName),
                                    '{ownerName}' => ucwords($ownerName),
                                    '{applicantName}' => ucwords($applicantName),
                                    '{clientContactName}' => $clientContactName,
                                    '{clientContactNumber}' => $clientContactNumber,
                                    '{propertyType}' => $model->property->title,
                                    '{valuationPurpose}' => Yii::$app->appHelperFunctions->purposeOfValuationArr[$model->purpose_of_valuation],
                                ],
                            ];

                            // if (model->client_reference <> null) {

                            \app\modules\wisnotify\listners\NotifyEvent::fire1('Received.Client.Valuation', $notifyData);

                            Yii::$app->db->createCommand()->update('client_valuation', ['email_status_wm' => 1], 'id=' . $model->id . '')->execute();
                            // }
                        }
                    }

                    $clientValuationDocs = ClientValuationDocs::find()->where(['valuation_id' => $id, 'property_id' => $model->property_id])->one();

                    $received_docs = \app\models\ClientValuationDocsFiles::find()->where(['valuation_id' => $id])->asArray()->all();
                    $step_2 = new ReceivedDocs();
                    unset($step_2->id);
                    $step_2->setAttributes($clientValuationDocs->attributes);
                    $step_2->valuation_id = $valuation->id;
                    // $step_2->save();
                    if (!$step_2->save()) {
                        echo "<pre>";
                        print_r($step_2->errors);
                        die;
                    }

                    // Save all Documents terms
                    foreach ($received_docs as $received_doc) {
                        $received_data_detail = new ReceivedDocsFiles();
                        $received_data_detail->document_id = $received_doc['document_id'];
                        $received_data_detail->attachment = $received_doc['attachment'];
                        $received_data_detail->doc_insert_date = $received_doc['doc_insert_date'];
                        $received_data_detail->valuation_id = $valuation->id;
                        $received_data_detail->save();
                    }



                    Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Valuation verified successfully'));
                    return $this->redirect(['index']);

                    // Yii::$app->controller->renderPartial('/client-valuation/proforma-invoice?id='.$valuation->id);
                    // }

                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ClientValuation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ClientValuation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClientValuation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ClientValuation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionUpdateVariable()
    {

        return $this->render('update_variable');
    }

    public function actionClientvaluationDocuments()
    {


        $propertyId = $_POST['propertyId'];
        $key = $_POST['key'];
        $extended = $_POST['extended'];
        $completionStatus = $_POST['completionStatus'];
        $city = $_POST['city'];
        $numberOfUnits = $_POST['numberOfUnits'];

        // dd($propertyId,$extended,$completionStatus,$city,$numberOfUnits,$key);

        $client_type = Yii::$app->user->identity->company->client_type;

        // $valuation = $this->findModel($id);
        $valuation = ClientValuation::find()->where(["key" => $key])->one();

        $propertyRequiredDocuments = Yii::$app->appHelperFunctions->getPropertyRequiredDocuments($propertyId);
        $propertyOptionalDocuments = Yii::$app->appHelperFunctions->getPropertyOptionalDocuments($propertyId);

        if (isset($propertyRequiredDocuments) && ($propertyRequiredDocuments <> null)) {
            $required_documents = explode(',', $propertyRequiredDocuments);
        }
        if (isset($propertyOptionalDocuments) && ($propertyOptionalDocuments <> null)) {
            $optional_documents = explode(',', $propertyOptionalDocuments);
        }

        $unit_row = 0;

        if ($valuation->status_verified == 1) {
            $disabled = "disabled-link";
        } else {
            $disabled = '';
        }

        if ($required_documents <> null && !empty($required_documents)) {
            $html = '';
            $html .= '<table id="requestTypes" class="table table-striped table-bordered table-hover images-table">
                    <thead>
                    <tr>
                        <td>Description</td>
                        <td>Attachment</td>
                        <td>Date</td>
                        
                    </tr>
                    </thead>
                    <tbody>';



            foreach ($required_documents as $required_document) {

                if ($required_document == 60) {
                    if ($client_type !== "corporate" && $client_type !== "individual") {
                        continue;
                    }
                }
                if ($required_document == 73) {
                    if ($client_type !== "bank") {
                        continue;
                    }
                }
                if ($required_document == 58 || $required_document == 59) {
                    if ($extended == "No") {
                        continue;
                    }
                }
                if ($required_document == 61) {
                    if ($completionStatus == 1) {
                        continue;
                    }
                }
                if ($required_document == 50) {
                    if ($city != "Dubai") {
                        continue;
                    }
                }
                if ($required_document == 11) {
                    if (in_array($propertyId, [20, 46]) && $completionStatus == 1) {
                        continue;
                    }
                }


                if ($required_document == 1 || $required_document == 6) {
                    continue;
                }





                if ($valuation->key <> null) {
                    $attachment_Details = \app\models\ClientValuationDocsFiles::find()->where(["key" => $valuation->key, "document_id" => $required_document])->one();
                }
                //$attachment_Details = \app\models\ClientValuationDocsFiles::find()->where([ "key" => $valuation->key, "document_id" => $required_document])->one();
                // echo "<pre>"; print_r($attachment_Details); echo "</pre>"; die;
                $doc_insert_date = '';
                if ($attachment_Details->attachment <> null) {
                    if ($attachment_Details->doc_insert_date <> null) {
                        $doc_insert_date = date('d F Y', strtotime($attachment_Details->doc_insert_date));
                    }
                }
                // dd($attachment_Details->id);
                $attachment = $attachment_Details->attachment;







                $html .= '<tr id="image-row' . $unit_row . '" class="required-documents">';

                $html .= '<td class="text-left">
                                    <div class="form-group required">
                                        <label class="control-label">'
                    . Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document] .
                    '</label>
                                        <div class="help-block2" style="display:none"></div>
                                    </div>
                                  </td>';

                $html .= '<td class="text-left upload-docs">
                                    <div class="form-group">
                                        <a href="javascript:;"
                                            id="upload-document' . $unit_row . '"
                                            onclick="uploadAttachment(' . $unit_row . ')"
                                            data-toggle="tooltip"
                                            class="img-thumbnail ' . $disabled . '"
                                            title="Upload Document">';


                if ($attachment <> null) {

                    $attachment_link= $attachment;
                    $explode_attach_doc = explode('received-info/',$attachment);

                    $explode_attach = explode('https://winsmills-wiz-prod.s3.eu-central-1.amazonaws.com/', $attachment);
                    if (isset($explode_attach[1])) {
                        $attachment_link = Yii::$app->get('s3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                    }else{
                        $explode_attach = explode('https://maxclientmodule.s3.ap-southeast-1.amazonaws.com/', $attachment);
                        $attachment_link = Yii::$app->get('s3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                    }

                    if(isset($explode_attach_doc[1])) {
                        $attachment_src = 'https://client.windmillsgroup.com/cache/'.$explode_attach_doc[1];
                    }else{
                        $attachment_src = Yii::$app->params['uploadDocsIcon'];
                    }

                    if (strpos($attachment, '.pdf') !== false) {

                        $html .= '<img src="' . Yii::$app->params['uploadPdfIcon'] . '"
                                                                id="deleted-' . $attachment_Details->id . '"
                                                                alt="" title=""
                                                                data-placeholder="no_image.png"/>
                                                                <a href="' . $attachment_link . '" target="_blank">
                                                                        <span class="glyphicon glyphicon-eye-open"></span>
                                                                </a>
                                                                <a href="javascript:;" id="del-btn-' . $attachment_Details->id . '" class="' . $disabled . '">
                                                                    <span class="glyphicon glyphicon-trash text-danger delete-file"
                                                                            id="' . $attachment_Details->id . '"></span>
                                                                </a>';
                    } else if (strpos($attachment, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {

                        $html .= '<img src="' . Yii::$app->params['uploadDocsIcon'] . '"
                                                                id="deleted-' . $attachment_Details->id . '"
                                                                alt="" title=""
                                                                data-placeholder="no_image.png"/>
                                                                <a href="' . $attachment_link . '"
                                                                    target="_blank">
                                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                                </a>
                                                                <a href="javascript:;"
                                                                    id="del-btn-' . $attachment_Details->id . '" class="' . $disabled . '">
                                                                    <span class="glyphicon glyphicon-trash text-danger delete-file"
                                                                            id="' . $attachment_Details->id . '"></span>
                                                                </a>';
                    } else {

                        $html .= '<img src="' . $attachment_src . '"
                                                                id="deleted-' . $attachment_Details->id . '"
                                                                alt="" title=""
                                                                data-placeholder="no_image.png"/>
                                                                <a href="' . $attachment_link . '"
                                                                    target="_blank">
                                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                                </a>
                                                                <a href="javascript:;"
                                                                    id="del-btn-' . $attachment_Details->id . '" class="' . $disabled . '">
                                                                    <span class="glyphicon glyphicon-trash text-danger delete-file"
                                                                            id="' . $attachment_Details->id . '"></span>
                                                                </a>';
                    }
                } else {


                    $html .= '<img src="' . Yii::$app->params['uploadIcon'] . '"
                                                            alt="" title=""
                                                            data-placeholder="no_image.png"/>
                                                            <a href="' . Yii::$app->params['uploadIcon'] . '"
                                                                target="_blank">
                                                                <span class="glyphicon glyphicon-eye-open"></span>
                                                            </a>';
                }


                $html .= '</a>
                                                <input type="hidden"
                                                        class="removed-' . $attachment_Details->id . ' mandatory-doc"
                                                        name="ClientValuation[received_docs][' . $unit_row . '][attachment]"
                                                        value="' . $attachment . '"
                                                        id="input-attachment' . $unit_row . '"/>
                                                <input type="hidden"
                                                        class="removed-' . $attachment_Details->id . '"
                                                        name="ClientValuation[received_docs][' . $unit_row . '][document_id]"
                                                        value="' . $required_document . '"
                                                        id="input-document' . $unit_row . '"/>

                                                <input type="hidden"
                                                        class="removed-' . $attachment_Details->id . '"
                                                        name="ClientValuation[received_docs][' . $unit_row . '][valuation_id]"
                                                        value="' . $valuation->id . '"
                                                        id="input-history_id' . $unit_row . '"/>

                                                <input type="hidden"
                                                        class="removed-' . $attachment_Details->id . '"
                                                        name="ClientValuation[received_docs][' . $unit_row . '][key]"
                                                        value="' . $key . '"
                                                        id="input-key' . $unit_row . '"/>
                                    </div>
                                </td>';
                if ($doc_insert_date <> null) {
                    $insert_date = $doc_insert_date;
                } else {
                    $insert_date = '';
                }
                $html .= '<td>
                                <span class="badge grid-badge badge-info bg-info">' .  $insert_date . '</span>
                            </td>';

                $html .= '</tr>';
                $unit_row++;
            }
            foreach ($optional_documents as $optinal_document) {

                if ($optinal_document == 68) {
                    if ($numberOfUnits < 2) {
                        continue;
                    }
                }
                if ($optinal_document == 9) {
                    if (in_array($propertyId, [4, 5, 39, 48, 49, 50, 53, 20, 46]) && $completionStatus == 1) {
                        continue;
                    }
                }
                if ($optinal_document == 52 || $optinal_document == 66 || $optinal_document == 67) {
                    if ($extended == "No") {
                        continue;
                    }
                }

                if ($optinal_document == 1 || $optinal_document == 6) {
                    continue;
                }

                if ($valuation->key <> null) {
                    $attachment_Details = \app\models\ClientValuationDocsFiles::find()->where(["key" => $valuation->key, "document_id" => $optinal_document])->one();
                }
                // $attachment_Details = \app\models\ClientValuationDocsFiles::find()->where(["valuation_id" => $valuation->id, "document_id" => $optinal_document])->one();
                // echo "<pre>"; print_r($attachment_Details); echo "</pre>"; die;
                $doc_insert_date = '';
                if ($attachment_Details->attachment <> null) {
                    if ($attachment_Details->doc_insert_date <> null) {
                        $doc_insert_date = date('d F Y', strtotime($attachment_Details->doc_insert_date));
                    }
                }
                // dd($attachment_Details->id);
                $attachment = $attachment_Details->attachment;



                $html .= '<tr id="image-row' . $unit_row . '">';

                $html .= '<td class="text-left ">
                                <div class="form-group required">


                                    <label>
                                        ' . Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$optinal_document] . '
                                    </label>
                                </div>
                            </td>';

                $html .= '<td class="text-left upload-docs">
                                <div class="form-group">
                                    <a href="javascript:;"
                                        id="upload-document' . $unit_row . '"
                                        onclick="uploadAttachment(' . $unit_row . ')"
                                        data-toggle="tooltip"
                                        class="img-thumbnail  ' . $disabled . '"
                                        title="Upload Document">';


                if ($attachment <> null) {

                    $attachment_link= $attachment;
                    $explode_attach_doc = explode('received-info/',$attachment);

                    $explode_attach = explode('https://winsmills-wiz-prod.s3.eu-central-1.amazonaws.com/', $attachment);
                    if (isset($explode_attach[1])) {
                        $attachment_link = Yii::$app->get('s3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                    }else{
                        $explode_attach = explode('https://maxclientmodule.s3.ap-southeast-1.amazonaws.com/', $attachment);
                        $attachment_link = Yii::$app->get('s3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                    }

                    if(isset($explode_attach_doc[1])) {
                        $attachment_src = 'https://client.windmillsgroup.com/cache/'.$explode_attach_doc[1];
                    }else{
                        $attachment_src = Yii::$app->params['uploadDocsIcon'];
                    }

                    if (strpos($attachment, '.pdf') !== false) {

                        $html .= '<img src="' . Yii::$app->params['uploadPdfIcon'] . '"
                                                        id="deleted-' . $attachment_Details->id . '"
                                                        alt="" title=""
                                                        data-placeholder="no_image.png"/>
                                                <a href="' . $attachment_link . '"
                                                    target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                                <a href="javascript:;"
                                                    id="del-btn-' . $attachment_Details->id . '" class="' . $disabled . '">
                                                    <span class="glyphicon glyphicon-trash text-danger delete-file"
                                                            id="' . $attachment_Details->id . '"></span>
                                                </a>';
                    } else if (strpos($attachment, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {

                        $html .= '<img src="' . Yii::$app->params['uploadDocsIcon'] . '"
                                                        id="deleted-' . $attachment_Details->id . '"
                                                        alt="" title=""
                                                        data-placeholder="no_image.png"/>
                                                <a href="' . $attachment_link . '"
                                                    target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                                <a href="javascript:;"
                                                    id="del-btn-' . $attachment_Details->id . '" class="' . $disabled . '">
                                                    <span class="glyphicon glyphicon-trash text-danger delete-file"
                                                            id="' . $attachment_Details->id . '"></span>
                                                </a>';
                    } else {

                        $html .= '<img src="' . $attachment_src . '"
                                                        id="deleted-' . $attachment_Details->id . '"
                                                        alt="" title=""
                                                        data-placeholder="no_image.png"/>
                                                <a href="' . $attachment_link . '"
                                                    target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                                <a href="javascript:;"
                                                    id="del-btn-' . $attachment_Details->id . '" class="' . $disabled . '">
                                                    <span class="glyphicon glyphicon-trash text-danger delete-file"
                                                            id="' . $attachment_Details->id . '"></span>
                                                </a>';
                    }
                } else {

                    $html .= '<img src="' . Yii::$app->params['uploadIcon'] . '"
                                                    alt="" title=""
                                                    data-placeholder="no_image.png"/>
                                            <a href="' . Yii::$app->params['uploadIcon'] . '"
                                                target="_blank">
                                                <span class="glyphicon glyphicon-eye-open"></span>
                                            </a>';
                }


                $html .= '</a>
                                    <input type="hidden"
                                            class="removed-' . $attachment_Details->id . '"
                                            name="ClientValuation[received_docs][' . $unit_row . '][attachment]"
                                            value="' . $attachment . '"
                                            id="input-attachment' . $unit_row . '"/>
                                    <input type="hidden"
                                            class="removed-' . $attachment_Details->id . '"
                                            name="ClientValuation[received_docs][' . $unit_row . '][document_id]"
                                            value="' . $optinal_document . '"
                                            id="input-attachment' . $unit_row . '"/>

                                    <input type="hidden"
                                            class="removed-' . $attachment_Details->id . '"
                                            name="ClientValuation[received_docs][' . $unit_row . '][valuation_id]"
                                            value="' . $valuation->id . '"
                                            id="input-history_id' . $unit_row . '"/>

                                    <input type="hidden"
                                            class="removed-' . $attachment_Details->id . '"
                                            name="ClientValuation[received_docs][' . $unit_row . '][key]"
                                            value="' . $key . '"
                                            id="input-key' . $unit_row . '"/>
                                </div>
                            </td>';

                if ($doc_insert_date <> null) {
                    $insert_date = $doc_insert_date;
                } else {
                    $insert_date = '';
                }
                $html .= '<td>
                                <span class="badge grid-badge badge-info bg-info">' . $insert_date . '</span>
                            </td>';


                $html .= '</tr>';
                $unit_row++;
            }


            $html .= '</tbody>
                    <tfoot>

                    </tfoot>
                </table>';
        }

        //  $atch_count = 0;
        //  $html .= '<section class="card mt-4" style="border-top:2px solid #FFA500;">
        //      <header class="card-header">
        //          <h2 class="card-title"><strong>Additional Documents Provided by Client</strong></h2>

        //          <div class="card-tools">
        //              <button type="button" class="btn btn-tool add-km-image btn-warning text-dark"
        //                      title="Additional Document Image">
        //                  <i class="fas fa-plus"></i>
        //              </button>
        //          </div>
        //      </header>
        //      <div class="card-body">
        //          <div class="row" id="km-table">';

        //              if($valuation->kmImages<>null){
        //                  foreach($valuation->kmImages as $key => $image){

        //                      $html .= '<div class="col-2 my-2 upload-docs" id="image-row'. $atch_count .'">
        //                          <div class="form-group">
        //                              <a href="javascript:;" id="upload-document'. $atch_count .'"
        //                                  data-uploadid='. $atch_count .'  data-toggle="tooltip"
        //                                  class="img-thumbnail open-img-window" title="Upload Document">

        //                                  <img src="'. $image->attachment .'" alt=""
        //                                          title="" data-placeholder="no_image.png" />
        //                              </a>
        //                              <a href="'. $image->attachment .'" class="mx-2" target="_blank">
        //                                  <i class="fa fa-eye text-primary"></i>
        //                              </a>
        //                              <input type="hidden"
        //                                      name="ClientValuation[km_images]['. $atch_count .'][attachment]"
        //                                      id="input-attachment'.$atch_count.'"
        //                                      value="'. $image->attachment .'" />
        //                              <input type="hidden"
        //                                      name="ClientValuation[km_images]['. $atch_count .'][db_id]"
        //                                      value="'. $image->id .'" />
        //                              <input type="hidden"
        //                                      name="ClientValuation[km_images]['. $atch_count .'][key]"
        //                                      value="'. $key .'" />
        //                          </div>
        //                      </div>';

        //                      $atch_count++;
        //                  }
        //              }

        //          $html .= '</div>
        //      </div>
        //  </section>';

        return $html;
    }


    // documents mandatory and optional columnwise
    public function actionClientvaluationDocumentsColumnView()
    {


        $propertyId = $_POST['propertyId'];
        $key = $_POST['key'];

        // $valuation = $this->findModel($id);
        $valuation = ClientValuation::find()->where(["key" => $key])->one();

        $propertyRequiredDocuments = Yii::$app->appHelperFunctions->getPropertyRequiredDocuments($propertyId);
        $propertyOptionalDocuments = Yii::$app->appHelperFunctions->getPropertyOptionalDocuments($propertyId);

        if (isset($propertyRequiredDocuments) && ($propertyRequiredDocuments <> null)) {
            $required_documents = explode(',', $propertyRequiredDocuments);
        }
        if (isset($propertyOptionalDocuments) && ($propertyOptionalDocuments <> null)) {
            $optional_documents = explode(',', $propertyOptionalDocuments);
        }

        $unit_row = 0;

        if ($valuation->status_verified == 1) {
            $disabled = "disabled-link";
        } else {
            $disabled = '';
        }

        if ($required_documents <> null && !empty($required_documents)) {
            $html = '<div class="row"><div class="col-md-6">';
            $html .= '<table id="requestTypes" class="table table-striped table-bordered table-hover images-table">
                            <thead>
                                <tr>
                                    <td>Description</td>
                                    <td>Attachment</td>
                                    <td>Date</td>
                                    
                                </tr>
                            </thead>
                            <tbody>';



            foreach ($required_documents as $required_document) {
                if ($valuation->key <> null) {
                    $attachment_Details = \app\models\ClientValuationDocsFiles::find()->where(["key" => $valuation->key, "document_id" => $required_document])->one();
                }
                //$attachment_Details = \app\models\ClientValuationDocsFiles::find()->where([ "key" => $valuation->key, "document_id" => $required_document])->one();
                // echo "<pre>"; print_r($attachment_Details); echo "</pre>"; die;
                $doc_insert_date = '';
                if ($attachment_Details->attachment <> null) {
                    if ($attachment_Details->doc_insert_date <> null) {
                        $doc_insert_date = date('d F Y', strtotime($attachment_Details->doc_insert_date));
                    }
                }
                // dd($attachment_Details->id);
                $attachment = $attachment_Details->attachment;



                $html .= '<tr id="image-row' . $unit_row . '">';

                $html .= '<td class="text-left">
                                    <div class="required">
                                        <label class="control-label">'
                    . Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$required_document] .
                    '</label>
                                    </div>
                                  </td>';

                $html .= '<td class="text-left upload-docs">
                                    <div class="form-group">
                                        <a href="javascript:;"
                                            id="upload-document' . $unit_row . '"
                                            onclick="uploadAttachment(' . $unit_row . ')"
                                            data-toggle="tooltip"
                                            class="img-thumbnail ' . $disabled . '"
                                            title="Upload Document">';


                if ($attachment <> null) {

                    $attachment_link= $attachment;
                    $explode_attach_doc = explode('received-info/',$attachment);

                    $explode_attach = explode('https://winsmills-wiz-prod.s3.eu-central-1.amazonaws.com/', $attachment);
                    if (isset($explode_attach[1])) {
                        $attachment_link = Yii::$app->get('s3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                    }else{
                        $explode_attach = explode('https://maxclientmodule.s3.ap-southeast-1.amazonaws.com/', $attachment);
                        $attachment_link = Yii::$app->get('s3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                    }

                    if(isset($explode_attach_doc[1])) {
                        $attachment_src = 'https://client.windmillsgroup.com/cache/'.$explode_attach_doc[1];
                    }else{
                        $attachment_src = Yii::$app->params['uploadDocsIcon'];
                    }

                    if (strpos($attachment, '.pdf') !== false) {

                        $html .= '<img src="' . Yii::$app->params['uploadPdfIcon'] . '"
                                                                id="deleted-' . $attachment_Details->id . '"
                                                                alt="" title=""
                                                                data-placeholder="no_image.png"/>
                                                                <a href="' . $attachment_link . '" target="_blank">
                                                                        <span class="glyphicon glyphicon-eye-open"></span>
                                                                </a>
                                                                <a href="javascript:;" id="del-btn-' . $attachment_Details->id . '" class="' . $disabled . '">
                                                                    <span class="glyphicon glyphicon-trash text-danger delete-file"
                                                                            id="' . $attachment_Details->id . '"></span>
                                                                </a>';
                    } else if (strpos($attachment, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {

                        $html .= '<img src="' . Yii::$app->params['uploadDocsIcon'] . '"
                                                                id="deleted-' . $attachment_Details->id . '"
                                                                alt="" title=""
                                                                data-placeholder="no_image.png"/>
                                                                <a href="' . $attachment_link . '"
                                                                    target="_blank">
                                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                                </a>
                                                                <a href="javascript:;"
                                                                    id="del-btn-' . $attachment_Details->id . '" class="' . $disabled . '">
                                                                    <span class="glyphicon glyphicon-trash text-danger delete-file"
                                                                            id="' . $attachment_Details->id . '"></span>
                                                                </a>';
                    } else {

                        $html .= '<img src="' . $attachment_src . '"
                                                                id="deleted-' . $attachment_Details->id . '"
                                                                alt="" title=""
                                                                data-placeholder="no_image.png"/>
                                                                <a href="' . $attachment_link . '"
                                                                    target="_blank">
                                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                                </a>
                                                                <a href="javascript:;"
                                                                    id="del-btn-' . $attachment_Details->id . '" class="' . $disabled . '">
                                                                    <span class="glyphicon glyphicon-trash text-danger delete-file"
                                                                            id="' . $attachment_Details->id . '"></span>
                                                                </a>';
                    }
                } else {


                    $html .= '<img src="' . Yii::$app->params['uploadIcon'] . '"
                                                            alt="" title=""
                                                            data-placeholder="no_image.png"/>
                                                            <a href="' . Yii::$app->params['uploadIcon'] . '"
                                                                target="_blank">
                                                                <span class="glyphicon glyphicon-eye-open"></span>
                                                            </a>';
                }


                $html .= '</a>
                                                <input type="hidden"
                                                        class="removed-' . $attachment_Details->id . '"
                                                        name="ClientValuation[received_docs][' . $unit_row . '][attachment]"
                                                        value="' . $attachment . '"
                                                        id="input-attachment' . $unit_row . '"/>
                                                <input type="hidden"
                                                        class="removed-' . $attachment_Details->id . '"
                                                        name="ClientValuation[received_docs][' . $unit_row . '][document_id]"
                                                        value="' . $required_document . '"
                                                        id="input-attachment' . $unit_row . '"/>

                                                <input type="hidden"
                                                        class="removed-' . $attachment_Details->id . '"
                                                        name="ClientValuation[received_docs][' . $unit_row . '][valuation_id]"
                                                        value="' . $valuation->id . '"
                                                        id="input-history_id' . $unit_row . '"/>

                                                <input type="hidden"
                                                        class="removed-' . $attachment_Details->id . '"
                                                        name="ClientValuation[received_docs][' . $unit_row . '][key]"
                                                        value="' . $key . '"
                                                        id="input-key' . $unit_row . '"/>
                                    </div>
                                </td>';
                if ($doc_insert_date <> null) {
                    $insert_date = $doc_insert_date;
                } else {
                    $insert_date = '';
                }
                $html .= '<td>
                                <span class="badge grid-badge badge-info bg-info">' .  $insert_date . '</span>
                            </td>';

                $html .= '</tr>';
                $unit_row++;
            }
            $html .= '</tbody>';
            $html .= '</table>';
            $html .= '</div>';

            $html .= '<div class="col-md-6">';
            $html .= '<table id="requestTypes" class="table table-striped table-bordered table-hover images-table">
                            <thead>
                                <tr>
                                    <td>Description</td>
                                    <td>Attachment</td>
                                    <td>Date</td>
                                    
                                </tr>
                            </thead>
                            <tbody>';

            foreach ($optional_documents as $optinal_document) {
                if ($valuation->key <> null) {
                    $attachment_Details = \app\models\ClientValuationDocsFiles::find()->where(["key" => $valuation->key, "document_id" => $optinal_document])->one();
                }
                // $attachment_Details = \app\models\ClientValuationDocsFiles::find()->where(["valuation_id" => $valuation->id, "document_id" => $optinal_document])->one();
                // echo "<pre>"; print_r($attachment_Details); echo "</pre>"; die;
                $doc_insert_date = '';
                if ($attachment_Details->attachment <> null) {
                    if ($attachment_Details->doc_insert_date <> null) {
                        $doc_insert_date = date('d F Y', strtotime($attachment_Details->doc_insert_date));
                    }
                }
                // dd($attachment_Details->id);
                $attachment = $attachment_Details->attachment;




                $html .= '<tr id="image-row' . $unit_row . '">';

                $html .= '<td class="text-left ">
                                <div class="required">


                                    <label>
                                        ' . Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$optinal_document] . '
                                    </label>
                                </div>
                            </td>';

                $html .= '<td class="text-left upload-docs">
                                <div class="form-group">
                                    <a href="javascript:;"
                                        id="upload-document' . $unit_row . '"
                                        onclick="uploadAttachment(' . $unit_row . ')"
                                        data-toggle="tooltip"
                                        class="img-thumbnail  ' . $disabled . '"
                                        title="Upload Document">';


                if ($attachment <> null) {

                    $attachment_link= $attachment;
                    $explode_attach_doc = explode('received-info/',$attachment);

                    $explode_attach = explode('https://winsmills-wiz-prod.s3.eu-central-1.amazonaws.com/', $attachment);
                    if (isset($explode_attach[1])) {
                        $attachment_link = Yii::$app->get('s3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                    }else{
                        $explode_attach = explode('https://maxclientmodule.s3.ap-southeast-1.amazonaws.com/', $attachment);
                        $attachment_link = Yii::$app->get('s3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
                    }

                    if(isset($explode_attach_doc[1])) {
                        $attachment_src = 'https://client.windmillsgroup.com/cache/'.$explode_attach_doc[1];
                    }else{
                        $attachment_src = Yii::$app->params['uploadDocsIcon'];
                    }


                    if (strpos($attachment, '.pdf') !== false) {

                        $html .= '<img src="' . Yii::$app->params['uploadPdfIcon'] . '"
                                                        id="deleted-' . $attachment_Details->id . '"
                                                        alt="" title=""
                                                        data-placeholder="no_image.png"/>
                                                <a href="' . $attachment_link . '"
                                                    target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                                <a href="javascript:;"
                                                    id="del-btn-' . $attachment_Details->id . '" class="' . $disabled . '">
                                                    <span class="glyphicon glyphicon-trash text-danger delete-file"
                                                            id="' . $attachment_Details->id . '"></span>
                                                </a>';
                    } else if (strpos($attachment, '.doc') !== false || strpos($attachment, '.docx') !== false || strpos($attachment, '.xlsx') !== false || strpos($attachment, '.xls') !== false) {

                        $html .= '<img src="' . Yii::$app->params['uploadDocsIcon'] . '"
                                                        id="deleted-' . $attachment_Details->id . '"
                                                        alt="" title=""
                                                        data-placeholder="no_image.png"/>
                                                <a href="' . $attachment_link . '"
                                                    target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                                <a href="javascript:;"
                                                    id="del-btn-' . $attachment_Details->id . '" class="' . $disabled . '">
                                                    <span class="glyphicon glyphicon-trash text-danger delete-file"
                                                            id="' . $attachment_Details->id . '"></span>
                                                </a>';
                    } else {

                        $html .= '<img src="' . $attachment_src . '"
                                                        id="deleted-' . $attachment_Details->id . '"
                                                        alt="" title=""
                                                        data-placeholder="no_image.png"/>
                                                <a href="' . $attachment_link . '"
                                                    target="_blank">
                                                    <span class="glyphicon glyphicon-eye-open"></span>
                                                </a>
                                                <a href="javascript:;"
                                                    id="del-btn-' . $attachment_Details->id . '" class="' . $disabled . '">
                                                    <span class="glyphicon glyphicon-trash text-danger delete-file"
                                                            id="' . $attachment_Details->id . '"></span>
                                                </a>';
                    }
                } else {

                    $html .= '<img src="' . Yii::$app->params['uploadIcon'] . '"
                                                    alt="" title=""
                                                    data-placeholder="no_image.png"/>
                                            <a href="' . Yii::$app->params['uploadIcon'] . '"
                                                target="_blank">
                                                <span class="glyphicon glyphicon-eye-open"></span>
                                            </a>';
                }


                $html .= '</a>
                                    <input type="hidden"
                                            class="removed-' . $attachment_Details->id . '"
                                            name="ClientValuation[received_docs][' . $unit_row . '][attachment]"
                                            value="' . $attachment . '"
                                            id="input-attachment' . $unit_row . '"/>
                                    <input type="hidden"
                                            class="removed-' . $attachment_Details->id . '"
                                            name="ClientValuation[received_docs][' . $unit_row . '][document_id]"
                                            value="' . $optinal_document . '"
                                            id="input-attachment' . $unit_row . '"/>

                                    <input type="hidden"
                                            class="removed-' . $attachment_Details->id . '"
                                            name="ClientValuation[received_docs][' . $unit_row . '][valuation_id]"
                                            value="' . $valuation->id . '"
                                            id="input-history_id' . $unit_row . '"/>

                                    <input type="hidden"
                                            class="removed-' . $attachment_Details->id . '"
                                            name="ClientValuation[received_docs][' . $unit_row . '][key]"
                                            value="' . $key . '"
                                            id="input-key' . $unit_row . '"/>
                                </div>
                            </td>';

                if ($doc_insert_date <> null) {
                    $insert_date = $doc_insert_date;
                } else {
                    $insert_date = '';
                }
                $html .= '<td>
                                <span class="badge grid-badge badge-info bg-info">' . $insert_date . '</span>
                            </td>';


                $html .= '</tr>';
                $unit_row++;
            }


            $html .= '</tbody>
                    <tfoot>

                    </tfoot>
                </table></div>';
        }

        //  $atch_count = 0;
        //  $html .= '<section class="card mt-4" style="border-top:2px solid #FFA500;">
        //      <header class="card-header">
        //          <h2 class="card-title"><strong>Additional Documents Provided by Client</strong></h2>

        //          <div class="card-tools">
        //              <button type="button" class="btn btn-tool add-km-image btn-warning text-dark"
        //                      title="Additional Document Image">
        //                  <i class="fas fa-plus"></i>
        //              </button>
        //          </div>
        //      </header>
        //      <div class="card-body">
        //          <div class="row" id="km-table">';

        //              if($valuation->kmImages<>null){
        //                  foreach($valuation->kmImages as $key => $image){

        //                      $html .= '<div class="col-2 my-2 upload-docs" id="image-row'. $atch_count .'">
        //                          <div class="form-group">
        //                              <a href="javascript:;" id="upload-document'. $atch_count .'"
        //                                  data-uploadid='. $atch_count .'  data-toggle="tooltip"
        //                                  class="img-thumbnail open-img-window" title="Upload Document">

        //                                  <img src="'. $image->attachment .'" alt=""
        //                                          title="" data-placeholder="no_image.png" />
        //                              </a>
        //                              <a href="'. $image->attachment .'" class="mx-2" target="_blank">
        //                                  <i class="fa fa-eye text-primary"></i>
        //                              </a>
        //                              <input type="hidden"
        //                                      name="ClientValuation[km_images]['. $atch_count .'][attachment]"
        //                                      id="input-attachment'.$atch_count.'"
        //                                      value="'. $image->attachment .'" />
        //                              <input type="hidden"
        //                                      name="ClientValuation[km_images]['. $atch_count .'][db_id]"
        //                                      value="'. $image->id .'" />
        //                          </div>
        //                      </div>';

        //                      $atch_count++;
        //                  }
        //              }

        //          $html .= '</div>
        //      </div>
        //  </section>';

        return $html;
    }


    public function actionProformaInvoice($id)
    {

        $model = Valuation::find()->where(['id' => $id])->one();
        $valuation = Valuation::find()->where(['id' => $id])->one();


        $rateAndItemAmount = Yii::$app->appHelperFunctions->getRateAndItemAmount($valuation->client->id, $valuation->building->city, $valuation->inspection_type, $valuation->tenure, $valuation->property_id, $valuation->urgency, $valuation->inspectProperty->built_up_area, $valuation->land_size, $valuation->inspectProperty->total_units);


        require_once(__DIR__ . '/../components/tcpdf/ClientProformaInvoice.php');
        // create new PDF document
        $pdf = new \ProformaInvoice(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle($model->reference_number);
        $pdf->SetSubject('Quotation');
        $pdf->SetKeywords($model->reference_number);

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(10, 35, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('times', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P', 'A4');

        $qpdf = Yii::$app->controller->renderPartial('/client-valuation/qpdf', [
            'id' => $id,
            'model' => $model,
            'rateAndItemAmount' => $rateAndItemAmount,
        ]);

        $proformaInvoicePdfFile = 'ProformaInvoice-' . $model->reference_number . '.pdf';

        $fullPath = realpath(dirname(__FILE__) . '/../p-invoices') . '/' . $proformaInvoicePdfFile;

        // $pdf->writeHTML($qpdf, true, false, false, false, '');
        // $pdf->Output($fullPath, 'F');
        // return $fullPath;

        $pdf->writeHTML($qpdf, true, false, false, false, '');
        $pdf->Output($proformaInvoicePdfFile, 'D');
        // $pdf->Output($fullPath, 'I');
        exit;
    }


    public function actionDownloadReport($id)
    {
        // dd('here');
        /*  Yii::$app->getSession()->addFlash('error', "Permission denied!");
          return $this->redirect(['index']);*/
        $this->checkLogin();
        $model = Valuation::find()->where(['id' => $id])->one();
        $valuation = Valuation::find()->where(['id' => $id])->one();

        $valuationController = new \app\controllers\ValuationController('valuation', Yii::$app);


        // check value for valuation approach view change
        $valuation_approach = $model->valuation_approach;

        $allowed_properties = array(1, 2, 4, 5, 6, 11, 12, 17, 20, 23, 24, 25, 26, 28, 29, 37, 39, 41, 44, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 77, 90);
        $allow = 1;
        /*        if(Yii::$app->user->identity->permission_group_id==3) {
                    if ($model->client->id == 1) {

                        $allow = 1;
                    } else if (!in_array($model->property_id, $allowed_properties)) {
                        $allow = 1;
                    } else if (Yii::$app->menuHelperFunction->checkActionAllowed('pdf') && $model->client->print_report == 1) {
                        $allow = 1;
                    }
                }else {

                    if (Yii::$app->menuHelperFunction->checkActionAllowed('pdf') && $model->client->print_report == 1) {
                        $allow = 1;
                    }
                }*/

        if (!Yii::$app->menuHelperFunction->checkActionAllowed('pdf') && $allow == 0) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        /*  echo $allow;
          die;*/
        //      ini_set('max_execution_time', '150'); //300 seconds = 5 minutes
        ini_set('max_execution_time', '0'); // for infinite time of execution
        /*  if(!Yii::$app->menuHelperFunction->checkActionAllowed('pdf')) {
              Yii::$app->getSession()->addFlash('error', "Permission denied!");
              return $this->redirect(['index']);
          }*/

        // $model = $this->findModel($id);

        // 1.72 Market Value
        $approver_data = ValuationApproversData::find()->where(['valuation_id' => $id, 'approver_type' => 'approver'])->one();
        $mv = $approver_data->estimated_market_value;
        $estimate_price_byapprover = ($approver_data <> null) ? (number_format($approver_data->estimated_market_value) . '<br>' . trim($valuationController->convertNumberToWord($approver_data->estimated_market_value)) . ' Dirhams Only') : '';
        $estimate_price_land_byapprover = ($approver_data <> null) ? (number_format($approver_data->estimated_market_value_land) . '<br>' . trim($valuationController->convertNumberToWord($approver_data->estimated_market_value_land)) . ' Dirhams Only') : '';

        //define ('K_PATH_IMAGES', dirname(__FILE__).'/../web/images/');

        // Include the main TCPDF library (search for installation path).
        require_once(__DIR__ . '/../components/tcpdf/MyPDF.php');

        // create new PDF document
        $pdf = new \MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->model = $model;
        $report_name = '';
        if ($model->client_reference <> null) {
            $report_name = $model->reference_number . '_' . $model->client_reference;
        } else {
            $report_name = $model->reference_number;
        }
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle($report_name);
        $pdf->SetSubject('Valuation Report');
        $pdf->SetKeywords($report_name);

        if ($model->client->report_password <> null) {
            $pdf->SetProtection(array('modify', 'copy', 'print'), $model->client->report_password, $model->client->report_password, 0, null);
        }
        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(10, 0, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        // ---------------------------------------------------------
        // set default font subsetting mode
        $pdf->setFontSubsetting(true);


        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('arialn', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P', 'A4');

        if ($valuation_approach == 1) {
            //for income approach report

            $configuration = InspectProperty::find()->where(['valuation_id' => $id])->one();

            $pdf->Image('images/income_frontpage.png', 0, 0, 470, 600, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
            $pdf->writeHTML($html, true, false, false, false, '');
            $pdf->SetMargins(myPDF_MARGIN_LEFT, myPDF_MARGIN_TOP, myPDF_MARGIN_RIGHT, 0);


            $incomeapproch_data = $valuationController->actionIncomeEstimateValue($model->id);
            $costapproch_data = $valuationController->actionCostEstimateValue($model->id);
            //1.73 Market Value Rate , 1.74 Market Rent
            $summary = Yii::$app->PdfHelper->getMarketRent($incomeapproch_data, $model, $costapproch_data);

            //Table of Content
            $toc = $valuationController->renderPartial('pdf/income_report/toc', ['model' => $model]);



            // 1. Property and Valuation Overview
            if ($model->id > 12034) {
                $pavo = $valuationController->renderPartial('pdf/income_report/pavo_age', [
                    'model' => $model,
                    'estimate_price_byapprover' => $estimate_price_byapprover,
                    'estimate_price_land_byapprover' => $estimate_price_land_byapprover,
                    'estimate_price_byapproverpdf' => number_format($mv),
                    'summary' => $summary,
                    'configuration' => $configuration,
                ]);
            } else {
                $pavo = $valuationController->renderPartial('pdf/income_report/pavo', [
                    'model' => $model,
                    'estimate_price_byapprover' => $estimate_price_byapprover,
                    'estimate_price_land_byapprover' => $estimate_price_land_byapprover,
                    'estimate_price_byapproverpdf' => number_format($mv),
                    'summary' => $summary,
                    'configuration' => $configuration,
                ]);
            }
            $pdfdetail = $valuationController->renderPartial('pdf/income_report/pdfdetail', ['model' => $model]);
            $transactionList = $valuationController->renderPartial('pdf/income_report/transaction', ['model' => $model]);
            $inspectionSheet = $valuationController->renderPartial('pdf/income_report/inspectionSheet_Email', ['model' => $model]);

            $pdffirstsection = $valuationController->renderPartial('pdf/income_report/pdffirstsection', ['model' => $model]);




            if ($model->client->id == 183  || $model->id > 12570) {
                $html1 = '' . $pdffirstsection . Yii::$app->PdfHelper->tabeleCss . $toc . $pavo;

                $html2 = Yii::$app->PdfHelper->tabeleCss . $inspectionSheet . $transactionList;
                $html201 = Yii::$app->PdfHelper->tabeleCss . $pdfdetail;
            } else {
                $html1 = '' . $pdffirstsection . Yii::$app->PdfHelper->tabeleCss . $toc . $pavo . $pdfdetail;

                $html2 = Yii::$app->PdfHelper->tabeleCss . $inspectionSheet;

                $html202 = Yii::$app->PdfHelper->tabeleCss . $transactionList;
            }


            //
            $html3 = '';

            $pdf->writeHTML($html1, true, false, false, false, '');

            $pdf->SetMargins(20, 10, 10, 25);
            $pdf->writeHTML($html2, true, false, false, false, '');




            if ($model->client->id == 183  || $model->id > 12570) {
                $pdf->SetMargins(20, 15, 10, 25);
                $pdf->writeHTML($html201, true, false, false, false, '');
            } else {
                $pdf->SetMargins(20, 5, 10, 25);
                $pdf->writeHTML($html202, true, false, false, false, '');
            }


            $pdf->AddPage('P', 'A4');
            $pdf->SetMargins(0, 0, 0, 0);
            $pdf->writeHTML($html3, true, false, false, false, '');

            /*    if(isset($model->building->city) && ($model->building->city == 3507)){
                    $pdf->Image('images/lastpage_ajman.png', 0, 0, 500, 600, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
                }else {
                // $pdf->Image('images/lastpage.png', 0, 0, 500, 600, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
                    $pdf->Image('images/lastpage-new.png', 0, 0, 500, 600, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
                }*/
            $pdf->Image('images/lastpage-new.png', 0, 0, 500, 600, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
            $pdfflastaddress = $valuationController->renderPartial('pdf/income_report/pdfflastaddress', ['model' => $model]);
            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->writeHTMLCell($w = 0, $h = 0, $x = 0, $y = 235, $pdfflastaddress, '', 0, 0, 0, 'j', true);
        } else {
            $pdf->Image('images/frontpage.png', 0, 0, 470, 600, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
            $pdf->writeHTML($html, true, false, false, false, '');
            $pdf->SetMargins(myPDF_MARGIN_LEFT, myPDF_MARGIN_TOP, myPDF_MARGIN_RIGHT, 0);


            $incomeapproch_data = $valuationController->actionIncomeEstimateValue($model->id);
            $costapproch_data = $valuationController->actionCostEstimateValue($model->id);
            //1.73 Market Value Rate , 1.74 Market Rent
            $summary = Yii::$app->PdfHelper->getMarketRent($incomeapproch_data, $model, $costapproch_data);


            //Table of Content
            $toc = $valuationController->renderPartial('pdf/toc', ['model' => $model]);


            // 1. Property and Valuation Overview
            if ($model->id > 12034) {
                $pavo = $valuationController->renderPartial('pdf/pavo_age', [
                    'model' => $model,
                    'estimate_price_byapprover' => $estimate_price_byapprover,
                    'estimate_price_land_byapprover' => $estimate_price_land_byapprover,
                    'estimate_price_byapproverpdf' => number_format($mv),
                    'summary' => $summary,
                ]);
            } else {
                $pavo = $valuationController->renderPartial('pdf/pavo', [
                    'model' => $model,
                    'estimate_price_byapprover' => $estimate_price_byapprover,
                    'estimate_price_land_byapprover' => $estimate_price_land_byapprover,
                    'estimate_price_byapproverpdf' => number_format($mv),
                    'summary' => $summary,
                ]);
            }
            $pdfdetail = $valuationController->renderPartial('pdf/pdfdetail', ['model' => $model]);
            $transactionList = $valuationController->renderPartial('pdf/transaction', ['model' => $model]);
            $inspectionSheet = $valuationController->renderPartial('pdf/inspectionSheet_Email', ['model' => $model]);

            $pdffirstsection = $valuationController->renderPartial('pdf/pdffirstsection', ['model' => $model]);





            if ($model->client->id == 183  || $model->id > 12570) {
                $html1 = '' . $pdffirstsection . Yii::$app->PdfHelper->tabeleCss . $toc . $pavo;

                $html2 = Yii::$app->PdfHelper->tabeleCss . $inspectionSheet . $transactionList;
                $html201 = Yii::$app->PdfHelper->tabeleCss . $pdfdetail;
            } else {
                $html1 = '' . $pdffirstsection . Yii::$app->PdfHelper->tabeleCss . $toc . $pavo . $pdfdetail;

                $html2 = Yii::$app->PdfHelper->tabeleCss . $inspectionSheet;

                $html202 = Yii::$app->PdfHelper->tabeleCss . $transactionList;
            }


            // $html1 = $transactionList.$inspectionSheet.'';

            $pdf->writeHTML($html1, true, false, false, false, '');

            //
            $html3 = '';

            $pdf->SetMargins(20, 10, 10, 25);
            $pdf->writeHTML($html2, true, false, false, false, '');


            if ($model->client->id == 183  || $model->id > 12570) {
                $pdf->SetMargins(20, 15, 10, 25);
                $pdf->writeHTML($html201, true, false, false, false, '');
            } else {
                $pdf->SetMargins(20, 5, 10, 25);
                $pdf->writeHTML($html202, true, false, false, false, '');
            }


            $pdf->AddPage('P', 'A4');
            $pdf->SetMargins(0, 0, 0, 0);
            $pdf->writeHTML($html3, true, false, false, false, '');

            /*    if(isset($model->building->city) && ($model->building->city == 3507)){
                    $pdf->Image('images/lastpage_ajman.png', 0, 0, 500, 600, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
                }else {
                // $pdf->Image('images/lastpage.png', 0, 0, 500, 600, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
                    $pdf->Image('images/lastpage-new.png', 0, 0, 500, 600, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
                }*/
            $pdf->Image('images/lastpage-new.png', 0, 0, 500, 600, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
            $pdfflastaddress = $valuationController->renderPartial('pdf/pdfflastaddress', ['model' => $model]);
            $pdf->SetFont('helvetica', 'B', 12);
            $pdf->writeHTMLCell($w = 0, $h = 0, $x = 0, $y = 235, $pdfflastaddress, '', 0, 0, 0, 'j', true);
        }


        // ---------------------------------------------------------
        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.


        // $pdf->Output($report_name . '.pdf', 'I');
        $pdf->Output($report_name . '.pdf', 'D');
    }


    public function actionDownloadPaymentReceipt($id)
    {
        $valuation = Valuation::find()->where(['id' => $id])->one();

        // dd($valuation->payment_slip);

        $filePath = $valuation->payment_slip;
        // $paymentSlip = $valuation->payment_slip;
        // $explode_attach_doc = explode('received-info/', $valuation->payment_slip);
        // if (isset($explode_attach_doc[1])) {
        //     $attachment_src = 'http://localhost:8888/windmills/cache/' . $explode_attach_doc[1];
        // }

        // $explode_attach = explode('https://max-medianew.s3.ap-southeast-1.amazonaws.com/', $valuation->payment_slip);
        // if (isset($explode_attach[1])) {
        //     $paymentSlip = Yii::$app->get('s3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
        // } else {
        //     $explode_attach = explode('https://newcliudfrontaclmax.s3.ap-southeast-1.amazonaws.com/', $attachment);
        //     if (isset($explode_attach[1])) {


        //         $paymentSlip = Yii::$app->get('s3bucketwe')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');

        //         /*  echo $explode_attach[1];
        //                         die;*/
        //     } else {
        //         $explode_attach = explode('https://maxima-media.s3.eu-central-1.amazonaws.com/', $attachment);
        //         $paymentSlip = Yii::$app->get('olds3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
        //     }
        // }

        // if (!file_exists($valuation->payment_slip)) {
        //     throw new NotFoundHttpException("The file does not exist.");
        // }

        // dd($paymentSlip);

        Yii::$app->response->sendFile($filePath);
    }

    public function actionDownloadSlaForm()
    {
        $filePath = Yii::getAlias('@webroot/sla-form.pdf'); // Adjust the file path as needed

        // if (!file_exists($filePath)) {
        //     throw new NotFoundHttpException("The file does not exist.");
        // }

        Yii::$app->response->sendFile($filePath);
    }

    public function actionDownloadValuationCertificate($id)
    {
        $valuation = Valuation::find()->where(['id' => $id])->one();

        // dd($valuation->payment_slip);

        $filePath = $valuation->taqyeem_certificate;
        // $paymentSlip = $valuation->payment_slip;
        // $explode_attach_doc = explode('received-info/', $valuation->taqyeem_certificate);
        // if (isset($explode_attach_doc[1])) {
        //     $attachment_src = 'http://localhost:8888/windmills/cache/' . $explode_attach_doc[1];
        // }

        // $explode_attach = explode('https://max-medianew.s3.ap-southeast-1.amazonaws.com/', $valuation->taqyeem_certificate);
        // if (isset($explode_attach[1])) {
        //     $taqyeemCertificate = Yii::$app->get('s3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
        // } else {
        //     $explode_attach = explode('https://newcliudfrontaclmax.s3.ap-southeast-1.amazonaws.com/', $attachment);
        //     if (isset($explode_attach[1])) {


        //         $taqyeemCertificate = Yii::$app->get('s3bucketwe')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');

        //         /*  echo $explode_attach[1];
        //                         die;*/
        //     } else {
        //         $explode_attach = explode('https://maxima-media.s3.eu-central-1.amazonaws.com/', $attachment);
        //         $taqyeemCertificate = Yii::$app->get('olds3bucket')->getPresignedUrl(urldecode($explode_attach[1]), '+10 minutes');
        //     }
        // }

        // dd($paymentSlip);

        Yii::$app->response->sendFile($filePath);
    }
}
