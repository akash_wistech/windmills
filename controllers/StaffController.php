<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\StaffSearch;
use app\models\UserProfileInfo;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* StaffController implements the CRUD actions for User model.
*/
class StaffController extends DefController
{
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all Staff models.
  * @return mixed
  */
  public function actionIndex()
  {
      $this->checkLogin();
    $this->checkAdmin();
    $searchModel = new StaffSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

    public function actionTamleek()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new StaffSearch();
        $searchModel->company_id = 119846;
       // $searchModel->client_staff = 1;
        $dataProvider = $searchModel->search_tamleek(Yii::$app->request->queryParams);

        return $this->render('index_tamleek', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionInjaz()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new StaffSearch();
        $searchModel->company_id = 119896;
        // $searchModel->client_staff = 1;
        $dataProvider = $searchModel->search_injaz(Yii::$app->request->queryParams);

        return $this->render('index_injaz', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionOntime()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new StaffSearch();
        $searchModel->company_id = 119877;
        // $searchModel->client_staff = 1;
        $dataProvider = $searchModel->search_ontime(Yii::$app->request->queryParams);

        return $this->render('index_ontime', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTabure()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new StaffSearch();
        $searchModel->company_id = 119900;
        // $searchModel->client_staff = 1;
        $dataProvider = $searchModel->search_tabure(Yii::$app->request->queryParams);

        return $this->render('index_tabure', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionFasttrack()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new StaffSearch();
        $searchModel->company_id = 119878;
        // $searchModel->client_staff = 1;
        $dataProvider = $searchModel->search_fasttrack(Yii::$app->request->queryParams);

        return $this->render('index_fasttrack', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

  /**
  * Creates a new Staff model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkSuperAdmin();
    $model = new User();
    $model->user_type=10;
    $model->status=1;
    $model->verified=1;
    

    // echo "<pre>";
    // print_r(Yii::$app->request->post());
    // die;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }
    return $this->render('create', [
      'model' => $model,
    ]);
  }

    public function actionCreateTamleek()
    {
        $this->checkSuperAdmin();
        $model = new User();
        $model->user_type=0;
        $model->status=1;
        $model->verified=1;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->db->createCommand()->update('user_profile_info', ['primary_contact' => 0], 'user_id=' . $model->id . '')->execute();
                Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
                return $this->redirect(['tamleek']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }
        return $this->render('create_tamleek', [
            'model' => $model,
        ]);
    }
    public function actionCreateInjaz()
    {
        $this->checkSuperAdmin();
        $model = new User();
        $model->user_type=0;
        $model->status=1;
        $model->verified=1;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->db->createCommand()->update('user_profile_info', ['primary_contact' => 0], 'user_id=' . $model->id . '')->execute();
                Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
                return $this->redirect(['injaz']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }
        return $this->render('create_injaz', [
            'model' => $model,
        ]);
    }

    public function actionCreateOntime()
    {
        $this->checkSuperAdmin();
        $model = new User();
        $model->user_type=0;
        $model->status=1;
        $model->verified=1;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->db->createCommand()->update('user_profile_info', ['primary_contact' => 0], 'user_id=' . $model->id . '')->execute();
                Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
                return $this->redirect(['ontime']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }
        return $this->render('create_ontime', [
            'model' => $model,
        ]);
    }

    public function actionCreateTabure()
    {
        $this->checkSuperAdmin();
        $model = new User();
        $model->user_type=0;
        $model->status=1;
        $model->verified=1;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->db->createCommand()->update('user_profile_info', ['primary_contact' => 0], 'user_id=' . $model->id . '')->execute();
                Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
                return $this->redirect(['tabure']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }
        return $this->render('create_tabure', [
            'model' => $model,
        ]);
    }

    public function actionCreateFasttrack()
    {
        $this->checkSuperAdmin();
        $model = new User();
        $model->user_type=0;
        $model->status=1;
        $model->verified=1;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->db->createCommand()->update('user_profile_info', ['primary_contact' => 0], 'user_id=' . $model->id . '')->execute();
                Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
                return $this->redirect(['fasttrack']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }
        return $this->render('create_fasttrack', [
            'model' => $model,
        ]);
    }

  /**
  * Updates an existing Staff model.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionUpdate($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);
    $model->status=1;
    $model->verified=1;
    $model->oldfile=$model->image;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('update', [
      'model' => $model,
    ]);
  }

    public function actionUpdateTamleek($id)
    {

        $this->checkLogin();
        $model = $this->findModel($id);
        $model->status=1;
        $model->verified=1;
        $model->oldfile=$model->image;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->db->createCommand()->update('user_profile_info', ['primary_contact' => 0], 'user_id=' . $model->id . '')->execute();
                Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
                return $this->redirect(['tamleek']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('update_tamleek', [
            'model' => $model,
        ]);
    }
    public function actionUpdateInjaz($id)
    {

        $this->checkLogin();
        $model = $this->findModel($id);
        $model->status=1;
        $model->verified=1;
        $model->oldfile=$model->image;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->db->createCommand()->update('user_profile_info', ['primary_contact' => 0], 'user_id=' . $model->id . '')->execute();
                Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
                return $this->redirect(['injaz']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('update_injaz', [
            'model' => $model,
        ]);
    }

    public function actionUpdateOntime($id)
    {

        $this->checkLogin();
        $model = $this->findModel($id);
        $model->status=1;
        $model->verified=1;
        $model->oldfile=$model->image;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->db->createCommand()->update('user_profile_info', ['primary_contact' => 0], 'user_id=' . $model->id . '')->execute();
                Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
                return $this->redirect(['ontime']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('update_ontime', [
            'model' => $model,
        ]);
    }

    public function actionUpdateTabure($id)
    {

        $this->checkLogin();
        $model = $this->findModel($id);
        $model->status=1;
        $model->verified=1;
        $model->oldfile=$model->image;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->db->createCommand()->update('user_profile_info', ['primary_contact' => 0], 'user_id=' . $model->id . '')->execute();
                Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
                return $this->redirect(['tabure']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('update_tabure', [
            'model' => $model,
        ]);
    }

    public function actionUpdateFasttrack($id)
    {

        $this->checkLogin();
        $model = $this->findModel($id);
        $model->status=1;
        $model->verified=1;
        $model->oldfile=$model->image;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->db->createCommand()->update('user_profile_info', ['primary_contact' => 0], 'user_id=' . $model->id . '')->execute();
                Yii::$app->getSession()->addFlash('success', Yii::t('app','Information saved successfully'));
                return $this->redirect(['fasttrack']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('update_fasttrack', [
            'model' => $model,
        ]);
    }

  /**
  * Creates a new model.
  */
  protected function newModel()
  {
    $model = new User;
    return $model;
  }

  /**
  * Finds the User model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Staff the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = User::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }



  public function actionSignaturedelete($id)
  {
      if ($id !=null) {
          $model = UserProfileInfo::find()->where(['user_id'=>$id])->one();
          $model->signature_img_name = '';
          $model->save();
      }
  }
    public function actionBanUser()
    {
        $this->checkAdmin();
        $searchModel = new StaffSearch();
        $searchModel->status=2;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize = 20;

        return $this->render('ban-user', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionApprovedpr()
    {
        $this->checkLogin();
        $this->checkAdmin();
        $searchModel = new StaffSearch();
        $searchModel->company_id = 120072;
        // $searchModel->client_staff = 1;
        $dataProvider = $searchModel->search_approvedpr(Yii::$app->request->queryParams);

        return $this->render('approvedpr/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreateApprovedpr()
    {
        $this->checkSuperAdmin();
        $model = new User();
        $model->user_type = 0;
        $model->status = 1;
        $model->verified = 1;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->db->createCommand()->update('user_profile_info', ['primary_contact' => 0], 'user_id=' . $model->id . '')->execute();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                return $this->redirect(['approvedpr']);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }
        return $this->render('approvedpr/create', [
            'model' => $model,
        ]);
    }


    public function actionUpdateApprovedpr($id)
    {

        $this->checkLogin();
        $model = $this->findModel($id);
        $model->status = 1;
        $model->verified = 1;
        $model->oldfile = $model->image;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->db->createCommand()->update('user_profile_info', ['primary_contact' => 0], 'user_id=' . $model->id . '')->execute();
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information saved successfully'));
                return $this->redirect(['approvedpr']);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('approvedpr/update', [
            'model' => $model,
        ]);
    }



}
