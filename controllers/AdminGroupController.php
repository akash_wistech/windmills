<?php

namespace app\controllers;

use Yii;
use app\models\AdminGroup;
use app\models\AdminGroupSearch;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

use app\models\StaffSearch;
use app\models\User;
use app\models\AdminGroupPermissions;
use app\models\AdminGroupPermissionsStaff;
use app\models\AdminGroupPermissionType;
use app\models\AdminMenu;

/**
* AdminGroupController implements the CRUD actions for AdminGroup model.
*/
class AdminGroupController extends DefController
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

  /**
  * Lists all AdminGroup models.
  * @return mixed
  */
  public function actionIndex()
  {
    $searchModel = new AdminGroupSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new model.
  */
  protected function newModel()
  {
    $model = new AdminGroup;
    return $model;
  }

  /**
  * Finds the AdminGroup model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return AdminGroup the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if ($id !== null && !empty($_POST['AdminGroup']['actions']) ) {

      $all_users = User::find()->where(['permission_group_id' => $id])->andWhere(['status' => 1])->andWhere(['user_type' => 10])->all();
      // dd($all_users);
      $total_ids = [];
      foreach( $all_users as $key => $user )
      {
        $total_ids[] = $user->id;
      }

      foreach ($total_ids as $id_value) {

        AdminGroupPermissionsStaff::deleteAll(['group_id' => $id_value]);
    
        foreach ($_POST['AdminGroup']['actions'] as $key => $permission_value) {
            $permission = new AdminGroupPermissionsStaff();
            $permission->group_id = $id_value;
            $permission->menu_id = $permission_value;
    
            if (!$permission->save()) {
                echo "Error saving permission for group ID: $id_value, menu ID: $permission_value<br>";
                foreach ($permission->errors as $error) {
                    echo $error[0] . "<br>";
                }
            }
        }
      }
    }
    if (($model = AdminGroup::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException(Yii::t('app','The requested page does not exist.'));
    }
  }

  public function actionStaffIndex()
  {
    $searchModel = new StaffSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('user-index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  public function actionUpdateStaffPermission($id)
  {
    if( !empty($_POST) )
    {
      //code for save
      $userID = User::findOne($id);
      $permissionGroups = AdminGroupPermissions::find()->where(['group_id' => $userID->permission_group_id])->all();

      if($_POST['AdminGroup']['actions'] != null){
        AdminGroupPermissionsStaff::deleteAll(['group_id'=>$id]);
        AdminGroupPermissionType::deleteAll(['group_id'=>$id]);
        foreach($_POST['AdminGroup']['actions'] as $key=>$val){
          $permission=new AdminGroupPermissionsStaff;
          $permission->group_id=$id;
          $permission->menu_id=$val;
          $permission->save();
          $menuItem=AdminMenu::find()->select(['controller_id','action_id'])->where(['id'=>$val])->asArray()->one();

          if($menuItem!=null && $menuItem['action_id']=='index'){
            $permissionListType=new AdminGroupPermissionType;
            $permissionListType->group_id=$id;
            $permissionListType->menu_id=$val;
            $permissionListType->controller_id=$menuItem['controller_id'];
            if(isset($_POST['AdminGroup']['list_type'][$val])){
              $permissionListType->list_type=$_POST['AdminGroup']['list_type'][$val];
            }
            $permissionListType->save();
          }
        }
        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
      }else{
        dd('You did not select any permission. Please Select at least 1 Permission.');
      }
    }else{
      $userID = User::findOne($id);

      $adminGroups = AdminGroup::find()->all();

      // $adminGroups = AdminGroupPermissionsStaff::find()->where(['group_id' => $userID->id])->all();
  
      return $this->render('user-update', [
        'adminGroups' => $adminGroups,
        'userID' => $userID,
      ]);
    }
  }
  
  public function actionResetPermission($id, $user_id)
  {
      $command = Yii::$app->db->createCommand()->update(
        User::tableName(),
        ['permission_group_id' => $id],
        ['user_type' => 10, 'id' => $user_id]
      )->execute();

      AdminGroupPermissionsStaff::deleteAll(['group_id' => $user_id]);

      $allSaved = true;
      $permissionGroups = AdminGroupPermissions::find()->where(['group_id' => $id])->all();
      foreach ($permissionGroups as $key => $value) {
          $permission = new AdminGroupPermissionsStaff;
          $permission->group_id = $user_id;
          $permission->menu_id = $value->menu_id;
          if (!$permission->save()) {
              $allSaved = false;
          }
      }
      return $allSaved;
  }
  
}
