<?php

namespace app\controllers;

use app\models\PropertiesSearch;
use app\models\ScheduleInspection;
use app\models\ValuationApproversData;
use app\models\ValuationReportsSearch;
use Yii;

use app\models\Valuation;
use app\models\StaffSearch;
use app\models\ValuationSearch;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\Response;
use yii\filters\VerbFilter;

class ReportsController extends DefController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Displays Setting Page.
     *
     * @return string
     */
    public function actionIndex()
    {
        die();
        $searchModel = new ValuationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('status-report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionStatusReport()
    {

        $searchModel = new ValuationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('status-report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionStatusReportSign()
    {
        $searchModel = new ValuationSearch();
        $searchModel->include_signature=true;
        $dataProvider = $searchModel->search_signature(Yii::$app->request->queryParams);
        return $this->render('status-report-signature', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionStatusReportNotSign()
    {

        $searchModel = new ValuationSearch();
        $searchModel->include_signature=false;
        $searchModel->valuation_status=5;
        $dataProvider = $searchModel->search_signature(Yii::$app->request->queryParams);
        return $this->render('status-report-not-signature', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }



    public function actionValuationStatus()
    {
        return $this->render('valuation-status');
    }

    public function actionClientRevenueAll()
    {

        if(!in_array( Yii::$app->user->identity->id, [1,14])) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['site/index']);
        }
        $holidays = array();
        $user=null;
        $type='All';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_revenue_all(Yii::$app->request->queryParams);
       // $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType();
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
        }else{
            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(0);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);
        }

        /*if (!empty($allvalues)){
            echo "<pre>";
            print_r($allvalues);
            die;
        }*/



        return $this->render('client_revenue_all', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'allvalues' => $allvalues['total_values'],
            'allvalues_count' => $allvalues['total_valuations_count'],
            'revevnue_all_types' => $revevnue_all_types,
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,

        ]);
    }

    public function actionClientRevenueBanks()
    {
        if(!in_array( Yii::$app->user->identity->id, [1,14])) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['site/index']);
        }
        $holidays = array();
        $user=null;
        $type='Banks';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_banks(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
        }else{
            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');

            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(0);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);
        }

        return $this->render('client_revenue', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
            'allvalues' => $revevnue_all_types['bank'],
        ]);
    }

    public function actionClientRevenueCorporate()
    {
        if(!in_array( Yii::$app->user->identity->id, [1,14])) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['site/index']);
        }
        $holidays = array();
        $user=null;
        $type='Corporate';

        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_corporate(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
        }else{
            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(0);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);
        }

        return $this->render('client_revenue', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'allvalues' => $revevnue_all_types['corporate'],
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
        ]);
    }

    public function actionClientRevenueIndividual()
    {
        if(!in_array( Yii::$app->user->identity->id, [1,14])) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['site/index']);
        }
        $user=null;
        $holidays = array();
        $type='Individual';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_individual(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
        }else{
            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $revevnue_all_types = Yii::$app->appHelperFunctions->getRevenueTotalByType(0);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);
        }

        return $this->render('client_revenue', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'allvalues' => $revevnue_all_types['individual'],
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
        ]);
    }
    public function actionClientRevenueByCities()
    {

        if(!in_array( Yii::$app->user->identity->id, [1,14])) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['site/index']);
        }
        $holidays = array();
        $user=null;
        $type='Individual';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_revenue_cities(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
        }else{
            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);

        }

        return $this->render('client_revenue_cities', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'allvalues' => $allvalues['total_values'],
            'allvalues_count' => $allvalues['total_valuations_count'],
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
        ]);
    }

    public function actionClientRevenueByValuers()
    {
        if(!in_array( Yii::$app->user->identity->id, [1,14])) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['site/index']);
        }
        $holidays = array();
        $user=null;
        $type='Individual';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_revenue_valuers(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
        }else{
            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);

        }

        return $this->render('client_revenue_valuers', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'allvalues' => $allvalues['total_values'],
            'allvalues_count' => $allvalues['total_valuations_count'],
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
        ]);
    }
    public function actionClientRevenueByPropertyType()
    {

        if(!in_array( Yii::$app->user->identity->id, [1,14])) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['site/index']);
        }
        $holidays = array();
        $user=null;
        $type='Individual';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_revenue_properties(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
        }else{
            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($start_date,$end_date,$holidays);

        }

        return $this->render('client_revenue_properties', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'allvalues' => $allvalues['total_values'],
            'allvalues_count' => $allvalues['total_valuations_count'],
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
            'total_working_days' => $total_working_days,
        ]);
    }


    public function actionLowHighValuations()
    {

        if(!in_array( Yii::$app->user->identity->id, [1,14])) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['site/index']);
        }
        $user=null;
        $change_average =0;
        $total_rows =0;
        $average =0;
        $type='Low-High Valuations';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_low_high_valuations(Yii::$app->request->queryParams);

        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {

            $query = Valuation::find()
                ->select([
                    Valuation::tableName().'.id',
                    Valuation::tableName().'.parent_id',
                    Valuation::tableName().'.client_id',
                    Valuation::tableName().'.property_id',
                    Valuation::tableName().'.building_info',
                    Valuation::tableName().'.service_officer_name',
                    'estimated_market_value'=>ValuationApproversData::tableName().'.estimated_market_value',
                    'approver'=>ValuationApproversData::tableName().'.created_by',
                ])
                ->leftJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                //->where([Valuation::tableName().'.valuation_status' => 5])
                ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
                ->andWhere([Valuation::tableName().'.revised_reason' => [1,2]]);

            $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $from_date = $date_array['start_date'];
            $to_date = $date_array['end_date'];
            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
            $all_data = $query->all();

            $total_rows = count($all_data);

            foreach ($all_data as $record){
                if($record->valuationParent->approverData->estimated_market_value > $record->estimated_market_value){
                    $updated_amount = ($record->valuationParent->approverData->estimated_market_value - $record->estimated_market_value);
                    $change_average = $change_average -number_format( ($updated_amount/$record->valuationParent->approverData->estimated_market_value) * 100,2). ' %';
                }else{
                    $updated_amount = ($record->estimated_market_value - $record->valuationParent->approverData->estimated_market_value);
                    $change_average = $change_average + number_format( ($updated_amount/$record->valuationParent->approverData->estimated_market_value) * 100,2). ' %';
                }

            }


        }else{

            $query = Valuation::find()
                ->select([
                    Valuation::tableName().'.id',
                    Valuation::tableName().'.parent_id',
                    Valuation::tableName().'.client_id',
                    Valuation::tableName().'.property_id',
                    Valuation::tableName().'.building_info',
                    Valuation::tableName().'.service_officer_name',
                    'estimated_market_value'=>ValuationApproversData::tableName().'.estimated_market_value',
                    'approver'=>ValuationApproversData::tableName().'.created_by',
                ])
                ->leftJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                //->where([Valuation::tableName().'.valuation_status' => 5])
                ->andWhere([ValuationApproversData::tableName().'.approver_type' => 'approver'])
                ->andWhere([Valuation::tableName().'.revised_reason' => [1,2]]);

            $start_date = '2021-04-28';
            $end_date = date('Y-m-d');
            $query->andFilterWhere([
                'between', 'instruction_date', $start_date, $end_date
            ]);
            $all_data = $query->all();

            $total_rows = count($all_data);

            foreach ($all_data as $record){
                if($record->valuationParent->approverData->estimated_market_value > $record->estimated_market_value){
                    $updated_amount = ($record->valuationParent->approverData->estimated_market_value - $record->estimated_market_value);
                    $change_average = $change_average -number_format( ($updated_amount/$record->valuationParent->approverData->estimated_market_value) * 100,2). ' %';
                }else{
                    $updated_amount = ($record->estimated_market_value - $record->valuationParent->approverData->estimated_market_value);
                    $change_average = $change_average + number_format( ($updated_amount/$record->valuationParent->approverData->estimated_market_value) * 100,2). ' %';
                }

            }


        }

        if($total_rows == 0){

            $average = 0;

        }else{
            $average = number_format(($change_average/$total_rows),2);
        }


        return $this->render('low-high-valuations', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'change_average' => $change_average,
            'average' => $average,
        ]);
    }

    public function actionModifiedValuations()
    {

        if(!in_array( Yii::$app->user->identity->id, [1,14])) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['site/index']);
        }
        $user=null;
        $type='Modified Valuations';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_modified_valuations(Yii::$app->request->queryParams);

        return $this->render('low-high-valuations', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
        ]);
    }

    public function actionErrorsValuations()
    {

        if(!in_array( Yii::$app->user->identity->id, [1,14])) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['site/index']);
        }
        $user=null;
        $type='Errors In Valuations';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_errors_valuations(Yii::$app->request->queryParams);


        return $this->render('low-high-valuations', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
        ]);
    }

    public function actionSoldDuplicates()
    {

        $user=null;
        //   $type='all sold , property placement , exposure, view_type';
        $type='Sold Duplicates';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_sold_duplicates(Yii::$app->request->queryParams);

        return $this->render('sold-duplicates', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
        ]);
    }

    public function actionListDuplicatesReference()
    {

        $user=null;
        //   $type='all sold , property placement , exposure, view_type';
        $type='List Duplicates';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_list_duplicates_reference(Yii::$app->request->queryParams);

        return $this->render('list-duplicates-reference', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
        ]);
    }

    public function actionListDuplicates()
    {

        $user=null;
        //   $type='all sold , property placement , exposure, view_type';
        $type='List Duplicates';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_list_duplicates(Yii::$app->request->queryParams);

        return $this->render('list-duplicates', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
        ]);
    }


    public function actionSoldNotUploaded()
    {

        $years = array_combine(range(date("Y"), 2018), range(date("Y"), 2018));
        $months = array(
            '01' => "January",
            '02' => "February",
            '03' => "March",
            '04' => "April",
            '05' => "May",
            '06' => "June",
            '07' => "July",
            '08' => "August",
            '09' => "September",
            '10' => "October",
            '11' => "Novemeber",
            '12' => "December"
        );

       /* $today = date('Y-m-d');
        $date = strtotime('2022-02-20');
        $day = date('l', strtotime('2022-02-20'));
        echo $day;
        die;*/
        $user=null;
        //   $type='all sold , property placement , exposure, view_type';
        $type='Sold Not Uploaded';
        $current_year = date('Y');
        $current_month = date('m');

        if(!empty(Yii::$app->request->queryParams)){
            $query = Yii::$app->request->queryParams;
            $current_year = $query['ValuationReportsSearch']['years'];
            $current_month = $query['ValuationReportsSearch']['months'];
        }

        $a_date = $current_year.'-'.$current_month."-01";
        $lastDayThisMonth =  date("Y-m-t", strtotime($a_date));

        $today_start_date = $current_year.'-'.$current_month.'-01 00:00:00';
        $today_end_date = $lastDayThisMonth.' 23:59:59';


        // Declare two dates
        $Date1 =  $current_year.'-'.$current_month.'-01';
        $Date2 = $lastDayThisMonth;




// Use strtotime function
        $Variable1 = strtotime($Date1);
        $Variable2 = strtotime($Date2);

// Use for loop to store dates into array
// 86400 sec = 24 hrs = 60*60*24 = 1 day
        for ($currentDate = $Variable1; $currentDate <= $Variable2;
             $currentDate += (86400)) {

            $Store = date('Y-m-d', $currentDate);
            $all_dates[] = $Store;
        }


        $sold_data = (new \yii\db\Query())
            ->select('created_at')
            ->from('sold_transaction')
            ->where(['between','created_at',$today_start_date, $today_end_date])
            ->all();
        $all_created_dates = array();

        foreach ($sold_data as $entry){
            $all_created_dates[] = date('Y-m-d',strtotime($entry['created_at']));
        }

        $results=array_diff($all_dates,$all_created_dates);

        $data = array();
        if(!empty($results)){
            foreach ($results as $key => $result){
                $day = date('l', strtotime($result));
               // if($day != 'Saturday' && $day != 'Sunday') {
                    $data[]['created_at'] = $result;
                //}

            }
        }

        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_list_duplicates(Yii::$app->request->queryParams);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => [
                'pageSize' => 50,
            ],
            'sort' => [
                'attributes' => ['created_at'],
            ],
        ]);


        return $this->render('sold-not-uploaded', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'years' => $years,
            'months' => $months,
            'type' => $type,
        ]);
    }


    public function actionTatReports()
    {

        if(!in_array( Yii::$app->user->identity->id, [1,14])) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['site/index']);
        }
        $sub_insp = 0;
        $sub_inst = 0;
        $insp_sub = 0;
        $app_insp = 0;
        $holidays = array();
        $user=null;
        $type='Tat Reports';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_tat(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {
            $query = Valuation::find()
                ->select([
                    Valuation::tableName().'.id',
                    Valuation::tableName().'.reference_number',
                    Valuation::tableName().'.instruction_date',
                    'submission_date' => ScheduleInspection::tableName().'.valuation_report_date',
                    'inspection_date' => ScheduleInspection::tableName().'.inspection_date',
                    Valuation::tableName().'.property_id'
                ])
                ->innerJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
               // ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->where([Valuation::tableName().'.valuation_status' => 5]);

            $date_array = Yii::$app->appHelperFunctions->getFilterDates(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);
            $from_date = $date_array['start_date'];
            $to_date = $date_array['end_date'];

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
           $all_data = $query->all();
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($date_array['start_date'],$date_array['end_date'],$holidays);
           foreach ($all_data as $record){

               $submission_date_sub_insp = date('Y-m-d', strtotime($record->approverData->created_at));;
               $diff_sub_insp = strtotime($submission_date_sub_insp) - strtotime($record->inspection_date);
               $sub_insp = $sub_insp + abs(round($diff_sub_insp / 86400));


               $submission_date_sub_ins = date('Y-m-d', strtotime($record->approverData->created_at));;
               $diff_sub_ins = strtotime($submission_date_sub_ins) - strtotime($record->instruction_date);
               $sub_inst = $sub_inst +  abs(round($diff_sub_ins / 86400));

               $diff_insp_sub = strtotime($record->inspection_date) - strtotime($record->instruction_date);
               $insp_sub =$insp_sub +  abs(round($diff_insp_sub / 86400));

               $submission_date_app_insp = date('Y-m-d', strtotime($record->approverData->created_at));;
               $diff_app_insp = strtotime($submission_date_app_insp) - strtotime($record->instruction_date);
               $app_insp = $app_insp + abs(round($diff_app_insp / 86400));
           }


        }else{

            $query = Valuation::find()
                ->select([
                    Valuation::tableName().'.id',
                    Valuation::tableName().'.reference_number',
                    Valuation::tableName().'.instruction_date',
                    'submission_date' => ScheduleInspection::tableName().'.valuation_report_date',
                    'inspection_date' => ScheduleInspection::tableName().'.inspection_date',
                    Valuation::tableName().'.property_id'
                ])
                ->innerJoin(ScheduleInspection::tableName(),ScheduleInspection::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->innerJoin(ValuationApproversData::tableName(),ValuationApproversData::tableName().'.valuation_id='.Valuation::tableName().'.id')
                ->where([Valuation::tableName().'.valuation_status' => 5]);

            $date_array = Yii::$app->appHelperFunctions->getFilterDates(1);
            $from_date = $date_array['start_date'];
            $to_date = $date_array['end_date'];

            $query->andFilterWhere([
                'between', 'instruction_date', $from_date, $to_date
            ]);
            $all_data = $query->all();
            $total_working_days = Yii::$app->appHelperFunctions->getWorkingDays($from_date,$to_date,$holidays);
            foreach ($all_data as $record){

                $submission_date_sub_insp = date('Y-m-d', strtotime($record->approverData->created_at));;
                $diff_sub_insp = strtotime($submission_date_sub_insp) - strtotime($record->inspection_date);
                $sub_insp = $sub_insp + abs(round($diff_sub_insp / 86400));


                $submission_date_sub_ins = date('Y-m-d', strtotime($record->approverData->created_at));;
                $diff_sub_ins = strtotime($submission_date_sub_ins) - strtotime($record->instruction_date);
                $sub_inst = $sub_inst +  abs(round($diff_sub_ins / 86400));

                $diff_insp_sub = strtotime($record->inspection_date) - strtotime($record->instruction_date);
                $insp_sub =$insp_sub +  abs(round($diff_insp_sub / 86400));

                $submission_date_app_insp = date('Y-m-d', strtotime($record->approverData->created_at));;
                $diff_app_insp = strtotime($submission_date_app_insp) - strtotime($record->instruction_date);
                $app_insp = $app_insp + abs(round($diff_app_insp / 86400));
            }


        }

        return $this->render('tat', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'sub_insp' => $sub_insp,
            'sub_inst' => $sub_inst,
            'insp_sub' => $insp_sub,
            'app_insp' => $app_insp,
            'total_rows' => (!empty($dataProvider->getTotalCount()) && $dataProvider->getTotalCount() > 0) ? $dataProvider->getTotalCount(): 1 ,
        ]);
    }

    public function actionTatAheadReports()
    {



/*
        if(!in_array( Yii::$app->user->identity->id, [1,14])) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['site/index']);
        }*/

        $user=null;
        $type='Ahead Of Time Reports';
        $action='tat-ahead-reports';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_tat_ahead(Yii::$app->request->queryParams);


        return $this->render('tat_ahead', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'action' => $action,
        ]);
    }
    public function actionTatOnTimeReports()
    {




      /*  if(!in_array( Yii::$app->user->identity->id, [1,14])) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['site/index']);
        }*/

        $user=null;
        $action='tat-on-time-reports';
        $type='On Time Reports';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_tat_ontime(Yii::$app->request->queryParams);


        return $this->render('tat_ahead', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'action' => $action,
        ]);
    }
    public function actionTatDelayReports()
    {




      /*  if(!in_array( Yii::$app->user->identity->id, [1,14])) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['site/index']);
        }*/

        $user=null;
        $type='Delay Reports';
        $action='tat-delay-reports';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_tat_delay(Yii::$app->request->queryParams);


        return $this->render('tat_ahead', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'action' => $action,
        ]);
    }

    public function actionGrossYieldByPropertyType()
    {

        /*  if(!in_array( Yii::$app->user->identity->id, [1,14])) {
              Yii::$app->getSession()->addFlash('error', "Permission denied!");
              return $this->redirect(['site/index']);
          }*/

        $searchModel_residential = new PropertiesSearch();
        $searchModel_residential->category = 1;
        $dataProvider_residential = $searchModel_residential->search(Yii::$app->request->queryParams);


        $searchModel_industrial = new PropertiesSearch();
        $searchModel_industrial->category = 2;
        $dataProvider_industrial = $searchModel_industrial->search(Yii::$app->request->queryParams);

        $searchModel_mixed_use = new PropertiesSearch();
        $searchModel_mixed_use->category = 3;
        $dataProvider_mixed_use = $searchModel_mixed_use->search(Yii::$app->request->queryParams);

        $searchModel_commercial = new PropertiesSearch();
        $searchModel_commercial->category = 4;
        $dataProvider_commercial = $searchModel_commercial->search(Yii::$app->request->queryParams);

        $searchModel_business = new PropertiesSearch();
        $searchModel_business->category = 5;
        $dataProvider_business = $searchModel_business->search(Yii::$app->request->queryParams);

        $searchModel_other = new PropertiesSearch();
        $searchModel_other->category = 6;
        $dataProvider_other = $searchModel_other->search(Yii::$app->request->queryParams);

        $user=null;
        $type='Property Type';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_gy_properties(Yii::$app->request->queryParams);
        $dataProvider_compare = $searchModel->search_gy_properties_compare(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);

        }else{
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);

        }

        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period_compare']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period_compare'] > 0) {
            $allvalues_compare = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period_compare']);

        }else{
            $allvalues_compare = Yii::$app->appHelperFunctions->getRevenueTotal(0);

        }

        return $this->render('gy_properties', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

            'dataProvider_residential' => $dataProvider_residential,

            'dataProvider_industrial' => $dataProvider_industrial,

            'dataProvider_mixed_use' => $dataProvider_mixed_use,

            'dataProvider_commercial' => $dataProvider_commercial,

            'dataProvider_business' => $dataProvider_business,

            'dataProvider_other' => $dataProvider_other,


           // 'dataProvider_compare' => $dataProvider_compare,
            'user' => $user,
            'type' => $type,
           // 'allvalues' => $allvalues['total_values'],
           // 'allvalues_compare' => $allvalues_compare['total_values'],
        ]);
    }
    public function actionGrossYieldByPropertyType_old_18_07_2022()
    {

      /*  if(!in_array( Yii::$app->user->identity->id, [1,14])) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['site/index']);
        }*/
        $user=null;
        $type='Individual';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_gy_properties(Yii::$app->request->queryParams);
        $dataProvider_compare = $searchModel->search_gy_properties_compare(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);

        }else{
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);

        }

        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period_compare']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period_compare'] > 0) {
            $allvalues_compare = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period_compare']);

        }else{
            $allvalues_compare = Yii::$app->appHelperFunctions->getRevenueTotal(0);

        }

        return $this->render('gy_properties', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProvider_compare' => $dataProvider_compare,
            'user' => $user,
            'type' => $type,
            'allvalues' => $allvalues['total_values'],
            'allvalues_compare' => $allvalues_compare['total_values'],
        ]);
    }

    public function actionGrossYieldByPropertyType_old()
    {

        /*  if(!in_array( Yii::$app->user->identity->id, [1,14])) {
              Yii::$app->getSession()->addFlash('error', "Permission denied!");
              return $this->redirect(['site/index']);
          }*/
        $user=null;
        $type='Individual';
        $searchModel = new ValuationReportsSearch();
        $dataProvider = $searchModel->search_gy_properties(Yii::$app->request->queryParams);
        if(isset(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']) && Yii::$app->request->queryParams['ValuationReportsSearch']['time_period'] > 0) {
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(Yii::$app->request->queryParams['ValuationReportsSearch']['time_period']);

        }else{
            $allvalues = Yii::$app->appHelperFunctions->getRevenueTotal(0);

        }

        return $this->render('gy_properties', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'user' => $user,
            'type' => $type,
            'allvalues' => $allvalues['total_values'],
        ]);
    }






}
