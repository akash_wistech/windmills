<?php

namespace app\controllers;

use Yii;

use yii\filters\VerbFilter;
use app\components\helpers\DefController;

/**
 * ApprovalAuthority implements the CRUD actions for Approval model.
 */
class ApprovalAuthorityController extends DefController
{
    public function actionIndex()
    {
        $searchModel = new ApprovalAuthoritySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    { 
        if(!empty(Yii::$app->request->post()))
        {

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    // public function actionUpdate($id)
    // {
    //     $model = AssetManager::findOne(['id' => $id]);
    //     if(!empty(Yii::$app->request->post()))
    //     {
    //         $model->transaction_type = Yii::$app->request->post()['transaction_type'];
    //         if( Yii::$app->request->post()['transaction_type'] == "asset" )
    //         {
    //             $model->asset_name = Yii::$app->request->post()['asset_name'];
    //             if( isset(Yii::$app->request->post()['asset_category_id']) )
    //             {
    //                 $parts = explode(",", Yii::$app->request->post()['asset_category_id']);
    
    //                 $model->category_id = $parts[0];
    //                 $model->category_name = $parts[1];
    //             }
    //             $model->depreciation = Yii::$app->request->post()['depreciation'];
    //             $model->brand = Yii::$app->request->post()['brand'];
    //         }elseif( Yii::$app->request->post()['transaction_type'] == "consumable" || Yii::$app->request->post()['transaction_type'] == "service"  )
    //         {
    //             $model->asset_name = Yii::$app->request->post()['expense_name'];
    //             if( isset(Yii::$app->request->post()['expense_category_id']) )
    //             {
    //                 $parts = explode(",", Yii::$app->request->post()['expense_category_id']);
    
    //                 $model->category_id = $parts[0];
    //                 $model->category_name = $parts[1];
    //             }
    //             $model->depreciation = NULL;
    //             $model->brand = Yii::$app->request->post()['brand'];
    //         }

    //         $model->status = Yii::$app->request->post()['status'];
    //         $model->save();
    //         return $this->redirect(['index']);
    //     }

    //     return $this->render('update', [
    //         'model' => $model,
    //     ]);
    // }

    // public function actionDelete($id)
    // {
    //     $model = AssetManager::findOne(['id' => $id])->delete();

    //     return $this->redirect(['index']);
    // }
}
