<?php

namespace app\controllers;

use app\models\SettingVal;
use app\models\SettingValForm;
use Yii;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Setting;
use app\models\SettingForm;

class SystemController extends DefController
{
  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['setting'],
        'rules' => [
          [
            'actions' => ['setting'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
    ];
  }

  /**
  * Displays Setting Page.
  *
  * @return string
  */
  public function actionSetting()
  {
    $this->checkSuperAdmin();
    $model=new SettingForm;
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      Yii::$app->getSession()->setFlash('success', 'Settings saved successfully');
      return $this->redirect(['setting']);
    }

    $sValues['SettingForm']=[];
    $settings=Setting::find()->all();
    foreach($settings as $record){
      $sValues['SettingForm'][$record['config_name']]=$record['config_value'];
    }
    $model->load($sValues);


    return $this->render('setting',['model'=>$model]);
  }
    public function actionValSetting()
    {
        $this->checkSuperAdmin();
        $model=new SettingValForm();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Settings saved successfully');
            return $this->redirect(['val-setting']);
        }

        $sValues['SettingForm']=[];
        $settings=SettingVal::find()->all();
        foreach($settings as $record){
            $sValues['SettingValForm'][$record['config_name']]=$record['config_value'];
        }


        $model->load($sValues);
       // $model->load($sValues);
       /* echo "<pre>";
        print_r($sValues);
        print_r($model);
        die;*/


        return $this->render('setting_val',['model'=>$model]);
    }
}
