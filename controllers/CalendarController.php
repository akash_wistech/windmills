<?php

namespace app\controllers;

use Yii;
use app\models\Calendar;
use app\models\CalendarSearch;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
* CalendarController implements the CRUD actions for Calendar model.
*/
class CalendarController extends DefController
{
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
    ];
  }

  /**
  * Lists all Calendar models.
  * @return mixed
  */
  public function actionIndex()
  {
    $searchModel = new CalendarSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new Calendar model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkSuperAdmin();
    $model = new Calendar();

    $request = Yii::$app->request;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->setFlash('success', Yii::t('app','Calendar saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }
    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
  * Updates an existing Calendar model.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionUpdate($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Calendar saved successfully'));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('update', [
      'model' => $model,
    ]);
  }

  /**
  * Creates a new model.
  */
  protected function newModel()
  {
    $model = new Calendar;
    return $model;
  }

  /**
  * Finds the Calendar model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Calendar the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = Calendar::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException(Yii::t('app','The requested page does not exist.'));
    }
  }
}
