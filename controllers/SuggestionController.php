<?php

namespace app\controllers;

use app\models\Units;
use Yii;
use yii\filters\AccessControl;
use app\components\helpers\DefController;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\AssetCategory;
use app\models\JobTitle;
use app\models\Company;
use app\models\Department;
use app\models\ActionLog;
use app\models\ActionLogCalendar;
use app\models\ActionLogManager;
use app\models\ActionLogEventInfo;
use app\models\ActionLogSubject;
use app\models\Prospect;
use app\models\User;
use app\models\Workflow;
use app\models\WorkflowStage;
use app\models\WorkflowDataItem;
use app\models\KanbanSearch;
use yii\data\Pagination;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\AssetManager;
use app\models\CategoryManager;

use yii\helpers\Json;
use yii\helpers\ArrayHelper;

class SuggestionController extends DefController
{
  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only' => ['job-title','department'],
        'rules' => [
          [
            'actions' => ['job-title','department'],
            'allow' => true,
            'roles' => ['@'],
          ],
        ],
      ],
    ];
  }

  /**
  * search all the job title
  * @return mixed
  */
  public function actionJobTitles($query=null)
  {
    $this->checkLogin();
    header('Content-type: application/json');
    $output_arrays=[];
    $results=JobTitle::find()
    ->select(['id','title'])
    ->where([
      'and',
      ['like','title',$query],
      ['status'=>1,'trashed'=>0]
    ])
    ->asArray()->all();
    if($results!=null){
      foreach($results as $result){
        $output_arrays[] = [
          'data'=>$result['id'],
          'value'=>trim($result['title']),
        ];
      }
    }
    // Send JSON to the client.
    echo json_encode(["suggestions"=>$output_arrays]);
    exit;
  }

    public function actionCommunityOptions($city_id)
    {
        $html = '';
        $results=Yii::$app->appHelperFunctions->getCommunitiesListCityArr($city_id);
        if($results!=null){
            $html.='<option value="">'.Yii::t('app','Select').'</option>';
            foreach($results as $key=>$val){
                $html.='<option value="'.$key.'">'.$val.'</option>';
            }
        }
        echo $html;
        exit;
    }

    public function actionSubCommunityOptions($community_id)
    {
        $html = '';
        $results=Yii::$app->appHelperFunctions->getSubCommunitiesListCityArr($community_id);
        if($results!=null){
            $html.='<option value="">'.Yii::t('app','Select').'</option>';
            foreach($results as $key=>$val){
                $html.='<option value="'.$key.'">'.$val.'</option>';
            }
        }
        echo $html;
        exit;
    }

  /**
  * search all the companies
  * @return mixed
  */
  public function actionCompany($query=null)
  {
    $this->checkLogin();
    header('Content-type: application/json');
    $output_arrays=[];
    $results=Company::find()
    ->select(['id','title'])
    ->where([
      'and',
      ['like','title',$query],
      ['status'=>1,'trashed'=>0]
    ])
    ->asArray()->all();
    if($results!=null){
      foreach($results as $result){
        $output_arrays[] = [
          'data'=>$result['id'],
          'value'=>trim($result['title']),
        ];
      }
    }
    // Send JSON to the client.
    echo json_encode(["suggestions"=>$output_arrays]);
    exit;
  }

  /**
  * search all the departments
  * @return mixed
  */
  public function actionDepartments($query=null)
  {
    $this->checkLogin();
    header('Content-type: application/json');
    $output_arrays=[];
    $results=Department::find()
    ->select(['id','title'])
    ->where([
      'and',
      ['like','title',$query],
      ['status'=>1,'trashed'=>0]
    ])
    ->asArray()->all();
    if($results!=null){
      foreach($results as $result){
        $output_arrays[] = [
          'data'=>$result['id'],
          'value'=>trim($result['title']),
        ];
      }
    }
    // Send JSON to the client.
    echo json_encode(["suggestions"=>$output_arrays]);
    exit;
  }

    public function actionUnits($query=null)
    {
        $this->checkLogin();
        header('Content-type: application/json');
        $output_arrays=[];
        $results=Units::find()
            ->select(['id','title'])
            ->where([
                'and',
                ['like','title',$query],
                ['status'=>1,'trashed'=>0]
            ])
            ->asArray()->all();
        if($results!=null){
            foreach($results as $result){
                $output_arrays[] = [
                    'data'=>$result['id'],
                    'value'=>trim($result['title']),
                ];
            }
        }
        // Send JSON to the client.
        echo json_encode(["suggestions"=>$output_arrays]);
        exit;
    }

  /**
  * search all the departments
  * @return mixed
  */
  public function actionModuleLookup($module=null,$query=null)
  {
    $this->checkLogin();
    header('Content-type: application/json');
    $output_arrays=[];
    if($module=='prospect'){
      $query = Prospect::find()
      ->select(['id','title'=>'CONCAT(firstname," ",lastname)'])
      ->where([
        'and',
        [
          'or',
          ['like','firstname',$query],
          ['like','lastname',$query],
        ],
        ['trashed'=>0]
      ]);
    }

    $results = $query->limit(20)->asArray()->all();
    if($results!=null){
      foreach($results as $result){
        $output_arrays[] = [
          'data'=>$result['id'],
          'value'=>trim($result['title']),
        ];
      }
    }
    // Send JSON to the client.
    echo json_encode(["suggestions"=>$output_arrays]);
    exit;
  }

  /**
  * Loads States / Province for a country
  *
  * @return string
  */
  public function actionZoneOptions($country_id)
  {
    $html = '';
    $results=Yii::$app->appHelperFunctions->getCountryZoneListArr($country_id);
    if($results!=null){
      $html.='<option value="">'.Yii::t('app','Select').'</option>';
      foreach($results as $key=>$val){
        $html.='<option value="'.$key.'">'.$val.'</option>';
      }
    }
    echo $html;
    exit;
  }

  /**
  * Loads Calendar Events
  *
  * @return string
  */
  public function actionCalendarData($start,$end,$calender_id=null)
  {
    $this->checkLogin();
    date_default_timezone_set('UTC');
    header('Content-type: application/json');
    $start=Yii::$app->helperFunctions->parseDateTime($start);
    $end=Yii::$app->helperFunctions->parseDateTime($end);
    $start=Yii::$app->helperFunctions->stripTime($start);
    $end=Yii::$app->helperFunctions->stripTime($end);
    $output_arrays=[];
    $subQueryCalender=null;;
    if($calender_id!=null){
      $subQueryCalender=ActionLogCalendar::find()->select(['action_log_id'])->where(['calendar_id'=>explode(",",$calender_id)]);
    }
    $subQueryAssigned=ActionLogManager::find()->select(['action_log_id'])->where(['staff_id'=>Yii::$app->user->identity->id]);
    $results=ActionLog::find()
      ->select([
        ActionLog::tableName().'.id',
        ActionLog::tableName().'.rec_type',
        ActionLog::tableName().'.comments',
        ActionLog::tableName().'.priority',
        ActionLog::tableName().'.visibility',
        ActionLogEventInfo::tableName().'.start_date',
        ActionLogEventInfo::tableName().'.end_date',
        ActionLogEventInfo::tableName().'.color_code',
        ActionLogEventInfo::tableName().'.all_day',
        ActionLogEventInfo::tableName().'.sub_type',
        ActionLogEventInfo::tableName().'.status',
        ActionLogSubject::tableName().'.subject',
        ActionLogSubject::tableName().'.due_date',
      ])
    ->leftJoin(ActionLogEventInfo::tableName(),ActionLogEventInfo::tableName().".action_log_id=".ActionLog::tableName().".id")
    ->leftJoin(ActionLogSubject::tableName(),ActionLogSubject::tableName().".action_log_id=".ActionLog::tableName().".id")
    ->where([
      ActionLog::tableName().'.rec_type'=>['action','calendar'],
      ActionLog::tableName().'.trashed'=>0,
    ])
    ->andWhere([
      'or',
      [
        'and',
        ['>=',ActionLogSubject::tableName().'.due_date',$start],
        ['<=',ActionLogSubject::tableName().'.due_date',$end],
      ],
      [
        'and',
        ['>=',ActionLogEventInfo::tableName().'.start_date',$start],
        ['<=',ActionLogEventInfo::tableName().'.end_date',$end],
      ],
    ])
    ->andFilterWhere([ActionLog::tableName().'.id'=>$subQueryAssigned])
    ->andFilterWhere([ActionLog::tableName().'.id'=>$subQueryCalender])
    ->asArray()->all();
    if($results!=null){
      foreach($results as $result){
        if($result['rec_type']=='action'){
          $output_arrays[] = [
            'id'=>$result['id'],
            'title'=>trim($result['subject']),
            'start'=>trim($result['due_date']),
          ];
        }
        elseif($result['rec_type']=='calendar'){
          $output_arrays[] = [
            'id'=>$result['id'],
            'title'=>trim($result['comments']),
            'start'=>trim($result['start_date']),
            'end'=>trim($result['end_date']),
            'color'=>trim($result['color_code']),
            'allDay'=>trim($result['all_day']),
            //'url'=>'',
          ];
        }
      }
    }
    // Send JSON to the client.
    echo json_encode($output_arrays);
  }

  /**
   * Loads board items for stage id sent
   * @return mixed
   */
  public function actionBoardData($module,$wf,$wfs)
  {
    if($module=='client'){

    }
    $searchModel=new KanbanSearch([]);
    $searchModel->module=$module;
    $searchModel->wf=$wf;
    $searchModel->wfi=explode(",",$wfs);

    $query = $searchModel->generateQuery([]);
    $query->orderBy(['sort_order'=>SORT_ASC]);

    $countQuery = clone $query;
    $pages = new Pagination(['pageSize'=>50,'totalCount' => $countQuery->count()]);
    $links = $pages->getLinks();

    $totalPages = ceil($pages->totalCount/$pages->pageSize);
    $pagesDone = $pages->page+1;

    $models = $query->offset($pages->offset)
        ->limit($pages->limit)
        ->all();
    $thisData=[];

    foreach ($models as $model)
    {
      $itemDetail = Yii::$app->workflowHelperFunctions->getBoardItemHtml($model);
			$thisData[]=[
				'wfsid'=>$model['wfsid'],
				'id'=>$model['id'],
				'title'=>$itemDetail,
			];
    }

    $response['data'] = $thisData;
    if($totalPages>0){
      $percentage=round(($pagesDone/$totalPages)*100);
    }else{
      $percentage=100;
    }
    $response['progress']=['totalPages'=>$totalPages,'pagesDone'=>$pagesDone,'percentage'=>$percentage];
    if($links!=null && isset($links['next'])){
      $response['url']=$links['next'];
    }
    echo json_encode($response);
    exit;
  }

  /**
  * Save workflow stage sort order
  */
  public function actionSaveBoardOrder()
  {
    $post = Yii::$app->request->post();
    $model = $this->findWorkFlowModel($post['id']);
    if($model!=null){
      $sortOrder = $post['sort_order'];
      $sortOrder = json_decode($sortOrder,true);
      if($sortOrder!=null){
        $connection = \Yii::$app->db;
        $i=1;
        foreach($sortOrder as $stageOrder){
          $stageId = $stageOrder['id'];
          $stageId = str_replace("_kb_","",$stageId);

          $connection->createCommand("update ".WorkflowStage::tableName()." set sort_order=:sort_order where workflow_id=:workflow_id and id=:id",
    			[':sort_order'=>$i,':workflow_id'=>$post['id'],':id'=>$stageId]
    			)->execute();
          $i++;
        }
      }
      $msg['ok']=['heading'=>Yii::t('app','Success'),'msg'=>Yii::t('app','Sort order saved successfully')];
      echo json_encode($msg);
      exit;
    }else{
      $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','Error while saving sort order')];
      echo json_encode($msg);
      exit;
    }
  }

  /**
  * Save workflow stage item sort order
  */
  public function actionSaveBoardItemOrder()
  {
    $post = Yii::$app->request->post();
    $model = $this->findWorkFlowModel($post['wfid']);
    if($model!=null){
      $stage = $this->findWorkFlowStageModel($post['stgid']);
      if($stage!=null){
        $sortOrder = $post['sort_order'];
        $sortOrder = json_decode($sortOrder,true);
        $connection = \Yii::$app->db;
        if($sortOrder!=null){
          $i=1;
          foreach($sortOrder as $itemOrder){
            $itemId = $itemOrder['id'];

            $connection->createCommand("update ".WorkflowDataItem::tableName()." set workflow_stage_id=:workflow_stage_id,sort_order=:sort_order where module_type=:module_type and module_id=:module_id and workflow_id=:workflow_id",
      			[':workflow_stage_id'=>$post['stgid'],':sort_order'=>$i,':module_type'=>$post['module'],':module_id'=>$itemId,':workflow_id'=>$post['wfid']]
      			)->execute();
            $i++;
          }
        }
        $msg['ok']=['heading'=>Yii::t('app','Success'),'msg'=>Yii::t('app','Item saved successfully')];
        echo json_encode($msg);
        exit;
      }else{
        $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','Error while saving item')];
        echo json_encode($msg);
        exit;
      }
    }else{
      $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>Yii::t('app','Process not found')];
      echo json_encode($msg);
      exit;
    }
  }

  /**
  * Finds the Workflow model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Workflow the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findWorkFlowModel($id)
  {
    if (($model = Workflow::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }

  /**
  * Finds the WorkflowStage model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return WorkflowStage the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findWorkFlowStageModel($id)
  {
    if (($model = WorkflowStage::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }

    public function actionScopeOfValuation($property_id)
    {
        $html = '';
        $result = \app\models\PropertiesFee::find()->where(['property_id'=>$property_id, 'ready' => 1])->asArray()->one();

        if($result!=null){
            $html.='<option value="">'.Yii::t('app','Select').'</option>';
            foreach(explode(',', $result['scope_of_work'] ) as $key => $val){
                $html.='<option value="'.$val.'">'.yii::$app->appHelperFunctions->getScopeOfWorkArr()[$val].'</option>';
            }
        }
        echo $html;
        exit;
    }
    
    

    public function actionGetClientData($id)
    {
      $client = \app\models\Company::find()->where(['id'=>$id])->one();
      $client_contacts = $this->getClientContacts($client->id);
      $response = [
        'client' => $client,
        'client_contacts' => $client_contacts,
      ];
      echo Json::encode($response);
      Yii::$app->end();
    }

    public function getClientContacts($client_id)
    {
      $query = \app\models\User::find()->where(['company_id'=>$client_id])->all();
      $options = '';
      if($query<>null){
        $options = '<option value="">select</option>';
        foreach($query as $key => $contact){
          $options .= '<option value="'.$contact->id.'">'.$contact->firstname.' '.$contact->lastname.'</option>';
        }
      }
      return $options;
    }
    
        public function getClientContactsArr($client_id)
        {
          return ArrayHelper::map(\app\models\User::find()->select(['id', 'CONCAT(firstname, " ", lastname) as fullname'])->where(['company_id'=>$client_id])->asArray()->all(), "id", "fullname");
        }


    public function actionGetClientAttendee($id, $client_counter)
    {
        $client_attendee_type = Html::textInput('SalesAndMarketing[client_attendee]['.$client_counter.'][client_attendee_type]', 0, [
            'class' => 'form-control d-none',
            'id' => 'client_attendee_type_'.$client_counter
        ]);
    
        $client_attendee_title = Html::dropDownList('SalesAndMarketing[client_attendee]['.$client_counter.'][client_attendee_title]', null, Yii::$app->smHelper->getTitleArr(), [
            'class' => 'form-control',
            'prompt' => 'Select an option',
            'id' => 'client_attendee_title_'.$client_counter,
            'required'=>true,
        ]);
    
        $client_contacts = $this->getClientContactsArr($id);
        
        $client_attendee_id = Html::dropDownList('SalesAndMarketing[client_attendee]['.$client_counter.'][client_attendee_id]', null, $client_contacts, [
            'class' => 'form-control',
            'prompt' => 'Select an option',
            'id' => 'client_attendee_id_'.$client_counter,
            'required'=>true,
        ]);
        
    
        $card = '
              <section class="card" style="border-top:2px solid #00897B;" id="client-other-attendees-'.$client_counter.'">
                  <div class="card-body">
                    <div class="row">
    
                    '.$client_attendee_type.'
                      <div class="col-5"><label class="control-label">Title</label>'.$client_attendee_title.'</div>
                      <div class="col-5"><label class="control-label">Attendee Name</label>'.$client_attendee_id.'</div>
                  <div class="col-2"><br><button data-delid="'.$client_counter.'" type="button" class="btn btn-sm btn-danger my-2 client-delete-btn">Remove</button></div>
                    </div>
                  </div>
              </section>
         ';
    
    
          return $this->asJson([
            'success' => true,
            'client_attend_card' => $card,
            // 'client_attend_card' => $selectInputHtml,
        ]);

    }


    public function actionGetWmAttendee($wm_counter)
    {

        $wm_attendee_type = Html::textInput('SalesAndMarketing[wm_attendee]['.$wm_counter.'][wm_attendee_type]', 0, [
            'class' => 'form-control d-none',
            'id' => 'wm_attendee_type_'.$wm_counter
        ]);
    
        $wm_attendee_title = Html::dropDownList('SalesAndMarketing[wm_attendee]['.$wm_counter.'][wm_attendee_title]', null, Yii::$app->smHelper->getTitleArr(), [
            'class' => 'form-control',
            'prompt' => 'Select an option',
            'id' => 'wm_attendee_title_'.$wm_counter,
            'required'=>true,
        ]);
    
        $wm_contacts = Yii::$app->smHelper->getAttendeesArr();
        
        $wm_attendee_id = Html::dropDownList('SalesAndMarketing[wm_attendee]['.$wm_counter.'][wm_attendee_id]', null, $wm_contacts, [
            'class' => 'form-control',
            'prompt' => 'Select an option',
            'id' => 'wm_attendee_id_'.$wm_counter,
            'required'=>true,
        ]);
        
    
        $card = '
              <section class="card" style="border-top:2px solid #455A64;" id="windmills-other-attendees-'.$wm_counter.'">
                  <div class="card-body">
                    <div class="row">
    
                    '.$wm_attendee_type.'
                      <div class="col-5"><label class="control-label">Title</label>'.$wm_attendee_title.'</div>
                      <div class="col-5"><label class="control-label">Attendee Name</label>'.$wm_attendee_id.'</div>
                  <div class="col-2"><br><button data-delid="'.$wm_counter.'" type="button" class="btn btn-sm btn-danger my-2 wm-delete-btn">Remove</button></div>
                    </div>
                  </div>
              </section>
        ';
    
    
          return $this->asJson([
            'success' => true,
            'wm_attend_card' => $card,
            // 'client_attend_card' => $selectInputHtml,
        ]);

    }
    
    

    public function actionGetclient($id)
    {
      // dd($id);
      $client = \app\models\Company::find()->where(['id'=>$id])->one();
      if($client->nick_name<>null){
        $name = $client->nick_name;
      }
      else{
        $name = $client->title;
      }
      echo Json::encode($name);
      Yii::$app->end();
    }
    
    
    
    public function actionGetkmimagehtml($atch_count)
    {

      $col = '<div class="col-2 my-2 upload-docs" id="image-row'.$atch_count.'">
        <div class="form-group">
            <a href="javascript:;" id="upload-document'.$atch_count.'"
                data-uploadid='.$atch_count.'  data-toggle="tooltip"
                class="img-thumbnail open-img-window" title="Upload Document">

                <img src="'.Yii::$app->params["uploadIcon"].'" alt=""
                    title="" data-placeholder="no_image.png" />
            </a>
            <a href="'.Yii::$app->params["uploadIcon"].'" class="mx-2" id="input-attachment-eye'.$atch_count.'" target="_blank">
                <i class="fa fa-eye text-primary"></i>
            </a>
            <input type="hidden"
                name="SalesAndMarketing[km_images]['.$atch_count.'][attachment]"
                id="input-attachment'.$atch_count.'" />
            </div>
        </div>';

        return $this->asJson([
          'success' => true,
          'col' => $col,
        ]);

    }

    public function actionGetvalimagehtml($atch_count)
    {

        $col = '<div class="col-2 my-2 upload-docs" id="image-row'.$atch_count.'">
        <div class="form-group">
            <a href="javascript:;" id="upload-document'.$atch_count.'"
                data-uploadid='.$atch_count.'  data-toggle="tooltip"
                class="img-thumbnail open-img-window" title="Upload Document">

                <img src="'.Yii::$app->params["uploadIcon"].'" alt=""
                    title="" data-placeholder="no_image.png" />
            </a>
            <a href="'.Yii::$app->params["uploadIcon"].'" class="mx-2" id="input-attachment-eye'.$atch_count.'" target="_blank">
                <i class="fa fa-eye text-primary"></i>
            </a>
            <input type="hidden"
                name="ReceivedDocs[km_images]['.$atch_count.'][attachment]"
                id="input-attachment'.$atch_count.'" />
            </div>
        </div>';

        return $this->asJson([
            'success' => true,
            'col' => $col,
        ]);

    }


    public function actionGetparkingimagehtml($atch_count)
    {

      $col = '<div class="col-2 my-2 upload-docs" id="image-row'.$atch_count.'">
                <div class="form-group">
                    <a href="javascript:;" id="upload-document'.$atch_count.'"
                        data-uploadid='.$atch_count.'  data-toggle="tooltip"
                        class="img-thumbnail open-img-window" title="Upload Document">

                        <img src="'.Yii::$app->params["uploadIcon"].'" alt=""
                            title="" data-placeholder="no_image.png" />
                    </a>
                    <a href="'.Yii::$app->params["uploadIcon"].'" class="mx-2" id="input-attachment-eye'.$atch_count.'" target="_blank">
                        <i class="fa fa-eye text-primary"></i>
                    </a>
                    <input type="hidden"
                        name="SalesAndMarketing[parking_images]['.$atch_count.'][attachment]"
                        id="input-attachment'.$atch_count.'" />
                </div>
            </div>';
    
        return $this->asJson([
            'success' => true,
            'col' => $col,
        ]);

    }    
    public function actionVonevaldata($ref_number)
    {
      $data = \app\models\Valuation::find()->where(['reference_number'=>$ref_number])->one();

      return $this->asJson([
        'success' => true,
        'data' => $data,
      ]);

    }
    
    

    public function actionGet_inspection_reasons($id)
    {
        $model = \app\models\Valuation::findOne($id);
        $html = $this->renderPartial('inspection-reasons-modal',[
          'model' => $model,
        ]);


        \Yii::$app->response->format = Response::FORMAT_HTML;

        return $html;
    }

    public function actionGet_pending_inspection_reasons($id)
    {
        $model = \app\models\Valuation::findOne($id);

        $html = $this->renderPartial('inspection-reasons-modal_new',[
            'model' => $model,
        ]);


        \Yii::$app->response->format = Response::FORMAT_HTML;

        return $html;
    }

    public function actionGetclientvalimagehtml($atch_count)
    {

        $col = '<div class="col-2 my-2 upload-docs" id="image-row'.$atch_count.'">
        <div class="form-group">
            <a href="javascript:;" id="upload-document'.$atch_count.'"
                data-uploadid='.$atch_count.'  data-toggle="tooltip"
                class="img-thumbnail open-img-window" title="Upload Document">

                <img src="'.Yii::$app->params["uploadIcon"].'" alt=""
                    title="" data-placeholder="no_image.png" />
            </a>
            <a href="'.Yii::$app->params["uploadIcon"].'" class="mx-2" id="input-attachment-eye'.$atch_count.'" target="_blank">
                <i class="fa fa-eye text-primary"></i>
            </a>
            <input type="hidden"
                name="ClientValuation[km_images]['.$atch_count.'][attachment]"
                id="input-attachment'.$atch_count.'" />
            </div>
        </div>';

        return $this->asJson([
            'success' => true,
            'col' => $col,
        ]);

    }

    public function actionAssetategories($query=null)
    {
      $this->checkLogin();
      header('Content-type: application/json');
      $output_arrays=[];
      $results=AssetCategory::find()
      ->select(['id','title'])
      ->where([
        'and',
        ['like','title',$query],
        ['status_verified'=>1,'trashed'=>0]
      ])
      ->asArray()->all();
      if($results!=null){
        foreach($results as $result){
          $output_arrays[] = [
            'data'=>$result['id'],
            'value'=>trim($result['title']),
          ];
        }
      }
      // Send JSON to the client.
      echo json_encode(["suggestions"=>$output_arrays]);
      exit;
    }

    
}
