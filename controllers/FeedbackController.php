<?php

namespace app\controllers;

use Yii;
use app\models\Feedback;
use app\models\FeedbackSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\helpers\DefController;

/**
 * FeedbackController implements the CRUD actions for Feedback model.
 */
class FeedbackController extends DefController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

 
    public function actionIndex()
    {
        $this->checkLogin();
        return $this->render('index');
    }
    
    public function actionValuations($id = null)
    {
        $this->checkLogin();
        $feedbackType = 0;
        $model = Feedback::find()->all();
        $searchModel = new FeedbackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $feedbackType);

        // return $this->render('index', [
        //     'searchModel' => $searchModel,
        //     'dataProvider' => $dataProvider,
        //     'model' => $model,
        // ]);

        if (Yii::$app->request->isPjax) {
            return $this->renderPartial('valuations', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
        } else {
            return $this->render('valuations', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
        }
        
    }
    public function actionProposals($id = null)
    {
        
        $this->checkLogin();
        $feedbackType = 1;
        $model = Feedback::find()->all();
        $searchModel = new FeedbackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $feedbackType);

        if (Yii::$app->request->isPjax) {
            return $this->renderPartial('proposals', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
        } else {
            return $this->render('proposals', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'model' => $model,
            ]);
        }
        
    }

  
    
    public function actionView($id)
    {
        // return $this->render('view', [
        //     'model' => $this->findModel($id),
        // ]);
    }

    public function actionSubmit()
    {
        $key = Yii::$app->request->getQueryParam('k');

        $this->layout='feedback';
        $model = $this->findModelKey($key);

        if ($model->load(Yii::$app->request->post())) {
            $model->status = 1;
            $model->quality_average = $model->qualityAverage;
            $model->updated_at = date('Y-m-d H:i:s');
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Thank you for your valuable feedback!');
                return $this->refresh();
            }
        }

        return $this->render('feedback_submit', ['model' => $model]);
    }


    
    public function actionCreate()
    {
        
    }


    
    public function actionUpdate($id)
    {
        
    }

 
    
    public function actionDelete($id)
    {
        // $this->findModel($id)->delete();

        // return $this->redirect(['index']);
    }


    
    protected function findModel($id)
    {
        if (($model = Feedback::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModelKey($key)
    {
        if (($model = Feedback::find()->where(['feedback_key' => $key])->one()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
