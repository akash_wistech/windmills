<?php

namespace app\controllers;

use app\models\ListingsTransactions;
use app\models\MergeProjectChilds;
use app\models\SoldTransaction;
use app\models\ValuationSelectedLists;
use Yii;
use app\models\MergeProject;
use app\models\MergeProjectSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\helpers\DefController;

/**
 * MergeProjectController implements the CRUD actions for MergeProject model.
 */
class MergeProjectController extends DefController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MergeProject models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MergeProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MergeProject model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MergeProject model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {





        $model = new MergeProject();

        if ($model->load(Yii::$app->request->post())) {

            $model->status_verified = $model->status;
            $this->StatusVerify($model);

            if($model->save()){
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                    'action' => 'data_created',
                    'verify_field' => 'status',
                ]);
                Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
                return $this->redirect(['index']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionRemoveDuplicates(){



        $mergprojects = MergeProjectChilds::find()->select('merge_id')->distinct()->asArray()->all();

        foreach ($mergprojects as $key => $mergproject_id) {


            $merge_data_set = SoldTransaction::find()
                ->where(['duplicate_status' => 0])
                ->andWhere(['building_info' => $mergproject_id['merge_id']])
                ->andWhere(['not', ['old_merge_id_building' => null]])
                ->andWhere(['not', ['old_merge_id' => null]])
                ->limit(20)
                ->asArray()
                ->all();



            if ($mergproject_id <> null) {
                $duplicate_list = array();
                $correct_list = array();
                foreach ($merge_data_set as $record) {


                    if ($record['reidin_ref_number'] <> null) {

                        $comparison = SoldTransaction::find()
                            ->where(['building_info' => $record['building_info'],
                                'reidin_ref_number' => $record['reidin_ref_number'],
                                'old_merge_id_building' => null,
                                'old_merge_id' => null
                            ])->one();


                    } else {
                        $comparison = SoldTransaction::find()
                            ->where(['building_info' => $record['building_info'],
                                'no_of_bedrooms' => $record['no_of_bedrooms'],
                                'built_up_area' => $record['built_up_area'],
                                'land_size' => $record['land_size'],
                                'transaction_date' => $record['transaction_date'],
                                'listings_price' => $record['listings_price'],
                                'price_per_sqt' => $record['price_per_sqt'],
                                'old_merge_id_building' => null,
                                'old_merge_id' => null
                            ])->one();
                    }
                    if ($comparison == null) {


                        if ($record['reidin_ref_number'] <> null) {

                            $comparison_2 = SoldTransaction::find()
                                ->where(['building_info' => $record['building_info'],
                                    'reidin_ref_number' => $record['reidin_ref_number'],
                                    'duplicate_status' => 6,
                                ])->one();


                        } else {
                            $comparison_2 = SoldTransaction::find()
                                ->where(['building_info' => $record['building_info'],
                                    'no_of_bedrooms' => $record['no_of_bedrooms'],
                                    'built_up_area' => $record['built_up_area'],
                                    'land_size' => $record['land_size'],
                                    'transaction_date' => $record['transaction_date'],
                                    'listings_price' => $record['listings_price'],
                                    'price_per_sqt' => $record['price_per_sqt'],
                                    'duplicate_status' => 6,
                                ])->one();
                        }

                        if ($comparison_2 == null) {
                            $correct_list[] = $record['id'];
                        } else {
                            $duplicate_list[] = $record['id'];
                        }
                    } else {
                        $duplicate_list[] = $record['id'];
                    }
                }
                Yii::$app->db->createCommand()->update('sold_transaction', ['duplicate_status' => 8], ['id' => $duplicate_list])->execute();
                Yii::$app->db->createCommand()->update('sold_transaction', ['duplicate_status' => 6], ['id' => $correct_list])->execute();
            }else{
                die('null');
            }
        }
        echo "<pre>";
        print_r($correct_list);
        print_r($duplicate_list);
        die;

    }

    public function actionRemoveDuplicatesList(){


        $merge_data_set = ListingsTransactions::find()
            ->where(['duplicate_status' => 0])
            ->andWhere(['building_info' => 149])
            ->andWhere(['not', ['old_merge_id_building' => null]])
            ->andWhere(['not', ['old_merge_id' => null]])
            ->limit(20)
            ->asArray()
            ->all();

        $duplicate_list= array();
        $correct_list= array();
        foreach ($merge_data_set as $record){

                $comparison = ListingsTransactions::find()
                    ->where(['building_info' => $record['building_info'],
                        'no_of_bedrooms' => $record['no_of_bedrooms'],
                        'built_up_area' => $record['built_up_area'],
                        'listings_price' => $record['listings_price'],
                        'listings_reference' => $record['listings_reference'],
                        'old_merge_id_building' => null,
                        'old_merge_id' => null
                    ])->one();

            if ($comparison == null) {
                $comparison_2 = ListingsTransactions::find()
                    ->where(['building_info' => $record['building_info'],
                        'no_of_bedrooms' => $record['no_of_bedrooms'],
                        'built_up_area' => $record['built_up_area'],
                        'listings_price' => $record['listings_price'],
                        'listings_reference' => $record['listings_reference'],
                        'duplicate_status' => 6,
                    ])->one();

                if($comparison_2 == null){
                    $correct_list[] = $record['id'];
                }else{
                    $duplicate_list[] = $record['id'];
                }


            }else{
                $duplicate_list[] = $record['id'];
            }
        }
        Yii::$app->db->createCommand()->update('listings_transactions', ['duplicate_status' => 8], ['id' => $duplicate_list])->execute();
        Yii::$app->db->createCommand()->update('listings_transactions', ['duplicate_status' => 6], ['id' => $correct_list])->execute();

        echo "<pre>";
        print_r($correct_list);
        print_r($duplicate_list);
        die;

    }

    public function actionSearchbuildings($q = null)
    {

        $results = (new \yii\db\Query())
            ->select('id,title')
            ->from('buildings')
            ->where(['LIKE', 'title', '%' . $q . '%', false])
            //->andWhere(['status'=> 1])
            ->all();


        //$results = []; // Query your database or data source to fetch search results based on $q

        // Format results for Select2
        foreach ($results as $result) {
           /* $status = ' (Unverified)';
            if ($result['status'] == 1) {
                $status = ' (Verified)';
            }*/
            $results[] = ['id' => $result['id'], 'text' => $result['title']];
        }

        return json_encode(['results' => $results]);
    }

    /**
     * Updates an existing MergeProject model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_verify_status = $model->status;
        $model->status_verified = $model->status;

        if ($model->load(Yii::$app->request->post())) {
            $model->status_verified = $model->status;
            $this->StatusVerify($model);

            if($model->save()){

                $this->makeHistory([
                    'model' => $model,
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                    'action' => 'data_updated',
                    'verify_field' => 'status',
                    'old_verify_status' => $old_verify_status,
                ]);
                Yii::$app->getSession()->addFlash('success', Yii::t('app','Information updated successfully'));
                return $this->redirect(['index']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MergeProject model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MergeProject model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MergeProject the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MergeProject::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
