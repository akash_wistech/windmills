<?php

namespace app\controllers;

use app\components\helpers\DefController;
use Yii;
use app\models\SubCommunities;
use app\models\SubCommunitiesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * SubCommunitiesController implements the CRUD actions for SubCommunities model.
 */
class SubCommunitiesController extends DefController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SubCommunities models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SubCommunitiesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SubCommunities model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SubCommunities model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SubCommunities();

        if ($model->load(Yii::$app->request->post())) {

            $this->StatusVerify($model);

            if($model->save()){

                $this->makeHistory([
                    'model' => $model, 
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                    'action' => 'data_created',
                    'verify_field' => 'status',
                ]);

                Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
                return $this->redirect(['index']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SubCommunities model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_verify_status = $model->status;

        if ($model->load(Yii::$app->request->post())) {
            
            $model->status_verified = $model->status;
            $this->StatusVerify($model);

            if($model->save()){

                $this->makeHistory([
                    'model' => $model, 
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                    'action' => 'data_updated',
                    'verify_field' => 'status',
                    'old_verify_status' => $old_verify_status,
                ]);

                Yii::$app->getSession()->addFlash('success', Yii::t('app','Information updated successfully'));
                return $this->redirect(['index']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * View Sub Comminities by search parameters and return units
     * @return array of search results
     * */

    public function actionSearchSubCommuinites()
    {
        ob_start();
        $out = [];

        if (Yii::$app->request->post('depdrop_parents')) {
            $parents = Yii::$app->request->post('depdrop_parents');
            if ($parents != null) {
                $community_id = $parents[0];
                $selected = false;

                if (Yii::$app->request->post('depdrop_all_params')) {
                    $params = Yii::$app->request->post('depdrop_all_params');
                }
                $sub_commuinities = SubCommunities::find()->where(['community' => $community_id])->all();


                if (isset($params['sub_community_value']) && $params['sub_community_value'] <> null) {
                    $selected =  $params['sub_community_value'];
                }

                $n = 1;
                foreach ($sub_commuinities as $sub_commuinity) {

                    $out[] = [
                        'id' => $sub_commuinity->id,
                        'name' => $sub_commuinity->title,
                    ];
                }

                echo Json::encode(['output' => $out, 'selected' => $selected]);
                return;
                exit();
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
        return;
        exit();
    }

    /**
     * Deletes an existing SubCommunities model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */


    /**
     * Finds the SubCommunities model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SubCommunities the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SubCommunities::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
