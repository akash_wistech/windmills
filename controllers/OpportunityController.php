<?php

namespace app\controllers;

use Yii;
use app\models\Opportunity;
use app\models\OpportunitySearch;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
* OpportunityController implements the CRUD actions for Opportunity model.
*/
class OpportunityController extends DefController
{
  public $rec_type='o';
  public $savedMsg='Opportunity saved successfully';
  /**
  * @inheritdoc
  */
  public function behaviors()
  {
    return [
      'verbs' => [
        'class' => VerbFilter::className(),
        'actions' => [
          'delete' => ['POST'],
        ],
      ],
    ];
  }

  /**
  * Lists all Opportunity models.
  * @return mixed
  */
  public function actionIndex()
  {
    $this->checkAdmin();
    $searchModel = new OpportunitySearch();
    $searchModel->rec_type=$this->rec_type;
    $dataProvider = $searchModel->searchMy(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel' => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
  * Creates a new Opportunity model.
  * If creation is successful, the browser will be redirected to the 'view' page.
  * @return mixed
  */
  public function actionCreate()
  {
    $this->checkSuperAdmin();
    $model = new Opportunity();
    $model->rec_type=$this->rec_type;

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->setFlash('success', Yii::t('app',$this->savedMsg));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                $msg['error']=['heading'=>Yii::t('app','Error'),'msg'=>$val];
                echo json_encode($msg);
                die();
                exit;
              }
            }
          }
        }
      }
    }
    return $this->render('create', [
      'model' => $model,
    ]);
  }

  /**
  * Updates an existing Opportunity model.
  * @param integer $id
  * @return mixed
  * @throws NotFoundHttpException if the model cannot be found
  */
  public function actionUpdate($id)
  {
    $this->checkLogin();
    $model = $this->findModel($id);
    $model->manager_id = ArrayHelper::map($model->managerIdz,"staff_id","staff_id");

    if ($model->load(Yii::$app->request->post())) {
      if($model->save()){
        Yii::$app->getSession()->addFlash('success', Yii::t('app',$this->savedMsg));
        return $this->redirect(['index']);
      }else{
        if($model->hasErrors()){
          foreach($model->getErrors() as $error){
            if(count($error)>0){
              foreach($error as $key=>$val){
                Yii::$app->getSession()->addFlash('error', $val);
              }
            }
          }
        }
      }
    }

    return $this->render('update', [
      'model' => $model,
    ]);
  }

  /**
  * Creates a new model.
  */
  protected function newModel()
  {
    $model = new Opportunity;
    return $model;
  }

  /**
  * Finds the Opportunity model based on its primary key value.
  * If the model is not found, a 404 HTTP exception will be thrown.
  * @param integer $id
  * @return Opportunity the loaded model
  * @throws NotFoundHttpException if the model cannot be found
  */
  protected function findModel($id)
  {
    if (($model = Opportunity::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
