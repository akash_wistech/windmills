<?php

namespace app\controllers;

use app\models\AutoCrmInvoices;
use app\models\AssetCategory;
use app\models\ClientDepartments;
use app\models\Clients;
use app\models\Company;
use app\models\ConfigurationFiles;
use app\models\CrmInspectProperty;
use app\models\CrmQuotationConfigraions;
use app\models\CrmReceivedDocs;
use app\models\CrmReceivedProperties;
use app\models\CrmScheduleInspection;
use app\models\CrmValuationConflict;
use app\models\InspectProperty;
use app\models\PreviousTransactions;
use app\models\ProposalMasterFile;
use app\models\QuotationFeeMasterFile;
use app\models\ReceivedDocs;
use app\models\Feedback;
use app\models\ReceivedDocsFiles;
use app\models\ScheduleInspection;
use app\models\Valuation;
use app\models\ValuationApproversData;
use app\models\ValuationConfiguration;
use app\models\ValuationConflict;
use app\models\ValuationDetail;
use app\models\ValuationOwners;
use app\models\CrmQuotationOwner;
use app\models\CrmReceivedDocsFiles;
use Yii;
use app\models\CrmQuotations;
use app\models\CrmQuotationsSearch;
use app\models\FeeDifference;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\helpers\Url;
use \app\modules\wisnotify\listners\NotifyEvent;

/**
 * CrmQuotationsController implements the CRUD actions for CrmQuotations model.
 */
class CrmQuotationsController extends DefController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    // 'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CrmQuotations models.
     * @return mixed
     */
    public function actionIndex()
    {



        $this->checkLogin();
        $searchModel = new CrmQuotationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionInquirersDetails()
    {
        $this->checkLogin();
        $model = new CrmQuotations();
        $model->reference_number = Yii::$app->crmQuotationHelperFunctions->uniqueReference;
        if ($model->load(Yii::$app->request->post())) {

            // changing the date format from 10-Oct-2023 t0 2023-10-10
            $model->instruction_date = date('Y-m-d', strtotime($model->instruction_date));
            //$model->target_date = date('Y-m-d', strtotime($model->target_date));

            // Email subject for non bank clients
            if ($model->email_subject === null || $model->email_subject === '') {
                if($model->type_of_service == 3){
                    $model->email_subject = "Windmills BCS Proposal - " . $model->reference_number;
                }else if($model->type_of_service == 2){
                    $model->email_subject = "Windmills Valuation Proposal - " . $model->reference_number;
                }else{
                    $model->email_subject = "Windmills Valuation Proposal - " . $model->reference_number;
                }
            } else {
                $model->email_subject = $model->email_subject . " - " . $model->reference_number;
            }

            if ($model->save()) {
                Yii::$app->crmQuotationHelperFunctions->addStatusHistory($model);
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                return $this->redirect(['crm-quotations/inquirers-details-duplicate?id=' . $model->id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }
        return $this->render('create', [
            //inquirers-details
            'model' => $model,
        ]);
    }


    public function actionInquirersDetailsDuplicate($id)
    {
        $this->checkLogin();
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            // changing the date format from 10-Oct-2023 t0 2023-10-10
            $model->instruction_date = date('Y-m-d', strtotime($model->instruction_date));
            // $model->target_date = date('Y-m-d', strtotime($model->target_date));

            // Email subject for non bank clients
            if ($model->email_subject === null || $model->email_subject === '') {
                if($model->type_of_service == 3){
                    $model->email_subject = "Windmills BCS Proposal - " . $model->reference_number;
                }else if($model->type_of_service == 2){
                    $model->email_subject = "Windmills Valuation Proposal - " . $model->reference_number;
                }else{
                    $model->email_subject = "Windmills Valuation Proposal - " . $model->reference_number;
                }
            } else {
                // $model->email_subject = $model->email_subject." - ".$model->reference_number;
                $model->email_subject = $model->email_subject;
            }


            if ($model->save()) {
                Yii::$app->crmQuotationHelperFunctions->addStatusHistory($model);
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                return $this->redirect(['crm-quotations/inquirers-details-duplicate?id=' . $model->id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }
        $existing_clients = Clients::find()->where(['status' => 1])->all();
        $existing_client_array=array();
        foreach ($existing_clients as $keyid =>$cname){

            $all_clients = \app\models\Company::find()
                ->where(['status' => 1])
                ->andWhere(['allow_for_valuation'=>1])
                ->andWhere(['main_client_id' => $cname->id])
                ->orderBy(['title' => SORT_ASC,])
                ->all();
            $client_array=array();
            foreach ($all_clients as $key => $client){
                if($client->client_department <> null &&  $client->client_department !=0){
                    $department_data = ClientDepartments::find()->where(['id' => $client->client_department])->one();
                    $client_array[$client->id] = $client->title. ' ('.($department_data->title).')';
                }else{
                    $client_array[$client->id] = $client->title;
                }

            }
            if(count($client_array) > 0) {
                $existing_client_array[$cname->title] = $client_array;
            }
        }
        return $this->render('inquirers-details-duplicate', [
            'model' => $model,
            'quotation' => $model,
            'existing_client_array' => $existing_client_array,
        ]);
    }


    /**
     * Creates a new CrmQuotations model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->checkLogin();
        $date = date("Y-m-d H:i:s");
        $model = new CrmQuotations();

        $typeOfQuot = Yii::$app->request->get('t');
        if($typeOfQuot == 'bcs'){
            $model->reference_number = Yii::$app->crmQuotationHelperFunctions->uniqueReferenceBcs;
            $model->type_of_service = 3;
        }else if($typeOfQuot == 'pme'){
            $model->reference_number = Yii::$app->crmQuotationHelperFunctions->uniqueReferencePme;
            $model->type_of_service = 2;
        }else{
            $model->reference_number = Yii::$app->crmQuotationHelperFunctions->uniqueReference;
            $model->type_of_service = 1;
        }


        $model->quotation_status = 0;
        $model->status_change_date = $date;
        $model->inquiry_received_date = $date;

        if ($model->load(Yii::$app->request->post())) {

            // if($typeOfQuot == 'bcs'){
            //     $model->type_of_service = 3;
            // }else if($typeOfQuot == 'pme'){
            //     $model->type_of_service = 2;
            // }else{
            //     $model->type_of_service = 1;
            // }
            if ($model->client_name <> null ){
                echo $model->client->client_type;
                $client_segment_data = \app\models\ClientSegmentFile::find()
                    ->where(['client_type' => $model->client->client_type])
                    ->one();
                if(isset($client_segment_data->fee_crm) && $client_segment_data->fee_crm > 0){
                    $model->fee_crm = $client_segment_data->fee_crm;
                }
            }


            // changing the date format from 10-Oct-2023 t0 2023-10-10
            $model->instruction_date = date('Y-m-d', strtotime($model->instruction_date));
            // $model->target_date = date('Y-m-d', strtotime($model->target_date));

            // Email subject for non bank clients
            if ($model->email_subject === null || $model->email_subject === '') {
                // $model->email_subject = "Windmills Valuation Proposal - " . $model->reference_number;
                if($typeOfQuot == 'bcs'){
                    $model->email_subject = "Windmills BCS Proposal - " . $model->reference_number;
                }else if($typeOfQuot == 'pme'){
                    $model->email_subject = "Windmills Valuation Proposal - " . $model->reference_number;
                }else{
                    $model->email_subject = "Windmills Valuation Proposal - " . $model->reference_number;
                }
            } else {
                $model->email_subject = $model->email_subject . " - " . $model->reference_number;
            }


            if ($model->advance_payment_terms == '25%' || $model->advance_payment_terms == '50%' || $model->advance_payment_terms == '75%') {
                $advance_payment = explode("%", $model->advance_payment_terms);
                $model->first_half_payment = $advance_payment[0];
                $model->second_half_payment = 100 - $advance_payment[0];
                $model->payment_status = 1;
            } else {
                $model->payment_status = 0;
            }

            // echo "<pre>"; print_r(Yii::$app->request->post());   echo "</pre>"; die();
            // dd($model);

            if ($model->save()) {
                /*          if($model->email_status_rcv !=1) {

                              $attachments[] = realpath(dirname(__FILE__).'/../uploads').'/about_windmills.pdf';
                              $attachments[] = realpath(dirname(__FILE__).'/../uploads').'/windmills_corporate_profile.pdf';
                              $notifyData = [
                                  'client' => $model->client,
                                  'attachments' => $attachments,
                                  'subject' => $model->email_subject,
                                  'uid' => 'crm'.$model->id,
                                  'replacements' => [
                                      '{clientName}' => $model->client->title,
                                  ],
                              ];

                              \app\modules\wisnotify\listners\NotifyEvent::fire23('quotation.received', $notifyData);

                              Yii::$app->db->createCommand()->update('crm_quotations', ['email_status_rcv' => 1], 'id=' . $model->id . '')->execute();*/
                //     }
                Yii::$app->crmQuotationHelperFunctions->addStatusHistory($model);

                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                // return $this->redirect(['crm-quotations/step_0?id=' . $model->id]);
                return $this->redirect(['crm-quotations/inquirers-details-duplicate?id=' . $model->id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }
        // die('here');
        $existing_clients = Clients::find()->where(['status' => 1])->all();
        $existing_client_array=array();
        foreach ($existing_clients as $keyid =>$cname){

            $all_clients = \app\models\Company::find()
                ->where(['status' => 1])
                ->andWhere(['allow_for_valuation'=>1])
                ->andWhere(['main_client_id' => $cname->id])
                ->orderBy(['title' => SORT_ASC,])
                ->all();
            $client_array=array();
            foreach ($all_clients as $key => $client){
                if($client->client_department <> null &&  $client->client_department !=0){
                    $department_data = ClientDepartments::find()->where(['id' => $client->client_department])->one();
                    $client_array[$client->id] = $client->title. ' ('.($department_data->title).')';
                }else{
                    $client_array[$client->id] = $client->title;
                }

            }
            if(count($client_array) > 0) {
                $existing_client_array[$cname->title] = $client_array;
            }
        }
        return $this->render('create', [
            'model' => $model,
            'existing_client_array' => $existing_client_array,
            'typeOfQuot' => $typeOfQuot,
        ]);
    }

    public function actionCreateAndMail()
    {
        $this->checkLogin();
        $date = date("Y-m-d H:i:s");
        $model = new CrmQuotations();

        $typeOfQuot = Yii::$app->request->get('t');
        if($typeOfQuot == 'bcs'){
            $model->reference_number = Yii::$app->crmQuotationHelperFunctions->uniqueReferenceBcs;
            $model->type_of_service = 3;
        }else if($typeOfQuot == 'pme'){
            $model->reference_number = Yii::$app->crmQuotationHelperFunctions->uniqueReferencePme;
            $model->type_of_service = 2;
        }else{
            $model->reference_number = Yii::$app->crmQuotationHelperFunctions->uniqueReference;
            $model->type_of_service = 1;
        }

        $model->quotation_status = 0;
        $model->status_change_date = $date;
        $model->inquiry_received_date = $date;

        if ($model->load(Yii::$app->request->post())) {


            // if($typeOfQuot == 'bcs'){
            //     $model->type_of_service = 3;
            // }else if($typeOfQuot == 'pme'){
            //     $model->type_of_service = 2;
            // }else{
            //     $model->type_of_service = 1;
            // }

            // changing the date format from 10-Oct-2023 t0 2023-10-10
            $model->instruction_date = date('Y-m-d', strtotime($model->instruction_date));
            //$model->target_date = date('Y-m-d', strtotime($model->target_date));

            // Email subject for non bank clients
            if ($model->email_subject === null || $model->email_subject === '') {
                if($typeOfQuot == 'bcs'){
                    $model->email_subject = "Windmills BCS Proposal - " . $model->reference_number;
                }else if($typeOfQuot == 'pme'){
                    $model->email_subject = "Windmills Valuation Proposal - " . $model->reference_number;
                }else{
                    $model->email_subject = "Windmills Valuation Proposal - " . $model->reference_number;
                }
            } else {
                $model->email_subject = $model->email_subject . " - " . $model->reference_number;
            }

            if ($model->advance_payment_terms == '25%' || $model->advance_payment_terms == '50%' || $model->advance_payment_terms == '75%') {
                $advance_payment = explode("%", $model->advance_payment_terms);
                $model->first_half_payment = $advance_payment[0];
                $model->second_half_payment = 100 - $advance_payment[0];
                $model->payment_status = 1;
            } else {
                $model->payment_status = 0;
            }

            // echo "<pre>"; print_r(Yii::$app->request->post());   echo "</pre>"; die();
            // dd($model);

            if ($model->save()) {

                if ($model->email_status_rcv != 1) {

                    $attachments[] = realpath(dirname(__FILE__) . '/../uploads') . '/about_windmills.pdf';
                    $attachments[] = realpath(dirname(__FILE__) . '/../uploads') . '/windmills_corporate_profile.pdf';
                    $notifyData = [
                        'client' => $model->client,
                        'attachments' => $attachments,
                        'subject' => $model->email_subject,
                        'uid' => 'crm' . $model->id,
                        'instructing_person_email' => $model->instructing_party_email,
                        'replacements' => [
                            '{clientName}' => $model->client->title,
                        ],
                    ];
                    if ($model->client_name > 0 && $model->email_subject <> null) {

                        \app\modules\wisnotify\listners\NotifyEvent::fire23('quotation.received', $notifyData);

                        Yii::$app->db->createCommand()->update('crm_quotations', ['email_status_rcv' => 1], 'id=' . $model->id . '')->execute();
                    }
                }

                /*echo $model->inspection_type;
                die;*/
                Yii::$app->db->createCommand()->update('crm_received_properties', ['inspection_type' => $model->inspection_type], 'quotation_id=' . $model->id . '')->execute();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved and email sent successfully'));
                return $this->redirect(['crm-quotations/step_0?id=' . $model->id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }
        // die('here');


        return $this->render('create', [
            'model' => $model,
            'typeOfQuot' => $typeOfQuot
            ,
        ]);
    }

    public function actionConfirmAndMail($id, $showAlert = null)
    {

        $this->checkLogin();
        $model = $this->findModel($id);
        $reference = $model->reference_number;
        $previous = AutoCrmInvoices::find()->where(['qoutation_id' => $id])->asArray()->one();
        if ($previous <> null) {
            // $reference = $previous['client_type'].' - '.$previous['prefix'].' - '.$previous['current_year'].' - '.$previous['current_year_number'].' - '.$previous['general_number'];
            $reference = $previous['client_type'] . ' - ' . $previous['prefix'] . ' - ' . $previous['current_year'] . ' - ' . $previous['current_year_number'];
        }
        if ($model->load(Yii::$app->request->post())) {

            // changing the date format from 10-Oct-2023 t0 2023-10-10
            $model->instruction_date = date('Y-m-d', strtotime($model->instruction_date));
            //$model->target_date = date('Y-m-d', strtotime($model->target_date));

            // Email subject for non bank clients
            if ($model->email_subject === null || $model->email_subject === '') {
                if($model->type_of_service == 3){
                    $model->email_subject = "Windmills BCS Proposal - " . $model->reference_number;
                }else if($model->type_of_service == 2){
                    $model->email_subject = "Windmills Valuation Proposal - " . $model->reference_number;
                }else{
                    $model->email_subject = "Windmills Valuation Proposal - " . $model->reference_number;
                }
            } else {
                // $model->email_subject = $model->email_subject." - ".$model->reference_number;
                $model->email_subject = $model->email_subject;
            }

            if ($model->advance_payment_terms == '25%' || $model->advance_payment_terms == '50%' || $model->advance_payment_terms == '75%') {
                $advance_payment = explode("%", $model->advance_payment_terms);
                $model->first_half_payment = $advance_payment[0];
                $model->second_half_payment = 100 - $advance_payment[0];
                $model->payment_status = 1;
            } else {
                $model->payment_status = 0;
            }
            // echo "<pre>"; print_r(Yii::$app->request->post());   echo "</pre>"; die();

            if ($model->save()) {

                if ($model->email_status_rcv != 1) {

                    $attachments[] = realpath(dirname(__FILE__) . '/../uploads') . '/about_windmills.pdf';
                    $attachments[] = realpath(dirname(__FILE__) . '/../uploads') . '/windmills_corporate_profile.pdf';
                    $notifyData = [
                        'client' => $model->client,
                        'attachments' => $attachments,
                        'subject' => $model->email_subject,
                        'uid' => 'crm' . $model->id,
                        'instructing_person_email' => $model->instructing_party_email,
                        'replacements' => [
                            '{clientName}' => $model->client->title,
                        ],
                    ];
                    if ($model->client_name > 0 && $model->email_subject <> null) {

                        \app\modules\wisnotify\listners\NotifyEvent::fire23('quotation.received', $notifyData);

                        Yii::$app->db->createCommand()->update('crm_quotations', ['email_status_rcv' => 1], 'id=' . $model->id . '')->execute();
                    }
                }

                /*echo $model->inspection_type;
                die;*/
                Yii::$app->db->createCommand()->update('crm_received_properties', ['inspection_type' => $model->inspection_type], 'quotation_id=' . $model->id . '')->execute();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved and email sent successfully'));
                return $this->redirect(['crm-quotations/step_0?id=' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }


        return $this->render('steps/_step0', [
            'model' => $model,
            'quotation' => $model,
            'showAlert' => $showAlert,
            'reference' => $reference
        ]);
    }

    public function actionStep_0_1($id, $showAlert = null)
    {

        $this->checkLogin();
        $model = $this->findModel($id);
        $model->scenario = 'step_0_1';
        $reference = $model->reference_number;
        $previous = AutoCrmInvoices::find()->where(['qoutation_id' => $id])->asArray()->one();
        if ($previous <> null) {
            // $reference = $previous['client_type'].' - '.$previous['prefix'].' - '.$previous['current_year'].' - '.$previous['current_year_number'].' - '.$previous['general_number'];
            $reference = $previous['client_type'] . ' - ' . $previous['prefix'] . ' - ' . $previous['current_year'] . ' - ' . $previous['current_year_number'];
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($model->advance_payment_terms == '25%' || $model->advance_payment_terms == '50%' || $model->advance_payment_terms == '75%') {
                $advance_payment = explode("%", $model->advance_payment_terms);
                $model->first_half_payment = $advance_payment[0];
                $model->second_half_payment = 100 - $advance_payment[0];
                $model->payment_status = 1;
            } else {
                $model->payment_status = 0;
            }
            // echo "<pre>"; print_r(Yii::$app->request->post());   echo "</pre>"; die();






            if ($model->save()) {

                if ($model->email_status_rcv != 1) {

                    $attachments[] = realpath(dirname(__FILE__) . '/../uploads') . '/about_windmills.pdf';
                    $attachments[] = realpath(dirname(__FILE__) . '/../uploads') . '/windmills_corporate_profile.pdf';
                    $notifyData = [
                        'client' => $model->client,
                        'attachments' => $attachments,
                        'subject' => $model->email_subject,
                        'uid' => 'crm' . $model->id,
                        'instructing_person_email' => $model->instructing_party_email,
                        'replacements' => [
                            '{clientName}' => $model->client->title,
                        ],
                    ];
                    if ($model->client_name > 0 && $model->email_subject <> null) {

                        \app\modules\wisnotify\listners\NotifyEvent::fire23('quotation.received', $notifyData);

                        Yii::$app->db->createCommand()->update('crm_quotations', ['email_status_rcv' => 1], 'id=' . $model->id . '')->execute();
                    }
                }

                /*echo $model->inspection_type;
                die;*/
                Yii::$app->db->createCommand()->update('crm_received_properties', ['inspection_type' => $model->inspection_type], 'quotation_id=' . $model->id . '')->execute();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                return $this->redirect(['crm-quotations/step_0_1?id=' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }


        return $this->render('steps/_step0_1', [
            'model' => $model,
            'quotation' => $model,
            'showAlert' => $showAlert,
            'reference' => $reference
        ]);
    }

    public function actionStep_0($id, $showAlert = null)
    {
        $this->checkLogin();
        $model = $this->findModel($id);
        $model->scenario = 'step_0';
        $invalid_status = [2, 5, 6, 7, 8];
        
        // if (in_array($model->quotation_status, $invalid_status)) {
        //     Yii::$app->session->setFlash('error', 'Permission Denied!');
        //     return $this->redirect(Yii::$app->request->referrer);
        // }
        if($model->id > 3001 && $model->email_status_rcv == 0){ 
            return $this->redirect(['crm-quotations/inquirers-details-duplicate?id=' . $id]);
        }
        
        $reference = $model->reference_number;
        $previous = AutoCrmInvoices::find()->where(['qoutation_id' => $id])->asArray()->one();
        if ($previous <> null) {
            // $reference = $previous['client_type'].' - '.$previous['prefix'].' - '.$previous['current_year'].' - '.$previous['current_year_number'].' - '.$previous['general_number'];
            $reference = $previous['client_type'] . ' - ' . $previous['prefix'] . ' - ' . $previous['current_year'] . ' - ' . $previous['current_year_number'];
        }
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->advance_payment_terms == '25%' || $model->advance_payment_terms == '50%' || $model->advance_payment_terms == '75%') {
                $advance_payment = explode("%", $model->advance_payment_terms);
                $model->first_half_payment = $advance_payment[0];
                $model->second_half_payment = 100 - $advance_payment[0];
                $model->payment_status = 1;
            } else {
                $model->payment_status = 0;
            }

            if ($model->reference_fee_3rdparty_bk === null) {
                $model->reference_fee_3rdparty_bk = $model->reference_fee_3rdparty;
            }

            if ($model->reference_fee_staff_bk === null) {
                $model->reference_fee_staff_bk = $model->reference_fee_staff;
            }

            // dd($model);
            if($model->no_of_units_same_building == 1){
                $model->same_building_discount = 0;
                $model->no_of_units_same_building_final_amount = 0;
                $model->aprvd_same_building_discount = 0;
                $model->aprvd_same_building_discount_amount = 0;
            }

            // echo "<pre>"; print_r(Yii::$app->request->post());   echo "</pre>"; die();

            // change date format from 10-Oct-2023 to 2023-10-10
            $model->target_date = date('Y-m-d', strtotime($model->target_date));
            $model->valuation_date = ($model->valuation_date != '') ? date('Y-m-d', strtotime($model->valuation_date)) : null;

            //dd($model);


            
            if ($model->save()) {
                /*echo $model->inspection_type;
                die;*/
                Yii::$app->db->createCommand()->update('crm_received_properties', ['inspection_type' => $model->inspection_type], 'quotation_id=' . $model->id . '')->execute();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
                return $this->redirect(['crm-quotations/step_0?id=' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        if($model->type_of_service == 3){
            $viewpath = 'steps/bcs/_step0';
        }else  if($model->type_of_service == 2){
            $viewpath = 'steps/valuation-pme/_step0';
        }else {
            $viewpath = 'steps/_step0';
        }


        return $this->render($viewpath, [
            'model' => $model,
            'quotation' => $model,
            'showAlert' => $showAlert,
            'reference' => $reference
        ]);
    }
    public function actionStep_1($id, $property_index)
    {
        $this->checkLogin();
        $quotation = $this->findModel($id);
        $model = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => $property_index])->one();
        
        if ($model !== null) {
            $model->city = Yii::$app->appHelperFunctions->emiratedListArr[$model->building->city];
            $model->community = $model->building->communities->title;
            $model->sub_community = $model->building->subCommunities->title;

        } else {
            // die('kj');
            $model = new CrmReceivedProperties();
        }
        $old_verify_status = $model->status_verified;
        if ($model->load(Yii::$app->request->post())) { 
            $model->quotation_id = $id;
            $model->property_index = $property_index;
            $model->inspection_type = $quotation->inspection_type;

            // added to make the enum field null
            if ($model->furnished == "") {
                $model->furnished = null;
            }
            if ($model->ready == "") {
                $model->ready = null;
            }
            
            if($id > 2236) {
            $model->number_of_comparables = yii::$app->quotationHelperFunctions->getNumberofComparablesValue($model->no_of_comparables_value);

            $model->no_of_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingValueArr($model->no_of_units_value);
            $model->no_of_residential_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingValueArr($model->no_of_res_units_value);
            $model->no_of_commercial_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingValueArr($model->no_of_com_units_value);
            $model->retails_units = yii::$app->quotationHelperFunctions->getNumberofUnitsInBuildingValueArr($model->no_of_ret_units_value);
            }


            if($quotation->type_of_service == 2){
                // dd($model);
                // $existingAssetCategory  = AssetCategory::find()->where(['title'=>trim($model->asset_category)])->one();

                if($model->asset_category!=null && $model->asset_category!=""){
                    $existingAssetCategory = AssetCategory::findOne(['title' => trim($model->asset_category)]);

                    if ($existingAssetCategory === null) {
                        $newCategory = new AssetCategory();
                        $newCategory->title = $model->asset_category;
                        $newCategory->status_verified=1;
                        $newCategory->save();
                        if($newCategory->save()){
                            $model->asset_category = $newCategory->id;
                        }
                        
                    } else {
                        $model->asset_category = $existingAssetCategory->id;
                    }
                }

            }
            
            $this->StatusVerify($model);
            if ($model->save()) {

                $this->makeHistory([
                    'model' => $model,
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                    'action' => 'data_updated',
                    'verify_field' => 'status_verified',
                    'old_verify_status' => $old_verify_status,
                ]);

                $previous = AutoCrmInvoices::find()->where(['qoutation_id' => $id])->asArray()->one();
                if ($previous <> null) {

                } else {
                    if ($model->property->prefix <> null) {
                        $prefix = Yii::$app->appHelperFunctions->crmInvoiceArr[$model->property->prefix];
                    } else {
                        $prefix = 'REQ';
                    }

                    if ($quotation->client->client_type == "bank") {
                        $clinet_type = 'B';
                    } else if ($quotation->client->client_type == "corporate") {
                        $clinet_type = 'C';
                    } else {
                        $clinet_type = 'I';
                    }

                    $previous_record = AutoCrmInvoices::find()
                        ->select([
                            'id',
                            'current_year_number',
                        ])->where(['current_year' => date('Y')])
                        ->orderBy(['id' => SORT_DESC])->asArray()->one();

                    $current_year_number = str_pad($previous_record['current_year_number'] + 1, 4, "0", STR_PAD_LEFT);
                    $prev_ref = explode("-", $quotation->reference_number);
                    if ($id > 1019) {
                        $prefix_number = trim($prev_ref[4]);
                    } else {
                        $prefix_number = $prev_ref[2];
                    }



                    $crm_inv_reference = new AutoCrmInvoices();
                    $crm_inv_reference->qoutation_id = $id;
                    $crm_inv_reference->client_type = $clinet_type;
                    $crm_inv_reference->prefix = $prefix;
                    $crm_inv_reference->current_year = date('Y');
                    $crm_inv_reference->current_year_number = $current_year_number;
                    $crm_inv_reference->general_number = $prefix_number;
                    $crm_inv_reference->created_at = date('Y-m-d h:i:s');
                    $crm_inv_reference->created_by = \Yii::$app->user->identity->id;
                    $crm_inv_reference->save();
                }
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['crm-quotations/step_1?id=' . $id . '&property_index=' . $property_index]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        if($quotation->type_of_service == 3){
            $viewpath = 'steps/bcs/_step1';
        }else  if($quotation->type_of_service == 2){
            $viewpath = 'steps/valuation-pme/_step1';
        }else {
            $viewpath = 'steps/_step1';
        }


        return $this->render($viewpath, [
            'model' => $model,
            'quotation' => $quotation,
            'property_index' => $property_index,
        ]);
    }

    public function StatusVerify($model = null)
    {
        if ($model->status_verified == 1) {
            $model->status_verified_at = date('Y-m-d H:i:s');
            $model->status_verified_by = Yii::$app->user->identity->id;
        }
    }



    public function saveStatusHistory($data, $status_action)
    {
        if ($data) {
            $h_model = new \app\models\History;
            $h_model->model_id = $data['model']->id;
            $h_model->action = $status_action;
            $h_model->model_name = $data['model_name'];
            $h_model->created_at = date('Y-m-d H:i:s');
            $h_model->updated_at = date('Y-m-d H:i:s');
            $h_model->created_by = Yii::$app->user->id;
            $h_model->updated_by = Yii::$app->user->id;

            if (isset($data['file_type']) && $data['file_type'] <> null) {
                $h_model->file_type = $data['file_type'];
                $h_model->rec_type = 'masterfile';
            }
            $h_model->save();
        }
    }

    public function actionStep_2($id, $property_index)
    {
        $this->checkLogin();
        $quotation = $this->findModel($id);
        $model = CrmReceivedDocs::find()->where(['quotation_id' => $id, 'property_index' => $property_index])->one();
        $valuation = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => $property_index])->one();


        if ($model !== null) {

        } else {
            $model = new CrmReceivedDocs();
        }

        if ($model->load(Yii::$app->request->post())) {
            $date = date("Y-m-d H:i:s");
            $model->quotation_id = $id;
            $model->property_index = $property_index;
            $quotation->quotation_status = 14;
            $quotation->document_requested_date = $date;
            $quotation->save();
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['crm-quotations/step_2?id=' . $id . '&property_index=' . $property_index]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        if($quotation->type_of_service == 3){
            $viewpath = 'steps/bcs/_step2';
        }else  if($quotation->type_of_service == 2){
            $viewpath = 'steps/valuation-pme/_step2';
        }else {
            $viewpath = 'steps/_step2';
        }

        return $this->render($viewpath, [
            'model' => $model,
            'quotation' => $quotation,
            'valuation' => $valuation,
            'property_index' => $property_index,
        ]);
    }

    public function actionStep_3($id, $property_index)
    {
        $this->checkLogin();
        $quotation = $this->findModel($id);
        $model = CrmValuationConflict::find()->where(['quotation_id' => $id, 'property_index' => $property_index])->one();
        $valuation = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => $property_index])->one();

        $client = Company::find()->where(['id' => $quotation->client_name])->one();

        $related_to_buyer_check = PreviousTransactions::find()->where(['client_name' => $valuation->client_name_passport])->all();

        $owners_in_valuation = ArrayHelper::map(\app\models\CrmQuotationOwner::find()->where(['quotation_id' => $id, 'property_index' => $property_index])->all(), 'id', 'name');
        $owners_valutions_names = ArrayHelper::map(\app\models\ValuationOwners::find()->where(['name' => $owners_in_valuation])->all(), 'id', 'name');


        $owners_previous_data = PreviousTransactions::find()->where(['client_name' => $owners_valutions_names])->all();

        $related_to_client_check = PreviousTransactions::find()->where(['client_name' => $client->title])->all();
        $related_to_property_check = PreviousTransactions::find()->where(['building_info' => $valuation->building_info, 'unit_number' => $valuation->unit_number])->all();



        if ($model !== null) {
        } else {
            $model = new CrmValuationConflict();
            if ($related_to_buyer_check <> null && count($related_to_buyer_check) > 0) {
                $model->related_to_buyer = 'Yes';
            }
            if ($owners_previous_data <> null && count($owners_previous_data) > 0) {
                $model->related_to_seller = 'Yes';
            }
            if ($related_to_client_check <> null && count($related_to_client_check) > 0) {
                $model->related_to_client = 'Yes';
            }
            if ($related_to_property_check <> null && count($related_to_property_check) > 0) {
                $model->related_to_property = 'Yes';
            }

        }

        if ($model->load(Yii::$app->request->post())) {
            $model->quotation_id = $id;
            $model->property_index = $property_index;
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['crm-quotations/step_3?id=' . $id . '&property_index=' . $property_index]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error)
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) { {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                }
            }
        }

        return $this->render('steps/_step3', [
            'model' => $model,
            'valuation' => $valuation,
            'quotation' => $quotation,
            'buyer_data' => $related_to_buyer_check,
            'seller_data' => $owners_previous_data,
            'client_data' => $related_to_client_check,
            'property_data' => $related_to_property_check,
            'property_index' => $property_index,
        ]);
    }

    public function actionStep_4($id, $property_index)
    {
        $this->checkLogin();
        $quotation = $this->findModel($id);
        $model = CrmScheduleInspection::find()->where(['quotation_id' => $id, 'property_index' => $property_index])->one();
        $valuation = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => $property_index])->one();

        if ($model !== null) {

        } else {
            $model = new CrmScheduleInspection();
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->quotation_id = $id;
            $model->property_index = $property_index;
            if ($model->save()) {

                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['crm-quotations/step_4?id=' . $id . '&property_index=' . $property_index]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('steps/_step4', [
            'model' => $model,
            'quotation' => $quotation,
            'property_index' => $property_index,
            'valuation' => $valuation,
        ]);
    }

    public function actionStep_5($id, $property_index)
    {
        $this->checkLogin();
        $quotation = $this->findModel($id);
        $model = CrmInspectProperty::find()->where(['quotation_id' => $id, 'property_index' => $property_index])->one();
        $valuation = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => $property_index])->one();
        if ($model !== null) {

        } else {

            $model = new CrmInspectProperty();
            $model->makani_number = $valuation->building->makani_number;
            $model->latitude = $valuation->building->latitude;
            $model->longitude = $valuation->building->longitude;
            $model->property_placement = $valuation->building->property_placement;
            $model->property_visibility = $valuation->building->property_visibility;
            $model->property_exposure = $valuation->building->property_exposure;
            $model->property_category = $valuation->building->property_category;
            $model->property_condition = $valuation->building->property_condition;
            $model->development_type = $valuation->building->development_type;
            $model->finished_status = $valuation->building->finished_status;
            $model->developer_id = $valuation->building->developer_id;
            $model->estimated_age = $valuation->building->estimated_age;
            $model->estimated_remaining_life = $valuation->property->age - $valuation->building->estimated_age;
            $model->number_of_basement = $valuation->building->number_of_basement;
            $model->pool = $valuation->building->pool;
            $model->other_facilities = $valuation->building->other_facilities;
            $model->completion_status = $valuation->building->completion_status;
            $model->landscaping = $valuation->building->landscaping;
            $model->white_goods = $valuation->building->white_goods;
            $model->furnished = $valuation->building->furnished;
            $model->utilities_connected = $valuation->building->utilities_connected;
            $model->location_highway_drive = $valuation->building->location_highway_drive;
            $model->location_school_drive = $valuation->building->location_school_drive;
            $model->location_mall_drive = $valuation->building->location_mall_drive;
            $model->location_sea_drive = $valuation->building->location_sea_drive;
            $model->location_park_drive = $valuation->building->location_park_drive;
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->quotation_id = $id;
            $model->property_index = $property_index;
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['crm-quotations/step_5?id=' . $id . '&property_index=' . $property_index]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }


        }

        return $this->render('steps/_step5', [
            'model' => $model,
            'valuation' => $valuation,
            'quotation' => $quotation,
            'property_index' => $property_index,
        ]);
    }

    public function actionStep_6($id, $property_index)
    {
        $this->checkLogin();
        $quotation = $this->findModel($id);
        $configuration = CrmInspectProperty::find()->where(['quotation_id' => $id, 'property_index' => $property_index])->one();
        $valuation = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => $property_index])->one();
        $model = CrmQuotationConfigraions::find()->where(['quotation_id' => $id, 'property_index' => $property_index])->one();

        if ($model !== null) {

        } else {
            $model = new CrmQuotationConfigraions();
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->quotation_id = $id;
            $model->property_index = $property_index;

            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['crm-quotations/step_6?id=' . $id . '&property_index=' . $property_index]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('steps/_step6', [
            'model' => $model,
            'valuation' => $valuation,
            'configuration' => $configuration,
            'quotation' => $quotation,
            'property_index' => $property_index,
        ]);
    }

    public function actionStep_7($id, $property_index)
    {
        $this->checkLogin();
        $quotation = $this->findModel($id);
        $model = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => $property_index])->one();

        /* $inspect_property = CrmInspectProperty::find()->where(['quotation_id' => $id,'property_index' => $property_index])->one();
         $schecdule_inspections = CrmScheduleInspection::find()->where(['quotation_id' => $id,'property_index' => $property_index])->one();
         //Fee criteria
         $fee_parameters = array();
         $fee_parameters['clientType']= $quotation->client->client_type;
         $fee_parameters['paymentTerms']= $quotation->advance_payment_terms;
         $fee_parameters['property']= $model->property_id;
         $fee_parameters['city']= $model->building->city;
         $fee_parameters['tenure']= $model->tenure;
         $fee_parameters['complexity']= $model->complexity;
         $fee_parameters['repeat_valuation']= $model->repeat_valuation;
         $fee_parameters['built_up_area']= $inspect_property->built_up_area;
         $fee_parameters['type_of_valuation']= $schecdule_inspections->7inspection_type;
         $fee_parameters['number_of_comparables']= $model->number_of_comparables;
         $fee_parameters['no_of_units']= $model->no_of_units;
         $fee_parameters['land_size']= $model->land_size;


         $amount = yii::$app->propertyCalcHelperFunction->getAmount_new($fee_parameters);


         if(isset($amount['fee'])){
             $model->recommended_fee = $amount['fee'];
         }else{
             $model->recommended_fee = $amount['fee'];
         }


         if($model->quotation_fee <> null){

         }else{
             $model->quotation_fee = $model->recommended_fee;
         }

         if($model->toe_fee <> null && $model->toe_fee !=='0.00'){

         }else{
             $model->toe_fee = $model->quotation_fee;
         }
         if($model->toe_tat <> null && $model->toe_fee !=='0.00'){

         }else{
             $model->toe_tat = $model->tat;
         }*/
         
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['crm-quotations/step_7?id=' . $id . '&property_index=' . $property_index]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }


        return $this->render('steps/_step7', [
            'model' => $model,
            'quotation' => $quotation,
            'property_index' => $property_index,
        ]);
    }


    public function actionStep_ar_quotation($id)
    {
        return $this->redirect(Url::to(['crm-quotations/step_8', 'id' => $id, 'step_num' => 'step_ar_quotations']));
    }

    public function actionStep_ar_toe($id)
    {
        return $this->redirect(Url::to(['crm-quotations/step_8', 'id' => $id, 'step_num' => 'step_ar_toe']));
    }


    public function actionStep_8($id, $step_num = null, $quotation_status = null)
    {
        $this->checkLogin();
        $model = $this->findModel($id);
        $quotation = $this->findModel($id);
        $old_qid_30012024 = 2284;

        if($quotation_status <> null) {
            $date = date("Y-m-d H:i:s");
            if($quotation_status == 2){            
                if ($model->quotation_sent_date == null) {
                    // if ($model->quotation_status <= 0 || $model->quotation_status == 14) {
                        $condition = false;
                        if ($quotation->client_name > 0 && $quotation->email_subject <> null && $quotation->client_name != 112 && $quotation->client_name != 118945 && $quotation->client_name != 54514) {
                            $model->quotationPdf($condition);
                        }
                        $date = date("Y-m-d H:i:s");
                        $model->quotation_status = 1;
                        $model->status_change_date = $date;
                        $model->quotation_sent_date = $date;
                        $model->quotation_approved_by = \Yii::$app->user->identity->id;
                        yii::$app->getsession()->addFlash('success', yii::t('app', 'Quotation send successfully'));
                        
                        if($quotation->client_name == 9166) {
                            // create and send feedback form link
                            $this->createFeedback($quotation);
                        }
                    // }
                }
                $model->approved_date = $date;
                $model->quotation_status = 2;
                $model->status_approve = 'Approve';            
            }
            else if($quotation_status == 17){
                $model->quotation_status = 17;
                $model->status_approve = 'Recommended';
                $model->quotation_recommended_date = $date;
            } else {}
        }

        //if($quotation->quotation_status >=0 && $quotation->quotation_status < 1) {

        if ($model->id > 3030) { 
            $properties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->groupBy('multiscope_id')->all();
        }else {
            $properties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();
        }    
       
       

      //  dd($properties);   

        $total_amount = 0;
        $final_amount = 0;

        $ref_fee_total = 0;

        $no_of_properties = count($properties);
        if ($quotation->reference_fee_3rdparty != 0) {
            $reference_fee_3rdparty = $quotation->reference_fee_3rdparty / $no_of_properties;
        }
        
        

        $amount_summary = array();
        $count = 0;
        foreach ($properties as $key => $property_data) {
            $merge= array();
           // echo "d".$property_data->multiscope_id;
            // die;
            // dd($property_data);
            $inspect_property = CrmInspectProperty::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
            $schecdule_inspections = CrmScheduleInspection::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
            if($quotation->type_of_service == 3) {

                // $property = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'scope_of_service_saved' => $property_data->scope_of_service_saved])->one();

                $property = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'multiscope_id' => $property_data->multiscope_id])->one();
            
            }
            else{
                $property = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
            }

            // assign fee master file property type based on property id    
            $property_type = yii::$app->quotationHelperFunctions->getPropertyTypeApproach($property->property_id, $property->valuation_approach);
            
            // get multi scope property
            $multiScopeProperty = \app\models\CrmReceivedProperties::find()
                ->where(['multiscope_id' => $property->multiscope_id])
                ->orderBy(['id' => SORT_DESC])->all();

               

            //Fee criteria
            $fee_parameters = array();
            $fee_parameters['building_title'] = $property->building->title;
            $fee_parameters['clientType'] = $quotation->client->client_type;
            $fee_parameters['paymentTerms'] = $quotation->advance_payment_terms;
            $fee_parameters['property'] = $property->property_id;
            $fee_parameters['property_type'] = $property_type;
            $fee_parameters['property_category'] = $property->property_category;
            $fee_parameters['community'] = $property->building->community;
            $fee_parameters['sub_community'] = $property->building->sub_community;
            $fee_parameters['city'] = $property->building->city;
            $fee_parameters['tenure'] = $property->tenure;
            $fee_parameters['complexity'] = str_replace('-', '_', $property->complexity);
            // if($quotation->scope_of_service != 6 && $quotation->scope_of_service != 7 && $quotation->scope_of_service != 8  ){
            //     $fee_parameters['complexity'] = str_replace('-', '_', $property->complexity);
            // }
            $fee_parameters['repeat_valuation'] = $property->repeat_valuation;
            if ($quotation->id > 508) {
                $fee_parameters['built_up_area'] = $property->built_up_area;
            } else {
                $fee_parameters['built_up_area'] = $inspect_property->built_up_area;
            }
            /* echo $quotation->inspection_type;
             die;*/
            $fee_parameters['net_leasable_area'] = $property->net_leasable_area;
            $fee_parameters['type_of_valuation'] = $property->inspection_type;
            $fee_parameters['scope_of_service'] = $property->scope_of_service;
            $fee_parameters['number_of_comparables'] = $property->number_of_comparables;
            $fee_parameters['no_of_units'] = $property->no_of_units;
            $fee_parameters['land_size'] = ($property->land_size <> null) ? $property->land_size : 0;
            $fee_parameters['upgrades'] = ($property->upgrades <> null) ? $property->upgrades : 0;
            $fee_parameters['other_intended_users'] = ($quotation->other_intended_users_check_1 == "Yes") ? 'yes' : 'no';
            $fee_parameters['approach_type'] = Yii::$app->appHelperFunctions->valuationApproachListArr[$property->valuation_approach];
            $fee_parameters['number_of_types'] = $property->no_of_unit_types;
            $fee_parameters['typical_floors'] = $property->typical_floors;
            $fee_parameters['basement_floors'] = $property->basement_floors;
            $fee_parameters['mezzanine_floors'] = $property->mezzanine_floors;
            $fee_parameters['parking_floors'] = $property->parking_floors;
            $fee_parameters['no_of_rooms'] = $property->number_of_rooms_building;
            $fee_parameters['last_three_years_finance'] = $property->last_3_years_finance;
            $fee_parameters['ten_years_projections'] = $property->projections_10_years;
            $fee_parameters['restaurant'] = $property->restaurant;
            $fee_parameters['ballrooms'] = $property->ballrooms;
            $fee_parameters['atms'] = $property->atms;
            $fee_parameters['retails_units'] = $property->retails_units;
            $fee_parameters['night_clubs'] = $property->night_clubs;
            $fee_parameters['bars'] = $property->bars;
            $fee_parameters['health_club'] = ($property->health_club <> null) ? 'yes' : 'no';
            $fee_parameters['meeting_rooms'] = $property->meeting_rooms;
            $fee_parameters['spa'] = ($property->spa <> null) ? 'yes' : 'no';
            $fee_parameters['beach_access'] = ($property->beach_access <> null) ? 'yes' : 'no';
            $fee_parameters['parking_sale'] = $property->parking_sale;
            // $fee_parameters['drawings_available'] = $property->drawings_available;
            $fee_parameters['civil_drawing_available'] = $property->civil_drawing_available;
            $fee_parameters['mechanical_drawing_available'] = $property->mechanical_drawing_available;
            $fee_parameters['electrical_drawing_available'] = $property->electrical_drawing_available;
            $fee_parameters['plumbing_drawing_available'] = $property->plumbing_drawing_available;
            $fee_parameters['hvac_drawing_available'] = $property->hvac_drawing_available;
            $fee_parameters['no_of_units_value'] = $property->no_of_units_value;
            $fee_parameters['no_of_res_units_value'] = $property->no_of_res_units_value;
            $fee_parameters['no_of_com_units_value'] = $property->no_of_com_units_value;
            $fee_parameters['no_of_ret_units_value'] = $property->no_of_ret_units_value;
            $fee_parameters['no_of_warehouse_units_value'] = $property->no_of_warehouse_units_value;
            $fee_parameters['no_of_comparables_value'] = $property->no_of_comparables_value;
            $fee_parameters['asset_category'] = $property->asset_category;
            $fee_parameters['no_of_asset'] = $property->no_of_asset;
            $fee_parameters['asset_complexity'] = $property->asset_complexity;
            $fee_parameters['working_days'] = $property->working_days;
            $fee_parameters['asset_age'] = $property->asset_age;
            $fee_parameters['location'] = $quotation->location;
            $fee_parameters['no_of_location'] = $quotation->no_of_location;
            $fee_parameters['key'] = $key;

            
            // $amount = yii::$app->propertyCalcHelperFunction->getAmount_auto($fee_parameters);
            //  $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoIncome($fee_parameters);

            /* echo $amount;
             die;*/
            // $ApprovedArr = [2, 4];
            $ApprovedArr = [];

            if (!in_array($model->quotation_status, $ApprovedArr)) {

                if ($model->id > 3029) { 


                    if($quotation->type_of_service == 1) {

                        foreach ($multiScopeProperty as $key => $multiproperty) {

                            if ($property->valuation_approach == 2) {
                                $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoIncome($fee_parameters, $model);
                                $amount_summary[$multiproperty->id] = yii::$app->propertySummaryHelper->getAmount_autoIncome($fee_parameters, $model);
                            } else if ($property->valuation_approach == 3) {
                                $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoProfit($fee_parameters, $model);
                                $amount_summary[$multiproperty->id] = yii::$app->propertySummaryHelper->getAmount_autoProfit($fee_parameters, $model);
                            } else if ($property->valuation_approach == 1) {
                                $amount = yii::$app->propertyCalcHelperFunction->getAmount_auto($fee_parameters, $model);
                                $amount_summary[$multiproperty->id] = yii::$app->propertySummaryHelper->getAmount_auto($fee_parameters, $model);    
                            }else{

                                $amount = yii::$app->propertyCalcHelperFunction->getAmount_auto($fee_parameters, $model);
                                $amount_summary[$multiproperty->id] = yii::$app->propertySummaryHelper->getAmount_auto($fee_parameters, $model);   
            
                                // $amount = $amount['fee'];
                                // echo 'Scope of service is not selected!';
                                // die;
                            }

                            $merge[$multiproperty->id]=$amount;

                        }


                    }
                    else if($quotation->type_of_service == 2) {
                        foreach ($multiScopeProperty as $key => $multiproperty) {
                            $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoPME($fee_parameters, $model);
                            $amount_summary[$multiproperty->id] = yii::$app->propertySummaryHelper->getAmount_autoPME($fee_parameters, $model);

                            $merge[$multiproperty->id]=$amount;
                        }

                    }
                    else if($quotation->type_of_service == 3) {
                        $flag_check=0;

                        foreach ($multiScopeProperty as $key => $multiproperty) {
                          
                            if($multiproperty->scope_of_service_saved == 6) {
                                // for rfs
                                $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoRFS($fee_parameters, $model); 
                                $amount_summary[$multiproperty->id] = yii::$app->propertySummaryHelper->getAmount_autoRFS($fee_parameters, $model);                  
                            } else if($multiproperty->scope_of_service_saved == 7) { 
                                // for scs
                                $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoSCS($fee_parameters, $model);
                                $amount_summary[$multiproperty->id] = yii::$app->propertySummaryHelper->getAmount_autoSCS($fee_parameters, $model);
                            } else if($multiproperty->scope_of_service_saved == 8) { 
                                // for bca
                                $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoBCA($fee_parameters, $model);
                                $amount_summary[$multiproperty->id] = yii::$app->propertySummaryHelper->getAmount_autoBCA($fee_parameters, $model);                            
                            }                             
                            else if($multiproperty->scope_of_service_saved == 9) {
                                // for rica
                                $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoRICA($fee_parameters, $model);
                                $amount_summary[$multiproperty->id] = yii::$app->propertySummaryHelper->getAmount_autoRICA($fee_parameters, $model);
                            }  
                            else if($multiproperty->scope_of_service_saved == 10) {
                                // for ts
                                $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoTS($fee_parameters, $model);
                                $amount_summary[$multiproperty->id] = yii::$app->propertySummaryHelper->getAmount_autoTS($fee_parameters, $model);
                            } 
                            else if($multiproperty->scope_of_service_saved == 11) {
                                // for err
                                $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoERR($fee_parameters, $model);
                                $amount_summary[$multiproperty->id] = yii::$app->propertySummaryHelper->getAmount_autoERR($fee_parameters, $model);
                            }  
                            else if($multiproperty->scope_of_service_saved == 12) {
                                // for arct
                                $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoARCT($fee_parameters, $model);
                                $amount_summary[$multiproperty->id] = yii::$app->propertySummaryHelper->getAmount_autoARCT($fee_parameters, $model);
                            } 
                            else if($multiproperty->scope_of_service_saved == 13) {
                                // for rfv
                                $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoRFV($fee_parameters, $model);
                                $amount_summary[$multiproperty->id] = yii::$app->propertySummaryHelper->getAmount_autoRFV($fee_parameters, $model);
                            }else{
                                echo 'Scope of service not selected';
                                die;
                            } 
                           
                            
                            $merge[$multiproperty->id]=$amount;
                           
                            

                           $flag_check++;
                            
                            
                            
                        }
                    }

                    
                    
                    

                }else if ($model->id > 1203 && $model->id <= 3029) {

                    if ($property->valuation_approach == 2) {
                        $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoIncome($fee_parameters, $model);
                        $amount_summary[$key] = yii::$app->propertySummaryHelper->getAmount_autoIncome($fee_parameters, $model);
                    } else if ($property->valuation_approach == 3) {
                        $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoProfit($fee_parameters, $model);
                        $amount_summary[$key] = yii::$app->propertySummaryHelper->getAmount_autoProfit($fee_parameters, $model);
                    }
                    else if($property->scope_of_service == 6) {
                        // for rfs
                        $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoRFS($fee_parameters, $model); 
                        $amount_summary[$key] = yii::$app->propertySummaryHelper->getAmount_autoRFS($fee_parameters, $model);                    
                    } else if($property->scope_of_service == 7) { 
                        // for scs
                        $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoSCS($fee_parameters, $model);
                        $amount_summary[$key] = yii::$app->propertySummaryHelper->getAmount_autoSCS($fee_parameters, $model);
                    } else if($property->scope_of_service == 8) {
                        // for bca
                        $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoBCA($fee_parameters, $model);
                        $amount_summary[$key] = yii::$app->propertySummaryHelper->getAmount_autoBCA($fee_parameters, $model);
                    }  
                    else if($quotation->scope_of_service == 9) {
                        // for rica
                        $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoRICA($fee_parameters, $model);
                        $amount_summary[$key] = yii::$app->propertySummaryHelper->getAmount_autoRICA($fee_parameters, $model);
                    }  
                    else if($quotation->scope_of_service == 10) {
                        // for ts
                        $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoTS($fee_parameters, $model);
                        $amount_summary[$key] = yii::$app->propertySummaryHelper->getAmount_autoTS($fee_parameters, $model);
                    }  
                    else if($quotation->scope_of_service == 11) {
                        // for err
                        $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoERR($fee_parameters, $model);
                        $amount_summary[$key] = yii::$app->propertySummaryHelper->getAmount_autoERR($fee_parameters, $model);
                    }  
                    else if($quotation->scope_of_service == 12) {
                        // for arct
                        $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoARCT($fee_parameters, $model);
                        $amount_summary[$key] = yii::$app->propertySummaryHelper->getAmount_autoARCT($fee_parameters, $model);
                    }  
                    else if($quotation->scope_of_service == 13) {
                        // for rfv
                        $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoRFV($fee_parameters, $model);
                        $amount_summary[$key] = yii::$app->propertySummaryHelper->getAmount_autoRFV($fee_parameters, $model);
                    }  
                    else {
                        $amount = yii::$app->propertyCalcHelperFunction->getAmount_auto($fee_parameters, $model);
                        $amount_summary[$key] = yii::$app->propertySummaryHelper->getAmount_auto($fee_parameters, $model);

                    }
                 } else {
                    $amount = yii::$app->propertyCalcHelperFunction->getAmount_new($fee_parameters, $model);
                    $amount_summary[$key] = yii::$app->propertySummaryHelper->getAmount_new($fee_parameters, $model);

                    $amount = $amount['fee'];
                }              

                
               // echo "<pre>";
              //  echo $multiproperty->scope_of_service_saved.'<br>';
                //   print_r($merge);
                // die;
     

                if(!empty($merge)){
                
                    foreach($merge as $keys =>$rec){
                        
                        // $property_multiscope = CrmReceivedProperties::find()->where(['multiscope_id' => $property->multiscope_id, 'scope_of_service_saved' => $keys])->one(); 
                        $property_multiscope = CrmReceivedProperties::find()->where(['id' => $keys])->one(); 
                        
                     
                       //  Yii::$app->db->createCommand()->update('crm_received_properties', ['recommended_fee' => $rec], 'id=' . $property_multiscope->id . '')->execute();

                        
                        // $tat = $property->property->tat;
                        if($quotation->type_of_service == 2) {
                            $tat = $property_multiscope->working_days;
                        }
                        else{
                            $tat = $property_multiscope->property->tat;
                        }
                        
                        
                        Yii::$app->db->createCommand()->update('crm_received_properties', ['recommended_fee' => $rec, 'tat' => $tat], 'id=' . $property_multiscope->id . '')->execute();
                        
                        // Reference fee
                        $ref_fee_staff = number_format((float)($rec * $quotation->reference_fee_staff / 100),2);
                        $ref_fee_3rdparty = ($quotation->reference_fee_3rdparty_check == 0) ? number_format((float)($rec * $quotation->reference_fee_3rdparty / 100),2) : number_format($reference_fee_3rdparty,2);

                        $ref_fee_total = $ref_fee_staff + $ref_fee_3rdparty;
                        

                        $ref_flag = $quotation->reference_flag;
                        

                        if($model->id > $old_qid_30012024){
                            //$property_multiscope->recommended_fee = $property_multiscope->recommended_fee + $ref_fee_total;

                            if ($property_multiscope->quotation_fee <> null) {


                                if ($ref_fee_staff != $property->reference_fee_staff || $ref_fee_3rdparty != $property->reference_fee_3rdparty) {
                                     //$property->quotation_fee = $property->recommended_fee;
                                    // $property->bo_recommended_fee = $property->recommended_fee;
                                    // $property->reviewed_fee = $property->recommended_fee;
                                }
            
            
                            } else {
                                // $property->quotation_fee = $property->recommended_fee;
                                // $property_multiscope->quotation_fee = $property_multiscope->recommended_fee;
                                // $property_multiscope->bo_recommended_fee = $property_multiscope->recommended_fee;
                                // $property_multiscope->reviewed_fee = $property_multiscope->recommended_fee;

                                $property_multiscope->quotation_fee = $rec;
                                $property_multiscope->bo_recommended_fee = $rec;
                                $property_multiscope->reviewed_fee = $rec;

                                Yii::$app->db->createCommand()->update('crm_received_properties', ['quotation_fee' => $property_multiscope->quotation_fee, 'bo_recommended_fee' => $property_multiscope->bo_recommended_fee, 'reviewed_fee' => $property_multiscope->reviewed_fee], 'id=' . $property_multiscope->id . '')->execute();
            
                                if ($ref_fee_total > 0) {
                                    $model->reference_flag = 1;
                                }
                            }
                            // dd($property->recommended_fee,$property->quotation_fee,$property->bo_recommended_fee,$property->reviewed_fee);
                            
                        } else {
                            if ($property_multiscope->quotation_fee <> null) {


                                if ($ref_fee_staff != $property->reference_fee_staff || $ref_fee_3rdparty != $property->reference_fee_3rdparty) {
                                    $property_multiscope->quotation_fee = $property_multiscope->recommended_fee + $ref_fee_total;
                                    $property_multiscope->bo_recommended_fee = $property_multiscope->recommended_fee + $ref_fee_total;
                                    $property_multiscope->reviewed_fee = $property_multiscope->recommended_fee + $ref_fee_total;
                                }
            
            
                            } else {
                                // $property->quotation_fee = $property->recommended_fee;
                                $property_multiscope->quotation_fee = $property_multiscope->recommended_fee + $ref_fee_total;
                                $property_multiscope->bo_recommended_fee = $property_multiscope->recommended_fee + $ref_fee_total;
                                $property_multiscope->reviewed_fee = $property_multiscope->recommended_fee + $ref_fee_total;
            
                                if ($ref_fee_total > 0) {
                                    $model->reference_flag = 1;
                                }
                            }

                            
                        }

                        
                        
                        // add reference fee amount in received property
                        $property_multiscope->reference_fee_3rdparty = (float)$ref_fee_3rdparty;
                        $property_multiscope->reference_fee_staff = (float)$ref_fee_staff;
                        

                        /* if ($property->tat == 0) {

                            die('0s');
                        } else*/

                        if ($property_multiscope->auto_check != 0) {

                        } else {
                            $property_multiscope->tat = $tat;
                            $property_multiscope->auto_check = 1;
                        }

                        /* if($property->tat == 0) {

                        }else{
                            if ($property->tat == null) {

                                $property->tat = $tat;
                            }

                        }*/                
                        

                        if ($property_multiscope->toe_fee <> null && $property_multiscope->toe_fee !== '0.00') {
                            $property_multiscope->toe_fee = $property_multiscope->quotation_fee;

                        } else {
                            $property->toe_fee = $property_multiscope->quotation_fee;
                        }
                        if ($property_multiscope->toe_tat <> null && $property_multiscope->toe_fee !== '0.00') {

                        } else {
                            $property_multiscope->toe_tat = $property_multiscope->tat;
                        }                
                        $total_amount = $total_amount + $rec;
                        

                        if (!$property_multiscope->save()) {
                            echo "<pre>";
                            print_r($property_multiscope);
                            print_r($property_multiscope->errors);
                            die;
                        }
                    }
                } else { 

                        // $tat = $property->property->tat;
                        if($quotation->type_of_service == 2) {
                            $tat = $property->working_days;
                        }
                        else{
                            $tat = $property->property->tat;
                        }

                        $property->recommended_fee = $amount;
                        // Reference fee
                        $ref_fee_staff = number_format((float)($amount * $quotation->reference_fee_staff / 100),2);
                        $ref_fee_3rdparty = ($quotation->reference_fee_3rdparty_check == 0) ? number_format((float)($amount * $quotation->reference_fee_3rdparty / 100),2) : number_format($reference_fee_3rdparty,2);

                        $ref_fee_total = $ref_fee_staff + $ref_fee_3rdparty;
                        

                        $ref_flag = $quotation->reference_flag;

                        if($model->id > $old_qid_30012024){
                            $property->recommended_fee = $property->recommended_fee + $ref_fee_total;

                            if ($property->quotation_fee <> null) {


                                if ($ref_fee_staff != $property->reference_fee_staff || $ref_fee_3rdparty != $property->reference_fee_3rdparty) {
                                    // $property->quotation_fee = $property->recommended_fee;
                                    // $property->bo_recommended_fee = $property->recommended_fee;
                                    // $property->reviewed_fee = $property->recommended_fee;
                                }
            
            
                            } else {
                                // $property->quotation_fee = $property->recommended_fee;
                                $property->quotation_fee = $property->recommended_fee;
                                $property->bo_recommended_fee = $property->recommended_fee;
                                $property->reviewed_fee = $property->recommended_fee;
            
                                if ($ref_fee_total > 0) {
                                    $model->reference_flag = 1;
                                }
                            }
                        }else {
                            if ($property->quotation_fee <> null) {


                                if ($ref_fee_staff != $property->reference_fee_staff || $ref_fee_3rdparty != $property->reference_fee_3rdparty) {
                                    $property->quotation_fee = $property->recommended_fee + $ref_fee_total;
                                    $property->bo_recommended_fee = $property->recommended_fee + $ref_fee_total;
                                    $property->reviewed_fee = $property->recommended_fee + $ref_fee_total;
                                }
            
            
                            } else {
                                // $property->quotation_fee = $property->recommended_fee;
                                $property->quotation_fee = $property->recommended_fee + $ref_fee_total;
                                $property->bo_recommended_fee = $property->recommended_fee + $ref_fee_total;
                                $property->reviewed_fee = $property->recommended_fee + $ref_fee_total;
            
                                if ($ref_fee_total > 0) {
                                    $model->reference_flag = 1;
                                }
                            }
                        }
                        
                        // add reference fee amount in received property
                        $property->reference_fee_3rdparty = (float)$ref_fee_3rdparty;
                        $property->reference_fee_staff = (float)$ref_fee_staff;
                        

                        /* if ($property->tat == 0) {

                            die('0s');
                        } else*/

                        if ($property->auto_check != 0) {

                        } else {
                            $property->tat = $tat;
                            $property->auto_check = 1;
                        }

                        /* if($property->tat == 0) {

                        }else{
                            if ($property->tat == null) {

                                $property->tat = $tat;
                            }

                        }*/                                        

                        if ($property->toe_fee <> null && $property->toe_fee !== '0.00') {
                            $property->toe_fee = $property->quotation_fee;

                        } else {
                            $property->toe_fee = $property->quotation_fee;
                        }
                        if ($property->toe_tat <> null && $property->toe_fee !== '0.00') {

                        } else {
                            $property->toe_tat = $property->tat;
                        }                
                        $total_amount = $total_amount + $amount;

                        if (!$property->save()) {
                            /* echo "<pre>";
                            print_r($property);
                            print_r($property->errors);
                            die;*/
                        }
                }
                
                    //echo 'dd'.$amount.'<br>';

                
                
            }
            //  }

           


        }


        // for reference fee
        if ($quotation->reference_fee_3rdparty != $quotation->reference_fee_3rdparty_bk) {
            Yii::$app->db->createCommand()->update('crm_quotations', ['reference_fee_3rdparty_bk' => $quotation->reference_fee_3rdparty], 'id=' . $model->id . '')->execute();
        }
        if ($quotation->reference_fee_staff != $quotation->reference_fee_staff_bk) {
            Yii::$app->db->createCommand()->update('crm_quotations', ['reference_fee_staff_bk' => $quotation->reference_fee_staff], 'id=' . $model->id . '')->execute();
        }

        // for reference fee total
        $reference_fee_staff_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('reference_fee_staff');
        $reference_fee_3rdparty_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('reference_fee_3rdparty');
        $reference_fee_total = $reference_fee_staff_total + $reference_fee_3rdparty_total;
        

        $quotation_fee_total_cal = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');
        if (isset($_POST) && $_POST <> null) {
            $calculations = $_POST['CrmQuotations']['calculations'];
            $quotation_fee_total_cal = 0;
            foreach ($calculations as $calculation) {
                $quotation_fee_total_cal = $quotation_fee_total_cal + $calculation['quotation_fee'];
            }

        }

        // $final_amount = $total_amount;
        $recommended_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('recommended_fee');
        $quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');
        $bo_recommended_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('bo_recommended_fee');
        $reviewed_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('reviewed_fee');
        


        if ($quotation_fee_total_cal != $quotation_fee_total) {
            Yii::$app->db->createCommand()
                ->update('crm_quotations', ['reset_values' => 0], 'id=' . $model->id . '')
                ->execute();

        }

        $quotation_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('tat');
        $quotation_aprvd_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('aprvd_tat');
        $bo_recommended_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('bo_recommended_tat');
        $reviewed_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('reviewed_tat');

        $recommended_final_amount = $recommended_fee_total;
        $final_amount = $quotation_fee_total;
        $bo_recommended_final_amount = $bo_recommended_fee_total;
        $reviewed_final_amount = $reviewed_fee_total;

        //start fee master file discounts
        $total_discount = 0;
        $total_discount_custom = 0;
        $bo_recommended_total_discount = 0;
        $bo_recommended_total_discount_custom = 0;
        $reviewed_total_discount = 0;
        $reviewed_total_discount_custom = 0;

        // for aproved discount
        $aprvd_total_discount = 0;
        $aprvd_total_discount_custom = 0;

        $ApprovedArr = [2, 4];


        if ($quotation->client->client_type != 'bank') {

            //Advance Payment Terms
            $advance_payment_terms_data = explode('%', $model->advance_payment_terms);
            $advance_payment_terms = yii::$app->quotationHelperFunctions->getAdvancePaymentfee($advance_payment_terms_data[0], $final_amount, $model);
            if ($model->reset_values == 0) {
                $model->advance_payment_terms_final_amount = $advance_payment_terms['amount'];
            }
            if ($model->auto_save_advance_payment == 0) {
                $model->rec_advance_payment_terms = $advance_payment_terms['amount'];

                $model->auto_save_advance_payment = 1;
            }

        } else {
            $advance_payment_terms['value'] = 0;
            $advance_payment_terms['amount'] = 0;
            $model->advance_payment_terms_final_amount = $advance_payment_terms['amount'];
            if ($model->auto_save_advance_payment == 0) {
                $model->rec_advance_payment_terms = $advance_payment_terms['amount'];


                $model->auto_save_advance_payment = 1;
            }
        }

        $recommended_fee_total = $recommended_fee_total + $model->rec_advance_payment_terms;
        $recommended_fee_total_after_discount = $recommended_fee_total;
        $bo_recommended_fee_total = $bo_recommended_fee_total + $model->rec_advance_payment_terms;
        $reviewed_fee_total = $reviewed_fee_total + $model->rec_advance_payment_terms;
        $bo_recommended_fee_total_after_discount = $bo_recommended_fee_total;
        $reviewed_fee_total_after_discount = $reviewed_fee_total;
        $aprvd_fee_total_after_discount = $recommended_fee_total;
        $final_amount = $final_amount + $model->rec_advance_payment_terms;
        $quotation_fee_total = $quotation_fee_total + $model->advance_payment_terms_final_amount;

        $final_amount = $quotation_fee_total;
        $recommended_final_amount = $recommended_fee_total;
        $bo_recommended_final_amount = $bo_recommended_fee_total;
        $reviewed_final_amount = $reviewed_fee_total;


        //  if (in_array($model->quotation_status, $ApprovedArr)) {
        //no_of_property_discount
        if ($model->no_of_property_discount > 0 && (count($properties) > 0)) {
            $no_of_property_discount['value'] = $model->no_of_property_discount;
            $bo_recommended_no_of_property_discount['value'] = $model->no_of_property_discount;
            $reviewed_no_of_property_discount['value'] = $model->no_of_property_discount;
            $no_of_property_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->no_of_property_discount, $recommended_final_amount);
            $bo_recommended_no_of_property_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->no_of_property_discount, $bo_recommended_final_amount);
            $reviewed_no_of_property_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->no_of_property_discount, $reviewed_final_amount);
            $total_discount = $total_discount + $no_of_property_discount['amount'];
            $bo_recommended_total_discount = $bo_recommended_total_discount + $bo_recommended_no_of_property_discount['amount'];
            $reviewed_total_discount = $reviewed_total_discount + $reviewed_no_of_property_discount['amount'];
            if ($model->reset_values == 0) {
                $model->no_of_property_discount_final_amount = $no_of_property_discount['amount'];
            }

            if ($model->auto_save_no_of_property_discount == 0) {
                $model->rec_no_of_property_discount = $no_of_property_discount['amount'];
                $model->auto_save_no_of_property_discount = 1;
            }
            $recommended_fee_total_after_discount = $recommended_fee_total_after_discount - $model->rec_no_of_property_discount;
            $bo_recommended_fee_total_after_discount = $bo_recommended_fee_total_after_discount - $model->rec_no_of_property_discount;
            $reviewed_fee_total_after_discount = $reviewed_fee_total_after_discount - $model->rec_no_of_property_discount;

        } else {
            $no_of_property_discount = yii::$app->quotationHelperFunctions->getNumberOfPropertiesDiscount(count($properties), $recommended_final_amount, $model);
            $bo_recommended_no_of_property_discount = yii::$app->quotationHelperFunctions->getNumberOfPropertiesDiscount(count($properties), $bo_recommended_final_amount, $model);
            $reviewed_no_of_property_discount = yii::$app->quotationHelperFunctions->getNumberOfPropertiesDiscount(count($properties), $reviewed_final_amount, $model);
            $model->no_of_property_discount = $no_of_property_discount['value'];
            $total_discount = $total_discount + $no_of_property_discount['amount'];
            $bo_recommended_total_discount = $bo_recommended_total_discount + $bo_recommended_no_of_property_discount['amount'];
            $reviewed_total_discount = $reviewed_total_discount + $reviewed_no_of_property_discount['amount'];
            if ($model->reset_values == 0) {
                $model->no_of_property_discount_final_amount = $no_of_property_discount['amount'];
            }
            if ($model->auto_save_no_of_property_discount == 0) {

                $model->rec_no_of_property_discount = $no_of_property_discount['amount'];
                $model->auto_save_no_of_property_discount = 1;
            }
            $recommended_fee_total_after_discount = $recommended_fee_total_after_discount - $model->rec_no_of_property_discount;
            $bo_recommended_fee_total_after_discount = $bo_recommended_fee_total_after_discount - $model->rec_no_of_property_discount;
            $reviewed_fee_total_after_discount = $reviewed_fee_total_after_discount - $model->rec_no_of_property_discount;
        }

        if ($model->aprvd_no_of_property_discount > 0 && (count($properties) > 0)) {
            $aprvd_no_of_property_discount['value'] = $model->aprvd_no_of_property_discount;
            $aprvd_no_of_property_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->aprvd_no_of_property_discount, $final_amount);
            $aprvd_total_discount = $aprvd_total_discount + $aprvd_no_of_property_discount['amount'];
            if ($model->reset_values == 0) {
                $model->aprvd_no_of_property_discount_amount = $aprvd_no_of_property_discount['amount'];
            }

            $model->aprvd_no_of_property_discount_amount = $aprvd_no_of_property_discount['amount'];

            $aprvd_fee_total_after_discount = $aprvd_fee_total_after_discount - $model->aprvd_no_of_property_discount_amount;

        }
        else {
            $aprvd_no_of_property_discount = yii::$app->quotationHelperFunctions->getNumberOfPropertiesDiscount(count($properties), $final_amount, $model);
            $model->aprvd_no_of_property_discount = $aprvd_no_of_property_discount['value'];
            $aprvd_total_discount = $aprvd_total_discount + $aprvd_no_of_property_discount['amount'];
            if ($model->reset_values == 0) {
                $model->aprvd_no_of_property_discount_amount = $aprvd_no_of_property_discount['amount'];
            }
            $model->aprvd_no_of_property_discount_amount = $aprvd_no_of_property_discount['amount'];

            $aprvd_fee_total_after_discount = $aprvd_fee_total_after_discount - $model->aprvd_no_of_property_discount_amount;
        }

        $total_discount_custom = $total_discount_custom + $model->no_of_property_discount_final_amount;
        $bo_recommended_total_discount_custom = $bo_recommended_total_discount_custom + $model->no_of_property_discount_final_amount;
        $reviewed_total_discount_custom = $reviewed_total_discount_custom + $model->no_of_property_discount_final_amount;

        $aprvd_total_discount_custom = $aprvd_total_discount_custom + $model->aprvd_no_of_property_discount_amount;




        //same_building_discount
        if ($model->same_building_discount > 0 && ($model->no_of_units_same_building > 0)) {

            if ($model->no_of_units_same_building > 1) {

                $same_building_discount['value'] = $model->same_building_discount;
                $same_building_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->same_building_discount, $recommended_final_amount);
                $bo_recommended_same_building_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->same_building_discount, $bo_recommended_final_amount);
                $reviewed_same_building_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->same_building_discount, $reviewed_final_amount);
                $total_discount = $total_discount + $same_building_discount['amount'];
                $bo_recommended_total_discount = $bo_recommended_total_discount + $bo_recommended_same_building_discount['amount'];
                $reviewed_total_discount = $reviewed_total_discount + $reviewed_same_building_discount['amount'];
                if ($model->reset_values == 0) {
                    $model->no_of_units_same_building_final_amount = $same_building_discount['amount'];
                }
                //  $model->no_of_units_same_building_final_amount = $same_building_discount['amount'];
                if ($model->auto_save_no_of_units_same_building == 0) {
                    $model->rec_no_of_units_same_building = $same_building_discount['amount'];

                    $model->auto_save_no_of_units_same_building = 1;
                }
            }
            $recommended_fee_total_after_discount = $recommended_fee_total_after_discount - $model->rec_no_of_units_same_building;
            $bo_recommended_fee_total_after_discount = $bo_recommended_fee_total_after_discount - $model->rec_no_of_units_same_building;
            $reviewed_fee_total_after_discount = $reviewed_fee_total_after_discount - $model->rec_no_of_units_same_building;
        } else {
            if ($model->no_of_units_same_building > 0) {
                $same_building_discount = yii::$app->quotationHelperFunctions->getSameBuildingDiscount($model->no_of_units_same_building, $recommended_final_amount, $model);
                $bo_recommended_same_building_discount = yii::$app->quotationHelperFunctions->getSameBuildingDiscount($model->no_of_units_same_building, $bo_recommended_final_amount, $model);
                $reviewed_same_building_discount = yii::$app->quotationHelperFunctions->getSameBuildingDiscount($model->no_of_units_same_building, $reviewed_final_amount, $model);
                $model->same_building_discount = $same_building_discount['value'];
                $total_discount = $total_discount + $same_building_discount['amount'];
                $bo_recommended_total_discount = $bo_recommended_total_discount + $bo_recommended_same_building_discount['amount'];
                $reviewed_total_discount = $reviewed_total_discount + $reviewed_same_building_discount['amount'];
                if ($model->reset_values == 0) {
                    $model->no_of_units_same_building_final_amount = $same_building_discount['amount'];
                }
                if ($model->auto_save_no_of_units_same_building == 0) {
                    $model->rec_no_of_units_same_building = $same_building_discount['amount'];

                    $model->auto_save_no_of_units_same_building = 1;
                }

            } else {
                $model->same_building_discount = 0;
                $model->rec_no_of_units_same_building = 0;
                $model->no_of_units_same_building_final_amount = 0;
            }
            $recommended_fee_total_after_discount = $recommended_fee_total_after_discount - $model->rec_no_of_units_same_building;
            $bo_recommended_fee_total_after_discount = $bo_recommended_fee_total_after_discount - $model->rec_no_of_units_same_building;
            $reviewed_fee_total_after_discount = $reviewed_fee_total_after_discount - $model->rec_no_of_units_same_building;
        }

        // for approved same building
        if ($model->aprvd_same_building_discount > 0 && ($model->no_of_units_same_building > 0)) {

            if ($model->no_of_units_same_building > 1) {

                $aprvd_same_building_discount['value'] = $model->aprvd_same_building_discount;
                $aprvd_same_building_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->aprvd_same_building_discount, $final_amount);
                $aprvd_total_discount = $aprvd_total_discount + $aprvd_same_building_discount['amount'];
                if ($model->reset_values == 0) {
                    $model->aprvd_same_building_discount_amount = $aprvd_same_building_discount['amount'];
                }
                $model->aprvd_same_building_discount_amount = $aprvd_same_building_discount['amount'];

            }

            $aprvd_fee_total_after_discount = $aprvd_fee_total_after_discount - $model->aprvd_same_building_discount_amount;


        }
        else {
            if ($model->no_of_units_same_building > 0) {
                $aprvd_same_building_discount = yii::$app->quotationHelperFunctions->getSameBuildingDiscount($model->no_of_units_same_building, $final_amount, $model);
                $model->aprvd_same_building_discount = $aprvd_same_building_discount['value'];
                $total_discount = $total_discount + $aprvd_same_building_discount['amount'];
                if ($model->reset_values == 0) {
                    $model->aprvd_same_building_discount_amount = $aprvd_same_building_discount['amount'];
                }
                $model->aprvd_same_building_discount_amount = $aprvd_same_building_discount['amount'];

            }else {
                $model->aprvd_same_building_discount = 0;
                $model->aprvd_same_building_discount_amount = 0;
            }

            $recommended_fee_total_after_discount = $recommended_fee_total_after_discount - $model->rec_no_of_units_same_building;
            $bo_recommended_fee_total_after_discount = $bo_recommended_fee_total_after_discount - $model->rec_no_of_units_same_building;
            $reviewed_fee_total_after_discount = $reviewed_fee_total_after_discount - $model->rec_no_of_units_same_building;

            $aprvd_fee_total_after_discount = $aprvd_fee_total_after_discount - $model->aprvd_same_building_discount_amount;
        }

        $total_discount_custom = $total_discount_custom + $model->no_of_units_same_building_final_amount;
        $bo_recommended_total_discount_custom = $bo_recommended_total_discount_custom + $model->no_of_units_same_building_final_amount;
        $reviewed_total_discount_custom = $reviewed_total_discount_custom + $model->no_of_units_same_building_final_amount;

        $aprvd_total_discount_custom = $aprvd_total_discount_custom + $model->aprvd_same_building_discount_amount;

        // dd($model);
        //first_time_discount
        if ($model->first_time_discount == null) {
            $clinet_first_time_check = Valuation::find()->where(['client_id' => $model->client_name])->one();
            if ($clinet_first_time_check == null) {
                if ($model->first_time_discount > 0) {
                    $first_time_fee_discount['value'] = $model->first_time_discount;
                    $first_time_fee_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->first_time_discount, $recommended_final_amount);
                    $bo_recommended_first_time_fee_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->first_time_discount, $bo_recommended_final_amount);
                    $reviewed_first_time_fee_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->first_time_discount, $reviewed_final_amount);
                    $total_discount = $total_discount + $first_time_fee_discount['amount'];
                    $bo_recommended_total_discount = $bo_recommended_total_discount + $bo_recommended_first_time_fee_discount['amount'];
                    $reviewed_total_discount = $reviewed_total_discount + $reviewed_first_time_fee_discount['amount'];

                    if ($model->reset_values == 0) {
                        $model->first_time_discount_final_amount = $first_time_fee_discount['amount'];
                    }
                    if ($model->auto_save_first_time_discount == 0) {
                        $model->rec_first_time_discount = $first_time_fee_discount['amount'];
                        $model->auto_save_first_time_discount = 1;
                    }
                } else {

                    $first_time_fee_discount = yii::$app->quotationHelperFunctions->getFirstTimeDiscount($recommended_final_amount, $model);
                    $bo_recommended_first_time_fee_discount = yii::$app->quotationHelperFunctions->getFirstTimeDiscount($bo_recommended_final_amount, $model);
                    $reviewed_first_time_fee_discount = yii::$app->quotationHelperFunctions->getFirstTimeDiscount($reviewed_final_amount, $model);
                    $model->first_time_discount = $first_time_fee_discount['value'];
                    $total_discount = $total_discount + $first_time_fee_discount['amount'];
                    $bo_recommended_total_discount = $bo_recommended_total_discount + $bo_recommended_first_time_fee_discount['amount'];
                    $reviewed_total_discount = $reviewed_total_discount + $reviewed_first_time_fee_discount['amount'];
                    if ($model->reset_values == 0) {
                        $model->first_time_discount_final_amount = $first_time_fee_discount['amount'];
                    }
                    if ($model->auto_save_first_time_discount == 0) {
                        $model->rec_first_time_discount = $first_time_fee_discount['amount'];

                        $model->auto_save_first_time_discount = 1;
                    }
                }
            } else {
                $model->first_time_discount = 0;
                $model->rec_first_time_discount = 0;
                $model->first_time_discount_final_amount = 0;
            }
            $recommended_fee_total_after_discount = $recommended_fee_total_after_discount - $model->rec_first_time_discount;
            $bo_recommended_fee_total_after_discount = $bo_recommended_fee_total_after_discount - $model->rec_first_time_discount;
            $reviewed_fee_total_after_discount = $reviewed_fee_total_after_discount - $model->rec_first_time_discount;
        } else {
            $first_time_fee_discount['value'] = $model->first_time_discount;
            $first_time_fee_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->first_time_discount, $recommended_final_amount);
            $bo_recommended_first_time_fee_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->first_time_discount, $bo_recommended_final_amount);
            $reviewed_first_time_fee_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->first_time_discount, $reviewed_final_amount);
            $total_discount = $total_discount + $first_time_fee_discount['amount'];
            $bo_recommended_total_discount = $bo_recommended_total_discount + $bo_recommended_first_time_fee_discount['amount'];
            $reviewed_total_discount = $reviewed_total_discount + $reviewed_first_time_fee_discount['amount'];
            if ($model->reset_values == 0) {
                $model->first_time_discount_final_amount = $first_time_fee_discount['amount'];
            }

            if ($model->auto_save_first_time_discount == 0) {


                $model->rec_first_time_discount = $first_time_fee_discount['amount'];
                $model->auto_save_first_time_discount = 1;
            }
            $recommended_fee_total_after_discount = $recommended_fee_total_after_discount - $model->rec_first_time_discount;
            $bo_recommended_fee_total_after_discount = $bo_recommended_fee_total_after_discount - $model->rec_first_time_discount;
            $reviewed_fee_total_after_discount = $reviewed_fee_total_after_discount - $model->rec_first_time_discount;
        }

        // for approved first time discount
        // if ($model->aprvd_first_time_discount > 0 && $model->first_time_discount > 0) {
        if ($model->aprvd_first_time_discount > 0) {
            $aprvd_first_time_discount['value'] = $model->aprvd_first_time_discount;
            $aprvd_first_time_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->aprvd_first_time_discount, $final_amount);
            //$aprvd_first_time_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->first_time_discount, $final_amount);
            $aprvd_total_discount = $aprvd_total_discount + $aprvd_first_time_discount['amount'];
            if ($model->reset_values == 0) {
                $model->aprvd_first_time_discount_amount = $aprvd_first_time_discount['amount'];
            }
            $model->aprvd_first_time_discount_amount = $aprvd_first_time_discount['amount'];

            $aprvd_fee_total_after_discount = $aprvd_fee_total_after_discount - $model->aprvd_first_time_discount_amount;

        }else {
            // $aprvd_first_time_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->aprvd_first_time_discount, $final_amount);
            $aprvd_first_time_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->first_time_discount, $final_amount);
            $model->aprvd_first_time_discount = $model->first_time_discount;
            $aprvd_total_discount = $aprvd_total_discount + $aprvd_first_time_discount['amount'];
            
            if ($model->reset_values == 0) {
                $model->aprvd_first_time_discount_amount = $aprvd_first_time_discount['amount'];
            }
            $model->aprvd_first_time_discount_amount = $aprvd_first_time_discount['amount'];

            $aprvd_fee_total_after_discount = $aprvd_fee_total_after_discount - $model->aprvd_first_time_discount_amount;
        }

        $total_discount_custom = $total_discount_custom + $model->first_time_discount_final_amount;
        $bo_recommended_total_discount_custom = $bo_recommended_total_discount_custom + $model->first_time_discount_final_amount;
        $reviewed_total_discount_custom = $reviewed_total_discount_custom + $model->first_time_discount_final_amount;
        
        $aprvd_total_discount_custom = $aprvd_total_discount_custom + $model->aprvd_first_time_discount_amount;

        

        $rec_total_discount = $total_discount;
        $bo_recommended_total_discount = $bo_recommended_total_discount;
        $reviewed_total_discount = $reviewed_total_discount;
        //end fee master file discounts
       
        
        //check relative discount
        if ($model->relative_discount_toe != null) { 
            $discount = yii::$app->quotationHelperFunctions->getDiscountRupee($final_amount, $model->relative_discount_toe);
            $recommended_discount = yii::$app->quotationHelperFunctions->getDiscountRupee($recommended_final_amount, $model->relative_discount_toe);
            $bo_recommended_discount = yii::$app->quotationHelperFunctions->getDiscountRupee($bo_recommended_final_amount, $model->relative_discount_toe);
            $reveiewed_discount = yii::$app->quotationHelperFunctions->getDiscountRupee($reviewed_final_amount, $model->relative_discount_toe);
            

            $total_discount = $total_discount + $discount;
            $rec_total_discount = $rec_total_discount + $recommended_discount;
            $bo_recommended_total_discount = $bo_recommended_total_discount + $bo_recommended_discount;
            $reviewed_total_discount = $reviewed_total_discount + $reveiewed_discount;
            
            $discount_rec = yii::$app->quotationHelperFunctions->getDiscountRupee($recommended_fee_total, $model->relative_discount_toe);
            $model->rec_relationship_discount = $discount_rec;
            $model->relationship_discount_final_amount = $discount;
            $recommended_fee_total_after_discount = $recommended_fee_total_after_discount - $discount_rec;
            /* if($model->relationship_discount_final_amount != $model->rec_relationship_discount){

             }else {
                 $model->relationship_discount_final_amount = $discount;
             }*/

            $aprvd_total_discount = $aprvd_total_discount + $discount;

            

        }


        $aprvd_final_amount = $final_amount;    
        $final_amount = $final_amount - $total_discount;
        $aprvd_final_amount = $aprvd_final_amount - $aprvd_total_discount;
        $recommended_final_amount = $recommended_final_amount - $rec_total_discount;
        $bo_recommended_final_amount = $bo_recommended_final_amount - $bo_recommended_total_discount;
        $reviewed_final_amount = $reviewed_final_amount - $reviewed_total_discount;

        // dd($aprvd_total_discount);
        
        
        //urgencyfee check
        if ($model->tat_requirements > 0) {
            
            $urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $final_amount,$property_type, $model);
            $recommended_urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $recommended_final_amount, $property_type, $model);
            $bo_recommended_urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $bo_recommended_final_amount, $property_type, $model);
            $reviewed_urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $reviewed_final_amount, $property_type, $model);
            $aprvd_urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $aprvd_final_amount, $property_type, $model);



            $final_amount = $final_amount + $urgencyfee['amount']; 
            $recommended_final_amount = $recommended_final_amount + $recommended_urgencyfee['amount'];
            $bo_recommended_final_amount = $bo_recommended_final_amount + $bo_recommended_urgencyfee['amount'];
            $reviewed_final_amount = $reviewed_final_amount + $reviewed_urgencyfee['amount'];
            $aprvd_final_amount = $aprvd_final_amount + $aprvd_urgencyfee['amount'];


            if ($model->reset_values == 0) {
                $model->tat_requirements_final_amount = $urgencyfee['amount'];
            }
            $model->aprvd_tat_requirements_final_amount = $aprvd_urgencyfee['amount'];

            if ($model->relative_discount_toe > 0) {
                $urgencyfee_rec = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $recommended_fee_total_after_discount,$property_type, $model);
                $model->rec_tat_requirements = $urgencyfee_rec['amount'];

            }

            if ($model->auto_save_tat_requirements == 0) {
                $model->rec_tat_requirements = $urgencyfee['amount'];

                $model->auto_save_tat_requirements = 1;
            }
        }
        /*    if ($quotation->client->client_type != 'bank') {

            //Advance Payment Terms
            $advance_payment_terms_data = explode('%', $model->advance_payment_terms);
            $advance_payment_terms = yii::$app->quotationHelperFunctions->getAdvancePaymentfee($advance_payment_terms_data[0], $final_amount);
        }else{
            $advance_payment_terms['value'] =0;
            $advance_payment_terms['amount'] =0;
        }
          */// $final_amount = $final_amount + $advance_payment_terms['amount'];
        // }
        //relative discount
        $model->relative_discount_toe = $model->relative_discount;
        if ($model->relative_discount <> null) {

        } else {
            $model->relative_discount = $quotation->client->relative_discount;
        }
        if ($model->relative_discount_toe <> null) {

        } else {
            $model->relative_discount_toe = $quotation->client->relative_discount;
        }


        $toe_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_fee');
        $toe_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_tat');

        $receivedProperties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();
        $multiScopeProperties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->andWhere(['not', ['multiscope_id' => null]])->groupBy('multiscope_id')->orderBy(['property_index' => SORT_ASC])->all();


        $model->quotation_recommended_fee = $recommended_fee_total;
        $model->final_fee_approved = $quotation_fee_total;
        $model->final_fee_bo_recommended = $bo_recommended_fee_total;
        $model->final_fee_reviewed = $reviewed_fee_total;

        // dd($reviewed_fee_total);
        
        $model->quotation_turn_around_time = $quotation_tat_total;
        if($quotation_aprvd_tat_total <> null){
            $model->quotation_turn_around_time = $quotation_aprvd_tat_total;
        }
        $model->reset_values = 1;
        $model->save();


        $quotation_readonly = '';
        $toe_readonly = '';
        if (($model->quotation_status == 0 || $model->quotation_status == 14)  || ($model->status_approve == 'Pending')|| ($model->status_approve == 'Reject') || ($model->quotation_status == 17) || ($model->status_approve == 'Recommended')) {
            $quotation_readonly = '';
        } else {
            $quotation_readonly = 'readonly';
        }
        if (($model->quotation_status == 0  || $model->quotation_status == 14) || ($model->status_approve == 'Pending') || ($model->status_approve == 'Reject')) {
            $reommended_readonly = '';
        } else {
            $reommended_readonly = 'readonly';
        }

        if ($model->quotation_status > 1 && $model->quotation_status <= 2) {
            $toe_readonly = '';
        } else {
            $toe_readonly = 'readonly';
        }
        
        if ($model->quotation_status == 11) {
            $model->status_approve = 'cancelled';
        } else if ($model->quotation_status == 12) {
            $model->status_approve = 'regretted';
        } else if ($model->quotation_status == 2) {
            $model->status_approve = 'Approve';
        } else if ($model->quotation_status == 16) {
            $model->status_approve = 'client_accepted';
            $model->toe_recommended = 1;
            $model->status_change_date = $date;
        } else if ($model->quotation_status == 4) {
            $model->status_approve = 'toe_Accept';
        } else if ($model->quotation_status == 8) {
            $model->status_approve = 'toe_Reject';
        } else if ($model->quotation_status == 9) {
            $model->status_approve = 'Pending';
        }else if($quotation_status == 17){
            $model->status_approve = 'Recommended';
        }else if ($model->quotation_status == 18) {
            $model->status_approve = 'toe_verified';
            $model->toe_recommended = 0;
        }


        $high_low_fee = Yii::$app->crmQuotationHelperFunctions->GetHighLowFee($model);

        if ($model->load(Yii::$app->request->post())) {

            // dd($quotation_status);

            if($quotation_status <> null && $quotation_status == 2){
                $model->status_approve = 'Approve';
            }else if($quotation_status <> null && $quotation_status == 9){
                $model->status_approve = 'Pending';
            }


            $step_to_redirect = 'step_8';
            $date = date("Y-m-d H:i:s");



            if ($model->status_approve == 'Approve') {
                if ($model->quotation_sent_date == null) {
                    if ($model->quotation_status <= 0 || $model->quotation_status == 14) {
                        $condition = false;
                        if ($quotation->client_name > 0 && $quotation->email_subject <> null && $quotation->client_name != 112 && $quotation->client_name != 118945 && $quotation->client_name != 54514) {
                            $model->quotationPdf($condition);
                        }
                        // if($model->quotation_status <=0 || $model->quotation_status ==14){
                        $date = date("Y-m-d H:i:s");
                        $model->quotation_status = 1;
                        $model->status_change_date = $date;
                        $model->quotation_sent_date = $date;
                        yii::$app->getsession()->addFlash('success', yii::t('app', 'Quotation send successfully'));

                        // create and send feedback form link
                        $this->createFeedback($quotation);
                        // }
                    }
                }
                $model->quotation_status = 2;
                $model->approved_date = $date;
                $model->quotation_approved_by = \Yii::$app->user->identity->id;
                $step_to_redirect = 'step_ar_quotation';


            } else if ($model->status_approve == 'client_accepted') {
                // if ($model->toe_sent_date == null) {
                //     $condition = false;
                //     if ($quotation->client->client_type != 'bank') {
                //         if ($quotation->client_name > 0 && $quotation->email_subject <> null) {
                //             $model->toePdf($condition);
                //         }
                //     }
                //     $model->quotation_status = 3;
                //     $model->toe_sent_date = $date;
                //     yii::$app->getsession()->addFlash('success', yii::t('app', 'TOE send successfully'));
                //     $step_to_redirect = 'step_ar_quotation';
                // }

                $model->quotation_status = 16;
                $model->toe_recommended = 1;
                $model->toe_recommended_date = $date;
                $model->quotation_accepted_date = $date;
                $step_to_redirect = 'step_ar_toe';
            } else if ($model->status_approve == 'Reject') {
                $model->quotation_status = 7;
                $model->quotation_rejected_date = $date;
            } else if ($model->status_approve == 'Pending') {
                $model->quotation_status = 9;
            }
            else if ($model->status_approve == 'Recommended') {
                $model->quotation_status = 17;
                $model->quotation_recommended_date = $date;
            } else if ($model->status_approve == 'on-hold') {
                $model->quotation_status = 10;
                $model->on_hold_date = $date;
            } else if ($model->status_approve == 'cancelled') {
                $model->quotation_status = 11;
                $model->cancelled_date = $date;
            } else if ($model->status_approve == 'regretted') {
                $model->quotation_status = 12;
                $model->regretted_date = $date;
            }

            //toe status updated
            else if ($model->status_approve == 'toe_Accept') {
                $model->quotation_status = 4;
                $model->toe_approved_date = $date;
                $step_to_redirect = 'step_ar_toe';
            } else if ($model->status_approve == 'toe_Reject') {
                $model->quotation_status = 8;
                $model->toe_rejected_date = $date;
            } else if ($model->status_approve == 'toe_Pending') {
                $model->quotation_status = 9;
            }else if ($model->status_approve == 'toe_verified') {

                $model->valuer_id = \Yii::$app->user->identity->id;

                // send the toe from this code
                if ($model->toe_sent_date == null) {
                    $condition = false;
                    if ($quotation->client->client_type != 'bank') {
                        if ($quotation->client_name > 0 && $quotation->email_subject <> null) {
                            $model->toePdf($condition);
                        }
                    }
                    $model->quotation_status = 3;
                    $model->toe_sent_date = $date;
                    yii::$app->getsession()->addFlash('success', yii::t('app', 'TOE send successfully'));
                    $step_to_redirect = 'step_ar_quotation';
                }

                $model->quotation_status = 18;
                $model->toe_recommended = 2;
                $model->toe_verified_date = $date;
                
            } else {
                unset($model->status_approve);
            }
            $model->relative_discount_toe = $model->relative_discount;

            if ($model->quotation_status <> null) {
                $model->status_change_date = $date;
            } else {
            }
            if ($model->save()) {

                if ($model->quotation_status == 2 && $model->email_status_q_approved == 0) {
                    $notifyData = [
                        'client' => $model->client,
                        'attachments' => [],
                        'subject' => $model->email_subject,
                        'uid' => 'crm' . $model->id,
                        'instructing_person_email' => $model->instructing_party_email,
                        'replacements' => [
                            '{clientName}' => $model->client->title,
                        ],
                    ];
                    // \app\modules\wisnotify\listners\NotifyEvent::fire23('quotation.approved', $notifyData);
                    Yii::$app->db->createCommand()->update('crm_quotations', ['email_status_q_approved' => 1], 'id=' . $model->id . '')->execute();
                } else if ($model->quotation_status == 10 && $model->email_status_q_hold == 0) {
                    $notifyData = [
                        'client' => $model->client,
                        'attachments' => [],
                        'subject' => $model->email_subject,
                        'uid' => 'crm' . $model->id,
                        'instructing_person_email' => $model->instructing_party_email,
                        'replacements' => [
                            '{clientName}' => $model->client->title,
                        ],
                    ];
                    if ($quotation->client->client_type != 'bank') {
                        if ($quotation->client_name > 0 && $quotation->email_subject <> null) {
                            \app\modules\wisnotify\listners\NotifyEvent::fire23('quotation.rejected', $notifyData);
                        }
                    }
                    Yii::$app->db->createCommand()->update('crm_quotations', ['email_status_q_hold' => 1], 'id=' . $model->id . '')->execute();
                } else if ($model->quotation_status == 7 && $model->email_status_q_rejected == 0) {
                    $notifyData = [
                        'client' => $model->client,
                        'attachments' => [],
                        'subject' => $model->email_subject,
                        'uid' => 'crm' . $model->id,
                        'instructing_person_email' => $model->instructing_party_email,
                        'replacements' => [
                            '{clientName}' => $model->client->title,
                        ],
                    ];
                    if ($quotation->client->client_type != 'bank') {
                        if ($quotation->client_name > 0 && $quotation->email_subject <> null) {
                            \app\modules\wisnotify\listners\NotifyEvent::fire23('quotation.rejected', $notifyData);
                        }
                    }
                    Yii::$app->db->createCommand()->update('crm_quotations', ['email_status_q_rejected' => 1], 'id=' . $model->id . '')->execute();
                }

                Yii::$app->crmQuotationHelperFunctions->addStatusHistory($model);
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['crm-quotations/' . $step_to_redirect . '?id=' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }
        /*echo "<pre>";
                print_r($advance_payment_terms);
                die;*/

        if($quotation->type_of_service == 3){
            $viewpath = 'steps/bcs/_step8';
        }else  if($quotation->type_of_service == 2){
            $viewpath = 'steps/valuation-pme/_step8';
        }else {
            $viewpath = 'steps/_step8';
        }

        // echo "<pre>";
        // print_r($amount_summary);
        // die;

        return $this->render($viewpath, [
            'model' => $model,
            'quotation_readonly' => $quotation_readonly,
            'reommended_readonly' => $reommended_readonly,
            'toe_readonly' => $toe_readonly,
            'recommended_fee' => $recommended_fee_total,
            'recommended_fee_total' => $recommended_fee_total,
            'quotation_fee_total' => $quotation_fee_total,
            'quotation_tat_total' => $quotation_tat_total,
            'quotation_aprvd_tat_total' => $quotation_aprvd_tat_total,
            'toe_fee_total' => $toe_fee_total,
            'toe_tat_total' => $toe_tat_total,
            'receivedProperties' => $receivedProperties,
            'multiScopeProperties' => $multiScopeProperties,
            'amount_summary' => $amount_summary,
            'quotation' => $model,
            'no_of_property_discount_data' => $no_of_property_discount,
            'bo_recommended_no_of_property_discount_data' => $bo_recommended_no_of_property_discount,
            'reviewed_no_of_property_discount_data' => $reviewed_no_of_property_discount,
            'same_building_discount_data' => $same_building_discount,
            'bo_recommended_same_building_discount_data' => $bo_recommended_same_building_discount,
            'reviewed_same_building_discount_data' => $reviewed_same_building_discount,
            'first_time_fee_discount_data' => $first_time_fee_discount,
            'bo_recommended_first_time_fee_discount_data' => $bo_recommended_first_time_fee_discount,
            'reviewed_first_time_fee_discount_data' => $reviewed_first_time_fee_discount,
            'urgencyfee_data' => $urgencyfee,
            'recommended_urgencyfee_data' => $recommended_urgencyfee,
            'bo_recommended_urgencyfee_data' => $bo_recommended_urgencyfee,
            'reviewed_urgencyfee_data' => $reviewed_urgencyfee,
            'advance_payment_terms_data' => $advance_payment_terms,
            'high_low_fee' => $high_low_fee,
            'step_num' => $step_num,
            'reference_fee_total' => $reference_fee_total,
            'aprvd_no_of_property_discount_data' => $aprvd_no_of_property_discount,
            'aprvd_same_building_discount_data' => $aprvd_same_building_discount,
            'aprvd_first_time_fee_discount_data' => $aprvd_first_time_discount,
            'aprvd_urgencyfee_data' => $aprvd_urgencyfee,
            'bo_recommended_fee_total' => (float)$bo_recommended_fee_total,
            'bo_recommended_tat_total' => $bo_recommended_tat_total,
            'reviewed_fee_total' => (float)$reviewed_fee_total,
            'reviewed_tat_total' => $reviewed_tat_total,
            'old_qid_30012024' => $old_qid_30012024,

        ]);
    }
    public function createFeedback($quotation){
        // for proposal feedback

            $model_feedback = Feedback::find()->where(['quotation_id' => $quotation->id])->one();
            
            if($model_feedback == null && $quotation->client_name == 9166){

                $feedback_key = Yii::$app->security->generateRandomString(20, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
                

                $feedback = new Feedback();
                $feedback->client_id = $quotation->client_name;
                $feedback->quotation_id = $quotation->id;
                $feedback->proposer = $quotation->created_by;
                $feedback->feedback_key = $feedback_key;
                $feedback->type = 1;
                $feedback->client_type = ($quotation->client->client_type == "bank") ? 2 : 1 ;
                // $feedback->save();
                if (!$feedback->save()) {
                    echo "<pre>";
                    print_r($feedback->errors);
                    die;
                }else {
                    // $feedback_url = 'https://maxima.windmillsgroup.com/feedback/submit/?k='.$feedback->feedback_key;
                    $quotation->sendProposalFeedbackFormMail($feedback->feedback_key,$feedback->client_id);
                    // $quotation->sendProposalFeedbackFormSms($feedback->feedback_key, $quotation);
                    Yii::$app->db->createCommand()->update('feedback', ['email_status' => 1,'sms_status' => 1], 'id=' . $feedback->id . '')->execute();
                }
                



            }

    }

    public function actionStep_9_01_02_2022($id)
    {
        $this->checkLogin();
        $model = $this->findModel($id);
        $quotation = $this->findModel($id);

        if ($quotation->quotation_status >= 0 && $quotation->quotation_status < 1) {

            $properties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();

            foreach ($properties as $key => $property_data) {
                $inspect_property = CrmInspectProperty::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
                $schecdule_inspections = CrmScheduleInspection::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
                $property = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
                //Fee criteria
                $fee_parameters = array();
                $fee_parameters['clientType'] = $quotation->client->client_type;
                $fee_parameters['paymentTerms'] = $quotation->advance_payment_terms;
                $fee_parameters['property'] = $property->property_id;
                $fee_parameters['city'] = $property->building->city;
                $fee_parameters['tenure'] = $property->tenure;
                $fee_parameters['complexity'] = $property->complexity;
                $fee_parameters['repeat_valuation'] = $property->repeat_valuation;
                if ($quotation->id > 508) {
                    $fee_parameters['built_up_area'] = $property->built_up_area;
                } else {
                    $fee_parameters['built_up_area'] = $inspect_property->built_up_area;
                }
                // $fee_parameters['built_up_area'] = $inspect_property->built_up_area;
                $fee_parameters['type_of_valuation'] = $property->inspection_type;
                $fee_parameters['number_of_comparables'] = $property->number_of_comparables;
                $fee_parameters['no_of_units'] = $property->no_of_units;
                $fee_parameters['land_size'] = $property->land_size;
                $fee_parameters['upgrades'] = ($property->upgrades <> null) ? $property->upgrades : 0;

                if ($quotation->id > 938) {
                    $amount = yii::$app->propertyCalcHelperFunction->getAmount_new($fee_parameters);
                } else {
                    $amount = yii::$app->propertyCalcHelperFunction->getAmount_new_938($fee_parameters);
                }


                if (isset($amount['fee'])) {
                    $property->recommended_fee = $amount['fee'];
                } else {
                    $property->recommended_fee = $amount['fee'];
                }


                if ($property->quotation_fee <> null) {

                } else {
                    $property->quotation_fee = $property->recommended_fee;
                }

                if ($property->toe_fee <> null && $property->toe_fee !== '0.00') {

                } else {
                    $property->toe_fee = $property->quotation_fee;
                }
                if ($property->toe_tat <> null && $property->toe_fee !== '0.00') {

                } else {
                    $property->toe_tat = $property->tat;
                }

                $property->save();
            }
        }





        $recommended_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('recommended_fee');
        $quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');
        $quotation_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('tat');

        $toe_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_fee');
        $toe_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_tat');

        $receivedProperties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();
        //Start By Usama
        $discount = 0;
        $VAT = 0;
        if ($model->relative_discount_toe != null) {
            $discount = yii::$app->quotationHelperFunctions->getDiscountRupee($toe_fee_total, $model->relative_discount_toe);
        }
        $netValuationFee = $toe_fee_total - $discount;
        if ($model->client->vat == 1) {
            $VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
        }
        $finalFeePayable = $netValuationFee + $VAT;

        $model->grand_final_toe_fee = $finalFeePayable;
        //End By Usama
        $model->final_fee_approved = $quotation_fee_total;
        $model->quotation_turn_around_time = $quotation_tat_total;
        $model->toe_final_fee = $toe_fee_total;
        $model->toe_final_turned_around_time = $toe_tat_total;

        $model->save();
        $quotation_readonly = '';
        $toe_readonly = '';
        if ($model->quotation_status >= 0 && $model->quotation_status < 1) {
            $quotation_readonly = '';
        } else {
            $quotation_readonly = 'readonly';
        }

        if (($model->quotation_status > 1 && $model->quotation_status <= 2) || ($model->status_approve_toe == 'Pending') || ($model->status_approve_toe == 'Reject')) {
            $toe_readonly = '';
        } else {
            $toe_readonly = 'readonly';
        }

        /*if($model->toe_fee <> null){

        }else{
            $model->toe_fee = $model->recommended_fee;
        }*/



        if ($model->load(Yii::$app->request->post())) {
            $date = date("Y-m-d H:i:s");
            if ($model->status_approve_toe == 'Approve') {
                $model->quotation_status = 4;
            } else if ($model->status_approve_toe == 'Reject') {
                $model->quotation_status = 8;
                $model->toe_rejected_date = $date;

            } else if ($model->status_approve_toe == 'Pending') {
                $model->quotation_status = 9;
            } else if ($model->status_approve_toe == 'Verified') {
                $model->quotation_status = 18;
            }

            if ($model->quotation_status <> null) {
                $model->status_change_date = $date;
            } else {
            }


            if ($model->save()) {
                Yii::$app->crmQuotationHelperFunctions->addStatusHistory($model);
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['crm-quotations/step_9?id=' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }


        return $this->render('steps/_step9', [
            'model' => $model,
            'quotation_readonly' => $quotation_readonly,
            'toe_readonly' => $toe_readonly,
            'recommended_fee' => $recommended_fee_total,
            'quotation_fee_total' => $quotation_fee_total,
            'quotation_tat_total' => $quotation_tat_total,
            'toe_fee_total' => $toe_fee_total,
            'toe_tat_total' => $toe_tat_total,
            'receivedProperties' => $receivedProperties,
            'quotation' => $model,
        ]);
    }

    public function actionStep_9($id)
    {
        $this->checkLogin();
        $model = $this->findModel($id);
        $quotation = $this->findModel($id);
        $total_amount = 0;
        $final_amount = 0;
        $properties_toe = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();
        if ($quotation->quotation_status >= 0 && $quotation->quotation_status < 1) {

            $properties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();

            foreach ($properties as $key => $property_data) {
                $inspect_property = CrmInspectProperty::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
                $schecdule_inspections = CrmScheduleInspection::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
                $property = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
                //Fee criteria
                $fee_parameters = array();
                $fee_parameters['clientType'] = $quotation->client->client_type;
                $fee_parameters['paymentTerms'] = $quotation->advance_payment_terms;
                $fee_parameters['property'] = $property->property_id;
                $fee_parameters['city'] = $property->building->city;
                $fee_parameters['tenure'] = $property->tenure;
                $fee_parameters['complexity'] = str_replace('-', '_', $property->complexity);
                $fee_parameters['repeat_valuation'] = $property->repeat_valuation;
                if ($quotation->id > 508) {
                    $fee_parameters['built_up_area'] = $property->built_up_area;
                } else {
                    $fee_parameters['built_up_area'] = $inspect_property->built_up_area;
                }
                // $fee_parameters['built_up_area'] = $inspect_property->built_up_area;
                $fee_parameters['type_of_valuation'] = $property->inspection_type;
                $fee_parameters['number_of_comparables'] = $property->number_of_comparables;
                $fee_parameters['no_of_units'] = $property->no_of_units;
                $fee_parameters['land_size'] = $property->land_size;
                $fee_parameters['upgrades'] = ($property->upgrades <> null) ? $property->upgrades : 0;
                $fee_parameters['other_intended_users'] = ($quotation->other_intended_users_check_1 == "Yes") ? 'yes' : 'no';
                $fee_parameters['approach_type'] = Yii::$app->appHelperFunctions->valuationApproachListArr[$property->valuation_approach];
                $fee_parameters['number_of_types'] = $property->no_of_unit_types;

                $ApprovedArr = [4];
                if (!in_array($model->quotation_status, $ApprovedArr)) {

                    //$amount = yii::$app->propertyCalcHelperFunction->getAmount_auto($fee_parameters);
                    if ($model->id > 1203) {
                        if ($property->valuation_approach == 2) {
                            $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoIncome($fee_parameters);
                        } else if ($property->valuation_approach == 3) {
                            $amount = yii::$app->propertyCalcHelperFunction->getAmount_autoProfit($fee_parameters);
                        } else {
                            $amount = yii::$app->propertyCalcHelperFunction->getAmount_auto($fee_parameters);
                        }
                    } else {
                        $amount = yii::$app->propertyCalcHelperFunction->getAmount_new($fee_parameters);
                        $amount = $amount['fee'];
                    }

                    $property->recommended_fee = $amount;



                    if ($property->quotation_fee <> null) {

                    } else {
                        $property->quotation_fee = $property->recommended_fee;
                    }

                    if ($property->toe_fee <> null && $property->toe_fee !== '0.00') {

                    } else {
                        $property->toe_fee = $property->quotation_fee;
                    }
                    if ($property->toe_tat <> null && $property->toe_fee !== '0.00') {

                    } else {
                        $property->toe_tat = $property->tat;
                    }
                    $total_amount = $total_amount + $amount;
                    $property->save();
                }
            }
        }


        //   $final_amount = $total_amount;
        $recommended_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('recommended_fee');
        $quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');
        $quotation_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('tat');

        $toe_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_fee');
        $toe_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_tat');
        $final_amount = $toe_fee_total;
        //start fee master file discounts
        $total_discount = 0;
        //no_of_property_discount
        if ($model->no_of_property_discount > 0 && (count($properties) > 0)) {
            $no_of_property_discount['value'] = $model->no_of_property_discount;
            $no_of_property_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->no_of_property_discount, $final_amount);
            $total_discount = $total_discount + $no_of_property_discount['amount'];
        } else {
            $no_of_property_discount = yii::$app->quotationHelperFunctions->getNumberOfPropertiesDiscount(count($properties_toe), $final_amount);
            $model->no_of_property_discount = $no_of_property_discount['value'];
            $total_discount = $total_discount + $no_of_property_discount['amount'];
        }

        //same_building_discount

        if ($model->same_building_discount > 0 && ($model->no_of_units_same_building > 0)) {

            if ($model->no_of_units_same_building > 1) {
                $same_building_discount['value'] = $model->same_building_discount;
                $same_building_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->same_building_discount, $final_amount);
                $total_discount = $total_discount + $same_building_discount['amount'];
            }
        } else {
            if ($model->no_of_units_same_building > 0) {
                $same_building_discount = yii::$app->quotationHelperFunctions->getSameBuildingDiscount($model->no_of_units_same_building, $final_amount);
                $model->same_building_discount = $same_building_discount['value'];
                $total_discount = $total_discount + $same_building_discount['amount'];
            } else {
                $model->same_building_discount = 0;
            }
        }

        //first_time_discount
        // dd($model);

        if ($model->first_time_discount == null) {
            $clinet_first_time_check = Valuation::find()->where(['client_id' => $model->client_name])->one();
            if ($clinet_first_time_check == null) {
                if ($model->first_time_discount > 0) {
                    $first_time_fee_discount['value'] = $model->first_time_discount;
                    $first_time_fee_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->first_time_discount, $final_amount);
                    $total_discount = $total_discount + $first_time_fee_discount['amount'];
                } else {
                    $first_time_fee_discount = yii::$app->quotationHelperFunctions->getFirstTimeDiscount($final_amount);
                    $model->first_time_discount = $first_time_fee_discount['value'];
                    $total_discount = $total_discount + $first_time_fee_discount['amount'];
                }
            } else {
                $model->first_time_discount = 0;
            }
        } else {
            $first_time_fee_discount['value'] = $model->first_time_discount;
            $first_time_fee_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->first_time_discount, $final_amount);
            $total_discount = $total_discount + $first_time_fee_discount['amount'];
        }



        //check relative discount
        if ($model->relative_discount_toe != null) {
            $discount = yii::$app->quotationHelperFunctions->getDiscountRupee($final_amount, $model->relative_discount_toe);
            $total_discount = $total_discount + $discount;
        }


        $final_amount = $final_amount - $total_discount;


        //end fee master file discounts


        //urgencyfee check
        if ($model->tat_requirements > 0) {
            $urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $final_amount);
            $final_amount = $final_amount + $urgencyfee['amount'];
        }
        if ($model->client->client_type != 'bank') {
            //Advance Payment Terms
            $advance_payment_terms_data = explode('%', $model->advance_payment_terms);
            $advance_payment_terms = yii::$app->quotationHelperFunctions->getAdvancePaymentfee($advance_payment_terms_data[0], $final_amount);
        } else {
            $advance_payment_terms['amount'] = 0;
            $advance_payment_terms['value'] = 0;
        }
        $final_amount = $final_amount + $advance_payment_terms['amount'];

        $receivedProperties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();

        $discount = 0;
        $VAT = 0;


        $netValuationFee = $final_amount;
        if ($model->client->vat == 1) {
            $VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
        }
        $finalFeePayable = $netValuationFee + $VAT;
        /*  echo $finalFeePayable;
          die;*/
        $model->grand_final_toe_fee = $finalFeePayable;

        $model->final_fee_approved = $quotation_fee_total;
        $model->quotation_turn_around_time = $quotation_tat_total;
        $model->toe_final_fee = $toe_fee_total;
        $model->toe_final_turned_around_time = $toe_tat_total;

        $model->save();
        $quotation_readonly = '';
        $toe_readonly = '';
        if ($model->quotation_status >= 0 && $model->quotation_status < 1) {
            $quotation_readonly = '';
        } else {
            $quotation_readonly = 'readonly';
        }

        if (($model->quotation_status > 1 && $model->quotation_status <= 2) || ($model->status_approve_toe == 'Pending') || ($model->status_approve_toe == 'Reject')) {
            $toe_readonly = '';
        } else {
            $toe_readonly = 'readonly';
        }



        if ($model->load(Yii::$app->request->post())) {
            $date = date("Y-m-d H:i:s");
            if ($model->status_approve_toe == 'Approve') {
                $model->quotation_status = 4;
            } else if ($model->status_approve_toe == 'Reject') {
                $model->quotation_status = 8;
                $model->toe_rejected_date = $date;

            } else if ($model->status_approve_toe == 'Pending') {
                $model->quotation_status = 9;
            } else if ($model->status_approve_toe == 'Verified') {
                $model->quotation_status = 18;
            }

            if ($model->quotation_status <> null) {
                $model->status_change_date = $date;
            } else {
            }


            if ($model->save()) {
                if ($model->quotation_status == 8 && $model->email_status_t_rejected == 0) {
                    $notifyData = [
                        'client' => $model->client,
                        'attachments' => [],
                        'subject' => $model->email_subject,
                        'uid' => 'crm' . $model->id,
                        'instructing_person_email' => $model->instructing_party_email,
                        'replacements' => [
                            '{clientName}' => $model->client->title,
                        ],
                    ];
                    if ($model->client->client_type != 'bank') {
                        if ($quotation->client_name > 0 && $quotation->email_subject <> null) {
                            \app\modules\wisnotify\listners\NotifyEvent::fire23('toe.rejected', $notifyData);
                        }
                    }
                    Yii::$app->db->createCommand()->update('crm_quotations', ['email_status_t_rejected' => 1], 'id=' . $model->id . '')->execute();
                }

                Yii::$app->crmQuotationHelperFunctions->addStatusHistory($model);
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                return $this->redirect(['crm-quotations/step_9?id=' . $id]);
            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }


        return $this->render('steps/_step9', [
            'model' => $model,
            'quotation_readonly' => $quotation_readonly,
            'toe_readonly' => $toe_readonly,
            'recommended_fee' => $recommended_fee_total,
            'quotation_fee_total' => $quotation_fee_total,
            'quotation_tat_total' => $quotation_tat_total,
            'toe_fee_total' => $toe_fee_total,
            'toe_tat_total' => $toe_tat_total,
            'receivedProperties' => $receivedProperties,
            'quotation' => $model,
            'no_of_property_discount_data' => $no_of_property_discount,
            'same_building_discount_data' => $same_building_discount,
            'first_time_fee_discount_data' => $first_time_fee_discount,
            'urgencyfee_data' => $urgencyfee,
            'advance_payment_terms_data' => $advance_payment_terms,
        ]);
    }

    /*   public function actionStep_9_old($id)
       {
           $model = $this->findModel($id);
           $recommended_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('recommended_fee');
           $quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');
           $quotation_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('tat');
           $quotation_toe_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_fee');
           $quotation_toe_tat_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_tat');
           $receivedProperties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();






                   $model->toe_final_fee = $quotation_toe_fee_total;



               $model->toe_final_turned_around_time = $quotation_toe_tat_total;




           if ($model->load(Yii::$app->request->post())) {
               if($model->status_approve == 'Approve'){
                   $model->quotation_status = 2;
               }
               if ($model->save()) {
                   Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                   return $this->redirect(['crm-quotations/step_9?id='.$id]);
               } else {
                   if ($model->hasErrors()) {
                       foreach ($model->getErrors() as $error) {
                           if (count($error) > 0) {
                               foreach ($error as $key => $val) {
                                   Yii::$app->getSession()->addFlash('error', $val);
                               }
                           }
                       }
                   }
               }
           }


           return $this->render('steps/_step9', [
               'model' => $model,
               'recommended_fee' => $recommended_fee_total,
               'quotation_fee_total' => $quotation_fee_total,
               'quotation_tat_total' => $quotation_tat_total,
               'toe_fee_total' => $quotation_fee_total,
               'toe_tat_total' => $quotation_tat_total,
               'receivedProperties' => $receivedProperties,
               'quotation' => $model,
           ]);
       }*/

    public function actionStep_10_old($id)
    {
        $this->checkLogin();
        $model = $this->findModel($id);
        $receivedProperties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();
        if (Yii::$app->request->isPost) {
            $model->toe_image = UploadedFile::getInstance($model, 'toe_image');

            if ($model->toe_image <> null) {
                // echo "not null"; die;
                if ($model->upload()) {
                    $model->toe_image = $model->toe_image->baseName . '.' . $model->toe_image->extension;
                    if ($model->quotation_status > 0 && $model->quotation_status <= 4) {
                        $date = date("Y-m-d H:i:s");
                        $model->toe_signed_and_received = 5;
                        $model->toe_signed_and_received_date = $date;
                    }
                    if (!$model->save()) {
                        echo "<pre>";
                        print_r($model->errors);
                        die;
                    } else {

                        Yii::$app->crmQuotationHelperFunctions->addStatusHistory($model, 10);
                    }
                    return $this->redirect(['crm-quotations/step_10?id=' . $id]);
                } else {

                }
            } else {

                return $this->redirect(['crm-quotations/step_10?id=' . $id]);
            }
        }
        return $this->render('steps/_step10', [
            'model' => $model,
            'receivedProperties' => $receivedProperties,
            'quotation' => $model
        ]);
    }
    public function actionStep_10($id)
    {
        $this->checkLogin();
        $model = $this->findModel($id);
        $old_toe_image = $model->toe_image;
        $receivedProperties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();

        if (Yii::$app->request->isPost) {

            $model->toe_image = UploadedFile::getInstance($model, 'toe_image');
            /* echo "<pre>";
             print_r($_POST['CrmQuotations']['payment_status']);
             die;*/

            if ($model->toe_image <> null) {

                if ($model->upload()) {
                    $model->toe_image = $model->toe_image->baseName . '.' . $model->toe_image->extension;
                    if ($model->quotation_status > 0 && $model->quotation_status <= 4) {
                        $date = date("Y-m-d H:i:s");
                        $model->toe_signed_and_received = 5;
                        $model->toe_signed_and_received_date = $date;
                    }
                    if (isset($_POST['CrmQuotations']['payment_status']) && $_POST['CrmQuotations']['payment_status'] <> null) {

                        $model->payment_status = $_POST['CrmQuotations']['payment_status'];
                    }
                    if (!$model->save()) {
                        echo "<pre>";
                        print_r($model->errors);
                        die;
                    } else {
                        Yii::$app->crmQuotationHelperFunctions->addStatusHistory($model, 10);

                        // conversion for valuation
                        if($model->id > 3030){
                            if($model->type_of_service == 1){
                                if ($model->converted == 0) {
                                    $val_idz = [];
                                    $uniqueReferencePortfolio = Yii::$app->appHelperFunctions->uniqueReferencePortfolio;
    
                                    //convert to valutaion
                                    foreach ($receivedProperties as $key => $property) {
                                        $property_detail = CrmReceivedProperties::find()->where(['id' => $property->id])->one();
                                        $toe_fee_total = $property_detail->toe_fee;
                                        $total_discount = 0;
                                        $total_discount_amount = 0;
                                        $discount = 0;
                                        $netValuationFee = $toe_fee_total - $discount;
    
                                        $aprvd_total_discount = 0;
                                        $aprvd_total_discount_amount = 0;
                                        $aprvd_discount = 0;
                                        $aprvd_netValuationFee = $toe_fee_total - $aprvd_discount;
    
                                        // assign fee master file property type based on property id    
                                        $property_type = yii::$app->quotationHelperFunctions->getPropertyTypeApproach($property->property_id, $property->valuation_approach);
    
                                        if ($model->no_of_property_discount > 0) {
                                            $no_of_property_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->no_of_property_discount, $toe_fee_total);
                                            $total_discount = $total_discount + $model->no_of_property_discount;
                                            $total_discount_amount = $total_discount_amount + $no_of_property_discount_amount;
                                        }
                                        // for aproved number of property discount
                                        if ($model->aprvd_no_of_property_discount > 0) {
                                            $aprvd_no_of_property_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->aprvd_no_of_property_discount, $toe_fee_total);
                                            $aprvd_total_discount = $aprvd_total_discount + $model->aprvd_no_of_property_discount;
                                            $aprvd_total_discount_amount = $aprvd_total_discount_amount + $aprvd_no_of_property_discount_amount;
                                        }
                                        if ($model->same_building_discount > 0) {
                                            $same_building_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->same_building_discount, $toe_fee_total);
                                            $total_discount = $total_discount + $model->same_building_discount;
                                            $total_discount_amount = $total_discount_amount + $same_building_discount_amount;
                                        }
                                        // for aproved same building discount
                                        if ($model->aprvd_same_building_discount > 0) {
                                            $aprvd_same_building_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->aprvd_same_building_discount, $toe_fee_total);
                                            $aprvd_total_discount = $aprvd_total_discount + $model->aprvd_same_building_discount;
                                            $aprvd_total_discount_amount = $aprvd_total_discount_amount + $aprvd_same_building_discount_amount;
                                        }
                                        if ($model->first_time_discount > 0) {
                                            $first_time_fee_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->first_time_discount, $toe_fee_total);
                                            $total_discount = $total_discount + $model->first_time_discount;
                                            $total_discount_amount = $total_discount_amount + $first_time_fee_discount_amount;
                                        }
                                        // for aproved first time discount
                                        if ($model->aprvd_first_time_discount > 0) {
                                            $aprvd_first_time_fee_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->aprvd_first_time_discount, $toe_fee_total);
                                            $aprvd_total_discount = $aprvd_total_discount + $model->aprvd_first_time_discount;
                                            $aprvd_total_discount_amount = $aprvd_total_discount_amount + $aprvd_first_time_fee_discount_amount;
                                        }
    
                                        $netValuationFee = $netValuationFee - $total_discount_amount;
                                        $aprvd_netValuationFee = $aprvd_netValuationFee - $aprvd_total_discount_amount;
                                        $discount_quotations = 0;
    
                                        if ($model->relative_discount_toe != null) {
                                            $discount = yii::$app->quotationHelperFunctions->getDiscountRupee($toe_fee_total, $model->relative_discount_toe);
                                            $netValuationFee = $netValuationFee - $discount;
                                            $aprvd_netValuationFee = $aprvd_netValuationFee - $discount;
                                        }
                                        if ($model->relative_discount != null) {
                                            $discount_quotations = yii::$app->quotationHelperFunctions->getDiscountRupee($toe_fee_total, $model->relative_discount);
                                        }
    
                                        $discount_net_fee = $netValuationFee;
                                        $aprvd_discount_net_fee = $aprvd_netValuationFee;
    
                                        //urgencyfee check
                                        if ($model->tat_requirements > 0) {
                                            $urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $netValuationFee, $property_type, $model);
                                            $netValuationFee = $netValuationFee + $urgencyfee['amount'];
    
                                            $aprvd_urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $aprvd_netValuationFee, $property_type, $model);
                                            $aprvd_netValuationFee = $aprvd_netValuationFee + $aprvd_urgencyfee['amount'];
                                        }
    
                                        //Advance Payment Terms
    
    
                                        if ($model->client->client_type != 'bank') {
                                            $advance_payment_terms_data = explode('%', $model->advance_payment_terms);
                                            $advance_payment_terms = yii::$app->quotationHelperFunctions->getAdvancePaymentfee($advance_payment_terms_data[0], $netValuationFee, $model);
                                            $netValuationFee = $netValuationFee + $advance_payment_terms['amount'];
    
                                            $aprvd_advance_payment_terms_data = explode('%', $model->advance_payment_terms);
                                            $aprvd_advance_payment_terms = yii::$app->quotationHelperFunctions->getAdvancePaymentfee($aprvd_advance_payment_terms_data[0], $aprvd_netValuationFee, $model);
                                            $aprvd_netValuationFee = $aprvd_netValuationFee + $aprvd_advance_payment_terms['amount'];
                                        }
    
    
                                        $VAT = 0;
                                        $aprvd_VAT = 0;
                                        // $VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
                                        if ($model->client->vat == 1) {
                                            $VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
    
                                            $aprvd_VAT = yii::$app->quotationHelperFunctions->getVatTotal($aprvd_netValuationFee);
                                        }
    
    
    
                                        if ($model->client->id == 1) {
                                            $vat_amount = yii::$app->quotationHelperFunctions->getVatTotal_add($netValuationFee);
                                            $finalFeePayable = $netValuationFee - $vat_amount;
    
                                            $aprvd_vat_amount = yii::$app->quotationHelperFunctions->getVatTotal_add($aprvd_netValuationFee);
                                            $aprvd_finalFeePayable = $aprvd_netValuationFee - $aprvd_vat_amount;
                                        } 
                                        else {
                                            // $finalFeePayable = $netValuationFee + $VAT;
                                            // $aprvd_finalFeePayable = $aprvd_netValuationFee + $aprvd_VAT;
                                            $finalFeePayable = $netValuationFee;
                                            $aprvd_finalFeePayable = $aprvd_netValuationFee;
                                        }
    
    
                                        $property_detail->converted_fee = ($finalFeePayable <> $aprvd_finalFeePayable) ? $aprvd_finalFeePayable : $finalFeePayable;
                                        $property_detail->save();
    
    
                                        if($property->valuation_approach == 1){
                                            $valuationApproach = 0; // for market approach
                                        } else if($property->valuation_approach == 2){
                                            $valuationApproach = 1; // for income approach
                                        } else if($property->valuation_approach == 3){
                                            $valuationApproach = 2; // for profit approach
                                        } else if($property->valuation_approach == 4){
                                            $valuationApproach = 3; // for cost approach
                                        } else {
    
                                        }
    
                                        //Step 1
                                        $valuation = new Valuation();
                                        $property_detail->instruction_date = $model->instruction_date;
                                        $property_detail->target_date = $model->target_date;
                                        $valuation->setAttributes($property->attributes);
                                        $valuation->instruction_date = $model->instruction_date;
                                        $valuation->target_date = $model->target_date;
                                        $valuation->no_of_owners = 0;
                                        $valuation->valuation_scope = $model->scope_of_service;
                                        $valuation->purpose_of_valuation = $model->purpose_of_valuation;
                                        $valuation->inspection_type = $model->inspection_type;
                                        unset($valuation->id);
                                        $valuation->reference_number = Yii::$app->appHelperFunctions->uniqueReference;
                                        //  $valuation->reference_number = 'REV-2022-6479';
                                        $valuation->client_id = $model->client_name;
                                        // $valuation->email_subject = $valuation->reference_number;
                                        $valuation->email_subject = $model->email_subject;
                                        $valuation->unit_number = ($property->unit_number <> null) ? $property->unit_number : 0;
                                        $valuation->quotation_property = $property->id;
                                        $valuation->quotation_id = $id;
                                        $valuation->client_module_id = $model->client_module_id;
                                        $valuation->trustee_id = $model->trustee_id;
                                        if ($model->payment_status == 1) {
    
                                            $valuation->fee = number_format(((($finalFeePayable <> $aprvd_finalFeePayable) ? $aprvd_finalFeePayable : $finalFeePayable) * ('0.' . $model->first_half_payment)), 2, '.', '');
                                        } else {
                                            $valuation->fee = ($finalFeePayable <> $aprvd_finalFeePayable) ? $aprvd_finalFeePayable : $finalFeePayable;
                                        }
    
                                        $valuation->total_fee = ($finalFeePayable <> $aprvd_finalFeePayable) ? $aprvd_finalFeePayable : $finalFeePayable;
                                        // $valuation->total_fee = ($netValuationFee <> $aprvd_netValuationFee) ? $aprvd_netValuationFee : $netValuationFee;
                                        $valuation->valuation_status = 1;
                                        $valuation->client_fixed_fee_check = 1;
                                        $valuation->client_name_passport = ($property->client_name_passport <> null) ? $property->client_name_passport : '-';
                                        $valuation->service_officer_name = ($property->service_officer_name <> null) ? $property->service_officer_name : 142;
                                        $valuation->land_size = ($property->land_size <> null) ? $property->land_size : 0;
                                        $valuation->building_info = $property->building_info;
                                        $valuation->client_invoice_type = $model->client_invoice_type;
                                        $valuation->valuation_approach = $valuationApproach;
                                        if($model->portfolio == 1){
                                            $valuation->portfolio = 1;
                                            $valuation->ref_portfolio = $uniqueReferencePortfolio;
                                        }
                                        if (!$valuation->save()) {
                                            echo "<pre>";
                                            print_r($valuation->errors);
                                            die('here');
    
                                        } else {
                                            $val_idz[] = $valuation->id;
    
                                            //Step 101
                                            $valuationDetail = new ValuationDetail();
                                            $valuationDetail->building_info = $property->building_info;
                                            $valuationDetail->building_number = ($property->building_number <> null) ? "$property->building_number" : "0";
                                            $valuationDetail->plot_number = $property->plot_number;
                                            $valuationDetail->street = $property->street;
                                            $valuationDetail->community = $property->community;
                                            $valuationDetail->sub_community = $property->sub_community;
                                            $valuationDetail->city = $property->city;
                                            $valuationDetail->property_id = $property->property_id;
                                            $valuationDetail->property_category = $property->property_category;
                                            $valuationDetail->tenure = $property->tenure;
                                            $valuationDetail->land_size = ($property->land_size <> null) ? $property->land_size : 0;
                                            $valuationDetail->built_up_area = ($property->built_up_area <> null) ? $property->built_up_area : 0;
                                            $valuationDetail->valuation_id = $valuation->id;
                                            $valuationDetail->status = null;
    
                                            if (!$valuationDetail->save()) {
                                                echo "<pre>";
                                                print_r($valuationDetail->errors);
                                                die('here');
                                            }
    
    
                                        }
    
                                        //Step 2
                                        $CrmReceivedDocs = CrmReceivedDocs::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();
    
                                        $owners = \app\models\CrmQuotationOwner::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->asArray()->all();
                                        $received_docs = \app\models\CrmReceivedDocsFiles::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->asArray()->all();
                                        $step_2 = new ReceivedDocs();
                                        unset($step_2->id);
                                        $step_2->setAttributes($CrmReceivedDocs->attributes);
                                        $step_2->valuation_id = $valuation->id;
                                        $step_2->save();
                                        if (!$step_2->save()) {
                                            echo "<pre>";
                                            print_r($step_2->errors);
                                            die;
    
                                        }
    
    
                                        // Save all owners terms
                                        /* foreach ($owners as $owner_data) {
                                            $owner_data_detail = new ValuationOwners();
                                            $owner_data_detail->name = $owner_data['name'];
                                            $owner_data_detail->percentage = $owner_data['percentage'];
                                            $owner_data_detail->index_id = $owner_data['index_id'];
                                            $owner_data_detail->valuation_id = $valuation->id;
                                            // $owner_data_detail->save();
                                            if (!$owner_data_detail->save()) {
                                                echo "<pre>";
                                                print_r($step_2->errors);
                                                die;
    
                                            }
                                        }*/
    
                                        // Save all Documents terms
                                        foreach ($received_docs as $received_doc) {
                                            $received_data_detail = new ReceivedDocsFiles();
                                            $received_data_detail->document_id = $received_doc['document_id'];
                                            $received_data_detail->attachment = $received_doc['attachment'];
                                            $received_data_detail->valuation_id = $valuation->id;
                                            $received_data_detail->save();
                                        }
    
    
                                        //Step 3
                                        $step_3_data = CrmValuationConflict::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();
                                        unset($step_3_data->id);
                                        $step_3 = new ValuationConflict();
                                        $step_3->setAttributes($step_3_data->attributes);
                                        $step_3->valuation_id = $valuation->id;
                                        if (!$step_3->save()) {
                                            echo "<pre>";
                                            print_r($step_3->errors);
                                            die;
                                        }
    
    
                                        /*     //step 4
                                            $step_4_data = CrmScheduleInspection::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();
                                            unset($step_4_data->id);
                                            $step_4 = new ScheduleInspection();
                                            $step_4->setAttributes($step_4_data->attributes);
                                            $step_4->valuation_id = $valuation->id;
                                            if (!$step_4->save()) {
                                                echo "<pre>";
                                                print_r($step_4->errors);
                                                die;
    
                                            }
    
                                            //step 5
                                            $step_5_data = CrmInspectProperty::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();
    
                                            unset($step_5_data->id);
                                            $step_5 = new InspectProperty();
                                            $step_5->setAttributes($step_5_data->attributes);
    
                                            $step_5->valuation_id = $valuation->id;;
    
                                            if (!$step_5->save()) {
                                                echo "<pre>";
                                                print_r($step_5->errors);
                                                die;
                                            }*/
    
                                        /*   //step 6
                                        $step_6_data = CrmQuotationConfigraions::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();
                                        $configurationFiles = \app\models\CrmQuotationConfigraionsFiles::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->asArray()->all();
                                        unset($step_6_data->id);
                                        $step_6 = new ValuationConfiguration();
                                        $step_6->setAttributes($step_6_data->attributes);
                                        $step_6->valuation_id = $valuation->id;
                                        if (!$step_6->save()) {
                                            echo "<pre>";
                                            print_r($step_6->errors);
                                            die;
                                        }*/
    
                                        /* if (!empty($configurationFiles)) {
                                            foreach ($configurationFiles as $config_data) {
                                                $config_detail = new ConfigurationFiles();
                                                $config_detail->type = $config_data['type'];
                                                $config_detail->floor = $config_data['floor'];
                                                $config_detail->flooring = $config_data['flooring'];
                                                $config_detail->ceilings = $config_data['ceilings'];
                                                $config_detail->speciality = $config_data['speciality'];
                                                $config_detail->upgrade = $config_data['upgrade'];
                                                $config_detail->attachment = $config_data['attachment'];
                                                $config_detail->index_id = $config_data['index_id'];
                                                $config_detail->valuation_id = $valuation->id;
                                                $config_detail->checked_image = $config_data['checked_image'];
                                                $config_detail->save();
                                            }
                                        }
                                        $step_6_latest = ValuationConfiguration::find()->where(['valuation_id' => $valuation->id])->one();
                                        if ($step_6_data->over_all_upgrade <> null) {
                                            \Yii::$app->db->createCommand("UPDATE valuation_configuration SET over_all_upgrade=" . $step_6_data->over_all_upgrade . " WHERE id=" . $step_6_latest->id)->execute();
                                        }*/
    
    
                                    }
    
    
                                    \Yii::$app->db->createCommand("UPDATE crm_quotations SET converted=" . 1 . " WHERE id=" . $id)->execute();
                                    if ($model->payment_status == 1) {
    
                                    }
                                    // dd($val_idz[0]);
                                    if (is_array($val_idz) && $val_idz <> null) {
                                        $valuation = Valuation::findOne($val_idz[0]);
    
                                        $curl_handle_1 = curl_init();
                                        if ($model->payment_status == 1) {
                                            curl_setopt($curl_handle_1, CURLOPT_URL, Url::toRoute(['valuation/invoice_toe_pdf_half_first', 'id' => $valuation->id]));
                                        } else {
                                            curl_setopt($curl_handle_1, CURLOPT_URL, Url::toRoute(['valuation/invoice_toe_pdf', 'id' => $valuation->id]));
                                        }
                                        curl_setopt($curl_handle_1, CURLOPT_CONNECTTIMEOUT, 2);
    
                                        curl_setopt($curl_handle_1, CURLOPT_RETURNTRANSFER, 1);
                                        curl_setopt($curl_handle_1, CURLOPT_USERAGENT, 'Maxima');
    
                                        $path = curl_exec($curl_handle_1);
                                        $ip = curl_getinfo($curl_handle_1, CURLINFO_PRIMARY_IP);
                                        curl_close($curl_handle_1);
                                        $attachments[] = $path;
    
                                        $notifyData = [
                                            'client' => $valuation->client,
                                            'uid' => $valuation->id,
                                            'attachments' => $attachments,
                                            'subject' => $valuation->email_subject,
                                            //'valuer' => $valuation->approver->email,
                                            'valuer' => '',
                                            'replacements' => [
                                                '{clientName}' => $valuation->client->title,
                                            ],
                                        ];
    
                                        if ($model->payment_status == 1) {
                                            if ($model->client->client_type != 'bank') {
                                                \app\modules\wisnotify\listners\NotifyEvent::fire1('quotation.taxinvoiceFirstHalf', $notifyData);
                                            }
                                        } else {
                                            //a  \app\modules\wisnotify\listners\NotifyEvent::fire1('quotation.taxinvoice', $notifyData);
                                        }
    
                                        //  \app\modules\wisnotify\listners\NotifyEvent::fire1('quotation.taxinvoiceFirstHalf', $notifyData);
    
                                    }
    
                                }
                            }

                        }else{
                            if ($model->converted == 0) {
                                $val_idz = [];
                                $uniqueReferencePortfolio = Yii::$app->appHelperFunctions->uniqueReferencePortfolio;

                                //convert to valutaion
                                foreach ($receivedProperties as $key => $property) {
                                    $property_detail = CrmReceivedProperties::find()->where(['id' => $property->id])->one();
                                    $toe_fee_total = $property_detail->toe_fee;
                                    $total_discount = 0;
                                    $total_discount_amount = 0;
                                    $discount = 0;
                                    $netValuationFee = $toe_fee_total - $discount;

                                    $aprvd_total_discount = 0;
                                    $aprvd_total_discount_amount = 0;
                                    $aprvd_discount = 0;
                                    $aprvd_netValuationFee = $toe_fee_total - $aprvd_discount;

                                    // assign fee master file property type based on property id    
                                    $property_type = yii::$app->quotationHelperFunctions->getPropertyTypeApproach($property->property_id, $property->valuation_approach);

                                    if ($model->no_of_property_discount > 0) {
                                        $no_of_property_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->no_of_property_discount, $toe_fee_total);
                                        $total_discount = $total_discount + $model->no_of_property_discount;
                                        $total_discount_amount = $total_discount_amount + $no_of_property_discount_amount;
                                    }
                                    // for aproved number of property discount
                                    if ($model->aprvd_no_of_property_discount > 0) {
                                        $aprvd_no_of_property_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->aprvd_no_of_property_discount, $toe_fee_total);
                                        $aprvd_total_discount = $aprvd_total_discount + $model->aprvd_no_of_property_discount;
                                        $aprvd_total_discount_amount = $aprvd_total_discount_amount + $aprvd_no_of_property_discount_amount;
                                    }
                                    if ($model->same_building_discount > 0) {
                                        $same_building_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->same_building_discount, $toe_fee_total);
                                        $total_discount = $total_discount + $model->same_building_discount;
                                        $total_discount_amount = $total_discount_amount + $same_building_discount_amount;
                                    }
                                    // for aproved same building discount
                                    if ($model->aprvd_same_building_discount > 0) {
                                        $aprvd_same_building_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->aprvd_same_building_discount, $toe_fee_total);
                                        $aprvd_total_discount = $aprvd_total_discount + $model->aprvd_same_building_discount;
                                        $aprvd_total_discount_amount = $aprvd_total_discount_amount + $aprvd_same_building_discount_amount;
                                    }
                                    if ($model->first_time_discount > 0) {
                                        $first_time_fee_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->first_time_discount, $toe_fee_total);
                                        $total_discount = $total_discount + $model->first_time_discount;
                                        $total_discount_amount = $total_discount_amount + $first_time_fee_discount_amount;
                                    }
                                    // for aproved first time discount
                                    if ($model->aprvd_first_time_discount > 0) {
                                        $aprvd_first_time_fee_discount_amount = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->aprvd_first_time_discount, $toe_fee_total);
                                        $aprvd_total_discount = $aprvd_total_discount + $model->aprvd_first_time_discount;
                                        $aprvd_total_discount_amount = $aprvd_total_discount_amount + $aprvd_first_time_fee_discount_amount;
                                    }

                                    $netValuationFee = $netValuationFee - $total_discount_amount;
                                    $aprvd_netValuationFee = $aprvd_netValuationFee - $aprvd_total_discount_amount;
                                    $discount_quotations = 0;

                                    if ($model->relative_discount_toe != null) {
                                        $discount = yii::$app->quotationHelperFunctions->getDiscountRupee($toe_fee_total, $model->relative_discount_toe);
                                        $netValuationFee = $netValuationFee - $discount;
                                        $aprvd_netValuationFee = $aprvd_netValuationFee - $discount;
                                    }
                                    if ($model->relative_discount != null) {
                                        $discount_quotations = yii::$app->quotationHelperFunctions->getDiscountRupee($toe_fee_total, $model->relative_discount);
                                    }

                                    $discount_net_fee = $netValuationFee;
                                    $aprvd_discount_net_fee = $aprvd_netValuationFee;

                                    //urgencyfee check
                                    if ($model->tat_requirements > 0) {
                                        $urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $netValuationFee, $property_type, $model);
                                        $netValuationFee = $netValuationFee + $urgencyfee['amount'];

                                        $aprvd_urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $aprvd_netValuationFee, $property_type, $model);
                                        $aprvd_netValuationFee = $aprvd_netValuationFee + $aprvd_urgencyfee['amount'];
                                    }

                                    //Advance Payment Terms


                                    if ($model->client->client_type != 'bank') {
                                        $advance_payment_terms_data = explode('%', $model->advance_payment_terms);
                                        $advance_payment_terms = yii::$app->quotationHelperFunctions->getAdvancePaymentfee($advance_payment_terms_data[0], $netValuationFee, $model);
                                        $netValuationFee = $netValuationFee + $advance_payment_terms['amount'];

                                        $aprvd_advance_payment_terms_data = explode('%', $model->advance_payment_terms);
                                        $aprvd_advance_payment_terms = yii::$app->quotationHelperFunctions->getAdvancePaymentfee($aprvd_advance_payment_terms_data[0], $aprvd_netValuationFee, $model);
                                        $aprvd_netValuationFee = $aprvd_netValuationFee + $aprvd_advance_payment_terms['amount'];
                                    }


                                    $VAT = 0;
                                    $aprvd_VAT = 0;
                                    // $VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
                                    if ($model->client->vat == 1) {
                                        $VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);

                                        $aprvd_VAT = yii::$app->quotationHelperFunctions->getVatTotal($aprvd_netValuationFee);
                                    }



                                    if ($model->client->id == 1) {
                                        $vat_amount = yii::$app->quotationHelperFunctions->getVatTotal_add($netValuationFee);
                                        $finalFeePayable = $netValuationFee - $vat_amount;

                                        $aprvd_vat_amount = yii::$app->quotationHelperFunctions->getVatTotal_add($aprvd_netValuationFee);
                                        $aprvd_finalFeePayable = $aprvd_netValuationFee - $aprvd_vat_amount;
                                    } 
                                    else {
                                        // $finalFeePayable = $netValuationFee + $VAT;
                                        // $aprvd_finalFeePayable = $aprvd_netValuationFee + $aprvd_VAT;
                                        $finalFeePayable = $netValuationFee;
                                        $aprvd_finalFeePayable = $aprvd_netValuationFee;
                                    }


                                    $property_detail->converted_fee = ($finalFeePayable <> $aprvd_finalFeePayable) ? $aprvd_finalFeePayable : $finalFeePayable;
                                    $property_detail->save();


                                    if($property->valuation_approach == 1){
                                        $valuationApproach = 0; // for market approach
                                    } else if($property->valuation_approach == 2){
                                        $valuationApproach = 1; // for income approach
                                    } else if($property->valuation_approach == 3){
                                        $valuationApproach = 2; // for profit approach
                                    } else if($property->valuation_approach == 4){
                                        $valuationApproach = 3; // for cost approach
                                    } else {

                                    }

                                    //Step 1
                                    $valuation = new Valuation();
                                    $property_detail->instruction_date = $model->instruction_date;
                                    $property_detail->target_date = $model->target_date;
                                    $valuation->setAttributes($property->attributes);
                                    $valuation->instruction_date = $model->instruction_date;
                                    $valuation->target_date = $model->target_date;
                                    $valuation->no_of_owners = 0;
                                    $valuation->valuation_scope = $model->scope_of_service;
                                    $valuation->purpose_of_valuation = $model->purpose_of_valuation;
                                    $valuation->inspection_type = $model->inspection_type;
                                    unset($valuation->id);
                                    $valuation->reference_number = Yii::$app->appHelperFunctions->uniqueReference;
                                    //  $valuation->reference_number = 'REV-2022-6479';
                                    $valuation->client_id = $model->client_name;
                                    // $valuation->email_subject = $valuation->reference_number;
                                    $valuation->email_subject = $model->email_subject;
                                    $valuation->unit_number = ($property->unit_number <> null) ? $property->unit_number : 0;
                                    $valuation->quotation_property = $property->id;
                                    $valuation->quotation_id = $id;
                                    $valuation->client_module_id = $model->client_module_id;
                                    $valuation->trustee_id = $model->trustee_id;
                                    if ($model->payment_status == 1) {

                                        $valuation->fee = number_format(((($finalFeePayable <> $aprvd_finalFeePayable) ? $aprvd_finalFeePayable : $finalFeePayable) * ('0.' . $model->first_half_payment)), 2, '.', '');
                                    } else {
                                        $valuation->fee = ($finalFeePayable <> $aprvd_finalFeePayable) ? $aprvd_finalFeePayable : $finalFeePayable;
                                    }

                                    $valuation->total_fee = ($finalFeePayable <> $aprvd_finalFeePayable) ? $aprvd_finalFeePayable : $finalFeePayable;
                                    // $valuation->total_fee = ($netValuationFee <> $aprvd_netValuationFee) ? $aprvd_netValuationFee : $netValuationFee;
                                    $valuation->valuation_status = 1;
                                    $valuation->client_fixed_fee_check = 1;
                                    $valuation->client_name_passport = ($property->client_name_passport <> null) ? $property->client_name_passport : '-';
                                    $valuation->service_officer_name = ($property->service_officer_name <> null) ? $property->service_officer_name : 142;
                                    $valuation->land_size = ($property->land_size <> null) ? $property->land_size : 0;
                                    $valuation->building_info = $property->building_info;
                                    $valuation->client_invoice_type = $model->client_invoice_type;
                                    $valuation->valuation_approach = $valuationApproach;
                                    if($model->portfolio == 1){
                                        $valuation->portfolio = 1;
                                        $valuation->ref_portfolio = $uniqueReferencePortfolio;
                                    }
                                    if (!$valuation->save()) {
                                        echo "<pre>";
                                        print_r($valuation->errors);
                                        die('here');

                                    } else {
                                        $val_idz[] = $valuation->id;

                                        //Step 101
                                        $valuationDetail = new ValuationDetail();
                                        $valuationDetail->building_info = $property->building_info;
                                        $valuationDetail->building_number = ($property->building_number <> null) ? "$property->building_number" : "0";
                                        $valuationDetail->plot_number = $property->plot_number;
                                        $valuationDetail->street = $property->street;
                                        $valuationDetail->community = $property->community;
                                        $valuationDetail->sub_community = $property->sub_community;
                                        $valuationDetail->city = $property->city;
                                        $valuationDetail->property_id = $property->property_id;
                                        $valuationDetail->property_category = $property->property_category;
                                        $valuationDetail->tenure = $property->tenure;
                                        $valuationDetail->land_size = ($property->land_size <> null) ? $property->land_size : 0;
                                        $valuationDetail->built_up_area = ($property->built_up_area <> null) ? $property->built_up_area : 0;
                                        $valuationDetail->valuation_id = $valuation->id;
                                        $valuationDetail->status = null;

                                        if (!$valuationDetail->save()) {
                                            echo "<pre>";
                                            print_r($valuationDetail->errors);
                                            die('here');
                                        }


                                    }

                                    //Step 2
                                    $CrmReceivedDocs = CrmReceivedDocs::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();

                                    $owners = \app\models\CrmQuotationOwner::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->asArray()->all();
                                    $received_docs = \app\models\CrmReceivedDocsFiles::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->asArray()->all();
                                    $step_2 = new ReceivedDocs();
                                    unset($step_2->id);
                                    $step_2->setAttributes($CrmReceivedDocs->attributes);
                                    $step_2->valuation_id = $valuation->id;
                                    $step_2->save();
                                    if (!$step_2->save()) {
                                        echo "<pre>";
                                        print_r($step_2->errors);
                                        die;

                                    }


                                    // Save all owners terms
                                    /* foreach ($owners as $owner_data) {
                                        $owner_data_detail = new ValuationOwners();
                                        $owner_data_detail->name = $owner_data['name'];
                                        $owner_data_detail->percentage = $owner_data['percentage'];
                                        $owner_data_detail->index_id = $owner_data['index_id'];
                                        $owner_data_detail->valuation_id = $valuation->id;
                                        // $owner_data_detail->save();
                                        if (!$owner_data_detail->save()) {
                                            echo "<pre>";
                                            print_r($step_2->errors);
                                            die;

                                        }
                                    }*/

                                    // Save all Documents terms
                                    foreach ($received_docs as $received_doc) {
                                        $received_data_detail = new ReceivedDocsFiles();
                                        $received_data_detail->document_id = $received_doc['document_id'];
                                        $received_data_detail->attachment = $received_doc['attachment'];
                                        $received_data_detail->valuation_id = $valuation->id;
                                        $received_data_detail->save();
                                    }


                                    //Step 3
                                    $step_3_data = CrmValuationConflict::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();
                                    unset($step_3_data->id);
                                    $step_3 = new ValuationConflict();
                                    $step_3->setAttributes($step_3_data->attributes);
                                    $step_3->valuation_id = $valuation->id;
                                    if (!$step_3->save()) {
                                        echo "<pre>";
                                        print_r($step_3->errors);
                                        die;
                                    }


                                    /*     //step 4
                                        $step_4_data = CrmScheduleInspection::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();
                                        unset($step_4_data->id);
                                        $step_4 = new ScheduleInspection();
                                        $step_4->setAttributes($step_4_data->attributes);
                                        $step_4->valuation_id = $valuation->id;
                                        if (!$step_4->save()) {
                                            echo "<pre>";
                                            print_r($step_4->errors);
                                            die;

                                        }

                                        //step 5
                                        $step_5_data = CrmInspectProperty::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();

                                        unset($step_5_data->id);
                                        $step_5 = new InspectProperty();
                                        $step_5->setAttributes($step_5_data->attributes);

                                        $step_5->valuation_id = $valuation->id;;

                                        if (!$step_5->save()) {
                                            echo "<pre>";
                                            print_r($step_5->errors);
                                            die;
                                        }*/

                                    /*   //step 6
                                    $step_6_data = CrmQuotationConfigraions::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();
                                    $configurationFiles = \app\models\CrmQuotationConfigraionsFiles::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->asArray()->all();
                                    unset($step_6_data->id);
                                    $step_6 = new ValuationConfiguration();
                                    $step_6->setAttributes($step_6_data->attributes);
                                    $step_6->valuation_id = $valuation->id;
                                    if (!$step_6->save()) {
                                        echo "<pre>";
                                        print_r($step_6->errors);
                                        die;
                                    }*/

                                    /* if (!empty($configurationFiles)) {
                                        foreach ($configurationFiles as $config_data) {
                                            $config_detail = new ConfigurationFiles();
                                            $config_detail->type = $config_data['type'];
                                            $config_detail->floor = $config_data['floor'];
                                            $config_detail->flooring = $config_data['flooring'];
                                            $config_detail->ceilings = $config_data['ceilings'];
                                            $config_detail->speciality = $config_data['speciality'];
                                            $config_detail->upgrade = $config_data['upgrade'];
                                            $config_detail->attachment = $config_data['attachment'];
                                            $config_detail->index_id = $config_data['index_id'];
                                            $config_detail->valuation_id = $valuation->id;
                                            $config_detail->checked_image = $config_data['checked_image'];
                                            $config_detail->save();
                                        }
                                    }
                                    $step_6_latest = ValuationConfiguration::find()->where(['valuation_id' => $valuation->id])->one();
                                    if ($step_6_data->over_all_upgrade <> null) {
                                        \Yii::$app->db->createCommand("UPDATE valuation_configuration SET over_all_upgrade=" . $step_6_data->over_all_upgrade . " WHERE id=" . $step_6_latest->id)->execute();
                                    }*/


                                }


                                \Yii::$app->db->createCommand("UPDATE crm_quotations SET converted=" . 1 . " WHERE id=" . $id)->execute();
                                if ($model->payment_status == 1) {

                                }
                                // dd($val_idz[0]);
                                if (is_array($val_idz) && $val_idz <> null) {
                                    $valuation = Valuation::findOne($val_idz[0]);

                                    $curl_handle_1 = curl_init();
                                    if ($model->payment_status == 1) {
                                        curl_setopt($curl_handle_1, CURLOPT_URL, Url::toRoute(['valuation/invoice_toe_pdf_half_first', 'id' => $valuation->id]));
                                    } else {
                                        curl_setopt($curl_handle_1, CURLOPT_URL, Url::toRoute(['valuation/invoice_toe_pdf', 'id' => $valuation->id]));
                                    }
                                    curl_setopt($curl_handle_1, CURLOPT_CONNECTTIMEOUT, 2);

                                    curl_setopt($curl_handle_1, CURLOPT_RETURNTRANSFER, 1);
                                    curl_setopt($curl_handle_1, CURLOPT_USERAGENT, 'Maxima');

                                    $path = curl_exec($curl_handle_1);
                                    $ip = curl_getinfo($curl_handle_1, CURLINFO_PRIMARY_IP);
                                    curl_close($curl_handle_1);
                                    $attachments[] = $path;

                                    $notifyData = [
                                        'client' => $valuation->client,
                                        'uid' => $valuation->id,
                                        'attachments' => $attachments,
                                        'subject' => $valuation->email_subject,
                                        //'valuer' => $valuation->approver->email,
                                        'valuer' => '',
                                        'replacements' => [
                                            '{clientName}' => $valuation->client->title,
                                        ],
                                    ];

                                    if ($model->payment_status == 1) {
                                        if ($model->client->client_type != 'bank') {
                                            \app\modules\wisnotify\listners\NotifyEvent::fire1('quotation.taxinvoiceFirstHalf', $notifyData);
                                        }
                                    } else {
                                        //a  \app\modules\wisnotify\listners\NotifyEvent::fire1('quotation.taxinvoice', $notifyData);
                                    }

                                    //  \app\modules\wisnotify\listners\NotifyEvent::fire1('quotation.taxinvoiceFirstHalf', $notifyData);

                                }

                            }
                        }
                        


                        if ($model->email_status_t_sent == 0) {
                            $notifyData = [
                                'client' => $model->client,
                                'attachments' => [],
                                'subject' => $model->email_subject,
                                'uid' => 'crm' . $model->id,
                                'instructing_person_email' => $model->instructing_party_email,
                                'replacements' => [
                                    '{clientName}' => $model->client->title,
                                ],
                            ];
                            if ($model->client->client_type != 'bank') {
                                if ($model->client_name > 0 && $model->email_subject <> null) {
                                    \app\modules\wisnotify\listners\NotifyEvent::fire23('toe.signed', $notifyData);
                                }
                            }
                            Yii::$app->db->createCommand()->update('crm_quotations', ['email_status_t_sent' => 1], 'id=' . $model->id . '')->execute();
                        }

                        // for 5% difference between recommended fee and approved fee   
                        if($model->id > 3001){ 

                            $feeDiffAmount = abs($model->quotation_recommended_fee - $model->final_fee_approved);
                            $feeDiffPercentage = abs($model->quotation_recommended_fee - $model->final_fee_approved) / min($model->quotation_recommended_fee, $model->final_fee_approved) * 100;

                            if($feeDiffPercentage > 10 && $model->email_status_fee_diff == 0 ){
                                $notifyData = [
                                    'client' => $model->client,
                                    'attachments' => [],
                                    'subject' => 'Quotation Fee: 10% difference between recommended and approved fee',
                                    'uid' => 'crm-' . $model->id,
                                    'replacements' => [
                                        '{quotationRef}' => $model->reference_number,
                                        '{recFee}' => round($model->quotation_recommended_fee, 2),
                                        '{apprvdFee}' => round($model->final_fee_approved, 2),
                                        '{feeDiffAmount}' => round($feeDiffAmount, 2),
                                        '{feeDiffPercent}' => round($feeDiffPercentage, 2),
                                    ],
                                ];
                                if ($model->client_name > 0) {
                                    \app\modules\wisnotify\listners\NotifyEvent::fireQuotationFeeDifference('quotation.feeDifference', $notifyData);
                                }
                                Yii::$app->db->createCommand()->update('crm_quotations', ['email_status_fee_diff' => 1], 'id=' . $model->id . '')->execute();

                                $modelDiff = new FeeDifference();

                                $modelDiff->type = 1;
                                $modelDiff->quotation_id = $model->id;
                                $modelDiff->recommended_fee = round($model->quotation_recommended_fee, 2);
                                $modelDiff->approved_fee = round($model->final_fee_approved, 2);
                                $modelDiff->fee_differ_amount = round($feeDiffAmount, 2);
                                $modelDiff->fee_differ_percent = round($feeDiffPercentage, 2);

                                if (!$modelDiff->save()) {
                                    echo "<pre>";
                                    print_r($modelDiff->errors);
                                    die;
                                }

                                // Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Fee difference mail send'));
                            }
                        }


                        if($model->id > 3030 && $model->type_of_service != 1){
                            Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                        }else{
                            Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Converted to Valuation successfully'));
                        }

                        
                        return $this->redirect(['crm-quotations/step_10?id=' . $id]);

                    }
                    return $this->redirect(['crm-quotations/step_10?id=' . $id]);
                }

            } else {

                return $this->redirect(['crm-quotations/step_10?id=' . $id]);
            }
        }
        return $this->render('steps/_step10', [
            'model' => $model,
            'receivedProperties' => $receivedProperties,
            'quotation' => $model
        ]);
    }


    public function actionStep_11($id)
    {
        $this->checkLogin();
        $model = $this->findModel($id);
        $receivedProperties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();

        if (Yii::$app->request->isPost) {

            $payment_image = '';
            $payment_image2 = '';
            if (UploadedFile::getInstance($model, 'payment_image') <> null) {
                $model->payment_image = UploadedFile::getInstance($model, 'payment_image');
                $payment_image = UploadedFile::getInstance($model, 'payment_image');
            }

            if (UploadedFile::getInstance($model, 'payment_image_2') <> null) {
                $model->payment_image_2 = UploadedFile::getInstance($model, 'payment_image_2');
                $payment_image2 = UploadedFile::getInstance($model, 'payment_image_2');
            }
            /*
                        $model->payment_image = UploadedFile::getInstance($model, 'payment_image');
                        $model->payment_image_2 = UploadedFile::getInstance($model, 'payment_image_2');*/
            if ($payment_image <> null) {
                if ($model->uploadpayment()) {

                    $model->payment_image = $model->payment_image->baseName . '.' . $model->payment_image->extension;

                    if ($model->quotation_status > 0 && $model->quotation_status <= 5) {
                        $date = date("Y-m-d H:i:s");
                        $model->quotation_status = 6;
                        $model->status_change_date = $date;
                        $model->payment_received_date = $date;
                    }
                    if ($model->save()) {

                        if ($model->email_status_pr == 0) {


                            // $curl_handle_1 = curl_init();
                            // if ($model->payment_status == 1) {
                            //     curl_setopt($curl_handle_1, CURLOPT_URL, Url::toRoute(['crm-quotations/send-quotation-payment-receipt', 'id' => $model->id]));
                            // } 
                            
                            // curl_setopt($curl_handle_1, CURLOPT_CONNECTTIMEOUT, 2);

                            // curl_setopt($curl_handle_1, CURLOPT_RETURNTRANSFER, 1);
                            // curl_setopt($curl_handle_1, CURLOPT_USERAGENT, 'Maxima');

                            // $path = curl_exec($curl_handle_1);
                            // $ip = curl_getinfo($curl_handle_1, CURLINFO_PRIMARY_IP);
                            // curl_close($curl_handle_1);
                            // $attachments[] = $path;

                            $paymentReceipt = $this->actionSendQuotationPaymentReceiptHalf($model->id);
                            if($paymentReceipt){
                                $attachments[] = $paymentReceipt;
                            }else{
                                $attachments = [];
                            }
                            

                            $notifyData = [
                                'client' => $model->client,
                                'attachments' => $attachments,
                                'subject' => $model->email_subject,
                                'uid' => 'crm' . $model->id,
                                'instructing_person_email' => $model->instructing_party_email,
                                'replacements' => [
                                    '{clientName}' => $model->client->title,
                                ],
                            ];
                            if ($model->client->client_type != 'bank') {
                                if ($model->client_name > 0 && $model->email_subject <> null) {
                                    \app\modules\wisnotify\listners\NotifyEvent::fire23('payment.received', $notifyData);
                                }
                            }
                            Yii::$app->db->createCommand()->update('crm_quotations', ['email_status_pr' => 1], 'id=' . $model->id . '')->execute();

                            if ($paymentReceipt) {
                                yii::$app->getsession()->addFlash('success', yii::t('app', 'Payment Receipt send successfully'));
                            }

                        }

                        // if ($model->email_status_pr == 0) {
                        //     $notifyData = [
                        //         'client' => $model->client,
                        //         'attachments' => [],
                        //         'subject' => $model->email_subject,
                        //         'uid' => 'crm' . $model->id,
                        //         'replacements' => [
                        //             '{clientName}' => $model->client->title,
                        //         ],
                        //     ];
                        //     if ($model->client->client_type != 'bank') {
                        //         if ($model->client_name > 0 && $model->email_subject <> null) {
                        //             \app\modules\wisnotify\listners\NotifyEvent::fire23('payment.received', $notifyData);
                        //         }
                        //     }
                        //     Yii::$app->db->createCommand()->update('crm_quotations', ['email_status_pr' => 1], 'id=' . $model->id . '')->execute();
                        // }

                       

                        Yii::$app->crmQuotationHelperFunctions->addStatusHistory($model);

                        Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Payment updated successfully'));
                        return $this->redirect(['crm-quotations/step_11?id=' . $id]);
                    } else {
                        if ($model->hasErrors()) {
                            foreach ($model->getErrors() as $error) {
                                if (count($error) > 0) {
                                    foreach ($error as $key => $val) {
                                        Yii::$app->getSession()->addFlash('error', $val);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                    }
                }
            }
            if ($payment_image2 <> null) {
                if ($model->uploadpayment2()) {
                    
                    $model->payment_image_2 = $model->payment_image_2->baseName . '.' . $model->payment_image_2->extension;
                    
                    if ($model->save()) {
                        
                        $valuations = Valuation::find()->where(['quotation_id' => $model->id])->all();
                        foreach ($valuations as $key => $valuation) {
                            $approvers_data = ValuationApproversData::find()->where(['valuation_id' => $valuation->id, 'approver_type' => 'approver'])->one();
                            Yii::$app->helperFunctions->getModelStepSubmit($approvers_data, $valuation);
                        }

                        // -- code for payment receipt -------------------------
                        $paymentReceipt = $this->actionSendQuotationPaymentReceiptFull($model->id);
                        if($paymentReceipt){
                            $attachments[] = $paymentReceipt;
                        }else{
                            $attachments = [];
                        }

                        $notifyData = [
                            'client' => $model->client,
                            'attachments' => $attachments,
                            'subject' => $model->email_subject,
                            'uid' => 'crm' . $model->id,
                            'instructing_person_email' => $model->instructing_party_email,
                            'replacements' => [
                                '{clientName}' => $model->client->title,
                            ],
                        ];
                        if ($model->client->client_type != 'bank') {
                            if ($model->client_name > 0 && $model->email_subject <> null) {
                                \app\modules\wisnotify\listners\NotifyEvent::fire23('payment.received', $notifyData);
                            }
                        }
                        
                        if ($paymentReceipt) {
                            yii::$app->getsession()->addFlash('success', yii::t('app', 'Payment Receipt send successfully'));
                        }
                        // -----------------------------------------------------

                        
                        Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Payment updated successfully'));
                        return $this->redirect(['crm-quotations/step_11?id=' . $id]);
                    } else {
                        if ($model->hasErrors()) {
                            foreach ($model->getErrors() as $error) {
                                if (count($error) > 0) {
                                    foreach ($error as $key => $val) {
                                        Yii::$app->getSession()->addFlash('error', $val);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    if ($model->hasErrors()) {
                        foreach ($model->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                    }
                }
            } else {
                return $this->redirect(['crm-quotations/step_11?id=' . $id]);
            }
        }
        return $this->render('steps/_step11', [
            'model' => $model,
            'receivedProperties' => $receivedProperties,
            'quotation' => $model,
        ]);
    }



    public function actionRevise($id)
    {
        //step 1
        $model_old = $this->findModel($id);
        $quotation_old = $this->findModel($id);
        $quoatation_history = CrmQuotations::find()->where(['parent_id' => $id])->orderBy(['id' => 'DESC'])->all();
        $quoatation_root = CrmQuotations::find()->where(['id' => $model_old->root_id])->one();
        $quoatation_history_root = CrmQuotations::find()->where(['root_id' => $model_old->root_id])->orderBy(['id' => 'DESC'])->all();
        

        unset($model_old->id);
        unset($model_old->status);
        unset($model_old->status_approve);
        unset($model_old->status_approve_toe);
        unset($model_old->created_by);
        unset($model_old->created_at);
        unset($model_old->updated_at);
        unset($model_old->updated_by);
        //  unset($model_old->trashed);
        unset($model_old->deleted_at);
        unset($model_old->deleted_by);


        $model_old->parent_id = $id;
        if (($model_old->root_id <> null && $model_old->root_id > 0)) {
            $model_old->reference_number = $quoatation_root->reference_number . '-V' . (count($quoatation_history_root) + 1);
        } else if ($quoatation_history <> null && count($quoatation_history) > 0) {
            $model_old->reference_number = $model_old->reference_number . '-V' . (count($quoatation_history) + 1);
        } else {
            $model_old->root_id = $id;
            $model_old->reference_number = $model_old->reference_number . '-V1';
        }

        $model = new CrmQuotations();
        $model->setAttributes($model_old->attributes);

        $model->parent_id = $id;
        $model->quotation_status = 0;
        $model->status = 0;
        $model->inquiry_received_time = $model_old->inquiry_received_time;
        $model->email_status_fee_diff = 0;

        

        if($quotation_old->id >= 3030){
            $model->type_of_service = $model_old->type_of_service;
        }
        else{
            $model->type_of_service = 1;
        }

        

        if ($model->save()) {

            $receivedProperties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();

            foreach ($receivedProperties as $key => $property) {
                //step 1
                $property_detail = CrmReceivedProperties::find()->where(['id' => $property->id])->one();
                unset($property_detail->id);
                $new_property_detail = new CrmReceivedProperties();
                $new_property_detail->setAttributes($property_detail->attributes);
                $new_property_detail->quotation_id = $model->id;
                $new_property_detail->property_index = $property->property_index;
                
                
                if($property_detail->quotation_id >= 3030){
                    $new_property_detail->multiscope_id = $property_detail->multiscope_id - $property_detail->quotation_id;
                }
                else{
                    $multiscope_id = '';
                    for ($i = 0; $i < 10; $i++) {
                        $multiscope_id .= rand(0, 9);
                    }
                    $new_property_detail->multiscope_id = $multiscope_id;
                    $new_property_detail->scope_of_service = $quotation_old->scope_of_service;
                    $new_property_detail->scope_of_service_saved = $quotation_old->scope_of_service;
                }

                // dd($quotation_old, $property_detail, $new_property_detail);

                if (!$new_property_detail->save()) {
                    echo "<pre>";
                    print_r($new_property_detail->errors);
                    die;

                }


                //step 2
                $CrmReceivedDocs = CrmReceivedDocs::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();
                $owners = \app\models\CrmQuotationOwner::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->asArray()->all();
                $received_docs = \app\models\CrmReceivedDocsFiles::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->asArray()->all();
                $step_2 = new CrmReceivedDocs();
                $step_2->setAttributes($CrmReceivedDocs->attributes);
                unset($step_2->id);
                $step_2->quotation_id = $model->id;
                $step_2->property_index = $property->property_index;
                $step_2->save();
                if (!$step_2->save()) {
                    echo "<pre>";
                    print_r($step_2->errors);
                    die;

                }


                // Save all owners terms
                foreach ($owners as $owner_data) {
                    $owner_data_detail = new \app\models\CrmQuotationOwner();
                    $owner_data_detail->name = $owner_data['name'];
                    $owner_data_detail->percentage = $owner_data['percentage'];
                    $owner_data_detail->index_id = $owner_data['index_id'];
                    $owner_data_detail->quotation_id = $model->id;
                    $owner_data_detail->property_index = $property->property_index;
                    // $owner_data_detail->save();
                    if (!$owner_data_detail->save()) {
                        echo "<pre>";
                        print_r($step_2->errors);
                        die;

                    }
                }

                // Save all Documents terms
                foreach ($received_docs as $received_doc) {
                    $received_data_detail = new \app\models\CrmReceivedDocsFiles();
                    $received_data_detail->document_id = $received_doc['document_id'];
                    $received_data_detail->attachment = $received_doc['attachment'];
                    $received_data_detail->quotation_id = $model->id;
                    $received_data_detail->property_index = $property->property_index;
                    $received_data_detail->save();
                }


                //Step 3
                $step_3_data = CrmValuationConflict::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();
                unset($step_3_data->id);
                $step_3 = new CrmValuationConflict();
                $step_3->setAttributes($step_3_data->attributes);
                $step_3->quotation_id = $model->id;
                $step_3->property_index = $property->property_index;
                if (!$step_3->save()) {
                    echo "<pre>";
                    print_r($step_3->errors);
                    die;

                }

                /* //step 4
                 $step_4_data = CrmScheduleInspection::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();
                 unset($step_4_data->id);
                 $step_4 = new CrmScheduleInspection();
                 $step_4->setAttributes($step_4_data->attributes);
                 $step_4->quotation_id = $model->id;
                 $step_4->property_index = $property->property_index;
                 if (!$step_4->save()) {
                     echo "<pre>";
                     print_r($step_4->errors);
                     die;

                 }

                 //step 5
                 $step_5_data = CrmInspectProperty::find()->where(['quotation_id' => $id, 'property_index' => $property->property_index])->one();

                 unset($step_5_data->id);
                 $step_5 = new CrmInspectProperty();
                 $step_5->setAttributes($step_5_data->attributes);
                 $step_5->quotation_id = $model->id;
                 $step_5->property_index = $property->property_index;
                 if (!$step_5->save()) {
                     echo "<pre>";
                     print_r($step_5->errors);
                     die;
                 }*/



            }
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
            return $this->redirect(['crm-quotations/step_8?id=' . $model->id]);
        } else {
             echo "<pre>";
             print_r($model->errors);
             die;
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $error) {
                    if (count($error) > 0) {
                        foreach ($error as $key => $val) {
                            Yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

        die('Process is in progress.');

    }



    /**
     * Displays a single CrmQuotations model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }



    /**
     * Updates an existing CrmQuotations model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CrmQuotations model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        // echo $id; die;
        $result = $this->findModel($id);
        $result->softDelete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CrmQuotations model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CrmQuotations the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CrmQuotations::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findModel1($id)
    {
        if (($model = Valuation::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionQpdf($id)
    {

        $model = CrmQuotations::findOne($id);
        $condition = true;
        $model->quotationPdf($condition);        
    }

    public function actionSendQuotation($id)
    {
        // echo "string";die();
        $model = CrmQuotations::find()->where(['id' => $id])->one();
        // print_r($model);die();
        if ($model != null) {
            $condition = false;
            $model->quotationPdf($condition);

            if ($model->quotation_status <= 0) {
                $date = date("Y-m-d H:i:s");
                $model->quotation_status = 1;
                $model->status_change_date = $date;
                $model->quotation_sent_date = $date;
            }

            if ($model->save()) {
                Yii::$app->crmQuotationHelperFunctions->addStatusHistory($model);
                yii::$app->getsession()->addFlash('success', yii::t('app', 'Quotation Send successfully'));
                return $this->redirect(['crm-quotations/step_0?id=' . $model->id]);

            } else {
                if ($model->hasErrors()) {
                    foreach ($model->getErrors() as $errors) {

                        foreach ($errors as $key => $val) {
                            //die($val);
                            yii::$app->getSession()->addFlash('error', $val);
                        }
                    }
                }
            }
        }

    }
    public function actionToe($id)
    {
        $model = CrmQuotations::findOne($id);
        $condition = true;      
        $model->toePdf($condition);
    }

    public function actionSendToe($id)
    {
        $model = CrmQuotations::find()->where(['id' => $id])->one(); //done
        $properties = yii::$app->quotationHelperFunctions->getMultiplePropertiesNew($id); //done

        //  $model->quotation_status =2;
        // if($model->quotation_status >0 && $model->quotation_status <= 2){
        $date = date("Y-m-d H:i:s");
        $model->quotation_status = 3;
        $model->toe_sent_date = $date;
        // }
        $condition = false;
        $model->toePdf($condition);
        if ($model->save()) {
            Yii::$app->crmQuotationHelperFunctions->addStatusHistory($model);

            yii::$app->getsession()->addFlash('success', yii::t('app', 'TOE send successfully'));
            return $this->redirect(['crm-quotations/step_0?id=' . $model->id]);
        } else {
            if ($model->hasErrors()) {
                foreach ($model->getErrors() as $errors) {

                    foreach ($errors as $key => $val) {
                        //die($val);
                        yii::$app->getSession()->addFlash('error', $val);
                    }
                }
            }
        }

    }
    public function actionQuotationPdf2($id = null, $condition)
    {
        $model = new CrmQuotations;

        $model->GetInvoice($id, $condition);


        // $quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');

        // require_once( __DIR__ .'/../components/tcpdf/ProformaInvoice.php');
        // // create new PDF document
        // $pdf = new \ProformaInvoice(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // // set document information
        // $pdf->SetCreator(PDF_CREATOR);
        // $pdf->SetAuthor('Windmills');
        // $pdf->SetTitle($model->reference_number);
        // $pdf->SetSubject('Quotation');
        // $pdf->SetKeywords($model->reference_number);

        // // set default header data
        // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // // set header and footer fonts
        // $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        // $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // // set default monospaced font
        // $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // // set margins
        // $pdf->SetMargins(10, 35, 10);
        // $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        // $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // // set auto page breaks
        // $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        // $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        // // set default font subsetting mode
        // $pdf->setFontSubsetting(true);

        // // Set font
        // // dejavusans is a UTF-8 Unicode font, if you only need to
        // // print standard ASCII chars, you can use core fonts like
        // // helvetica or times to reduce file size.
        // $pdf->SetFont('times', '', 14, '', true);

        // // Add a page
        // // This method has several options, check the source code documentation for more information.
        // $pdf->AddPage('P','A4');

        // $qpdf=Yii::$app->controller->renderPartial('/crm-quotations/qpdf',[
        //     'id'=>$id,
        //     'model'=>$model,
        //     'quotation_fee_total' => $quotation_fee_total,
        // ]);

        //     $pdf->writeHTML($qpdf, true, false, false, false, '');
        //     $pdf->Output('Quotation-'.$model->reference_number, 'I');

    }
    public function actionQuotationPdf3($id = null)
    {
        $model = $this->findModel($id);
        $quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');

        require_once(__DIR__ . '/../components/tcpdf/ProformaInvoice.php');
        // create new PDF document
        $pdf = new \ProformaInvoice(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle($model->reference_number);
        $pdf->SetSubject('Quotation');
        $pdf->SetKeywords($model->reference_number);

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(10, 35, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('times', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P', 'A4');

        $qpdf = Yii::$app->controller->renderPartial('/crm-quotations/qpdf', [
            'id' => $id,
            'model' => $model,
            'quotation_fee_total' => $quotation_fee_total,
        ]);

        $pdf->writeHTML($qpdf, true, false, false, false, '');
        $pdf->Output('Quotation-' . $model->reference_number, 'I');
        exit;

    }

    public function actionQuotationPdf4($id = null)
    {
        $model = $this->findModel($id);
        $quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');

        require_once(__DIR__ . '/../components/tcpdf/ProformaInvoice.php');
        // create new PDF document
        $pdf = new \ProformaInvoice(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle($model->reference_number);
        $pdf->SetSubject('Quotation');
        $pdf->SetKeywords($model->reference_number);

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(10, 35, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('times', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P', 'A4');

        $qpdf = Yii::$app->controller->renderPartial('/crm-quotations/qpdf_bank', [
            'id' => $id,
            'model' => $model,
            'quotation_fee_total' => $quotation_fee_total,
        ]);

        $pdf->writeHTML($qpdf, true, false, false, false, '');
        $pdf->Output('Quotation-' . $model->reference_number, 'I');
        exit;

    }

    public function actionQuotationPaymentReceipt($id = null)
    {
        $model = $this->findModel($id);
        $quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');

        require_once( __DIR__ .'/../components/tcpdf/QuotationPaymentReceipt.php');
        // create new PDF document
        $pdf = new \QuotationPaymentReceipt(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle($model->reference_number);
        $pdf->SetSubject('QuotationPaymentReceipt');
        $pdf->SetKeywords($model->reference_number);

        // set default header data
        // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(10, 35, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('times', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P','A4');

        if($model->type_of_service == 3){
            $qpdf=Yii::$app->controller->renderPartial('/crm-quotations/payment_receipt_bcs_pdf',[
                'id'=>$id,
                'model'=>$model,
                'quotation_fee_total' => $quotation_fee_total,
            ]);
        }else  if($model->type_of_service == 2){
            $qpdf=Yii::$app->controller->renderPartial('/crm-quotations/payment_receipt_pme_pdf',[
                'id'=>$id,
                'model'=>$model,
                'quotation_fee_total' => $quotation_fee_total,
            ]);
        }else {
            $qpdf=Yii::$app->controller->renderPartial('/crm-quotations/payment_receipt_pdf',[
                'id'=>$id,
                'model'=>$model,
                'quotation_fee_total' => $quotation_fee_total,
            ]);
        }

        // $qpdf=Yii::$app->controller->renderPartial('/crm-quotations/payment_receipt_pdf',[
        //     'id'=>$id,
        //     'model'=>$model,
        //     'quotation_fee_total' => $quotation_fee_total,
        // ]);

    

        // $paymentReceiptPdfFile = 'Payment-Receipt-' . $model->reference_number . '.pdf';

        // $fullPath = realpath(dirname(__FILE__) . '/../uploads/payment_receipts') . '/' . $paymentReceiptPdfFile;

        $pdf->writeHTML($qpdf, true, false, false, false, '');
        // $pdf->Output($fullPath, 'I');
        // return $fullPath;
        $pdf->Output('Payment-Receipt-'.$model->reference_number, 'I');
        exit;

    }

    public function actionSendQuotationPaymentReceiptHalf($id = null)
    {
        $model = $this->findModel($id);
        $quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');

        require_once( __DIR__ .'/../components/tcpdf/QuotationPaymentReceipt.php');
        // create new PDF document
        $pdf = new \QuotationPaymentReceipt(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle($model->reference_number);
        $pdf->SetSubject('QuotationPaymentReceipt');
        $pdf->SetKeywords($model->reference_number);

        // set default header data
        // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(10, 35, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('times', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P','A4');


        if($model->type_of_service == 3){
            $qpdf=Yii::$app->controller->renderPartial('/crm-quotations/payment_receipt_bcs_pdf',[
                'id'=>$id,
                'model'=>$model,
                'quotation_fee_total' => $quotation_fee_total,
            ]);
        }else  if($model->type_of_service == 2){
            $qpdf=Yii::$app->controller->renderPartial('/crm-quotations/payment_receipt_pme_pdf',[
                'id'=>$id,
                'model'=>$model,
                'quotation_fee_total' => $quotation_fee_total,
            ]);
        }else {
            $qpdf=Yii::$app->controller->renderPartial('/crm-quotations/payment_receipt_pdf',[
                'id'=>$id,
                'model'=>$model,
                'quotation_fee_total' => $quotation_fee_total,
            ]);
        }
        // $qpdf=Yii::$app->controller->renderPartial('/crm-quotations/payment_receipt_pdf',[
        //     'id'=>$id,
        //     'model'=>$model,
        //     'quotation_fee_total' => $quotation_fee_total,
        // ]);

        $paymentReceiptPdfFile = 'Payment-Receipt-' . $model->reference_number . '.pdf';

        $fullPath = realpath(dirname(__FILE__) . '/../uploads/payment_receipts') . '/' . $paymentReceiptPdfFile;

        $pdf->writeHTML($qpdf, true, false, false, false, '');
        $pdf->Output($fullPath, 'F');
        return $fullPath;
        // $pdf->Output('Payment-Receipt-'.$model->reference_number, 'I');
        // exit;

    }

    public function actionSendQuotationPaymentReceiptFull($id = null)
    {
        $model = $this->findModel($id);
        $quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');

        require_once( __DIR__ .'/../components/tcpdf/QuotationPaymentReceipt.php');
        // create new PDF document
        $pdf = new \QuotationPaymentReceipt(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Windmills');
        $pdf->SetTitle($model->reference_number);
        $pdf->SetSubject('QuotationPaymentReceipt');
        $pdf->SetKeywords($model->reference_number);

        // set default header data
        // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(10, 35, 10);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('times', '', 14, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage('P','A4');

        if($model->type_of_service == 3){
            $qpdf=Yii::$app->controller->renderPartial('/crm-quotations/payment_receipt_bcs_pdf',[
                'id'=>$id,
                'model'=>$model,
                'quotation_fee_total' => $quotation_fee_total,
            ]);
        }else  if($model->type_of_service == 2){
            $qpdf=Yii::$app->controller->renderPartial('/crm-quotations/payment_receipt_pme_pdf',[
                'id'=>$id,
                'model'=>$model,
                'quotation_fee_total' => $quotation_fee_total,
            ]);
        }else {
            $qpdf=Yii::$app->controller->renderPartial('/crm-quotations/payment_receipt_pdf',[
                'id'=>$id,
                'model'=>$model,
                'quotation_fee_total' => $quotation_fee_total,
            ]);
        }
        // $qpdf=Yii::$app->controller->renderPartial('/crm-quotations/payment_receipt_pdf',[
        //     'id'=>$id,
        //     'model'=>$model,
        //     'quotation_fee_total' => $quotation_fee_total,
        // ]);

        $paymentReceiptPdfFile = 'Payment-Receipt-' . $model->reference_number . '-Final.pdf';

        $fullPath = realpath(dirname(__FILE__) . '/../uploads/payment_receipts') . '/' . $paymentReceiptPdfFile;

        $pdf->writeHTML($qpdf, true, false, false, false, '');
        $pdf->Output($fullPath, 'F');
        return $fullPath;
        // $pdf->Output('Payment-Receipt-'.$model->reference_number, 'I');
        // exit;

    }

    public function actionReceivedQuotations()
    {
        $date = date('Y-m-d', strtotime('-5 days'));
        $quotations = \app\models\CrmQuotations::find()
            ->where(['quotation_status' => 0])
            ->Andwhere(['>', 'created_at', $date])
            ->asArray()
            ->all();
        echo "<pre>";
        print_r($quotations);
        echo "</pre>";
        die();
    }


    public function actionOnholdQuotations()
    {
        // echo "http://windmills-1.local/crm-quotations/step_0?id=361"; echo "<br>";


        $date = date('Y-m-d', strtotime('-10 days'));
        $quotations = \app\models\CrmQuotations::find()
            ->where(['quotation_status' => 10])
            ->Andwhere(['<=', 'created_at', $date])
            ->asArray()
            ->all();

        $dateTwo = date('Y-m-d', strtotime('-12 days'));
        $quotationsTwo = \app\models\CrmQuotations::find()
            ->where(['quotation_status' => 10])
            ->Andwhere(['<=', 'created_at', $dateTwo])
            ->asArray()
            ->all();

        // echo $dateTwo;
        // echo "<pre>"; print_r($quotationsTwo); echo "</pre>"; die();

        //send email to business team
        $html = '';
        $i = 1;
        foreach ($quotations as $key => $quotation) {
            $html .= $i . ') ' . Url::base() . "/crm-quotations/step_8?id=" . $quotation['id'] . "<br>";
            $i++;
        }

        $notifyData1 = [
            'replacements' => [
                '{days}' => 10,
                '{status}' => 'On Hold',
                '{html}' => $html,
            ],
        ];

        $event1 = 'send.QuotationsToBusinessTeam';
        $this->SendFire($notifyData1, $event1);


        //send email to Bilal Moti
        $html2 = '';
        $j = 1;
        foreach ($quotationsTwo as $key => $quotation) {
            $html2 .= $j . ') ' . Url::base() . "/crm-quotations/step_8?id=" . $quotation['id'] . "<br>";
            $j++;
        }

        $notifyData2 = [
            'replacements' => [
                '{days}' => 12,
                '{status}' => 'On Hold',
                '{html}' => $html2,
            ],
        ];
        $event2 = 'send.QuotationsToCEO';
        $this->SendFire($notifyData2, $event2);


        // die('end');
    }





    public function actionGetActiveQuotations()
    {
        $quotations = \app\models\CrmQuotations::find()
            ->where(['quotation_status' => 0])
            ->where(['<=', 'created_at', date('Y-m-d', strtotime('-5 days'))])
            ->asArray()
            ->all();
        $html1 = '';
        $i = 1;
        foreach ($quotations as $quotation) {
            $html .= $i . ') ' . Url::base() . "/crm-quotations/step_8?id=" . $quotation['id'] . "<br>";
            $i++;
        }

        // echo "<pre>"; print_r($html); echo "</pre>"; die();

        $notifyData1 = [
            'replacements' => [
                '{days}' => 5,
                '{status}' => 'Active',
                '{html}' => $html
            ],
        ];
        $event1 = 'send.QuotationsToBusinessTeam';
        $this->SendFire($notifyData1, $event1);

        $quotations2 = \app\models\CrmQuotations::find()
            ->where(['quotation_status' => 0])
            ->where(['<=', 'created_at', date('Y-m-d', strtotime('-7 days'))])
            ->asArray()
            ->all();

        $html1 = '';
        $j = 1;
        foreach ($quotations2 as $quotation) {
            $html .= $j . ') ' . Url::base() . "/crm-quotations/step_8?id=" . $quotation['id'] . "<br>";
            $j++;
        }

        $notifyData2 = [
            'replacements' => [
                '{days}' => 7,
                '{status}' => 'Active',
                '{html}' => $html
            ],
        ];
        $event2 = 'send.QuotationsToCEO';
        $this->SendFire($notifyData2, $event2);
    }





    public function SendFire($notifyData = '', $keyword = '')
    {
        // NotifyEvent::fire2($keyword, $notifyData);
    }



    public function actionCopyProperty($id, $property_index)
    {
        // echo "Quotation_id:- ".$id."<br>Property Index:- ".$property_index; die();
        $check = CrmQuotations::findOne($id);
        $countProperties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->count();
        if ($countProperties < $check->no_of_properties) {

            $index = '';
            for ($i = 0; $i < $check->no_of_properties; $i++) {
                // echo $i; echo "<br>"; //die;
                $indexCheck = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => $i])->one();
                // echo "<pre>"; print_r($indexCheck); echo "</pre>"; die;
                if ($indexCheck == null) {
                    $index = $i;
                    break;
                }
            }

            // $indexes  = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => CrmReceivedProperties::find()->max('id')])->one();
            // $index = $indexes->property_index+1;
            // echo $index; die;

            //saveing step_1
            $property = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => $property_index])->one();
            $modelCrmReceivedProperties = new CrmReceivedProperties;
            $modelCrmReceivedProperties->no_of_owners = $property->no_of_owners;
            $modelCrmReceivedProperties->service_officer_name = $property->service_officer_name;
            $modelCrmReceivedProperties->instruction_date = $property->instruction_date;
            $modelCrmReceivedProperties->target_date = $property->target_date;
            $modelCrmReceivedProperties->building_info = $property->building_info;
            $modelCrmReceivedProperties->property_id = $property->property_id;
            $modelCrmReceivedProperties->client_name_passport = $property->client_name_passport;
            $modelCrmReceivedProperties->property_category = $property->property_category;
            $modelCrmReceivedProperties->community = $property->community;
            $modelCrmReceivedProperties->sub_community = $property->sub_community;
            $modelCrmReceivedProperties->tenure = $property->tenure;
            // $modelCrmReceivedProperties->unit_number = $property->unit_number;   
            $modelCrmReceivedProperties->city = $property->city;
            $modelCrmReceivedProperties->payment_plan = $property->payment_plan;
            $modelCrmReceivedProperties->building_number = $property->building_number;
            $modelCrmReceivedProperties->plot_number = $property->plot_number;
            $modelCrmReceivedProperties->street = $property->street;
            $modelCrmReceivedProperties->floor_number = $property->floor_number;
            $modelCrmReceivedProperties->instruction_person = $property->instruction_person;
            // $modelCrmReceivedProperties->land_size = $property->land_size;
            $modelCrmReceivedProperties->purpose_of_valuation = $property->purpose_of_valuation;
            $modelCrmReceivedProperties->valuation_scope = $property->valuation_scope;
            $modelCrmReceivedProperties->special_assumption = $property->special_assumption;
            $modelCrmReceivedProperties->approval_status = $property->approval_status;
            $modelCrmReceivedProperties->quotation_id = $id;
            $modelCrmReceivedProperties->property_index = $index;
            $modelCrmReceivedProperties->complexity = $property->complexity;
            $modelCrmReceivedProperties->type_of_valuation = $property->type_of_valuation;
            $modelCrmReceivedProperties->number_of_comparables = $property->number_of_comparables;
            $modelCrmReceivedProperties->no_of_units = $property->no_of_units;
            $modelCrmReceivedProperties->repeat_valuation = $property->repeat_valuation;
            $modelCrmReceivedProperties->recommended_fee = $property->recommended_fee;
            $modelCrmReceivedProperties->quotation_fee = $property->quotation_fee;
            $modelCrmReceivedProperties->tat = $property->tat;
            $modelCrmReceivedProperties->toe_fee = $property->toe_fee;
            $modelCrmReceivedProperties->toe_tat = $property->toe_tat;
            $modelCrmReceivedProperties->valuation_approach = $property->valuation_approach;
            $modelCrmReceivedProperties->converted_fee = $property->converted_fee;
            $modelCrmReceivedProperties->inspection_type = $property->inspection_type;
            $modelCrmReceivedProperties->general_asumption = $property->general_asumption;
            $modelCrmReceivedProperties->property_general_asumption = $property->property_general_asumption;
            $modelCrmReceivedProperties->property_special_asumption = $property->property_special_asumption;
            $modelCrmReceivedProperties->upgrades = $property->upgrades;
            // $modelCrmReceivedProperties->built_up_area = $property->built_up_area;
            $modelCrmReceivedProperties->last_3_years_finance = $property->last_3_years_finance;
            $modelCrmReceivedProperties->projections_10_years = $property->projections_10_years;
            $modelCrmReceivedProperties->no_of_unit_types = $property->no_of_unit_types;
            $modelCrmReceivedProperties->number_of_rooms_building = $property->number_of_rooms_building;
            $modelCrmReceivedProperties->restaurant = $property->restaurant;
            $modelCrmReceivedProperties->ballrooms = $property->ballrooms;
            $modelCrmReceivedProperties->atms = $property->atms;
            $modelCrmReceivedProperties->retails_units = $property->retails_units;
            $modelCrmReceivedProperties->night_clubs = $property->night_clubs;
            $modelCrmReceivedProperties->bars = $property->bars;
            $modelCrmReceivedProperties->health_club = $property->health_club;
            $modelCrmReceivedProperties->meeting_rooms = $property->meeting_rooms;
            $modelCrmReceivedProperties->spa = $property->spa;
            $modelCrmReceivedProperties->beach_access = $property->beach_access;
            $modelCrmReceivedProperties->parking_sale = $property->parking_sale;
            $modelCrmReceivedProperties->parking_floors = $property->parking_floors;
            $modelCrmReceivedProperties->parking_space = $property->parking_space;
            $modelCrmReceivedProperties->basement_floors = $property->basement_floors;
            $modelCrmReceivedProperties->mezzanine_floors = $property->mezzanine_floors;
            $modelCrmReceivedProperties->typical_floors = $property->typical_floors;
            $modelCrmReceivedProperties->swimming_pools = $property->swimming_pools;
            $modelCrmReceivedProperties->no_of_jacuzzi = $property->no_of_jacuzzi;
            // $modelCrmReceivedProperties->net_leasable_area = $property->net_leasable_area;
            $modelCrmReceivedProperties->furnished = $property->furnished;
            $modelCrmReceivedProperties->ready = $property->ready;
            $modelCrmReceivedProperties->no_of_buildings = $property->no_of_buildings;
            $modelCrmReceivedProperties->no_of_residential_units = $property->no_of_residential_units;
            $modelCrmReceivedProperties->no_of_commercial_units = $property->no_of_commercial_units;
            $modelCrmReceivedProperties->no_of_coffee_shops = $property->no_of_coffee_shops;
            $modelCrmReceivedProperties->no_of_sign_boards = $property->no_of_sign_boards;
            $modelCrmReceivedProperties->no_of_bbq_area = $property->no_of_bbq_area;
            $modelCrmReceivedProperties->no_of_play_area = $property->no_of_play_area;
            $modelCrmReceivedProperties->no_of_gyms = $property->no_of_gyms;
            $modelCrmReceivedProperties->no_of_schools = $property->no_of_schools;
            $modelCrmReceivedProperties->no_of_clinics = $property->no_of_clinics;
            $modelCrmReceivedProperties->no_of_sports_courts = $property->no_of_sports_courts;
            $modelCrmReceivedProperties->no_of_mosques = $property->no_of_mosques;
            $modelCrmReceivedProperties->no_of_health_club_spa = $property->no_of_health_club_spa;
            $modelCrmReceivedProperties->reference_fee_3rdparty = $property->reference_fee_3rdparty;
            $modelCrmReceivedProperties->reference_fee_staff = $property->reference_fee_staff;
            $modelCrmReceivedProperties->aprvd_tat = $property->aprvd_tat;
            $modelCrmReceivedProperties->civil_drawing_available = $property->civil_drawing_available;
            $modelCrmReceivedProperties->mechanical_drawing_available = $property->mechanical_drawing_available;
            $modelCrmReceivedProperties->electrical_drawing_available = $property->electrical_drawing_available;
            $modelCrmReceivedProperties->plumbing_drawing_available = $property->plumbing_drawing_available;
            $modelCrmReceivedProperties->hvac_drawing_available = $property->hvac_drawing_available;
            $modelCrmReceivedProperties->drawings_available = $property->drawings_available;
            $modelCrmReceivedProperties->no_of_units_value = $property->no_of_units_value;
            $modelCrmReceivedProperties->no_of_res_units_value = $property->no_of_res_units_value;
            $modelCrmReceivedProperties->no_of_com_units_value = $property->no_of_com_units_value;
            $modelCrmReceivedProperties->no_of_ret_units_value = $property->no_of_ret_units_value;
            $modelCrmReceivedProperties->no_of_comparables_value = $property->no_of_comparables_value;
            $modelCrmReceivedProperties->bo_recommended_fee = $property->bo_recommended_fee;
            $modelCrmReceivedProperties->bo_recommended_tat = $property->bo_recommended_tat;
            $modelCrmReceivedProperties->reviewed_fee = $property->reviewed_fee;
            $modelCrmReceivedProperties->reviewed_tat = $property->reviewed_tat;
            $modelCrmReceivedProperties->scope_of_service = $property->scope_of_service;
            $modelCrmReceivedProperties->asset_category = $property->asset_category;
            $modelCrmReceivedProperties->no_of_asset = $property->no_of_asset;
            $modelCrmReceivedProperties->asset_complexity = $property->asset_complexity;
            $modelCrmReceivedProperties->location = $property->location;
            $modelCrmReceivedProperties->no_of_location = $property->no_of_location;
            $modelCrmReceivedProperties->working_days = $property->working_days;
            $modelCrmReceivedProperties->asset_age = $property->asset_age;
            $modelCrmReceivedProperties->country_of_manufacturing = $property->country_of_manufacturing;
            $modelCrmReceivedProperties->scope_of_service_saved = $property->scope_of_service_saved;
            // $modelCrmReceivedProperties->multiscope_id = $property->multiscope_id;
            
            if (!$modelCrmReceivedProperties->save()) {
                if ($modelCrmReceivedProperties->hasErrors()) {
                    foreach ($modelCrmReceivedProperties->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }

            //saveing step_2
            $propertyReceivedDocs = CrmReceivedDocs::find()->where(['quotation_id' => $id, 'property_index' => $property_index])->one();
            $modelCrmReceivedDocs = new CrmReceivedDocs;
            $modelCrmReceivedDocs->quotation_id = $id;
            $modelCrmReceivedDocs->property_index = $index;
            if ($modelCrmReceivedDocs->save()) {
                $owners_data = CrmQuotationOwner::find()->where(['quotation_id' => $id, 'property_index' => $property_index])->all();
                if ($owners_data <> null) {
                    foreach ($owners_data as $owner_data) {
                        // echo "<pre>"; print_r($owner_data); echo "</pre>"; die;
                        $owner_data_detail = new CrmQuotationOwner();
                        $owner_data_detail->name = $owner_data['name'];
                        $owner_data_detail->percentage = $owner_data['percentage'];
                        $owner_data_detail->index_id = $owner_data['index_id'];
                        $owner_data_detail->quotation_id = $id;
                        $owner_data_detail->property_index = $index;
                        if (!$owner_data_detail->save()) {
                            if ($owner_data_detail->hasErrors()) {
                                foreach ($owner_data_detail->getErrors() as $error) {
                                    if (count($error) > 0) {
                                        foreach ($error as $key => $val) {
                                            Yii::$app->getSession()->addFlash('error', $val);
                                        }
                                    }
                                }
                            }
                        }
                        ;
                    }
                }

                $documents = CrmReceivedDocsFiles::find()->where(['quotation_id' => $id, 'property_index' => $property_index])->all();
                if ($documents <> null) {
                    foreach ($documents as $received_doc) {
                        $owner_data_detail = new CrmReceivedDocsFiles();
                        $owner_data_detail->document_id = $received_doc['document_id'];
                        $owner_data_detail->attachment = $received_doc['attachment'];
                        // $owner_data_detail->doc_insert_date = $received_doc['doc_insert_date'];
                        $owner_data_detail->quotation_id = $id;
                        $owner_data_detail->property_index = $index;
                        if (!$owner_data_detail->save()) {
                            if ($owner_data_detail->hasErrors()) {
                                foreach ($owner_data_detail->getErrors() as $error) {
                                    if (count($error) > 0) {
                                        foreach ($error as $key => $val) {
                                            Yii::$app->getSession()->addFlash('error', $val);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            } else {
                if ($modelCrmReceivedDocs->hasErrors()) {
                    foreach ($modelCrmReceivedDocs->getErrors() as $error) {
                        if (count($error) > 0) {
                            foreach ($error as $key => $val) {
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }

            //saveing step_3
            $conflict = CrmValuationConflict::find()->where(['quotation_id' => $id, 'property_index' => $property_index])->one();
            // echo "<pre>"; print_r($conflict); echo "</pre>"; die();
            if ($conflict <> null) {
                $modelConflict = new CrmValuationConflict();
                $modelConflict->quotation_id = $id;
                $modelConflict->property_index = $index;
                $modelConflict->related_to_buyer = $conflict->related_to_buyer;
                $modelConflict->related_to_seller = $conflict->related_to_seller;
                $modelConflict->related_to_client = $conflict->related_to_client;
                $modelConflict->related_to_property = $conflict->related_to_property;
                $modelConflict->related_to_buyer_reason = $conflict->related_to_buyer_reason;
                $modelConflict->related_to_seller_reason = $conflict->related_to_seller_reason;
                $modelConflict->related_to_client_reason = $conflict->related_to_client_reason;
                $modelConflict->related_to_property_reason = $conflict->related_to_property_reason;
                if (!$modelConflict->save()) {
                    if ($modelConflict->hasErrors()) {
                        foreach ($modelConflict->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                    }
                }
            }
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Property Duplicate Successfully'));
            return $this->redirect(['crm-quotations/step_1?id=' . $id .'&property_index=' . $index]);

        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('app', 'All Properties Are Reserved'));
            return $this->redirect(['crm-quotations/step_0?id=' . $id . '&&showAlert=1']);
        }



    }


    public function actionTrashProperty($id, $property_index)
    {
        CrmReceivedProperties::deleteAll(['quotation_id' => $id, 'property_index' => $property_index]);
        CrmQuotationOwner::deleteAll(['quotation_id' => $id, 'property_index' => $property_index]);
        CrmReceivedDocs::deleteAll(['quotation_id' => $id, 'property_index' => $property_index]);
        CrmReceivedDocsFiles::deleteAll(['quotation_id' => $id, 'property_index' => $property_index]);
        CrmValuationConflict::deleteAll(['quotation_id' => $id, 'property_index' => $property_index]);

        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Property Deleted Successfully'));
        return $this->redirect(['crm-quotations/step_0?id=' . $id]);
    }




    public function actionStatusSummary($id = null)
    {
        $logs = \app\models\QuotationStatusHistory::find()->where(['quotation_id' => $id])->orderBy(['date' => SORT_DESC,])->asArray()->all();
        // echo "<pre>"; print_r($logs); echo "</pre>"; die();
        return $this->render('logs', [
            'logs' => $logs,
        ]);
    }


    public function actionTrashedIndex()
    {
        $searchModel = new \app\models\CrmTrashedQuotationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('trashed-index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionTrashIndex()
    {
        $searchModel = new \app\models\CrmTrashedQuotationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataprovider->pagination = false;


        return $this->render('trashed-index-own', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionPermentlyDelete($id)
    {
        // echo $id; die;

        $model = $this->findModel($id);
        if ($model->delete()) {
            \app\models\CrmInspectProperty::deleteAll(['quotation_id' => $id]);
            \app\models\CrmQuotationConfigraions::deleteAll(['quotation_id' => $id]);
            \app\models\CrmQuotationConfigraionsFiles::deleteAll(['quotation_id' => $id]);
            \app\models\CrmQuotationOwner::deleteAll(['quotation_id' => $id]);
            \app\models\CrmReceivedDocs::deleteAll(['quotation_id' => $id]);
            \app\models\CrmReceivedDocsFiles::deleteAll(['quotation_id' => $id]);
            \app\models\CrmReceivedProperties::deleteAll(['quotation_id' => $id]);
            \app\models\CrmScheduleInspection::deleteAll(['quotation_id' => $id]);
            \app\models\CrmValuationConflict::deleteAll(['quotation_id' => $id]);
        }
        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Quotations Permently Delete successfully'));
        return $this->redirect(['trashed-index']);
    }

    public function actionMoveToActive($id)
    {
        $model = $this->findModel($id);
        Yii::$app->db->createCommand()
            ->update('crm_quotations', ['trashed' => null], ['id' => $model->id])
            ->execute();
        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Quotations Active successfully'));
        return $this->redirect(['trashed-index']);
    }


    public function actionDeleteAll()
    {
        $idz = ArrayHelper::map(\app\models\CrmQuotations::find()
            ->where(['trashed' => 1])
            ->select(['id'])
            ->all(), 'id', 'id');
        // echo "<pre>"; print_r($idz); echo "</pre>"; die();
        CrmQuotations::deleteAll(['in', 'id', $idz]);

        \app\models\CrmInspectProperty::deleteAll(['quotation_id' => $idz]);
        \app\models\CrmQuotationConfigraions::deleteAll(['quotation_id' => $idz]);
        \app\models\CrmQuotationConfigraionsFiles::deleteAll(['quotation_id' => $idz]);
        \app\models\CrmQuotationOwner::deleteAll(['quotation_id' => $idz]);
        \app\models\CrmReceivedDocs::deleteAll(['quotation_id' => $idz]);
        \app\models\CrmReceivedDocsFiles::deleteAll(['quotation_id' => $idz]);
        \app\models\CrmReceivedProperties::deleteAll(['quotation_id' => $idz]);
        \app\models\CrmScheduleInspection::deleteAll(['quotation_id' => $idz]);
        \app\models\CrmValuationConflict::deleteAll(['quotation_id' => $idz]);

        // echo "<pre>"; print_r($idz); echo "</pre>"; die();
        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'All Quotations Permently Deleted Form Trash'));
        return $this->redirect(['trashed-index']);
    }


    public function actionActiveAll()
    {
        Yii::$app->db->createCommand()
            ->update('crm_quotations', ['trashed' => null], ['trashed' => 1])
            ->execute();
        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'All quotations Active successfully'));
        return $this->redirect(['trashed-index']);
    }

    public function actionCheckVerification()
    {
        $post = Yii::$app->request->post();
        if ($post['quotation_id'] <> null) {
            $countProperties = CrmReceivedProperties::find()->where(['quotation_id' => $post['quotation_id']])->count();
            $countVerified = CrmReceivedProperties::find()->where(['quotation_id' => $post['quotation_id'], 'status_verified' => 1])->count();
            if ($countProperties == $countVerified) {
                $data = [
                    'msg' => 'allowAction',
                ];
                return json_encode($data);
            } else {
                $data = [
                    'msg' => 'actionNotAllowed',
                ];
                return json_encode($data);
            }
        }
    }

    public function actionQuotation_tax_invoice($id)
    {

        $model = CrmQuotations::find()->where(['id' => $id])->one();

        if ($model->quotation_status > 3 && $model->quotation_status <= 6) {
            if ($model->toe_signed_and_received == 5) {

                $branch_address = \app\models\Branch::find()->where(['zone_list' => 3510])->one();
                $properties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();
                $netQuotationFee = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('quotation_fee');
                $netToeFee = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_fee');
                $countProperties = count($properties);

                $relative_discount_toe = $total_discount = $total_discount_amount = 0;
                $first_time_discount = $discount_no_of_properties = $general_discount = 0;

                if ($countProperties > 5) {
                    $result = ProposalMasterFile::find()->where(['heading' => 'No Of Property Discount', 'sub_heading' => 6])->one();
                    $model->no_of_property_discount = $result['values'];
                } else if ($countProperties > 0 && $countProperties < 6) {
                    $result = ProposalMasterFile::find()->where(['heading' => 'No Of Property Discount', 'sub_heading' => $countProperties])->one();
                    $model->no_of_property_discount = $result['values'];
                }

                $clientFirstTimeDiscount = Valuation::find()->where(['client_id' => $model->client_name])->one();
                if ($clientFirstTimeDiscount == null) {
                    $first_time_fee = ProposalMasterFile::find()->where(['heading' => 'First Time Discount', 'sub_heading' => 'first-time-discount'])->one();
                    $model->first_time_discount = $first_time_fee['values'];
                }

                $generalDicsountFee = ProposalMasterFile::find()->where(['heading' => 'General Discount', 'sub_heading' => 'general-discount'])->one();
                if ($generalDicsountFee <> null) {
                    $model->general_discount = $generalDicsountFee['values'];
                }


                if ($model->relative_discount_toe > 0) {
                    $relative_discount_toe = yii::$app->quotationHelperFunctions->getDiscountRupee($netToeFee, $model->relative_discount_toe);
                    $total_discount = $total_discount + $model->relative_discount_toe;
                    $total_discount_amount = $total_discount_amount + $relative_discount_toe;
                }
                $netAfterDiscountFee = $netToeFee - $relative_discount_toe;

                if ($model->first_time_discount != null) {
                    $first_time_discount = yii::$app->quotationHelperFunctions->getDiscountRupeeNoOfProperties($netToeFee, $model->first_time_discount);
                    $netAfterDiscountFee = $netAfterDiscountFee - $first_time_discount;
                    $total_discount = $total_discount + $model->first_time_discount;
                    $total_discount_amount = $total_discount_amount + $first_time_discount;
                }

                if ($model->no_of_property_discount != null) {
                    $discount_no_of_properties = yii::$app->quotationHelperFunctions->getDiscountRupeeNoOfProperties($netToeFee, $model->no_of_property_discount);
                    $netAfterDiscountFee = $netAfterDiscountFee - $discount_no_of_properties;
                    $total_discount = $total_discount + $model->no_of_property_discount;
                    $total_discount_amount = $total_discount_amount + $discount_no_of_properties;
                }

                if ($model->general_discount != null) {
                    $general_discount = yii::$app->quotationHelperFunctions->getDiscountRupeeNoOfProperties($netToeFee, $model->general_discount);
                    $netAfterDiscountFee = $netAfterDiscountFee - $general_discount;
                    $total_discount = $total_discount + $model->general_discount;
                    $total_discount_amount = $total_discount_amount + $general_discount;
                }

                if ($model->client->vat == 1) {
                    $vat = yii::$app->quotationHelperFunctions->getVatTotal($netAfterDiscountFee);
                }
                $finalFeePayable = $netAfterDiscountFee + $vat;

                $fee_to_words = yii::$app->quotationHelperFunctions->numberTowords($finalFeePayable);


                if ($model->advance_payment_terms <> null and $model->advance_payment_terms > 0) {
                    $advance_payment_terms = $model->advance_payment_terms;
                } else {
                    $advance_payment_terms = '0%';
                }

                $fee = [
                    'netQuotationFee' => $netQuotationFee,
                    'netToeFee' => $netToeFee,
                    'relative_discount_toe' => $relative_discount_toe,
                    'first_time_discount' => $first_time_discount,
                    'discount_no_of_properties' => $discount_no_of_properties,
                    'general_discount' => $general_discount,
                    'total_discount' => $total_discount,
                    'total_discount_amount' => $total_discount_amount,
                    'netAfterDiscountFee' => $netAfterDiscountFee,
                    'finalFeePayable' => $finalFeePayable,
                    'fee_to_words' => $fee_to_words,
                    'vat' => $vat,
                ];


                require_once(__DIR__ . '/../components/tcpdf/TaxInvoice.php');
                // create new PDF document
                $pdf = new \TaxInvoice(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

                // set document information
                $pdf->SetCreator(PDF_CREATOR);
                $pdf->SetAuthor('Windmills');
                $pdf->SetTitle($model->reference_number);
                $pdf->SetSubject('Tax Invoice');
                $pdf->SetKeywords($model->reference_number);

                // set default header data
                $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

                // set header and footer fonts
                $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
                $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

                // set default monospaced font
                $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

                // set margins
                $pdf->SetMargins(10, 35, 10);
                $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
                $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

                // set auto page breaks
                $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                $pdf->Write(0, 'Example of HTML Justification', '', 0, 'L', true, 0, false, false, 0);

                // set default font subsetting mode
                $pdf->setFontSubsetting(true);
                $pdf->invoice_text = 'Tax Invoice';
                // Set font
                // dejavusans is a UTF-8 Unicode font, if you only need to
                // print standard ASCII chars, you can use core fonts like
                // helvetica or times to reduce file size.
                $pdf->SetFont('times', '', 14, '', true);

                // Add a page
                // This method has several options, check the source code documentation for more information.
                $pdf->AddPage('P', 'A4');

                $qpdf = Yii::$app->controller->renderPartial('quotation_tax_invoice', [
                    'model' => $model,
                    'branch_address' => $branch_address,
                    'properties' => $properties,
                    'fee' => $fee,
                    'advance_payment_terms' => $advance_payment_terms,
                ]);

                $pdf->writeHTML($qpdf, true, false, false, false, '');
                $pdf->Output('Valuation-' . $model->reference_number, 'I');
                exit;

            }

        } else {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }

    }


    public function actionStep_12($id)
    {
        //new code
        $this->checkLogin();
        $model = $this->findModel($id);
        $quotation = $this->findModel($id);

        $properties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();
        $total_amount = 0;
        $final_amount = 0;
        $amount = [];

        foreach ($properties as $key => $property_data) {

            $inspect_property = CrmInspectProperty::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
            $schecdule_inspections = CrmScheduleInspection::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
            $property = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
            // dd($property);
            //Fee criteria
            
            $fee_parameters = array();
            $fee_parameters['building_title'] = $property->building->title;
            $fee_parameters['clientType'] = $quotation->client->client_type;
            $fee_parameters['paymentTerms'] = $quotation->advance_payment_terms;
            $fee_parameters['property'] = $property->property_id;
            $fee_parameters['community'] = $property->building->community;
            $fee_parameters['sub_community'] = $property->building->sub_community;
            $fee_parameters['city'] = $property->building->city;
            $fee_parameters['tenure'] = $property->tenure;
            if($quotation->scope_of_service != 6 && $quotation->scope_of_service != 7 && $quotation->scope_of_service != 8  ){
                $fee_parameters['complexity'] = str_replace('-', '_', $property->complexity);
            }
            $fee_parameters['repeat_valuation'] = $property->repeat_valuation;
            if ($quotation->id > 508) {
                $fee_parameters['built_up_area'] = $property->built_up_area;
            } else {
                $fee_parameters['built_up_area'] = $inspect_property->built_up_area;
            }
            $fee_parameters['net_leasable_area'] = $property->net_leasable_area;
            $fee_parameters['type_of_valuation'] = $property->inspection_type;
            $fee_parameters['number_of_comparables'] = $property->number_of_comparables;
            $fee_parameters['no_of_units'] = $property->no_of_units;
            $fee_parameters['land_size'] = ($property->land_size <> null) ? $property->land_size : 0;
            $fee_parameters['upgrades'] = ($property->upgrades <> null) ? $property->upgrades : 0;
            $fee_parameters['other_intended_users'] = ($quotation->other_intended_users_check_1 == "Yes") ? 'yes' : 'no';
            $fee_parameters['approach_type'] = Yii::$app->appHelperFunctions->valuationApproachListArr[$property->valuation_approach];
            $fee_parameters['number_of_types'] = $property->no_of_unit_types;
            $fee_parameters['typical_floors'] = $property->typical_floors;
            $fee_parameters['basement_floors'] = $property->basement_floors;
            $fee_parameters['mezzanine_floors'] = $property->mezzanine_floors;
            $fee_parameters['parking_floors'] = $property->parking_floors;

            $fee_parameters['no_of_rooms'] = $property->number_of_rooms_building;
            $fee_parameters['last_three_years_finance'] = $property->last_3_years_finance;
            $fee_parameters['ten_years_projections'] = $property->projections_10_years;
            $fee_parameters['restaurant'] = $property->restaurant;
            $fee_parameters['ballrooms'] = $property->ballrooms;
            $fee_parameters['atms'] = $property->atms;
            $fee_parameters['retails_units'] = $property->retails_units;
            $fee_parameters['night_clubs'] = $property->night_clubs;
            $fee_parameters['bars'] = $property->bars;
            $fee_parameters['health_club'] = ($property->health_club <> null) ? 'yes' : 'no';
            $fee_parameters['meeting_rooms'] = $property->meeting_rooms;
            $fee_parameters['spa'] = ($property->spa <> null) ? 'yes' : 'no';
            $fee_parameters['beach_access'] = ($property->beach_access <> null) ? 'yes' : 'no';
            $fee_parameters['parking_sale'] = $property->parking_sale;
            $fee_parameters['civil_drawing_available'] = $property->civil_drawing_available;
            $fee_parameters['mechanical_drawing_available'] = $property->mechanical_drawing_available;
            $fee_parameters['electrical_drawing_available'] = $property->electrical_drawing_available;
            $fee_parameters['plumbing_drawing_available'] = $property->plumbing_drawing_available;
            $fee_parameters['hvac_drawing_available'] = $property->hvac_drawing_available;
            $fee_parameters['no_of_units_value'] = $property->no_of_units_value;
            $fee_parameters['no_of_res_units_value'] = $property->no_of_res_units_value;
            $fee_parameters['no_of_com_units_value'] = $property->no_of_com_units_value;
            $fee_parameters['no_of_ret_units_value'] = $property->no_of_ret_units_value;
            $fee_parameters['no_of_comparables_value'] = $property->no_of_comparables_value;
            



            // $ApprovedArr = [2, 4];
            $ApprovedArr = [];
            if (!in_array($model->quotation_status, $ApprovedArr)) {
                if ($model->id > 1203) {
                    if ($property->valuation_approach == 2) {
                        // echo "here2";
                        $amount[] = yii::$app->propertySummaryHelper->getAmount_autoIncome($fee_parameters, $model);
                    } else if ($property->valuation_approach == 3) {
                        // echo "here3"; die;
                        $amount[] = yii::$app->propertySummaryHelper->getAmount_autoProfit($fee_parameters, $model);
                    } else if($quotation->scope_of_service == 6) {
                        // for rfs
                        $amount[] = yii::$app->propertySummaryHelper->getAmount_autoRFS($fee_parameters, $model);                    
                    } else if($quotation->scope_of_service == 7) { 
                        // for scs
                        $amount[] = yii::$app->propertySummaryHelper->getAmount_autoSCS($fee_parameters, $model);
                    } else if($quotation->scope_of_service == 8) {
                        // for bca
                        $amount[] = yii::$app->propertySummaryHelper->getAmount_autoBCA($fee_parameters, $model);
                    }                  
                    else {
                        // echo "here else";
                        $amount[] = yii::$app->propertySummaryHelper->getAmount_auto($fee_parameters, $model);
                    }
                } else {
                    // dd("im here"); 
                    $amount[] = yii::$app->propertySummaryHelper->getAmount_new($fee_parameters, $model);
                    //    dd($amount);
                }
                $amount[$key]['recommended_fee']['fee'] = $property->recommended_fee;
                $amount[$key]['quotation_fee']['fee'] = $property->toe_fee;
                // dd($amount);
            }

        }


        $net_recommended_fee = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('recommended_fee');
        $net_quotation_fee = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_fee');

        $total_discount = 0;
        $total_discount_num = 0;
        $aprvd_total_discount = 0;
        $aprvd_total_discount_num = 0;

        //no_of_property_discount
        $no_of_property_discount = [];
        if ($model->no_of_property_discount > 0 && (count($properties) > 0)) {
            $no_of_property_discount['value'] = $model->no_of_property_discount;
            $no_of_property_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->no_of_property_discount, $net_quotation_fee);
            $total_discount = $total_discount + $no_of_property_discount['amount'];
            $total_discount_num = $total_discount_num + $model->no_of_property_discount;
        } else {
            $no_of_property_discount = yii::$app->quotationHelperFunctions->getNumberOfPropertiesDiscount(count($properties), $net_quotation_fee);
            $no_of_property_discount['value'] = $no_of_property_discount['value'];
            $no_of_property_discount['amount'] = $no_of_property_discount['amount'];
            $total_discount = $total_discount + $no_of_property_discount['amount'];
            $total_discount_num = $total_discount_num + $no_of_property_discount['value'];
        }
        // for approved no of property discount
        $aprvd_no_of_property_discount = [];
        if ($model->aprvd_no_of_property_discount > 0 && (count($properties) > 0)) {
            $aprvd_no_of_property_discount['value'] = $model->aprvd_no_of_property_discount;
            $aprvd_no_of_property_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->aprvd_no_of_property_discount, $net_quotation_fee);
            $aprvd_total_discount = $aprvd_total_discount + $aprvd_no_of_property_discount['amount'];
            $aprvd_total_discount_num = $aprvd_total_discount_num + $model->aprvd_no_of_property_discount;
        }

        //same_building_discount
        $same_building_discount = [];
        if ($model->same_building_discount > 0 && ($model->no_of_units_same_building > 0)) {
            if ($model->no_of_units_same_building > 1) {
                $same_building_discount['value'] = $model->same_building_discount;
                $same_building_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->same_building_discount, $net_quotation_fee);
                $total_discount = $total_discount + $same_building_discount['amount'];
                $total_discount_num = $total_discount_num + $model->same_building_discount;
            }
        } else {
            if ($model->no_of_units_same_building > 0) {
                $same_building_discount = yii::$app->quotationHelperFunctions->getSameBuildingDiscount($model->no_of_units_same_building, $net_quotation_fee);
                $same_building_discount['value'] = $same_building_discount['value'];
                $same_building_discount['amount'] = $same_building_discount['amount'];
                $total_discount = $total_discount + $same_building_discount['amount'];
                $total_discount_num = $total_discount_num + $model->same_building_discount;
            } else {
                $model->same_building_discount = 0;
            }
        }
        // for approved same building discount
        $aprvd_same_building_discount = [];
        if ($model->aprvd_same_building_discount > 0 && ($model->aprvd_same_building_discount > 0)) {
            if ($model->aprvd_same_building_discount > 1) {
                $aprvd_same_building_discount['value'] = $model->aprvd_same_building_discount;
                $aprvd_same_building_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->aprvd_same_building_discount, $net_quotation_fee);
                $aprvd_total_discount = $aprvd_total_discount + $aprvd_same_building_discount['amount'];
                $aprvd_total_discount_num = $aprvd_total_discount_num + $model->aprvd_same_building_discount;
            }
        }


        //first_time_discount
        $first_time_fee_discount = [];
        $clinet_first_time_check = Valuation::find()->where(['client_id' => $model->client_name])->one();
        if ($clinet_first_time_check == null) {
            if ($model->first_time_discount > 0) {
                $first_time_fee_discount['value'] = $model->first_time_discount;
                $first_time_fee_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->first_time_discount, $net_quotation_fee);
                $total_discount = $total_discount + $first_time_fee_discount['amount'];
                $total_discount_num = $total_discount_num + $model->first_time_discount;
            } else {
                $first_time_fee_discount = yii::$app->quotationHelperFunctions->getFirstTimeDiscount($net_quotation_fee);
                $first_time_fee_discount['value'] = $first_time_fee_discount['value'];
                $first_time_fee_discount['amount'] = $first_time_fee_discount['amount'];
                $total_discount = $total_discount + $first_time_fee_discount['amount'];
                $total_discount_num = $total_discount_num + $first_time_fee_discount['value'];
            }
        } else {
            $model->first_time_discount = 0;
        }

        // for approved first time discount
        $aprvd_first_time_fee_discount = [];
        $clinet_first_time_check = Valuation::find()->where(['client_id' => $model->client_name])->one();
        if ($clinet_first_time_check == null) {
            if ($model->aprvd_first_time_discount > 0) {
                $aprvd_first_time_discount['value'] = $model->aprvd_first_time_discount;
                $aprvd_first_time_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->aprvd_first_time_discount, $net_quotation_fee);
                $aprvd_total_discount = $aprvd_total_discount + $aprvd_first_time_discount['amount'];
                $aprvd_total_discount_num = $aprvd_total_discount_num + $model->aprvd_first_time_discount;
            } else {
                $aprvd_first_time_discount = yii::$app->quotationHelperFunctions->getFirstTimeDiscount($net_quotation_fee);
                $aprvd_first_time_discount['value'] = $aprvd_first_time_discount['value'];
                $aprvd_first_time_discount['amount'] = $aprvd_first_time_discount['amount'];
                $aprvd_total_discount = $aprvd_total_discount + $aprvd_first_time_discount['amount'];
                $aprvd_total_discount_num = $aprvd_total_discount_num + $aprvd_first_time_discount['value'];
            }
        } else {
            $model->aprvd_first_time_discount = 0;
        }



        //check relative discount
        $relative_discount_toe = [];
        if ($model->relative_discount_toe != null && $model->relative_discount_toe > 0) {
            $relative_discount_toe['value'] = $model->relative_discount;
            $relative_discount_toe['amount'] = yii::$app->quotationHelperFunctions->getDiscountRupee($net_quotation_fee, $model->relative_discount_toe);
            $total_discount = $total_discount + $relative_discount_toe['amount'];
            $total_discount_num = $total_discount_num + $model->relative_discount_toe;

            // added for approved total relative discount
            $aprvd_total_discount = $aprvd_total_discount + $relative_discount_toe['amount'];
            $aprvd_total_discount_num = $aprvd_total_discount_num + $model->relative_discount_toe;
        }

        // Reference fee
        $ref_fee_staff = $net_recommended_fee * $quotation->reference_fee_staff / 100;
        $ref_fee_3rdparty = ($quotation->reference_fee_3rdparty_check == 0) ? $net_recommended_fee * $quotation->reference_fee_3rdparty / 100 : $quotation->reference_fee_3rdparty;

        $netValuationFee = $net_quotation_fee - $total_discount;
        $discount_net_fee = $netValuationFee;

        if($aprvd_total_discount > 0){
            $aprvd_netValuationFee = $net_quotation_fee - $aprvd_total_discount;
        }
        else {
            $aprvd_netValuationFee = $net_quotation_fee - $total_discount;
        }
        // $aprvd_netValuationFee = $net_quotation_fee - $aprvd_total_discount;
        $aprvd_discount_net_fee = $aprvd_netValuationFee;



        //urgencyfee check
        if ($model->tat_requirements > 0) {
            $urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $netValuationFee);
            $urgencyfee_amount = $urgencyfee['amount'];
            $netValuationFee = $netValuationFee + $urgencyfee['amount'];


            $aprvd_urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $aprvd_netValuationFee);
            $aprvd_urgencyfee_amount = $aprvd_urgencyfee['amount'];
            $aprvd_netValuationFee = $aprvd_netValuationFee + $aprvd_urgencyfee['amount'];


        }

        //Advance Payment Terms
        if ($quotation->client->client_type != 'bank') {
            $advance_payment_terms_data = explode('%', $model->advance_payment_terms);
            $advance_payment_terms = yii::$app->quotationHelperFunctions->getAdvancePaymentfee($advance_payment_terms_data[0], $net_quotation_fee);
        } else {
            $advance_payment_terms['value'] = 0;
            $advance_payment_terms['amount'] = 0;
        }




        $netValuationFee = $netValuationFee + $advance_payment_terms_data['amount'];
        $aprvd_netValuationFee = $aprvd_netValuationFee + $advance_payment_terms_data['amount'];


        $VAT = 0;
        if ($model->client->vat == 1) {
            $VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
            $aprvd_VAT = yii::$app->quotationHelperFunctions->getVatTotal($aprvd_netValuationFee);

        }



        $finalFeePayable = $netValuationFee + $VAT;
        $aprvd_finalFeePayable = $aprvd_netValuationFee + $aprvd_VAT;




        return $this->render('steps/_step12', [
            'model' => $model,
            'quotation' => $quotation,
            'amount' => $amount,
            'net_recommended_fee' => $net_recommended_fee,
            'net_quotation_fee' => $net_quotation_fee,
            'VAT' => $VAT,
            'aprvd_VAT' => $aprvd_VAT,
            'no_of_property_discount' => $no_of_property_discount,
            'same_building_discount' => $same_building_discount,
            'first_time_fee_discount' => $first_time_fee_discount,
            'relative_discount_toe' => $relative_discount_toe,
            'total_discount' => $total_discount,
            'total_discount_num' => $total_discount_num,
            'discount_net_fee' => $discount_net_fee,
            'netValuationFee' => $netValuationFee,
            'urgencyfee_amount' => $urgencyfee_amount,
            'finalFeePayable' => $finalFeePayable,
            'aprvd_finalFeePayable' => $aprvd_finalFeePayable,
            'refFeeStaff' => $ref_fee_staff,
            'refFee3rdParty' => $ref_fee_3rdparty,
            'aprvd_discount_net_fee' => $aprvd_discount_net_fee,
            'aprvd_urgencyfee_amount' => $aprvd_urgencyfee_amount,
            'aprvd_total_discount' => $aprvd_total_discount,
            'aprvd_total_discount_num' => $aprvd_total_discount_num,
            'aprvd_no_of_property_discount' => $aprvd_no_of_property_discount,
            'aprvd_same_building_discount' => $aprvd_same_building_discount,
            'aprvd_first_time_fee_discount' => $aprvd_first_time_fee_discount,
        ]);

    }


    public function actionStep_13($id)
    {
        //new code
        $this->checkLogin();
        $model = $this->findModel($id);
        $quotation = $this->findModel($id);
        $old_qid_30012024 = 2284;

        $properties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();
        $total_amount = 0;
        $final_amount = 0;
        $amount = [];

        

        foreach ($properties as $key => $property_data) {

            $inspect_property = CrmInspectProperty::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
            $schecdule_inspections = CrmScheduleInspection::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
            $property = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => $key])->one();
            // dd($property);
            //Fee criteria

            // assign fee master file property type based on property id    
            $property_type = yii::$app->quotationHelperFunctions->getPropertyTypeApproach($property->property_id, $property->valuation_approach);
            
            $fee_parameters = array();
            $fee_parameters['building_title'] = $property->building->title;
            $fee_parameters['clientType'] = $quotation->client->client_type;
            $fee_parameters['paymentTerms'] = $quotation->advance_payment_terms;
            $fee_parameters['property'] = $property->property_id;
            $fee_parameters['property_type'] = $property_type;
            $fee_parameters['property_category'] = $property->property_category;
            $fee_parameters['community'] = $property->building->community;
            $fee_parameters['sub_community'] = $property->building->sub_community;
            $fee_parameters['city'] = $property->building->city;
            $fee_parameters['tenure'] = $property->tenure;
            if($quotation->scope_of_service != 6 && $quotation->scope_of_service != 7 && $quotation->scope_of_service != 8  ){
                $fee_parameters['complexity'] = str_replace('-', '_', $property->complexity);
            }
            $fee_parameters['repeat_valuation'] = $property->repeat_valuation;
            if ($quotation->id > 508) {
                $fee_parameters['built_up_area'] = $property->built_up_area;
            } else {
                $fee_parameters['built_up_area'] = $inspect_property->built_up_area;
            }
            $fee_parameters['net_leasable_area'] = $property->net_leasable_area;
            $fee_parameters['type_of_valuation'] = $property->inspection_type;
            $fee_parameters['number_of_comparables'] = $property->number_of_comparables;
            $fee_parameters['no_of_units'] = $property->no_of_units;
            $fee_parameters['no_of_residential_units'] = $property->no_of_residential_units;
            $fee_parameters['no_of_commercial_units'] = $property->no_of_commercial_units;
            $fee_parameters['no_of_retail_units'] = $property->retails_units;
            $fee_parameters['land_size'] = ($property->land_size <> null) ? $property->land_size : 0;
            $fee_parameters['upgrades'] = ($property->upgrades <> null) ? $property->upgrades : 0;
            $fee_parameters['other_intended_users'] = ($quotation->other_intended_users_check_1 == "Yes") ? 'yes' : 'no';
            $fee_parameters['approach_type'] = Yii::$app->appHelperFunctions->valuationApproachListArr[$property->valuation_approach];
            $fee_parameters['number_of_types'] = $property->no_of_unit_types;
            $fee_parameters['typical_floors'] = $property->typical_floors;
            $fee_parameters['basement_floors'] = $property->basement_floors;
            $fee_parameters['mezzanine_floors'] = $property->mezzanine_floors;
            $fee_parameters['parking_floors'] = $property->parking_floors;
            $fee_parameters['no_of_rooms'] = $property->number_of_rooms_building;
            $fee_parameters['last_three_years_finance'] = $property->last_3_years_finance;
            $fee_parameters['ten_years_projections'] = $property->projections_10_years;
            $fee_parameters['restaurant'] = $property->restaurant;
            $fee_parameters['ballrooms'] = $property->ballrooms;
            $fee_parameters['atms'] = $property->atms;
            $fee_parameters['retails_units'] = $property->retails_units;
            $fee_parameters['night_clubs'] = $property->night_clubs;
            $fee_parameters['bars'] = $property->bars;
            $fee_parameters['health_club'] = ($property->health_club <> null) ? 'yes' : 'no';
            $fee_parameters['meeting_rooms'] = $property->meeting_rooms;
            $fee_parameters['spa'] = ($property->spa <> null) ? 'yes' : 'no';
            $fee_parameters['beach_access'] = ($property->beach_access <> null) ? 'yes' : 'no';
            $fee_parameters['parking_sale'] = $property->parking_sale;
            $fee_parameters['civil_drawing_available'] = $property->civil_drawing_available;
            $fee_parameters['mechanical_drawing_available'] = $property->mechanical_drawing_available;
            $fee_parameters['electrical_drawing_available'] = $property->electrical_drawing_available;
            $fee_parameters['plumbing_drawing_available'] = $property->plumbing_drawing_available;
            $fee_parameters['hvac_drawing_available'] = $property->hvac_drawing_available;
            $fee_parameters['no_of_units_value'] = $property->no_of_units_value;
            $fee_parameters['no_of_res_units_value'] = $property->no_of_res_units_value;
            $fee_parameters['no_of_com_units_value'] = $property->no_of_com_units_value;
            $fee_parameters['no_of_ret_units_value'] = $property->no_of_ret_units_value;
            $fee_parameters['no_of_comparables_value'] = $property->no_of_comparables_value;



            // $ApprovedArr = [2, 4];
            $ApprovedArr = []; 
            if (!in_array($model->quotation_status, $ApprovedArr)) {
                if ($model->id > 1203) {
                    if ($property->valuation_approach == 2) {
                        // echo "here2";
                        $amount[] = yii::$app->propertySummaryHelper->getAmount_autoIncome($fee_parameters, $model);
                    } else if ($property->valuation_approach == 3) {
                        // echo "here3"; die;
                        $amount[] = yii::$app->propertySummaryHelper->getAmount_autoProfit($fee_parameters, $model);
                    } else if($quotation->scope_of_service == 6) {
                        // for rfs
                        $amount[] = yii::$app->propertySummaryHelper->getAmount_autoRFS($fee_parameters, $model);                    
                    } else if($quotation->scope_of_service == 7) { 
                        // for scs
                        $amount[] = yii::$app->propertySummaryHelper->getAmount_autoSCS($fee_parameters, $model);
                    } 
                    else if($quotation->scope_of_service == 8) {
                        // for bca
                        $amount[] = yii::$app->propertySummaryHelper->getAmount_autoBCA($fee_parameters, $model);
                    }                  
                    else {
                        // echo "here else";
                        $amount[] = yii::$app->propertySummaryHelper->getAmount_auto($fee_parameters, $model);
                    }
                } else {
                    // dd("im here"); 
                    $amount[] = yii::$app->propertySummaryHelper->getAmount_new($fee_parameters, $model);
                    //    dd($amount);
                }
                $amount[$key]['recommended_fee']['fee'] = $property->recommended_fee;
                // $amount[$key]['bo_recommended_fee']['fee'] = $property->bo_recommended_fee;
                // $amount[$key]['reviewed_fee']['fee'] = $property->reviewed_fee;
                $amount[$key]['quotation_fee']['fee'] = $property->toe_fee;
                // dd($amount);
            }

        }


        $net_recommended_fee = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('recommended_fee');
        $net_quotation_fee = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('toe_fee');
        $bo_recommended_fee = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('bo_recommended_fee');
        $reviewed_fee = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('reviewed_fee');

        

        $total_discount = 0;
        $total_discount_num = 0;
        $aprvd_total_discount = 0;
        $aprvd_total_discount_num = 0;

        //no_of_property_discount
        $no_of_property_discount = [];
        if ($model->no_of_property_discount > 0 && (count($properties) > 0)) {
            $no_of_property_discount['value'] = $model->no_of_property_discount;
            $no_of_property_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->no_of_property_discount, $net_quotation_fee);
            $total_discount = $total_discount + $no_of_property_discount['amount'];
            $total_discount_num = $total_discount_num + $model->no_of_property_discount;
        } else {
            $no_of_property_discount = yii::$app->quotationHelperFunctions->getNumberOfPropertiesDiscount(count($properties), $net_quotation_fee, $model);
            $no_of_property_discount['value'] = $no_of_property_discount['value'];
            $no_of_property_discount['amount'] = $no_of_property_discount['amount'];
            $total_discount = $total_discount + $no_of_property_discount['amount'];
            $total_discount_num = $total_discount_num + $no_of_property_discount['value'];
        }
        // for approved no of property discount
        $aprvd_no_of_property_discount = [];
        if ($model->aprvd_no_of_property_discount > 0 && (count($properties) > 0)) {
            $aprvd_no_of_property_discount['value'] = $model->aprvd_no_of_property_discount;
            $aprvd_no_of_property_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->aprvd_no_of_property_discount, $net_quotation_fee);
            $aprvd_total_discount = $aprvd_total_discount + $aprvd_no_of_property_discount['amount'];
            $aprvd_total_discount_num = $aprvd_total_discount_num + $model->aprvd_no_of_property_discount;
        }

        //same_building_discount
        $same_building_discount = [];
        if ($model->same_building_discount > 0 && ($model->no_of_units_same_building > 0)) {
            if ($model->no_of_units_same_building > 1) {
                $same_building_discount['value'] = $model->same_building_discount;
                $same_building_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->same_building_discount, $net_quotation_fee);
                $total_discount = $total_discount + $same_building_discount['amount'];
                $total_discount_num = $total_discount_num + $model->same_building_discount;
            }
        } else {
            if ($model->no_of_units_same_building > 0) {
                $same_building_discount = yii::$app->quotationHelperFunctions->getSameBuildingDiscount($model->no_of_units_same_building, $net_quotation_fee, $model);
                $same_building_discount['value'] = $same_building_discount['value'];
                $same_building_discount['amount'] = $same_building_discount['amount'];
                $total_discount = $total_discount + $same_building_discount['amount'];
                $total_discount_num = $total_discount_num + $model->same_building_discount;
            } else {
                $model->same_building_discount = 0;
            }
        }
        // for approved same building discount
        $aprvd_same_building_discount = [];
        if ($model->aprvd_same_building_discount > 0 && ($model->aprvd_same_building_discount > 0)) {
            if ($model->aprvd_same_building_discount > 1) {
                $aprvd_same_building_discount['value'] = $model->aprvd_same_building_discount;
                $aprvd_same_building_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->aprvd_same_building_discount, $net_quotation_fee);
                $aprvd_total_discount = $aprvd_total_discount + $aprvd_same_building_discount['amount'];
                $aprvd_total_discount_num = $aprvd_total_discount_num + $model->aprvd_same_building_discount;
            }
        }


        //first_time_discount
        $first_time_fee_discount = [];
        $clinet_first_time_check = Valuation::find()->where(['client_id' => $model->client_name])->one();
        if ($clinet_first_time_check == null) {
            if ($model->first_time_discount > 0) {
                $first_time_fee_discount['value'] = $model->first_time_discount;
                $first_time_fee_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->first_time_discount, $net_quotation_fee);
                $total_discount = $total_discount + $first_time_fee_discount['amount'];
                $total_discount_num = $total_discount_num + $model->first_time_discount;
            } else {
                $first_time_fee_discount = yii::$app->quotationHelperFunctions->getFirstTimeDiscount($net_quotation_fee, $model);
                $first_time_fee_discount['value'] = $first_time_fee_discount['value'];
                $first_time_fee_discount['amount'] = $first_time_fee_discount['amount'];
                $total_discount = $total_discount + $first_time_fee_discount['amount'];
                $total_discount_num = $total_discount_num + $first_time_fee_discount['value'];
            }
        } else {
            $model->first_time_discount = 0;
        }

        // for approved first time discount
        $aprvd_first_time_fee_discount = [];
        $clinet_first_time_check = Valuation::find()->where(['client_id' => $model->client_name])->one();
        if ($clinet_first_time_check == null) {
            if ($model->aprvd_first_time_discount > 0) {
                $aprvd_first_time_discount['value'] = $model->aprvd_first_time_discount;
                $aprvd_first_time_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->aprvd_first_time_discount, $net_quotation_fee);
                $aprvd_total_discount = $aprvd_total_discount + $aprvd_first_time_discount['amount'];
                $aprvd_total_discount_num = $aprvd_total_discount_num + $model->aprvd_first_time_discount;
            } else {
                $aprvd_first_time_discount = yii::$app->quotationHelperFunctions->getFirstTimeDiscount($net_quotation_fee);
                $aprvd_first_time_discount['value'] = $aprvd_first_time_discount['value'];
                $aprvd_first_time_discount['amount'] = $aprvd_first_time_discount['amount'];
                $aprvd_total_discount = $aprvd_total_discount + $aprvd_first_time_discount['amount'];
                $aprvd_total_discount_num = $aprvd_total_discount_num + $aprvd_first_time_discount['value'];
            }
        } else {
            $model->aprvd_first_time_discount = 0;
        }



        //check relative discount
        $relative_discount_toe = [];
        if ($model->relative_discount_toe != null && $model->relative_discount_toe > 0) {
            $relative_discount_toe['value'] = $model->relative_discount;
            $relative_discount_toe['amount'] = yii::$app->quotationHelperFunctions->getDiscountRupee($net_quotation_fee, $model->relative_discount_toe);
            $total_discount = $total_discount + $relative_discount_toe['amount'];
            $total_discount_num = $total_discount_num + $model->relative_discount_toe;

            // added for approved total relative discount
            $aprvd_total_discount = $aprvd_total_discount + $relative_discount_toe['amount'];
            $aprvd_total_discount_num = $aprvd_total_discount_num + $model->relative_discount_toe;
        }

        // Reference fee
        $ref_fee_staff = $net_recommended_fee * $quotation->reference_fee_staff / 100;
        $ref_fee_3rdparty = ($quotation->reference_fee_3rdparty_check == 0) ? $net_recommended_fee * $quotation->reference_fee_3rdparty / 100 : $quotation->reference_fee_3rdparty;

        $reference_fee_staff_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('reference_fee_staff');
        $reference_fee_3rdparty_total = CrmReceivedProperties::find()->where(['quotation_id' => $id])->sum('reference_fee_3rdparty');
        $reference_fee_total = $reference_fee_staff_total + $reference_fee_3rdparty_total;

        
        

        $netValuationFee = $net_quotation_fee - $total_discount;
        $discount_net_fee = $netValuationFee;

        if($aprvd_total_discount > 0){
            $aprvd_netValuationFee = $net_quotation_fee - $aprvd_total_discount;
        }
        else {
            $aprvd_netValuationFee = $net_quotation_fee - $total_discount;
        }
        // $aprvd_netValuationFee = $net_quotation_fee - $aprvd_total_discount;
        $aprvd_discount_net_fee = $aprvd_netValuationFee;



        //urgencyfee check
        if ($model->tat_requirements > 0) {
            $urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $netValuationFee, $property_type, $model);
            $urgencyfee_amount = $urgencyfee['amount'];
            $netValuationFee = $netValuationFee + $urgencyfee['amount'];


            $aprvd_urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $aprvd_netValuationFee, $property_type, $model);
            $aprvd_urgencyfee_amount = $aprvd_urgencyfee['amount'];
            $aprvd_netValuationFee = $aprvd_netValuationFee + $aprvd_urgencyfee['amount'];


        }

        //Advance Payment Terms
        if ($quotation->client->client_type != 'bank') {
            $advance_payment_terms_data = explode('%', $model->advance_payment_terms);
            $advance_payment_terms = yii::$app->quotationHelperFunctions->getAdvancePaymentfee($advance_payment_terms_data[0], $net_quotation_fee, $model);
        } else {
            $advance_payment_terms['value'] = 0;
            $advance_payment_terms['amount'] = 0;
        }




        $netValuationFee = $netValuationFee + $advance_payment_terms_data['amount'];
        $aprvd_netValuationFee = $aprvd_netValuationFee + $advance_payment_terms_data['amount'];


        $VAT = 0;
        if ($model->client->vat == 1) {
            $VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
            $aprvd_VAT = yii::$app->quotationHelperFunctions->getVatTotal($aprvd_netValuationFee);

        }



        $finalFeePayable = $netValuationFee + $VAT;
        $aprvd_finalFeePayable = $aprvd_netValuationFee + $aprvd_VAT;




        return $this->render('steps/_step13', [
            'model' => $model,
            'quotation' => $quotation,
            'amount' => $amount,
            'net_recommended_fee' => $net_recommended_fee,
            'bo_recommended_fee' => $bo_recommended_fee,
            'reviewed_fee' => $reviewed_fee,
            'net_quotation_fee' => $net_quotation_fee,
            'VAT' => $VAT,
            'aprvd_VAT' => $aprvd_VAT,
            'no_of_property_discount' => $no_of_property_discount,
            'same_building_discount' => $same_building_discount,
            'first_time_fee_discount' => $first_time_fee_discount,
            'relative_discount_toe' => $relative_discount_toe,
            'total_discount' => $total_discount,
            'total_discount_num' => $total_discount_num,
            'discount_net_fee' => $discount_net_fee,
            'netValuationFee' => $netValuationFee,
            'urgencyfee_amount' => $urgencyfee_amount,
            'finalFeePayable' => $finalFeePayable,
            'aprvd_finalFeePayable' => $aprvd_finalFeePayable,
            'refFeeStaff' => $ref_fee_staff,
            'refFee3rdParty' => $ref_fee_3rdparty,
            'reference_fee_staff_total' => $reference_fee_staff_total,
            'reference_fee_3rdparty_total' => $reference_fee_3rdparty_total,
            'aprvd_discount_net_fee' => $aprvd_discount_net_fee,
            'aprvd_urgencyfee_amount' => $aprvd_urgencyfee_amount,
            'aprvd_total_discount' => $aprvd_total_discount,
            'aprvd_total_discount_num' => $aprvd_total_discount_num,
            'aprvd_no_of_property_discount' => $aprvd_no_of_property_discount,
            'aprvd_same_building_discount' => $aprvd_same_building_discount,
            'aprvd_first_time_fee_discount' => $aprvd_first_time_fee_discount,
            'old_qid_30012024' => $old_qid_30012024,
        ]);

    }   

    public function actionReceiveCrmEmail($id, $step)
    {
        $model = CrmQuotations::find()->where(['id' => $id])->one();
        $notifyData = [
            'client' => $model->client,
            'attachments' => [],
            'replacements' => [
                '{clientName}' => $model->client->title,
            ],
        ];
        //  \app\modules\wisnotify\listners\NotifyEvent::fire2('quotation.received', $notifyData);
        Yii::$app->db->createCommand()->update('crm_quotations', ['email_status_rcv' => 1], 'id=' . $id . '')->execute();
        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
        return $this->redirect(['valuation/step_' . $step . '/' . $model->id]);

    }
    public function actionReceiveDocEmail($id, $step)
    {
        $model = ReceivedDocs::find()->where(['id' => $id])->one();
        $valuation = Valuation::find()->where(['id' => $model->valuation_id])->one();
        $notifyData = [
            'client' => $valuation->client,
            'attachments' => [],
            'replacements' => [
                '{clientName}' => $valuation->client->title,
            ],
        ];
        // $valuation_steps=[ 21=>'summary', 22=>'review',23=>'approval',1=>'receive'];
        //\app\modules\wisnotify\listners\NotifyEvent::fire1('Received.Doc', $notifyData);
        // UPDATE (table name, column values, condition)
        // Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 7], 'id='.$model->valuation_id.'')->execute();
        Yii::$app->db->createCommand()->update('received_docs', ['email_status' => 1], 'id=' . $model->id . '')->execute();

        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Information saved successfully'));
        return $this->redirect(['valuation/step_' . $step . '/' . $valuation->id]);

    }


    public function actionDashboardIndex()
    {
        // dd(Yii::$app->request->queryParams);
        $this->checkLogin();
        $searchModel = new CrmQuotationsSearch();
        $dataProvider = $searchModel->dashboard_search(Yii::$app->request->queryParams);

        return $this->render('dashboard-widgets/' . $searchModel->widget_view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
        
    }




    public function actionDashboardAheadOntimeDelay()
    {
        $today_date = date("Y-m-d");
        $q_tbl = CrmQuotations::tableName();
        $widget_type = Yii::$app->request->queryParams['CrmQuotationsSearch']['widget_type'];
        $status_type = Yii::$app->request->queryParams['CrmQuotationsSearch']['status_type'];

        if ($status_type == 'ahead_of_time') {
            if ($widget_type == 'today') {
                $view = 'today-ahead-of-time';
                $page_title = 'Today Ahead of Time';
            } elseif ($widget_type == 'all') {
                $view = 'all-ahead-of-time';
                $page_title = 'All Ahead of Time';
            }
        } elseif ($status_type == 'on_time') {
            if ($widget_type == 'today') {
                $view = 'today-on-time';
                $page_title = 'Today on Time';
            } elseif ($widget_type == 'all') {
                $view = 'all-on-time';
                $page_title = 'All on Time';
            }
        } elseif ($status_type == 'delay') {
            if ($widget_type == 'today') {
                $view = 'today-delay';
                $page_title = 'Today Delay';
            } elseif ($widget_type == 'all') {
                $view = 'all-delay';
                $page_title = 'All Delay';
            }
        }

        $query = CrmQuotations::find()->where([$q_tbl . '.trashed' => null]);
        if ($widget_type == 'today') {
            $query->andFilterWhere(['like', $q_tbl . '.quotation_sent_date', $today_date]);
            $query->andFilterWhere(['like', $q_tbl . '.toe_sent_date', $today_date]);
        } elseif ($widget_type == 'all') {
            $query->andWhere(['not', [$q_tbl . '.quotation_sent_date' => null]]);
            $query->andWhere(['not', [$q_tbl . '.toe_sent_date' => null]]);
        }

        $models = $query->all();

        return $this->render('dashboard-widgets/' . $view, [
            'models' => $models,
            'page_title' => $page_title,
            'status_type' => $status_type,
        ]);
    }


    //if payment received then updated all possible columns mentioned below
    public function actionPrStatus()
    {
        $models = CrmQuotations::find()
            ->where(['quotation_status' => 6])
            ->andWhere(['crm_quotations.payment_received_date' => null])
            ->all();
        dd(count($models));

        foreach ($models as $key => $model) {
            $date_to_update = ($model->updated_at <> null) ? $model->updated_at : $model->status_change_date;
            Yii::$app->db->createCommand()
                ->update('crm_quotations', [
                    'status_change_date' => $date_to_update,
                    'quotation_sent_date' => $date_to_update,
                    'toe_sent_date' => $date_to_update,
                    'payment_received_date' => $date_to_update,
                    'toe_signed_and_received_date' => $date_to_update,
                    'toe_signed_and_received' => 5,
                    'approved_date' => $date_to_update,
                ], 'id=' . $model->id . '')
                ->execute();

        }

    }

    // quotation_sent_date
    public function actionQsStatus()
    {
        $models = CrmQuotations::find()
            ->where(['quotation_status' => 1])
            ->andWhere(['crm_quotations.quotation_sent_date' => null])
            // ->andWhere(['not', ['quotation_sent_date' => null]])
            ->all();
        dd(count($models));

        foreach ($models as $key => $model) {
            $date_to_update = ($model->updated_at <> null) ? $model->updated_at : $model->status_change_date;
            Yii::$app->db->createCommand()
                ->update('crm_quotations', [
                    'status_change_date' => $date_to_update,
                    'quotation_sent_date' => $date_to_update,
                ], 'id=' . $model->id . '')
                ->execute();

        }

    }

    // quotation_approved
    public function actionQaStatus()
    {
        $models = CrmQuotations::find()
            ->where(['quotation_status' => 2])
            ->andWhere(['crm_quotations.approved_date' => null])
            ->all();
        dd(count($models));

        foreach ($models as $key => $model) {
            $date_to_update = ($model->updated_at <> null) ? $model->updated_at : $model->status_change_date;
            Yii::$app->db->createCommand()
                ->update('crm_quotations', [
                    'status_change_date' => $date_to_update,
                    'quotation_sent_date' => $date_to_update,
                    'approved_date' => $date_to_update,
                ], 'id=' . $model->id . '')
                ->execute();

        }

    }

    // toe_sent
    public function actionTsStatus()
    {
        $models = CrmQuotations::find()
            ->where(['quotation_status' => 3])
            ->andWhere(['crm_quotations.toe_sent_date' => null])
            ->all();
        dd(count($models));
        foreach ($models as $key => $model) {
            $date_to_update = ($model->updated_at <> null) ? $model->updated_at : $model->status_change_date;
            Yii::$app->db->createCommand()
                ->update('crm_quotations', [
                    'status_change_date' => $date_to_update,
                    'toe_sent_date' => $date_to_update,
                ], 'id=' . $model->id . '')
                ->execute();

        }

    }

    // toe_approved
    public function actionTaStatus()
    {
        $models = CrmQuotations::find()
            ->where(['quotation_status' => 4])
            ->andWhere(['crm_quotations.toe_sent_date' => null])
            ->all();
        dd(count($models));
        foreach ($models as $key => $model) {
            $date_to_update = ($model->updated_at <> null) ? $model->updated_at : $model->status_change_date;
            Yii::$app->db->createCommand()
                ->update('crm_quotations', [
                    'status_change_date' => $date_to_update,
                    'toe_sent_date' => $date_to_update,
                ], 'id=' . $model->id . '')
                ->execute();

        }

    }

    // toe_signed_and_received
    public function actionTsignedStatus()
    {
        $models = CrmQuotations::find()
            ->where(['quotation_status' => 5])
            // ->andWhere(['crm_quotations.toe_signed_and_received' => null])
            ->all();
        dd(count($models));
        foreach ($models as $key => $model) {
            $date_to_update = ($model->updated_at <> null) ? $model->updated_at : $model->status_change_date;
            Yii::$app->db->createCommand()
                ->update('crm_quotations', [
                    'status_change_date' => $date_to_update,
                    'toe_signed_and_received' => 5,
                    'toe_signed_and_received_date' => $date_to_update,
                ], 'id=' . $model->id . '')
                ->execute();

        }

    }

    // Quotation rejected
    public function actionQrStatus()
    {
        $models = CrmQuotations::find()
            ->where(['quotation_status' => 7])
            ->andWhere(['crm_quotations.quotation_rejected_date' => null])
            ->all();
        dd(count($models));
        foreach ($models as $key => $model) {
            $date_to_update = ($model->updated_at <> null) ? $model->updated_at : $model->status_change_date;
            Yii::$app->db->createCommand()
                ->update('crm_quotations', [
                    'status_change_date' => $date_to_update,
                    'quotation_rejected_date' => $date_to_update,
                ], 'id=' . $model->id . '')
                ->execute();

        }

    }

    // Toe rejected
    public function actionToerejectedStatus()
    {
        $models = CrmQuotations::find()
            ->where(['quotation_status' => 8])
            ->andWhere(['crm_quotations.toe_rejected_date' => null])
            ->all();
        dd(count($models));
        foreach ($models as $key => $model) {
            $date_to_update = ($model->updated_at <> null) ? $model->updated_at : $model->status_change_date;
            Yii::$app->db->createCommand()
                ->update('crm_quotations', [
                    'status_change_date' => $date_to_update,
                    'toe_rejected_date' => $date_to_update,
                ], 'id=' . $model->id . '')
                ->execute();

        }

    }

    // on hold
    public function actionHoldStatus()
    {
        $models = CrmQuotations::find()
            ->where(['quotation_status' => 10])
            ->andWhere(['crm_quotations.on_hold_date' => null])
            ->all();
        dd(count($models));
        foreach ($models as $key => $model) {
            $date_to_update = ($model->updated_at <> null) ? $model->updated_at : $model->status_change_date;
            Yii::$app->db->createCommand()
                ->update('crm_quotations', [
                    'status_change_date' => $date_to_update,
                    'on_hold_date' => $date_to_update,
                ], 'id=' . $model->id . '')
                ->execute();

        }

    }

    // cancelled
    public function actionCancelledStatus()
    {
        $models = CrmQuotations::find()
            ->where(['quotation_status' => 11])
            ->andWhere(['crm_quotations.cancelled_date' => null])
            ->all();
        dd(count($models));
        foreach ($models as $key => $model) {
            $date_to_update = ($model->updated_at <> null) ? $model->updated_at : $model->status_change_date;
            Yii::$app->db->createCommand()
                ->update('crm_quotations', [
                    'status_change_date' => $date_to_update,
                    'cancelled_date' => $date_to_update,
                ], 'id=' . $model->id . '')
                ->execute();

        }

    }

    // regretted
    public function actionReggStatus()
    {
        $models = CrmQuotations::find()
            ->where(['quotation_status' => 12])
            ->andWhere(['crm_quotations.regretted_date' => null])
            ->all();
        dd(count($models));
        foreach ($models as $key => $model) {
            $date_to_update = ($model->updated_at <> null) ? $model->updated_at : $model->status_change_date;
            Yii::$app->db->createCommand()
                ->update('crm_quotations', [
                    'status_change_date' => $date_to_update,
                    'regretted_date' => $date_to_update,
                ], 'id=' . $model->id . '')
                ->execute();

        }

    }

    // banks with total num of valuations and estimated market value
    public function actionBanksData()
    {

        $companies = Company::find()
            ->select(['company.title as ClientName', 'COUNT(valuation.id) AS TotalValuations', 'SUM(valuation_approvers_data.estimated_market_value) AS TotalMarketValue'])
            ->leftJoin('valuation', 'valuation.client_id = company.id')
            ->leftJoin('valuation_approvers_data', 'valuation_approvers_data.valuation_id = valuation.id')
            ->where(['valuation.valuation_status' => 5])
            ->andWhere(['valuation_approvers_data.approver_type' => 'approver'])
            ->andWhere(['company.client_typee' => 'bank'])
            // ->andWhere(['company.client_type' => 'corporate'])
            ->groupBy('company.id')
            ->orderBy(['TotalValuations' => SORT_DESC])
            ->asArray()
            ->all();
        dd($companies);


    }



    public function actionUpdateQuoteFee()
    {
        $models = CrmQuotations::find()->orderBy(['id' => SORT_DESC])->all();

        foreach ($models as $model) {
            $properties = CrmReceivedProperties::find()->where(['quotation_id' => $model->id])->all();
            $quotation_fee_total = CrmReceivedProperties::find()->where(['quotation_id' => $model->id])->sum('quotation_fee');

            $final_amount = $quotation_fee_total;

            //no_of_property_discount
            if ($model->no_of_property_discount > 0 && (count($properties) > 0)) {
                $no_of_property_discount['value'] = $model->no_of_property_discount;
                $no_of_property_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->no_of_property_discount, $final_amount);
                $total_discount = $total_discount + $no_of_property_discount['amount'];
            } else {
                $no_of_property_discount = yii::$app->quotationHelperFunctions->getNumberOfPropertiesDiscount(count($properties), $final_amount);
                $model->no_of_property_discount = $no_of_property_discount['value'];
                $total_discount = $total_discount + $no_of_property_discount['amount'];
            }


            //same_building_discount
            $same_building_discount = [];
            if ($model->same_building_discount > 0 && ($model->no_of_units_same_building > 0)) {
                if ($model->no_of_units_same_building > 1) {
                    $same_building_discount['value'] = $model->same_building_discount;
                    $same_building_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->same_building_discount, $final_amount);
                    $total_discount = $total_discount + $same_building_discount['amount'];
                }
            } else {
                if ($model->no_of_units_same_building > 0) {
                    $same_building_discount = yii::$app->quotationHelperFunctions->getSameBuildingDiscount($model->no_of_units_same_building, $final_amount);
                    $model->same_building_discount = $same_building_discount['value'];
                    $total_discount = $total_discount + $same_building_discount['amount'];
                } else {
                    $model->same_building_discount = 0;
                }
            }



            //first_time_discount
            $clinet_first_time_check = Valuation::find()->where(['client_id' => $model->client_name])->one();
            if ($clinet_first_time_check == null) {
                if ($model->first_time_discount > 0) {
                    $first_time_fee_discount['value'] = $model->first_time_discount;
                    $first_time_fee_discount['amount'] = yii::$app->propertyCalcHelperFunction->getPercentageAmount($model->first_time_discount, $final_amount);
                    $total_discount = $total_discount + $first_time_fee_discount['amount'];
                } else {

                    $first_time_fee_discount = yii::$app->quotationHelperFunctions->getFirstTimeDiscount($final_amount);
                    $model->first_time_discount = $first_time_fee_discount['value'];
                    $total_discount = $total_discount + $first_time_fee_discount['amount'];
                }
            } else {
                $model->first_time_discount = 0;
            }

            //check relative discount
            if ($model->relative_discount_toe != null) {
                $discount = yii::$app->quotationHelperFunctions->getDiscountRupee($final_amount, $model->relative_discount_toe);
                $total_discount = $total_discount + $discount;
            }

            $final_amount = $final_amount - $total_discount;
            //urgencyfee check
            if ($model->tat_requirements > 0) {
                $urgencyfee = yii::$app->quotationHelperFunctions->getUrgencyfee($model->tat_requirements, $final_amount);
                $final_amount = $final_amount + $urgencyfee['amount'];
            }
            if ($model->client->client_type != 'bank') {
                //Advance Payment Terms
                $advance_payment_terms_data = explode('%', $model->advance_payment_terms);
                $advance_payment_terms = yii::$app->quotationHelperFunctions->getAdvancePaymentfee($advance_payment_terms_data[0], $final_amount);
            } else {
                $advance_payment_terms['value'] = 0;
                $advance_payment_terms['amount'] = 0;
            }






            $no_of_property_discount_data = $no_of_property_discount;
            $same_building_discount_data = $same_building_discount;
            $first_time_fee_discount_data = $first_time_fee_discount;
            $urgencyfee_data = $urgencyfee;
            $advance_payment_terms_data = $advance_payment_terms;

            // echo "no_of_property_discount_data<pre>"; print_r($no_of_property_discount_data); echo "</pre>"; echo"<br>";
            // echo "same_building_discount_data<pre>"; print_r($same_building_discount_data); echo "</pre>"; echo"<br>";
            // echo "first_time_fee_discount_data<pre>"; print_r($first_time_fee_discount_data); echo "</pre>"; echo"<br>";
            // echo "urgencyfee_data<pre>"; print_r($urgencyfee_data); echo "</pre>"; echo"<br>";
            // echo "advance_payment_terms_data<pre>"; print_r($advance_payment_terms_data); echo "</pre>"; echo"<br>";
            // die(); 


            // dd($no_of_property_discount_data);






            $total_discount = 0;
            $total_discount_amount = 0;
            $discount = 0;
            $discount_no_of_properties = 0;
            $discount_first_time = 0;
            $same_building_discount = 0;
            $urgencyfee = 0;
            $advance_paymentfee = 0;
            $VAT = 0;

            $netValuationFee = $quotation_fee_total - $discount;


            if ($model->no_of_property_discount != null) {
                // dd($no_of_property_discount_data);
                $netValuationFee = $netValuationFee - $no_of_property_discount_data['amount'];
                $total_discount = $total_discount + $model->no_of_property_discount;
                $total_discount_amount = $total_discount_amount + $no_of_property_discount_data['amount'];
            }
            // dd($total_discount_amount);

            if ($model->same_building_discount != null) {
                $netValuationFee = $netValuationFee - $same_building_discount_data['amount'];
                $total_discount = $total_discount + $model->same_building_discount;
                $total_discount_amount = $total_discount_amount + $same_building_discount_data['amount'];

            }

            if ($model->first_time_discount != null) {
                $netValuationFee = $netValuationFee - $first_time_fee_discount_data['amount'];
                $total_discount = $total_discount + $model->first_time_discount;
                $total_discount_amount = $total_discount_amount + $first_time_fee_discount_data['amount'];

            }



            if ($model->relative_discount != null) {
                $discount = yii::$app->quotationHelperFunctions->getDiscountRupee($quotation_fee_total, $model->relative_discount);
                $total_discount = $total_discount + $model->relative_discount;
                $total_discount_amount = $total_discount_amount + $discount;

                $netValuationFee = $netValuationFee - $discount;
            }
            $discount_net_fee = $netValuationFee;

            if ($urgencyfee_data != null) {
                $netValuationFee = $netValuationFee + $urgencyfee_data['amount'];
            }


            if ($advance_payment_terms_data != null) {
                $netValuationFee = $netValuationFee + $advance_payment_terms_data['amount'];
            }


            if ($model->client->vat == 1) {
                $VAT = yii::$app->quotationHelperFunctions->getVatTotal($netValuationFee);
            }


            $finalFeePayable = $netValuationFee + $VAT;

            Yii::$app->db->createCommand()
                ->update('crm_quotations', ['final_quoted_fee' => $finalFeePayable], 'id=' . $model->id . '')
                ->execute();


        }


    }




    public function actionDashboardDynamicIndex()
    {
        // dd(Yii::$app->request->queryParams);
        $this->checkLogin();
        $searchModel = new CrmQuotationsSearch();
        $dataProvider = $searchModel->dashboard_dynamic_search(Yii::$app->request->queryParams);

        return $this->render('dashboard-widgets/' . $searchModel->widget_view, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionRequestedDocsData($id)
    {
        // dd(Yii::$app->request->queryParams);

        $crm_qt_data = \app\models\CrmQuotations::find()->where(['id'=> $id])->one();
        $last_property_index = $crm_qt_data->no_of_properties - 1;
        $thisdoc = CrmReceivedDocs::find()->where(['quotation_id' => $id])->one();
        $files = CrmReceivedDocsFiles::find()->where(['quotation_id' => $id])->all();
      //  if($last_property_index == $thisdoc->property_index ){

            $table ='';
            $table .='<table style="width:100%">';
            $table .='<tr>';
            $table .='<th style="text-align: left;">Serial #</th>';
            $table .='<th style="text-align: left;">Property</th>';
            $table .='<th style="text-align: left;">Documents</th>';
            $table .='<th style="text-align: left;">Received Status</th>';
            $table .='<th style="text-align: left;">Mandatory Status</th>';
            $table .='</tr>';
            $files = CrmReceivedDocsFiles::find()->where(['quotation_id' => $id])->all();

            $properties = CrmReceivedProperties::find()->where(['quotation_id' => $id])->all();;
            $counter =-1;
            foreach ($files as $file){
                if($crm_qt_data->client->client_type == 'bank'){
                    if($file->document_id != 1) {
                        $property_details = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => $file->property_index])->one();
                        if ($counter == $file->property_index) {
                            $serial_number = '';
                            $property_detail = '';
                        } else {
                            $serial_number = ($file->property_index + 1);
                            $property_detail = $property_details->property->title . '(' . $property_details->building->communities->title . ')';
                        }

                        if ($file->attachment <> null) {
                            $document_received_status = "Received";
                        } else {
                            $document_received_status = "Not Received";
                        }

                        if ($file->needed == 0) {
                            $document_required_status = "Mandatory";
                        } else {
                            $document_required_status = "Helpful";
                        }

                        $table .= '<tr>';
                        $table .= '<td>' . $serial_number . '</td>';
                        $table .= '<td>' . $property_detail . '</td>';
                        $table .= '<td>' . Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$file->document_id] . '</td>';
                        $table .= '<td>' . $document_received_status . '</td>';
                        $table .= '<td>' . $document_required_status . '</td>';
                        $table .= '</tr>';
                        $counter = $file->property_index;
                    }
                }else{

                    $property_details = CrmReceivedProperties::find()->where(['quotation_id' => $id, 'property_index' => $file->property_index])->one();
                    if ($counter == $file->property_index) {
                        $serial_number = '';
                        $property_detail = '';
                    } else {
                        $serial_number = ($file->property_index + 1);
                        $property_detail = $property_details->property->title . '(' . $property_details->building->communities->title . ')';
                    }

                    if ($file->attachment <> null) {
                        $document_received_status = "Received";
                    } else {
                        $document_received_status = "Not Received";
                    }

                    if ($file->needed == 0) {
                        $document_required_status = "Mandatory";
                    } else {
                        $document_required_status = "Helpful";
                    }

                    $table .= '<tr>';
                    $table .= '<td>' . $serial_number . '</td>';
                    $table .= '<td>' . $property_detail . '</td>';
                    $table .= '<td>' . Yii::$app->appHelperFunctions->propertiesDocumentsListArr[$file->document_id] . '</td>';
                    $table .= '<td>' . $document_received_status . '</td>';
                    $table .= '<td>' . $document_required_status . '</td>';
                    $table .= '</tr>';
                    $counter = $file->property_index;

                }

            }
            $table .='</table>';


      //  }

        echo $table;
    }


    public function actionReasonViewData($id)
    {
        $crm_qt_data = \app\models\CrmQuotations::find()->where(['id'=> $id])->one();
        $cancelReasonArr = \app\models\QuotationCancelReasons::find()
        ->select([\app\models\QuotationCancelReasons::tableName() . ".id", \app\models\QuotationCancelReasons::tableName() . ".title"])
        ->where([\app\models\QuotationCancelReasons::tableName() . ".id" => $crm_qt_data->quotation_cancel_reason])
        ->one();

        $reason = '';
        $reason .= '<div>' . ($cancelReasonArr->title) ? $cancelReasonArr->title : 'No reason provided.'. '</div>';

        echo $reason;
    }

   

















}