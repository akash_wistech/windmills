<?php

namespace app\controllers;

use app\assets\ListingsFormAsset;
use app\models\BuildingForSave;
use app\models\Buildings;
use app\models\ListData;
use app\models\ListingTransactionsImportForm;
use Yii;
use app\models\ListingsTransactions;
use app\models\ListingsTransactionsSearch;
use app\components\helpers\DefController;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Expression;

/**
 * ListingsTransactionsController implements the CRUD actions for ListingsTransactions model.
 */
class ListingsTransactionsController extends DefController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        // Check if the user is logged in
        if (Yii::$app->user->isGuest) {
            // Redirect to the login page
            return Yii::$app->response->redirect(['site/login'])->send();
        }
    }

    /**
     * Lists all ListingsTransactions models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->checkLogin();
        $searchModel = new ListingsTransactionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndexMerge()
    {
        $this->checkLogin();
        $searchModel = new ListingsTransactionsSearch();
        $searchModel->duplicate_status =8;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ListingsTransactions model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
       // Yii::$app->db->createCommand()->update('valuation', ['valuation_status' => 3], 'id=5357')->execute();
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ListingsTransactions model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ListingsTransactions();

        if ($model->load(Yii::$app->request->post())) {

            $model_ref = \app\models\ListingsTransactions::find()->where(['listings_reference' =>$model->listings_reference])->one();
            if ($model_ref <> null) {
                Yii::$app->getSession()->addFlash('error', Yii::t('app', 'Listing Reference Is Already Exist.'));
                return false;
            }
            $model->status_verified = $model->status;
            $this->StatusVerify($model);
            if($model->save()){
                $this->makeHistory([
                    'model' => $model, 
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                    'action' => 'data_created',
                    'verify_field' => 'status',
                ]);
                Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
                return $this->redirect(['index']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionListNotSaved()
    {
        $this->checkLogin();
        $day1 = date('Y-m-d',strtotime("-1 days"));
        $day2 = date('Y-m-d',strtotime("-2 days"));
        $day3 = date('Y-m-d',strtotime("-3 days"));
        $day4 = date('Y-m-d',strtotime("-4 days"));
        $day5 = date('Y-m-d',strtotime("-5 days"));
        $day6 = date('Y-m-d',strtotime("-6 days"));
        $day7 = date('Y-m-d',strtotime("-7 days"));
        $day8 = date('Y-m-d',strtotime("-8 days"));
        $day9 = date('Y-m-d',strtotime("-9 days"));
        $day10 = date('Y-m-d',strtotime("-10 days"));
        $start_date = date('Y-m-d',strtotime("-1 days"));


        $startDate = $start_date.' 00:00:00';
        $endDate = $start_date.' 23:59:59';


        $all_data = ListData::find()
            ->where(['in', 'move_to_listing', [0] ])
            ->andWhere(['in', 'listing_date', [$day1,$day2,$day3,$day4,$day5,$day6,$day7,$day8,$day9,$day10] ])
           // ->andWhere([ 'between', 'created_at', $startDate, $endDate ])
          //  ->andWhere(new Expression('listing_date LIKE "%'.$start_date.'%" '))
           // ->asArray()
            ->all();

        return $this->render('not_saved_list', [
            'all_data' => $all_data,
            'title_view' => 'Auto Lists Not Saved',
        ]);
    }
    public function actionListNotSaveBuilding()
    {
        $this->checkLogin();
        $all_data =  BuildingForSave::find()->where(['status'=>0])->all();


       /* foreach ($all_data as $data){
            $buildingRow = Buildings::find()
                ->where(['=', 'bayut_title', trim($data->building_name)])
               // ->orWhere(['=', 'reidin_title', trim($buildingName)])
                ->asArray()->one();

            if ($buildingRow != null) {
                Yii::$app->db->createCommand()->update('building_for_save', [ 'status' => 1 ], [ 'id'=>$data->id ])->execute();
            }

        }*/

        return $this->render('not_saved_list', [
            'all_data' => $all_data,
            'title_view' => 'Auto Lists Not Saved Building',
        ]);
    }

    public function actionDup()
    {
        $this->checkLogin();
        $all_data =  BuildingForSave::find()->where(['status'=>0])->all();


         foreach ($all_data as $data){
             $buildingRow = Buildings::find()
                 ->where(['=', 'title', trim($data->building_name)])
                 ->orwhere(['=', 'bayut_title', trim($data->building_name)])
                 ->orwhere(['=', 'bayut_title_2', trim($data->building_name)])
                 ->orWhere(['=', 'reidin_title', trim($data->building_name)])
                 ->orWhere(['=', 'reidin_title_2', trim($data->building_name)])
                 ->asArray()->one();

             if ($buildingRow != null) {
                 Yii::$app->db->createCommand()->update('building_for_save', [ 'status' => 1 ], [ 'id'=>$data->id ])->execute();
             }

         }

       /* return $this->render('not_saved_list', [
            'all_data' => $all_data,
            'title_view' => 'Auto Lists Not Saved Building',
        ]);*/
    }

    /**
     * Updates an existing ListingsTransactions model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_verify_status = $model->status;

        if ($model->load(Yii::$app->request->post())) {
            $model->status_verified = $model->status;
            $this->StatusVerify($model);
            if($model->save()){
                $this->makeHistory([
                    'model' => $model, 
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                    'action' => 'data_updated',
                    'verify_field' => 'status',
                    'old_verify_status' => $old_verify_status,
                ]);
                Yii::$app->getSession()->addFlash('success', Yii::t('app','Information updated successfully'));
                return $this->redirect(['index']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ListingsTransactions model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Import for Listings Transactions .
     * If import is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionImport()
    {
        die;
        $model = new ListingTransactionsImportForm();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->getSession()->setFlash('success', Yii::t('app','Data imported successfully'));
                return $this->redirect(['index']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('import', [
            'model' => $model,
        ]);
    }
    public function actionDeleteDuplicate($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Information deleted successfully'));
        return $this->redirect(['reports/list-duplicates']);
    }

    public function actionUpdateDuplicates($id)
    {

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->getSession()->addFlash('success', Yii::t('app','Information updated successfully'));
                return $this->redirect(['reports/list-duplicates']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDeleteDuplicateRef($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->getSession()->addFlash('success', Yii::t('app','Information deleted successfully'));
        return $this->redirect(['reports/list-duplicates-reference']);
    }

    public function actionUpdateDuplicatesRef($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->getSession()->addFlash('success', Yii::t('app','Information updated successfully'));
                return $this->redirect(['reports/list-duplicates-reference']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
    /**
     * Finds the ListingsTransactions model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ListingsTransactions the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ListingsTransactions::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
