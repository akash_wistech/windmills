<?php

namespace app\controllers;

use app\models\Buildings;
use app\models\Company;
use app\models\Country;
use app\models\PreviousTransactions;
use app\models\SharjahListings;
use app\models\SubCommunities;
use app\models\User;
use app\models\Valuation;
use app\models\ValuationApproversData;
use app\models\Zone;
use Yii;
use app\models\Communities;
use app\models\CommunitiesSearch;
use app\components\helpers\DefController;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use GuzzleHttp\Client;
use Sunra\PhpSimple\HtmlDomParser;
use yii\helpers\Url;
use Symfony\Component\DomCrawler\Crawler;
use app\components\traits\BayutRentProperties;
use Exception;
use yii\db\Expression;
use DateTime;
use DateInterval;
use DatePeriod;
/**
 * CommunitiesController implements the CRUD actions for Communities model.
 */
class CommunitiesController extends DefController
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Communities models.
     * @return mixed
     */
    public function actionScrapSharjha()

    {
/*$phonenumbers = array('971528078852','971528078852');
foreach ($phonenumbers as $phonenumber) {
//bank 1
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "https://restapi.smscountry.com/v0.1/Accounts/GSzAf5xi3QzofjOMtlkl/SMSes/");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);

    curl_setopt($ch, CURLOPT_POST, TRUE);

    curl_setopt($ch, CURLOPT_POSTFIELDS, "{
  \"Text\": \"Valuation Received from your financing bank.
Thank you.
Please advise your suitable inspection day/time.


Emmalou Torres
Valuation Support Team
Windmills
+971 55 621 8786
support@windmillsgroup.com
\",
  \"Number\": $phonenumber,
  \"SenderId\": \"WINDMILLS\",
  \"DRNotifyHttpMethod\": \"POST\",
  \"Tool\": \"API\"
}");

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "Authorization : Basic " . base64_encode('GSzAf5xi3QzofjOMtlkl:m82nK16BVJ67vFfulSxosIfhlgwEJgx2XYaBUPHm')
    ));

    $response = curl_exec($ch);
    curl_close($ch);


}

die;*/








        $ch = curl_init();

// Set the URL to fetch
        $url = "https://www.shjrerd.gov.ae/default.aspx?PageId=355&LanguageId=1";
        curl_setopt($ch, CURLOPT_URL, $url);

// Set cURL options to receive the response as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

// Execute the cURL request
        $response = curl_exec($ch);

// Close cURL handle
        curl_close($ch);

        $html = $response;

        $crawler = new Crawler($html);

        $table = $crawler->filter('table')->first();
        $rows = $table->filter('tr')->each(function (Crawler $row, $i) {
            return $row->filter('td')->each(function (Crawler $cell, $j) {
                return $cell->text();
            });
        });
        $results = array_values(array_filter($rows));
        if(!empty($results) && $results <> null) {
            foreach ($results as $result) {
                $price = explode(' AED',$result[2]);
                $data = new SharjahListings();
                $data->area =  $result[0];
                $data->property_type =  $result[1];
                $data->price =  $price[0];
                $data->created_date =  '2023-09-06';
                $data->save();
            }
        }
        echo "<pre>";
        print_r($results);
        die;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.scrapingant.com/v2/general?url=https%3A%2F%2Fwww.bayut.com%2Fproperty-market-analysis%2F&x-api-key=fc0416bae97a47e4a51af30e212232d1',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        // Load the HTML content of the page
        $html = $response;
        $crawler = new Crawler($html);
        $table = $crawler->filter('table')->first();
        $rows = $table->filter('tr')->each(function (Crawler $row, $i) {
            return $row->filter('td')->each(function (Crawler $cell, $j) {
                return $cell->text();
            });
        });
        $result = array_values(array_filter($rows));
        echo "<pre>";
        print_r($result);
        die;
        $buttonLinks = [];
        $buttonTitles = [];
        $table = $crawler->filter('table')->filter('tr')->each(function ($tr, $i) {
            $buttonLinks[]= $tr->filter('td')->each(function ($td, $i) {
                $buttonTitles[] = trim($td->text());
            });
        });

        echo "<pre>";
        print_r($title);
        print_r($table);
        print_r($buttonLinks);
        print_r($buttonTitles);
        die;
        $title = $crawler->filter('table')->text();
        $buttonLinks = [];
        $buttonTitles = [];
        $response->filter('table')->each(function($buttonNode) use(&$buttonLinks,&$buttonTitles){
            $buttonLinks[] = $buttonNode->attr('tr');
            $buttonTitles[] = $buttonNode->attr('td');
        });

        // Parse the HTML using Simple HTML DOM Parser
        $dom = HtmlDomParser::str_get_html($html);
        echo "<pre>";
        print_r($crawler);
        die;
        // Find the table element
        $table = $dom->find('table', 0);

        echo "<pre>";
        print_r($table);
        die;



        // Base URL of the page with the table
        $url = 'https://www.bayut.com/property-market-analysis';

        $client = new Client();

        // Send a GET request to the URL of the page containing the dynamically loaded content
        $response = $client->request('GET', $url);

        // Get the response body
        $content = $response->getBody()->getContents();
        echo "<pre>";
        print_r($content);
        die;
    }
    public function actionCrmdates()
    {
         ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');
        $configuration = (new \yii\db\Query())
            ->select('*')
            ->from('crm_quotations')
            ->where(['trashed' => null])
            ->all();

        if($configuration <> null) {
            foreach ($configuration as $key => $value) {

                $startDate = $value['instruction_date'] . ' ' . $value['inquiry_received_time'];
                $endDate = $value['approved_date'];
                // $endDate = date("Y-m-d  H:i:s");
                if ($endDate <> null) {
                    $startDateTime = new DateTime($startDate); // Start date and time
                    $endDateTime = new DateTime($endDate);

                    $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                    $workingDays = abs(number_format(($workingHours / 8.5), 1));
                    $s = ($workingDays > 2) ? 's' : '';


                    \Yii::$app->db->createCommand("UPDATE crm_quotations SET verify_time_days='" . $workingDays . "' WHERE id=" . $value['id'])->execute();
                    \Yii::$app->db->createCommand("UPDATE crm_quotations SET verify_time_hours='" . $workingHours . "' WHERE id=" . $value['id'])->execute();
                }
            }
            }

        die('here');
    }
    public function actionCrmdatescancel()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');
        $configuration = (new \yii\db\Query())
            ->select('*')
            ->from('crm_quotations')
            ->where(['trashed' => null])
            ->all();

        if($configuration <> null) {
            foreach ($configuration as $key => $value) {

                $startDate = $value['instruction_date'] . ' ' . $value['inquiry_received_time'];
                $endDate = $value['cancelled_date'];
                // $endDate = date("Y-m-d  H:i:s");
                if ($value['quotation_status'] == 7 || $value['quotation_status'] == 11) {
                    $startDateTime = new DateTime($startDate); // Start date and time
                    $endDateTime = new DateTime($endDate);

                    $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                    $workingDays = abs(number_format(($workingHours / 8.5), 1));
                    $s = ($workingDays > 2) ? 's' : '';


                    \Yii::$app->db->createCommand("UPDATE crm_quotations SET cancelled_date_days='" . $workingDays . "' WHERE id=" . $value['id'])->execute();
                    \Yii::$app->db->createCommand("UPDATE crm_quotations SET cancelled_date_hours='" . $workingHours . "' WHERE id=" . $value['id'])->execute();
                }
            }
        }

        die('here');
    }
    public function actionCrmdatesfinal()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');
        $configuration = (new \yii\db\Query())
            ->select('*')
            ->from('crm_quotations')
            ->where(['trashed' => null])
            ->all();

        if($configuration <> null) {
            foreach ($configuration as $key => $value) {

                $startDate = $value['instruction_date'] . ' ' . $value['inquiry_received_time'];
                $endDate = $value['payment_received_date'];
                // $endDate = date("Y-m-d  H:i:s");
                if ($endDate <> null) {
                    $startDateTime = new DateTime($startDate); // Start date and time
                    $endDateTime = new DateTime($endDate);

                    $workingHours = Yii::$app->appHelperFunctions->getCalculateWorkingHours($startDateTime, $endDateTime);
                    $workingDays = abs(number_format(($workingHours / 8.5), 1));
                    $s = ($workingDays > 2) ? 's' : '';


                    \Yii::$app->db->createCommand("UPDATE crm_quotations SET total_time_days='" . $workingDays . "' WHERE id=" . $value['id'])->execute();
                    \Yii::$app->db->createCommand("UPDATE crm_quotations SET total_time_hours='" . $workingHours . "' WHERE id=" . $value['id'])->execute();
                }
            }
        }

        die('here');
    }
    public function actionImagesresize()
    {
       // ini_set('memory_limit', '-1');
       // ini_set('max_execution_time', '0');
        $configuration = (new \yii\db\Query())
            ->select('configuration_files.id,configuration_files.type,configuration_files.attachment,configuration_files.checked_image,configuration_files.rep_image')
            ->from('valuation')
            ->innerJoin('configuration_files', "configuration_files.valuation_id=valuation.id")
            ->where(['configuration_files.rep_image' => null])
            ->andWhere(['valuation.valuation_status' => 3])
            //->andWhere(['valuation.id' => 10540])
            ->andWhere(['configuration_files.checked_image' => 'on'])
            ->all();

        if($configuration <> null) {
            foreach ($configuration as $key => $value) {


                if ($value['attachment'] != null && $value['type'] != '') {


                    $image = Yii::$app->helperFunctions->resize($value['attachment'], 200, 200);
                    \Yii::$app->db->createCommand("UPDATE configuration_files SET rep_image='" . $image . "' WHERE id=" . $value['id'])->execute();
                }
            }
        }
            die('here');
    }
    public function actionImagesresizeid($id)
    {
       // ini_set('memory_limit', '-1');
       // ini_set('max_execution_time', '0');
        $configuration = (new \yii\db\Query())
            ->select('configuration_files.id,configuration_files.type,configuration_files.attachment,configuration_files.checked_image,configuration_files.rep_image')
            ->from('valuation')
            ->innerJoin('configuration_files', "configuration_files.valuation_id=valuation.id")
            ->where(['configuration_files.rep_image' => null])
          //  ->andWhere(['valuation.valuation_status' => 3])
            ->andWhere(['valuation.id' => $id])
            ->andWhere(['configuration_files.checked_image' => 'on'])
            ->all();

        if($configuration <> null) {
            foreach ($configuration as $key => $value) {


                if ($value['attachment'] != null && $value['type'] != '') {


                    $image = Yii::$app->helperFunctions->resize($value['attachment'], 200, 200);
                    \Yii::$app->db->createCommand("UPDATE configuration_files SET rep_image='" . $image . "' WHERE id=" . $value['id'])->execute();
                }
            }
        }
            die('here');
    }
    public function actionVoneresize()
    {
        // ini_set('memory_limit', '-1');
        // ini_set('max_execution_time', '0');
        $vid= 10440;
        $configuration = (new \yii\db\Query())
            ->select('configuration_files.id,configuration_files.type,configuration_files.attachment,configuration_files.checked_image,configuration_files.rep_image')
            ->from('valuation')
            ->innerJoin('configuration_files', "configuration_files.valuation_id=valuation.id")
            ->where(['configuration_files.rep_image' => null])
            ->andWhere(['in', 'valuation.valuation_status', [1,2,3,7]])
            ->andWhere(['>', 'valuation.id', $vid])
            ->andWhere(['not', ['valuation.parent_id' => null]])
            //->andWhere(['valuation.id' => 10540])
            ->andWhere(['configuration_files.checked_image' => 'on'])
            ->all();

        if($configuration <> null) {
            foreach ($configuration as $key => $value) {


                if ($value['attachment'] != null && $value['type'] != '') {


                    $image = Yii::$app->helperFunctions->resize($value['attachment'], 200, 200);
                    \Yii::$app->db->createCommand("UPDATE configuration_files SET rep_image='" . $image . "' WHERE id=" . $value['id'])->execute();
                }
            }
        }
        die('here');
    }
    public function actionScrapeTableData()
    {


        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');

        $clients = Company::find()->where(['client_type'=> 'bank','monthly_email'=> 1,'status'=> 1])->all();


        $month =  date('m') - 1;
        $year = date('Y');
        if($month == 0){
            $month= 1;

        }
        foreach ($clients as $client) {

                $valuations_month_number = Valuation::find()->where(['client_id' => $client->id])
                    ->andWhere('valuation_status=5')
                    ->andWhere(['valuation.parent_id' => null])
                    ->andFilterWhere([
                        'between', 'submission_approver_date', '2024-11-01 00:00:00', '2024-11-30 23:59:59'
                    ])
                    ->all();

                $valuations_year_number = Valuation::find()->where(['client_id' => $client->id])
                    ->andWhere('valuation_status=5')
                    ->andFilterWhere([
                        'between', 'submission_approver_date', '2024-01-01 00:00:00', '2024-11-30 23:59:59'
                    ])
                    ->andWhere(['valuation.parent_id' => null])
                    ->all();

                $last_month = date('F', strtotime('last month'));
                $date_value = $last_month . ' ' . date('Y');
                $subject = 'Valuation Instructions to Windmills in ' . $date_value;

                echo $client->id . '<br>' . $client->title . '<br>' . $client->primaryContact->email . '<br> total:' . count($valuations_year_number) . '<br> this month:' . count($valuations_month_number) . '<br><br>';
            $attachments = array();
          if(count($valuations_month_number) > 0) {

              $curl_handle_1 = curl_init();
              curl_setopt($curl_handle_1, CURLOPT_URL, Url::toRoute(['client/monthlystatsbankfile', 'id' => $client->id]));
              curl_setopt($curl_handle_1, CURLOPT_CONNECTTIMEOUT, 2);
              curl_setopt($curl_handle_1, CURLOPT_RETURNTRANSFER, 1);
              curl_setopt($curl_handle_1, CURLOPT_USERAGENT, 'Maxima');

              $val2 = curl_exec($curl_handle_1);
              $ip = curl_getinfo($curl_handle_1, CURLINFO_PRIMARY_IP);
              curl_close($curl_handle_1);

              $attachments[] = $val2;
          }else{
              $attachments = array();
          }

                 $notifyData = [
                     'client' => $client,
                     'subject' => $subject,
                     'attachments' => $attachments,
                     'uid' => $client->id,
                     'replacements' => [
                         '{clientName}' => $client->title,
                         '{total_valuation}' => count($valuations_year_number),
                         '{date_value}' => $date_value,
                         '{this_month_valuation}' => count($valuations_month_number),

                     ],
                 ];

                if (count($valuations_month_number) > 10) {
                      \app\modules\wisnotify\listners\NotifyEvent::fireMonthlyAllBanks('valuation.monthlymorethanten', $notifyData);

                } else if (count($valuations_month_number) < 10) {
                     \app\modules\wisnotify\listners\NotifyEvent::fireMonthlyAllBanks('valuation.monthlylessthanten', $notifyData);
                }
                 Yii::$app->db->createCommand()
                     ->update('company', ['monthly_email' => 2], 'id='.$client->id .'')
                     ->execute();

        }
        die('done');



 /*       UPDATE `company` SET `monthly_email` = '1' WHERE `company`.`id` = 1;
        UPDATE `company` SET `monthly_email` = '1' WHERE `company`.`id` = 3;
        UPDATE `company` SET `monthly_email` = '1' WHERE `company`.`id` = 4;
        UPDATE `company` SET `monthly_email` = '1' WHERE `company`.`id` = 5;
        UPDATE `company` SET `monthly_email` = '1' WHERE `company`.`id` = 9;
        UPDATE `company` SET `monthly_email` = '1' WHERE `company`.`id` = 11;
        UPDATE `company` SET `monthly_email` = '1' WHERE `company`.`id` = 12;

        UPDATE `company` SET `monthly_email` = '1' WHERE `company`.`id` = 29;
        UPDATE `company` SET `monthly_email` = '1' WHERE `company`.`id` = 71;
        UPDATE `company` SET `monthly_email` = '1' WHERE `company`.`id` = 110;
        UPDATE `company` SET `monthly_email` = '1' WHERE `company`.`id` = 180;
        UPDATE `company` SET `monthly_email` = '1' WHERE `company`.`id` = 183;
        UPDATE `company` SET `monthly_email` = '1' WHERE `company`.`id` = 230;
        UPDATE `company` SET `monthly_email` = '1' WHERE `company`.`id` = 253;
        UPDATE `company` SET `monthly_email` = '1' WHERE `company`.`id` = 6583;
        UPDATE `company` SET `monthly_email` = '1' WHERE `company`.`id` = 39242;
        UPDATE `company` SET `monthly_email` = '1' WHERE `company`.`id` = 9437;
        UPDATE `company` SET `monthly_email` = '1' WHERE `company`.`id` = 54514;*/



$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://api.scrapingant.com/v2/general?url=https%3A%2F%2Fwww.bayut.com%2Fproperty-market-analysis%2F&x-api-key=fc0416bae97a47e4a51af30e212232d1',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'GET',
));

$response = curl_exec($curl);

curl_close($curl);





        // Load the HTML content of the page
        $html = $response;

        $crawler = new Crawler($html);

        $table = $crawler->filter('table')->first();
        $rows = $table->filter('tr')->each(function (Crawler $row, $i) {
            return $row->filter('td')->each(function (Crawler $cell, $j) {
                return $cell->text();
            });
        });
        $result = array_values(array_filter($rows));
        echo "<pre>";
        print_r($result);
        die;
        $buttonLinks = [];
        $buttonTitles = [];
        $table = $crawler->filter('table')->filter('tr')->each(function ($tr, $i) {
            $buttonLinks[]= $tr->filter('td')->each(function ($td, $i) {
                $buttonTitles[] = trim($td->text());
            });
        });

        echo "<pre>";
        print_r($title);
        print_r($table);
        print_r($buttonLinks);
        print_r($buttonTitles);
        die;
            $title = $crawler->filter('table')->text();
        $buttonLinks = [];
        $buttonTitles = [];
        $response->filter('table')->each(function($buttonNode) use(&$buttonLinks,&$buttonTitles){
            $buttonLinks[] = $buttonNode->attr('tr');
            $buttonTitles[] = $buttonNode->attr('td');
        });

        // Parse the HTML using Simple HTML DOM Parser
        $dom = HtmlDomParser::str_get_html($html);
        echo "<pre>";
        print_r($crawler);
        die;
        // Find the table element
        $table = $dom->find('table', 0);

        echo "<pre>";
        print_r($table);
        die;



        // Base URL of the page with the table
        $url = 'https://www.bayut.com/property-market-analysis';

        $client = new Client();

        // Send a GET request to the URL of the page containing the dynamically loaded content
        $response = $client->request('GET', $url);

        // Get the response body
        $content = $response->getBody()->getContents();
        echo "<pre>";
        print_r($content);
        die;
    }
    public function actionIndex()
    {

        $searchModel = new CommunitiesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Communities model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Communities model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Communities();

        if ($model->load(Yii::$app->request->post())) {

            $this->StatusVerify($model);

            if($model->save()){

                $this->makeHistory([
                    'model' => $model, 
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                    'action' => 'data_created',
                    'verify_field' => 'status',
                ]);

                Yii::$app->getSession()->setFlash('success', Yii::t('app','Information saved successfully'));
                return $this->redirect(['index']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Communities model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $old_verify_status = $model->status;

        if ($model->load(Yii::$app->request->post())) {
            
            $model->status_verified = $model->status;
            $this->StatusVerify($model);
            if($model->save()){

                $this->makeHistory([
                    'model' => $model, 
                    'model_name' => Yii::$app->appHelperFunctions->getModelName(),
                    'action' => 'data_updated',
                    'verify_field' => 'status',
                    'old_verify_status' => $old_verify_status,
                ]);

                Yii::$app->getSession()->addFlash('success', Yii::t('app','Information updated successfully'));
                return $this->redirect(['index']);
            }else{
                if($model->hasErrors()){
                    foreach($model->getErrors() as $error){
                        if(count($error)>0){
                            foreach($error as $key=>$val){
                                Yii::$app->getSession()->addFlash('error', $val);
                            }
                        }
                    }
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
















    /**
     * View Comminities by search parameters and return units
     * @return array of search results
     * */
    public function actionSearchCommuinites()
    {
        ob_start();
        $out = [];

        if (Yii::$app->request->post('depdrop_parents')) {
            $parents = Yii::$app->request->post('depdrop_parents');
            if ($parents != null) {
                $city_id = $parents[0];
                $selected = false;

                if (Yii::$app->request->post('depdrop_all_params')) {
                    $params = Yii::$app->request->post('depdrop_all_params');
                }
                $commuinities = Communities::find()->where(['city' => $city_id])->andWhere(['status'=>1])->andWhere(['trashed'=>0])->all();


                if (isset($params['community_value']) && $params['community_value'] <> null) {
                    $selected =  $params['community_value'];
                }

                $n = 1;
                foreach ($commuinities as $commuinity) {

                    $out[] = [
                        'id' => $commuinity->id,
                        'name' => $commuinity->title,
                    ];
                }

                echo Json::encode(['output' => $out, 'selected' => $selected]);
                return;
                exit();
            }
        }
        echo Json::encode(['output' => '', 'selected' => '']);
        return;
        exit();
    }

    /**
     * Deletes an existing Communities model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
/*    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }*/

    /**
     * Finds the Communities model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Communities the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Communities::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionCommunitydetail($id)
    {

        // $buildings = Buildings::find()->where(['community' => $id])->orderBy(['title' => SORT_ASC])->all();
        $buildingOptions = Buildings::find()
            ->where(['community' => $id])
            ->andWhere(['trashed' => 0])
            ->andWhere(['status' => 1])
            ->orderBy(['title' => SORT_ASC])
            ->all();
        $buildingOptionsHtml = '<option value="">Select a Building ...</option>';
        foreach ($buildingOptions as $building) {
            $buildingOptionsHtml .= '<option value="' . $building->id . '">' . $building->title . '</option>';
        }

        // Fetch sub-community options
        $subCommunityOptions = SubCommunities::find()
            ->where(['community' => $id])
            ->andWhere(['trashed' => 0])
            ->andWhere(['status' => 1])
            ->orderBy(['title' => SORT_ASC])
            ->all();

        $subCommunityOptionsHtml = '<option value="">Select a Sub Community ...</option>';
        foreach ($subCommunityOptions as $subCommunity) {
            $subCommunityOptionsHtml .= '<option value="' . $subCommunity->id . '">' . $subCommunity->title . '</option>';
        }

        // Fetch city and country
        $community = Communities::findOne($id);


        $city=Zone::find()
            ->select([
                'id',
                'country_id',
            ])
            ->where(['id' => $community->city])
            ->orderBy(['title' => SORT_ASC])->asArray()->one();
        $country = Country::find()
            ->select([
                'title',
            ])
            ->where(['id' => $city['country_id']])->asArray()->one();

        // $city = $city['id'];
        $city = Yii::$app->appHelperFunctions->emiratedListArr[$city['id']];
        // $country = $community->country;

        // Prepare data to send back
        $responseData = [
            'buildingOptions' => $buildingOptionsHtml,
            'subCommunityOptions' => $subCommunityOptionsHtml,
            'city' => $city,
            'country' => $country['title'],
        ];

        // Encode data as JSON and return
        return Json::encode($responseData);
        // return json_encode(array('buildingOptions' => $buildingOptionsHtml,'subCommunityOptions' => $subCommunityOptionsHtml, 'city' => $city,'country'=>$country['title']));


    }

    public function actionBuildingdetail($id)
    {
        $detail = Buildings::findOne($id);

        // Fetch sub-community options
        $subCommunityOptions = SubCommunities::find()
            ->where(['community' => $detail->communities->id])
            ->andWhere(['trashed' => 0])
            ->orderBy(['title' => SORT_ASC])
            ->all();


        $subCommunityOptionsHtml = '<option value="">Select a Sub Community ...</option>';
        foreach ($subCommunityOptions as $subCommunity) {
            $selected = ($detail->subCommunities->id == $subCommunity->id ) ? "selected" : "";
            $subCommunityOptionsHtml .= '<option value="' . $subCommunity->id . '" '.$selected.'>' . $subCommunity->title . '</option>';
        }

        $city=Zone::find()
            ->select([
                'country_id',
            ])
            ->where(['id' => $detail->city])
            ->orderBy(['title' => SORT_ASC])->asArray()->one();
        $country = Country::find()
            ->select([
                'title',
            ])
            ->where(['id' => $city['country_id']])->asArray()->one();
        $detail->city = Yii::$app->appHelperFunctions->emiratedListArr[$detail->city];
        $detail->community = $detail->communities->title;
        $detail->sub_community = $subCommunityOptionsHtml;
        $property_type = $detail->property->id;
        $valuation_approach = $detail->property->valuation_approach;


        return json_encode(array('detail' => $detail->attributes,'subCommunity' => $subCommunityOptionsHtml,'ptype'=>$property_type, 'valuation_approach'=>$valuation_approach,'country'=>$country['title']));
    }

    public function actionCommunitydetailbycity($id)
    {

        // $buildings = Buildings::find()->where(['community' => $id])->orderBy(['title' => SORT_ASC])->all();
        $communityOptions = Communities::find()
            ->where(['city' => $id])
            ->andWhere(['trashed' => 0])
            ->andWhere(['status' => 1])
            ->orderBy(['title' => SORT_ASC])
            ->all();
        $communitiesOptionsHtml = '<option value="">Select a Community ...</option>';
        foreach ($communityOptions as $community) {
            $communitiesOptionsHtml .= '<option value="' . $community->id . '">' . $community->title . '</option>';
        }




        $city=Zone::find()
            ->select([
                'id',
                'country_id',
            ])
            ->where(['id' => $id])
            ->orderBy(['title' => SORT_ASC])->asArray()->one();
        $country = Country::find()
            ->select([
                'title',
            ])
            ->where(['id' => $city['country_id']])->asArray()->one();

        // $city = $city['id'];
        $city = Yii::$app->appHelperFunctions->emiratedListArr[$city['id']];
        // $country = $community->country;

        // Prepare data to send back
        $responseData = [
            'communitiesOptionsHtml' => $communitiesOptionsHtml,
            'city' => $city,
            'country' => $country['title'],
        ];

        // Encode data as JSON and return
        return Json::encode($responseData);
        // return json_encode(array('buildingOptions' => $buildingOptionsHtml,'subCommunityOptions' => $subCommunityOptionsHtml, 'city' => $city,'country'=>$country['title']));


    }
}
