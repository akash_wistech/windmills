<?php

namespace app\controllers;

use app\models\Company;
use app\models\ConfigurationFiles;
use app\models\CrmInspectProperty;
use app\models\CrmQuotationConfigraions;
use app\models\CrmReceivedDocs;
use app\models\CrmReceivedProperties;
use app\models\CrmScheduleInspection;
use app\models\CrmValuationConflict;
use app\models\InspectProperty;
use app\models\PreviousTransactions;
use app\models\ReceivedDocs;
use app\models\ReceivedDocsFiles;
use app\models\ScheduleInspection;
use app\models\Valuation;
use app\models\ValuationConfiguration;
use app\models\ValuationConflict;
use app\models\ValuationOwners;
use Yii;
use app\models\CrmQuotations;
use app\models\CrmQuotationsSearch;
use app\components\helpers\DefController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * CrmQuotationsController implements the CRUD actions for CrmQuotations model.
 */
class CrmQuotationsController extends DefController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionStep_10_v($id)
    {
        $this->checkLogin();
        if (!Yii::$app->menuHelperFunction->checkActionAllowed('step_10')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }


        //Step 1st enquiry (step 10)


        $InspectProperty = \app\models\InspectProperty::find()->where(['valuation_id' => $id])->one();
        $current_date = date('Y-m-d');
        $current_date_1_year = date('Y-m-d', strtotime($current_date . ' -1 year'));

        $autolisting_count = 0;
        $mode = 0;

        $valuation = $this->findModel($id);

        $model = ValuationEnquiry::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1])->one();
        if ($valuation->valuation_status != 5 && $valuation->pre_summary == 0) {
            if ($model !== null) {
                // $model->date_from = $current_date_1_year;

            } else {
                //  $model = new ValuationEnquiry();
                $model = new ValuationEnquiry();
                $model->date_from = $current_date_1_year;
                $model->new_from_date = $current_date_1_year;
                $model->date_to = $current_date;
                $model->building_info = $valuation->building_info;
                $model->valuation_id = $id;
                $model->bedroom_from = $InspectProperty->no_of_bedrooms;
                $model->bedroom_to = $InspectProperty->no_of_bedrooms;
                $model->type = 'sold';
                $model->search_type = 0;
                $model->save();

                $model = new ValuationEnquiry();
                $model->date_from = $current_date_1_year;
                $model->new_from_date = $current_date_1_year;
                $model->date_to = $current_date;
                $model->building_info = $valuation->building_info;
                $model->valuation_id = $id;
                $model->bedroom_from = $InspectProperty->no_of_bedrooms;
                $model->bedroom_to = $InspectProperty->no_of_bedrooms;
                $model->type = 'sold';
                $model->search_type = 1;
                $model->save();
            }




            $this->actionAutolistingValuer($id, 'sold');
        }


        //Step 2nd Selection (step 11)
        $selected_data = ArrayHelper::map(\app\models\ValuationSelectedLists::find()
            ->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');
        $selected_data_auto = ArrayHelper::map(\app\models\ValuationSelectedListsAuto::find()
            ->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');


        $totalResults = (new \yii\db\Query())
            ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) avg_listings_price_size, MIN(listings_price) as min_price, MIN(price_per_sqt) as min_price_sqt, MAX(listings_price) as max_price, MAX(price_per_sqt) as max_price_sqt , FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
            ->from('sold_transaction')
            ->where(['id' => $selected_data])
            ->all();





        //Average and calculations
        if ($valuation->valuation_status != 5 && $valuation->pre_summary == 0) {
            if (!empty($totalResults) && $totalResults <> null) {
                if (isset($totalResults[0]['avg_bedrooms'])) {
                    ValuationListCalculation::deleteAll(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 0]);
                    $calculations = new ValuationListCalculation();
                    $calculations->avg_bedrooms = round($totalResults[0]['avg_bedrooms']);
                    $calculations->avg_land_size = round($totalResults[0]['avg_land_size']);
                    $calculations->built_up_area_size = round($totalResults[0]['built_up_area_size']);
                    $calculations->avg_listings_price_size = round($totalResults[0]['avg_listings_price_size']);
                    $calculations->min_price = round($totalResults[0]['min_price']);
                    $calculations->max_price = round($totalResults[0]['max_price']);
                    $calculations->min_price_sqt = round($totalResults[0]['min_price_sqt']);
                    $calculations->max_price_sqt = round($totalResults[0]['max_price_sqt']);
                    $calculations->avg_listing_date = date('Y-m-d', strtotime($totalResults[0]['avg_listing_date']));
                    $calculations->avg_psf = round($calculations->avg_listings_price_size / $calculations->built_up_area_size);
                    $calculations->avg_gross_yield = 0;
                    $calculations->type = 'sold';
                    $calculations->valuation_id = $id;
                    $calculations->search_type = 0;
                    $calculations->save();

                    ValuationListCalculation::deleteAll(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1]);
                    $calculations_v = new ValuationListCalculation();
                    $calculations_v->avg_bedrooms = round($totalResults[0]['avg_bedrooms']);
                    $calculations_v->avg_land_size = round($totalResults[0]['avg_land_size']);
                    $calculations_v->built_up_area_size = round($totalResults[0]['built_up_area_size']);
                    $calculations_v->avg_listings_price_size = round($totalResults[0]['avg_listings_price_size']);
                    $calculations_v->min_price = round($totalResults[0]['min_price']);
                    $calculations_v->max_price = round($totalResults[0]['max_price']);
                    $calculations_v->min_price_sqt = round($totalResults[0]['min_price_sqt']);
                    $calculations_v->max_price_sqt = round($totalResults[0]['max_price_sqt']);
                    $calculations_v->avg_listing_date = date('Y-m-d', strtotime($totalResults[0]['avg_listing_date']));
                    $calculations_v->avg_psf = round($calculations_v->avg_listings_price_size / $calculations_v->built_up_area_size);
                    $calculations_v->avg_gross_yield = 0;
                    $calculations_v->type = 'sold';
                    $calculations_v->valuation_id = $id;
                    $calculations_v->search_type = 1;
                    $calculations_v->save();

                    ValuationListCalculation::deleteAll(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 2]);
                    $calculations_v = new ValuationListCalculation();
                    $calculations_v->avg_bedrooms = round($totalResults[0]['avg_bedrooms']);
                    $calculations_v->avg_land_size = round($totalResults[0]['avg_land_size']);
                    $calculations_v->built_up_area_size = round($totalResults[0]['built_up_area_size']);
                    $calculations_v->avg_listings_price_size = round($totalResults[0]['avg_listings_price_size']);
                    $calculations_v->min_price = round($totalResults[0]['min_price']);
                    $calculations_v->max_price = round($totalResults[0]['max_price']);
                    $calculations_v->min_price_sqt = round($totalResults[0]['min_price_sqt']);
                    $calculations_v->max_price_sqt = round($totalResults[0]['max_price_sqt']);
                    $calculations_v->avg_listing_date = date('Y-m-d', strtotime($totalResults[0]['avg_listing_date']));
                    $calculations_v->avg_psf = round($calculations_v->avg_listings_price_size / $calculations_v->built_up_area_size);
                    $calculations_v->avg_gross_yield = 0;
                    $calculations_v->type = 'sold';
                    $calculations_v->valuation_id = $id;
                    $calculations_v->search_type = 2;
                    $calculations_v->save();
                }

            }
        }

        $select_calculations = \app\models\ValuationListCalculation::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1])->one();


        // $sold_selected_records = \app\models\ValuationSelectedLists::find()->where(['selected_id' => $selected_data,'type'=> 'sold'])->all();

        // selected_data
        $sold_selected_records = \app\models\ValuationEnquiry::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1])->one();
        $sold_selected_records_0 = \app\models\ValuationEnquiry::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 0])->one();


        $listing_filter = array();
        if (!empty($sold_selected_records) && $sold_selected_records) {

            $listing_filter['SoldTransactionSearch']['date_from'] = $sold_selected_records->date_from;
            $listing_filter['SoldTransactionSearch']['date_to'] = $sold_selected_records->date_to;
            $listing_filter['SoldTransactionSearch']['building_info_data'] = $sold_selected_records->building_info;
            $listing_filter['SoldTransactionSearch']['property_id'] = $sold_selected_records->property_id;
            $listing_filter['SoldTransactionSearch']['property_category'] = $sold_selected_records->property_category;
            $listing_filter['SoldTransactionSearch']['bedroom_from'] = $sold_selected_records->bedroom_from;
            $listing_filter['SoldTransactionSearch']['bedroom_to'] = $sold_selected_records->bedroom_to;
            $listing_filter['SoldTransactionSearch']['landsize_from'] = $sold_selected_records->landsize_from;
            $listing_filter['SoldTransactionSearch']['landsize_to'] = $sold_selected_records->landsize_to;
            $listing_filter['SoldTransactionSearch']['bua_from'] = $sold_selected_records->bua_from;
            $listing_filter['SoldTransactionSearch']['bua_to'] = $sold_selected_records->bua_to;
            $listing_filter['SoldTransactionSearch']['price_from'] = $sold_selected_records->price_from;
            $listing_filter['SoldTransactionSearch']['price_to'] = $sold_selected_records->price_to;
            $listing_filter['SoldTransactionSearch']['price_psf_from'] = $sold_selected_records->price_psf_from;
            $listing_filter['SoldTransactionSearch']['price_psf_to'] = $sold_selected_records->price_psf_to;
        } else {
            $valuation = $this->findModel($id);
            $listing_filter['SoldTransactionSearch']['date_from'] = '2020-01-01';
            $listing_filter['SoldTransactionSearch']['date_to'] = date('Y-m-d');
            $listing_filter['SoldTransactionSearch']['building_info_data'] = $valuation->building_info;
        }


        $selected_records_display = $sold_selected_records_0;

        if ($selected_records_display <> null & !empty($selected_records_display)) {
            $building_info_data = '';

            if ($selected_records_display->building_info <> null) {
                $buildings = explode(",", $sold_selected_records->building_info);
                $all_buildings_titles = Buildings::find()->where(['id' => $buildings])->all();

                if ($all_buildings_titles <> null && !empty($all_buildings_titles)) {
                    foreach ($all_buildings_titles as $title) {
                        $building_info_data .= $title['title'] . ',&nbsp';
                    }

                }

            }
            $selected_records_display->building_info = $building_info_data;
            $property_data = Properties::find()->where(['id' => $sold_selected_records->property_id])->one();
            if ($property_data <> null && !empty($property_data)) {
                $selected_records_display->property_id = $property_data->title;
            }
            $property_data = Properties::find()->where(['id' => $sold_selected_records->property_id])->one();
            if ($selected_records_display->property_category <> null) {
                $selected_records_display->property_category = Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$sold_selected_records_display->property_category];
            }
        }

        $valuation = $this->findModel($id);
        // $searchModel = new ListingsTransactionsSearch();
        $searchModel = new SoldTransactionSearch();
        $dataProvider = $searchModel->searchvaluation($listing_filter);
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;






        //Step 3rd derive function (step 12)
        $model = $this->findModel($id);

        $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $id])->one();

        $location_attributes_value = Yii::$app->appHelperFunctions->getLocationAttributes($inspection_data);
        $view_attributes_value = Yii::$app->appHelperFunctions->getViewAttributes($inspection_data);

        $list_calculate_data = \app\models\ValuationListCalculation::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1])->one();
        $weightages = \app\models\Weightages::find()->where(['property_type' => $model->property_id])->one();


        $buildongs_info = \app\models\Buildings::find()->where(['id' => $model->building_info])->one();
        $cost_info = \app\models\CostDetails::find()->where(['valuation_id' => $id])->one();
        $developer_margin = \app\models\ValuationDeveloperDriveMargin::find()->where(['valuation_id' => $id])->one();

        $weightages_required_month = array();
        $weightages_required_year = array();

        $weightages_required_month[1] = $weightages->less_than_1_month;
        $weightages_required_month[2] = $weightages->less_than_2_month;
        $weightages_required_month[3] = $weightages->less_than_3_month;
        $weightages_required_month[4] = $weightages->less_than_4_month;
        $weightages_required_month[5] = $weightages->less_than_5_month;
        $weightages_required_month[6] = $weightages->less_than_6_month;
        $weightages_required_month[7] = $weightages->less_than_7_month;
        $weightages_required_month[8] = $weightages->less_than_8_month;
        $weightages_required_month[9] = $weightages->less_than_9_month;
        $weightages_required_month[10] = $weightages->less_than_10_month;
        $weightages_required_month[11] = $weightages->less_than_11_month;
        $weightages_required_month[12] = $weightages->less_than_12_month;

        $weightages_required_year[2] = $weightages->less_than_2_year;
        $weightages_required_year[3] = $weightages->less_than_3_year;
        $weightages_required_year[4] = $weightages->less_than_4_year;
        $weightages_required_year[5] = $weightages->less_than_5_year;
        $weightages_required_year[6] = $weightages->less_than_6_year;
        $weightages_required_year[7] = $weightages->less_than_7_year;
        $weightages_required_year[8] = $weightages->less_than_8_year;
        $weightages_required_year[9] = $weightages->less_than_9_year;
        $weightages_required_year[10] = $weightages->less_than_10_year;

        $avg_date = date_create($list_calculate_data->avg_listing_date);
        $current_date = date('Y-m-d');
        $current_date = date_create($current_date);
        $diff = date_diff($current_date, $avg_date);
        $difference_in_days = $diff->format("%a");
        $date_weightages = 0;

        $years_remaining = intval($difference_in_days / 365);
        $days_remaining = $difference_in_days % 365;

        if ($years_remaining > 1) {
            if ($days_remaining > 0) {

                $date_weightages = $weightages_required_year[$years_remaining + 1];
            } else {
                $date_weightages = $weightages_required_year[$years_remaining];
            }
        } else {
            $month_no = intval($days_remaining / 30);
            $days_remaining_month = $days_remaining % 30;
            if ($days_remaining_month > 0) {
                $date_weightages = $weightages_required_month[$month_no + 1];
            } else {
                $date_weightages = $weightages_required_month[$month_no];
            }
        }

        $drive_margin_saved = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1])->one();
        $drive_margin_saved_m = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 0])->one();
        $drive_margin_saved_r = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 2])->one();
        /*if ($list_calculate_data->built_up_area_size > 0) {
            $landsize = round(($list_calculate_data->avg_land_size / $list_calculate_data->built_up_area_size), 4);
        } else {
            $landsize = 0;
        }
        $difference_land_size = ($landsize * $inspection_data->built_up_area) - $model->land_size;
        if ($list_calculate_data->avg_land_size > 0) {
            $difference_land_size_percentage = round(abs(($difference_land_size / $list_calculate_data->avg_land_size) * 100), 2);
        } else {
            $difference_land_size_percentage = 0;
        }*/

        $simple_land_check = 0;
        $allow_properties_simple = [4, 5, 29, 39, 44, 46, 48, 49, 50, 51, 52, 53, 5, 55, 56];
        if (in_array($valuation->property_id, $allow_properties_simple)) {
            $simple_land_check = 1;
        }
        if($simple_land_check == 1){
            $gfa = $inspection_data->gfa;
            if($gfa <= 0) {
                $gfa = round($inspection_data->built_up_area / $model->land_size);
            }
            if ($list_calculate_data->avg_land_size > 0) {
                $gfa_avg = round($list_calculate_data->built_up_area_size / $list_calculate_data->avg_land_size);
            }else{
                $gfa_avg = 0;
            }
            $difference_land_size_percentage = $gfa - $gfa_avg;
        }else {


            $difference_land_size = $model->land_size - $list_calculate_data->avg_land_size;
            if ($list_calculate_data->avg_land_size > 0) {
                $difference_land_size_percentage = round(abs(($difference_land_size / $list_calculate_data->avg_land_size) * 100), 2);
            } else {
                $difference_land_size_percentage = 0;
            }
        }

        $difference_bua = $inspection_data->built_up_area - $list_calculate_data->built_up_area_size;
        if ($list_calculate_data->built_up_area_size > 0) {
            $difference_bua_percentage = round(abs(($difference_bua / $list_calculate_data->built_up_area_size) * 100), 2);
        } else {
            $difference_bua_percentage = 0;
        }
        if ($valuation->valuation_status != 5 && $valuation->pre_summary == 0) {
            if (empty($drive_margin_saved) && $drive_margin_saved == null) {

                //  $selected_data_last_step = ArrayHelper::map(\app\models\ValuationSelectedLists::find()->where(['valuation_id' => $id, 'type' => 'sold'])->all(), 'id', 'selected_id');
                $selected_data_last_step = ArrayHelper::map(\app\models\ValuationSelectedLists::find()
                    ->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1])
                    ->limit(10)
                    ->orderBy(['latest' => SORT_DESC])
                    ->all(), 'id', 'selected_id');
                $totalResults_average = (new \yii\db\Query())
                    ->select('AVG(no_of_bedrooms) as avg_no_of_bedrooms')
                    ->from('sold_transaction')
                    ->where(['id' => $selected_data_last_step])
                    ->all();

                $drive_margin_saved_data = new ValuationDriveMv();

                $drive_margin_saved_data->average_location = $weightages->location_avg;
                $drive_margin_saved_data->average_age = $buildongs_info->estimated_age;
                $drive_margin_saved_data->average_tenure = $weightages->tenure_avg;
                $drive_margin_saved_data->average_view = $weightages->view_avg;
                $drive_margin_saved_data->average_finished_status = $weightages->finished_status_avg;
                $drive_margin_saved_data->average_property_condition = $weightages->property_condition_avg;
                $drive_margin_saved_data->average_upgrades = $weightages->upgrades_avg;
                // $drive_margin_saved_data->average_furnished = 3;
                $drive_margin_saved_data->average_property_exposure = $weightages->property_exposure_avg;
                $drive_margin_saved_data->average_property_placement = $weightages->property_exposure_avg;
                // $drive_margin_saved_data->average_full_building_floors = (isset($model->floor_number) ? round($model->floor_number, 2) : 0);;
                $drive_margin_saved_data->average_full_building_floors = (isset($inspection_data->full_building_floors) ? round($inspection_data->full_building_floors / 2, 2) : 0);
                $drive_margin_saved_data->average_number_of_levels = $weightages->number_of_levels_avg;
                $drive_margin_saved_data->average_no_of_bedrooms = (isset($totalResults_average[0]['avg_no_of_bedrooms']) ? round($totalResults_average[0]['avg_no_of_bedrooms'], 2) : 0);
                $drive_margin_saved_data->average_parking_space = $weightages->parking_space_avg;
                $drive_margin_saved_data->average_pool = $weightages->pool_avg;
                $drive_margin_saved_data->average_landscaping = $weightages->landscaping_avg;
                $drive_margin_saved_data->average_white_goods = $weightages->white_goods_avg;
                $drive_margin_saved_data->average_utilities_connected = $weightages->utilities_connected_avg;
                $drive_margin_saved_data->average_developer_margin = $weightages->developer_margin_avg;
                //  $drive_margin_saved_data->average_land_size = $weightages->land_size_avg;
                $drive_margin_saved_data->average_land_size = $list_calculate_data->avg_land_size;
                $drive_margin_saved_data->average_balcony_size = $weightages->balcony_size_avg;
                $drive_margin_saved_data->average_built_up_area = $list_calculate_data->built_up_area_size;
                $drive_margin_saved_data->average_date = $list_calculate_data->avg_listing_date;
                $drive_margin_saved_data->average_sale_price = $list_calculate_data->avg_listings_price_size;
                $drive_margin_saved_data->average_psf = $list_calculate_data->avg_psf;

                $drive_margin_saved_data->weightage_location = $weightages->location;
                $drive_margin_saved_data->weightage_age = $weightages->age;
                $drive_margin_saved_data->weightage_tenure = $weightages->tenure;
                $drive_margin_saved_data->weightage_view = $weightages->view;
                $drive_margin_saved_data->weightage_finished_status = $weightages->finishing_status;
                $drive_margin_saved_data->weightage_property_condition = $weightages->upgrades;
                $drive_margin_saved_data->weightage_upgrades = $weightages->quality;
                $drive_margin_saved_data->weightage_furnished = $weightages->furnished;
                $drive_margin_saved_data->weightage_property_exposure = $weightages->property_exposure;
                $drive_margin_saved_data->weightage_property_placement = $weightages->property_placement;
                $drive_margin_saved_data->weightage_full_building_floors = $weightages->floor;
                $drive_margin_saved_data->weightage_number_of_levels = $weightages->number_of_levels;
                $drive_margin_saved_data->weightage_no_of_bedrooms = $weightages->bedrooom;
                $drive_margin_saved_data->weightage_parking_space = $weightages->parking;
                $drive_margin_saved_data->weightage_pool = $weightages->pool;
                $drive_margin_saved_data->weightage_landscaping = $weightages->landscape;
                $drive_margin_saved_data->weightage_white_goods = $weightages->white_goods;
                $drive_margin_saved_data->weightage_utilities_connected = $weightages->utilities_connected;
                $drive_margin_saved_data->weightage_developer_margin = $developer_margin->developer_margin_percentage;
                // $drive_margin_saved_data->weightage_land_size = Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);


                if($simple_land_check == 1) {
                    $drive_margin_saved_data->weightage_land_size = Yii::$app->appHelperFunctions->getGfaweightages($difference_land_size_percentage);
                }else{
                    $drive_margin_saved_data->weightage_land_size = Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);
                }

                $drive_margin_saved_data->weightage_balcony_size = $weightages->balcony_size;
                $drive_margin_saved_data->weightage_built_up_area = Yii::$app->appHelperFunctions->getBuaweightages($difference_bua_percentage);
                $drive_margin_saved_data->weightage_date = $date_weightages;
                $drive_margin_saved_data->valuation_id = $id;
                $drive_margin_saved_data->type = 'sold';
                $drive_margin_saved_data->search_type = 0;
                if (!$drive_margin_saved_data->save()) {
                }

                $drive_margin_saved_data = new ValuationDriveMv();

                $drive_margin_saved_data->average_location = $weightages->location_avg;
                $drive_margin_saved_data->average_age = $buildongs_info->estimated_age;
                $drive_margin_saved_data->average_tenure = $weightages->tenure_avg;
                $drive_margin_saved_data->average_view = $weightages->view_avg;
                $drive_margin_saved_data->average_finished_status = $weightages->finished_status_avg;
                $drive_margin_saved_data->average_property_condition = $weightages->property_condition_avg;
                $drive_margin_saved_data->average_upgrades = $weightages->upgrades_avg;
                // $drive_margin_saved_data->average_furnished = 3;
                $drive_margin_saved_data->average_property_exposure = $weightages->property_exposure_avg;
                $drive_margin_saved_data->average_property_placement = $weightages->property_exposure_avg;
                // $drive_margin_saved_data->average_full_building_floors = (isset($model->floor_number) ? round($model->floor_number, 2) : 0);;
                $drive_margin_saved_data->average_full_building_floors = (isset($inspection_data->full_building_floors) ? round($inspection_data->full_building_floors / 2, 2) : 0);
                $drive_margin_saved_data->average_number_of_levels = $weightages->number_of_levels_avg;
                $drive_margin_saved_data->average_no_of_bedrooms = (isset($totalResults_average[0]['avg_no_of_bedrooms']) ? round($totalResults_average[0]['avg_no_of_bedrooms'], 2) : 0);
                ;
                $drive_margin_saved_data->average_parking_space = $weightages->parking_space_avg;
                $drive_margin_saved_data->average_pool = $weightages->pool_avg;
                $drive_margin_saved_data->average_landscaping = $weightages->landscaping_avg;
                $drive_margin_saved_data->average_white_goods = $weightages->white_goods_avg;
                $drive_margin_saved_data->average_utilities_connected = $weightages->utilities_connected_avg;
                $drive_margin_saved_data->average_developer_margin = $weightages->developer_margin_avg;
                $drive_margin_saved_data->average_land_size = $list_calculate_data->avg_land_size;
                $drive_margin_saved_data->average_balcony_size = $weightages->balcony_size_avg;
                ;
                $drive_margin_saved_data->average_built_up_area = $list_calculate_data->built_up_area_size;
                $drive_margin_saved_data->average_date = $list_calculate_data->avg_listing_date;
                $drive_margin_saved_data->average_sale_price = $list_calculate_data->avg_listings_price_size;
                ;
                $drive_margin_saved_data->average_psf = $list_calculate_data->avg_psf;
                ;

                $drive_margin_saved_data->weightage_location = $weightages->location;
                $drive_margin_saved_data->weightage_age = $weightages->age;
                $drive_margin_saved_data->weightage_tenure = $weightages->tenure;
                $drive_margin_saved_data->weightage_view = $weightages->view;
                $drive_margin_saved_data->weightage_finished_status = $weightages->finishing_status;
                $drive_margin_saved_data->weightage_property_condition = $weightages->upgrades;
                $drive_margin_saved_data->weightage_upgrades = $weightages->quality;
                $drive_margin_saved_data->weightage_furnished = $weightages->furnished;
                $drive_margin_saved_data->weightage_property_exposure = $weightages->property_exposure;
                $drive_margin_saved_data->weightage_property_placement = $weightages->property_placement;
                $drive_margin_saved_data->weightage_full_building_floors = $weightages->floor;
                $drive_margin_saved_data->weightage_number_of_levels = $weightages->number_of_levels;
                $drive_margin_saved_data->weightage_no_of_bedrooms = $weightages->bedrooom;
                $drive_margin_saved_data->weightage_parking_space = $weightages->parking;
                $drive_margin_saved_data->weightage_pool = $weightages->pool;
                $drive_margin_saved_data->weightage_landscaping = $weightages->landscape;
                $drive_margin_saved_data->weightage_white_goods = $weightages->white_goods;
                $drive_margin_saved_data->weightage_utilities_connected = $weightages->utilities_connected;
                $drive_margin_saved_data->weightage_developer_margin = $developer_margin->developer_margin_percentage;
                if($simple_land_check == 1) {
                    $drive_margin_saved_data->weightage_land_size = Yii::$app->appHelperFunctions->getGfaweightages($difference_land_size_percentage);
                }else{
                    $drive_margin_saved_data->weightage_land_size = Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);
                }
                $drive_margin_saved_data->weightage_balcony_size = $weightages->balcony_size;
                $drive_margin_saved_data->weightage_built_up_area = Yii::$app->appHelperFunctions->getBuaweightages($difference_bua_percentage);
                $drive_margin_saved_data->weightage_date = $date_weightages;
                $drive_margin_saved_data->valuation_id = $id;
                $drive_margin_saved_data->type = 'sold';
                $drive_margin_saved_data->search_type = 1;
                if (!$drive_margin_saved_data->save()) {
                }


                $drive_margin_saved_data = new ValuationDriveMv();

                $drive_margin_saved_data->average_location = $weightages->location_avg;
                $drive_margin_saved_data->average_age = $buildongs_info->estimated_age;
                $drive_margin_saved_data->average_tenure = $weightages->tenure_avg;
                $drive_margin_saved_data->average_view = $weightages->view_avg;
                $drive_margin_saved_data->average_finished_status = $weightages->finished_status_avg;
                $drive_margin_saved_data->average_property_condition = $weightages->property_condition_avg;
                $drive_margin_saved_data->average_upgrades = $weightages->upgrades_avg;
                // $drive_margin_saved_data->average_furnished = 3;
                $drive_margin_saved_data->average_property_exposure = $weightages->property_exposure_avg;
                $drive_margin_saved_data->average_property_placement = $weightages->property_exposure_avg;
                // $drive_margin_saved_data->average_full_building_floors = (isset($model->floor_number) ? round($model->floor_number, 2) : 0);;
                $drive_margin_saved_data->average_full_building_floors = (isset($inspection_data->full_building_floors) ? round($inspection_data->full_building_floors / 2, 2) : 0);
                $drive_margin_saved_data->average_number_of_levels = $weightages->number_of_levels_avg;
                $drive_margin_saved_data->average_no_of_bedrooms = (isset($totalResults_average[0]['avg_no_of_bedrooms']) ? round($totalResults_average[0]['avg_no_of_bedrooms'], 2) : 0);
                ;
                $drive_margin_saved_data->average_parking_space = $weightages->parking_space_avg;
                $drive_margin_saved_data->average_pool = $weightages->pool_avg;
                $drive_margin_saved_data->average_landscaping = $weightages->landscaping_avg;
                $drive_margin_saved_data->average_white_goods = $weightages->white_goods_avg;
                $drive_margin_saved_data->average_utilities_connected = $weightages->utilities_connected_avg;
                $drive_margin_saved_data->average_developer_margin = $weightages->developer_margin_avg;
                $drive_margin_saved_data->average_land_size = $list_calculate_data->avg_land_size;
                $drive_margin_saved_data->average_balcony_size = $weightages->balcony_size_avg;
                ;
                $drive_margin_saved_data->average_built_up_area = $list_calculate_data->built_up_area_size;
                $drive_margin_saved_data->average_date = $list_calculate_data->avg_listing_date;
                $drive_margin_saved_data->average_sale_price = $list_calculate_data->avg_listings_price_size;
                ;
                $drive_margin_saved_data->average_psf = $list_calculate_data->avg_psf;
                ;

                $drive_margin_saved_data->weightage_location = $weightages->location;
                $drive_margin_saved_data->weightage_age = $weightages->age;
                $drive_margin_saved_data->weightage_tenure = $weightages->tenure;
                $drive_margin_saved_data->weightage_view = $weightages->view;
                $drive_margin_saved_data->weightage_finished_status = $weightages->finishing_status;
                $drive_margin_saved_data->weightage_property_condition = $weightages->upgrades;
                $drive_margin_saved_data->weightage_upgrades = $weightages->quality;
                $drive_margin_saved_data->weightage_furnished = $weightages->furnished;
                $drive_margin_saved_data->weightage_property_exposure = $weightages->property_exposure;
                $drive_margin_saved_data->weightage_property_placement = $weightages->property_placement;
                $drive_margin_saved_data->weightage_full_building_floors = $weightages->floor;
                $drive_margin_saved_data->weightage_number_of_levels = $weightages->number_of_levels;
                $drive_margin_saved_data->weightage_no_of_bedrooms = $weightages->bedrooom;
                $drive_margin_saved_data->weightage_parking_space = $weightages->parking;
                $drive_margin_saved_data->weightage_pool = $weightages->pool;
                $drive_margin_saved_data->weightage_landscaping = $weightages->landscape;
                $drive_margin_saved_data->weightage_white_goods = $weightages->white_goods;
                $drive_margin_saved_data->weightage_utilities_connected = $weightages->utilities_connected;
                $drive_margin_saved_data->weightage_developer_margin = $developer_margin->developer_margin_percentage;
                if($simple_land_check == 1) {
                    $drive_margin_saved_data->weightage_land_size = Yii::$app->appHelperFunctions->getGfaweightages($difference_land_size_percentage);
                }else{
                    $drive_margin_saved_data->weightage_land_size = Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);
                }
                $drive_margin_saved_data->weightage_balcony_size = $weightages->balcony_size;
                $drive_margin_saved_data->weightage_built_up_area = Yii::$app->appHelperFunctions->getBuaweightages($difference_bua_percentage);
                $drive_margin_saved_data->weightage_date = $date_weightages;
                $drive_margin_saved_data->valuation_id = $id;
                $drive_margin_saved_data->type = 'sold';
                $drive_margin_saved_data->search_type = 2;
                if (!$drive_margin_saved_data->save()) {
                }
                $drive_margin_saved = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1])->one();
                $drive_margin_saved_m = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 0])->one();
                $drive_margin_saved_r = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 2])->one();
            }
        }

        $preCalculationArray = array();

        $preCalculationArray['location']['average'] = $drive_margin_saved->average_location;
        // $preCalculationArray['location']['subject_property'] = $inspection_data->location;
        $preCalculationArray['location']['subject_property'] = $location_attributes_value;
        $preCalculationArray['location']['difference'] = $preCalculationArray['location']['subject_property'] - $preCalculationArray['location']['average'];
        $preCalculationArray['location']['standard_weightage'] = $weightages->location;
        $preCalculationArray['location']['change_weightage'] = $drive_margin_saved->weightage_location;
        $preCalculationArray['location']['adjustments'] = round(($preCalculationArray['location']['difference'] * ($preCalculationArray['location']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['age']['average'] = $drive_margin_saved->average_age;
        $preCalculationArray['age']['subject_property'] = $inspection_data->estimated_age;
        $preCalculationArray['age']['difference'] = $preCalculationArray['age']['average'] - $preCalculationArray['age']['subject_property'];
        $preCalculationArray['age']['standard_weightage'] = $weightages->age;
        $preCalculationArray['age']['change_weightage'] = $drive_margin_saved->weightage_age;
        $preCalculationArray['age']['adjustments'] = round(($preCalculationArray['age']['difference'] * ($preCalculationArray['age']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['tenure']['average'] = $drive_margin_saved->average_tenure;
        $preCalculationArray['tenure']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure]];
        $preCalculationArray['tenure']['difference'] = $preCalculationArray['tenure']['subject_property'] - $preCalculationArray['tenure']['average'];
        $preCalculationArray['tenure']['standard_weightage'] = $weightages->tenure;
        $preCalculationArray['tenure']['change_weightage'] = $drive_margin_saved->weightage_tenure;
        $preCalculationArray['tenure']['adjustments'] = round(($preCalculationArray['tenure']['difference'] * ($preCalculationArray['tenure']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['view']['average'] = $drive_margin_saved->average_view;
        // $preCalculationArray['view']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->otherPropertyViewListArr[$inspection_data->view]];
        $preCalculationArray['view']['subject_property'] = $view_attributes_value;
        $preCalculationArray['view']['difference'] = $preCalculationArray['view']['subject_property'] - $preCalculationArray['view']['average'];
        $preCalculationArray['view']['standard_weightage'] = $weightages->view;
        $preCalculationArray['view']['change_weightage'] = $drive_margin_saved->weightage_view;
        $preCalculationArray['view']['adjustments'] = round(($preCalculationArray['view']['difference'] * ($preCalculationArray['view']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['finished_status']['average'] = $drive_margin_saved->average_finished_status;
        $preCalculationArray['finished_status']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->finished_status];
        $preCalculationArray['finished_status']['difference'] = $preCalculationArray['finished_status']['subject_property'] - $preCalculationArray['finished_status']['average'];
        $preCalculationArray['finished_status']['standard_weightage'] = $weightages->furnished;
        $preCalculationArray['finished_status']['change_weightage'] = $drive_margin_saved->weightage_finished_status;
        $preCalculationArray['finished_status']['adjustments'] = round(($preCalculationArray['finished_status']['difference'] * ($preCalculationArray['finished_status']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['property_condition']['average'] = $drive_margin_saved->average_property_condition;
        $preCalculationArray['property_condition']['subject_property'] = Yii::$app->appHelperFunctions->propertyConditionRatingtArr[$inspection_data->property_condition];
        $preCalculationArray['property_condition']['difference'] = $preCalculationArray['property_condition']['subject_property'] - $preCalculationArray['property_condition']['average'];
        $preCalculationArray['property_condition']['standard_weightage'] = $weightages->upgrades;
        $preCalculationArray['property_condition']['change_weightage'] = $drive_margin_saved->weightage_property_condition;
        $preCalculationArray['property_condition']['adjustments'] = round(($preCalculationArray['property_condition']['difference'] * ($preCalculationArray['property_condition']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));


        $preCalculationArray['upgrades']['average'] = $drive_margin_saved->average_upgrades;
        $preCalculationArray['upgrades']['subject_property'] = $model->valuationConfiguration->over_all_upgrade;
        $preCalculationArray['upgrades']['difference'] = $preCalculationArray['upgrades']['subject_property'] - $preCalculationArray['upgrades']['average'];
        $preCalculationArray['upgrades']['standard_weightage'] = $weightages->quality;
        $preCalculationArray['upgrades']['change_weightage'] = $drive_margin_saved->weightage_upgrades;
        $preCalculationArray['upgrades']['adjustments'] = round(($preCalculationArray['upgrades']['difference'] * ($preCalculationArray['upgrades']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));


        $preCalculationArray['property_exposure']['average'] = $drive_margin_saved->average_property_exposure;
        $preCalculationArray['property_exposure']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->propertyExposureListArr[$inspection_data->property_exposure]];
        $preCalculationArray['property_exposure']['difference'] = $preCalculationArray['property_exposure']['subject_property'] - $preCalculationArray['property_exposure']['average'];
        $preCalculationArray['property_exposure']['standard_weightage'] = $weightages->property_exposure;
        $preCalculationArray['property_exposure']['change_weightage'] = $drive_margin_saved->weightage_property_exposure;
        $preCalculationArray['property_exposure']['adjustments'] = round(($preCalculationArray['property_exposure']['difference'] * ($preCalculationArray['property_exposure']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['property_placement']['average'] = $drive_margin_saved->average_property_placement;
        $preCalculationArray['property_placement']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->propertyPlacementListArr[$inspection_data->property_placement]];
        $preCalculationArray['property_placement']['difference'] = $preCalculationArray['property_placement']['subject_property'] - $preCalculationArray['property_placement']['average'];
        $preCalculationArray['property_placement']['standard_weightage'] = $weightages->property_placement;
        $preCalculationArray['property_placement']['change_weightage'] = $drive_margin_saved->weightage_property_placement;
        $preCalculationArray['property_placement']['adjustments'] = round(($preCalculationArray['property_placement']['difference'] * ($preCalculationArray['property_placement']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['floors_adjustment']['average'] = $drive_margin_saved->average_full_building_floors;
        $preCalculationArray['floors_adjustment']['subject_property'] = (isset($model->floor_number) ? $model->floor_number : 0);
        $preCalculationArray['floors_adjustment']['difference'] = $preCalculationArray['floors_adjustment']['subject_property'] - $preCalculationArray['floors_adjustment']['average'];
        $preCalculationArray['floors_adjustment']['standard_weightage'] = $weightages->floor;
        $preCalculationArray['floors_adjustment']['change_weightage'] = $drive_margin_saved->weightage_full_building_floors;
        $preCalculationArray['floors_adjustment']['adjustments'] = round(($preCalculationArray['floors_adjustment']['difference'] * ($preCalculationArray['floors_adjustment']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['number_of_levels']['average'] = $drive_margin_saved->average_number_of_levels;
        $preCalculationArray['number_of_levels']['subject_property'] = $inspection_data->number_of_levels;
        $preCalculationArray['number_of_levels']['difference'] = $preCalculationArray['number_of_levels']['subject_property'] - $preCalculationArray['number_of_levels']['average'];
        $preCalculationArray['number_of_levels']['standard_weightage'] = $weightages->number_of_levels;
        $preCalculationArray['number_of_levels']['change_weightage'] = $drive_margin_saved->weightage_number_of_levels;
        $preCalculationArray['number_of_levels']['adjustments'] = round(($preCalculationArray['number_of_levels']['difference'] * ($preCalculationArray['number_of_levels']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['no_of_bedrooms']['average'] = $drive_margin_saved->average_no_of_bedrooms;
        $preCalculationArray['no_of_bedrooms']['subject_property'] = $inspection_data->no_of_bedrooms;
        $preCalculationArray['no_of_bedrooms']['difference'] = $preCalculationArray['no_of_bedrooms']['subject_property'] - $preCalculationArray['no_of_bedrooms']['average'];
        $preCalculationArray['no_of_bedrooms']['standard_weightage'] = $weightages->bedrooom;
        $preCalculationArray['no_of_bedrooms']['change_weightage'] = $drive_margin_saved->weightage_no_of_bedrooms;
        $preCalculationArray['no_of_bedrooms']['adjustments'] = round(($preCalculationArray['no_of_bedrooms']['difference'] * ($preCalculationArray['no_of_bedrooms']['standard_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['parking_space']['average'] = $drive_margin_saved->average_parking_space;
        $preCalculationArray['parking_space']['subject_property'] = $inspection_data->parking_floors;
        $preCalculationArray['parking_space']['difference'] = $preCalculationArray['parking_space']['subject_property'] - $preCalculationArray['parking_space']['average'];
        $preCalculationArray['parking_space']['standard_weightage'] = $weightages->parking;
        $preCalculationArray['parking_space']['change_weightage'] = $drive_margin_saved->weightage_parking_space;
        $preCalculationArray['parking_space']['adjustments'] = round(($cost_info->parking_price * $preCalculationArray['parking_space']['difference']) * ($preCalculationArray['parking_space']['change_weightage'] / 100), 2);


        $preCalculationArray['pool']['average'] = $drive_margin_saved->average_pool;
        $preCalculationArray['pool']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->pool];
        $preCalculationArray['pool']['difference'] = $preCalculationArray['pool']['subject_property'] - $preCalculationArray['pool']['average'];
        $preCalculationArray['pool']['standard_weightage'] = $weightages->pool;
        $preCalculationArray['pool']['change_weightage'] = $drive_margin_saved->weightage_pool;
        $preCalculationArray['pool']['adjustments'] = round(($cost_info->pool_price * $preCalculationArray['pool']['difference']) * ($preCalculationArray['pool']['change_weightage'] / 100), 2);

        $preCalculationArray['landscaping']['average'] = $drive_margin_saved->average_landscaping;
        $preCalculationArray['landscaping']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->landscaping];
        $preCalculationArray['landscaping']['difference'] = $preCalculationArray['landscaping']['subject_property'] - $preCalculationArray['landscaping']['average'];
        $preCalculationArray['landscaping']['standard_weightage'] = $weightages->landscape;
        $preCalculationArray['landscaping']['change_weightage'] = $drive_margin_saved->weightage_landscaping;
        ;
        $preCalculationArray['landscaping']['adjustments'] = round(($cost_info->landscape_price * $preCalculationArray['landscaping']['difference']) * ($preCalculationArray['landscaping']['change_weightage'] / 100), 2);

        $preCalculationArray['white_goods']['average'] = $drive_margin_saved->average_white_goods;
        $preCalculationArray['white_goods']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->white_goods];
        $preCalculationArray['white_goods']['difference'] = $preCalculationArray['white_goods']['subject_property'] - $preCalculationArray['white_goods']['average'];
        $preCalculationArray['white_goods']['standard_weightage'] = $weightages->white_goods;
        $preCalculationArray['white_goods']['change_weightage'] = $drive_margin_saved->weightage_white_goods;
        $preCalculationArray['white_goods']['change_weightage'] = $drive_margin_saved->weightage_white_goods;
        $preCalculationArray['white_goods']['adjustments'] = round(($cost_info->white_goods_price * $preCalculationArray['white_goods']['difference']) * ($preCalculationArray['white_goods']['change_weightage'] / 100), 2);

        $preCalculationArray['utilities_connected']['average'] = $drive_margin_saved->average_utilities_connected;
        $preCalculationArray['utilities_connected']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->utilities_connected];
        $preCalculationArray['utilities_connected']['difference'] = $preCalculationArray['utilities_connected']['subject_property'] - $preCalculationArray['utilities_connected']['average'];
        $preCalculationArray['utilities_connected']['standard_weightage'] = $weightages->utilities_connected;
        $preCalculationArray['utilities_connected']['change_weightage'] = $drive_margin_saved->weightage_utilities_connected;
        $preCalculationArray['utilities_connected']['adjustments'] = round(($cost_info->utilities_connected_price * $preCalculationArray['utilities_connected']['difference']) * ($preCalculationArray['utilities_connected']['change_weightage'] / 100), 2);

        $preCalculationArray['developer_margin']['average'] = 0;
        $preCalculationArray['developer_margin']['subject_property'] = 0;
        $preCalculationArray['developer_margin']['difference'] = 0;
        $preCalculationArray['developer_margin']['standard_weightage'] = 0;
        $preCalculationArray['developer_margin']['change_weightage'] = ($drive_margin_saved->weightage_developer_margin > 0) ? -$drive_margin_saved->weightage_developer_margin : $drive_margin_saved->weightage_developer_margin;
        $preCalculationArray['developer_margin']['adjustments'] = round((($preCalculationArray['developer_margin']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size), 2);
        /* if ($list_calculate_data->built_up_area_size > 0) {
             $landsize = round(($list_calculate_data->avg_land_size / $list_calculate_data->built_up_area_size), 4);
         } else {
             $landsize = 0;
         }*/
        $preCalculationArray['land_size']['average'] =  $drive_margin_saved->average_land_size;
        $preCalculationArray['land_size']['subject_property'] = $model->land_size;
        $preCalculationArray['land_size']['difference'] = $preCalculationArray['land_size']['subject_property'] - $preCalculationArray['land_size']['average'];
        $preCalculationArray['land_size']['standard_weightage'] = Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);
        $preCalculationArray['land_size']['change_weightage'] = $drive_margin_saved->weightage_land_size;
        $preCalculationArray['land_size']['adjustments'] = round(($preCalculationArray['land_size']['difference'] * ($preCalculationArray['land_size']['change_weightage'] / 100) * $cost_info->lands_price));

        $preCalculationArray['balcony_size']['average'] = $drive_margin_saved->average_balcony_size;
        $preCalculationArray['balcony_size']['subject_property'] = $inspection_data->balcony_size;
        $preCalculationArray['balcony_size']['difference'] = $preCalculationArray['balcony_size']['subject_property'] - $preCalculationArray['balcony_size']['average'];
        $preCalculationArray['balcony_size']['standard_weightage'] = $weightages->balcony_size;
        $preCalculationArray['balcony_size']['change_weightage'] = $drive_margin_saved->weightage_balcony_size;
        // $preCalculationArray['balcony_size']['adjustments'] = round(($preCalculationArray['balcony_size']['difference'] * ($preCalculationArray['balcony_size']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));
        $preCalculationArray['balcony_size']['adjustments'] = round(($preCalculationArray['balcony_size']['difference'] * ($preCalculationArray['balcony_size']['change_weightage'] / 100) * $list_calculate_data->avg_psf));

        $preCalculationArray['built_up_area']['average'] = $drive_margin_saved->average_built_up_area;
        $preCalculationArray['built_up_area']['subject_property'] = $inspection_data->built_up_area;
        $preCalculationArray['built_up_area']['difference'] = $preCalculationArray['built_up_area']['subject_property'] - $preCalculationArray['built_up_area']['average'];
        $preCalculationArray['built_up_area']['standard_weightage'] = Yii::$app->appHelperFunctions->getBuaweightages($difference_bua_percentage);
        $preCalculationArray['built_up_area']['change_weightage'] = $drive_margin_saved->weightage_built_up_area;
        $preCalculationArray['built_up_area']['adjustments'] = round(($preCalculationArray['built_up_area']['difference'] * ($preCalculationArray['built_up_area']['change_weightage'] / 100) * $list_calculate_data->avg_psf));

        $preCalculationArray['date']['average'] = $drive_margin_saved->average_date;
        $preCalculationArray['date']['subject_property'] = date('Y-m-d');
        $preCalculationArray['date']['difference'] = $difference_in_days;
        $preCalculationArray['date']['standard_weightage'] = $date_weightages;
        $preCalculationArray['date']['change_weightage'] = $drive_margin_saved->weightage_date;
        $preCalculationArray['date']['adjustments'] = round((($preCalculationArray['date']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        if ($inspection_data->built_up_area > 0) {
            $sold_avg_psf = round($list_calculate_data->avg_psf / $inspection_data->built_up_area, 2);
        } else {
            $sold_avg_psf = 0;
        }


        $sold_adjustments = 0;

        foreach ($preCalculationArray as $calucation) {
            $sold_adjustments = $sold_adjustments + $calucation['adjustments'];
        }
        $total_estimate_price = $list_calculate_data->avg_listings_price_size + $sold_adjustments;
        if ($inspection_data->built_up_area > 0) {
            $sold_avg_psf = round($total_estimate_price / $inspection_data->built_up_area, 2);
        } else {
            $sold_avg_psf = 0;
        }
        if ($valuation->valuation_status != 5 && $valuation->pre_summary == 0) {
            $drive_margin_saved->mv_total_price = $total_estimate_price;
            $drive_margin_saved->mv_avg_psf = $sold_avg_psf;
            $drive_margin_saved->search_type = 1;
            $drive_margin_saved->save();

            $drive_margin_saved_m->mv_total_price = $total_estimate_price;
            $drive_margin_saved_m->mv_avg_psf = $sold_avg_psf;
            $drive_margin_saved_m->search_type = 0;
            $drive_margin_saved_m->save();

            $drive_margin_saved_r->mv_total_price = $total_estimate_price;
            $drive_margin_saved_r->mv_avg_psf = $sold_avg_psf;
            $drive_margin_saved_r->search_type = 2;
            $drive_margin_saved_r->save();
        }

        $drive_margin_saved_updated = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1])->one();

        $viewpath = 'steps_locked/_step11_v';
        if ($valuation->valuation_status == 5) {
            $viewpath = 'steps_locked/_step11_v';
        }

        return $this->render($viewpath, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'valuation' => $valuation,
            'selected_data' => $selected_data,
            'selected_data_auto' => $selected_data_auto,
            'totalResults' => $totalResults,
            'select_calculations' => $select_calculations,
            'selected_records_display' => $selected_records_display,
            'selected_records_display' => $sold_selected_records_0,
            'model' => $drive_margin_saved_updated,
            'readonly' => $readonly

        ]);

        /* $viewpath = 'steps/_step12_v';
         if( $model->valuation_status == 5){
             $viewpath = 'steps_locked/_step12_v';
         }
         return $this->render($viewpath, [
             'model' => $drive_margin_saved_updated,
             'valuation' => $model,
             'preCalculationArray' => $preCalculationArray,
             'avg_psf' => $list_calculate_data->avg_psf,
             'avg_listings_price_size' => $list_calculate_data->avg_listings_price_size,
             'sold_avg_psf' => $sold_avg_psf,
             'inspection_data' => $inspection_data,
             'readonly' => $readonly,
         ]);*/




    }
    public function actionStep_110($id, $readonly = 0)
    {
        $this->checkLogin();
        if (!Yii::$app->menuHelperFunction->checkActionAllowed('step_11')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        if (Yii::$app->request->post()) {
            $post = Yii::$app->request->post();
            if (isset($post['selection']) && $post['selection'] <> null) {
                ValuationSelectedListsR::deleteAll(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 0]);
                ValuationSelectedLists::deleteAll(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 0]);

                foreach ($post['selection'] as $selected_row) {
                    $selected_data_detail = new ValuationSelectedListsR();
                    $selected_data_detail->selected_id = $selected_row;
                    $selected_data_detail->type = 'sold';
                    $selected_data_detail->valuation_id = $id;
                    $selected_data_detail->search_type = 0;
                    $selected_data_detail->latest = 1;
                    $selected_data_detail->save();
                }

                foreach ($post['selection'] as $selected_row) {
                    $selected_data_detail = new ValuationSelectedLists();
                    $selected_data_detail->selected_id = $selected_row;
                    $selected_data_detail->type = 'sold';
                    $selected_data_detail->valuation_id = $id;
                    $selected_data_detail->search_type = 0;
                    $selected_data_detail->latest = 1;
                    $selected_data_detail->save();
                }

            }
        }

        $selected_data = ArrayHelper::map(\app\models\ValuationSelectedListsR::find()
            ->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 0])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');

        $selected_data_auto = ArrayHelper::map(\app\models\ValuationSelectedListsAuto::find()
            ->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 1])
            ->limit(10)
            ->orderBy(['latest' => SORT_DESC])
            ->all(), 'id', 'selected_id');
        /*  echo "<pre>";
          print_r($selected_data);
          print_r($selected_data_auto);
          die;*/

        /*if($id == 10083) {
            echo "<pre>";
            print_r($selected_data);
            print_r($selected_data_auto);
            die;
        }*/


        $totalResults = (new \yii\db\Query())
            ->select('AVG(no_of_bedrooms) as avg_bedrooms,AVG(land_size) as avg_land_size,AVG(built_up_area) as built_up_area_size,AVG(listings_price) avg_listings_price_size, MIN(listings_price) as min_price, MIN(price_per_sqt) as min_price_sqt, MAX(listings_price) as max_price, MAX(price_per_sqt) as max_price_sqt , FROM_UNIXTIME(AVG(UNIX_TIMESTAMP(transaction_date))) as avg_listing_date')
            ->from('sold_transaction')
            ->where(['id' => $selected_data])
            ->all();
        /*  if($id == 10083) {
              echo "<pre>";
              print_r($selected_data);
              print_r($selected_data_auto);
              print_r($totalResults);
              die;
          }*/

        //Average and calculations/*
        /* if (array_key_exists("avg_bedrooms", $totalResults[0])) {
              if($id == 10083) {
                  echo "<pre>";
                  print_r($selected_data);
                  print_r($selected_data_auto);
                  print_r($totalResults);
                  die;
              }
         }*/
        if (!empty($totalResults) && $totalResults <> null) {
            if (array_key_exists("avg_bedrooms", $totalResults[0])) {

                /* if($id == 10083) {
                     echo "<pre>";
                     print_r($selected_data);
                     print_r($selected_data_auto);
                     print_r($totalResults);
                     die;
                 }*/
                ValuationListCalculation::deleteAll(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 0]);
                $calculations = new ValuationListCalculation();
                $calculations->avg_bedrooms = round($totalResults[0]['avg_bedrooms']);
                $calculations->avg_land_size = round($totalResults[0]['avg_land_size']);
                $calculations->built_up_area_size = round($totalResults[0]['built_up_area_size']);
                $calculations->avg_listings_price_size = round($totalResults[0]['avg_listings_price_size']);
                $calculations->min_price = round($totalResults[0]['min_price']);
                $calculations->max_price = round($totalResults[0]['max_price']);
                $calculations->min_price_sqt = round($totalResults[0]['min_price_sqt']);
                $calculations->max_price_sqt = round($totalResults[0]['max_price_sqt']);
                $calculations->avg_listing_date = date('Y-m-d', strtotime($totalResults[0]['avg_listing_date']));
                $calculations->avg_psf = round($calculations->avg_listings_price_size / $calculations->built_up_area_size);
                $calculations->avg_gross_yield = 0;
                $calculations->type = 'sold';
                $calculations->valuation_id = $id;
                $calculations->search_type = 0;
                if(!$calculations->save()){
                    /*  echo "<pre>";
                      print_r($calculations->errors);
                      die;*/
                }

                ValuationListCalculation::deleteAll(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 2]);
                $calculations = new ValuationListCalculation();
                $calculations->avg_bedrooms = round($totalResults[0]['avg_bedrooms']);
                $calculations->avg_land_size = round($totalResults[0]['avg_land_size']);
                $calculations->built_up_area_size = round($totalResults[0]['built_up_area_size']);
                $calculations->avg_listings_price_size = round($totalResults[0]['avg_listings_price_size']);
                $calculations->min_price = round($totalResults[0]['min_price']);
                $calculations->max_price = round($totalResults[0]['max_price']);
                $calculations->min_price_sqt = round($totalResults[0]['min_price_sqt']);
                $calculations->max_price_sqt = round($totalResults[0]['max_price_sqt']);
                $calculations->avg_listing_date = date('Y-m-d', strtotime($totalResults[0]['avg_listing_date']));
                $calculations->avg_psf = round($calculations->avg_listings_price_size / $calculations->built_up_area_size);
                $calculations->avg_gross_yield = 0;
                $calculations->type = 'sold';
                $calculations->valuation_id = $id;
                $calculations->search_type = 2;
                if(!$calculations->save()){
                    /*  echo "<pre>";
                      print_r($calculations->errors);
                      die;*/
                }
            }

        }

        $select_calculations = \app\models\ValuationListCalculation::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 2])->one();


        // $sold_selected_records = \app\models\ValuationSelectedLists::find()->where(['selected_id' => $selected_data,'type'=> 'sold'])->all();

        // selected_data
        $sold_selected_records = \app\models\ValuationEnquiry::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 0])->one();


        $listing_filter = array();
        if (!empty($sold_selected_records) && $sold_selected_records) {

            $listing_filter['SoldTransactionSearch']['date_from'] = $sold_selected_records->new_from_date;
            $listing_filter['SoldTransactionSearch']['date_to'] = $sold_selected_records->date_to;
            $listing_filter['SoldTransactionSearch']['building_info_data'] = $sold_selected_records->building_info;
            $listing_filter['SoldTransactionSearch']['property_id'] = $sold_selected_records->property_id;
            $listing_filter['SoldTransactionSearch']['property_category'] = $sold_selected_records->property_category;
            $listing_filter['SoldTransactionSearch']['bedroom_from'] = $sold_selected_records->bedroom_from;
            $listing_filter['SoldTransactionSearch']['bedroom_to'] = $sold_selected_records->bedroom_to;
            $listing_filter['SoldTransactionSearch']['landsize_from'] = $sold_selected_records->landsize_from;
            $listing_filter['SoldTransactionSearch']['landsize_to'] = $sold_selected_records->landsize_to;
            $listing_filter['SoldTransactionSearch']['bua_from'] = $sold_selected_records->bua_from;
            $listing_filter['SoldTransactionSearch']['bua_to'] = $sold_selected_records->bua_to;
            $listing_filter['SoldTransactionSearch']['price_from'] = $sold_selected_records->price_from;
            $listing_filter['SoldTransactionSearch']['price_to'] = $sold_selected_records->price_to;
            $listing_filter['SoldTransactionSearch']['price_psf_from'] = $sold_selected_records->price_psf_from;
            $listing_filter['SoldTransactionSearch']['price_psf_to'] = $sold_selected_records->price_psf_to;
        } else {
            $valuation = $this->findModel($id);
            $listing_filter['SoldTransactionSearch']['date_from'] = '2020-01-01';
            $listing_filter['SoldTransactionSearch']['date_to'] = date('Y-m-d');
            $listing_filter['SoldTransactionSearch']['building_info_data'] = $valuation->building_info;
        }


        $selected_records_display = $sold_selected_records;

        if ($selected_records_display <> null & !empty($selected_records_display)) {
            $building_info_data = '';

            if ($selected_records_display->building_info <> null) {
                $buildings = explode(",", $sold_selected_records->building_info);
                $all_buildings_titles = Buildings::find()->where(['id' => $buildings])->all();

                if ($all_buildings_titles <> null && !empty($all_buildings_titles)) {
                    foreach ($all_buildings_titles as $title) {
                        $building_info_data .= $title['title'] . ',&nbsp';
                    }

                }

            }
            $selected_records_display->building_info = $building_info_data;
            $property_data = Properties::find()->where(['id' => $sold_selected_records->property_id])->one();
            if ($property_data <> null && !empty($property_data)) {
                $selected_records_display->property_id = $property_data->title;
            }
            $property_data = Properties::find()->where(['id' => $sold_selected_records->property_id])->one();
            if ($selected_records_display->property_category <> null) {
                $selected_records_display->property_category = Yii::$app->appHelperFunctions->propertiesCategoriesListArr[$sold_selected_records_display->property_category];
            }
        }

        $valuation = $this->findModel($id);
        // $searchModel = new ListingsTransactionsSearch();
        $searchModel = new SoldTransactionSearch();
        $dataProvider = $searchModel->searchvaluation($listing_filter);
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;


        if ($readonly == 1) {
            $dataProvider->sort = false;
            return $this->renderPartial('steps/_step11', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'valuation' => $valuation,
                'selected_data' => $selected_data,
                'selected_data_auto' => $selected_data_auto,
                'totalResults' => $totalResults,
                'select_calculations' => $select_calculations,
                'selected_records_display' => $selected_records_display,
                'readonly' => $readonly
            ]);
        }

        $viewpath = 'steps/_step11r';
        if ($valuation->valuation_status == 5) {
            $viewpath = 'steps_locked/_step11';
        }

        return $this->render($viewpath, [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'valuation' => $valuation,
            'selected_data' => $selected_data,
            // 'selected_data_r' => $selected_data_r,
            'selected_data_auto' => $selected_data_auto,
            'totalResults' => $totalResults,
            'select_calculations' => $select_calculations,
            'selected_records_display' => $selected_records_display,
            'readonly' => $readonly

        ]);

    }
    public function actionStep_120($id, $readonly = 0)
    {
        $this->checkLogin();
        if (!Yii::$app->menuHelperFunction->checkActionAllowed('step_12')) {
            Yii::$app->getSession()->addFlash('error', "Permission denied!");
            return $this->redirect(['index']);
        }
        $model = $this->findModel($id);
        $valuation = $this->findModel($id);

        if (Yii::$app->request->post()) {
            $drive_margin_saved = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 2])->one();
            $drive_margin_saved_r = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 0])->one();
            if($drive_margin_saved_r <> null) {
                if ($drive_margin_saved_r->load(Yii::$app->request->post())) {
                    $drive_margin_saved_r->search_type = 0;
                    $drive_margin_saved_r->save();
                }
            }


            if ($drive_margin_saved->load(Yii::$app->request->post())) {
                $drive_margin_saved->search_type = 2;
                if ($drive_margin_saved->save()) {
                    Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Information updated successfully'));
                    // return $this->redirect(['index']);
                    return $this->redirect(['valuation/step_120/' . $id]);
                } else {
                    if ($drive_margin_saved->hasErrors()) {
                        foreach ($drive_margin_saved->getErrors() as $error) {
                            if (count($error) > 0) {
                                foreach ($error as $key => $val) {
                                    Yii::$app->getSession()->addFlash('error', $val);
                                }
                            }
                        }
                    }
                }
            }
        }

        $inspection_data = \app\models\InspectProperty::find()->where(['valuation_id' => $id])->one();

        $location_attributes_value = Yii::$app->appHelperFunctions->getLocationAttributes($inspection_data);
        $view_attributes_value = Yii::$app->appHelperFunctions->getViewAttributes($inspection_data);

        $list_calculate_data = \app\models\ValuationListCalculation::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 0])->one();
        $weightages = \app\models\Weightages::find()->where(['property_type' => $model->property_id])->one();


        $buildongs_info = \app\models\Buildings::find()->where(['id' => $model->building_info])->one();
        $cost_info = \app\models\CostDetails::find()->where(['valuation_id' => $id])->one();
        $developer_margin = \app\models\ValuationDeveloperDriveMargin::find()->where(['valuation_id' => $id])->one();

        $weightages_required_month = array();
        $weightages_required_year = array();

        $weightages_required_month[1] = $weightages->less_than_1_month;
        $weightages_required_month[2] = $weightages->less_than_2_month;
        $weightages_required_month[3] = $weightages->less_than_3_month;
        $weightages_required_month[4] = $weightages->less_than_4_month;
        $weightages_required_month[5] = $weightages->less_than_5_month;
        $weightages_required_month[6] = $weightages->less_than_6_month;
        $weightages_required_month[7] = $weightages->less_than_7_month;
        $weightages_required_month[8] = $weightages->less_than_8_month;
        $weightages_required_month[9] = $weightages->less_than_9_month;
        $weightages_required_month[10] = $weightages->less_than_10_month;
        $weightages_required_month[11] = $weightages->less_than_11_month;
        $weightages_required_month[12] = $weightages->less_than_12_month;

        $weightages_required_year[2] = $weightages->less_than_2_year;
        $weightages_required_year[3] = $weightages->less_than_3_year;
        $weightages_required_year[4] = $weightages->less_than_4_year;
        $weightages_required_year[5] = $weightages->less_than_5_year;
        $weightages_required_year[6] = $weightages->less_than_6_year;
        $weightages_required_year[7] = $weightages->less_than_7_year;
        $weightages_required_year[8] = $weightages->less_than_8_year;
        $weightages_required_year[9] = $weightages->less_than_9_year;
        $weightages_required_year[10] = $weightages->less_than_10_year;

        $avg_date = date_create($list_calculate_data->avg_listing_date);
        $current_date = date('Y-m-d');
        $current_date = date_create($current_date);
        $diff = date_diff($current_date, $avg_date);
        $difference_in_days = $diff->format("%a");
        $date_weightages = 0;

        $years_remaining = intval($difference_in_days / 365);
        $days_remaining = $difference_in_days % 365;

        if ($years_remaining > 1) {
            if ($days_remaining > 0) {

                $date_weightages = $weightages_required_year[$years_remaining + 1];
            } else {
                $date_weightages = $weightages_required_year[$years_remaining];
            }
        } else {
            $month_no = intval($days_remaining / 30);
            $days_remaining_month = $days_remaining % 30;
            if ($days_remaining_month > 0) {
                $date_weightages = $weightages_required_month[$month_no + 1];
            } else {
                $date_weightages = $weightages_required_month[$month_no];
            }
        }

        $drive_margin_saved_a = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 0])->one();
        $drive_margin_saved = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 2])->one();
        /*if ($list_calculate_data->built_up_area_size > 0) {
            $landsize = round(($list_calculate_data->avg_land_size / $list_calculate_data->built_up_area_size), 4);
        } else {
            $landsize = 0;
        }
        $difference_land_size = ($landsize * $inspection_data->built_up_area) - $model->land_size;
        if ($list_calculate_data->avg_land_size > 0) {
            $difference_land_size_percentage = round(abs(($difference_land_size / $list_calculate_data->avg_land_size) * 100), 2);
        } else {
            $difference_land_size_percentage = 0;
        }*/

        $simple_land_check = 0;
        $allow_properties_simple = [4, 5, 29, 39, 44, 46, 48, 49, 50, 51, 52, 53, 5, 55, 56];
        if (in_array($valuation->property_id, $allow_properties_simple)) {
            $simple_land_check = 1;
        }
        if($simple_land_check == 1){
            $gfa = $inspection_data->gfa;
            if($gfa <= 0) {
                $gfa = round($inspection_data->built_up_area / $model->land_size);
            }
            if ($list_calculate_data->avg_land_size > 0) {
                $gfa_avg = round($list_calculate_data->built_up_area_size / $list_calculate_data->avg_land_size);
            }else{
                $gfa_avg = 0;
            }
            $difference_land_size_percentage = $gfa - $gfa_avg;
        }else {

            $difference_land_size = $model->land_size - $list_calculate_data->avg_land_size;
            if ($list_calculate_data->avg_land_size > 0) {
                $difference_land_size_percentage = round(abs(($difference_land_size / $list_calculate_data->avg_land_size) * 100), 2);
            } else {
                $difference_land_size_percentage = 0;
            }
        }

        $difference_bua = $inspection_data->built_up_area - $list_calculate_data->built_up_area_size;
        if ($list_calculate_data->built_up_area_size > 0) {
            $difference_bua_percentage = round(abs(($difference_bua / $list_calculate_data->built_up_area_size) * 100), 2);
        } else {
            $difference_bua_percentage = 0;
        }
        if (empty($drive_margin_saved) && $drive_margin_saved == null) {

            $selected_data_last_step = ArrayHelper::map(\app\models\ValuationSelectedListsR::find()
                ->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 0])
                ->limit(10)
                ->orderBy(['latest' => SORT_DESC])
                ->all(), 'id', 'selected_id');

            $totalResults_average = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_no_of_bedrooms')
                ->from('sold_transaction')
                ->where(['id' => $selected_data_last_step])
                ->all();

            $drive_margin_saved_data = new ValuationDriveMv();

            $drive_margin_saved_data->average_location = $weightages->location_avg;
            $drive_margin_saved_data->average_age = $buildongs_info->estimated_age;
            $drive_margin_saved_data->average_tenure = $weightages->tenure_avg;
            $drive_margin_saved_data->average_view = $weightages->view_avg;
            $drive_margin_saved_data->average_finished_status = $weightages->finished_status_avg;
            $drive_margin_saved_data->average_property_condition = $weightages->property_condition_avg;
            $drive_margin_saved_data->average_upgrades = $weightages->upgrades_avg;
            // $drive_margin_saved_data->average_furnished = 3;
            $drive_margin_saved_data->average_property_exposure = $weightages->property_exposure_avg;
            $drive_margin_saved_data->average_property_placement = $weightages->property_exposure_avg;
            // $drive_margin_saved_data->average_full_building_floors = (isset($model->floor_number) ? round($model->floor_number, 2) : 0);;
            $drive_margin_saved_data->average_full_building_floors = (isset($inspection_data->full_building_floors) ? round($inspection_data->full_building_floors / 2, 2) : 0);
            $drive_margin_saved_data->average_number_of_levels = $weightages->number_of_levels_avg;
            $drive_margin_saved_data->average_no_of_bedrooms = (isset($totalResults_average[0]['avg_no_of_bedrooms']) ? round($totalResults_average[0]['avg_no_of_bedrooms'], 2) : 0);
            ;
            $drive_margin_saved_data->average_parking_space = $weightages->parking_space_avg;
            $drive_margin_saved_data->average_pool = $weightages->pool_avg;
            $drive_margin_saved_data->average_landscaping = $weightages->landscaping_avg;
            $drive_margin_saved_data->average_white_goods = $weightages->white_goods_avg;
            $drive_margin_saved_data->average_utilities_connected = $weightages->utilities_connected_avg;
            $drive_margin_saved_data->average_developer_margin = $weightages->developer_margin_avg;
            $drive_margin_saved_data->average_land_size = $list_calculate_data->avg_land_size;
            $drive_margin_saved_data->average_balcony_size = $weightages->balcony_size_avg;
            ;
            $drive_margin_saved_data->average_built_up_area = $list_calculate_data->built_up_area_size;
            $drive_margin_saved_data->average_date = $list_calculate_data->avg_listing_date;
            $drive_margin_saved_data->average_sale_price = $list_calculate_data->avg_listings_price_size;
            ;
            $drive_margin_saved_data->average_psf = $list_calculate_data->avg_psf;
            ;

            $drive_margin_saved_data->weightage_location = $weightages->location;
            $drive_margin_saved_data->weightage_age = $weightages->age;
            $drive_margin_saved_data->weightage_tenure = $weightages->tenure;
            $drive_margin_saved_data->weightage_view = $weightages->view;
            $drive_margin_saved_data->weightage_finished_status = $weightages->finishing_status;
            $drive_margin_saved_data->weightage_property_condition = $weightages->upgrades;
            $drive_margin_saved_data->weightage_upgrades = $weightages->quality;
            $drive_margin_saved_data->weightage_furnished = $weightages->furnished;
            $drive_margin_saved_data->weightage_property_exposure = $weightages->property_exposure;
            $drive_margin_saved_data->weightage_property_placement = $weightages->property_placement;
            $drive_margin_saved_data->weightage_full_building_floors = $weightages->floor;
            $drive_margin_saved_data->weightage_number_of_levels = $weightages->number_of_levels;
            $drive_margin_saved_data->weightage_no_of_bedrooms = $weightages->bedrooom;
            $drive_margin_saved_data->weightage_parking_space = $weightages->parking;
            $drive_margin_saved_data->weightage_pool = $weightages->pool;
            $drive_margin_saved_data->weightage_landscaping = $weightages->landscape;
            $drive_margin_saved_data->weightage_white_goods = $weightages->white_goods;
            $drive_margin_saved_data->weightage_utilities_connected = $weightages->utilities_connected;
            $drive_margin_saved_data->weightage_developer_margin = $developer_margin->developer_margin_percentage;




            // $drive_margin_saved_data->weightage_land_size = Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);

            if($simple_land_check == 1) {
                $drive_margin_saved_data->weightage_land_size = Yii::$app->appHelperFunctions->getGfaweightages($difference_land_size_percentage);
            }else{
                $drive_margin_saved_data->weightage_land_size = Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);
            }
            $drive_margin_saved_data->weightage_balcony_size = $weightages->balcony_size;
            $drive_margin_saved_data->weightage_built_up_area = Yii::$app->appHelperFunctions->getBuaweightages($difference_bua_percentage);
            $drive_margin_saved_data->weightage_date = $date_weightages;
            $drive_margin_saved_data->valuation_id = $id;
            $drive_margin_saved_data->type = 'sold';
            $drive_margin_saved_data->search_type = 2;
            if (!$drive_margin_saved_data->save()) {
            }
            $drive_margin_saved = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 2])->one();
        }

        if (empty($drive_margin_saved_a) && $drive_margin_saved_a == null) {

            $selected_data_last_step = ArrayHelper::map(\app\models\ValuationSelectedListsR::find()
                ->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 0])
                ->limit(10)
                ->orderBy(['latest' => SORT_DESC])
                ->all(), 'id', 'selected_id');

            $totalResults_average = (new \yii\db\Query())
                ->select('AVG(no_of_bedrooms) as avg_no_of_bedrooms')
                ->from('sold_transaction')
                ->where(['id' => $selected_data_last_step])
                ->all();

            $drive_margin_saved_data_a = new ValuationDriveMv();

            $drive_margin_saved_data_a->average_location = $weightages->location_avg;
            $drive_margin_saved_data_a->average_age = $buildongs_info->estimated_age;
            $drive_margin_saved_data_a->average_tenure = $weightages->tenure_avg;
            $drive_margin_saved_data_a->average_view = $weightages->view_avg;
            $drive_margin_saved_data_a->average_finished_status = $weightages->finished_status_avg;
            $drive_margin_saved_data_a->average_property_condition = $weightages->property_condition_avg;
            $drive_margin_saved_data_a->average_upgrades = $weightages->upgrades_avg;
            // $drive_margin_saved_data->average_furnished = 3;
            $drive_margin_saved_data_a->average_property_exposure = $weightages->property_exposure_avg;
            $drive_margin_saved_data_a->average_property_placement = $weightages->property_exposure_avg;
            // $drive_margin_saved_data->average_full_building_floors = (isset($model->floor_number) ? round($model->floor_number, 2) : 0);;
            $drive_margin_saved_data_a->average_full_building_floors = (isset($inspection_data->full_building_floors) ? round($inspection_data->full_building_floors / 2, 2) : 0);
            $drive_margin_saved_data_a->average_number_of_levels = $weightages->number_of_levels_avg;
            $drive_margin_saved_data_a->average_no_of_bedrooms = (isset($totalResults_average[0]['avg_no_of_bedrooms']) ? round($totalResults_average[0]['avg_no_of_bedrooms'], 2) : 0);
            ;
            $drive_margin_saved_data_a->average_parking_space = $weightages->parking_space_avg;
            $drive_margin_saved_data_a->average_pool = $weightages->pool_avg;
            $drive_margin_saved_data_a->average_landscaping = $weightages->landscaping_avg;
            $drive_margin_saved_data_a->average_white_goods = $weightages->white_goods_avg;
            $drive_margin_saved_data_a->average_utilities_connected = $weightages->utilities_connected_avg;
            $drive_margin_saved_data_a->average_developer_margin = $weightages->developer_margin_avg;
            $drive_margin_saved_data_a->average_land_size = $list_calculate_data->avg_land_size;
            $drive_margin_saved_data_a->average_balcony_size = $weightages->balcony_size_avg;
            ;
            $drive_margin_saved_data_a->average_built_up_area = $list_calculate_data->built_up_area_size;
            $drive_margin_saved_data_a->average_date = $list_calculate_data->avg_listing_date;
            $drive_margin_saved_data_a->average_sale_price = $list_calculate_data->avg_listings_price_size;
            ;
            $drive_margin_saved_data_a->average_psf = $list_calculate_data->avg_psf;
            ;

            $drive_margin_saved_data_a->weightage_location = $weightages->location;
            $drive_margin_saved_data_a->weightage_age = $weightages->age;
            $drive_margin_saved_data_a->weightage_tenure = $weightages->tenure;
            $drive_margin_saved_data_a->weightage_view = $weightages->view;
            $drive_margin_saved_data_a->weightage_finished_status = $weightages->finishing_status;
            $drive_margin_saved_data_a->weightage_property_condition = $weightages->upgrades;
            $drive_margin_saved_data_a->weightage_upgrades = $weightages->quality;
            $drive_margin_saved_data_a->weightage_furnished = $weightages->furnished;
            $drive_margin_saved_data_a->weightage_property_exposure = $weightages->property_exposure;
            $drive_margin_saved_data_a->weightage_property_placement = $weightages->property_placement;
            $drive_margin_saved_data_a->weightage_full_building_floors = $weightages->floor;
            $drive_margin_saved_data_a->weightage_number_of_levels = $weightages->number_of_levels;
            $drive_margin_saved_data_a->weightage_no_of_bedrooms = $weightages->bedrooom;
            $drive_margin_saved_data_a->weightage_parking_space = $weightages->parking;
            $drive_margin_saved_data_a->weightage_pool = $weightages->pool;
            $drive_margin_saved_data_a->weightage_landscaping = $weightages->landscape;
            $drive_margin_saved_data_a->weightage_white_goods = $weightages->white_goods;
            $drive_margin_saved_data_a->weightage_utilities_connected = $weightages->utilities_connected;
            $drive_margin_saved_data_a->weightage_developer_margin = $developer_margin->developer_margin_percentage;




            // $drive_margin_saved_data->weightage_land_size = Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);

            if($simple_land_check == 1) {
                $drive_margin_saved_data_a->weightage_land_size = Yii::$app->appHelperFunctions->getGfaweightages($difference_land_size_percentage);
            }else{
                $drive_margin_saved_data_a->weightage_land_size = Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);
            }
            $drive_margin_saved_data_a->weightage_balcony_size = $weightages->balcony_size;
            $drive_margin_saved_data_a->weightage_built_up_area = Yii::$app->appHelperFunctions->getBuaweightages($difference_bua_percentage);
            $drive_margin_saved_data_a->weightage_date = $date_weightages;
            $drive_margin_saved_data_a->valuation_id = $id;
            $drive_margin_saved_data_a->type = 'sold';
            $drive_margin_saved_data_a->search_type = 0;
            if (!$drive_margin_saved_data_a->save()) {
            }
            $drive_margin_saved_a = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 0])->one();
        }

        $preCalculationArray = array();

        $preCalculationArray['location']['average'] = $drive_margin_saved->average_location;
        // $preCalculationArray['location']['subject_property'] = $inspection_data->location;
        $preCalculationArray['location']['subject_property'] = $location_attributes_value;
        $preCalculationArray['location']['difference'] = $preCalculationArray['location']['subject_property'] - $preCalculationArray['location']['average'];
        $preCalculationArray['location']['standard_weightage'] = $weightages->location;
        $preCalculationArray['location']['change_weightage'] = $drive_margin_saved->weightage_location;
        $preCalculationArray['location']['adjustments'] = round(($preCalculationArray['location']['difference'] * ($preCalculationArray['location']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['age']['average'] = $drive_margin_saved->average_age;
        $preCalculationArray['age']['subject_property'] = $inspection_data->estimated_age;
        $preCalculationArray['age']['difference'] = $preCalculationArray['age']['average'] - $preCalculationArray['age']['subject_property'];
        $preCalculationArray['age']['standard_weightage'] = $weightages->age;
        $preCalculationArray['age']['change_weightage'] = $drive_margin_saved->weightage_age;
        $preCalculationArray['age']['adjustments'] = round(($preCalculationArray['age']['difference'] * ($preCalculationArray['age']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['tenure']['average'] = $drive_margin_saved->average_tenure;
        $preCalculationArray['tenure']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->buildingTenureArr[$model->tenure]];
        $preCalculationArray['tenure']['difference'] = $preCalculationArray['tenure']['subject_property'] - $preCalculationArray['tenure']['average'];
        $preCalculationArray['tenure']['standard_weightage'] = $weightages->tenure;
        $preCalculationArray['tenure']['change_weightage'] = $drive_margin_saved->weightage_tenure;
        $preCalculationArray['tenure']['adjustments'] = round(($preCalculationArray['tenure']['difference'] * ($preCalculationArray['tenure']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['view']['average'] = $drive_margin_saved->average_view;
        // $preCalculationArray['view']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->otherPropertyViewListArr[$inspection_data->view]];
        $preCalculationArray['view']['subject_property'] = $view_attributes_value;
        $preCalculationArray['view']['difference'] = $preCalculationArray['view']['subject_property'] - $preCalculationArray['view']['average'];
        $preCalculationArray['view']['standard_weightage'] = $weightages->view;
        $preCalculationArray['view']['change_weightage'] = $drive_margin_saved->weightage_view;
        $preCalculationArray['view']['adjustments'] = round(($preCalculationArray['view']['difference'] * ($preCalculationArray['view']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['finished_status']['average'] = $drive_margin_saved->average_finished_status;
        $preCalculationArray['finished_status']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->finished_status];
        $preCalculationArray['finished_status']['difference'] = $preCalculationArray['finished_status']['subject_property'] - $preCalculationArray['finished_status']['average'];
        $preCalculationArray['finished_status']['standard_weightage'] = $weightages->furnished;
        $preCalculationArray['finished_status']['change_weightage'] = $drive_margin_saved->weightage_finished_status;
        $preCalculationArray['finished_status']['adjustments'] = round(($preCalculationArray['finished_status']['difference'] * ($preCalculationArray['finished_status']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['property_condition']['average'] = $drive_margin_saved->average_property_condition;
        $preCalculationArray['property_condition']['subject_property'] = Yii::$app->appHelperFunctions->propertyConditionRatingtArr[$inspection_data->property_condition];
        $preCalculationArray['property_condition']['difference'] = $preCalculationArray['property_condition']['subject_property'] - $preCalculationArray['property_condition']['average'];
        $preCalculationArray['property_condition']['standard_weightage'] = $weightages->upgrades;
        $preCalculationArray['property_condition']['change_weightage'] = $drive_margin_saved->weightage_property_condition;
        $preCalculationArray['property_condition']['adjustments'] = round(($preCalculationArray['property_condition']['difference'] * ($preCalculationArray['property_condition']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));


        $preCalculationArray['upgrades']['average'] = $drive_margin_saved->average_upgrades;
        $preCalculationArray['upgrades']['subject_property'] = $model->valuationConfiguration->over_all_upgrade;
        $preCalculationArray['upgrades']['difference'] = $preCalculationArray['upgrades']['subject_property'] - $preCalculationArray['upgrades']['average'];
        $preCalculationArray['upgrades']['standard_weightage'] = $weightages->quality;
        $preCalculationArray['upgrades']['change_weightage'] = $drive_margin_saved->weightage_upgrades;
        $preCalculationArray['upgrades']['adjustments'] = round(($preCalculationArray['upgrades']['difference'] * ($preCalculationArray['upgrades']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));


        $preCalculationArray['property_exposure']['average'] = $drive_margin_saved->average_property_exposure;
        $preCalculationArray['property_exposure']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->propertyExposureListArr[$inspection_data->property_exposure]];
        $preCalculationArray['property_exposure']['difference'] = $preCalculationArray['property_exposure']['subject_property'] - $preCalculationArray['property_exposure']['average'];
        $preCalculationArray['property_exposure']['standard_weightage'] = $weightages->property_exposure;
        $preCalculationArray['property_exposure']['change_weightage'] = $drive_margin_saved->weightage_property_exposure;
        $preCalculationArray['property_exposure']['adjustments'] = round(($preCalculationArray['property_exposure']['difference'] * ($preCalculationArray['property_exposure']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['property_placement']['average'] = $drive_margin_saved->average_property_placement;
        $preCalculationArray['property_placement']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[Yii::$app->appHelperFunctions->propertyPlacementListArr[$inspection_data->property_placement]];
        $preCalculationArray['property_placement']['difference'] = $preCalculationArray['property_placement']['subject_property'] - $preCalculationArray['property_placement']['average'];
        $preCalculationArray['property_placement']['standard_weightage'] = $weightages->property_placement;
        $preCalculationArray['property_placement']['change_weightage'] = $drive_margin_saved->weightage_property_placement;
        $preCalculationArray['property_placement']['adjustments'] = round(($preCalculationArray['property_placement']['difference'] * ($preCalculationArray['property_placement']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['floors_adjustment']['average'] = $drive_margin_saved->average_full_building_floors;
        $preCalculationArray['floors_adjustment']['subject_property'] = (isset($model->floor_number) ? $model->floor_number : 0);
        $preCalculationArray['floors_adjustment']['difference'] = $preCalculationArray['floors_adjustment']['subject_property'] - $preCalculationArray['floors_adjustment']['average'];
        $preCalculationArray['floors_adjustment']['standard_weightage'] = $weightages->floor;
        $preCalculationArray['floors_adjustment']['change_weightage'] = $drive_margin_saved->weightage_full_building_floors;
        $preCalculationArray['floors_adjustment']['adjustments'] = round(($preCalculationArray['floors_adjustment']['difference'] * ($preCalculationArray['floors_adjustment']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['number_of_levels']['average'] = $drive_margin_saved->average_number_of_levels;
        $preCalculationArray['number_of_levels']['subject_property'] = $inspection_data->number_of_levels;
        $preCalculationArray['number_of_levels']['difference'] = $preCalculationArray['number_of_levels']['subject_property'] - $preCalculationArray['number_of_levels']['average'];
        $preCalculationArray['number_of_levels']['standard_weightage'] = $weightages->number_of_levels;
        $preCalculationArray['number_of_levels']['change_weightage'] = $drive_margin_saved->weightage_number_of_levels;
        $preCalculationArray['number_of_levels']['adjustments'] = round(($preCalculationArray['number_of_levels']['difference'] * ($preCalculationArray['number_of_levels']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['no_of_bedrooms']['average'] = $drive_margin_saved->average_no_of_bedrooms;
        $preCalculationArray['no_of_bedrooms']['subject_property'] = $inspection_data->no_of_bedrooms;
        $preCalculationArray['no_of_bedrooms']['difference'] = $preCalculationArray['no_of_bedrooms']['subject_property'] - $preCalculationArray['no_of_bedrooms']['average'];
        $preCalculationArray['no_of_bedrooms']['standard_weightage'] = $weightages->bedrooom;
        $preCalculationArray['no_of_bedrooms']['change_weightage'] = $drive_margin_saved->weightage_no_of_bedrooms;
        $preCalculationArray['no_of_bedrooms']['adjustments'] = round(($preCalculationArray['no_of_bedrooms']['difference'] * ($preCalculationArray['no_of_bedrooms']['standard_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        $preCalculationArray['parking_space']['average'] = $drive_margin_saved->average_parking_space;
        $preCalculationArray['parking_space']['subject_property'] = $inspection_data->parking_floors;
        $preCalculationArray['parking_space']['difference'] = $preCalculationArray['parking_space']['subject_property'] - $preCalculationArray['parking_space']['average'];
        $preCalculationArray['parking_space']['standard_weightage'] = $weightages->parking;
        $preCalculationArray['parking_space']['change_weightage'] = $drive_margin_saved->weightage_parking_space;
        $preCalculationArray['parking_space']['adjustments'] = round(($cost_info->parking_price * $preCalculationArray['parking_space']['difference']) * ($preCalculationArray['parking_space']['change_weightage'] / 100), 2);


        $preCalculationArray['pool']['average'] = $drive_margin_saved->average_pool;
        $preCalculationArray['pool']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->pool];
        $preCalculationArray['pool']['difference'] = $preCalculationArray['pool']['subject_property'] - $preCalculationArray['pool']['average'];
        $preCalculationArray['pool']['standard_weightage'] = $weightages->pool;
        $preCalculationArray['pool']['change_weightage'] = $drive_margin_saved->weightage_pool;
        $preCalculationArray['pool']['adjustments'] = round(($cost_info->pool_price * $preCalculationArray['pool']['difference']) * ($preCalculationArray['pool']['change_weightage'] / 100), 2);

        $preCalculationArray['landscaping']['average'] = $drive_margin_saved->average_landscaping;
        $preCalculationArray['landscaping']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->landscaping];
        $preCalculationArray['landscaping']['difference'] = $preCalculationArray['landscaping']['subject_property'] - $preCalculationArray['landscaping']['average'];
        $preCalculationArray['landscaping']['standard_weightage'] = $weightages->landscape;
        $preCalculationArray['landscaping']['change_weightage'] = $drive_margin_saved->weightage_landscaping;
        ;
        $preCalculationArray['landscaping']['adjustments'] = round(($cost_info->landscape_price * $preCalculationArray['landscaping']['difference']) * ($preCalculationArray['landscaping']['change_weightage'] / 100), 2);

        $preCalculationArray['white_goods']['average'] = $drive_margin_saved->average_white_goods;
        $preCalculationArray['white_goods']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->white_goods];
        $preCalculationArray['white_goods']['difference'] = $preCalculationArray['white_goods']['subject_property'] - $preCalculationArray['white_goods']['average'];
        $preCalculationArray['white_goods']['standard_weightage'] = $weightages->white_goods;
        $preCalculationArray['white_goods']['change_weightage'] = $drive_margin_saved->weightage_white_goods;
        $preCalculationArray['white_goods']['change_weightage'] = $drive_margin_saved->weightage_white_goods;
        $preCalculationArray['white_goods']['adjustments'] = round(($cost_info->white_goods_price * $preCalculationArray['white_goods']['difference']) * ($preCalculationArray['white_goods']['change_weightage'] / 100), 2);

        $preCalculationArray['utilities_connected']['average'] = $drive_margin_saved->average_utilities_connected;
        $preCalculationArray['utilities_connected']['subject_property'] = Yii::$app->appHelperFunctions->deriveMvValue[$inspection_data->utilities_connected];
        $preCalculationArray['utilities_connected']['difference'] = $preCalculationArray['utilities_connected']['subject_property'] - $preCalculationArray['utilities_connected']['average'];
        $preCalculationArray['utilities_connected']['standard_weightage'] = $weightages->utilities_connected;
        $preCalculationArray['utilities_connected']['change_weightage'] = $drive_margin_saved->weightage_utilities_connected;
        $preCalculationArray['utilities_connected']['adjustments'] = round(($cost_info->utilities_connected_price * $preCalculationArray['utilities_connected']['difference']) * ($preCalculationArray['utilities_connected']['change_weightage'] / 100), 2);

        $preCalculationArray['developer_margin']['average'] = 0;
        $preCalculationArray['developer_margin']['subject_property'] = 0;
        $preCalculationArray['developer_margin']['difference'] = 0;
        $preCalculationArray['developer_margin']['standard_weightage'] = 0;
        $preCalculationArray['developer_margin']['change_weightage'] = ($drive_margin_saved->weightage_developer_margin > 0) ? -$drive_margin_saved->weightage_developer_margin : $drive_margin_saved->weightage_developer_margin;
        $preCalculationArray['developer_margin']['adjustments'] = round((($preCalculationArray['developer_margin']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size), 2);
        /*  if ($list_calculate_data->built_up_area_size > 0) {
              $landsize = round(($list_calculate_data->avg_land_size / $list_calculate_data->built_up_area_size), 4);
          } else {
              $landsize = 0;
          }*/
        $preCalculationArray['land_size']['average'] = $drive_margin_saved->average_land_size;
        $preCalculationArray['land_size']['subject_property'] = $model->land_size;
        $preCalculationArray['land_size']['difference'] = $preCalculationArray['land_size']['subject_property'] - $preCalculationArray['land_size']['average'];
        $preCalculationArray['land_size']['standard_weightage'] = Yii::$app->appHelperFunctions->getBuaweightages($difference_land_size_percentage);
        $preCalculationArray['land_size']['change_weightage'] = $drive_margin_saved->weightage_land_size;
        $preCalculationArray['land_size']['adjustments'] = round(($preCalculationArray['land_size']['difference'] * ($preCalculationArray['land_size']['change_weightage'] / 100) * $cost_info->lands_price));

        $preCalculationArray['balcony_size']['average'] = $drive_margin_saved->average_balcony_size;
        $preCalculationArray['balcony_size']['subject_property'] = $inspection_data->balcony_size;
        $preCalculationArray['balcony_size']['difference'] = $preCalculationArray['balcony_size']['subject_property'] - $preCalculationArray['balcony_size']['average'];
        $preCalculationArray['balcony_size']['standard_weightage'] = $weightages->balcony_size;
        $preCalculationArray['balcony_size']['change_weightage'] = $drive_margin_saved->weightage_balcony_size;
        // $preCalculationArray['balcony_size']['adjustments'] = round(($preCalculationArray['balcony_size']['difference'] * ($preCalculationArray['balcony_size']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));
        $preCalculationArray['balcony_size']['adjustments'] = round(($preCalculationArray['balcony_size']['difference'] * ($preCalculationArray['balcony_size']['change_weightage'] / 100) * $list_calculate_data->avg_psf));

        $preCalculationArray['built_up_area']['average'] = $drive_margin_saved->average_built_up_area;
        $preCalculationArray['built_up_area']['subject_property'] = $inspection_data->built_up_area;
        $preCalculationArray['built_up_area']['difference'] = $preCalculationArray['built_up_area']['subject_property'] - $preCalculationArray['built_up_area']['average'];
        $preCalculationArray['built_up_area']['standard_weightage'] = Yii::$app->appHelperFunctions->getBuaweightages($difference_bua_percentage);
        $preCalculationArray['built_up_area']['change_weightage'] = $drive_margin_saved->weightage_built_up_area;
        $preCalculationArray['built_up_area']['adjustments'] = round(($preCalculationArray['built_up_area']['difference'] * ($preCalculationArray['built_up_area']['change_weightage'] / 100) * $list_calculate_data->avg_psf));

        $preCalculationArray['date']['average'] = $drive_margin_saved->average_date;
        $preCalculationArray['date']['subject_property'] = date('Y-m-d');
        $preCalculationArray['date']['difference'] = $difference_in_days;
        $preCalculationArray['date']['standard_weightage'] = $date_weightages;
        $preCalculationArray['date']['change_weightage'] = $drive_margin_saved->weightage_date;
        $preCalculationArray['date']['adjustments'] = round((($preCalculationArray['date']['change_weightage'] / 100) * $list_calculate_data->avg_listings_price_size));

        if ($inspection_data->built_up_area > 0) {
            $sold_avg_psf = round($list_calculate_data->avg_psf / $inspection_data->built_up_area, 2);
        } else {
            $sold_avg_psf = 0;
        }


        $sold_adjustments = 0;

        foreach ($preCalculationArray as $calucation) {
            $sold_adjustments = $sold_adjustments + $calucation['adjustments'];
        }
        $total_estimate_price = $list_calculate_data->avg_listings_price_size + $sold_adjustments;
        if ($inspection_data->built_up_area > 0) {
            $sold_avg_psf = round($total_estimate_price / $inspection_data->built_up_area, 2);
        } else {
            $sold_avg_psf = 0;
        }

        $drive_margin_saved->mv_total_price = $total_estimate_price;
        $drive_margin_saved->mv_avg_psf = $sold_avg_psf;
        $drive_margin_saved->search_type = 2;
        if ($model->valuation_status != 5) {
            $drive_margin_saved->save();
        }

        $drive_margin_saved_a->mv_total_price = $total_estimate_price;
        $drive_margin_saved_a->mv_avg_psf = $sold_avg_psf;
        $drive_margin_saved_a->search_type = 0;
        if ($model->valuation_status != 5) {
            $drive_margin_saved_a->save();
        }

        $drive_margin_saved_updated = \app\models\ValuationDriveMv::find()->where(['valuation_id' => $id, 'type' => 'sold', 'search_type' => 2])->one();

        if ($readonly == 1) {

            return $this->renderPartial('steps/_step120', [
                'model' => $drive_margin_saved_updated,
                'valuation' => $model,
                'preCalculationArray' => $preCalculationArray,
                'avg_psf' => $list_calculate_data->avg_psf,
                'avg_listings_price_size' => $list_calculate_data->avg_listings_price_size,
                'sold_avg_psf' => $sold_avg_psf,
                'inspection_data' => $inspection_data,
                'readonly' => $readonly,
            ]);
        }
        $viewpath = 'steps/_step120';
        if ($model->valuation_status == 5) {
            $viewpath = 'steps_locked/_step12';
        }
        return $this->render($viewpath, [
            'model' => $drive_margin_saved_updated,
            'valuation' => $model,
            'preCalculationArray' => $preCalculationArray,
            'avg_psf' => $list_calculate_data->avg_psf,
            'avg_listings_price_size' => $list_calculate_data->avg_listings_price_size,
            'sold_avg_psf' => $sold_avg_psf,
            'inspection_data' => $inspection_data,
            'readonly' => $readonly,
        ]);

    }
}
