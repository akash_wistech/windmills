<?php

namespace app\controllers;

use Yii;
use app\models\Industry;
use app\models\IndustrySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\helpers\DefController;

/**
 * AssetCategoryController implements the CRUD actions for AssetCategory model.
 */
class IndustriesController extends DefController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Industry models.
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        $old_verify_status = null;
        if ($id <> null) {
            $model = Industry::findOne($id);
            $action = "data_updated";
            $old_verify_status = $model->status_verified;
        } else {
            $model = new Industry;
            $action = "data_created";
        }

        if ($model->load(Yii::$app->request->post())) {
            $this->StatusVerify($model);
            if ($model->save()) {
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => get_class($model),
                    'action' => $action,
                    'verify_field' => 'status_verified',
                    'old_verify_status' => $old_verify_status,
                ]);
                Yii::$app->getSession()->addFlash('success', Yii::t('app', 'Saved Successfully'));
                return $this->redirect(['index']);
            }
        }

        $searchModel = new IndustrySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }

    /**
     * Displays a single Industry model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Industry model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Industry();

        if ($model->load(Yii::$app->request->post())) {
            $this->StatusVerify($model);
            if ($model->save()) {
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => get_class($model),
                    'action' => 'data_created',
                    'verify_field' => 'status_verified',
                ]);
                return $this->redirect(['index']);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Industry model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_verify_status = $model->status_verified;

        if ($model->load(Yii::$app->request->post())) {
            $this->StatusVerify($model);
            if ($model->save()) {
                $this->makeHistory([
                    'model' => $model,
                    'model_name' => get_class($model),
                    'action' => 'data_updated',
                    'verify_field' => 'status_verified',
                    'old_verify_status' => $old_verify_status,
                ]);
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Industry model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Industry model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Industry the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Industry::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
