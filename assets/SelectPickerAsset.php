<?php
namespace app\assets;

use yii\web\AssetBundle;

class SelectPickerAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/bootstrap-selectpicker/css/bootstrap-select.min.css',
  ];
  public $js = [
    'plugins/bootstrap-selectpicker/js/bootstrap-select.min.js',
  ];
  public $depends = [
    // 'app\assets\MomentAsset'
  ];
}
