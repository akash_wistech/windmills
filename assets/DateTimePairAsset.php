<?php
namespace app\assets;

use yii\web\AssetBundle;

class DateTimePairAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
    'plugins/jquery-datepair/jquery.datepair.min.js',
  ];
  public $depends = [
    'app\assets\MomentAsset'
  ];
}
