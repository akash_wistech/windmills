<?php
namespace app\assets;

use yii\web\AssetBundle;

class JQueryTagsInputAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/jquery-tags-input/jquery.tagsinput.min.css',
  ];
  public $js = [
    'plugins/jquery-tags-input/jquery.tagsinput.min.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
  ];
}
