<?php
namespace app\assets;

use yii\web\AssetBundle;

class MomentAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
    'plugins/moment/moment.min.js',
  ];
  public $depends = [
  ];
}
