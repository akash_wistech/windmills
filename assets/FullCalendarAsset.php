<?php
namespace app\assets;

use yii\web\AssetBundle;

class FullCalendarAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/fullcalendar-5.4.0/main.css',
  ];
  public $js = [
    'plugins/fullcalendar-5.4.0/main.js',
  ];
  public $depends = [
  ];
}
