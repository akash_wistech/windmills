<?php
namespace app\assets;

use yii\web\AssetBundle;

class ListingsFormAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
      'js/listing_transactions.js',
      'js/field_validation.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
    'app\assets\DatePickerAsset',
  ];
}
