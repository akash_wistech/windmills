<?php
namespace app\assets;

use yii\web\AssetBundle;

class MCustomScrollerAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/custom-scrollbar/jquery.mCustomScrollbar.min.css',
  ];
  public $js = [
    'plugins/custom-scrollbar/jquery.mCustomScrollbar.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
  ];
}
