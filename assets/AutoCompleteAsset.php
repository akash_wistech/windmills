<?php
namespace app\assets;

use yii\web\AssetBundle;

class AutoCompleteAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/jquery-autocomplete/css/jquery.autocomplete.css',
  ];
  public $js = [
    'plugins/jquery-autocomplete/js/jquery.autocomplete.js',
  ];
  public $depends = [
  ];
}
