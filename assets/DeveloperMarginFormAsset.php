<?php
namespace app\assets;

use yii\web\AssetBundle;

class DeveloperMarginFormAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
      'js/developer_margin.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
    'app\assets\DatePickerAsset',
  ];
}
