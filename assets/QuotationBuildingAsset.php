<?php
namespace app\assets;

use yii\web\AssetBundle;

class QuotationBuildingAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
      'js/quotation_building.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
  ];
}
