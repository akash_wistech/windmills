<?php
namespace app\assets;

use yii\web\AssetBundle;

class DateRangePickerAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/daterangepicker/daterangepicker.css',
  ];
  public $js = [
    'plugins/daterangepicker/daterangepicker.js',
  ];
  public $depends = [
    'app\assets\MomentAsset'
  ];
}
