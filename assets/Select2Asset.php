<?php
namespace app\assets;

use yii\web\AssetBundle;

class Select2Asset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/select2/css/select2.min.css',
    'plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css',
  ];
  public $js = [
    'plugins/select2/js/select2.full.min.js',
  ];
  public $depends = [
  ];
}
