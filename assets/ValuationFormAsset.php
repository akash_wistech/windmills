<?php
namespace app\assets;

use yii\web\AssetBundle;

class ValuationFormAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
      'js/valuation.js',
      'js/quotation.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
    'app\assets\DatePickerAsset',
    'app\assets\AutoCompleteAsset',
  ];
}
