<?php
namespace app\assets;

use yii\web\AssetBundle;

class LightBoxAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/ekko-lightbox/ekko-lightbox.css',
  ];
  public $js = [
    'plugins/ekko-lightbox/ekko-lightbox.min.js',
  ];
  public $depends = [
  ];
}
