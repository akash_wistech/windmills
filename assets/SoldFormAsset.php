<?php
namespace app\assets;

use yii\web\AssetBundle;

class SoldFormAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
      'js/sold_transactions.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
    'app\assets\DatePickerAsset',
  ];
}
