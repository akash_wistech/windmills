<?php
namespace app\assets;

use yii\web\AssetBundle;

class NumeralAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
    'plugins/numeral/numeral.min.js',
  ];
  public $depends = [
  ];
}
