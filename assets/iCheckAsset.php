<?php
namespace app\assets;

use yii\web\AssetBundle;

class iCheckAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/icheck-bootstrap/icheck-bootstrap.min.css',
  ];
  public $js = [
  ];
  public $depends = [
  ];
}
