<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'plugins/fontawesome-free/css/all.min.css',
        'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',
        'css/adminlte.css',
        'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700',
        'assets/sweetalert/dist/sweetalert.css',
        'css/custom.css',
    ];
    public $js = [
        'plugins/bootstrap/js/bootstrap.bundle.min.js',
        'plugins/block-ui/jquery.blockui.min.js',
        'js/adminlte.min.js',
        'assets/sweetalert/dist/sweetalert.min.js', // Sweet alerts Core
        'assets/pages/jquery.sweet-alert.init.js', // Sweet alerts Core
        'js/custom.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'app\assets\SweetAlertAsset',
    ];
}
