<?php
namespace app\assets;

use yii\web\AssetBundle;

class SweetAlertAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/sweetalert2/sweetalert2.min.css',
    'plugins/sweetalert2/bootstrap-4.css',
  ];
  public $js = [
    'plugins/sweetalert2/sweetalert2.min.js',
  ];
  public $depends = [
  ];
}
