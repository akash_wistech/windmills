<?php
namespace app\assets;

use yii\web\AssetBundle;

class ProspectFormAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
  ];
  public $depends = [
    'app\assets\AppAsset',
    'app\assets\AutoCompleteAsset',
    'app\assets\DatePickerAsset',
    'app\assets\StarRatingAsset',
    'app\assets\NumeralAsset',
    'app\assets\Select2Asset',
    'app\assets\JQueryTagsInputAsset',
  ];
}
