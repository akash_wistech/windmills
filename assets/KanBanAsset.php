<?php
namespace app\assets;

use yii\web\AssetBundle;

class KanBanAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/kanban/kanban.bundle.css',
  ];
  public $js = [
    'plugins/kanban/kanban.bundle.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
  ];
}
