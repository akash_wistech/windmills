<?php
namespace app\assets;

use yii\web\AssetBundle;

class CustomFieldsAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
  ];
  public $depends = [
    'app\assets\AppAsset',
    'app\assets\AutoCompleteAsset',
    'app\assets\DatePickerAsset',
    'app\assets\DateRangePickerAsset',
    'app\assets\JQueryTimePickerAsset',
    'app\assets\DateTimePairAsset',
    'app\assets\Select2Asset',
    'app\assets\TinyMceAsset',
  ];
}
