<?php
namespace app\assets;

use yii\web\AssetBundle;

class ColorPickerAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/spectrum-colorpicker/spectrum.css',
  ];
  public $js = [
    'plugins/spectrum-colorpicker/spectrum.js',
  ];
  public $depends = [
  ];
}
