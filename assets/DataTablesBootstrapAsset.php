<?php
namespace app\assets;

use yii\web\AssetBundle;

class DataTablesBootstrapAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/DataTables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css',
  ];
  public $js = [
    'plugins/DataTables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js',
  ];
  public $depends = [
    'app\assets\DataTablesAsset',
  ];
}
