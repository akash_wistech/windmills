<?php
namespace app\assets;

use yii\web\AssetBundle;

class AppLoginAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/fontawesome-free/css/all.min.css',
    'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',
    'css/adminlte.min.css',
    'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700',
    'css/custom.css',
  ];
  public $js = [
    'plugins/bootstrap/js/bootstrap.bundle.min.js',
    'js/adminlte.min.js',
  ];
  public $depends = [
    'yii\web\YiiAsset',
  ];
}
