<?php
namespace app\assets;

use yii\web\AssetBundle;

class DatePickerAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css',
  ];
  public $js = [
    'plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js',
  ];
  public $depends = [
    'app\assets\MomentAsset'
  ];
}
