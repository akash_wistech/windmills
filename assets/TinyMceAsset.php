<?php
namespace app\assets;

use Yii;
use yii\web\AssetBundle;

class TinyMceAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
    'plugins/tinymce/tinymce.min.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
  ];
}
