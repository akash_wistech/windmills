<?php
namespace app\assets;

use yii\web\AssetBundle;

class DateRangePickerAsset2 extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/daterangepicker/daterangepicker.css',
  ];
  public $js = [
    'plugins/moment/moment.min.js',
    'plugins/daterangepicker/daterangepicker.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
  ];
}
