<?php
namespace app\assets;

use yii\web\AssetBundle;

class JQueryTimePickerAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'plugins/jquery-timepicker/jquery.timepicker.min.css',
  ];
  public $js = [
    'plugins/jquery-timepicker/jquery.timepicker.min.js',
  ];
  public $depends = [
    'app\assets\AppAsset',
  ];
}
