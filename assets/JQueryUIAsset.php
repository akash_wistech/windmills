<?php
namespace app\assets;

use yii\web\AssetBundle;

class JQueryUIAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
    'plugins/jquery-ui-1.12.1/jquery-ui.js',
  ];
  public $depends = [
  ];
}
