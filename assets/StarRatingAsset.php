<?php
namespace app\assets;

use yii\web\AssetBundle;

class StarRatingAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    //'plugins/rater/css/starrr.css',
  ];
  public $js = [
    'plugins/rater/rater.min.js',
  ];
  public $depends = [
  ];
}
