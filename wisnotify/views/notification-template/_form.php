<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\modules\wisnotify\helpers\NotificationListHelper;
use app\modules\wisnotify\assets\NotificationTemplateFormAsset;
NotificationTemplateFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\NotificationTemplate */
/* @var $form yii\widgets\ActiveForm */
$sysLanguages=NotificationListHelper::getSystemLanguages();
$rtlLangs=NotificationListHelper::getSystemRtlLanguages();
$notificationServices=NotificationListHelper::getNotificationTypes();
$rtlLangs=NotificationListHelper::getSystemRtlLanguages();

$this->registerJs('
tinymce.init({
    selector: ".textEditor",
    menubar: false,
    toolbar: ["styleselect fontselect fontsizeselect | link image",
        "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code",
        "advlist | autolink | lists"],
    plugins : "advlist autolink link image lists charmap code"
});
tinymce.init({
    selector: ".textEditor-rtl",
    directionality : "rtl",
    menubar: false,
    toolbar: ["styleselect fontselect fontsizeselect | link image",
        "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | subscript superscript charmap | code",
        "advlist | autolink | lists"],
    plugins : "advlist autolink link image lists charmap code"
});
');
$colLen=6;
$totalLangs = count($sysLanguages);
if($totalLangs==1)$colLen=12;
?>
<section class="notification-template-form card card-outline card-primary">
  <header class="card-header">
    <h2 class="card-title"><?= $cardTitle?></h2>
  </header>
  <?php $form = ActiveForm::begin(); ?>
  <div class="card-body">
    <?= $form->field($model, 'keyword')->textInput(['maxlength' => true])?>
    <div class="row">
      <?php
      foreach($sysLanguages as $key=>$val){
        $fldLabel = $model->getAttributeLabel('subject').' ('.$val.')';
        if($totalLangs==1){
          $fldLabel = $model->getAttributeLabel('subject');
        }
      ?>
      <div class="col-md-<?= $colLen?>">
        <?= $form->field($model, 'subject['.$key.']')->textInput(['style'=>in_array($key,$rtlLangs) ? 'direction:rtl' : '', 'maxlength' => true])->label($fldLabel)?>
      </div>
      <?php }?>
    </div>
    <h3>Emails</h3>
    <div class="row">
      <?php foreach($sysLanguages as $key=>$val){?>
      <div class="col-md-<?= $colLen?>">
        <?= $form->field($model, 'email_body['.$key.']')->textArea(['class'=>'form-control'.(in_array($key,$rtlLangs) ? ' textEditor-rtl' : ' textEditor'), 'rows' => 5])->label(false)?>
      </div>
      <?php }?>
    </div>
    <?php if(isset($notificationServices['sms'])){?>
    <h3>SMS</h3>
    <div class="row">
      <?php foreach($sysLanguages as $key=>$val){?>
      <div class="col-md-<?= $colLen?>">
        <?= $form->field($model, 'sms_body['.$key.']')->textArea(['class'=>'form-control', 'rows' => 5])->label(false)?>
      </div>
      <?php }?>
    </div>
    <?php }?>
    <?php if(isset($notificationServices['push'])){?>
    <h3>Push Notification</h3>
    <div class="row">
      <?php foreach($sysLanguages as $key=>$val){?>
      <div class="col-md-<?= $colLen?>">
        <?= $form->field($model, 'push_body['.$key.']')->textArea(['class'=>'form-control', 'rows' => 5])->label(false)?>
      </div>
      <?php }?>
    </div>
    <?php }?>
  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>
