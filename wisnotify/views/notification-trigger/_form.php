<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\modules\wisnotify\helpers\NotificationListHelper;
use app\modules\wisnotify\assets\NotificationTriggerFormAsset;
NotificationTriggerFormAsset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\NotificationTrigger */
/* @var $form yii\widgets\ActiveForm */

$notificationEvents=NotificationListHelper::getSystemNotificationEvents();
$userTypes=NotificationListHelper::getSystemUserTypes();
$templates=NotificationListHelper::getNotificationTemplatesArr();
$notificationServices=NotificationListHelper::getNotificationTypes();
$staffRoles=NotificationListHelper::getStaffRolesListArr();

$this->registerJs('
$(".selectpicker").selectpicker();
');

?>
<section class="notification-trigger-form card card-outline card-primary">
  <header class="card-header">
    <h2 class="card-title"><?= $cardTitle?></h2>
  </header>
  <?php $form = ActiveForm::begin(); ?>
  <div class="card-body">
    <div class="row">
      <div class="col-md-4">
        <?= $form->field($model, 'event_id')->dropDownList($notificationEvents,['class'=>'form-control selectpicker',]) ?>
      </div>
      <div class="col-md-4">
        <?= $form->field($model, 'user_type')->dropDownList($userTypes,['class'=>'form-control selectpicker']) ?>
      </div>
      <div class="col-md-4">
        <?= $form->field($model, 'cc_roles')->dropDownList($staffRoles,['class'=>'form-control selectpicker','multiple'=>'multiple']) ?>
      </div>
      <div class="col-md-8">
        <?= $form->field($model, 'template_id')->dropDownList($templates,['class'=>'form-control selectpicker',]) ?>
      </div>
      <div class="col-md-4">
        <?= $form->field($model, 'notification_triggers')->dropDownList($notificationServices,['class'=>'form-control selectpicker','multiple'=>'multiple']) ?>
      </div>
    </div>
  </div>
  <div class="card-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</section>
