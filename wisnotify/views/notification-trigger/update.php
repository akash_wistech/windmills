<?php

use yii\helpers\Html;
use app\modules\wisnotify\helpers\NotificationListHelper;

/* @var $this yii\web\View */
/* @var $model app\models\NotificationTrigger */

$eventLabels = NotificationListHelper::getSystemNotificationEventLabels();

$this->title = Yii::t('app', 'Notification Triggers');
$cardTitle = Yii::t('app','Notification Trigger:  {nameAttribute}', [
    'nameAttribute' => $eventLabels[$model->event_id],
]);
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="notification-trigger-update">
    <?= $this->render('_form', [
        'model' => $model,
        'cardTitle' => $cardTitle,
    ]) ?>
</div>
