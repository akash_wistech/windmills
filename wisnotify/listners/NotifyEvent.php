<?php
namespace app\modules\wisnotify\listners;

use Yii;
use yii\base\Event;
use yii\base\Component;
use app\modules\wisnotify\models\NotificationTrigger;
use app\modules\wisnotify\models\NotificationTemplate;
use app\models\User;

class NotifyEvent extends Component
{
  public static function fire($eventId,$data)
  {
    // echo "<pre>";
    // print_r($eventId);
    // echo "</pre>";
    // die();

    

    $dbTriggers = NotificationTrigger::find()->where(['event_id'=>$eventId])->all();
    $moduleId = \Yii::$app->getModule('wisnotify');


    $replacements = isset($data['replacements']) ? $data['replacements'] : null;
    $attachments = isset($data['attachments']) ? $data['attachments'] : null;
    // Log::debug($eventId);
    if($dbTriggers!=null){
      foreach($dbTriggers as $dbTrigger){
        $userType = $dbTrigger['user_type'];
        $ccUserTypes = json_decode($dbTrigger['cc_roles']);
        $template = NotificationTemplate::findOne($dbTrigger['template_id']);
        $notificationTypes = json_decode($dbTrigger['notification_triggers']);

        if($userType!=null && $userType!=''){
          // foreach($userTypes as $key=>$val){
            $toInfo=[];
            if($userType==0){
              //Client
              $toUser = $data['client'];
              $toInfo[] = [
                'name'=>$toUser->title,
               // 'email'=>$toUser->primaryContact->email !== '' ? $toUser->primaryContact->email : '',
               // 'email'=> 'akash@wistech.biz',
                'mobile'=>'',
                'deviceIdz' => null,
              ];
    // echo "<pre>";
    // print_r($toInfo);
    // echo "</pre>";
    // die();
            }else{
              $toUsers = User::find()->where(['permission_group_id'=>$userType,'status'=>1])->all();
              if($toUsers!=null){
                foreach($toUsers as $toUser){
                  $toInfo[]=[
                    'name'=>$toUser->name,
                    'email'=>$toUser->email,
                    'mobile'=>'',
                    'deviceIdz' => null,
                  ];
                }
              }
            }
            $ccUsers = User::find()->where(['permission_group_id'=>$ccUserTypes,'status'=>1])->all();

            if($notificationTypes!=null){
              foreach($notificationTypes as $ntKey=>$ntVal){
                //Email
                if($ntVal=='email'){
                  (new NotifyEvent)->sendEmail($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments);
                }
                //Sms
                if($ntVal=='sms'){
                  (new NotifyEvent)->sendSms($moduleId,$template,$replacements,$toInfo);
                }
                //Push
                if($ntVal=='push'){
                  (new NotifyEvent)->sendPush($moduleId,$template,$replacements,$toInfo);
                }
              }
            }
          // }
        }
      }
    }
  }

  /*
  * Send Emsil
  */
  public function sendEmail($moduleId,$template,$replacements,$toInfo,$ccUsers,$attachments){
    $emailBody = $template->langDetail->email_body;
    if($replacements!=null){
      foreach($replacements as $rKey=>$rVal){
        $emailBody = str_replace($rKey,$rVal,$emailBody);
      }
    }
    if($toInfo!=null){
      foreach($toInfo as $key=>$info){
        if ($info['email'] !='') {
          $emailBody = str_replace("{name}",$info['name'],$emailBody);
        $emailBody = str_replace("{clientName}",$info['name'],$emailBody);

        Yii::$app->mailer->viewPath = '@app/modules/wisnotify/mail';
        $mail = Yii::$app->mailer->compose();//compose(['html' => 'notification-html', 'text' => 'notification-text'], ['emailBody' => $emailBody, 'textBody'=>strip_tags($emailBody)]);
        $mail->setTextBody(strip_tags($emailBody));
        $mail->setHtmlBody($emailBody);
        $mail->setFrom([$moduleId->params['notificationSenderEmail'] => $moduleId->params['nNotificationSenderName']]);
        $mail->setTo($info['email']);
        if($ccUsers!=null){
          foreach($ccUsers as $ccUser){
            $ccEmails[]=$ccUser->email;
          }
          if($ccEmails!=null)$mail->setCc($ccEmails);
        }
        $mail->setSubject($template->langDetail->subject);
        if($attachments!=null){
          foreach($attachments as $attachment){
              $mail->attach($attachment);
          }
        }
        $mail->send();
        }
        
      }
    }
  }

  /*
  * Send Sms
  */
  public function sendSms($moduleId,$template,$replacements,$toInfo){
    $smsBody = $template->langDetail->sms_body;
    if($replacements!=null){
      foreach($replacements as $rKey=>$rVal){
        $smsBody = str_replace($rKey,$rVal,$smsBody);
      }
    }
    if($toInfo!=null){
      foreach($toInfo as $key=>$info){
        if($info['mobile']!=''){
          $smsBody = str_replace("{name}",$toInfo['name'],$smsBody);
        }
      }
    }
  }

  /*
  * Send Push Notification
  */
  public function sendPush($moduleId,$template,$replacements,$toInfo){
    $pushBody = $template->langDetail->push_body;
    if($replacements!=null){
      foreach($replacements as $rKey=>$rVal){
        $pushBody = str_replace($rKey,$rVal,$pushBody);
      }
    }
    $pushBody = str_replace("{name}",$toInfo['name'],$pushBody);
  }
}
