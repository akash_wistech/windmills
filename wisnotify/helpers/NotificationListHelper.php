<?php
namespace app\modules\wisnotify\helpers;

use  app\modules\wisnotify\models\Setting;
use  app\modules\wisnotify\models\NotificationTemplate;
use  app\modules\wisnotify\models\NotificationTemplateDetail;
use app\models\AdminGroup;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
* This is a helper class for notification lists
*/
class NotificationListHelper
{
  /**
  * return all languages
  */
  public static function getSystemLanguages()
  {
    return [
      'en'=>'English',
      // 'ar'=>'Arabic',
    ];
  }

  /**
  * return all rtl languages
  */
  public static function getSystemRtlLanguages()
  {
    return [
      'ar',
    ];
  }

  /**
  * return all user types
  */
  public static function getSystemUserTypes()
  {
    $arr = [];
    $arr[0]='Client';
    $roles = (new NotificationListHelper)->getStaffRolesList();
    if($roles!=null){
      foreach($roles as $role){
        $arr[$role['id']]=$role['title'];
      }
    }
    return $arr;
  }

  /**
  * return events
  */
  public static function getSystemNotificationEventsArr()
  {
    return [
      'Evaluation'=>[
        // 'evaluation.created'=>['shortLabel'=>'Submitted','fullLabel'=>'Evaluation Submitted'],
        // 'evaluation.approved'=>['shortLabel'=>'Approved','fullLabel'=>'Evaluation Approved'],
        // 'evaluation.rejected'=>['shortLabel'=>'Rejected','fullLabel'=>'Evaluation Rejected'],
      ],
      'Quotation'=>[
        'quotation.send'=>['shortLabel'=>'Quotation Send','fullLabel'=>'Quotation Send'],
        'toe.send'=>['shortLabel'=>'Toe Send','fullLabel'=>'Toe Send'],
        // 'quote.submitted'=>['shortLabel'=>'Submitted','fullLabel'=>'Quotation Submitted'],
        // 'quote.accepted'=>['shortLabel'=>'Accepted','fullLabel'=>'Quotation Accepted'],
      ],
       'Valuation'=>[
        'schedule.send'=>['shortLabel'=>'Schedule Inspection Send','fullLabel'=>'Schedule Inspection Send'],
        'summary.send'=>['shortLabel'=>'Summary Send','fullLabel'=>'Summary  Send'],
        'review.send'=>['shortLabel'=>'Review Send','fullLabel'=>'Review Send'],
        'approval.send'=>['shortLabel'=>'Approval Send','fullLabel'=>'Approval Send'],
        'inspectProperty.send'=>['shortLabel'=>'Inspect Property Send','fullLabel'=>'Inspect Property Send'],
      ], 
       'Company'=>[
        'Client.pdf.send'=>['shortLabel'=>'Client Pdf Send','fullLabel'=>'Client Pdf Send'],
      ],
    ];
  }

  /**
  * return events for triggers dropdown
  */
  public static function getSystemNotificationEvents()
  {
    $arr = [];
    $events = (new NotificationListHelper)::getSystemNotificationEventsArr();
    if($events!=null){
      foreach($events as $mkey=>$eventHeadings){
        $subArr=[];
        foreach($eventHeadings as $key=>$eventInfo){
          $subArr[$key]=$eventInfo['shortLabel'];
        }
        $arr[$mkey]=$subArr;
      }
    }
    return $arr;
  }

  /**
  * return events labels for grid
  */
  public static function getSystemNotificationEventLabels()
  {
    $arr = [];
    $events = (new NotificationListHelper)::getSystemNotificationEventsArr();
    if($events!=null){
      foreach($events as $mkey=>$eventHeadings){
        foreach($eventHeadings as $key=>$eventInfo){
          $arr[$key]=$eventInfo['fullLabel'];
        }
      }
    }
    return $arr;
  }

  /**
  * return notification services
  */
  public static function getNotificationTypes()
  {
    $allowedServices=[];
    $settings=Setting::find()->where(['config_name'=>['enable_email_notification','enable_sms_notification','enable_inapp_notification']])->all();
    foreach($settings as $record){
      if($record['config_name']=='enable_email_notification' && $record['config_value']==1)$allowedServices['email']='Email';
      if($record['config_name']=='enable_sms_notification' && $record['config_value']==1)$allowedServices['sms']='SMS';
      if($record['config_name']=='enable_inapp_notification' && $record['config_value']==1)$allowedServices['push']='Push Notification';
    }
    return $allowedServices;
  }

  /**
  * return templates
  */
  public static function getNotificationTemplates()
  {
    return NotificationTemplate::find()
    ->select([
      NotificationTemplate::tableName().'.id',
      'CONCAT(subject," (",keyword,")") AS tmpsubject'
    ])
    ->innerJoin(NotificationTemplateDetail::tableName(),NotificationTemplateDetail::tableName().".[[notification_template_id]]=".NotificationTemplate::tableName().".[[id]]")
    ->where([
      'and',
      ['lang'=>'en'],
      ['is','deleted_at',new Expression('null')]
    ])
    ->asArray()->all();
  }
  /**
  * return templates array
  */
  public static function getNotificationTemplatesArr()
  {
    return ArrayHelper::map((new NotificationListHelper)->getNotificationTemplates(),"id","tmpsubject");
  }

  /**
  * return staff roles
  */
  public static function getStaffRolesList()
  {
    return AdminGroup::find()->where(['and',['!=','id',1],['status'=>1,'trashed'=>0]])->asArray()->all();
  }

  /**
  * return staff roles array
  */
  public static function getStaffRolesListArr()
  {
    return ArrayHelper::map((new NotificationListHelper)->getStaffRolesList(),"id","title");
  }
}
