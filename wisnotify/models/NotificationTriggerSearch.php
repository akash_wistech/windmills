<?php

namespace app\modules\wisnotify\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\wisnotify\models\NotificationTrigger;
use yii\db\Expression;

/**
* NotificationTriggerSearch represents the model behind the search form about `app\models\NotificationTrigger`.
*/
class NotificationTriggerSearch extends NotificationTrigger
{
  public $pageSize;

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id', 'template_id', 'status', 'created_by', 'updated_by', 'trashed', 'trashed_by','pageSize'], 'integer'],
      [['event_id', 'user_type', 'notification_triggers', 'created_at', 'updated_at', 'trashed_at'], 'safe'],
    ];
  }

  /**
  * @inheritdoc
  */
  public function scenarios()
  {
    // bypass scenarios() implementation in the parent class
    return Model::scenarios();
  }

  /**
  * Creates data provider instance with search query applied
  *
  * @param array $params
  *
  * @return ActiveDataProvider
  */
  public function search($params)
  {
    $this->load($params);

    $query = NotificationTrigger::find();

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => [
        'pageSize' => $this->pageSize!=null ? $this->pageSize : array_values(Yii::$app->appHelperFunctions->pageSizeArray)[0],
      ],
    ]);

    $query->andFilterWhere([
      'id' => $this->id,
      'status' => $this->status,
      'event_id' => $this->event_id,
      'user_type' => $this->user_type,
      'template_id' => $this->template_id,
      'notification_triggers' => $this->notification_triggers,
    ]);
    $query->andWhere([
        'is','deleted_at',new Expression('null')
    ]);

    return $dataProvider;
  }
}
