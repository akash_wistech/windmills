<?php
namespace app\modules\wisnotify\models;

use Yii;
use app\components\models\ActiveRecordFull;
use app\modules\wisnotify\helpers\NotificationListHelper;

class NotificationTemplate extends ActiveRecordFull
{
  public $subject,$email_body,$push_body,$sms_body;
	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%notification_templates}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['keyword','subject'], 'required'],
			[['user_type', 'notification_triggers', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
			[['created_by', 'updated_by', 'deleted_by'], 'integer'],
			[['keyword'], 'string', 'max' => 255],
			[['subject','push_body','sms_body'], 'each', 'rule'=>['string', 'max' => 255]],
			[['email_body'], 'each', 'rule'=>['string']],
      [['keyword'],'trim'],
      [['subject','email_body','push_body','sms_body'],'each','rule'=>['trim']],
		];
	}

	/**
	* @inheritdoc
	*/
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'keyword' => Yii::t('app', 'keyword'),
			'subject' => Yii::t('app', 'Subject'),
			'email_body' => Yii::t('app', 'Email Body'),
			'push_body' => Yii::t('app', 'Push Notification Message'),
			'sms_body' => Yii::t('app', 'SMS Message'),
			'created_at' => Yii::t('app', 'Created At'),
			'created_by' => Yii::t('app', 'Created By'),
			'updated_at' => Yii::t('app', 'Updated At'),
			'updated_by' => Yii::t('app', 'Updated By'),
			'deleted_at' => Yii::t('app', 'Trashed At'),
			'deleted_by' => Yii::t('app', 'Trashed By'),
		];
	}

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecTitle()
  {
    return $this->keyword;
  }

  /**
  * returns main title/name of row for status and delete loging
  */
  public function getRecType()
  {
    return 'Notification Template';
  }

	/**
	* @inheritdoc
	*/
	public function afterSave($insert, $changedAttributes)
	{
    $sysLangs = NotificationListHelper::getSystemLanguages();
    foreach ($sysLangs as $key=>$val) {
      if(isset($this->subject[$key])){
        $subject = $this->subject[$key];
        $email_body = isset($this->email_body[$key]) ? $this->email_body[$key] : '';
        $push_body = isset($this->push_body[$key]) ? $this->push_body[$key] : '';
        $sms_body = isset($this->sms_body[$key]) ? $this->sms_body[$key] : '';

        $langDetail = NotificationTemplateDetail::find()->where(['notification_template_id'=>$this->id,'lang'=>$key])->one();
        if($langDetail==null){
          $langDetail = new NotificationTemplateDetail();
          $langDetail->notification_template_id = $this->id;
          $langDetail->lang = $key;
        }
        $langDetail->subject = $subject;
        $langDetail->email_body = $email_body;
        $langDetail->push_body = $push_body;
        $langDetail->sms_body = $sms_body;
        $langDetail->save();
      }
    }
		parent::afterSave($insert, $changedAttributes);
	}

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getLangDetails()
  {
    return $this->hasMany(NotificationTemplateDetail::className(), ['notification_template_id' => 'id']);
  }

  /**
  * @return \yii\db\ActiveQuery
  */
  public function getLangDetail()
  {
    return $this->hasOne(NotificationTemplateDetail::className(), ['notification_template_id' => 'id'])->andWhere(['lang'=>'en']);
  }
}
