<?php

namespace app\modules\wisnotify\models;

use Yii;
use yii\base\Model;

/**
* NotificationSettingsForm is the model behind the setting form.
*/
class NotificationSettingsForm extends Model
{
  public $enable_email_notification,$enable_sms_notification,$enable_inapp_notification;


  /**
  * @return array the validation rules.
  */
  public function rules()
  {
    return [
      [[
        'enable_email_notification','enable_sms_notification','enable_inapp_notification'
      ], 'integer'],
    ];
  }

  /**
  * @return array customized attribute labels
  */
  public function attributeLabels()
  {
    return [
      'enable_email_notification' => 'Email',
      'enable_sms_notification' => 'SMS',
      'enable_inapp_notification' => 'Push Notification',
    ];
  }

  /**
  * save the settings data.
  */
  public function save()
  {
    if ($this->validate()) {
      $post=Yii::$app->request->post();
      foreach($post['SettingForm'] as $key=>$val){
        $setting=Setting::find()->where("config_name='$key'")->one();
        if($setting==null){
          $setting=new Setting;
          $setting->config_name=$key;
        }
        $setting->config_value=$val;
        $setting->save();
      }
      return true;
    } else {
      return false;
    }
  }
}
