<?php

use yii\db\Migration;

/**
 * Class m210707_093808_create_notification_trigger
 */
class m210707_093808_create_notification_trigger extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210707_093808_create_notification_trigger cannot be reverted.\n";

        return false;
    }

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
      $this->createTable('notification_triggers', [
            'id' => $this->primaryKey(),
            'event_id' => $this->string(50),
            'user_type' => $this->string(150),
            'cc_roles' => $this->string(150),
            'template_id' => $this->integer(),
            'notification_triggers' => $this->string(50),
            'status' => $this->integer(1)->defaultValue(1),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'deleted_at' => $this->dateTime(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'deleted_by' => $this->integer()
        ]);
    }

    public function down()
    {
      $this->dropTable('notification_triggers');
    }
}
