<?php
return [
    'params' => [
        'enable_sms' => true,
        'enable_apppush' => true,
        'notificationSenderEmail' => 'info@notify.com',
        'nNotificationSenderName' => 'Notify Sys',
    ],
];
