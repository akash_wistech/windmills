<?php
namespace app\modules\wisnotify\assets;

use yii\web\AssetBundle;

class NotificationTriggerFormAsset extends AssetBundle
{
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
  ];
  public $js = [
  ];
  public $depends = [
    'app\assets\AppAsset',
    'app\assets\SelectPickerAsset',
  ];
}
